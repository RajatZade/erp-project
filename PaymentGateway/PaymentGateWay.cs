﻿using System;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using DataLayer;
using DataLayer.ViewModels;

namespace PaymentGateway
{
    public class PaymentGateWay
    {
        public void GotoPayment(PaymentGateWayViewModel model, string btn)
        {
            string firstName = model.Name;
            string amount = model.Amount;
            string productInfo = "Data";
            string email = null;
            string phone = null;
            if (model.Email != null)
            {
                email = model.Email;
            }
            else
            {
                email = "";
            }

            if (model.Phone != null)
            {
                phone = model.Phone;
            }
            else
            {
                phone = "";
            }
            string surl = model.OnSuccess;
            string furl = model.OnFailure;
            string udf1 = model.OrderId;
            RemotePost myremotepost = new RemotePost();
            //posting all the parameters required for integration.
            myremotepost.Url = ConfigurationManager.AppSettings["PAYU_BASE_URL"];
            myremotepost.Add("key", ConfigurationManager.AppSettings["MERCHANT_KEY"]);
            string txnid = Generatetxnid();
            myremotepost.Add("txnid", txnid);
            myremotepost.Add("amount", amount);
            myremotepost.Add("productinfo", productInfo);
            myremotepost.Add("firstname", firstName);
            myremotepost.Add("phone", phone);
            myremotepost.Add("email", email);
            myremotepost.Add("surl", surl);//Change the success url here depending upon the port number of your local system.
            myremotepost.Add("furl", furl);//Change the failure url here depending upon the port number of your local system.
            myremotepost.Add("udf1", udf1);
            myremotepost.Add("udf2", btn);
            myremotepost.Add("service_provider", "payu_paisa");
            string hashString = ConfigurationManager.AppSettings["MERCHANT_KEY"] + "|" + txnid + "|" + amount + "|" + productInfo + "|" + firstName + "|" + email + "|" + udf1 + "|"+ btn + "|||||||||" + ConfigurationManager.AppSettings["SALT"];
            //string hashString = "3Q5c3q|2590640|3053.00|OnlineBooking|vimallad|ladvimal@gmail.com|||||||||||mE2RxRwx";
            string hash = Generatehash512(hashString);
            myremotepost.Add("hash", hash);
            myremotepost.Add("abc", hashString);
            myremotepost.Post();
        }

        public string Generatetxnid()
        {
            Random rnd = new Random();
            string strHash = Generatehash512(rnd.ToString() + DateTime.Now);
            string txnid1 = strHash.ToString().Substring(0, 20);
            return txnid1;
        }

        public string Generatehash512(string text)
        {
            byte[] message = Encoding.UTF8.GetBytes(text);
            UnicodeEncoding UE = new UnicodeEncoding();
            byte[] hashValue;
            SHA512Managed hashString = new SHA512Managed();
            string hex = "";
            hashValue = hashString.ComputeHash(message);
            foreach (byte x in hashValue)
            {
                hex += String.Format("{0:x2}", x);
            }
            return hex;
        }
    }
    public class RemotePost
    {
        private System.Collections.Specialized.NameValueCollection Inputs = new System.Collections.Specialized.NameValueCollection();
        public string Url = "";
        public string Method = "post";
        public string FormName = "form1";
        public void Add(string name, string value)
        {
            Inputs.Add(name, value);
        }
        public void Post()
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Write("<html><head>");
            HttpContext.Current.Response.Write(string.Format("</head><body onload=\"document.{0}.submit()\">", FormName));
            HttpContext.Current.Response.Write(string.Format("<form name=\"{0}\" method=\"{1}\" action=\"{2}\" >", FormName, Method, Url));
            for (int i = 0; i < Inputs.Keys.Count; i++)
            {
                HttpContext.Current.Response.Write(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", Inputs.Keys[i], Inputs[Inputs.Keys[i]]));
            }
            HttpContext.Current.Response.Write("</form>");
            HttpContext.Current.Response.Write("</body></html>");
            HttpContext.Current.Response.End();
        }
    }
}
