﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLayer.Models;
using BarCode.Models;
using System.Data.Entity;
using DataLayer.Utils;
using System.Web.Mvc;

namespace DataLayer.Utils
{
    public class WPSHandler
    {
        SaleInvoiceDbUtil saledb = new SaleInvoiceDbUtil();
        DbConnector db = new DbConnector();

        public bool RemoveSalesInvoice(SaleInvoice salesinvoice)
        {
            bool result = true;
            List<int> saleInvoiceItemIds = salesinvoice.SaleInvoiceItemList.Select(x => x.SaleInvoiceItemId).ToList();
            List<int> productIds = salesinvoice.SaleInvoiceItemList.Select(x => x.ProductId).ToList();
            List<Product> productList = saledb.GetProductsBySaleInvoiceItems(productIds);
            List<SaleInvoiceItem> saleInvoiceItems = db.SaleInvoiceItems.Where(x => saleInvoiceItemIds.Contains(x.SaleInvoiceItemId)).ToList();

            //Mark products as unsold            
            List<Product> UpdateProducts = new List<Product>();
            foreach (var item in saleInvoiceItems)
            {
                if(item.Status == Constants.SALE_ITEM_SOLD)
                {
                    Product product = productList.Where(x => x.ProductId == item.ProductId).FirstOrDefault();
                    if (product.SellInPartial)
                    {
                        product.Quantity = product.Quantity + item.ConversionValue.Value;
                    }
                    product.IsSold = false;
                    UpdateProducts.Add(product);
                }
                else if(item.Status == Constants.SALE_ITEM_EXCESSES)
                {
                    Product product = productList.Where(x => x.ProductId == item.ProductId).FirstOrDefault();
                    if (product.SellInPartial)
                    {
                        product.Quantity = product.Quantity - item.ConversionValue.Value;
                        if(product.Quantity < 1 || product.Quantity == 0)
                        {
                            product.IsSold = false;
                        }
                    }
                    else
                    {
                        product.IsSold = false;
                    }
                    UpdateProducts.Add(product);
                }
            }

            if (saledb.DeleteSaleInvoice(UpdateProducts, saleInvoiceItems, salesinvoice))
            {
                result = true;
            }
            return result;
        }

        public SaleInvoice SaveSaleInvoice(SaleInvoice saleInvoice, string btn)
        {
            switch (btn)
            {
                case "Hold":
                    {
                        saleInvoice.Status = Constants.HOLD;
                        SaveSaleInvoice(saleInvoice);
                        SaveSaleInvoiceItem(saleInvoice.SaleInvoiceId);                        
                        SessionManager.EmptySessionList(Constants.SALE_TYPE_WPS);
                        break;
                    }
                case "Make Payment":
                    {
                        SaveSaleInvoice(saleInvoice);
                        SaveSaleInvoiceItem(saleInvoice.SaleInvoiceId);
                        UpdateInventory(saleInvoice.SaleInvoiceId);
                        SessionManager.EmptySessionList(Constants.SALE_TYPE_WPS);
                        break;
                    }
                case "Save":
                    {
                        SaveSaleInvoice(saleInvoice);
                        SaveSaleInvoiceItem(saleInvoice.SaleInvoiceId);
                        UpdateInventory(saleInvoice.SaleInvoiceId);
                        SessionManager.EmptySessionList(Constants.SALE_TYPE_WPS);
                        break;
                    }
                case "Email":
                    {
                        break;
                    }
                case "SMS":
                    {
                        break;
                    }
            }
            return saleInvoice;
        }

        public string DeleteInvoice(int id)
        {
            string error = string.Empty;
            SaleInvoice saleinvoice = db.SaleInvoices.Include("SaleInvoiceItemList").Where(x => x.SaleInvoiceId == id).FirstOrDefault();
            if (saleinvoice == null)
            {
                error = MessageStore.DeleteError;
            }
            if (!RemoveSalesInvoice(saleinvoice))
            {
                error = "This invoice contains Products that have been sold. Please remove the item from the associated sale invoice and try again.";
            }
            return error;
        }

        public SaleInvoice GetSaleInvoiceForCreateEdit(int? id, bool? IsWPS, int? SalesOrderId,int? SalesOrderItemId)
        {
            SaleInvoice saleinvoice = null;
            try
            {              
                if (id.HasValue)
                {
                    saleinvoice = db.SaleInvoices.Include("SaleInvoiceItemList").Where(x => x.SaleInvoiceId == id).FirstOrDefault();
                    SessionManager.EmptySessionList(Constants.SALE_TYPE_WPS);
                    saleinvoice.SaleOrderList = DropdownManager.GetSaleOrderList(null,Enums.PurchaseOrderStatus.Closed);
                    saleinvoice.SaleMemoList = DropdownManager.GetDeliveryMemoList();
                }
                else
                {
                    saleinvoice = new SaleInvoice();

                    SaleInvoice LastSaleInvoice = db.SaleInvoices.OrderByDescending(x => x.SaleInvoiceId).Where(x => (x.Type == Constants.SALE_TYPE_SALE_INVOICE || x.Type == Constants.SALE_TYPE_SALE_VOUCHER) && x.CompanyId == db.CompanyId).FirstOrDefault();
                    if(LastSaleInvoice != null)
                    {
                        saleinvoice.BranchId = LastSaleInvoice.BranchId;
                        saleinvoice.CustomerId = LastSaleInvoice.CustomerId;
                        saleinvoice.DivisionId = LastSaleInvoice.DivisionId;
                        saleinvoice.EmployeeId = LastSaleInvoice.EmployeeId;
                        saleinvoice.LocationId = LastSaleInvoice.LocationId;
                        saleinvoice.SaleAccountId = LastSaleInvoice.SaleAccountId;
                        saleinvoice.SaleTypeId = LastSaleInvoice.SaleTypeId;
                    }
                    saleinvoice.BranchId = SessionManager.GetSessionBranch().BranchId;
                    saleinvoice.DivisionId = SessionManager.GetSessionDivision().DivisionId;
                    saleinvoice.SaleInvoiceItemList = new List<SaleInvoiceItem>();
                    saleinvoice.InvoicingDate = Convert.ToDateTime(DateTime.Now);
                    saleinvoice.Status = Constants.DRAFT;
                    saleinvoice.Type = Constants.SALE_TYPE_WPS;
                    #region SaleOrder Item
                    if (SalesOrderId.HasValue)
                    {
                        SalesOrder SalesOrder = db.GetSalesOrderById(SalesOrderId ?? 0);
                        saleinvoice.CustomerId = SalesOrder.CustomerId;
                        saleinvoice.BranchId = SalesOrder.BranchId;
                        saleinvoice.LocationId = SalesOrder.LocationId;
                        saleinvoice.DivisionId = SalesOrder.DivisionId;

                        SessionManager.EmptySessionList(Constants.SALE_TYPE_WPS);
                        saleinvoice.SaleInvoiceItemList = new List<SaleInvoiceItem>();
                        SalesOrder.SaleInvoiceItemList = db.GetInvoiceItemsForOrder(SalesOrderId.Value);
                        foreach (var items in SalesOrder.SaleInvoiceItemList)
                        {
                            SessionManager.AddSaleInvoiceItem(new SaleInvoiceItem(items), Constants.SALE_TYPE_WPS);
                        }
                        List <int> ProductIds = SalesOrder.SaleInvoiceItemList.Select(x => x.ProductId).ToList();
                        List<SalesOrderItem> SalesOrderItemList = db.SalesOrderItems.Where(x => x.SalesOrderId == SalesOrderId).ToList();
                        List<int> SkuId = SalesOrderItemList.Select(x => x.SKUMasterId).ToList();
                        List<Product> ProductToAdded = db.Products.Where(x => SkuId.Contains(x.SKUMasterId) && x.CompanyId == db.CompanyId && x.IsSold == false && !ProductIds.Contains(x.ProductId)).ToList();
                        foreach (var item in SalesOrderItemList)
                        {
                            List<Product> ProductVsSKU = ProductToAdded.Where(x => x.SKUMasterId == item.SKUMasterId).ToList();
                            if (ProductVsSKU.Count() >= item.Quantity)
                            {
                                for (int i = 0; i < item.Quantity; i++)
                                {
                                    ProductManager.AddInvoiceItem(ProductVsSKU[i], null, null,Constants.SALE_TYPE_WPS, Constants.SALE_ITEM_SOLD);
                                }
                            }
                            else
                            {
                                for (int i = 0; i < ProductVsSKU.Count; i++)
                                {
                                    ProductManager.AddInvoiceItem(ProductVsSKU[i], null, null, Constants.SALE_TYPE_WPS, Constants.SALE_ITEM_SOLD);
                                }
                            }
                        }
                        saleinvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_WPS);
                    }
                    else if (SalesOrderItemId.HasValue)
                    {
                        SalesOrderItem saleOrderitem = db.SalesOrderItems.Where(x => x.SalesOrderItemId == SalesOrderItemId.Value).FirstOrDefault();
                        if(saleOrderitem != null)
                        {
                            SalesOrder SalesOrder = db.GetSalesOrderById(saleOrderitem.SalesOrderId);
                            saleinvoice.CustomerId = SalesOrder.CustomerId;
                            saleinvoice.BranchId = SalesOrder.BranchId;
                            saleinvoice.LocationId = SalesOrder.LocationId;
                            saleinvoice.DivisionId = SalesOrder.DivisionId;
                            SessionManager.EmptySessionList(Constants.SALE_TYPE_WPS);
                            saleinvoice.SaleInvoiceItemList = new List<SaleInvoiceItem>();
                            saleinvoice.SalesOrderItemId = SalesOrderItemId;
                            //int PurchaseInvoiceItemId = ;
                            List<Product> ProductToAdded = db.Products.Where(x => x.PurchaseInvoiceItemId == (db.PurchaseInvoiceItems.Where(y => y.JobsheetId == saleOrderitem.JobSheetId).Select(y => y.PurchaseInvoiceItemId).FirstOrDefault())).ToList();
                            foreach(var item in ProductToAdded)
                            {
                                saleinvoice.SaleInvoiceItemList.Add(ProductManager.AddInvoiceItem(item, null, null, Constants.SALE_TYPE_WPS, Constants.SALE_ITEM_SOLD));
                            }
                            saleinvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_WPS);
                        }
                    }                    
                    else
                    {
                        var item = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_WPS);
                        if (item != null)
                        {
                            for (int i = 0; i < item.Count; i++)
                            {
                                saleinvoice.SaleInvoiceItemList.Add(item[i]);
                            }
                        }
                    }
                    #endregion

                    saleinvoice.SaleOrderList = DropdownManager.GetSaleOrderList(null, Enums.PurchaseOrderStatus.Open);
                    saleinvoice.SaleMemoList = DropdownManager.GetDeliveryMemoList();
                }

                if (saleinvoice.SaleInvoiceItemList != null)
                {
                    saleinvoice = SaleCalculator.Calculate(saleinvoice);
                }
                if (IsWPS == true)
                {
                    saleinvoice.IsWPS = true;
                }
                saleinvoice.LocationList = DropdownManager.GetLocationByBranch(saleinvoice.BranchId);
                saleinvoice.SaleAccountList = DropdownManager.GetLedgerListForType(null, Constants.LEDGER_TYPE_COMPANY);
                saleinvoice.CustomerAccountList = DropdownManager.GetLedgerListForType(null, Constants.LEDGER_TYPE_USER);
                saleinvoice.SaleTypeList = DropdownManager.GetLedgerListForType(null, Constants.LEDGER_TYPE_TAX);
                saleinvoice.EmployeeAccountList = DropdownManager.GetEmployeeList(null);
                saleinvoice.PaymentTypeList = DropdownManager.GetPaymentTypeList(null);
                //saleinvoice.BankAccountsList = DropdownManager.GetBankAccountsList(true, saleinvoice.LedgerBankId ?? 0);
                saleinvoice.DivisionList = DropdownManager.GetDivisionList(null);
                saleinvoice.BranchList = DropdownManager.GetBranchList(null);
                saleinvoice.AttributeSetListNew = db.GetAttributeSets();

                for (int i = 0; i < saleinvoice.AttributeSetListNew.Count; i++)
                {
                    int Id = saleinvoice.AttributeSetListNew[i].AttributeSetId;
                    saleinvoice.AttributeSetListNew[i].AttributeValueList = DropdownManager.GetAttributeValueList(Id);
                }
            }
            catch(Exception ex)
            {
                saledb.RegisterException(db, ex);
            }

            return saleinvoice;
        }

        public void SaveSaleInvoice(SaleInvoice saleInvoice)
        {
            saleInvoice.SaleInvoiceItemList = null;
            try
            {              
                if (saleInvoice.SaleInvoiceId > 0)
                {                  
                    db.Entry(saleInvoice).State = EntityState.Modified;
                    saledb.SaveChanges(db);
                }
                else
                {
                    if(saleInvoice.SaleOrderId != 0)
                    {
                        SalesOrder saleorder = db.SalesOrders.Where(x => x.SalesOrderId == saleInvoice.SaleOrderId).FirstOrDefault();
                        saleorder.OrderStatus = Enums.PurchaseOrderStatus.Closed;
                        db.SaveSalesOrder(saleorder);                        
                    }
                    TimeZoneInfo timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                    DateTime dateTime = TimeZoneInfo.ConvertTime(DateTime.UtcNow, timeZoneInfo);
                    saleInvoice.Time = dateTime.ToString("hh:mm tt");
                    saleInvoice.Type = Constants.SALE_TYPE_WPS;
                    saleInvoice.CompanyId = SessionManager.GetSessionCompany().CompanyId;
                    GenerateAutoNo No = new GenerateAutoNo();
                    saleInvoice.SaleInvoiceNo = No.GenerateSaleInvoiceNo(saleInvoice.InvoicingDate);
                    db.SaleInvoices.Add(saleInvoice);
                    saledb.SaveChanges(db);

                    if (saleInvoice.SalesOrderItemId.HasValue)
                    {
                        SalesOrderItem saleOrderitem = db.SalesOrderItems.Where(x => x.SalesOrderItemId == saleInvoice.SalesOrderItemId).FirstOrDefault();
                        if (saleOrderitem != null)
                        {
                            saleOrderitem.IsSaleInvoiceCreated = true;
                            saleOrderitem.SaleInvoiceId = saleInvoice.SaleInvoiceId;
                            db.Entry(saleOrderitem).State = EntityState.Modified;
                            saledb.SaveChanges(db);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                saledb.RegisterException(db, ex);
            }
        }

        public void UpdateInventory(int SaleInvoiceId)
        {
            SaleInvoice item = db.GetSalesInvoiceById(SaleInvoiceId); 
            if (item.IsInventoryUpdate != true)
            {
                #region Reduce Products From Stock
                foreach (var item1 in item.SaleInvoiceItemList)
                {
                    int ProductId = item1.ProductId;
                    Product product = db.Products.Where(x => x.ProductId == ProductId).FirstOrDefault();
                    if (product.SellInPartial == true)
                    {
                        product.Quantity = product.Quantity - item1.ConversionValue.Value;
                        if (product.Quantity < 0)
                        {
                            product.IsSold = true;
                        }
                        db.SaveProduct(product);
                    }
                    else
                    {
                        product.IsSold = true;
                        db.SaveProduct(product);
                    }
                    if (item1.SalesmanCommision != null)
                    {
                        item.SalesManCommision = item1.SalesmanCommision;
                    }             
                }
                #endregion
               
                item.Status = Constants.SALE_INVOICE_SOLD;
                item.IsInventoryUpdate = true;
                db.SaveSaleInvoiceEdit(item);
            }            
        }

        public void SaveSaleInvoiceItem(int SaleInvoiceId)
        {
            try
            {
                List<SaleInvoiceItem> SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_WPS);
                if (SaleInvoiceItemList != null)
                {
                    foreach (var items in SaleInvoiceItemList)
                    {
                        SaleInvoiceItem Val = db.SaleInvoiceItems.Where(x => x.SaleInvoiceItemId == items.SaleInvoiceItemId).FirstOrDefault();
                        if (Val != null)
                        {

                            Val = null;
                            Val = items;
                            Val.Product = null;
                            Val.GSTList = null;
                            Val.DiscountCodeList = null;
                            db.Entry(Val).State = EntityState.Modified;
                            saledb.SaveChanges(db);
                        }
                        else
                        {
                            var Id = SaleInvoiceId;
                            items.Product = null;
                            items.GST = null;
                            items.DiscountCodeList = null;
                            items.GSTList = null;
                            items.Unit = null;
                            items.SKUMaster = null;
                            items.Status = Constants.SALE_ITEM_SOLD;
                            items.SaleInvoiceId = Id;
                            db.SaleInvoiceItems.Add(items);
                            saledb.SaveChanges(db);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                saledb.RegisterException(db, ex);
            }
            SessionManager.EmptySessionList(Constants.SALE_TYPE_WPS);
        }

        public void SaveSaleMemoItem(SaleInvoiceItem Item)
        {
            try
            {
                Item.Product = null;
                Item.GST = null;
                Item.DiscountCodeList = null;
                Item.GSTList = null;
                Item.Unit = null;
                Item.SKUMaster = null;
                db.SaleInvoiceItems.Add(Item);
                saledb.SaveChanges(db);
            }
            catch (Exception ex)
            {
                saledb.RegisterException(db, ex);
            }            
        }

        public int UpDateSaleInvoiceItem(SaleInvoiceItem model)
        {
            db.Configuration.ProxyCreationEnabled = false;
            SaleInvoiceItem item = db.SaleInvoiceItems.Where(x => x.SaleInvoiceItemId == model.SaleInvoiceItemId).FirstOrDefault();
            if (item != null)
            {
                item.Product = db.Products.Where(x => x.ProductId == item.ProductId).FirstOrDefault();
                item.SalesmanCommision = model.SalesmanCommision;
                item.Adjustment = model.Adjustment;
                item.MRP = model.MRP;
                item.Discount = model.Discount;
                item.UnitId = model.UnitId;
                item.ConversionValue = model.ConversionValue;
                item.Quantity = model.Quantity;
                item.DiscountId = model.DiscountId;
                item.GSTId = model.GSTId;
                if (item.GSTId > 0)
                {
                    item.GST = db.GetGSTMasterById(item.GSTId);
                    item.ApplicableTax = ((item.GST.CGST ?? 0) + (item.GST.IGST ?? 0) + item.GST.SGST);
                }
                else
                {
                    item.GST = new GST
                    {
                        CGST = 0,
                        SGST = 0,
                        IGST = 0
                    };
                    item.ApplicableTax = 0;
                }

                if (item.Product.SellInPartial)
                {
                    item.BasicPrice = Math.Round((item.MRP * item.ConversionValue.Value) / (1 + ((item.ApplicableTax) / 100)) * 10000) / 10000;

                }
                else
                {
                    item.BasicPrice = Math.Round(item.MRP / (1 + ((item.ApplicableTax) / 100)) * 10000) / 10000;
                }

                if (item.DiscountId.HasValue)
                {
                    Decimal Percent = db.Discounts.Where(x => x.DiscountId == item.DiscountId.Value).Select(x => x.DiscountPercent.Value).FirstOrDefault();
                    item.Discount = Math.Round((item.BasicPrice.Value * (Percent / 100)) * 10000) / 10000;
                }
                else
                {
                    item.Discount = 0;
                }
                item.BasicPrice = item.BasicPrice - item.Discount;
                item.ApplicableTaxValue = Math.Round(((item.BasicPrice.Value) * (item.ApplicableTax / 100)) * 10000) / 10000;
                item.CGST = Math.Round(((item.BasicPrice.Value) * (item.GST.CGST ?? 0) / 100) * 10000) / 10000;
                item.SGST = Math.Round(((item.BasicPrice.Value) * (item.GST.SGST) / 100) * 10000) / 10000;
                item.IGST = Math.Round(((item.BasicPrice.Value) * (item.GST.IGST ?? 0) / 100) * 10000) / 10000;
                item.SubTotal = Math.Round(((item.BasicPrice.Value + item.Adjustment) + item.ApplicableTaxValue) * 10000) / 10000;
                db.Entry(item).State = EntityState.Modified;
                saledb.SaveChanges(db);
            }
            return item.SaleInvoiceId ?? 0;
        }



        public List<SaleInvoiceItem> GetSaleInvoiceItems(int? SaleInvoiceId)
        {
            List<SaleInvoiceItem> SaleInvoiceItem = null;
            try
            {
                SaleInvoiceItem = db.SaleInvoiceItems.Include(x=>x.Product.SKUMaster).Where(x => x.SaleInvoiceId == SaleInvoiceId).OrderByDescending(x => x.SaleInvoiceId).ToList();
                if (SaleInvoiceItem == null)
                {
                    SaleInvoiceItem = new List<SaleInvoiceItem>();
                }
            }
            catch (Exception ex)
            {
                saledb.RegisterException(db, ex);
            }
            return SaleInvoiceItem;
        }

        public bool RemoveSalesInvoiceItem(int? SaleInvoiceId,int? SaleMemoId, int? SalesOrderId, int ProductId)
        {
            bool result = true;
            SaleInvoiceItem item;
            try
            {
                item = db.SaleInvoiceItems.Where(x => x.ProductId == ProductId && (x.SaleInvoiceId == SaleInvoiceId || x.SalesOrderId == SalesOrderId)).FirstOrDefault();
                db.SaleInvoiceItems.Remove(item);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                db.RegisterException(ex);
            }
            return result;
        }

        public void LedgerUpdate(int SaleInvoiceId, int PaymentTypeId)
        {
            SaleInvoice item = db.GetSalesInvoiceById(SaleInvoiceId);
            foreach (var item1 in item.SaleInvoiceItemList)
            {
                if (item1.SalesmanCommision != null)
                {
                    item.SalesManCommision = item1.SalesmanCommision;
                }
            }
            if(PaymentTypeId != 7 && item.CustomerId != 61)
            {
                #region Customer Voucher For Credit
                Voucher CustomerVoucherForCredit = new Voucher();
                CustomerVoucherForCredit.LedgerId = item.CustomerId;
                CustomerVoucherForCredit.SaleInvoiceId = item.SaleInvoiceId;
                CustomerVoucherForCredit.Total = item.TotalWithAdjustment;
                CustomerVoucherForCredit.Type = Constants.CR;
                CustomerVoucherForCredit.InvoiceNumber = item.SaleInvoiceNo;
                CustomerVoucherForCredit.EntryDate = item.InvoicingDate;
                CustomerVoucherForCredit.VoucherFor = Constants.SALE_TYPE_WPS;
                CustomerVoucherForCredit.Particulars = "Sale Invoice Number :" + item.SaleInvoiceNo;
                db.SaveVauchers(CustomerVoucherForCredit);
                #endregion

                #region Customer Voucher For Debit
                Voucher CustomerVoucherForDebit = new Voucher();
                CustomerVoucherForDebit.LedgerId = item.CustomerId;
                CustomerVoucherForDebit.SaleInvoiceId = item.SaleInvoiceId;
                CustomerVoucherForDebit.Total = item.TotalWithAdjustment;
                CustomerVoucherForDebit.Type = Constants.DR;
                CustomerVoucherForDebit.InvoiceNumber = item.SaleInvoiceNo;
                CustomerVoucherForDebit.EntryDate = item.InvoicingDate;
                CustomerVoucherForDebit.VoucherFor = Constants.SALE_TYPE_WPS;
                CustomerVoucherForDebit.Particulars = "Sale Invoice Number :" + item.SaleInvoiceNo;
                db.SaveVauchers(CustomerVoucherForDebit);
                #endregion
            }
            else if(item.CustomerId != 61)
            {
                #region Customer Voucher For Debit
                Voucher CustomerVoucherForDebit = new Voucher();
                CustomerVoucherForDebit.LedgerId = item.CustomerId;
                CustomerVoucherForDebit.SaleInvoiceId = item.SaleInvoiceId;
                CustomerVoucherForDebit.Total = item.TotalWithAdjustment;
                CustomerVoucherForDebit.Type = Constants.DR;
                CustomerVoucherForDebit.InvoiceNumber = item.SaleInvoiceNo;
                CustomerVoucherForDebit.EntryDate = item.InvoicingDate;
                CustomerVoucherForDebit.VoucherFor = Constants.SALE_TYPE_WPS;
                CustomerVoucherForDebit.Particulars = "Sale Invoice Number :" + item.SaleInvoiceNo;
                db.SaveVauchers(CustomerVoucherForDebit);
                #endregion
            }

            #region Ledger For PaymentType
            Ledger LedgerForPaymentType = db.Ledgers.Where(x => x.LedgerId == db.PaymentTypes.Where(y => y.PaymentTypeId == PaymentTypeId).Select(y => y.LedgerId).FirstOrDefault()).FirstOrDefault();
            if(LedgerForPaymentType != null && PaymentTypeId != 7)
            {
                
                Voucher CustomerVoucherForCredit = db.Vouchers.Where(x => x.LedgerId == LedgerForPaymentType.LedgerId && x.SaleInvoiceId == item.SaleInvoiceId && x.Type == Constants.DR).FirstOrDefault();
                if (CustomerVoucherForCredit != null)
                {
                    CustomerVoucherForCredit.Total = item.TotalWithAdjustment;
                    db.SaveVauchers(CustomerVoucherForCredit);
                }
                else
                {
                    CustomerVoucherForCredit = new Voucher();
                    CustomerVoucherForCredit.LedgerId = LedgerForPaymentType.LedgerId;
                    CustomerVoucherForCredit.SaleInvoiceId = item.SaleInvoiceId;
                    CustomerVoucherForCredit.Total = item.TotalWithAdjustment;
                    CustomerVoucherForCredit.Type = Constants.DR;
                    CustomerVoucherForCredit.InvoiceNumber = item.SaleInvoiceNo;
                    CustomerVoucherForCredit.EntryDate = item.InvoicingDate;
                    CustomerVoucherForCredit.Particulars = "Sale Invoice Number :" + item.SaleInvoiceNo;
                    CustomerVoucherForCredit.VoucherFor = Constants.SALE_TYPE_WPS;
                    db.SaveVauchers(CustomerVoucherForCredit);
                }
            }
            #endregion

            #region SaleVoucher  
            Voucher SaleVoucher = db.Vouchers.Where(x => x.LedgerId == item.SaleAccountId && x.SaleInvoiceId == item.SaleInvoiceId && x.Type == Constants.CR).FirstOrDefault();
            if (SaleVoucher != null)
            {
                SaleVoucher.Total = item.TotalWithAdjustment;
                db.SaveVauchers(SaleVoucher);
            }
            else
            {
                SaleVoucher = new Voucher();
                SaleVoucher.LedgerId = item.SaleAccountId;
                SaleVoucher.SaleInvoiceId = item.SaleInvoiceId;
                SaleVoucher.Total = item.TotalWithAdjustment;
                SaleVoucher.Type = Constants.CR;
                SaleVoucher.InvoiceNumber = item.SaleInvoiceNo;
                SaleVoucher.EntryDate = item.InvoicingDate;
                SaleVoucher.Particulars = "Sale Invoice Number :" + item.SaleInvoiceNo;
                SaleVoucher.VoucherFor = Constants.SALE_TYPE_WPS;
                db.SaveVauchers(SaleVoucher);
            }
            #endregion

            #region TaxVoucher  
            Voucher Taxvoucher = db.Vouchers.Where(x => x.LedgerId == item.SaleTypeId && x.SaleInvoiceId == item.SaleInvoiceId && x.Type == Constants.CR).FirstOrDefault();
            if (Taxvoucher != null)
            {
                Taxvoucher.SGST = Taxvoucher.IGST = Taxvoucher.CGST = 0;
                foreach (var item1 in item.SaleInvoiceItemList)
                {
                    Taxvoucher.CGST = Taxvoucher.CGST + item1.CGST;
                    Taxvoucher.IGST = Taxvoucher.IGST + item1.IGST;
                    Taxvoucher.SGST = Taxvoucher.SGST + item1.SGST;
                }
                Taxvoucher.Total = item.TotalTax;
                db.SaveVauchers(Taxvoucher);
            }
            else
            {
                Taxvoucher = new Voucher();
                Taxvoucher.SGST = Taxvoucher.IGST = Taxvoucher.CGST = 0;
                foreach (var item1 in item.SaleInvoiceItemList)
                {
                    Taxvoucher.CGST = Taxvoucher.CGST + item1.CGST;
                    Taxvoucher.IGST = Taxvoucher.IGST + item1.IGST;
                    Taxvoucher.SGST = Taxvoucher.SGST + item1.SGST;
                }
                Taxvoucher.LedgerId = item.SaleTypeId;
                Taxvoucher.SaleInvoiceId = item.SaleInvoiceId;
                Taxvoucher.Total = item.TotalTax;
                Taxvoucher.Type = Constants.CR;
                Taxvoucher.InvoiceNumber = item.SaleInvoiceNo;
                Taxvoucher.EntryDate = item.InvoicingDate;
                Taxvoucher.Particulars = "Sale Invoice Number :" + item.SaleInvoiceNo;
                Taxvoucher.VoucherFor = Constants.SALE_TYPE_WPS;
                db.SaveVauchers(Taxvoucher);
            }
            #endregion
            
            #region EmployeeCommision  
            if (item.EmployeeId.HasValue && item.EmployeeId > 0)
            {
                Voucher EmployeeVoucher = db.Vouchers.Where(x => x.LedgerId == item.EmployeeId.Value && x.SaleInvoiceId == item.SaleInvoiceId && x.Type == Constants.CR).FirstOrDefault();
                if (EmployeeVoucher != null)
                {
                    item.SalesManCommision = 0;
                    foreach (var item1 in item.SaleInvoiceItemList)
                    {
                        if (item1.SalesmanCommision.HasValue && item1.SalesmanCommision > 0)
                        {
                            item.SalesManCommision = item.SalesManCommision + (item1.BasicPrice * (item1.SalesmanCommision / 100));
                        }
                    }
                    EmployeeVoucher.Total = item.SalesManCommision.Value;
                    db.SaveVauchers(EmployeeVoucher);
                }
                else
                {
                    EmployeeVoucher = new Voucher();
                    item.SalesManCommision = 0;
                    foreach (var item1 in item.SaleInvoiceItemList)
                    {
                        if (item1.SalesmanCommision.HasValue && item1.SalesmanCommision > 0)
                        {
                            item.SalesManCommision = item.SalesManCommision + (item1.BasicPrice * (item1.SalesmanCommision / 100));
                        }
                    }
                    if (item.SalesManCommision > 0)
                    {
                        EmployeeVoucher.LedgerId = item.EmployeeId;
                        EmployeeVoucher.SaleInvoiceId = item.SaleInvoiceId;
                        EmployeeVoucher.Total = item.SalesManCommision.Value;
                        EmployeeVoucher.Type = Constants.CR;
                        EmployeeVoucher.EntryDate = item.InvoicingDate;
                        EmployeeVoucher.InvoiceNumber = item.SaleInvoiceNo;
                        EmployeeVoucher.Particulars = "Sale Invoice Number :" + item.SaleInvoiceNo;
                        EmployeeVoucher.VoucherFor = Constants.SALE_TYPE_WPS;
                        db.SaveVauchers(EmployeeVoucher);
                    }
                }
            }
            #endregion

            #region Discount Voucher
            if (item.DiscountWithAdjustment != 0)
            {
                Ledger ledger = db.Ledgers.Where(x => x.Name == Constants.LEDGER_TYPE_DISCOUNT).FirstOrDefault();
                if (ledger != null)
                {
                    Voucher DiscountVoucher = db.Vouchers.Where(x => x.LedgerId == ledger.LedgerId && x.SaleInvoiceId == item.SaleInvoiceId).FirstOrDefault();
                    if(DiscountVoucher == null)
                    {
                        DiscountVoucher = new Voucher();
                    }
                    DiscountVoucher.LedgerId = ledger.LedgerId;
                    DiscountVoucher.SaleInvoiceId = item.SaleInvoiceId;
                    DiscountVoucher.Total = Math.Abs(item.DiscountWithAdjustment ?? 0);
                    if (item.DiscountWithAdjustment > 0)
                    {
                        DiscountVoucher.Type = Constants.DR;
                    }
                    else
                    {
                        DiscountVoucher.Type = Constants.CR;
                    }
                    DiscountVoucher.EntryDate = item.InvoicingDate;
                    DiscountVoucher.InvoiceNumber = item.SaleInvoiceNo;
                    DiscountVoucher.Particulars = "Sale Invoice Number :" + item.SaleInvoiceNo;
                    DiscountVoucher.VoucherFor = Constants.SALE_TYPE_WPS;
                    db.SaveVauchers(DiscountVoucher);
                }
            }
            #endregion

            #region SaleLedger  
            Ledger SaleLedger = db.Ledgers.Where(x => x.Name == Constants.LEDGER_TYPE_SALE_RETURN_LEDGER).FirstOrDefault();
            if(SaleLedger != null)
            {
                Voucher SaleLedgerVoucher = db.Vouchers.Where(x => x.LedgerId == SaleLedger.LedgerId && x.SaleInvoiceId == item.SaleInvoiceId && x.Type == Constants.CR).FirstOrDefault();
                if (SaleLedgerVoucher != null)
                {
                    SaleLedgerVoucher.Total = item.TotalWithAdjustment;
                    db.SaveVauchers(SaleLedgerVoucher);
                }
                else
                {
                    SaleLedgerVoucher = new Voucher();
                    SaleLedgerVoucher.LedgerId = SaleLedger.LedgerId;
                    SaleLedgerVoucher.SaleInvoiceId = item.SaleInvoiceId;
                    SaleLedgerVoucher.Total = item.TotalWithAdjustment;
                    SaleLedgerVoucher.Type = Constants.CR;
                    SaleLedgerVoucher.InvoiceNumber = item.SaleInvoiceNo;
                    SaleLedgerVoucher.EntryDate = item.InvoicingDate;
                    SaleLedgerVoucher.Particulars = "Sale Invoice Number :" + item.SaleInvoiceNo;
                    SaleLedgerVoucher.VoucherFor = Constants.SALE_TYPE_WPS;
                    db.SaveVauchers(SaleLedgerVoucher);
                }
            }            
            #endregion

            //item.Status = Constants.SALE_INVOICE_SOLD;
            item.IsLedgerUpdate = true;
            db.SaveSaleInvoiceEdit(item);

        }
    }
}
