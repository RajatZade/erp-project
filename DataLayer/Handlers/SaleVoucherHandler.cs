﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Utils
{
    public class SaleVoucherHandler
    {
        SaleInvoiceDbUtil saledb = new SaleInvoiceDbUtil();
        DbConnector db = new DbConnector();
        public SaleInvoice GetSaleVoucher(int? SaleInvoiceId)
        {
            SaleInvoice saleinvoice = null;
            try
            {
                if (SaleInvoiceId.HasValue)
                {
                    saleinvoice = db.SaleInvoices.Where(x => x.SaleInvoiceId == SaleInvoiceId).FirstOrDefault();
                    if (saleinvoice == null)
                    {
                        saleinvoice = new SaleInvoice();
                    }
                    saleinvoice.ProductVoucherItems = db.ProductVouchers.Where(x => x.SaleInvoiceId == SaleInvoiceId).ToList();
                    SessionManager.EmptySessionList(Constants.SALE_TYPE_SALE_VOUCHER);
                }
                else
                {
                    saleinvoice = new SaleInvoice();

                    SaleInvoice LastSaleInvoice = db.SaleInvoices.OrderByDescending(x => x.SaleInvoiceId).Where(x => (x.Type == Constants.SALE_TYPE_SALE_INVOICE || x.Type == Constants.SALE_TYPE_SALE_VOUCHER) && x.CompanyId == db.CompanyId).FirstOrDefault();
                    if (LastSaleInvoice != null)
                    {
                        saleinvoice.BranchId = LastSaleInvoice.BranchId;
                        saleinvoice.CustomerId = LastSaleInvoice.CustomerId;
                        saleinvoice.DivisionId = LastSaleInvoice.DivisionId;
                        saleinvoice.EmployeeId = LastSaleInvoice.EmployeeId;
                        saleinvoice.LocationId = LastSaleInvoice.LocationId;
                        saleinvoice.SaleAccountId = LastSaleInvoice.SaleAccountId;
                        saleinvoice.SaleTypeId = LastSaleInvoice.SaleTypeId;
                    }
                    saleinvoice.BranchId = SessionManager.GetSessionBranch().BranchId;
                    saleinvoice.DivisionId = SessionManager.GetSessionDivision().DivisionId;
                    saleinvoice.ProductVoucherItems = SessionManager.GetProductVoucherList(Constants.SALE_TYPE_SALE_VOUCHER);
                    saleinvoice.SaleInvoiceItemList = new List<SaleInvoiceItem>();
                    saleinvoice.InvoicingDate = Convert.ToDateTime(DateTime.Now);
                    saleinvoice.Status = Constants.DRAFT;
                }

                if (saleinvoice.SaleInvoiceItemList != null)
                {
                    saleinvoice = SaleCalculator.Calculate(saleinvoice);
                }
                saleinvoice.ProductVoucher = new ProductVoucher();
                saleinvoice.ProductVoucher.GSTMasterList = DropdownManager.GetGSTList(null);
                saleinvoice.ProductVoucher.UnitMasterList = DropdownManager.GetUnitMasterList(null);
                saleinvoice.LocationList = DropdownManager.GetLocationByBranch(saleinvoice.BranchId);
                saleinvoice.SaleAccountList = DropdownManager.GetLedgerListForType(null, Constants.LEDGER_TYPE_COMPANY);
                saleinvoice.CustomerAccountList = DropdownManager.GetLedgerListForType(null, Constants.LEDGER_TYPE_USER);
                saleinvoice.SaleTypeList = DropdownManager.GetLedgerListForType(null, Constants.LEDGER_TYPE_TAX);
                saleinvoice.EmployeeAccountList = DropdownManager.GetEmployeeList(null);
                saleinvoice.PaymentTypeList = DropdownManager.GetPaymentTypeList(null);
                //saleinvoice.BankAccountsList = DropdownManager.GetBankAccountsList(true, saleinvoice.LedgerBankId ?? 0);
                saleinvoice.DivisionList = DropdownManager.GetDivisionList(null);
                saleinvoice.BranchList = DropdownManager.GetBranchList(null);
                Calculate(saleinvoice);
            }
            catch (Exception ex)
            {
                saledb.RegisterException(db, ex);
            }

            return saleinvoice;
        }

        public List<ProductVoucher> GetProductVoucherList(int? SaleInvoiceId)
        {
            List<ProductVoucher> ProductVoucherList = null;
            try
            {
                ProductVoucherList = db.ProductVouchers.Where(x => x.SaleInvoiceId == SaleInvoiceId).ToList();
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
            return ProductVoucherList;
        }

        public void SaveProductVoucher(ProductVoucher model)
        {
            try
            {
                model.Discount = null;
                model.GST = null;
                model.Unit = null;
                if (model.UnitId == 0 || model.UnitId == -1)
                {
                    model.UnitId = null;
                }
                if (model.ProductVoucherId > 0)
                {
                    db.Entry(model).State = EntityState.Modified;
                }
                else
                {
                    db.ProductVouchers.Add(model);
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        public SaleInvoice SaveSaleInvoice(SaleInvoice saleInvoice, string btn)
        {
            switch (btn)
            {
                case "Hold":
                    {
                        saleInvoice.Status = Constants.HOLD;
                        SaveSaleInvoice(saleInvoice);
                        break;
                    }
                case "Make Payment":
                    {
                        SaveSaleInvoice(saleInvoice);
                        break;
                    }
                case "Save":
                    {
                        SaveSaleInvoice(saleInvoice);
                        break;
                    }
                case "Email":
                    {
                        break;
                    }
                case "SMS":
                    {
                        break;
                    }
            }
            return saleInvoice;
        }

        public void SaveSaleInvoice(SaleInvoice saleInvoice)
        {
            saleInvoice.SaleInvoiceItemList = null;
            try
            {
                if (saleInvoice.SaleInvoiceId > 0)
                {
                    db.Entry(saleInvoice).State = EntityState.Modified;
                    saledb.SaveChanges(db);
                }
                else
                {
                    TimeZoneInfo timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                    DateTime dateTime = TimeZoneInfo.ConvertTime(DateTime.UtcNow, timeZoneInfo);
                    saleInvoice.Time = dateTime.ToString("hh:mm tt");
                    saleInvoice.Type = Constants.SALE_TYPE_SALE_VOUCHER;
                    saleInvoice.CompanyId = SessionManager.GetSessionCompany().CompanyId;
                    GenerateAutoNo No = new GenerateAutoNo();
                    saleInvoice.SaleInvoiceNo = No.GenerateSaleInvoiceNo(saleInvoice.InvoicingDate);
                    db.SaleInvoices.Add(saleInvoice);
                    saledb.SaveChanges(db);
                    List<ProductVoucher> ProductVoucherItemList = SessionManager.GetProductVoucherList(Constants.SALE_TYPE_SALE_VOUCHER);
                    if (ProductVoucherItemList != null)
                    {
                        foreach (var items in ProductVoucherItemList)
                        {
                            items.Discount = null;
                            items.GST = null;
                            items.Unit = null;
                            if (items.UnitId == 0 || items.UnitId == -1)
                            {
                                items.UnitId = null;
                            }
                            items.SaleInvoiceId = saleInvoice.SaleInvoiceId;
                            db.ProductVouchers.Add(items);
                            saledb.SaveChanges(db);
                        }
                    }
                    SessionManager.EmptySessionList(Constants.SALE_TYPE_SALE_VOUCHER);
                }
            }
            catch (Exception ex)
            {
                saledb.RegisterException(db, ex);
            }
        }

        public void LedgerUpdate(int SaleInvoiceId, int PaymentTypeId)
        {
            SaleInvoice item = db.GetSalesInvoiceById(SaleInvoiceId);
            item.ProductVoucherItems = db.ProductVouchers.Where(x => x.SaleInvoiceId == SaleInvoiceId).ToList();
            item.TotalDiscountNotMapped = item.TotalDiscount + (item.Adjustment ?? 0);
            if (PaymentTypeId != 7 && item.CustomerId != 61)
            {
                #region Customer Voucher For Credit
                Voucher CustomerVoucherForCredit = db.Vouchers.Where(x => x.LedgerId == item.CustomerId && x.SaleInvoiceId == item.SaleInvoiceId && x.Type == Constants.CR).FirstOrDefault();
                if (CustomerVoucherForCredit != null)
                {
                    CustomerVoucherForCredit.Total = item.TotalWithAdjustment;
                    db.SaveVauchers(CustomerVoucherForCredit);
                }
                else
                {
                    CustomerVoucherForCredit = new Voucher();
                    CustomerVoucherForCredit.LedgerId = item.CustomerId;
                    CustomerVoucherForCredit.SaleInvoiceId = item.SaleInvoiceId;
                    CustomerVoucherForCredit.Total = item.TotalWithAdjustment;
                    CustomerVoucherForCredit.Type = Constants.CR;
                    CustomerVoucherForCredit.InvoiceNumber = item.SaleInvoiceNo;
                    CustomerVoucherForCredit.EntryDate = item.InvoicingDate;
                    CustomerVoucherForCredit.VoucherFor = Constants.SALE_TYPE_SALE_VOUCHER;
                    CustomerVoucherForCredit.Particulars = "Sale Voucher Number :" + item.SaleInvoiceNo;
                    db.SaveVauchers(CustomerVoucherForCredit);
                }
                #endregion

                #region Customer Voucher For Debit
                Voucher CustomerVoucherForDebit = db.Vouchers.Where(x => x.LedgerId == item.CustomerId && x.SaleInvoiceId == item.SaleInvoiceId && x.Type == Constants.DR).FirstOrDefault();
                if (CustomerVoucherForDebit != null)
                {
                    CustomerVoucherForDebit.Total = item.TotalWithAdjustment;
                    db.SaveVauchers(CustomerVoucherForDebit);
                }
                else
                {
                    CustomerVoucherForDebit = new Voucher();
                    CustomerVoucherForDebit.LedgerId = item.CustomerId;
                    CustomerVoucherForDebit.SaleInvoiceId = item.SaleInvoiceId;
                    CustomerVoucherForDebit.Total = item.TotalWithAdjustment;
                    CustomerVoucherForDebit.Type = Constants.DR;
                    CustomerVoucherForDebit.InvoiceNumber = item.SaleInvoiceNo;
                    CustomerVoucherForDebit.EntryDate = item.InvoicingDate;
                    CustomerVoucherForDebit.VoucherFor = Constants.SALE_TYPE_SALE_VOUCHER;
                    CustomerVoucherForDebit.Particulars = "Sale Voucher Number :" + item.SaleInvoiceNo;
                    db.SaveVauchers(CustomerVoucherForDebit);
                }
                #endregion
            }
            else if (item.CustomerId != 61)
            {
                #region Customer Voucher For Debit
                Voucher CustomerVoucherForDebit = db.Vouchers.Where(x => x.LedgerId == item.CustomerId && x.SaleInvoiceId == item.SaleInvoiceId && x.Type == Constants.DR).FirstOrDefault();
                if (CustomerVoucherForDebit != null)
                {
                    CustomerVoucherForDebit.Total = item.TotalWithAdjustment;
                    db.SaveVauchers(CustomerVoucherForDebit);
                }
                else
                {
                    CustomerVoucherForDebit = new Voucher();
                    CustomerVoucherForDebit.LedgerId = item.CustomerId;
                    CustomerVoucherForDebit.SaleInvoiceId = item.SaleInvoiceId;
                    CustomerVoucherForDebit.Total = item.TotalWithAdjustment;
                    CustomerVoucherForDebit.Type = Constants.DR;
                    CustomerVoucherForDebit.InvoiceNumber = item.SaleInvoiceNo;
                    CustomerVoucherForDebit.EntryDate = item.InvoicingDate;
                    CustomerVoucherForDebit.VoucherFor = Constants.SALE_TYPE_SALE_VOUCHER;
                    CustomerVoucherForDebit.Particulars = "Sale Voucher Number :" + item.SaleInvoiceNo;
                    db.SaveVauchers(CustomerVoucherForDebit);
                }
                #endregion
            }

            #region Ledger For PaymentType
            Ledger LedgerForPaymentType = db.Ledgers.Where(x => x.LedgerId == db.PaymentTypes.Where(y => y.PaymentTypeId == PaymentTypeId).Select(y => y.LedgerId).FirstOrDefault()).FirstOrDefault();
            if (LedgerForPaymentType != null && PaymentTypeId != 7)
            {

                Voucher CustomerVoucherForCredit = db.Vouchers.Where(x => x.LedgerId == LedgerForPaymentType.LedgerId && x.SaleInvoiceId == item.SaleInvoiceId && x.Type == Constants.DR).FirstOrDefault();
                if (CustomerVoucherForCredit != null)
                {
                    CustomerVoucherForCredit.Total = item.TotalWithAdjustment;
                    db.SaveVauchers(CustomerVoucherForCredit);
                }
                else
                {
                    CustomerVoucherForCredit = new Voucher();
                    CustomerVoucherForCredit.LedgerId = LedgerForPaymentType.LedgerId;
                    CustomerVoucherForCredit.SaleInvoiceId = item.SaleInvoiceId;
                    CustomerVoucherForCredit.Total = item.TotalWithAdjustment;
                    CustomerVoucherForCredit.Type = Constants.DR;
                    CustomerVoucherForCredit.InvoiceNumber = item.SaleInvoiceNo;
                    CustomerVoucherForCredit.EntryDate = item.InvoicingDate;
                    CustomerVoucherForCredit.VoucherFor = Constants.SALE_TYPE_SALE_VOUCHER;
                    CustomerVoucherForCredit.Particulars = "Sale Voucher Number :" + item.SaleInvoiceNo;
                    db.SaveVauchers(CustomerVoucherForCredit);
                }
            }
            #endregion

            #region SaleVoucher  
            Voucher SaleVoucher = db.Vouchers.Where(x => x.LedgerId == item.SaleAccountId && x.SaleInvoiceId == item.SaleInvoiceId && x.Type == Constants.CR).FirstOrDefault();
            if (SaleVoucher != null)
            {
                SaleVoucher.Total = item.TotalWithAdjustment;
                db.SaveVauchers(SaleVoucher);
            }
            else
            {
                SaleVoucher = new Voucher();
                SaleVoucher.LedgerId = item.SaleAccountId;
                SaleVoucher.SaleInvoiceId = item.SaleInvoiceId;
                SaleVoucher.Total = item.TotalWithAdjustment;
                SaleVoucher.Type = Constants.CR;
                SaleVoucher.InvoiceNumber = item.SaleInvoiceNo;
                SaleVoucher.EntryDate = item.InvoicingDate;
                SaleVoucher.VoucherFor = Constants.SALE_TYPE_SALE_VOUCHER;
                SaleVoucher.Particulars = "Sale Voucher Number :" + item.SaleInvoiceNo;
                db.SaveVauchers(SaleVoucher);
            }
            #endregion

            #region TaxVoucher  
            Voucher Taxvoucher = db.Vouchers.Where(x => x.LedgerId == item.SaleTypeId && x.SaleInvoiceId == item.SaleInvoiceId && x.Type == Constants.CR).FirstOrDefault();
            if (Taxvoucher != null)
            {
                Taxvoucher.SGST = Taxvoucher.IGST = Taxvoucher.CGST = 0;
                foreach (var item1 in item.ProductVoucherItems)
                {
                    Taxvoucher.CGST = Taxvoucher.CGST + item1.CGST;
                    Taxvoucher.IGST = Taxvoucher.IGST + item1.IGST;
                    Taxvoucher.SGST = Taxvoucher.SGST + item1.SGST;
                }
                Taxvoucher.Total = item.TotalTax;
                db.SaveVauchers(Taxvoucher);
            }
            else
            {
                Taxvoucher = new Voucher();
                Taxvoucher.SGST = Taxvoucher.IGST = Taxvoucher.CGST = 0;
                foreach (var item1 in item.ProductVoucherItems)
                {
                    Taxvoucher.CGST = Taxvoucher.CGST + item1.CGST;
                    Taxvoucher.IGST = Taxvoucher.IGST + item1.IGST;
                    Taxvoucher.SGST = Taxvoucher.SGST + item1.SGST;
                }
                Taxvoucher.LedgerId = item.SaleTypeId;
                Taxvoucher.SaleInvoiceId = item.SaleInvoiceId;
                Taxvoucher.Total = item.TotalTax;
                Taxvoucher.Type = Constants.CR;
                Taxvoucher.InvoiceNumber = item.SaleInvoiceNo;
                Taxvoucher.EntryDate = item.InvoicingDate;
                Taxvoucher.VoucherFor = Constants.SALE_TYPE_SALE_VOUCHER;
                Taxvoucher.Particulars = "Sale Voucher Number :" + item.SaleInvoiceNo;
                db.SaveVauchers(Taxvoucher);
            }
            #endregion        

            #region Discount Voucher
            if (item.TotalDiscountNotMapped != 0)
            {
                Ledger ledger = db.Ledgers.Where(x => x.Name == Constants.LEDGER_TYPE_DISCOUNT).FirstOrDefault();
                if (ledger != null)
                {
                    Voucher DiscountVoucher = db.Vouchers.Where(x => x.LedgerId == ledger.LedgerId && x.SaleInvoiceId == item.SaleInvoiceId).FirstOrDefault();
                    if (DiscountVoucher == null)
                    {
                        DiscountVoucher = new Voucher();
                    }
                    DiscountVoucher.LedgerId = ledger.LedgerId;
                    DiscountVoucher.SaleInvoiceId = item.SaleInvoiceId;
                    DiscountVoucher.Total = Math.Abs(item.TotalDiscountNotMapped);
                    if (item.TotalDiscountNotMapped > 0)
                    {
                        DiscountVoucher.Type = Constants.DR;
                    }
                    else
                    {
                        DiscountVoucher.Type = Constants.CR;
                    }
                    DiscountVoucher.EntryDate = item.InvoicingDate;
                    DiscountVoucher.InvoiceNumber = item.SaleInvoiceNo;
                    DiscountVoucher.VoucherFor = Constants.SALE_TYPE_SALE_VOUCHER;
                    DiscountVoucher.Particulars = "Sale Voucher Number :" + item.SaleInvoiceNo;
                    db.SaveVauchers(DiscountVoucher);
                }
            }
            #endregion

            #region SaleLedger  
            Ledger SaleLedger = db.Ledgers.Where(x => x.Name == Constants.LEDGER_TYPE_SALE_RETURN_LEDGER).FirstOrDefault();
            if (SaleLedger != null)
            {
                Voucher SaleLedgerVoucher = db.Vouchers.Where(x => x.LedgerId == SaleLedger.LedgerId && x.SaleInvoiceId == item.SaleInvoiceId && x.Type == Constants.CR).FirstOrDefault();
                if (SaleLedgerVoucher != null)
                {
                    SaleLedgerVoucher.Total = item.TotalWithAdjustment;
                    db.SaveVauchers(SaleLedgerVoucher);
                }
                else
                {
                    SaleLedgerVoucher = new Voucher();
                    SaleLedgerVoucher.LedgerId = SaleLedger.LedgerId;
                    SaleLedgerVoucher.SaleInvoiceId = item.SaleInvoiceId;
                    SaleLedgerVoucher.Total = item.TotalWithAdjustment;
                    SaleLedgerVoucher.Type = Constants.CR;
                    SaleLedgerVoucher.InvoiceNumber = item.SaleInvoiceNo;
                    SaleLedgerVoucher.EntryDate = item.InvoicingDate;
                    SaleLedgerVoucher.Particulars = "Sale Voucher Number :" + item.SaleInvoiceNo;
                    SaleLedgerVoucher.VoucherFor = Constants.SALE_TYPE_SALE_VOUCHER;
                    db.SaveVauchers(SaleLedgerVoucher);
                }
            }
            #endregion
            item.Status = Constants.SALE_INVOICE_SOLD;
            item.IsLedgerUpdate = true;
            db.SaveSaleInvoiceEdit(item);

        }

        public SaleInvoice Calculate(SaleInvoice saleinvoice)
        {
            if (saleinvoice.ProductVoucherItems != null)
            {
                saleinvoice.Subtotal = 0;
                saleinvoice.TotalTax = 0;
                saleinvoice.TotalDiscount = 0;
                saleinvoice.TotalInvoiceValue = 0;
                Dictionary<int, GST> idVsGST = db.GetGSTMasterDictionary(saleinvoice.ProductVoucherItems.Select(x => x.GSTId.Value).ToList());
                Dictionary<int, Unit> idVsUnit = db.GetUnitMasterDictionary(saleinvoice.ProductVoucherItems.Select(x => x.UnitId ?? 0).ToList());
                foreach (var Newitem in saleinvoice.ProductVoucherItems)
                {
                    Newitem.GST = idVsGST[Newitem.GSTId.Value];
                    if (Newitem.UnitId.HasValue && Newitem.UnitId != 0 && Newitem.UnitId != -1)
                    {
                        Newitem.Unit = idVsUnit[Newitem.UnitId.Value];
                    }

                    Newitem.BasePrice = Math.Round(((Newitem.Rate  * Newitem.Quantity) / (1 + (((Newitem.GST.CGST ?? 0) + Newitem.GST.SGST + (Newitem.GST.IGST ?? 0)) / 100)) - Newitem.TotalDiscount ?? 0) * 10000) / 10000;
                    Newitem.TotalTax = Math.Round(((Newitem.BasePrice ?? 0) * ((Newitem.GST.CGST ?? 0) + Newitem.GST.SGST + (Newitem.GST.IGST ?? 0)) / 100) * 10000) / 10000; 
                    Newitem.CGST = Math.Round(((Newitem.GST.CGST ?? 0) / 100) * (Newitem.BasePrice ?? 0) * 10000) / 10000;
                    Newitem.SGST = Math.Round((Newitem.GST.SGST / 100) * (Newitem.BasePrice ?? 0) * 10000) / 10000;
                    Newitem.IGST = Math.Round(((Newitem.GST.IGST ?? 0) / 100) * (Newitem.BasePrice ?? 0) * 10000) / 10000;
                    Newitem.Total = Math.Round(((Newitem.BasePrice ?? 0) - (Newitem.TotalDiscount ?? 0) + Newitem.TotalTax) * 10000) / 10000;
                    saleinvoice.Subtotal = saleinvoice.Subtotal + Newitem.BasePrice ?? 0;
                    saleinvoice.TotalTax = saleinvoice.TotalTax + Newitem.TotalTax;
                    saleinvoice.TotalDiscount = saleinvoice.TotalDiscount + Newitem.TotalDiscount ?? 0;
                    saleinvoice.TotalInvoiceValue = saleinvoice.TotalInvoiceValue + Newitem.Total;
                }
            }
            return saleinvoice;
        }

        public string DeleteInvoice(int id)
        {
            string error = string.Empty;
            SaleInvoice saleinvoice = db.SaleInvoices.Include("SaleInvoiceItemList").Where(x => x.SaleInvoiceId == id).FirstOrDefault();
            if (saleinvoice == null)
            {
                error = MessageStore.DeleteError;
            }
            if (!RemoveSalesInvoice(saleinvoice))
            {
                error = "This invoice contains Products that have been sold. Please remove the item from the associated sale invoice and try again.";
            }
            return error;
        }

        public bool RemoveSalesInvoice(SaleInvoice salesinvoice)
        {
            bool result = true;
            if (saledb.DeleteSaleVoucher(salesinvoice))
            {
                result = true;
            }
            return result;
        }
    }
}
