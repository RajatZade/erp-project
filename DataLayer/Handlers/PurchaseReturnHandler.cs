﻿using DataLayer.Models;
using DataLayer.Utils;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Utils
{
    public class PurchaseReturnHandler
    {
        DbConnector db = new DbConnector();

        public SaleInvoice GetPurchaseReturn(int? id,string Type)
        {
            SaleInvoice saleinvoice = null;
            try
            {
                if (id.HasValue)
                {
                    saleinvoice = db.SaleInvoices.Include("SaleInvoiceItemList").Where(x => x.SaleInvoiceId == id).FirstOrDefault();
                    SessionManager.EmptySessionList(Type);
                    saleinvoice.SaleOrderList = DropdownManager.GetSaleOrderList(null, Enums.PurchaseOrderStatus.Closed);
                    saleinvoice.SaleMemoList = DropdownManager.GetDeliveryMemoList();
                }
                else
                {
                    saleinvoice = new SaleInvoice();
                    saleinvoice.BranchId = SessionManager.GetSessionBranch().BranchId;
                    saleinvoice.DivisionId = SessionManager.GetSessionDivision().DivisionId;
                    saleinvoice.SaleInvoiceItemList = new List<SaleInvoiceItem>();
                    saleinvoice.InvoicingDate = Convert.ToDateTime(DateTime.Now);
                    saleinvoice.Status = Constants.DRAFT;
                    saleinvoice.Type = Type;
                    var item = SessionManager.GetInvoiceItemList(Type);
                    if (item != null)
                    {
                        for (int i = 0; i < item.Count; i++)
                        {
                            saleinvoice.SaleInvoiceItemList.Add(item[i]);
                        }
                    }

                    saleinvoice.SaleOrderList = DropdownManager.GetSaleOrderList(null, Enums.PurchaseOrderStatus.Open);
                    saleinvoice.SaleMemoList = DropdownManager.GetDeliveryMemoList();
                    saleinvoice.CompanyId = db.CompanyId ?? 0;
                }

                if (saleinvoice.SaleInvoiceItemList != null)
                {
                    saleinvoice = SaleCalculator.Calculate(saleinvoice);
                }
                saleinvoice.LocationList = DropdownManager.GetLocationByBranch(saleinvoice.BranchId);
                saleinvoice.SaleAccountList = DropdownManager.GetLedgerListForType(null, Constants.LEDGER_TYPE_COMPANY);
                saleinvoice.CustomerAccountList = DropdownManager.GetLedgerListForType(null, Constants.LEDGER_TYPE_USER);
                saleinvoice.SaleTypeList = DropdownManager.GetLedgerListForType(null, Constants.LEDGER_TYPE_TAX);
                saleinvoice.EmployeeAccountList = DropdownManager.GetEmployeeList(null);
                saleinvoice.PaymentTypeList = DropdownManager.GetPaymentTypeList(null);
                //saleinvoice.BankAccountsList = DropdownManager.GetBankAccountsList(true, saleinvoice.LedgerBankId ?? 0);
                saleinvoice.DivisionList = DropdownManager.GetDivisionList(null);
                saleinvoice.BranchList = DropdownManager.GetBranchList(null);
                saleinvoice.PaymentTypeList = DropdownManager.GetPaymentTypeList(null);

                saleinvoice.AttributeSetListNew = db.GetAttributeSets();

                for (int i = 0; i < saleinvoice.AttributeSetListNew.Count; i++)
                {
                    int Id = saleinvoice.AttributeSetListNew[i].AttributeSetId;
                    saleinvoice.AttributeSetListNew[i].AttributeValueList = DropdownManager.GetAttributeValueList(Id);
                }

            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }

            return saleinvoice;
        }

        public SaleInvoice SaveSaleInvoice(SaleInvoice saleInvoice, string btn)
        {
            switch (btn)
            {
                case "Hold":
                    {
                        saleInvoice.Status = Constants.HOLD;
                        SaveSaleInvoice(saleInvoice);
                        SaveSaleInvoiceItem(saleInvoice.SaleInvoiceId,saleInvoice.Type);
                        break;
                    }
                case "Save & Print Invoice":
                    {
                        SaveSaleInvoice(saleInvoice);
                        SaveSaleInvoiceItem(saleInvoice.SaleInvoiceId,saleInvoice.Type);
                        UpdateInventory(saleInvoice.SaleInvoiceId);
                        if(saleInvoice.Type == Constants.SALE_TYPE_PURCHASE_RETURN)
                        {
                            LedgerUpdate(saleInvoice.SaleInvoiceId);
                        }
                        else if(saleInvoice.Type == Constants.SALE_TYPE_INTERNAL_CONSUMSSION || saleInvoice.Type == Constants.SALE_TYPE_SHORTAGES)
                        {
                            LedgerUpdateForInternalConsumssion(saleInvoice.SaleInvoiceId);
                        }
                        break;
                    }
                case "Save":
                    {
                        SaveSaleInvoice(saleInvoice);
                        SaveSaleInvoiceItem(saleInvoice.SaleInvoiceId,saleInvoice.Type);
                        UpdateInventory(saleInvoice.SaleInvoiceId);
                        if (saleInvoice.Type == Constants.SALE_TYPE_PURCHASE_RETURN)
                        {
                            LedgerUpdate(saleInvoice.SaleInvoiceId);
                        }
                        else if (saleInvoice.Type == Constants.SALE_TYPE_INTERNAL_CONSUMSSION || saleInvoice.Type == Constants.SALE_TYPE_SHORTAGES)
                        {
                            LedgerUpdateForInternalConsumssion(saleInvoice.SaleInvoiceId);
                        }
                        break;
                    }
                case "Email":
                    {
                        break;
                    }
                case "SMS":
                    {
                        break;
                    }
            }
            return saleInvoice;
        }

        public List<SaleInvoiceItem> GetSaleInvoiceItems(int? SaleInvoiceId)
        {
            List<SaleInvoiceItem> SaleInvoiceItem = null;
            try
            {
                SaleInvoiceItem = db.SaleInvoiceItems.Include(x => x.Product.SKUMaster).Where(x => x.SaleInvoiceId == SaleInvoiceId).OrderByDescending(x => x.SaleInvoiceId).ToList();
                if (SaleInvoiceItem == null)
                {
                    SaleInvoiceItem = new List<SaleInvoiceItem>();
                }
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
            return SaleInvoiceItem;
        }

        public bool RemoveSalesInvoiceItem(int? SaleInvoiceId, int? SaleMemoId, int ProductId)
        {
            bool result = true;
            SaleInvoiceItem item;
            try
            {
                item = db.SaleInvoiceItems.Where(x => x.ProductId == ProductId && (x.SaleInvoiceId == SaleInvoiceId)).FirstOrDefault();
                db.SaleInvoiceItems.Remove(item);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                db.RegisterException(ex);
            }
            return result;
        }

        public void SaveSaleInvoice(SaleInvoice saleInvoice)
        {
            saleInvoice.SaleInvoiceItemList = null;
            try
            {
                if (saleInvoice.SaleInvoiceId > 0)
                {
                    db.Entry(saleInvoice).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    TimeZoneInfo timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                    DateTime dateTime = TimeZoneInfo.ConvertTime(DateTime.UtcNow, timeZoneInfo);
                    saleInvoice.Time = dateTime.ToString("hh:mm tt");
                    GenerateAutoNo No = new GenerateAutoNo();
                    saleInvoice.SaleInvoiceNo = No.GenerateInvoiceNoForType(saleInvoice);
                    db.SaleInvoices.Add(saleInvoice);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
        }

        public void SaveSaleInvoiceItem(int SaleInvoiceId,string Type)
        {
            try
            {
                List<SaleInvoiceItem> SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Type);
                if (SaleInvoiceItemList != null)
                {
                    foreach (var items in SaleInvoiceItemList)
                    {
                        items.Product = null;
                        items.GST = null;
                        items.DiscountCodeList = null;
                        items.GSTList = null;
                        items.Unit = null;
                        items.SKUMaster = null;
                        items.Status = Constants.SALE_ITEM_SOLD;
                        items.SaleInvoiceId = SaleInvoiceId;
                        db.SaleInvoiceItems.Add(items);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
            SessionManager.EmptySessionList(Type);
        }

        public void UpdateInventory(int SaleInvoiceId)
        {
            SaleInvoice item = db.GetSalesInvoiceById(SaleInvoiceId);
            if (item.IsInventoryUpdate != true)
            {
                #region Reduce Products From Stock
                foreach (var item1 in item.SaleInvoiceItemList)
                {
                    int ProductId = item1.ProductId;
                    Product product = db.Products.Where(x => x.ProductId == ProductId).FirstOrDefault();
                    if (product.SellInPartial == true)
                    {
                        product.Quantity = product.Quantity - item1.ConversionValue.Value;
                        if (product.Quantity < 0)
                        {
                            product.IsSold = true;
                        }
                        db.SaveProduct(product);
                    }
                    else
                    {
                        product.IsSold = true;
                        db.SaveProduct(product);
                    }
                    if (item1.SalesmanCommision != null)
                    {
                        item.SalesManCommision = item1.SalesmanCommision;
                    }
                }
                #endregion

                item.Status = Constants.SALE_INVOICE_SOLD;
                item.IsInventoryUpdate = true;
                db.SaveSaleInvoiceEdit(item);
            }
        }

        public void LedgerUpdate(int SaleInvoiceId)
        {
            SaleInvoice item = db.GetSalesInvoiceById(SaleInvoiceId);
            item.TotalDiscountNotMapped = item.TotalDiscount + (item.Adjustment ?? 0);
            foreach (var item1 in item.SaleInvoiceItemList)
            {
                if (item1.SalesmanCommision != null)
                {
                    item.SalesManCommision = item1.SalesmanCommision;
                }
            }
            if (item.PaymentTypeId != 7 && item.CustomerId != 61)
            {
                #region Customer Voucher For Credit
                Voucher CustomerVoucherForCredit = db.Vouchers.Where(x => x.LedgerId == item.CustomerId && x.SaleInvoiceId == item.SaleInvoiceId && x.Type == Constants.CR).FirstOrDefault();
                if (CustomerVoucherForCredit != null)
                {
                    CustomerVoucherForCredit.Total = item.TotalWithAdjustment;
                    db.SaveVauchers(CustomerVoucherForCredit);
                }
                else
                {
                    CustomerVoucherForCredit = new Voucher();
                    CustomerVoucherForCredit.LedgerId = item.CustomerId;
                    CustomerVoucherForCredit.SaleInvoiceId = item.SaleInvoiceId;
                    CustomerVoucherForCredit.Total = item.TotalWithAdjustment;
                    CustomerVoucherForCredit.Type = Constants.CR;
                    CustomerVoucherForCredit.InvoiceNumber = item.SaleInvoiceNo;
                    CustomerVoucherForCredit.EntryDate = item.InvoicingDate;
                    CustomerVoucherForCredit.VoucherFor = Constants.SALE_TYPE_PURCHASE_RETURN;
                    CustomerVoucherForCredit.Particulars = "Purchase Return Number :" + item.SaleInvoiceNo;
                    db.SaveVauchers(CustomerVoucherForCredit);
                }
                #endregion

                #region Customer Voucher For Debit
                Voucher CustomerVoucherForDebit = db.Vouchers.Where(x => x.LedgerId == item.CustomerId && x.SaleInvoiceId == item.SaleInvoiceId && x.Type == Constants.DR).FirstOrDefault();
                if (CustomerVoucherForDebit != null)
                {
                    CustomerVoucherForDebit.Total = item.TotalWithAdjustment;
                    db.SaveVauchers(CustomerVoucherForDebit);
                }
                else
                {
                    CustomerVoucherForDebit = new Voucher();
                    CustomerVoucherForDebit.LedgerId = item.CustomerId;
                    CustomerVoucherForDebit.SaleInvoiceId = item.SaleInvoiceId;
                    CustomerVoucherForDebit.Total = item.TotalWithAdjustment;
                    CustomerVoucherForDebit.Type = Constants.DR;
                    CustomerVoucherForDebit.InvoiceNumber = item.SaleInvoiceNo;
                    CustomerVoucherForDebit.EntryDate = item.InvoicingDate;
                    CustomerVoucherForDebit.VoucherFor = Constants.SALE_TYPE_PURCHASE_RETURN;
                    CustomerVoucherForDebit.Particulars = "Purchase Return Number :" + item.SaleInvoiceNo;
                    db.SaveVauchers(CustomerVoucherForDebit);
                }
                #endregion
            }
            else if (item.CustomerId != 61)
            {
                #region Customer Voucher For Debit
                Voucher CustomerVoucherForDebit = db.Vouchers.Where(x => x.LedgerId == item.CustomerId && x.SaleInvoiceId == item.SaleInvoiceId && x.Type == Constants.DR).FirstOrDefault();
                if (CustomerVoucherForDebit != null)
                {
                    CustomerVoucherForDebit.Total = item.TotalWithAdjustment;
                    db.SaveVauchers(CustomerVoucherForDebit);
                }
                else
                {
                    CustomerVoucherForDebit = new Voucher();
                    CustomerVoucherForDebit.LedgerId = item.CustomerId;
                    CustomerVoucherForDebit.SaleInvoiceId = item.SaleInvoiceId;
                    CustomerVoucherForDebit.Total = item.TotalWithAdjustment;
                    CustomerVoucherForDebit.Type = Constants.DR;
                    CustomerVoucherForDebit.InvoiceNumber = item.SaleInvoiceNo;
                    CustomerVoucherForDebit.EntryDate = item.InvoicingDate;
                    CustomerVoucherForDebit.VoucherFor = Constants.SALE_TYPE_PURCHASE_RETURN;
                    CustomerVoucherForDebit.Particulars = "Purchase Return Number :" + item.SaleInvoiceNo;
                    db.SaveVauchers(CustomerVoucherForDebit);
                }
                #endregion
            }

            #region Ledger For PaymentType
            Ledger LedgerForPaymentType = db.Ledgers.Where(x => x.LedgerId == db.PaymentTypes.Where(y => y.PaymentTypeId == item.PaymentTypeId).Select(y => y.LedgerId).FirstOrDefault()).FirstOrDefault();
            if (LedgerForPaymentType != null && item.PaymentTypeId != 7)
            {

                Voucher CustomerVoucherForCredit = db.Vouchers.Where(x => x.LedgerId == LedgerForPaymentType.LedgerId && x.SaleInvoiceId == item.SaleInvoiceId && x.Type == Constants.DR).FirstOrDefault();
                if (CustomerVoucherForCredit != null)
                {
                    CustomerVoucherForCredit.Total = item.TotalWithAdjustment;
                    db.SaveVauchers(CustomerVoucherForCredit);
                }
                else
                {
                    CustomerVoucherForCredit = new Voucher();
                    CustomerVoucherForCredit.LedgerId = LedgerForPaymentType.LedgerId;
                    CustomerVoucherForCredit.SaleInvoiceId = item.SaleInvoiceId;
                    CustomerVoucherForCredit.Total = item.TotalWithAdjustment;
                    CustomerVoucherForCredit.Type = Constants.DR;
                    CustomerVoucherForCredit.InvoiceNumber = item.SaleInvoiceNo;
                    CustomerVoucherForCredit.EntryDate = item.InvoicingDate;
                    CustomerVoucherForCredit.Particulars = "Purchase Return Number :" + item.SaleInvoiceNo;
                    CustomerVoucherForCredit.VoucherFor = Constants.SALE_TYPE_PURCHASE_RETURN;
                    db.SaveVauchers(CustomerVoucherForCredit);
                }
            }
            #endregion

            #region SaleVoucher  
            Voucher SaleVoucher = db.Vouchers.Where(x => x.LedgerId == item.SaleAccountId && x.SaleInvoiceId == item.SaleInvoiceId && x.Type == Constants.CR).FirstOrDefault();
            if (SaleVoucher != null)
            {
                SaleVoucher.Total = item.TotalWithAdjustment;
                db.SaveVauchers(SaleVoucher);
            }
            else
            {
                SaleVoucher = new Voucher();
                SaleVoucher.LedgerId = item.SaleAccountId;
                SaleVoucher.SaleInvoiceId = item.SaleInvoiceId;
                SaleVoucher.Total = item.TotalWithAdjustment;
                SaleVoucher.Type = Constants.CR;
                SaleVoucher.InvoiceNumber = item.SaleInvoiceNo;
                SaleVoucher.EntryDate = item.InvoicingDate;
                SaleVoucher.Particulars = "Purchase Return Number :" + item.SaleInvoiceNo;
                SaleVoucher.VoucherFor = Constants.SALE_TYPE_PURCHASE_RETURN;
                db.SaveVauchers(SaleVoucher);
            }
            #endregion

            #region TaxVoucher  
            Voucher Taxvoucher = db.Vouchers.Where(x => x.LedgerId == item.SaleTypeId && x.SaleInvoiceId == item.SaleInvoiceId && x.Type == Constants.CR).FirstOrDefault();
            if (Taxvoucher != null)
            {
                Taxvoucher.SGST = Taxvoucher.IGST = Taxvoucher.CGST = 0;
                foreach (var item1 in item.SaleInvoiceItemList)
                {
                    Taxvoucher.CGST = Taxvoucher.CGST + item1.CGST;
                    Taxvoucher.IGST = Taxvoucher.IGST + item1.IGST;
                    Taxvoucher.SGST = Taxvoucher.SGST + item1.SGST;
                }
                Taxvoucher.Total = item.TotalTax;
                db.SaveVauchers(Taxvoucher);
            }
            else
            {
                Taxvoucher = new Voucher();
                Taxvoucher.SGST = Taxvoucher.IGST = Taxvoucher.CGST = 0;
                foreach (var item1 in item.SaleInvoiceItemList)
                {
                    Taxvoucher.CGST = Taxvoucher.CGST + item1.CGST;
                    Taxvoucher.IGST = Taxvoucher.IGST + item1.IGST;
                    Taxvoucher.SGST = Taxvoucher.SGST + item1.SGST;
                }
                Taxvoucher.LedgerId = item.SaleTypeId;
                Taxvoucher.SaleInvoiceId = item.SaleInvoiceId;
                Taxvoucher.Total = item.TotalTax;
                Taxvoucher.Type = Constants.CR;
                Taxvoucher.InvoiceNumber = item.SaleInvoiceNo;
                Taxvoucher.EntryDate = item.InvoicingDate;
                Taxvoucher.Particulars = "Purchase Return Number :" + item.SaleInvoiceNo;
                Taxvoucher.VoucherFor = Constants.SALE_TYPE_PURCHASE_RETURN;
                db.SaveVauchers(Taxvoucher);
            }
            #endregion

            #region EmployeeCommision  
            if (item.EmployeeId.HasValue && item.EmployeeId > 0)
            {
                Voucher EmployeeVoucher = db.Vouchers.Where(x => x.LedgerId == item.EmployeeId.Value && x.SaleInvoiceId == item.SaleInvoiceId && x.Type == Constants.CR).FirstOrDefault();
                if (EmployeeVoucher != null)
                {
                    item.SalesManCommision = 0;
                    foreach (var item1 in item.SaleInvoiceItemList)
                    {
                        if (item1.SalesmanCommision.HasValue && item1.SalesmanCommision > 0)
                        {
                            item.SalesManCommision = item.SalesManCommision + (item1.BasicPrice * (item1.SalesmanCommision / 100));
                        }
                    }
                    EmployeeVoucher.Total = item.SalesManCommision.Value;
                    db.SaveVauchers(EmployeeVoucher);
                }
                else
                {
                    EmployeeVoucher = new Voucher();
                    item.SalesManCommision = 0;
                    foreach (var item1 in item.SaleInvoiceItemList)
                    {
                        if (item1.SalesmanCommision.HasValue && item1.SalesmanCommision > 0)
                        {
                            item.SalesManCommision = item.SalesManCommision + (item1.BasicPrice * (item1.SalesmanCommision / 100));
                        }
                    }
                    if (item.SalesManCommision > 0)
                    {
                        EmployeeVoucher.LedgerId = item.EmployeeId;
                        EmployeeVoucher.SaleInvoiceId = item.SaleInvoiceId;
                        EmployeeVoucher.Total = item.SalesManCommision.Value;
                        EmployeeVoucher.Type = Constants.CR;
                        EmployeeVoucher.EntryDate = item.InvoicingDate;
                        EmployeeVoucher.InvoiceNumber = item.SaleInvoiceNo;
                        EmployeeVoucher.Particulars = "Purchase Return Number :" + item.SaleInvoiceNo;
                        EmployeeVoucher.VoucherFor = Constants.SALE_TYPE_PURCHASE_RETURN;
                        db.SaveVauchers(EmployeeVoucher);
                    }
                }
            }
            #endregion

            #region Discount Voucher
            if (item.TotalDiscountNotMapped != 0)
            {
                Ledger ledger = db.Ledgers.Where(x => x.Name == Constants.LEDGER_TYPE_DISCOUNT).FirstOrDefault();
                if (ledger != null)
                {
                    Voucher DiscountVoucher = db.Vouchers.Where(x => x.LedgerId == ledger.LedgerId && x.SaleInvoiceId == item.SaleInvoiceId).FirstOrDefault();
                    if (DiscountVoucher == null)
                    {
                        DiscountVoucher = new Voucher();
                    }
                    DiscountVoucher.LedgerId = ledger.LedgerId;
                    DiscountVoucher.SaleInvoiceId = item.SaleInvoiceId;
                    DiscountVoucher.Total = Math.Abs(item.TotalDiscountNotMapped);
                    if (item.TotalDiscountNotMapped > 0)
                    {
                        DiscountVoucher.Type = Constants.DR;
                    }
                    else
                    {
                        DiscountVoucher.Type = Constants.CR;
                    }
                    DiscountVoucher.EntryDate = item.InvoicingDate;
                    DiscountVoucher.InvoiceNumber = item.SaleInvoiceNo;
                    DiscountVoucher.Particulars = "Purchase Return Number :" + item.SaleInvoiceNo;
                    DiscountVoucher.VoucherFor = Constants.SALE_TYPE_PURCHASE_RETURN;
                    db.SaveVauchers(DiscountVoucher);
                }
            }
            #endregion

            #region PurchaseLedger  
            Ledger PurchaseLedger = db.Ledgers.Where(x => x.Name == Constants.LEDGER_TYPE_PURCHASE_RETURN_LEDGER).FirstOrDefault();
            if (PurchaseLedger != null)
            {
                Voucher PurchaseLedgerVoucher = db.Vouchers.Where(x => x.LedgerId == PurchaseLedger.LedgerId && x.SaleInvoiceId == item.SaleInvoiceId && x.Type == Constants.DR).FirstOrDefault();
                if (PurchaseLedgerVoucher != null)
                {
                    PurchaseLedgerVoucher.Total = item.TotalWithAdjustment;
                    db.SaveVauchers(PurchaseLedgerVoucher);
                }
                else
                {
                    PurchaseLedgerVoucher = new Voucher();
                    PurchaseLedgerVoucher.LedgerId = PurchaseLedger.LedgerId;
                    PurchaseLedgerVoucher.SaleInvoiceId = item.SaleInvoiceId;
                    PurchaseLedgerVoucher.Total = item.TotalWithAdjustment;
                    PurchaseLedgerVoucher.Type = Constants.DR;
                    PurchaseLedgerVoucher.EntryDate = item.InvoicingDate;
                    PurchaseLedgerVoucher.InvoiceNumber = item.SaleInvoiceNo;
                    PurchaseLedgerVoucher.Particulars = "Purchase Return Number :" + item.SaleInvoiceNo;
                    PurchaseLedgerVoucher.VoucherFor = Constants.SALE_TYPE_PURCHASE_RETURN;
                    db.SaveVauchers(PurchaseLedgerVoucher);
                }
            }
            #endregion
            //item.Status = Constants.SALE_INVOICE_SOLD;
            item.IsLedgerUpdate = true;
            db.SaveSaleInvoiceEdit(item);

        }
        public void LedgerUpdateForInternalConsumssion(int SaleInvoiceId)
        {
            SaleInvoice item = db.GetSalesInvoiceById(SaleInvoiceId);
            if(item != null)
            {
                #region Customer Voucher For Debit
                Voucher CustomerVoucherForDebit = db.Vouchers.Where(x => x.LedgerId == item.CustomerId && x.SaleInvoiceId == item.SaleInvoiceId && x.Type == Constants.DR).FirstOrDefault();
                if (CustomerVoucherForDebit != null)
                {
                    CustomerVoucherForDebit.Total = item.TotalWithAdjustment;
                    db.SaveVauchers(CustomerVoucherForDebit);
                }
                else
                {
                    CustomerVoucherForDebit = new Voucher();
                    CustomerVoucherForDebit.LedgerId = item.CustomerId;
                    CustomerVoucherForDebit.SaleInvoiceId = item.SaleInvoiceId;
                    CustomerVoucherForDebit.Total = item.TotalWithAdjustment;
                    CustomerVoucherForDebit.Type = Constants.DR;
                    CustomerVoucherForDebit.InvoiceNumber = item.SaleInvoiceNo;
                    CustomerVoucherForDebit.EntryDate = item.InvoicingDate;
                    CustomerVoucherForDebit.VoucherFor = item.Type;
                    CustomerVoucherForDebit.Particulars = item.Type + " Number :" + item.SaleInvoiceNo;
                    db.SaveVauchers(CustomerVoucherForDebit);
                }
                #endregion
            }
            item.IsLedgerUpdate = true;
            db.SaveSaleInvoiceEdit(item);
        }



        public int UpDateSaleInvoiceItem(SaleInvoiceItem model)
        {
            db.Configuration.ProxyCreationEnabled = false;
            SaleInvoiceItem item = db.SaleInvoiceItems.Where(x => x.SaleInvoiceItemId == model.SaleInvoiceItemId).FirstOrDefault();
            if (item != null)
            {
                item.Product = db.Products.Where(x => x.ProductId == item.ProductId).FirstOrDefault();
                item.SalesmanCommision = model.SalesmanCommision;
                item.Adjustment = model.Adjustment;
                item.MRP = model.MRP;
                item.Discount = model.Discount;
                item.UnitId = model.UnitId;
                item.ConversionValue = model.ConversionValue;
                item.Quantity = model.Quantity;
                item.DiscountId = model.DiscountId;
                item.GSTId = model.GSTId;
                if (item.Product.SellInPartial)
                {
                    item.BasicPrice = item.MRP * item.ConversionValue.Value;
                }
                else
                {
                    item.BasicPrice = item.MRP;
                }
                if (item.GSTId > 0)
                {
                    item.GST = db.GetGSTMasterById(item.GSTId);
                    item.ApplicableTax = ((item.GST.CGST ?? 0) + (item.GST.IGST ?? 0) + item.GST.SGST);
                }
                else
                {
                    item.GST = new GST
                    {
                        CGST = 0,
                        SGST = 0,
                        IGST = 0
                    };
                    item.ApplicableTax = 0;
                }

                if (item.DiscountId.HasValue)
                {
                    Decimal Percent = db.Discounts.Where(x => x.DiscountId == item.DiscountId.Value).Select(x => x.DiscountPercent.Value).FirstOrDefault();
                    item.Discount = Math.Round((item.BasicPrice.Value * (Percent / 100)) * 10000) / 10000;
                }
                else
                {
                    item.Discount = 0;
                }
                item.BasicPrice = item.BasicPrice - item.Discount;
                item.ApplicableTaxValue = Math.Round(((item.BasicPrice.Value) * (item.ApplicableTax / 100)) * 10000) / 10000;
                item.CGST = Math.Round(((item.BasicPrice.Value) * (item.GST.CGST ?? 0) / 100) * 10000) / 10000;
                item.SGST = Math.Round(((item.BasicPrice.Value) * (item.GST.SGST) / 100) * 10000) / 10000;
                item.IGST = Math.Round(((item.BasicPrice.Value) * (item.GST.IGST ?? 0) / 100) * 10000) / 10000;
                item.SubTotal = Math.Round(((item.BasicPrice.Value + item.Adjustment) + item.ApplicableTaxValue) * 10000) / 10000;
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
            }
            return item.SaleInvoiceId ?? 0;
        }


        public void TempLedgerUpdate(int SaleInvoiceId)
        {
            SaleInvoice item = db.GetSalesInvoiceById(SaleInvoiceId);
            item.TotalDiscountNotMapped = item.TotalDiscount + (item.Adjustment ?? 0);


            #region PurchaseLedger  
            Ledger PurchaseLedger = db.Ledgers.Where(x => x.Name == Constants.LEDGER_TYPE_PURCHASE_RETURN_LEDGER).FirstOrDefault();
            if (PurchaseLedger != null)
            {
                Voucher PurchaseLedgerVoucher = db.Vouchers.Where(x => x.LedgerId == PurchaseLedger.LedgerId && x.SaleInvoiceId == item.SaleInvoiceId && x.Type == Constants.DR).FirstOrDefault();
                if (PurchaseLedgerVoucher != null)
                {
                    PurchaseLedgerVoucher.Total = (item.TotalInvoiceValue + (item.Adjustment ?? 0));
                    db.SaveVauchers(PurchaseLedgerVoucher);
                }
                else
                {
                    PurchaseLedgerVoucher = new Voucher();
                    PurchaseLedgerVoucher.LedgerId = PurchaseLedger.LedgerId;
                    PurchaseLedgerVoucher.SaleInvoiceId = item.SaleInvoiceId;
                    PurchaseLedgerVoucher.Total = (item.TotalInvoiceValue + (item.Adjustment ?? 0));
                    PurchaseLedgerVoucher.Type = Constants.DR;
                    PurchaseLedgerVoucher.EntryDate = item.InvoicingDate;
                    PurchaseLedgerVoucher.InvoiceNumber = item.SaleInvoiceNo;
                    PurchaseLedgerVoucher.Particulars = "Purchase Return Number :" + item.SaleInvoiceNo;
                    PurchaseLedgerVoucher.VoucherFor = Constants.SALE_TYPE_PURCHASE_RETURN;
                    db.SaveVauchers(PurchaseLedgerVoucher);
                }
            }
            #endregion

        }
    }
}
