﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Utils
{
    public class PurchaseVoucherHandler
    {
        DbConnector db = new DbConnector();

        public PurchaseInvoice GetPurchaseInvoiceForCreateEdit(int? PurchaseInvoiceId)
        {
            PurchaseInvoice purchaseinvoice = null;
            if (PurchaseInvoiceId.HasValue)
            {
                purchaseinvoice = db.PurchaseInvoices.Where(x => x.PurchaseInvoiceId == PurchaseInvoiceId).FirstOrDefault();
                if (purchaseinvoice == null)
                {
                    purchaseinvoice = new PurchaseInvoice();
                }
                purchaseinvoice.ProductVoucherItems = db.ProductVouchers.Where(x => x.PurchaseInvoiceId == PurchaseInvoiceId).ToList();
                Dictionary<int, GST> idVsGST = db.GetGSTMasterDictionary(purchaseinvoice.ProductVoucherItems.Select(x => x.GSTId ?? 0).ToList());
                foreach (var item in purchaseinvoice.ProductVoucherItems)
                {
                    if(item.GSTId.HasValue)
                    {
                        item.GST = idVsGST[item.GSTId.Value];
                    }
                }
            }
            else
            {

                purchaseinvoice = new PurchaseInvoice
                {
                    Type = Constants.PURCHASE_TYPE_PURCHASE_VOUCHER,
                    InvoicingDate = Convert.ToDateTime(DateTime.Now),
                    EntryDate = Convert.ToDateTime(DateTime.Now),
                };
                PurchaseInvoice LastPurchaseInvoice = db.PurchaseInvoices.OrderByDescending(x => x.PurchaseInvoiceId).FirstOrDefault();
                if (LastPurchaseInvoice != null)
                {
                    purchaseinvoice.PurchaseAccountId = LastPurchaseInvoice.PurchaseAccountId;
                    purchaseinvoice.AccountId = LastPurchaseInvoice.AccountId;
                    purchaseinvoice.BranchId = LastPurchaseInvoice.BranchId;
                    purchaseinvoice.PurchaseTypeId = LastPurchaseInvoice.PurchaseTypeId;
                    purchaseinvoice.ShippingId = LastPurchaseInvoice.ShippingId;
                }
                purchaseinvoice.BranchId = SessionManager.GetSessionBranch().BranchId;
                purchaseinvoice.DivisionId = SessionManager.GetSessionDivision().DivisionId;
                purchaseinvoice.ProductVoucherItems = SessionManager.GetProductVoucherList(Constants.PURCHASE_TYPE_PURCHASE_VOUCHER);
            }
            purchaseinvoice.ShippingTypeList = DropdownManager.GetShippingMasterList(null);
            purchaseinvoice.PurchaseMemoList = DropdownManager.GetPurchaseMemoList();
            purchaseinvoice.LocationList = DropdownManager.GetLocationMasterList(null);
            purchaseinvoice.PurchaseAccountList = DropdownManager.GetLedgerListForType(null, Constants.LEDGER_TYPE_COMPANY);
            purchaseinvoice.AccountList = DropdownManager.GetLedgerListForType(null, Constants.LEDGER_TYPE_USER);
            purchaseinvoice.PurchaseTypeList = DropdownManager.GetLedgerListForType(null, Constants.LEDGER_TYPE_TAX);
            purchaseinvoice.PaymentTypeList = DropdownManager.GetPaymentTypeList(null);
            purchaseinvoice.DivisionList = DropdownManager.GetDivisionList(null);
            purchaseinvoice.BranchList = DropdownManager.GetBranchList(null);
            purchaseinvoice.LocationList = DropdownManager.GetLocationByBranch(purchaseinvoice.BranchId ?? 0);
            purchaseinvoice.ProductVoucher = new ProductVoucher();
            purchaseinvoice.ProductVoucher.GSTMasterList = DropdownManager.GetGSTList(null);
            if (purchaseinvoice.ProductVoucherItems != null)
            {
                Dictionary<int, GST> idVsGST = db.GetGSTMasterDictionary(purchaseinvoice.ProductVoucherItems.Select(x => x.GSTId.Value).ToList());
                foreach (var Newitem in purchaseinvoice.ProductVoucherItems)
                {
                    purchaseinvoice.ItemSubTotal = purchaseinvoice.ItemSubTotal + (Newitem.Quantity * Newitem.Rate);
                    purchaseinvoice.NotMappedTotalTax = (purchaseinvoice.NotMappedTotalTax ?? 0) + Newitem.TotalTax;
                    Newitem.GST = idVsGST[Newitem.GSTId.Value];
                }
            }
            return purchaseinvoice;
        }

        public List<ProductVoucher> GetProductVoucherList(int? purchaseInvoiceId)
        {
            List<ProductVoucher> ProductVoucherList = null;
            try
            {
                ProductVoucherList = db.ProductVouchers.Where(x => x.PurchaseInvoiceId == purchaseInvoiceId).ToList();
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
            return ProductVoucherList;
        }

        public void SaveProductVoucher(ProductVoucher model)
        {
            try
            {
                model.Unit = null;
                model.GST = null;
                if (model.ProductVoucherId > 0)
                {
                    db.Entry(model).State = EntityState.Modified;
                }
                else
                {
                    db.ProductVouchers.Add(model);
                }
                db.SaveChanges();
            }
            catch(Exception ex)
            {

            }
        }

        public int SavePurchaseInvoice(PurchaseInvoice purchaseInvoice, string btn)
        {
            db.SavePurchaseInvoice(purchaseInvoice);
            db.SavePurchaseVoucherItem(purchaseInvoice.PurchaseInvoiceId);
            CalculatePurchaseInvoice(purchaseInvoice.PurchaseInvoiceId);
            return purchaseInvoice.PurchaseInvoiceId;
        }

        public void CalculatePurchaseInvoice(int? id)
        {
            try
            {
                PurchaseInvoice Invoice = db.PurchaseInvoices.Where(x => x.PurchaseInvoiceId == id).FirstOrDefault();
                Invoice.ProductVoucherItems = db.ProductVouchers.Where(x => x.PurchaseInvoiceId == id).ToList();

                foreach (var item in Invoice.ProductVoucherItems)
                {
                    Invoice.CGST = Invoice.CGST + item.CGST;
                    Invoice.SGST = Invoice.SGST + item.SGST;
                    Invoice.IGST = Invoice.IGST + item.IGST;
                }

                if (Invoice.IsLedgerUpdate == false)
                {
                    List<Voucher> VoucherList = db.Vouchers.Where(x => x.PurchaseInvoiceId == Invoice.PurchaseInvoiceId).ToList();
                    foreach (var item in VoucherList)
                    {
                        db.Vouchers.Remove(item);
                    }
                    db.SaveChanges();
                    if (Invoice.PaymentId != 7)
                    {
                        #region SupplierVoucherForCredit  
                        Voucher SupplierVoucherForCredit = db.Vouchers.Where(x => x.LedgerId == Invoice.AccountId && x.PurchaseInvoiceId == Invoice.PurchaseInvoiceId && x.Type == Constants.CR).FirstOrDefault();
                        if (SupplierVoucherForCredit != null)
                        {
                            SupplierVoucherForCredit.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_VOUCHER;
                            SupplierVoucherForCredit.Total = Invoice.TotalInvoiceValue ?? 0;
                            db.SaveVauchers(SupplierVoucherForCredit);
                        }
                        else
                        {
                            SupplierVoucherForCredit = new Voucher();
                            SupplierVoucherForCredit.LedgerId = Invoice.AccountId;
                            SupplierVoucherForCredit.PurchaseInvoiceId = Invoice.PurchaseInvoiceId;
                            SupplierVoucherForCredit.Total = Invoice.TotalInvoiceValue ?? 0;
                            SupplierVoucherForCredit.Type = Constants.CR;
                            SupplierVoucherForCredit.EntryDate = Invoice.EntryDate ?? DateTime.Now;
                            SupplierVoucherForCredit.InvoiceNumber = Invoice.PurchaseInvoiceNo;
                            SupplierVoucherForCredit.Particulars = "Purchase Voucher Number :" + Invoice.PurchaseInvoiceNo;
                            SupplierVoucherForCredit.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_VOUCHER;
                            db.SaveVauchers(SupplierVoucherForCredit);
                        }
                        #endregion

                        #region PurchaseAccountVoucherForDebit  
                        Voucher PurchaseAccountVoucherForDebit = db.Vouchers.Where(x => x.LedgerId == Invoice.PurchaseAccountId && x.PurchaseInvoiceId == Invoice.PurchaseInvoiceId && x.Type == Constants.DR).FirstOrDefault();
                        if (PurchaseAccountVoucherForDebit != null)
                        {
                            PurchaseAccountVoucherForDebit.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_VOUCHER;
                            PurchaseAccountVoucherForDebit.Total = Invoice.TotalInvoiceValue ?? 0;
                            db.SaveVauchers(PurchaseAccountVoucherForDebit);
                        }
                        else
                        {
                            PurchaseAccountVoucherForDebit = new Voucher();
                            PurchaseAccountVoucherForDebit.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_VOUCHER;
                            PurchaseAccountVoucherForDebit.LedgerId = Invoice.PurchaseAccountId;
                            PurchaseAccountVoucherForDebit.PurchaseInvoiceId = Invoice.PurchaseInvoiceId;
                            PurchaseAccountVoucherForDebit.Total = Invoice.TotalInvoiceValue ?? 0;
                            PurchaseAccountVoucherForDebit.Type = Constants.DR;
                            PurchaseAccountVoucherForDebit.EntryDate = Invoice.EntryDate ?? DateTime.Now;
                            PurchaseAccountVoucherForDebit.InvoiceNumber = Invoice.PurchaseInvoiceNo;
                            PurchaseAccountVoucherForDebit.Particulars = "Purchase Voucher Number :" + Invoice.PurchaseInvoiceNo;
                            db.SaveVauchers(PurchaseAccountVoucherForDebit);
                        }
                        #endregion

                        #region Supplier Voucher For Debit 
                        Voucher SupplierVoucherForDebit = db.Vouchers.Where(x => x.LedgerId == Invoice.AccountId && x.PurchaseInvoiceId == Invoice.PurchaseInvoiceId && x.Type == Constants.DR).FirstOrDefault();
                        if (SupplierVoucherForDebit != null)
                        {
                            SupplierVoucherForDebit.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_VOUCHER;
                            SupplierVoucherForDebit.Total = Invoice.TotalInvoiceValue ?? 0;
                            db.SaveVauchers(SupplierVoucherForDebit);
                        }
                        else
                        {
                            SupplierVoucherForDebit = new Voucher();
                            SupplierVoucherForDebit.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_VOUCHER;
                            SupplierVoucherForDebit.LedgerId = Invoice.AccountId;
                            SupplierVoucherForDebit.PurchaseInvoiceId = Invoice.PurchaseInvoiceId;
                            SupplierVoucherForDebit.Total = Invoice.TotalInvoiceValue ?? 0;
                            SupplierVoucherForDebit.Type = Constants.DR;
                            SupplierVoucherForDebit.EntryDate = Invoice.EntryDate ?? DateTime.Now;
                            SupplierVoucherForDebit.InvoiceNumber = Invoice.PurchaseInvoiceNo;
                            SupplierVoucherForDebit.Particulars = "Purchase Voucher Number :" + Invoice.PurchaseInvoiceNo;
                            db.SaveVauchers(SupplierVoucherForDebit);
                        }
                        #endregion

                        #region Purchase Account Voucher For Credit
                        Voucher PurchaseAccountVoucherCredit = db.Vouchers.Where(x => x.LedgerId == Invoice.PurchaseAccountId && x.PurchaseInvoiceId == Invoice.PurchaseInvoiceId && x.Type == Constants.CR).FirstOrDefault();
                        if (PurchaseAccountVoucherCredit != null)
                        {
                            PurchaseAccountVoucherCredit.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_VOUCHER;
                            PurchaseAccountVoucherCredit.Total = Invoice.TotalInvoiceValue ?? 0;
                            db.SaveVauchers(PurchaseAccountVoucherCredit);
                        }
                        else
                        {
                            PurchaseAccountVoucherCredit = new Voucher();
                            PurchaseAccountVoucherCredit.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_VOUCHER;
                            PurchaseAccountVoucherCredit.EntryDate = Invoice.EntryDate ?? DateTime.Now;
                            PurchaseAccountVoucherCredit.LedgerId = Invoice.PurchaseAccountId;
                            PurchaseAccountVoucherCredit.PurchaseInvoiceId = Invoice.PurchaseInvoiceId;
                            PurchaseAccountVoucherCredit.Total = Invoice.TotalInvoiceValue ?? 0;
                            PurchaseAccountVoucherCredit.Type = Constants.CR;
                            PurchaseAccountVoucherCredit.InvoiceNumber = Invoice.PurchaseInvoiceNo;
                            PurchaseAccountVoucherCredit.Particulars = "Purchase Voucher Number :" + Invoice.PurchaseInvoiceNo;
                            db.SaveVauchers(PurchaseAccountVoucherCredit);
                        }
                        #endregion                        
                    }
                    else
                    {
                        #region Supplier Voucher For Debit 
                        Voucher SupplierVoucherDebit = db.Vouchers.Where(x => x.LedgerId == Invoice.AccountId && x.PurchaseInvoiceId == Invoice.PurchaseInvoiceId && x.Type == Constants.CR).FirstOrDefault();
                        if (SupplierVoucherDebit != null)
                        {
                            SupplierVoucherDebit.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_VOUCHER;
                            SupplierVoucherDebit.Total = Invoice.TotalInvoiceValue ?? 0;
                            db.SaveVauchers(SupplierVoucherDebit);
                        }
                        else
                        {
                            SupplierVoucherDebit = new Voucher();
                            SupplierVoucherDebit.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_VOUCHER;
                            SupplierVoucherDebit.LedgerId = Invoice.AccountId;
                            SupplierVoucherDebit.PurchaseInvoiceId = Invoice.PurchaseInvoiceId;
                            SupplierVoucherDebit.Total = Invoice.TotalInvoiceValue ?? 0;
                            SupplierVoucherDebit.Type = Constants.CR;
                            SupplierVoucherDebit.EntryDate = Invoice.EntryDate ?? DateTime.Now;
                            SupplierVoucherDebit.InvoiceNumber = Invoice.PurchaseInvoiceNo;
                            SupplierVoucherDebit.Particulars = "Purchase Voucher Number :" + Invoice.PurchaseInvoiceNo;
                            db.SaveVauchers(SupplierVoucherDebit);
                        }
                        #endregion
                    }

                    #region TaxtVoucher  
                    Voucher TaxtVoucher = db.Vouchers.Where(x => x.LedgerId == Invoice.PurchaseTypeId && x.PurchaseInvoiceId == Invoice.PurchaseInvoiceId).FirstOrDefault();
                    if (TaxtVoucher != null)
                    {
                        TaxtVoucher.SGST = Invoice.SGST;
                        TaxtVoucher.CGST = Invoice.CGST;
                        TaxtVoucher.IGST = Invoice.IGST;
                        TaxtVoucher.Total = Invoice.TotalTax ?? 0;
                        TaxtVoucher.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_VOUCHER;
                        db.SaveVauchers(TaxtVoucher);
                    }
                    else
                    {
                        TaxtVoucher = new Voucher();
                        TaxtVoucher.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_VOUCHER;
                        TaxtVoucher.LedgerId = Invoice.PurchaseTypeId;
                        TaxtVoucher.PurchaseInvoiceId = Invoice.PurchaseInvoiceId;
                        TaxtVoucher.SGST = Invoice.SGST;
                        TaxtVoucher.CGST = Invoice.CGST;
                        TaxtVoucher.IGST = Invoice.IGST;
                        TaxtVoucher.Total = Invoice.TotalTax ?? 0;
                        TaxtVoucher.Type = Constants.DR;
                        TaxtVoucher.EntryDate = Invoice.EntryDate ?? DateTime.Now;
                        TaxtVoucher.InvoiceNumber = Invoice.PurchaseInvoiceNo;
                        TaxtVoucher.Particulars = "Purchase Voucher Number :" + Invoice.PurchaseInvoiceNo;
                        db.SaveVauchers(TaxtVoucher);
                    }
                    #endregion
                }

                #region Discount Voucher
                if (Invoice.Adjustment.HasValue && Invoice.Adjustment != 0)
                {
                    Ledger ledger = db.Ledgers.Where(x => x.Name == Constants.LEDGER_TYPE_DISCOUNT).FirstOrDefault();
                    if (ledger != null)
                    {
                        Voucher DiscountVoucher = db.Vouchers.Where(x => x.LedgerId == ledger.LedgerId && x.PurchaseInvoiceId == Invoice.PurchaseInvoiceId).FirstOrDefault();
                        if (DiscountVoucher != null)
                        {
                            DiscountVoucher.Total = Math.Abs(Invoice.Adjustment.Value);
                            if (Invoice.Adjustment.Value > 0)
                            {
                                DiscountVoucher.Type = Constants.CR;
                            }
                            else
                            {
                                DiscountVoucher.Type = Constants.DR;
                            }
                            DiscountVoucher.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_VOUCHER;
                            db.SaveVauchers(DiscountVoucher);
                        }
                        else
                        {
                            DiscountVoucher = new Voucher();
                            DiscountVoucher.LedgerId = ledger.LedgerId;
                            DiscountVoucher.PurchaseInvoiceId = Invoice.PurchaseInvoiceId;
                            DiscountVoucher.Total = Math.Abs(Invoice.Adjustment.Value);
                            if (Invoice.Adjustment.Value > 0)
                            {
                                DiscountVoucher.Type = Constants.CR;
                            }
                            else
                            {
                                DiscountVoucher.Type = Constants.DR;
                            }
                            DiscountVoucher.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_VOUCHER;
                            DiscountVoucher.EntryDate = Invoice.EntryDate ?? DateTime.Now;
                            DiscountVoucher.InvoiceNumber = Invoice.PurchaseInvoiceNo;
                            DiscountVoucher.Particulars = "Purchase Voucher Number :" + Invoice.PurchaseInvoiceNo;
                            db.SaveVauchers(DiscountVoucher);
                        }
                    }
                }
                #endregion

                #region PurchaseLedger  
                Ledger PurchaseLedger = db.Ledgers.Where(x => x.Name == Constants.LEDGER_TYPE_PURCHASE_RETURN_LEDGER).FirstOrDefault();
                if (PurchaseLedger != null)
                {
                    Voucher PurchaseLedgerVoucher = db.Vouchers.Where(x => x.LedgerId == PurchaseLedger.LedgerId && x.PurchaseInvoiceId == Invoice.PurchaseInvoiceId && x.Type == Constants.CR).FirstOrDefault();
                    if (PurchaseLedgerVoucher != null)
                    {
                        PurchaseLedgerVoucher.Total = Invoice.TotalInvoiceValue.Value;
                        db.SaveVauchers(PurchaseLedgerVoucher);
                    }
                    else
                    {
                        PurchaseLedgerVoucher = new Voucher();
                        PurchaseLedgerVoucher.LedgerId = PurchaseLedger.LedgerId;
                        PurchaseLedgerVoucher.PurchaseInvoiceId = Invoice.PurchaseInvoiceId;
                        PurchaseLedgerVoucher.Total = Invoice.TotalInvoiceValue.Value;
                        PurchaseLedgerVoucher.Type = Constants.CR;
                        PurchaseLedgerVoucher.InvoiceNumber = Invoice.PurchaseInvoiceNo;
                        PurchaseLedgerVoucher.EntryDate = Invoice.InvoicingDate ?? DateTime.Now;
                        PurchaseLedgerVoucher.Particulars = "Purchase Voucher Number :" + Invoice.PurchaseInvoiceNo;
                        PurchaseLedgerVoucher.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_VOUCHER;
                        db.SaveVauchers(PurchaseLedgerVoucher);
                    }
                }
                #endregion

                db.Entry(Invoice).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
        }

        public bool DeletePurchaseInvoice(PurchaseInvoice purchaseInvoice)
        {
            bool result = false;

            List<int> VoucherIds = db.Vouchers.Where(x => x.PurchaseInvoiceId == purchaseInvoice.PurchaseInvoiceId).Select(x => x.VoucherId).ToList();
            List<int> ProductVoucherIds = db.ProductVouchers.Where(x => x.PurchaseInvoiceId == purchaseInvoice.PurchaseInvoiceId).Select(x => x.ProductVoucherId).ToList();

            List<Voucher> VoucherList = new List<Voucher>();
            List<ProductVoucher> ProductVoucherList = new List<ProductVoucher>();
            foreach (var item in VoucherIds)
            {
                Voucher obj = new Voucher() { VoucherId = item };
                VoucherList.Add(obj);
            }
            foreach (var item in ProductVoucherIds)
            {
                ProductVoucher obj = new ProductVoucher() { ProductVoucherId = item };
                ProductVoucherList.Add(obj);
            }

            if (DeletePurchaseInvoice(purchaseInvoice, VoucherList, ProductVoucherList))
            {
                result = true;
            }
            return result;
        }

        internal bool DeletePurchaseInvoice(PurchaseInvoice purchaseInvoice, List<Voucher> VoucherList, List<ProductVoucher> ProductVoucherList)
        {

            foreach (var item in VoucherList)
            {
                db.Vouchers.Attach(item);
                db.Vouchers.Remove(item);
            }
            foreach (var item in ProductVoucherList)
            {
                db.ProductVouchers.Attach(item);
                db.ProductVouchers.Remove(item);
            }

            try
            {
                db.SaveChanges();
                PurchaseInvoice invoice = db.PurchaseInvoices.Where(x => x.PurchaseInvoiceId == purchaseInvoice.PurchaseInvoiceId).FirstOrDefault();
                db.Entry(invoice).State = EntityState.Deleted;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
                return false;
            }
            return true;
        }
    }
}
