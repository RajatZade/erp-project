﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DataLayer.Models;
using DataLayer.Utils;
using PagedList;

namespace DataLayer.Utils
{
    public class ProductionHandler
    {
        ProductionDbUtil proddb = new ProductionDbUtil();
        DbConnector db = new DbConnector();

        public List<JobSheetItem> GetJobSheetItems(int? JobSheetId)
        {
            List<JobSheetItem> ProductionItem = null;
            try
            {
                ProductionItem = db.JobSheetItems.Where(x => x.JobSheetId == JobSheetId).OrderByDescending(x => x.JobSheetId).ToList();
                if (ProductionItem == null)
                {
                    ProductionItem = new List<JobSheetItem>();
                }
            }
            catch (Exception ex)
            {
                proddb.RegisterException(db, ex);
            }
            return ProductionItem;
        }

        public IPagedList<JobSheet> GetJobSheetList(int? PageNo)
        {
            IPagedList<JobSheet> JobSheetList = null;
            try
            {
                JobSheetList = db.JobSheets.Where(x => x.CompanyId == db.CompanyId && x.JobSheetStatus != Enums.JobSheetStatus.FurtherProcess).OrderByDescending(x => x.JobSheetId).ToList().ToPagedList(PageNo ?? 1, Constants.PageSize); 
            }
            catch (Exception ex)
            {
                proddb.RegisterException(db, ex);
            }
            return JobSheetList;
        }

        public JobSheet GetProductionForCreateEdit(int? id, Enums.JobSheetStatus? Status, int? SalesOrderItemId)
        {
            JobSheet jobsheet = null;
            if (id.HasValue)
            {
                jobsheet = db.JobSheets.Include("JobSheetItemList").Include("JobSheetServicesList").Where(x => x.JobSheetId == id).FirstOrDefault();
                jobsheet.JobSheetItemList = db.JobSheetItems.Where(x => x.JobSheetId == id).ToList();
                jobsheet.JobSheetServicesList = db.JobSheetServices.Where(x => x.JobSheetId == id).ToList();
                Dictionary<int, GST> idVsGST = db.GetGSTMasterDictionary(jobsheet.JobSheetServicesList.Select(x => x.GSTId.Value).ToList());
                Dictionary<int, ServiceType> idVsServiceType = db.GetServiceTypeMasterDictionary(jobsheet.JobSheetServicesList.Select(x => x.ServiceTypeId).ToList());
                Dictionary<int, Ledger> idVsLedger = db.GetLedgerDictionary(jobsheet.JobSheetServicesList.Select(x => x.LedgerId).ToList());
                foreach (var Newitem in jobsheet.JobSheetServicesList)
                {
                    Newitem.ServiceType = idVsServiceType[Newitem.ServiceTypeId];
                    Newitem.GST = idVsGST[Newitem.GSTId.Value];
                    Newitem.Ledger = idVsLedger[Newitem.LedgerId];
                    decimal TotalGST = (Newitem.GST.CGST ?? 0) + (Newitem.GST.IGST ?? 0) + Newitem.GST.SGST;
                    Newitem.TotalIncludeGST = (Newitem.Price.Value + ((Newitem.Price.Value * TotalGST) / 100)) * Newitem.Quantity;
                    jobsheet.TotalServiceCostNotMapped = jobsheet.TotalServiceCostNotMapped + (Newitem.TotalIncludeGST ?? 0);
                }
                jobsheet.NewMeasurmentList = db.Measurments.ToList();
                jobsheet.MeasurmentSetListForCustome = db.MeasurmentSets.Where(x => x.JobSheetId == id && x.Type == Enums.MeasurementType.Custom).ToList();
                jobsheet.MeasurmentSetListForReadymade = db.MeasurmentSets.Where(x => x.JobSheetId == id && x.Type == Enums.MeasurementType.Readymade).ToList();
                jobsheet.FutherProcessJobSheetList = db.JobSheets.Where(x => x.ParentJobSheetId == id).ToList();
                if(jobsheet.FutherProcessJobSheetList != null && jobsheet.FutherProcessJobSheetList.Count > 0)
                {
                    jobsheet.FurtherProcessTotal = 0;
                    foreach (var Jobsheetitem in jobsheet.FutherProcessJobSheetList)
                    {
                        jobsheet.FurtherProcessTotal = jobsheet.FurtherProcessTotal + Jobsheetitem.TotalProductionCost;
                    }
                }
            }
            else
            {
                jobsheet = new JobSheet
                {
                    JobDate = DateTime.Now,
                    JobSheetStatus = Status,
                    JobSheetComplete = Enums.JobSheetComplete.Open,
                    MeasurmentType = Enums.MeasurementType.Custom.ToString(),
                    JobSheetItemList = new List<JobSheetItem>(),
                    MeasurmentSet = new MeasurmentSet(),
                    NewMeasurmentList = db.Measurments.ToList(),
                    CompanyId = db.CompanyId ?? 0,
                };

                if (SalesOrderItemId.HasValue)
                {
                    SalesOrderItem SalesOrderItem = db.SalesOrderItems.Where(x => x.SalesOrderItemId == SalesOrderItemId).FirstOrDefault();
                    if(SalesOrderItem != null)
                    {
                        SalesOrder salesOrder = db.SalesOrders.Where(x => x.SalesOrderId == SalesOrderItem.SalesOrderId).FirstOrDefault();
                        if (salesOrder != null)
                        {
                            jobsheet.CustomerId = salesOrder.CustomerId;
                            jobsheet.SaleOrderId = SalesOrderItem.SalesOrderId;
                            jobsheet.SKUMasterId = SalesOrderItem.SKUMasterId;
                            jobsheet.Quantity = SalesOrderItem.Quantity;
                        }
                    }
                }

                var item = SessionManager.GetJobSheetItemList();
                if (item != null)
                {
                    for (int i = 0; i < item.Count; i++)
                    {
                        jobsheet.JobSheetItemList.Add(item[i]);
                    }
                }
            }
            
            if (jobsheet.JobSheetItemList != null)
            {
                jobsheet = SaleCalculator.CalculateProduction(jobsheet);
            }

            if(jobsheet.MeasurmentSetListForCustome != null && jobsheet.MeasurmentSetListForCustome.Count > 0)
            {
                foreach (var item1 in jobsheet.MeasurmentSetListForCustome)
                {
                    item1.MeasurementList = new List<MeasurementAccessModel>();
                    item1.Measurment = db.Measurments.Where(x => x.MeasurmentId == item1.MeasurmentId).FirstOrDefault();
                    List<string> ValueList = item1.MeasurmentValues.Split(',').ToList();
                    foreach (var value in ValueList)
                    {
                        MeasurementAccessModel AccessModel = new MeasurementAccessModel
                        {
                            MeasurementType = value.Substring(0, value.IndexOf('=')),
                            MeasurementValue = value.Substring(value.LastIndexOf('=') + 1)
                        };
                        item1.MeasurementList.Add(AccessModel);
                    }
                }

                if (jobsheet.NewMeasurmentList.Count != jobsheet.MeasurmentSetListForCustome.Count)
                {
                    int i = jobsheet.MeasurmentSetListForCustome.Count;
                    for (; i < jobsheet.NewMeasurmentList.Count; i++)
                    {
                        MeasurmentSet MS = new MeasurmentSet
                        {
                            MeasurmentId = jobsheet.NewMeasurmentList[i].MeasurmentId,
                            Measurment = jobsheet.NewMeasurmentList[i],
                            MeasurementList = new List<MeasurementAccessModel>()
                        };
                        List<string> MeasurementValueList = jobsheet.NewMeasurmentList[i].MeasurementValues.Split(',').ToList();
                        foreach (var ListItem in MeasurementValueList)
                        {
                            MeasurementAccessModel newModel = new MeasurementAccessModel
                            {
                                MeasurementType = ListItem
                            };
                            MS.MeasurementList.Add(newModel);
                        }
                        jobsheet.MeasurmentSetListForCustome.Add(MS);
                    }

                }
            }
            else
            {
                foreach (var MeasurmentItem in jobsheet.NewMeasurmentList)
                {
                    MeasurmentSet MS = new MeasurmentSet
                    {
                        MeasurmentId = MeasurmentItem.MeasurmentId,
                        Measurment = MeasurmentItem,
                        MeasurementList = new List<MeasurementAccessModel>()
                    };
                    List<string> MeasurementValueList = MeasurmentItem.MeasurementValues.Split(',').ToList();
                    foreach (var ListItem in MeasurementValueList)
                    {
                        MeasurementAccessModel newModel = new MeasurementAccessModel
                        {
                            MeasurementType = ListItem
                        };
                        MS.MeasurementList.Add(newModel);
                    }
                    jobsheet.MeasurmentSetListForCustome.Add(MS);
                }
            }

            if(jobsheet.MeasurmentSet == null)
            {
                jobsheet.MeasurmentSet = new MeasurmentSet();
            }
            jobsheet.SalesOrderItemId = SalesOrderItemId;
            jobsheet.ProductionList = DropdownManager.GetProductionList(null);
            jobsheet.InProcessStatus = DropdownManager.InProcessStatus();
            jobsheet.EmployeeList = DropdownManager.GetEmployeeList(null);
            jobsheet.CategoryList = DropdownManager.GetCategoryList(jobsheet.CategoryId);
            jobsheet.SubcategoryList = DropdownManager.GetSubCategoryList(jobsheet.SubCategoryId);
            jobsheet.SKUList = DropdownManager.GetSKUList(jobsheet.SKUMasterId ?? 0);
            jobsheet.MeasurmentList = DropdownManager.GetMeasurmentTypeList(null);
            jobsheet.CustomerList = DropdownManager.GetLedgerListForType(null, Constants.LEDGER_TYPE_USER);
            jobsheet.ServiceTypeList = DropdownManager.ServiceTypeList(null);
            jobsheet.GSTList = DropdownManager.GetGSTList(null);
            jobsheet.SaleOrderList = DropdownManager.GetSaleOrderList(null,null);
            jobsheet.SaleInvoiceList = DropdownManager.GetSaleInvoiceList(null);
            SessionManager.EmptySessionList(Constants.JOBSHEET_SERVICE);
            SessionManager.EmptySessionList(Constants.JOBSHEET_MEASURMENTSET);
            return jobsheet;
        }


        public JobSheet GetProductionForFutherProcess(int? JobSheetId, int? ParentJobSheetId)
        {
            JobSheet jobsheet = null;
            if (JobSheetId.HasValue)
            {
                jobsheet = db.JobSheets.Include("JobSheetItemList").Include("JobSheetServicesList").Where(x => x.JobSheetId == JobSheetId).FirstOrDefault();
                Dictionary<int, GST> idVsGST = db.GetGSTMasterDictionary(jobsheet.JobSheetServicesList.Select(x => x.GSTId.Value).ToList());
                Dictionary<int, ServiceType> idVsServiceType = db.GetServiceTypeMasterDictionary(jobsheet.JobSheetServicesList.Select(x => x.ServiceTypeId).ToList());
                Dictionary<int, Ledger> idVsLedger = db.GetLedgerDictionary(jobsheet.JobSheetServicesList.Select(x => x.LedgerId).ToList());
                foreach (var Newitem in jobsheet.JobSheetServicesList)
                {
                    Newitem.ServiceType = idVsServiceType[Newitem.ServiceTypeId];
                    Newitem.GST = idVsGST[Newitem.GSTId.Value];
                    Newitem.Ledger = idVsLedger[Newitem.LedgerId];
                    decimal TotalGST = (Newitem.GST.CGST ?? 0) + (Newitem.GST.IGST ?? 0) + Newitem.GST.SGST;
                    Newitem.TotalIncludeGST = (Newitem.Price.Value + ((Newitem.Price.Value * TotalGST) / 100)) * Newitem.Quantity;
                    jobsheet.TotalServiceCostNotMapped = jobsheet.TotalServiceCostNotMapped + (Newitem.TotalIncludeGST ?? 0);
                }
                jobsheet.NewMeasurmentList = db.Measurments.ToList();
                jobsheet.MeasurmentSetListForCustome = db.MeasurmentSets.Where(x => x.JobSheetId == JobSheetId && x.Type == Enums.MeasurementType.Custom).ToList();
                jobsheet.MeasurmentSetListForReadymade = db.MeasurmentSets.Where(x => x.JobSheetId == JobSheetId && x.Type == Enums.MeasurementType.Readymade).ToList();
            }
            else
            {
                JobSheet NewJobSheet = db.JobSheets.Where(x => x.JobSheetId == ParentJobSheetId).FirstOrDefault();
                jobsheet = new JobSheet
                {
                    JobDate = DateTime.Now,
                    JobSheetStatus = Enums.JobSheetStatus.FurtherProcess,
                    SKUMasterId = NewJobSheet.SKUMasterId,
                    ParentJobSheetId = ParentJobSheetId,
                    JobSheetComplete = Enums.JobSheetComplete.Open,
                    MeasurmentType = Enums.MeasurementType.Custom.ToString(),
                    JobSheetItemList = new List<JobSheetItem>(),
                    MeasurmentSet = new MeasurmentSet(),
                    NewMeasurmentList = db.Measurments.ToList(),
                    CompanyId = db.CompanyId ?? 0,
                };
                foreach (var MeasurmentItem in jobsheet.NewMeasurmentList)
                {
                    MeasurmentSet MS = new MeasurmentSet
                    {
                        MeasurmentId = MeasurmentItem.MeasurmentId,
                        Measurment = MeasurmentItem,
                        MeasurementList = new List<MeasurementAccessModel>()
                    };
                    List<string> MeasurementValueList = MeasurmentItem.MeasurementValues.Split(',').ToList();
                    foreach (var ListItem in MeasurementValueList)
                    {
                        MeasurementAccessModel newModel = new MeasurementAccessModel
                        {
                            MeasurementType = ListItem
                        };
                        MS.MeasurementList.Add(newModel);
                    }
                    jobsheet.MeasurmentSetListForCustome.Add(MS);
                }
            }

            jobsheet.MeasurmentSetListForCustome = db.MeasurmentSets.Where(x => x.JobSheetId == (JobSheetId ?? 0) && x.Type == Enums.MeasurementType.Custom).ToList();
            if (jobsheet.MeasurmentSetListForCustome != null && jobsheet.MeasurmentSetListForCustome.Count > 0)
            {
                foreach (var item1 in jobsheet.MeasurmentSetListForCustome)
                {
                    item1.MeasurementList = new List<MeasurementAccessModel>();
                    item1.Measurment = db.Measurments.Where(x => x.MeasurmentId == item1.MeasurmentId).FirstOrDefault();
                    List<string> ValueList = item1.MeasurmentValues.Split(',').ToList();
                    foreach (var value in ValueList)
                    {
                        MeasurementAccessModel AccessModel = new MeasurementAccessModel
                        {
                            MeasurementType = value.Substring(0, value.IndexOf('=')),
                            MeasurementValue = value.Substring(value.LastIndexOf('=') + 1)
                        };
                        item1.MeasurementList.Add(AccessModel);
                    }
                }

                if (jobsheet.NewMeasurmentList.Count != jobsheet.MeasurmentSetListForCustome.Count)
                {
                    int i = jobsheet.MeasurmentSetListForCustome.Count;
                    for (; i < jobsheet.NewMeasurmentList.Count; i++)
                    {
                        MeasurmentSet MS = new MeasurmentSet
                        {
                            MeasurmentId = jobsheet.NewMeasurmentList[i].MeasurmentId,
                            Measurment = jobsheet.NewMeasurmentList[i],
                            MeasurementList = new List<MeasurementAccessModel>()
                        };
                        List<string> MeasurementValueList = jobsheet.NewMeasurmentList[i].MeasurementValues.Split(',').ToList();
                        foreach (var ListItem in MeasurementValueList)
                        {
                            MeasurementAccessModel newModel = new MeasurementAccessModel
                            {
                                MeasurementType = ListItem
                            };
                            MS.MeasurementList.Add(newModel);
                        }
                        jobsheet.MeasurmentSetListForCustome.Add(MS);
                    }

                }
            }
            else
            {
                foreach (var MeasurmentItem in jobsheet.NewMeasurmentList)
                {
                    MeasurmentSet MS = new MeasurmentSet
                    {
                        MeasurmentId = MeasurmentItem.MeasurmentId,
                        Measurment = MeasurmentItem,
                        MeasurementList = new List<MeasurementAccessModel>()
                    };
                    List<string> MeasurementValueList = MeasurmentItem.MeasurementValues.Split(',').ToList();
                    foreach (var ListItem in MeasurementValueList)
                    {
                        MeasurementAccessModel newModel = new MeasurementAccessModel
                        {
                            MeasurementType = ListItem
                        };
                        MS.MeasurementList.Add(newModel);
                    }
                    jobsheet.MeasurmentSetListForCustome.Add(MS);
                }
            }

            if (jobsheet.MeasurmentSet != null)
            {
                jobsheet.MeasurmentSet = new MeasurmentSet();
            }

            var item = SessionManager.GetJobSheetItemList();
            if (item != null)
            {
                for (int i = 0; i < item.Count; i++)
                {
                    jobsheet.JobSheetItemList.Add(item[i]);
                }
            }
            if (jobsheet.JobSheetItemList != null)
            {
                jobsheet = SaleCalculator.CalculateProduction(jobsheet);
            }
            jobsheet.ProductionList = DropdownManager.GetProductionList(null);
            jobsheet.InProcessStatus = DropdownManager.InProcessStatus();
            jobsheet.EmployeeList = DropdownManager.GetEmployeeList(null);
            jobsheet.CategoryList = DropdownManager.GetCategoryList(jobsheet.CategoryId);
            jobsheet.SubcategoryList = DropdownManager.GetSubCategoryList(jobsheet.SubCategoryId);
            jobsheet.SKUList = DropdownManager.GetSKUList(jobsheet.SKUMasterId ?? 0);
            jobsheet.MeasurmentList = DropdownManager.GetMeasurmentTypeList(null);
            jobsheet.CustomerList = DropdownManager.GetLedgerListForType(null, Constants.LEDGER_TYPE_USER);
            jobsheet.ServiceTypeList = DropdownManager.ServiceTypeList(null);
            jobsheet.GSTList = DropdownManager.GetGSTList(null);
            SessionManager.EmptySessionList(Constants.JOBSHEET_SERVICE);
            SessionManager.EmptySessionList(Constants.JOBSHEET_MEASURMENTSET);
            return jobsheet;
        }

        public bool RemoveJobSheetItem(int? JobSheetId, int ProductId)
        {
            bool result = true;
            JobSheetItem item;
            try
            {
                item = db.JobSheetItems.Where(x => x.ProductId == ProductId && x.JobSheetId == JobSheetId).FirstOrDefault();
                db.JobSheetItems.Remove(item);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                db.RegisterException(ex);
            }
            return result;
        }

        public int UpDateJobSheetItem(JobSheetItem model)
        {
            JobSheetItem item = db.JobSheetItems.Where(x => x.JobSheetItemId == model.JobSheetItemId).FirstOrDefault();
            if (item != null)
            {
                if (item != null)
                {
                    item.UnitId = model.UnitId;
                    item.ConversionValue = model.ConversionValue;
                    item.Quantity = model.Quantity;
                    if (item.ConversionValue > item.Product.Quantity)
                    {
                        item.QuantityNotAvailable = true;                        
                    }
                    else
                    {
                        item.QuantityNotAvailable = false;
                    }
                }
                db.Entry(item).State = EntityState.Modified;
                proddb.SaveChanges(db);
            }
            return item.JobSheetId.Value;
        }

        public JobSheet SaveJobSheet(JobSheet jobSheet)
        {
            proddb.SaveJobSheet(jobSheet);
            proddb.SaveMeasurment(jobSheet);
            proddb.SaveJobSheetItem(jobSheet.JobSheetId);
            proddb.SaveJobSheetService(jobSheet.JobSheetId);
            if (jobSheet.IsInventoryUpdate != true && jobSheet.JobSheetComplete == Enums.JobSheetComplete.Complete)
            {
                proddb.UpdateProducts(jobSheet.JobSheetId);
                jobSheet.IsInventoryUpdate = true;
                proddb.SaveJobSheet(jobSheet);
                jobSheet.FutherProcessJobSheetList = db.JobSheets.Where(x => x.ParentJobSheetId == jobSheet.JobSheetId).ToList();
                if (jobSheet.FutherProcessJobSheetList != null && jobSheet.FutherProcessJobSheetList.Count > 0)
                {
                    foreach (var item in jobSheet.FutherProcessJobSheetList)
                    {
                        SaveFutherProcessJobSheet(item);
                    }
                }
            }
            if(jobSheet.SalesOrderItemId.HasValue)
            {
                SalesOrderItem orderItem = db.SalesOrderItems.Where(x => x.SalesOrderItemId == jobSheet.SalesOrderItemId.Value).FirstOrDefault();
                if(orderItem != null)
                {
                    orderItem.JobSheetId = jobSheet.JobSheetId;
                    orderItem.IsJobSheetCreated = true;
                    db.SaveSaleOrderItem(orderItem);
                    jobSheet.SaleOrderId = orderItem.SalesOrderId;
                }
            }
            return jobSheet;
        }

        public JobSheet SaveFutherProcessJobSheet(JobSheet jobSheet)
        {
            proddb.SaveFutherProcessJobSheet(jobSheet);
            proddb.SaveMeasurment(jobSheet);
            proddb.SaveJobSheetItem(jobSheet.JobSheetId);
            proddb.SaveJobSheetService(jobSheet.JobSheetId);
            if (jobSheet.IsInventoryUpdate != true && jobSheet.JobSheetComplete == Enums.JobSheetComplete.Complete)
            {
                proddb.UpdateProducts(jobSheet.JobSheetId);
                jobSheet.IsInventoryUpdate = true;
                proddb.SaveJobSheet(jobSheet);
            }
            return jobSheet;
        }

        public bool DeleteJobSheet(JobSheet jobSheet)
        {
            db.Configuration.ProxyCreationEnabled = false;
            bool result = false;
            ProductionDbUtil Proddb = new ProductionDbUtil();
            List<int> productionItemIds = jobSheet.JobSheetItemList.Select(x => x.JobSheetItemId).ToList();
            List<int> ServicesIds = jobSheet.JobSheetServicesList.Select(x => x.JobSheetServiceId).ToList();
            List<int> MeasurmentSetIds = db.MeasurmentSets.Where(x => x.JobSheetId == jobSheet.JobSheetId).Select(x => x.MeasurmentSetId).ToList();
            List<int> SaleInvoiceItemIds = db.SaleInvoiceItems.Where(x => x.JobSheetId == jobSheet.JobSheetId).Select(x => x.SaleInvoiceItemId).ToList();
            List<JobSheetItem> productionItems = db.JobSheetItems.Where(x => x.JobSheetId == jobSheet.JobSheetId).ToList();
            List<int> productIds = productionItems.Select(x => x.ProductId).ToList();
            List<Product> productList = productList = db.Products.Where(x => productIds.Contains(x.ProductId)).ToList();

            //Deletion Process starts here
            List<Product> UpdateProducts = new List<Product>();
            if(jobSheet.IsInventoryUpdate)
            {
                foreach (var item in productionItems)
                {
                    Product product = productList.Where(x => x.ProductId == item.ProductId).FirstOrDefault();
                    if (product.SellInPartial)
                    {
                        product.Quantity = product.Quantity + item.ConversionValue.Value;
                    }
                    product.IsSold = false;
                    UpdateProducts.Add(product);
                }
            }

            //Attributes Delete           
            List<JobSheetService> ServiceList = db.JobSheetServices.Where(x => x.JobSheetId == jobSheet.JobSheetId).ToList();
            List<MeasurmentSet> MeasurmentSetList = new List<MeasurmentSet>();
            List<SaleInvoiceItem> SaleInvoiceItemList = new List<SaleInvoiceItem>();

            foreach (var item in productionItemIds)
            {
                JobSheetItem obj = new JobSheetItem() { JobSheetItemId = item };
                productionItems.Add(obj);
            }

            foreach (var item in MeasurmentSetIds)
            {
                MeasurmentSet obj = new MeasurmentSet() { MeasurmentSetId = item };
                MeasurmentSetList.Add(obj);
            }

            if (Proddb.DeleteProduction(productionItems, jobSheet, MeasurmentSetList, ServiceList, productList, SaleInvoiceItemList))
            {
                result = true;
            }
            return result;
        }

        public void CreateProducts(int JobsheetId,int CompanyId)
        {
            List<Product> productsToBeAdded = new List<Product>();
            PurchaseInvoiceItem item = db.PurchaseInvoiceItems.Where(x => x.JobsheetId == JobsheetId).FirstOrDefault();
            item.JobSheetNumber = db.JobSheets.Where(x => x.JobSheetId == JobsheetId).Select(x => x.ProductNumber).FirstOrDefault();
            if (item != null)
            {
                if (item.IsProductCreated != true)
                {
                    item.AttributeLinkingList = db.GetAttributeLinkingList(item.PurchaseInvoiceItemId);
                    #region Sale Without Partial
                    if (item.SellInPartial != true)
                    {
                        if (item.AttributeLinkingList != null && item.AttributeLinkingList.Count > 0)
                        {
                            int CheckQuantity = 0;
                            foreach (var AttributeItem in item.AttributeLinkingList)
                            {
                                for (int i = 0; i < AttributeItem.Quantity; i++)
                                {
                                    Product product = new Product(item)
                                    {
                                        AttributeLinkingId = AttributeItem.AttributeLinkingId,
                                        JobSheetId = JobsheetId
                                    };
                                    productsToBeAdded.Add(product);
                                }
                                CheckQuantity = CheckQuantity + Convert.ToInt32(AttributeItem.Quantity);
                            }
                            if ((item.Quantity - CheckQuantity) > 0)
                            {
                                int NewQunatity = Convert.ToInt32(item.Quantity) - CheckQuantity;
                                for (int i = 0; i < NewQunatity; i++)
                                {
                                    Product product = new Product(item)
                                    {
                                        AttributeLinkingId = null,
                                        JobSheetId = JobsheetId
                                    };
                                    productsToBeAdded.Add(product);
                                }
                            }
                        }
                        else
                        {
                            Product product = new Product(item)
                            {
                                JobSheetId = JobsheetId
                            };
                            for (int i = 0; i < item.Quantity; i++)
                            {
                                productsToBeAdded.Add(product);
                            }
                        }
                    }
                    #endregion
                    #region Sale in Partial
                    else
                    {
                        if (item.AttributeLinkingList != null && item.AttributeLinkingList.Count > 0)
                        {
                            decimal CheckQuantity = 0;
                            foreach (var AttributeItem in item.AttributeLinkingList)
                            {
                                Product product = new Product(item)
                                {
                                    Quantity = AttributeItem.Quantity
                                };
                                var AttributeLinkingId = AttributeItem.AttributeLinkingId;
                                product.AttributeLinkingId = AttributeLinkingId;
                                product.JobSheetId = JobsheetId;
                                productsToBeAdded.Add(product);
                                CheckQuantity = CheckQuantity + AttributeItem.Quantity;
                            }
                            if ((item.Quantity - CheckQuantity) > 0)
                            {
                                decimal NewQunatity = item.Quantity - CheckQuantity;
                                Product product = new Product(item)
                                {
                                    Quantity = NewQunatity,
                                    AttributeLinkingId = null,
                                    JobSheetId = JobsheetId
                                };
                                productsToBeAdded.Add(product);
                            }
                        }
                        else
                        {
                            Product product = new Product(item)
                            {
                                JobSheetId = JobsheetId
                            };
                            productsToBeAdded.Add(product);
                        }
                    }
                    #endregion
                }
                else
                {
                    var SoldItem = db.GetProductsListByInvoiceIdIsSoldTrue(item.PurchaseInvoiceItemId);
                    if (SoldItem.Count == 0)
                    {
                        ProductManager.UpdateProduct(item);
                    }
                }
            }
            if (db.InsertProducts(productsToBeAdded,CompanyId) == true)
            {
                item.IsProductCreated = true;
                db.SaveChanges();
            }
        }

        public PurchaseInvoiceItem CreateProductFromJobsheet(JobSheet JobSheet)
        {
            PurchaseInvoiceItem item = db.PurchaseInvoiceItems.Include(x => x.SKUMaster).Where(x => x.JobsheetId == JobSheet.JobSheetId).FirstOrDefault();

            if (item != null)
            {
                //item.DiscountIDTEmp = item.DiscountId;
                item.AttributeLinkingList = db.GetInvoiceItemAttribute(item.PurchaseInvoiceItemId);
                if (item.AttributeLinkingList != null)
                {
                    foreach (var i in item.AttributeLinkingList)
                    {
                        i.NameList = i.AttributeSetIds.Split(',').ToList();
                        i.ValuesList = i.Value.Split(',').ToList();
                    }
                }
            }
            else
            {
                item = new PurchaseInvoiceItem
                {
                    JobsheetId = JobSheet.JobSheetId,
                    SKUMasterId = JobSheet.SKUMasterId ?? 0,
                    DiscountsAccessible = "",
                };
            }
            item.Quantity = JobSheet.Quantity.Value;
            item.Rate = JobSheet.TotalProductionCost / JobSheet.Quantity.Value;
            item.CategoryList = DropdownManager.GetCategoryList(item.CategoryId);
            item.SubcategoryList = DropdownManager.GetSubCategoryList(item.SubCategoryId);
            item.SKUList = DropdownManager.GetSKUList(item.SKUMasterId);
            item.HSNList = DropdownManager.GetHSNList(item.HSNId);
            item.BrandList = DropdownManager.GetBrandList(item.BrandId);
            item.LocationList = DropdownManager.GetLocationByBranch(SessionManager.GetSessionBranch().BranchId);
            item.DiscountCodeList = DropdownManager.GetDiscountsAccessibleList(item.DiscountsAccessible,null);
            item.BrandList = DropdownManager.GetBrandList(item.BrandId);
            item.GSTList = DropdownManager.GetGSTList(item.GSTId);

            Dictionary<AttributeSet, List<SelectListItem>> AttributeDictionayList = null;
            item.AttributeSetListNew = db.GetAttributeSets();

            AttributeDictionayList = new Dictionary<AttributeSet, List<SelectListItem>>();
            for (int i = 0; i < item.AttributeSetListNew.Count; i++)
            {
                int Id = item.AttributeSetListNew[i].AttributeSetId;
                //item.AttributeLinkingList = db.getAttributeLinkingList(Id);
                item.AttributeSetListNew[i].AttributeValueList = DropdownManager.GetAttributeValueList(Id);
                if (AttributeDictionayList == null)
                {
                    //  AttributeDictionayList.Add(0,)

                }
                AttributeDictionayList.Add(item.AttributeSetListNew[i], item.AttributeSetListNew[i].AttributeValueList);
            }
            item.AttributeDictionayList = AttributeDictionayList;
            SessionManager.EmptySessionList(Constants.ATTRIBUTE_LIST);
            if (item.IsProductCreated)
            {
                item.UnitList = DropdownManager.GetFreezedUnitList(item.UnitId);
            }
            else
            {
                item.UnitList = DropdownManager.GetUnitMasterList(item.UnitId);
            }
            return item;
        }

        public PurchaseInvoiceItem SavePurchaseInvoiceItem(PurchaseInvoiceItem purchaseInvoiceItem)
        {
            db.SavePurchaseInvoiceItem(purchaseInvoiceItem);
            purchaseInvoiceItem.AttributeLinkingList = SessionManager.GetAttributeList();
            if (purchaseInvoiceItem.AttributeLinkingList != null)
            {
                foreach (var item in purchaseInvoiceItem.AttributeLinkingList)
                {
                    if (item.PurchaseInvoiceItemId == null || item.PurchaseInvoiceItemId == 0)
                    {
                        item.PurchaseInvoiceItemId = purchaseInvoiceItem.PurchaseInvoiceItemId;
                        item.AttributeLinkingId = 0;
                    }
                }
                db.SaveAttributeLinking(purchaseInvoiceItem.AttributeLinkingList);
                SessionManager.EmptySessionList(Constants.ATTRIBUTE_LIST);
            }
            return purchaseInvoiceItem;
        }

        public JobSheet GetProductionForAlteration(int? JobSheetId, int? SaleInvoiceId, int? SaleOrderId)
        {
            JobSheet jobsheet = null;

            if (JobSheetId.HasValue)
            {
                jobsheet = db.JobSheets.Include("JobSheetItemList").Include("JobSheetServicesList").Where(x => x.JobSheetId == (JobSheetId ?? 0)).FirstOrDefault();
                Dictionary<int, GST> idVsGST = db.GetGSTMasterDictionary(jobsheet.JobSheetServicesList.Select(x => x.GSTId.Value).ToList());
                Dictionary<int, ServiceType> idVsServiceType = db.GetServiceTypeMasterDictionary(jobsheet.JobSheetServicesList.Select(x => x.ServiceTypeId).ToList());
                Dictionary<int, Ledger> idVsLedger = db.GetLedgerDictionary(jobsheet.JobSheetServicesList.Select(x => x.LedgerId).ToList());
                foreach (var Newitem in jobsheet.JobSheetServicesList)
                {
                    Newitem.ServiceType = idVsServiceType[Newitem.ServiceTypeId];
                    Newitem.GST = idVsGST[Newitem.GSTId.Value];
                    Newitem.Ledger = idVsLedger[Newitem.LedgerId];
                    decimal TotalGST = (Newitem.GST.CGST ?? 0) + (Newitem.GST.IGST ?? 0) + Newitem.GST.SGST;
                    Newitem.TotalIncludeGST = (Newitem.Price.Value + ((Newitem.Price.Value * TotalGST) / 100)) * Newitem.Quantity;
                    jobsheet.TotalServiceCostNotMapped = jobsheet.TotalServiceCostNotMapped + (Newitem.TotalIncludeGST ?? 0);
                }
                jobsheet.NewMeasurmentList = db.Measurments.ToList();
                jobsheet.MeasurmentSetListForCustome = db.MeasurmentSets.Where(x => x.JobSheetId == JobSheetId && x.Type == Enums.MeasurementType.Custom).ToList();
                jobsheet.MeasurmentSetListForReadymade = db.MeasurmentSets.Where(x => x.JobSheetId == JobSheetId && x.Type == Enums.MeasurementType.Readymade).ToList();
                if (jobsheet.FutherProcessJobSheetList != null && jobsheet.FutherProcessJobSheetList.Count > 0)
                {
                    jobsheet.FurtherProcessTotal = 0;
                    foreach (var Jobsheetitem in jobsheet.FutherProcessJobSheetList)
                    {
                        jobsheet.FurtherProcessTotal = jobsheet.FurtherProcessTotal + Jobsheetitem.TotalProductionCost;
                    }
                }

                #region If SaleInvoiceId Has Value
                if (jobsheet.SaleInvoiceId > -1)
                {
                    jobsheet.SaleInvoice = db.SaleInvoices.Where(x => x.SaleInvoiceId == jobsheet.SaleInvoiceId).FirstOrDefault();
                    if (jobsheet.SaleInvoice != null)
                    {
                        jobsheet.SaleInvoiceItemList = db.SaleInvoiceItems.Where(x => x.JobSheetId == jobsheet.JobSheetId).ToList();
                        if (jobsheet.SaleInvoiceItemList != null && jobsheet.SaleInvoiceItemList.Count > 0)
                        {                          
                            Dictionary<int, SKUMaster> idVsSKU = db.GetSKUMasterDictionary(jobsheet.SaleInvoiceItemList.Select(x => x.SKUMasterId ?? 0).ToList());
                            Dictionary<int, GST> IdVsGST = db.GetGSTMasterDictionary(jobsheet.SaleInvoiceItemList.Select(x => x.GSTId).ToList());
                            Dictionary<int, Unit> IdVsUnit = db.GetUnitMasterDictionary(jobsheet.SaleInvoiceItemList.Select(x => x.UnitId ?? 0).ToList());
                            foreach (var Item in jobsheet.SaleInvoiceItemList)
                            {
                                Item.SKUMaster = idVsSKU[Item.SKUMasterId ?? 0];
                                Item.GST = IdVsGST[Item.GSTId];
                                Item.Unit = IdVsUnit[Item.UnitId ?? 0];
                            }
                        }
                        //jobsheet.SaleInvoice.ShippingTypeList = DropdownManager.GetShippingMasterList(null);
                        jobsheet.SaleInvoice.SaleMemoList = DropdownManager.GetDeliveryMemoList();
                        jobsheet.SaleInvoice.LocationList = DropdownManager.GetLocationMasterList(null);
                        jobsheet.SaleInvoice.SaleOrderList = DropdownManager.GetSaleOrderList(null, Enums.PurchaseOrderStatus.Closed);
                        jobsheet.SaleInvoice.SaleAccountList = DropdownManager.GetLedgerListForType(null,null);
                        jobsheet.SaleInvoice.CustomerAccountList = DropdownManager.GetLedgerListForType(null,null);
                        jobsheet.SaleInvoice.SaleTypeList = DropdownManager.GetLedgerListForType(null,null);
                        //jobsheet.SaleInvoice.ContactCategoryList = DropdownManager.GetContactCategoryList(null);
                        jobsheet.SaleInvoice.EmployeeAccountList = DropdownManager.GetEmployeeList(null);
                        jobsheet.SaleInvoice.PaymentTypeList = DropdownManager.GetPaymentTypeList(null);
                        jobsheet.SaleInvoice.DivisionList = DropdownManager.GetSessionDivisionSelectList();
                        jobsheet.SaleInvoice.BranchList = DropdownManager.GetSessionBranchSelectList();
                    }
                }
                #endregion
            }
            else
            {
                jobsheet = new JobSheet
                {
                    JobDate = DateTime.Now,
                    JobSheetStatus = Enums.JobSheetStatus.Alteration,
                    JobSheetComplete = Enums.JobSheetComplete.Open,
                    MeasurmentType = Enums.MeasurementType.Custom.ToString(),
                    JobSheetItemList = new List<JobSheetItem>(),
                    MeasurmentSet = new MeasurmentSet(),
                    NewMeasurmentList = db.Measurments.ToList()
                };

                #region If SaleInvoiceId Has Value
                if(SaleInvoiceId > -1)
                {
                    db.Configuration.ProxyCreationEnabled = false;
                    jobsheet.SaleInvoiceId = SaleInvoiceId;
                    jobsheet.SaleInvoice = db.SaleInvoices.Include("SaleInvoiceItemList").Where(x => x.SaleInvoiceId == SaleInvoiceId).FirstOrDefault();
                    if(jobsheet.SaleInvoice != null)
                    {
                        if (jobsheet.SaleInvoice.SaleInvoiceItemList != null && jobsheet.SaleInvoice.SaleInvoiceItemList.Count > 0)
                        {
                            foreach (var Item in jobsheet.SaleInvoice.SaleInvoiceItemList)
                            {
                                if(Item.Status == Constants.SALE_ITEM_SOLD)
                                {
                                    Item.SaleInvoiceId = null;
                                    Item.SaleInvoiceItemId = 0;
                                    Item.Product = db.Products.Where(x => x.ProductId == Item.ProductId).FirstOrDefault();
                                    Item.Status = Constants.SALE_INVOICE_FOR_ALTERATION;
                                    SessionManager.AddSaleInvoiceItem(Item,Constants.SALE_TYPE_SALE_INVOICE);
                                }
                            }
                            jobsheet.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_SALE_INVOICE);

                            Dictionary<int, SKUMaster> idVsSKU = db.GetSKUMasterDictionary(jobsheet.SaleInvoiceItemList.Select(x => x.SKUMasterId ?? 0).ToList());
                            Dictionary<int, GST> idVsGST = db.GetGSTMasterDictionary(jobsheet.SaleInvoiceItemList.Select(x => x.GSTId).ToList());
                            foreach (var Item in jobsheet.SaleInvoiceItemList)
                            {
                                Item.SKUMaster = idVsSKU[Item.SKUMasterId ?? 0];
                                Item.GST = idVsGST[Item.GSTId];
                            }
                        }
                        //jobsheet.SaleInvoice.ShippingTypeList = DropdownManager.GetShippingMasterList(null);
                        jobsheet.SaleInvoice.SaleMemoList = DropdownManager.GetDeliveryMemoList();
                        jobsheet.SaleInvoice.LocationList = DropdownManager.GetLocationMasterList(null);
                        jobsheet.SaleInvoice.SaleOrderList = DropdownManager.GetSaleOrderList(null, Enums.PurchaseOrderStatus.Closed);
                        jobsheet.SaleInvoice.SaleAccountList = DropdownManager.GetLedgerListForType(null, Constants.LEDGER_TYPE_COMPANY);
                        jobsheet.SaleInvoice.CustomerAccountList = DropdownManager.GetLedgerListForType(null, Constants.LEDGER_TYPE_USER);
                        jobsheet.SaleInvoice.SaleTypeList = DropdownManager.GetLedgerListForType(null, Constants.LEDGER_TYPE_TAX);
                        //jobsheet.SaleInvoice.ContactCategoryList = DropdownManager.GetContactCategoryList(null);
                        jobsheet.SaleInvoice.EmployeeAccountList = DropdownManager.GetEmployeeList(null);
                        jobsheet.SaleInvoice.PaymentTypeList = DropdownManager.GetPaymentTypeList(null);
                        jobsheet.SaleInvoice.DivisionList = DropdownManager.GetSessionDivisionSelectList();
                        jobsheet.SaleInvoice.BranchList = DropdownManager.GetSessionBranchSelectList();
                    }
                }
                #endregion
            }

            var item = SessionManager.GetJobSheetItemList();
            if (item != null)
            {
                for (int i = 0; i < item.Count; i++)
                {
                    jobsheet.JobSheetItemList.Add(item[i]);
                }
            }
            if (jobsheet.JobSheetItemList != null)
            {
                jobsheet = SaleCalculator.CalculateProduction(jobsheet);
            }

            #region Measurment region
            jobsheet.MeasurmentSetListForCustome = db.MeasurmentSets.Where(x => x.JobSheetId == (JobSheetId ?? 0) && x.Type == Enums.MeasurementType.Custom).ToList();
            if (jobsheet.MeasurmentSetListForCustome != null && jobsheet.MeasurmentSetListForCustome.Count > 0)
            {
                foreach (var item1 in jobsheet.MeasurmentSetListForCustome)
                {
                    item1.MeasurementList = new List<MeasurementAccessModel>();
                    item1.Measurment = db.Measurments.Where(x => x.MeasurmentId == item1.MeasurmentId).FirstOrDefault();
                    List<string> ValueList = item1.MeasurmentValues.Split(',').ToList();
                    foreach (var value in ValueList)
                    {
                        MeasurementAccessModel AccessModel = new MeasurementAccessModel
                        {
                            MeasurementType = value.Substring(0, value.IndexOf('=')),
                            MeasurementValue = value.Substring(value.LastIndexOf('=') + 1)
                        };
                        item1.MeasurementList.Add(AccessModel);
                    }
                }

                if (jobsheet.NewMeasurmentList.Count != jobsheet.MeasurmentSetListForCustome.Count)
                {
                    int i = jobsheet.MeasurmentSetListForCustome.Count;
                    for (; i < jobsheet.NewMeasurmentList.Count; i++)
                    {
                        MeasurmentSet MS = new MeasurmentSet
                        {
                            MeasurmentId = jobsheet.NewMeasurmentList[i].MeasurmentId,
                            Measurment = jobsheet.NewMeasurmentList[i],
                            MeasurementList = new List<MeasurementAccessModel>()
                        };
                        List<string> MeasurementValueList = jobsheet.NewMeasurmentList[i].MeasurementValues.Split(',').ToList();
                        foreach (var ListItem in MeasurementValueList)
                        {
                            MeasurementAccessModel newModel = new MeasurementAccessModel
                            {
                                MeasurementType = ListItem
                            };
                            MS.MeasurementList.Add(newModel);
                        }
                        jobsheet.MeasurmentSetListForCustome.Add(MS);
                    }

                }
            }
            else
            {
                foreach (var MeasurmentItem in jobsheet.NewMeasurmentList)
                {
                    MeasurmentSet MS = new MeasurmentSet
                    {
                        MeasurmentId = MeasurmentItem.MeasurmentId,
                        Measurment = MeasurmentItem,
                        MeasurementList = new List<MeasurementAccessModel>()
                    };
                    List<string> MeasurementValueList = MeasurmentItem.MeasurementValues.Split(',').ToList();
                    foreach (var ListItem in MeasurementValueList)
                    {
                        MeasurementAccessModel newModel = new MeasurementAccessModel
                        {
                            MeasurementType = ListItem
                        };
                        MS.MeasurementList.Add(newModel);
                    }
                    jobsheet.MeasurmentSetListForCustome.Add(MS);
                }
            }

            jobsheet.MeasurmentSet = db.MeasurmentSets.Where(x => x.JobSheetId == (JobSheetId ?? 0) && x.Type == Enums.MeasurementType.Readymade).FirstOrDefault();
            if (jobsheet.MeasurmentSet == null)
            {
                jobsheet.MeasurmentSet = new MeasurmentSet();
            }
            #endregion

            #region Dropdown List
            jobsheet.ProductionList = DropdownManager.GetProductionList(null);
            jobsheet.InProcessStatus = DropdownManager.InProcessStatus();
            jobsheet.EmployeeList = DropdownManager.GetEmployeeList(null);
            jobsheet.MeasurmentList = DropdownManager.GetMeasurmentTypeList(null);
            jobsheet.CustomerList = DropdownManager.GetLedgerListForType(null, Constants.LEDGER_TYPE_USER);
            jobsheet.ServiceTypeList = DropdownManager.ServiceTypeList(null);
            jobsheet.GSTList = DropdownManager.GetGSTList(null);
            jobsheet.SaleOrderList = DropdownManager.GetSaleOrderList(null, null);
            jobsheet.SaleInvoiceList = DropdownManager.GetSaleInvoiceList(null);
            #endregion

            SessionManager.EmptySessionList(Constants.JOBSHEET_SERVICE);
            return jobsheet;
        }

        public JobSheet SaveAlteration(JobSheet jobSheet)
        {
            proddb.SaveJobSheet(jobSheet);
            proddb.SaveMeasurment(jobSheet);
            proddb.SaveJobSheetItem(jobSheet.JobSheetId);


            proddb.SaveSaleInvoiceItemForAlteration(jobSheet.JobSheetId);
            proddb.SaveJobSheetService(jobSheet.JobSheetId);
            if (jobSheet.IsInventoryUpdate != true && jobSheet.JobSheetComplete == Enums.JobSheetComplete.Complete)
            {
                proddb.UpdateProducts(jobSheet.JobSheetId);
                jobSheet.IsInventoryUpdate = true;
                proddb.SaveJobSheet(jobSheet);
            }
            return jobSheet;
        }

        public bool RemoveSalesInvoiceItem(int? JobSheetId, int ProductId)
        {
            bool result = true;
            SaleInvoiceItem item;
            try
            {
                item = db.SaleInvoiceItems.Where(x => x.ProductId == ProductId && x.JobSheetId == JobSheetId).FirstOrDefault();
                db.SaleInvoiceItems.Remove(item);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                db.RegisterException(ex);
            }
            return result;
        }

        public decimal SaveJobSheetCost(int? JobSheetId)
        {
            JobSheet jobsheet = db.JobSheets.Include("JobSheetItemList").Include("JobSheetServicesList").Where(x => x.JobSheetId == JobSheetId).FirstOrDefault();
            Dictionary<int, GST> idVsGST = db.GetGSTMasterDictionary(jobsheet.JobSheetServicesList.Select(x => x.GSTId.Value).ToList());
            foreach (var Newitem in jobsheet.JobSheetServicesList)
            {
                Newitem.GST = idVsGST[Newitem.GSTId.Value];
                decimal TotalGST = (Newitem.GST.CGST ?? 0) + (Newitem.GST.IGST ?? 0) + Newitem.GST.SGST;
                Newitem.TotalIncludeGST = (Newitem.Price.Value + ((Newitem.Price.Value * TotalGST) / 100)) * Newitem.Quantity;
                jobsheet.TotalServiceCostNotMapped = jobsheet.TotalServiceCostNotMapped + (Newitem.TotalIncludeGST ?? 0);
            }
            foreach (var item in jobsheet.JobSheetItemList)
            {
                jobsheet.TotalProductionCostNotMapped = jobsheet.TotalProductionCostNotMapped + (item.ConversionValue.Value * item.Product.NetAmount.Value);
            }

            jobsheet.FutherProcessJobSheetList = db.JobSheets.Where(x => x.ParentJobSheetId == JobSheetId).ToList();
            if (jobsheet.FutherProcessJobSheetList != null && jobsheet.FutherProcessJobSheetList.Count > 0)
            {                
                foreach (var Jobsheetitem in jobsheet.FutherProcessJobSheetList)
                {
                    jobsheet.FurtherProcessTotal = jobsheet.FurtherProcessTotal + SaveJobSheetCost(Jobsheetitem.JobSheetId);
                }
            }
            jobsheet.TotalProductionCost = jobsheet.TotalServiceCostNotMapped + jobsheet.TotalProductionCostNotMapped + jobsheet.FurtherProcessTotal;
            //proddb.SaveJobSheetCost(jobsheet);
            return jobsheet.TotalProductionCost;
        }
    }
}
