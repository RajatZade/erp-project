﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;
using System.Data.Entity;

namespace DataLayer.Utils
{
    public class SaleExchangeHandler
    {
        SaleInvoiceDbUtil saledb = new SaleInvoiceDbUtil();
        DbConnector db = new DbConnector();

        public SaleInvoice Calculations(SaleInvoice saleInvoice)
        {
            try
            {
                Dictionary<int, SKUMaster> idVsSKU = db.GetSKUMasterDictionary(saleInvoice.SaleInvoiceItemList.Select(x => x.SKUMasterId.Value).ToList());
                Dictionary<int, GST> idVsGST = db.GetGSTMasterDictionary(saleInvoice.SaleInvoiceItemList.Select(x => x.GSTId).ToList());
                Dictionary<int, Product> idVsProduct = db.GetProductDictionary(saleInvoice.SaleInvoiceItemList.Select(x => x.ProductId).ToList());
                saleInvoice.Subtotal = 0;
                saleInvoice.TotalTax = 0;
                saleInvoice.TotalDiscount = 0;
                saleInvoice.DiscountWithAdjustment = 0;
                foreach (var item in saleInvoice.SaleInvoiceItemList)
                {
                    item.SKUMaster = idVsSKU[item.SKUMasterId.Value];
                    item.GST = idVsGST[item.GSTId];
                    item.Product = idVsProduct[item.ProductId];
                    if (item.Status == Constants.SALE_ITEM_SOLD)
                    {
                        saleInvoice.Subtotal = saleInvoice.Subtotal + item.BasicPrice.Value;
                        saleInvoice.TotalTax = saleInvoice.TotalTax + item.ApplicableTaxValue;
                        saleInvoice.TotalDiscount = (saleInvoice.TotalDiscount + item.Discount);

                        saleInvoice.SalesManCommisionNotMap = Convert.ToDecimal(saleInvoice.SalesManCommisionNotMap + (item.SalesmanCommision.HasValue ? item.SalesmanCommision.Value : 0));
                        saleInvoice.TotalCGST += item.CGST.Value;
                        saleInvoice.TotalSGST += item.SGST.Value;
                        saleInvoice.TotalIGST += item.IGST.Value;
                    }
                    else if (item.Status == Constants.SALE_ITEM_RETURN)
                    {
                        saleInvoice.Subtotal = saleInvoice.Subtotal - item.BasicPrice.Value;
                        saleInvoice.TotalTax = saleInvoice.TotalTax - item.ApplicableTaxValue;
                        saleInvoice.TotalDiscount = (saleInvoice.TotalDiscount - item.Discount);

                        saleInvoice.TotalCGST -= item.CGST.Value;
                        saleInvoice.TotalSGST -= item.SGST.Value;
                        saleInvoice.TotalIGST -= item.IGST.Value;
                    }
                    item.GSTList = DropdownManager.GetGSTList(item.GSTId);
                    item.DiscountCodeList = DropdownManager.GetDiscountsAccessibleList(item.Product.DiscountsAccessible, item.Product.ProductId);
                    if (item.Product.SellInPartial)
                    {
                        item.Unit = db.GetUnitParents(item.Product.UnitId);
                        item.UnitList = DropdownManager.GetUnitConversionList(item.Unit.UnitId);
                        foreach (var listValue in item.UnitList)
                        {
                            if (listValue.Value == item.UnitId.ToString())
                            {
                                listValue.Selected = true;
                            }
                        }
                    }
                    foreach (var listValue in item.DiscountCodeList)
                    {
                        if (listValue.Value == item.DiscountId.ToString())
                        {
                            listValue.Selected = true;
                        }
                    }
                }

                saleInvoice.Adjustment = saleInvoice.Adjustment ?? 0;
                saleInvoice.DiscountWithAdjustment = saleInvoice.TotalDiscount + saleInvoice.Adjustment;
                saleInvoice.TotalInvoiceValue = (saleInvoice.Subtotal + saleInvoice.TotalTax);
                saleInvoice.TotalWithAdjustment = saleInvoice.TotalInvoiceValue - saleInvoice.Adjustment.Value;
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
            return saleInvoice;
        }

        public void UpdateSaleInvoiceExchange(int saleInvoiceId)
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<SaleInvoiceItem> SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_SALE_EXCHANGE);
            if (SaleInvoiceItemList != null)
            {
                foreach (var item in SaleInvoiceItemList)
                {
                    if (item.Status == Constants.SALE_ITEM_RETURN)
                    {
                        item.ReturnSaleInvoiceId = saleInvoiceId;
                        db.UpdateSaleInvoiceItem(item);
                    }
                    else if (item.Status == Constants.SALE_ITEM_SOLD)
                    {
                        item.SaleInvoiceId = saleInvoiceId;
                        db.SaveSaleInvoiceItem(item);
                    }
                }
            }
            SessionManager.EmptySessionList(Constants.SALE_TYPE_SALE_EXCHANGE);
        }

        public void SaveSaleInvoiceItemWhenExchange(int SaleInvoiceId)
        {
            db.Configuration.ProxyCreationEnabled = false;
            SaleInvoice saleInvoice = db.SaleInvoices.Where(x => x.SaleInvoiceId == SaleInvoiceId).FirstOrDefault();
            if (saleInvoice != null && !saleInvoice.IsInventoryUpdate)
            {
                List<SaleInvoiceItem> SaleInvoiceItemList = db.SaleInvoiceItems.Where(x => x.ReturnSaleInvoiceId == SaleInvoiceId).ToList();
                foreach (var item in SaleInvoiceItemList)
                {
                    if (item.Status == Constants.SALE_ITEM_RETURN)
                    {
                        Product product = db.Products.Where(x => x.ProductId == item.ProductId).FirstOrDefault();
                        if (product.SellInPartial == true)
                        {
                            product.Quantity = product.Quantity + item.ConversionValue.Value;
                            product.IsSold = false;
                            db.SaveProduct(product);
                        }
                        else
                        {
                            product.IsSold = false;
                            db.SaveProduct(product);
                        }
                    }
                }
                SaleInvoiceItemList = db.SaleInvoiceItems.Where(x => x.SaleInvoiceId == SaleInvoiceId).ToList();
                foreach (var item in SaleInvoiceItemList)
                {
                    if (item.Status == Constants.SALE_ITEM_SOLD)
                    {
                        Product product = db.Products.Where(x => x.ProductId == item.ProductId).FirstOrDefault();
                        if (product.SellInPartial == true)
                        {
                            product.Quantity = product.Quantity - item.ConversionValue.Value;
                            if (product.Quantity == 0)
                            {
                                product.IsSold = true;
                            }
                            db.SaveProduct(product);
                        }
                        else
                        {
                            product.IsSold = true;
                            db.SaveProduct(product);
                        }
                    }
                }
                saleInvoice.IsInventoryUpdate = true;
                db.SaveSaleInvoiceReturn(saleInvoice);
            }
        }

        public void SaveSaleInvoiceItemWhenReturn(int saleInvoiceId)
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<SaleInvoiceItem> SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_SALE_RETURN);
            if (SaleInvoiceItemList != null)
            {
                foreach (var item in SaleInvoiceItemList)
                {
                    if (item.Status == Constants.SALE_ITEM_RETURN)
                    {
                        saledb.UpdateSaleInvoiceItem(item);
                        Product product = db.Products.Where(x => x.ProductId == item.ProductId).FirstOrDefault();
                        if (product.SellInPartial == true)
                        {
                            product.Quantity = product.Quantity + item.ConversionValue.Value;
                            product.IsSold = false;
                            db.SaveProduct(product);
                        }
                        else
                        {
                            product.IsSold = false;
                            db.SaveProduct(product);
                        }

                        SaleInvoiceItem newItem = new SaleInvoiceItem(item);
                        newItem.SaleInvoiceId = saleInvoiceId;
                        db.SaveSaleInvoiceItem(newItem);
                    }
                }
            }
            SessionManager.EmptySessionList(Constants.SALE_TYPE_SALE_RETURN);
        }

        public SaleInvoice CalculationForReturn(SaleInvoice saleInvoice)
        {
            try
            {
                saleInvoice.Subtotal = 0;
                saleInvoice.TotalTax = 0;
                saleInvoice.TotalDiscount = 0;
                saleInvoice.TotalInvoiceValue = 0;
                saleInvoice.TotalCGST = 0;
                saleInvoice.TotalSGST = 0;
                saleInvoice.TotalIGST = 0;
                foreach (var item in saleInvoice.SaleInvoiceItemList)
                {
                    if (item.Status == Constants.SALE_ITEM_RETURN)
                    {
                        saleInvoice.Subtotal = saleInvoice.Subtotal - item.BasicPrice.Value;
                        saleInvoice.TotalTax = saleInvoice.TotalTax - item.ApplicableTaxValue;
                        saleInvoice.TotalDiscount = (saleInvoice.TotalDiscount - item.Discount);
                        if (saleInvoice.Adjustment == null)
                        {
                            saleInvoice.Adjustment = 0;
                        }
                        saleInvoice.TotalInvoiceValue = (saleInvoice.Subtotal - saleInvoice.TotalDiscount + saleInvoice.Adjustment.Value + saleInvoice.TotalTax);
                        saleInvoice.FinalTotalAdj = saleInvoice.TotalInvoiceValue - saleInvoice.Adjustment.Value;

                        saleInvoice.TotalCGST -= item.CGST.Value;
                        saleInvoice.TotalSGST -= item.SGST.Value;
                        saleInvoice.TotalIGST -= item.IGST.Value;
                    }
                }
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
            return saleInvoice;
        }

        public void UpdateLedger(int SaleInvoiceId)
        {
            SaleInvoice saleinvoice = db.SaleInvoices.Include("SaleInvoiceItemList").Where(x => x.SaleInvoiceId == SaleInvoiceId).FirstOrDefault();
            if (saleinvoice != null && !saleinvoice.IsLedgerUpdate)
            {
                saleinvoice.TotalDiscountNotMapped = saleinvoice.TotalDiscount + (saleinvoice.Adjustment ?? 0);
                var item = db.SaleInvoiceItems.Where(x => x.ReturnSaleInvoiceId == saleinvoice.ReturnSaleInvoiceId).ToList();
                if (item != null)
                {
                    for (int i = 0; i < item.Count; i++)
                    {
                        saleinvoice.SaleInvoiceItemList.Add(item[i]);
                    }
                }
                if (saleinvoice.PaymentTypeId != 7 && saleinvoice.CustomerId != 61)
                {
                    #region Customer Voucher For Credit
                    Voucher CustomerVoucherForCredit = new Voucher();
                    CustomerVoucherForCredit.LedgerId = saleinvoice.CustomerId;
                    CustomerVoucherForCredit.SaleInvoiceId = saleinvoice.SaleInvoiceId;
                    if (saleinvoice.TotalWithAdjustment > 0)
                    {
                        CustomerVoucherForCredit.Total = saleinvoice.TotalWithAdjustment;
                        CustomerVoucherForCredit.Type = Constants.CR;
                    }
                    else
                    {
                        CustomerVoucherForCredit.Total = Math.Abs(saleinvoice.TotalWithAdjustment);
                        CustomerVoucherForCredit.Type = Constants.DR;

                    }
                    CustomerVoucherForCredit.InvoiceNumber = saleinvoice.SaleInvoiceNo;
                    CustomerVoucherForCredit.EntryDate = saleinvoice.InvoicingDate;
                    CustomerVoucherForCredit.VoucherFor = Constants.SALE_TYPE_SALE_EXCHANGE;
                    CustomerVoucherForCredit.Particulars = "Sale Exchange Number :" + saleinvoice.SaleInvoiceNo;
                    db.SaveVauchers(CustomerVoucherForCredit);
                    #endregion

                    #region Customer Voucher For Debit
                    Voucher CustomerVoucherForDebit = new Voucher();
                    CustomerVoucherForDebit.LedgerId = saleinvoice.CustomerId;
                    CustomerVoucherForDebit.SaleInvoiceId = saleinvoice.SaleInvoiceId;
                    if (saleinvoice.TotalWithAdjustment > 0)
                    {
                        CustomerVoucherForDebit.Total = saleinvoice.TotalWithAdjustment;
                        CustomerVoucherForDebit.Type = Constants.DR;
                    }
                    else
                    {
                        CustomerVoucherForDebit.Total = Math.Abs(saleinvoice.TotalWithAdjustment);
                        CustomerVoucherForDebit.Type = Constants.CR;

                    }
                    CustomerVoucherForDebit.InvoiceNumber = saleinvoice.SaleInvoiceNo;
                    CustomerVoucherForDebit.EntryDate = saleinvoice.InvoicingDate;
                    CustomerVoucherForDebit.VoucherFor = Constants.SALE_TYPE_SALE_EXCHANGE;
                    CustomerVoucherForDebit.Particulars = "Sale Exchange Number :" + saleinvoice.SaleInvoiceNo;
                    db.SaveVauchers(CustomerVoucherForDebit);
                    #endregion
                }
                else if (saleinvoice.CustomerId != 61)
                {
                    #region Customer Voucher For Debit
                    Voucher CustomerVoucherForDebit = new Voucher();
                    CustomerVoucherForDebit.LedgerId = saleinvoice.CustomerId;
                    CustomerVoucherForDebit.SaleInvoiceId = saleinvoice.SaleInvoiceId;
                    if (saleinvoice.TotalWithAdjustment > 0)
                    {
                        CustomerVoucherForDebit.Total = saleinvoice.TotalWithAdjustment;
                        CustomerVoucherForDebit.Type = Constants.DR;
                    }
                    else
                    {
                        CustomerVoucherForDebit.Total = Math.Abs(saleinvoice.TotalWithAdjustment);
                        CustomerVoucherForDebit.Type = Constants.CR;

                    }
                    CustomerVoucherForDebit.InvoiceNumber = saleinvoice.SaleInvoiceNo;
                    CustomerVoucherForDebit.EntryDate = saleinvoice.InvoicingDate;
                    CustomerVoucherForDebit.VoucherFor = Constants.SALE_TYPE_SALE_EXCHANGE;
                    CustomerVoucherForDebit.Particulars = "Sale Exchange Number :" + saleinvoice.SaleInvoiceNo;
                    db.SaveVauchers(CustomerVoucherForDebit);
                    #endregion
                }

                #region Ledger For PaymentType
                Ledger LedgerForPaymentType = db.Ledgers.Where(x => x.LedgerId == db.PaymentTypes.Where(y => y.PaymentTypeId == saleinvoice.PaymentTypeId).Select(y => y.LedgerId).FirstOrDefault()).FirstOrDefault();
                if (LedgerForPaymentType != null && saleinvoice.PaymentTypeId != 7)
                {

                    Voucher CustomerVoucherForCredit = new Voucher();
                    CustomerVoucherForCredit.LedgerId = LedgerForPaymentType.LedgerId;
                    CustomerVoucherForCredit.SaleInvoiceId = saleinvoice.SaleInvoiceId;
                    if (saleinvoice.TotalWithAdjustment > 0)
                    {
                        CustomerVoucherForCredit.Total = saleinvoice.TotalWithAdjustment;
                        CustomerVoucherForCredit.Type = Constants.DR;
                    }
                    else
                    {
                        CustomerVoucherForCredit.Total = Math.Abs(saleinvoice.TotalWithAdjustment);
                        CustomerVoucherForCredit.Type = Constants.CR;
                    }
                    CustomerVoucherForCredit.InvoiceNumber = saleinvoice.SaleInvoiceNo;
                    CustomerVoucherForCredit.EntryDate = saleinvoice.InvoicingDate;
                    CustomerVoucherForCredit.VoucherFor = Constants.SALE_TYPE_SALE_EXCHANGE;
                    CustomerVoucherForCredit.Particulars = "Sale Exchange Number :" + saleinvoice.SaleInvoiceNo;
                    db.SaveVauchers(CustomerVoucherForCredit);
                }
                #endregion

                #region SaleVoucher  
                Voucher SaleVoucher = new Voucher();
                SaleVoucher.LedgerId = saleinvoice.SaleAccountId;
                SaleVoucher.SaleInvoiceId = saleinvoice.SaleInvoiceId;
                if (saleinvoice.TotalWithAdjustment > 0)
                {
                    SaleVoucher.Total = saleinvoice.TotalWithAdjustment;
                    SaleVoucher.Type = Constants.CR;
                }
                else
                {
                    SaleVoucher.Total = Math.Abs(saleinvoice.TotalWithAdjustment);
                    SaleVoucher.Type = Constants.DR;
                }
                SaleVoucher.InvoiceNumber = saleinvoice.SaleInvoiceNo;
                SaleVoucher.EntryDate = saleinvoice.InvoicingDate;
                SaleVoucher.VoucherFor = Constants.SALE_TYPE_SALE_EXCHANGE;
                SaleVoucher.Particulars = "Sale Exchange Number :" + saleinvoice.SaleInvoiceNo;
                db.SaveVauchers(SaleVoucher);
                #endregion

                #region TaxVoucher  
                Voucher Taxvoucher = db.Vouchers.Where(x => x.LedgerId == saleinvoice.SaleTypeId && x.SaleInvoiceId == saleinvoice.SaleInvoiceId && x.Type == Constants.CR).FirstOrDefault();
                if (Taxvoucher != null)
                {
                    Taxvoucher.SGST = Taxvoucher.IGST = Taxvoucher.CGST = 0;
                    foreach (var item1 in saleinvoice.SaleInvoiceItemList)
                    {
                        Taxvoucher.CGST = Taxvoucher.CGST + item1.CGST;
                        Taxvoucher.IGST = Taxvoucher.IGST + item1.IGST;
                        Taxvoucher.SGST = Taxvoucher.SGST + item1.SGST;
                    }
                    Taxvoucher.Total = saleinvoice.TotalTax;
                    db.SaveVauchers(Taxvoucher);
                }
                else
                {
                    Taxvoucher = new Voucher();
                    Taxvoucher.SGST = Taxvoucher.IGST = Taxvoucher.CGST = 0;
                    foreach (var item1 in saleinvoice.SaleInvoiceItemList)
                    {
                        Taxvoucher.CGST = Taxvoucher.CGST + item1.CGST;
                        Taxvoucher.IGST = Taxvoucher.IGST + item1.IGST;
                        Taxvoucher.SGST = Taxvoucher.SGST + item1.SGST;
                    }
                    Taxvoucher.LedgerId = saleinvoice.SaleTypeId;
                    Taxvoucher.SaleInvoiceId = saleinvoice.SaleInvoiceId;
                    Taxvoucher.Total = saleinvoice.TotalTax;
                    Taxvoucher.Type = Constants.CR;
                    Taxvoucher.InvoiceNumber = saleinvoice.SaleInvoiceNo;
                    Taxvoucher.EntryDate = saleinvoice.InvoicingDate;
                    Taxvoucher.VoucherFor = Constants.SALE_TYPE_SALE_EXCHANGE;
                    Taxvoucher.Particulars = "Sale Exchange Number :" + saleinvoice.SaleInvoiceNo;
                    db.SaveVauchers(Taxvoucher);
                }
                #endregion

                #region EmployeeCommision  
                if (saleinvoice.EmployeeId.HasValue && saleinvoice.EmployeeId > 0)
                {
                    Voucher EmployeeVoucher = db.Vouchers.Where(x => x.LedgerId == saleinvoice.EmployeeId.Value && x.SaleInvoiceId == saleinvoice.SaleInvoiceId && x.Type == Constants.CR).FirstOrDefault();
                    if (EmployeeVoucher != null)
                    {
                        saleinvoice.SalesManCommision = 0;
                        foreach (var item1 in saleinvoice.SaleInvoiceItemList)
                        {
                            if (item1.SalesmanCommision.HasValue && item1.SalesmanCommision > 0)
                            {
                                saleinvoice.SalesManCommision = saleinvoice.SalesManCommision + (item1.BasicPrice * (item1.SalesmanCommision / 100));
                            }
                        }
                        EmployeeVoucher.Total = saleinvoice.SalesManCommision.Value;
                        db.SaveVauchers(EmployeeVoucher);
                    }
                    else
                    {
                        EmployeeVoucher = new Voucher();
                        saleinvoice.SalesManCommision = 0;
                        foreach (var item1 in saleinvoice.SaleInvoiceItemList)
                        {
                            if (item1.SalesmanCommision.HasValue && item1.SalesmanCommision > 0)
                            {
                                saleinvoice.SalesManCommision = saleinvoice.SalesManCommision + (item1.BasicPrice * (item1.SalesmanCommision / 100));
                            }
                        }
                        if (saleinvoice.SalesManCommision > 0)
                        {
                            EmployeeVoucher.LedgerId = saleinvoice.EmployeeId;
                            EmployeeVoucher.SaleInvoiceId = saleinvoice.SaleInvoiceId;
                            EmployeeVoucher.Total = saleinvoice.SalesManCommision.Value;
                            EmployeeVoucher.Type = Constants.CR;
                            EmployeeVoucher.EntryDate = saleinvoice.InvoicingDate;
                            EmployeeVoucher.InvoiceNumber = saleinvoice.SaleInvoiceNo;
                            EmployeeVoucher.Particulars = "Sale Invoice Number :" + saleinvoice.SaleInvoiceNo;
                            EmployeeVoucher.VoucherFor = Constants.SALE_TYPE_SALE_INVOICE;
                            db.SaveVauchers(EmployeeVoucher);
                        }
                    }
                }
                #endregion

                #region Discount Voucher
                if (saleinvoice.TotalDiscountNotMapped != 0)
                {
                    Ledger ledger = db.Ledgers.Where(x => x.Name == Constants.LEDGER_TYPE_DISCOUNT).FirstOrDefault();
                    if (ledger != null)
                    {
                        Voucher DiscountVoucher = db.Vouchers.Where(x => x.LedgerId == ledger.LedgerId && x.SaleInvoiceId == saleinvoice.SaleInvoiceId).FirstOrDefault();
                        if (DiscountVoucher == null)
                        {
                            DiscountVoucher = new Voucher();
                        }
                        DiscountVoucher.LedgerId = ledger.LedgerId;
                        DiscountVoucher.SaleInvoiceId = saleinvoice.SaleInvoiceId;
                        DiscountVoucher.Total = Math.Abs(saleinvoice.TotalDiscountNotMapped);
                        if (saleinvoice.TotalDiscountNotMapped > 0)
                        {
                            DiscountVoucher.Type = Constants.DR;
                        }
                        else
                        {
                            DiscountVoucher.Type = Constants.CR;
                        }
                        DiscountVoucher.EntryDate = saleinvoice.InvoicingDate;
                        DiscountVoucher.InvoiceNumber = saleinvoice.SaleInvoiceNo;
                        DiscountVoucher.VoucherFor = Constants.SALE_TYPE_SALE_EXCHANGE;
                        DiscountVoucher.Particulars = "Sale Exchange Number :" + saleinvoice.SaleInvoiceNo;
                        db.SaveVauchers(DiscountVoucher);
                    }
                }
                #endregion

                #region SaleLedger  
                Ledger SaleLedger = db.Ledgers.Where(x => x.Name == Constants.LEDGER_TYPE_SALE_RETURN_LEDGER).FirstOrDefault();
                if (SaleLedger != null)
                {
                    Voucher SaleLedgerVoucher = new Voucher();
                        SaleLedgerVoucher.LedgerId = SaleLedger.LedgerId;
                        SaleLedgerVoucher.SaleInvoiceId = saleinvoice.SaleInvoiceId;
                        if (saleinvoice.TotalWithAdjustment > 0)
                        {
                        SaleLedgerVoucher.Total = saleinvoice.TotalWithAdjustment;
                        SaleLedgerVoucher.Type = Constants.CR;
                        }
                        else
                        {
                        SaleLedgerVoucher.Total = Math.Abs(saleinvoice.TotalWithAdjustment);
                        SaleLedgerVoucher.Type = Constants.DR;
                        }
                        SaleLedgerVoucher.InvoiceNumber = saleinvoice.SaleInvoiceNo;
                        SaleLedgerVoucher.EntryDate = saleinvoice.InvoicingDate;
                        SaleLedgerVoucher.Particulars = "Sale Exchange Number :" + saleinvoice.SaleInvoiceNo;
                        SaleLedgerVoucher.VoucherFor = Constants.SALE_TYPE_SALE_EXCHANGE;
                        db.SaveVauchers(SaleLedgerVoucher);
                }
                #endregion
                saleinvoice.IsLedgerUpdate = true;
                db.Entry(saleinvoice).State = EntityState.Modified;
            }
        }

        public SaleInvoice GetSaleInvoiceForCreateEdit(int id)
        {
            SaleInvoice saleinvoice = null;
            try
            {
                saleinvoice = db.SaleInvoices.Include("SaleInvoiceItemList").Where(x => x.SaleInvoiceId == id).FirstOrDefault();
                List<SaleInvoiceItem> SaleInvoiceItemList = db.SaleInvoiceItems.Where(x => x.ReturnSaleInvoiceId == id).ToList();
                saleinvoice.SaleInvoiceItemList.AddRange(SaleInvoiceItemList);
                SessionManager.EmptySessionList(Constants.SALE_TYPE_SALE_EXCHANGE);
                saleinvoice.SaleOrderList = DropdownManager.GetSaleOrderList(null, Enums.PurchaseOrderStatus.Closed);
                saleinvoice.SaleMemoList = DropdownManager.GetDeliveryMemoList();


                if (saleinvoice.SaleInvoiceItemList != null)
                {
                    saleinvoice = SaleCalculator.Calculate(saleinvoice);
                }
                //saleinvoice.ShippingTypeList = DropdownManager.GetShippingMasterList(null);                
                saleinvoice.LocationList = DropdownManager.GetLocationMasterList(null);
                saleinvoice.SaleAccountList = DropdownManager.GetLedgerListForType(null, Constants.LEDGER_TYPE_COMPANY);
                saleinvoice.CustomerAccountList = DropdownManager.GetLedgerListForType(null, Constants.LEDGER_TYPE_USER);
                saleinvoice.SaleTypeList = DropdownManager.GetLedgerListForType(null, Constants.LEDGER_TYPE_TAX);
                saleinvoice.EmployeeAccountList = DropdownManager.GetEmployeeList(null);
                saleinvoice.PaymentTypeList = DropdownManager.GetPaymentTypeList(null);
                //saleinvoice.BankAccountsList = DropdownManager.GetBankAccountsList(true, saleinvoice.LedgerBankId ?? 0);
                saleinvoice.DivisionList = DropdownManager.GetSessionDivisionSelectList();
                saleinvoice.BranchList = DropdownManager.GetSessionBranchSelectList();
                saleinvoice = Calculations(saleinvoice);

            }
            catch (Exception ex)
            {
                saledb.RegisterException(db, ex);
            }

            return saleinvoice;
        }

        public string DeleteInvoice(int id)
        {
            string error = string.Empty;
            SaleInvoice saleinvoice = db.SaleInvoices.Include("SaleInvoiceItemList").Where(x => x.SaleInvoiceId == id).FirstOrDefault();
            List<SaleInvoiceItem> SaleInvoiceItemList = db.SaleInvoiceItems.Where(x => x.ReturnSaleInvoiceId == id).ToList();
            saleinvoice.SaleInvoiceItemList.AddRange(SaleInvoiceItemList);
            if (saleinvoice == null)
            {
                error = MessageStore.DeleteError;
            }
            if (!RemoveSalesInvoice(saleinvoice))
            {
                error = "This invoice contains Products that have been sold. Please remove the item from the associated sale invoice and try again.";
            }
            return error;
        }

        public bool RemoveSalesInvoice(SaleInvoice salesinvoice)
        {
            bool result = true;
            List<int> saleInvoiceItemIds = salesinvoice.SaleInvoiceItemList.Select(x => x.SaleInvoiceItemId).ToList();
            List<int> productIds = salesinvoice.SaleInvoiceItemList.Select(x => x.ProductId).ToList();
            List<Product> productList = saledb.GetProductsBySaleInvoiceItems(productIds);
            //List<SaleInvoiceItem> saleInvoiceItems = db.SaleInvoiceItems.Where(x => saleInvoiceItemIds.Contains(x.SaleInvoiceItemId)).ToList();

            //Mark products as unsold            
            List<Product> UpdateProducts = new List<Product>();
            foreach (var item in salesinvoice.SaleInvoiceItemList)
            {
                if(item.Status == Constants.SALE_ITEM_SOLD)
                {
                    Product product = productList.Where(x => x.ProductId == item.ProductId).FirstOrDefault();
                    if (product.SellInPartial)
                    {
                        product.Quantity = product.Quantity + item.ConversionValue.Value;
                    }
                    product.IsSold = false;
                    UpdateProducts.Add(product);
                }
                else if(item.Status == Constants.SALE_ITEM_RETURN)
                {
                    Product product = productList.Where(x => x.ProductId == item.ProductId).FirstOrDefault();
                    if (product.SellInPartial)
                    {
                        product.Quantity = product.Quantity - item.ConversionValue.Value;
                    }
                    product.IsSold = true;
                    UpdateProducts.Add(product);
                }
            }

            if (saledb.DeleteSaleInvoice(UpdateProducts, salesinvoice.SaleInvoiceItemList, salesinvoice))
            {
                result = true;
            }
            return result;
        }
    }
}
