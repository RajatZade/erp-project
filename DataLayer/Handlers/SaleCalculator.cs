﻿using DataLayer.Models;
using DataLayer.Utils;
using System;
using System.Linq;
using System.Collections.Generic;

namespace DataLayer.Utils
{
    public class SaleCalculator
    {
        static DbConnector db = new DbConnector();

        public static SaleInvoice Calculate(SaleInvoice saleInvoice)
        {
            try
            {
                Dictionary<int, SKUMaster> idVsSKU = db.GetSKUMasterDictionary(saleInvoice.SaleInvoiceItemList.Select(x => x.SKUMasterId.Value).ToList());
                Dictionary<int, GST> idVsGST = db.GetGSTMasterDictionary(saleInvoice.SaleInvoiceItemList.Select(x => x.GSTId).ToList());
                Dictionary<int, Product> idVsProduct = db.GetProductDictionary(saleInvoice.SaleInvoiceItemList.Select(x => x.ProductId).ToList());
                saleInvoice.Subtotal = 0;
                saleInvoice.TotalTax = 0;
                saleInvoice.TotalDiscount = 0;
                saleInvoice.DiscountWithAdjustment = 0;
                foreach (var item in saleInvoice.SaleInvoiceItemList)
                {
                    item.Product = idVsProduct[item.ProductId];
                    saleInvoice.Subtotal = saleInvoice.Subtotal + item.BasicPrice.Value;
                    saleInvoice.TotalTax = saleInvoice.TotalTax + item.ApplicableTaxValue;
                    saleInvoice.TotalDiscount = (saleInvoice.TotalDiscount + item.Discount);

                    saleInvoice.SalesManCommisionNotMap = Convert.ToDecimal((saleInvoice.SalesManCommisionNotMap ?? 0) + (item.SalesmanCommision ?? 0));
                    saleInvoice.TotalCGST += (item.CGST.HasValue ? item.CGST.Value : 0);
                    saleInvoice.TotalSGST += (item.SGST ?? 0);
                    saleInvoice.TotalIGST += (item.IGST ?? 0);
                    item.GSTList = DropdownManager.GetGSTList(item.GSTId);
                    item.SKUMaster = idVsSKU[item.SKUMasterId.Value];
                    item.GST = idVsGST[item.GSTId];
                       
                        item.DiscountCodeList = DropdownManager.GetDiscountsAccessibleList(item.Product.DiscountsAccessible, item.Product.ProductId);
                        if (item.Product.SellInPartial)
                        {
                            item.Unit = db.GetUnitParents(item.Product.UnitId);
                            item.UnitList = DropdownManager.GetUnitConversionList(item.Unit.UnitId);
                            foreach (var listValue in item.UnitList)
                            {
                                if (listValue.Value == item.UnitId.ToString())
                                {
                                    listValue.Selected = true;
                                }
                            }
                    }
                    foreach (var listValue in item.DiscountCodeList)
                    {
                        if (listValue.Value == item.DiscountId.ToString())
                        {
                            listValue.Selected = true;
                        }
                    }

                }
                saleInvoice.Adjustment = saleInvoice.Adjustment ?? 0;
                saleInvoice.DiscountWithAdjustment = saleInvoice.TotalDiscount + saleInvoice.Adjustment;
                saleInvoice.TotalInvoiceValue = (saleInvoice.Subtotal + saleInvoice.TotalTax);
                saleInvoice.TotalWithAdjustment = saleInvoice.TotalInvoiceValue - saleInvoice.Adjustment.Value;
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
            return saleInvoice;
        }

        public static JobSheet CalculateProduction(JobSheet jobsheet)
        {
            foreach (var item in jobsheet.JobSheetItemList)
            {                              
                if (item.Product.SellInPartial)
                {
                    item.Unit = db.GetUnitParents(item.Product.UnitId);
                    item.UnitList = DropdownManager.GetUnitConversionList(item.Unit.UnitId);
                    foreach (var listValue in item.UnitList)
                    {
                        if (listValue.Value == item.UnitId.ToString())
                        {
                            listValue.Selected = true;
                        }
                    }
                    if(item.QuantityNotAvailable)
                    {
                        item.QuantityErrorMsg = item.Product.Quantity + item.Unit.UnitValue;
                    }
                }
                jobsheet.TotalProductionCostNotMapped = jobsheet.TotalProductionCostNotMapped + (item.ConversionValue.Value * item.Product.NetAmount.Value);
                jobsheet.TotalNotMapped = jobsheet.TotalNotMapped + (item.ConversionValue.Value * item.Product.NetAmount.Value);
            }
            return jobsheet;
        }

        public static SaleInvoice CalculatePurchaseReturn(SaleInvoice saleInvoice)
        {
            try
            {
                Dictionary<int, SKUMaster> idVsSKU = db.GetSKUMasterDictionary(saleInvoice.SaleInvoiceItemList.Select(x => x.SKUMasterId.Value).ToList());
                Dictionary<int, GST> idVsGST = db.GetGSTMasterDictionary(saleInvoice.SaleInvoiceItemList.Select(x => x.GSTId).ToList());
                foreach (var item in saleInvoice.SaleInvoiceItemList)
                {                    
                    saleInvoice.FinalTotalAdj = saleInvoice.TotalNotMapped = saleInvoice.TotalNotMapped + (item.BasicPrice ?? 0);
                    item.GSTList = DropdownManager.GetGSTList(item.GSTId);
                    item.SKUMaster = idVsSKU[item.SKUMasterId.Value];
                    item.GST = idVsGST[item.GSTId];
                    if (item.SKUMasterId != null)
                    {
                        item.DiscountCodeList = DropdownManager.GetDiscountsAccessibleList(item.Product.DiscountsAccessible, item.Product.ProductId);
                        if (item.Product.SellInPartial)
                        {
                            item.Unit = db.GetUnitParents(item.Product.UnitId);
                            item.UnitList = DropdownManager.GetUnitConversionList(item.Unit.UnitId);
                            foreach (var listValue in item.UnitList)
                            {
                                if (listValue.Value == item.UnitId.ToString())
                                {
                                    listValue.Selected = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
            return saleInvoice;
        }

        public static SalesOrder CalculateForOrder(SalesOrder SalesOrder)
        {
            try
            {
                Dictionary<int, SKUMaster> idVsSKU = db.GetSKUMasterDictionary(SalesOrder.SaleInvoiceItemList.Select(x => x.SKUMasterId.Value).ToList());
                Dictionary<int, GST> idVsGST = db.GetGSTMasterDictionary(SalesOrder.SaleInvoiceItemList.Select(x => x.GSTId).ToList());
                foreach (var item in SalesOrder.SaleInvoiceItemList)
                {
                    SalesOrder.SubtotalNotMapped = SalesOrder.SubtotalNotMapped + item.BasicPrice.Value;
                    SalesOrder.TotalTaxNotMapped = SalesOrder.TotalTaxNotMapped + item.ApplicableTaxValue;
                    SalesOrder.TotalDiscountNotMapped = (SalesOrder.TotalDiscountNotMapped + item.Discount);
                    SalesOrder.FinalTotalAdj = (SalesOrder.SubtotalNotMapped - SalesOrder.TotalDiscountNotMapped + SalesOrder.TotalTaxNotMapped);
                    item.GSTList = DropdownManager.GetGSTList(item.GSTId);
                    item.SKUMaster = idVsSKU[item.SKUMasterId.Value];
                    item.GST = idVsGST[item.GSTId];
                    if (item.SKUMasterId != null)
                    {
                        item.DiscountCodeList = DropdownManager.GetDiscountsAccessibleList(item.Product.DiscountsAccessible, item.Product.ProductId);
                        if (item.Product.SellInPartial)
                        {
                            item.Unit = db.GetUnitParents(item.Product.UnitId);
                            item.UnitList = DropdownManager.GetUnitConversionList(item.Unit.UnitId);
                            foreach (var listValue in item.UnitList)
                            {
                                if (listValue.Value == item.UnitId.ToString())
                                {
                                    listValue.Selected = true;
                                }
                            }
                        }
                        foreach (var listValue in item.DiscountCodeList)
                        {
                            if (listValue.Value == item.DiscountId.ToString())
                            {
                                listValue.Selected = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
            return SalesOrder;
        }

        public static InternalTransfer CalculateInternalTransfers(InternalTransfer Transfer)
        {
            try
            {
                Transfer.SubTotal = 0;
                Transfer.DiscountTotal = 0;
                Transfer.TaxTotal = 0;
                Transfer.GrandTotal = 0;
                Dictionary<int, SKUMaster> idVsSKU = db.GetSKUMasterDictionary(Transfer.InternalTransfers.Select(x => x.SKUMasterId.Value).ToList());
                Dictionary<int, GST> idVsGST = db.GetGSTMasterDictionary(Transfer.InternalTransfers.Select(x => x.GSTId).ToList());
                Dictionary<int, Product> idVsProduct = db.GetProductDictionary(Transfer.InternalTransfers.Select(x => x.ProductId).ToList());
                foreach (var item in Transfer.InternalTransfers)
                {
                    item.Product = idVsProduct[item.ProductId];
                    if (item.Product.SellInPartial)
                    {
                        Transfer.SubTotal = Transfer.SubTotal + item.BasicPrice.Value;
                    }
                    else
                    {
                        Transfer.SubTotal = Transfer.SubTotal + item.BasicPrice.Value;
                    }
                    Transfer.TaxTotal = Transfer.TaxTotal + item.ApplicableTaxValue;
                    Transfer.DiscountTotal = (Transfer.DiscountTotal + item.Discount);
                    Transfer.GrandTotal = Transfer.GrandTotal + (Transfer.SubTotal - Transfer.DiscountTotal + Transfer.TaxTotal);

                    item.GSTList = DropdownManager.GetGSTList(item.GSTId);
                    item.SKUMaster = idVsSKU[item.SKUMasterId.Value];
                    item.GST = idVsGST[item.GSTId];

                    item.DiscountCodeList = DropdownManager.GetDiscountsAccessibleList(item.Product.DiscountsAccessible, item.Product.ProductId);
                    if (item.Product.SellInPartial)
                    {
                        item.Unit = db.GetUnitParents(item.Product.UnitId);
                        item.UnitList = DropdownManager.GetUnitConversionList(item.Unit.UnitId);
                        foreach (var listValue in item.UnitList)
                        {
                            if (listValue.Value == item.UnitId.ToString())
                            {
                                listValue.Selected = true;
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
            return Transfer;
        }
    }
}
