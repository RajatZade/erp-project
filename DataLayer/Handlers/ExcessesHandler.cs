﻿using DataLayer.Models;
using DataLayer.Utils;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Utils
{
    public class ExcessesHandler
    {
        DbConnector db = new DbConnector();

        public SaleInvoice SaveSaleInvoice(SaleInvoice saleInvoice, string btn)
        {
            switch (btn)
            {
                case "Hold":
                    {
                        saleInvoice.Status = Constants.HOLD;
                        SaveSaleInvoice(saleInvoice);
                        SaveSaleInvoiceItem(saleInvoice.SaleInvoiceId,saleInvoice.Type);
                        break;
                    }
                case "Save & Print Invoice":
                    {
                        SaveSaleInvoice(saleInvoice);
                        SaveSaleInvoiceItem(saleInvoice.SaleInvoiceId,saleInvoice.Type);
                        UpdateInventory(saleInvoice.SaleInvoiceId);
                        LedgerUpdate(saleInvoice.SaleInvoiceId);
                        break;
                    }
                case "Save":
                    {
                        SaveSaleInvoice(saleInvoice);
                        SaveSaleInvoiceItem(saleInvoice.SaleInvoiceId,saleInvoice.Type);
                        UpdateInventory(saleInvoice.SaleInvoiceId);
                        LedgerUpdate(saleInvoice.SaleInvoiceId);
                        break;
                    }
                case "Email":
                    {
                        break;
                    }
                case "SMS":
                    {
                        break;
                    }
            }
            return saleInvoice;
        }

        public List<SaleInvoiceItem> GetSaleInvoiceItems(int? SaleInvoiceId)
        {
            List<SaleInvoiceItem> SaleInvoiceItem = null;
            try
            {
                SaleInvoiceItem = db.SaleInvoiceItems.Include(x => x.Product.SKUMaster).Where(x => x.SaleInvoiceId == SaleInvoiceId).OrderByDescending(x => x.SaleInvoiceId).ToList();
                if (SaleInvoiceItem == null)
                {
                    SaleInvoiceItem = new List<SaleInvoiceItem>();
                }
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
            return SaleInvoiceItem;
        }

        public bool RemoveSalesInvoiceItem(int? SaleInvoiceId, int? SaleMemoId, int ProductId)
        {
            bool result = true;
            SaleInvoiceItem item;
            try
            {
                item = db.SaleInvoiceItems.Where(x => x.ProductId == ProductId && (x.SaleInvoiceId == SaleInvoiceId)).FirstOrDefault();
                db.SaleInvoiceItems.Remove(item);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                db.RegisterException(ex);
            }
            return result;
        }

        public void SaveSaleInvoice(SaleInvoice saleInvoice)
        {
            saleInvoice.SaleInvoiceItemList = null;
            try
            {
                if (saleInvoice.SaleInvoiceId > 0)
                {
                    db.Entry(saleInvoice).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    TimeZoneInfo timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                    DateTime dateTime = TimeZoneInfo.ConvertTime(DateTime.UtcNow, timeZoneInfo);
                    saleInvoice.Time = dateTime.ToString("hh:mm tt");
                    GenerateAutoNo No = new GenerateAutoNo();
                    saleInvoice.SaleInvoiceNo = No.GenerateInvoiceNoForType(saleInvoice);
                    db.SaleInvoices.Add(saleInvoice);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
        }

        public void SaveSaleInvoiceItem(int SaleInvoiceId,string Type)
        {
            try
            {
                List<SaleInvoiceItem> SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Type);
                if (SaleInvoiceItemList != null)
                {
                    foreach (var items in SaleInvoiceItemList)
                    {
                        items.Product = null;
                        items.GST = null;
                        items.DiscountCodeList = null;
                        items.GSTList = null;
                        items.Unit = null;
                        items.SKUMaster = null;
                        items.Status = Constants.SALE_ITEM_EXCESSES;
                        items.SaleInvoiceId = SaleInvoiceId;
                        db.SaleInvoiceItems.Add(items);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
            SessionManager.EmptySessionList(Type);
        }

        public void UpdateInventory(int SaleInvoiceId)
        {
            SaleInvoice item = db.GetSalesInvoiceById(SaleInvoiceId);
            if (item.IsInventoryUpdate != true)
            {
                #region Reduce Products From Stock
                foreach (var item1 in item.SaleInvoiceItemList)
                {
                    int ProductId = item1.ProductId;
                    Product product = db.Products.Where(x => x.ProductId == ProductId).FirstOrDefault();
                    if (product.SellInPartial == true)
                    {
                        product.Quantity = product.Quantity + item1.ConversionValue.Value;
                        product.IsSold = false;
                        db.SaveProduct(product);
                    }
                    else
                    {
                        product.IsSold = false;
                        db.SaveProduct(product);
                    }
                    if (item1.SalesmanCommision != null)
                    {
                        item.SalesManCommision = item1.SalesmanCommision;
                    }
                }
                #endregion

                item.Status = Constants.SALE_INVOICE_SOLD;
                item.IsInventoryUpdate = true;
                db.SaveSaleInvoiceEdit(item);
            }
        }

        public void LedgerUpdate(int SaleInvoiceId)
        {
            SaleInvoice item = db.GetSalesInvoiceById(SaleInvoiceId);
            if(item != null)
            {
                #region Customer Voucher For Debit
                Voucher CustomerVoucherForDebit = db.Vouchers.Where(x => x.LedgerId == item.CustomerId && x.SaleInvoiceId == item.SaleInvoiceId && x.Type == Constants.DR).FirstOrDefault();
                if (CustomerVoucherForDebit != null)
                {
                    CustomerVoucherForDebit.Total = item.TotalWithAdjustment;
                    db.SaveVauchers(CustomerVoucherForDebit);
                }
                else
                {
                    CustomerVoucherForDebit = new Voucher();
                    CustomerVoucherForDebit.LedgerId = item.CustomerId;
                    CustomerVoucherForDebit.SaleInvoiceId = item.SaleInvoiceId;
                    CustomerVoucherForDebit.Total = item.TotalWithAdjustment;
                    CustomerVoucherForDebit.Type = Constants.DR;
                    CustomerVoucherForDebit.InvoiceNumber = item.SaleInvoiceNo;
                    CustomerVoucherForDebit.EntryDate = item.InvoicingDate;
                    CustomerVoucherForDebit.VoucherFor = item.Type;
                    CustomerVoucherForDebit.Particulars = item.Type + " Number :" + item.SaleInvoiceNo;
                    db.SaveVauchers(CustomerVoucherForDebit);
                }
                #endregion
            }
            item.IsLedgerUpdate = true;
            db.SaveSaleInvoiceEdit(item);
        }



        public int UpDateSaleInvoiceItem(SaleInvoiceItem model)
        {
            db.Configuration.ProxyCreationEnabled = false;
            SaleInvoiceItem item = db.SaleInvoiceItems.Where(x => x.SaleInvoiceItemId == model.SaleInvoiceItemId).FirstOrDefault();
            if (item != null)
            {
                item.Product = db.Products.Where(x => x.ProductId == item.ProductId).FirstOrDefault();
                item.SalesmanCommision = model.SalesmanCommision;
                item.Adjustment = model.Adjustment;
                item.MRP = model.BasicPrice ?? 0;
                item.BasicPrice = model.BasicPrice;
                item.Discount = model.Discount;
                item.UnitId = model.UnitId;
                item.ConversionValue = model.ConversionValue;
                item.Quantity = model.Quantity;
                item.DiscountId = model.DiscountId;
                item.GSTId = model.GSTId;
                if (item.GSTId > 0)
                {
                    item.GST = db.GetGSTMasterById(item.GSTId);
                    item.ApplicableTax = ((item.GST.CGST ?? 0) + (item.GST.IGST ?? 0) + item.GST.SGST);
                }
                else
                {
                    item.GST = new GST
                    {
                        CGST = 0,
                        SGST = 0,
                        IGST = 0
                    };
                    item.ApplicableTax = 0;
                }

                if (item.DiscountId.HasValue)
                {
                    Decimal Percent = db.Discounts.Where(x => x.DiscountId == item.DiscountId.Value).Select(x => x.DiscountPercent.Value).FirstOrDefault();
                    item.Discount = Math.Round((item.BasicPrice.Value * (Percent / 100)) * 10000) / 10000;
                }
                else
                {
                    item.Discount = 0;
                }

                item.ApplicableTaxValue = Math.Round(((item.BasicPrice.Value - item.Discount) * (item.ApplicableTax / 100)) * 10000) / 10000;
                item.CGST = Math.Round(((item.BasicPrice.Value - item.Discount) * (item.GST.CGST ?? 0) / 100) * 10000) / 10000;
                item.SGST = Math.Round(((item.BasicPrice.Value - item.Discount) * (item.GST.SGST) / 100) * 10000) / 10000;
                item.IGST = Math.Round(((item.BasicPrice.Value - item.Discount) * (item.GST.IGST ?? 0) / 100) * 10000) / 10000;
                item.SubTotal = Math.Round(((item.BasicPrice.Value + item.Adjustment - item.Discount) + item.ApplicableTaxValue) * 10000) / 10000;
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
            }
            return item.SaleInvoiceId ?? 0;
        }
    }
}
