﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using DataLayer.Utils;

namespace DataLayer.Utils
{
    public class ContactHandler
    {
        DbConnector db = new DbConnector();

        public string CreateContact(Contact contact)
        {
            
            if (contact == null)
            {
                return string.Empty;
            }
            try
            {
                if (contact.ContactId == 0)
                {
                    if (contact.Type == "Employee")
                    {
                        contact.EmployeeNumber = new GenerateAutoNo().GenerateEmployeeNumber();
                    }
                    else if (contact.Type == "Customer")
                    {
                        contact.CustomerNumber = new GenerateAutoNo().GenerateCustomerNumber();
                    }
                    else if (contact.Type == "Supplier")
                    {
                        contact.SupplierNumber = new GenerateAutoNo().GenerateSupplierNumber();
                    }
                    db.Contacts.Add(contact);
                    db.SaveChanges();
                }
                else
                {
                    db.Entry(contact).State = EntityState.Modified;
                    db.SaveChanges();
                }


                if (contact.IsLedgerRequired)
                {
                    if(contact.Ledger.LedgerId == 0)
                    {
                        if(contact.TitleList == Enums.TitleType.Ms)
                        {
                            contact.Ledger.Name = contact.CompanyName;
                        }
                        else
                        {
                            contact.Ledger.Name = contact.FirstName + " " + contact.LastName;
                        }
                        contact.Ledger.ContactId = contact.ContactId;
                        //contact.Ledger.CurrentBalance = contact.Ledger.StartingBalance;
                        if(contact.Ledger.AccountSubCategoryId == -1)
                        {
                            contact.Ledger.AccountSubCategoryId = null;
                        }
                        db.Ledgers.Add(contact.Ledger);
                        db.SaveChanges();
                    }
                    else
                    {
                        db.UpdateLedger(contact.Ledger);
                    }
                    Voucher Voucher = db.Vouchers.Where(x => x.LedgerId == contact.Ledger.LedgerId && x.VoucherFor == Constants.OPENING_BALANCE).FirstOrDefault();
                    if (Voucher != null)
                    {
                        Voucher.Type = contact.Ledger.Type;
                        Voucher.Total = contact.Ledger.StartingBalance;
                        Voucher.EntryDate = contact.Ledger.EntryDate ?? DateTime.Now;
                        db.SaveVauchers(Voucher);
                    }
                    else
                    {
                        Voucher = new Voucher();
                        Voucher.LedgerId = contact.Ledger.LedgerId;
                        Voucher.Total = contact.Ledger.StartingBalance;
                        Voucher.Type = contact.Ledger.Type;
                        Voucher.VoucherFor = Constants.OPENING_BALANCE;
                        Voucher.Particulars = Constants.OPENING_BALANCE;
                        Voucher.EntryDate = contact.Ledger.EntryDate ?? DateTime.Now;
                        db.SaveVauchers(Voucher);
                    }
                }

                if (contact.IsLoginRequired)
                {
                    if(contact.LoginUser.LoginUserId == 0)
                    {
                        contact.LoginUser.ContactId = contact.ContactId;
                        db.LoginUsers.Add(contact.LoginUser);
                        db.SaveChanges();
                    }
                    {
                        db.UpdateLoginUser(contact.LoginUser);
                    }
                }
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
            return null;
        }

        public List<Contact> GetContactList(string Type)
        {
            List<Contact> ContactList = new List<Contact>();
            try
            {
                ContactList = db.Contacts.Where(p => p.Type == Type).OrderByDescending(p => p.ContactId).ToList();
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
            return ContactList;
        }

        public Contact GetContactById(int? id)
        {
            Contact model = null;
            try
            {
                model = db.Contacts.Where(p => p.ContactId == id).FirstOrDefault();
                if(model.IsLedgerRequired)
                {
                    model.Ledger = db.Ledgers.Where(x => x.ContactId == model.ContactId).FirstOrDefault();
                    if(model.Ledger.AccountCategoryId.HasValue)
                    {
                        model.Ledger.ContactSubCategoryList = DropdownManager.GetContactSubCategoryList(model.Ledger.AccountCategoryId);
                    }
                    Voucher voucher = db.Vouchers.Where(x => x.LedgerId == model.Ledger.LedgerId && x.VoucherFor == Constants.OPENING_BALANCE).FirstOrDefault();
                    if (voucher != null)
                    {
                        model.Ledger.StartingBalance = voucher.Total;
                        model.Ledger.EntryDate = voucher.EntryDate;
                    }
                    else
                    {
                        model.Ledger.StartingBalance = 0;
                        model.Ledger.EntryDate = DateTime.Now;
                    }
                }
                else
                {
                    model.Ledger = new Ledger();
                    model.Ledger.EntryDate = DateTime.Now;
                }

                if(model.IsLoginRequired)
                {
                    model.LoginUser = db.LoginUsers.Where(x => x.ContactId == model.ContactId).FirstOrDefault();
                    model.LoginUser.ConfirmPassword = model.LoginUser.Password;
                }
                else
                {
                    model.LoginUser = new LoginUser();
                }
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
            return model;
        }

        public bool DeleteContact(int id)
        {
            try
            {
                Contact model = db.Contacts.Where(x => x.ContactId == id).FirstOrDefault();
                db.Contacts.Remove(model);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
                return false;
            }
            return true;
        }
    }
}
