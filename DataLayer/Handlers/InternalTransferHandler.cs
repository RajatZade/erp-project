﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;
using PagedList;

namespace DataLayer.Utils
{
    public class InternalTransferHandler
    {
        InternalTransferDbUtil DbUtil = new InternalTransferDbUtil();
        DbConnector db = new DbConnector();

        public IPagedList<InternalTransfer> GetInternalTransferList(int? PageNo)
        {
            IPagedList<InternalTransfer> InternalTransferList = DbUtil.GetInternalTransfer(PageNo);
            return InternalTransferList;
        }

        public InternalTransfer GetInternalTransfer(int? id)
        {
            InternalTransfer Transfer = null;
            try
            {
                if (id.HasValue)
                {
                    Transfer = db.InternalTransfers.Include("InternalTransfers").Where(x => x.InternalTransferId == id).FirstOrDefault();
                    SessionManager.EmptySessionList(Constants.INTERNAL_TRANSFER);
                    Transfer.ToBranchList = DropdownManager.GetBranchByDivision(Transfer.ToDivisionId);
                    Transfer.FromBranchList = DropdownManager.GetBranchByDivision(Transfer.FromDivisionId);
                    Transfer.ToLocationList = DropdownManager.GetLocationByBranch(Transfer.ToBranchId);
                    Transfer.FromLocationList = DropdownManager.GetLocationByBranch(Transfer.FromBranchId);
                }
                else
                {
                    Transfer = new InternalTransfer();
                    Transfer.InternalTransfers = new List<SaleInvoiceItem>();
                    Transfer.TransferDate = Convert.ToDateTime(DateTime.Now);
                    var item = SessionManager.GetInvoiceItemList(Constants.INTERNAL_TRANSFER);
                    if (item != null)
                    {
                        for (int i = 0; i < item.Count; i++)
                        {
                            Transfer.InternalTransfers.Add(item[i]);
                        }
                    }
                }

                if (Transfer.InternalTransfers != null)
                {
                    Transfer = SaleCalculator.CalculateInternalTransfers(Transfer);
                }
                Transfer.DivisionList = DropdownManager.GetDivisionListByCompany(db.CompanyId);
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }

            return Transfer;
        }

        public InternalTransfer SaveInternalTransfer(InternalTransfer Transfer)
        {
            DbUtil.SaveInternalTransfer(Transfer);
            DbUtil.SaveInternalTransferItem(Transfer.InternalTransferId, Transfer.ToLocationId, Transfer.FromLocationId);
            return Transfer;
        }

        public string DeleteTransfer(int id)
        {
            string error = string.Empty;
            InternalTransfer Transfer = db.InternalTransfers.Include("InternalTransfers").Where(x => x.InternalTransferId == id).FirstOrDefault();
            if (Transfer == null)
            {
                error = MessageStore.DeleteError;
            }
            if (!RemoveInternalTransfer(Transfer))
            {
                error = "This invoice contains Products that have been sold. Please remove the item from the associated sale invoice and try again.";
            }
            return error;
        }

        public bool RemoveInternalTransfer(InternalTransfer Transfer)
        {
            bool result = true;
            List<int> saleInvoiceItemIds = Transfer.InternalTransfers.Select(x => x.SaleInvoiceItemId).ToList();
            List<int> productIds = Transfer.InternalTransfers.Select(x => x.ProductId).ToList();
            List<Product> productList = db.Products.Where(x => productIds.Contains(x.ProductId)).ToList();
            List<SaleInvoiceItem> saleInvoiceItems = db.SaleInvoiceItems.Where(x => saleInvoiceItemIds.Contains(x.SaleInvoiceItemId)).ToList();


            foreach (var item in productList)
            {
                item.LocationId = item.FromLocation ?? 0;
                item.FromLocation = null;
            }

            if (DbUtil.DeleteInternalTransfer(productList, Transfer))
            {
                result = true;
            }
            return result;
        }
    }
}
