﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLayer.Models;
using System.Web.Mvc;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;

namespace DataLayer.Utils
{
    public class PurchaseInvoiceHandler
    {
        DbConnector db = new DbConnector();
        PurchaseInvoiceDbUtil DbUtil = new PurchaseInvoiceDbUtil();
        public bool DeletePurchaseInvoice(PurchaseInvoice purchaseInvoice)
        {
            bool result = false;
            PurchaseInvoiceDbUtil db = new PurchaseInvoiceDbUtil();
            List<int> purchaseInvoiceItemIds = purchaseInvoice.PurchaseInvoiceItemList.Select(x => x.PurchaseInvoiceItemId).ToList();
            Product product = db.GetUnsoldProductByPurchaseInvoiceItemId(purchaseInvoiceItemIds);
            if (product != null)
            {
                return result;
            }
            List<int> productIds = db.GetProductIdsByInvoiceItems(purchaseInvoiceItemIds);
            List<int> attributesToBeDeleted = db.GetProductAttributes(productIds, purchaseInvoiceItemIds);

            //Query Ledger

            //Deletion Process starts here

            //Ledger delete

            //Attributes Delete
            //List<Voucher> VoucherList = db.GetVoucherListForPurchaseInvoiceId(purchaseInvoice.PurchaseInvoiceId);
            List<AttributeLinking> attributeLinkings = new List<AttributeLinking>();
            List<Product> products = new List<Product>();
            List<PurchaseInvoiceItem> purchaseInvoiceItems = new List<PurchaseInvoiceItem>();
            foreach (var item in attributesToBeDeleted)
            {
                AttributeLinking obj = new AttributeLinking() { AttributeLinkingId = item };
                attributeLinkings.Add(obj);
            }
            foreach (var item in productIds)
            {
                Product obj = new Product() { ProductId = item };
                products.Add(obj);
            }
            foreach (var item in purchaseInvoiceItemIds)
            {
                PurchaseInvoiceItem obj = new PurchaseInvoiceItem() { PurchaseInvoiceItemId = item };
                purchaseInvoiceItems.Add(obj);
            }

            if (DbUtil.DeletePurchaseInvoice(attributeLinkings, products, purchaseInvoiceItems, purchaseInvoice))
            {
                result = true;
            }
            return result;
        }

        public int SavePurchaseInvoice(PurchaseInvoice purchaseInvoice, string btn)
        {
            switch (btn)
            {
                case "Save":
                    {
                        DbUtil.SavePurchaseInvoice(purchaseInvoice);
                        break;
                    }
                case "Save and Print Barcode":
                    {
                        DbUtil.SavePurchaseInvoice(purchaseInvoice);
                        CreateProducts(purchaseInvoice.PurchaseInvoiceId, purchaseInvoice.CompanyId ?? 0);
                        break;
                    }
                case "Email":
                    {
                        break;
                    }
                case "SMS":
                    {
                        break;
                    }
                case "Save Invoice":
                    {
                        DbUtil.SavePurchaseInvoice(purchaseInvoice);
                        ChangePurchaseOrderStatus(purchaseInvoice.PurchaseInvoiceId);
                        break;
                    }
            }
            CalculatePurchaseInvoice(purchaseInvoice.PurchaseInvoiceId);
            return purchaseInvoice.PurchaseInvoiceId;
        }

        public PurchaseInvoiceItem SavePurchaseInvoiceItem(PurchaseInvoiceItem purchaseInvoiceItem, string btn)
        {
            db.SavePurchaseInvoiceItem(purchaseInvoiceItem);
            purchaseInvoiceItem.AttributeLinkingList = SessionManager.GetAttributeList();
            if (purchaseInvoiceItem.AttributeLinkingList != null)
            {
                foreach (var item in purchaseInvoiceItem.AttributeLinkingList)
                {
                    if (item.PurchaseInvoiceItemId == null || item.PurchaseInvoiceItemId == 0)
                    {
                        item.PurchaseInvoiceItemId = purchaseInvoiceItem.PurchaseInvoiceItemId;
                        item.AttributeLinkingId = 0;
                    }
                }
                db.SaveAttributeLinking(purchaseInvoiceItem.AttributeLinkingList);
                SessionManager.EmptySessionList(Constants.ATTRIBUTE_LIST);
            }

            CalculatePurchaseInvoice(purchaseInvoiceItem.PurchaseInvoiceId);
            return purchaseInvoiceItem;
        }

        public PurchaseInvoiceItem GetInvoiceItemForCreateEdit(int invoiceId, int? invoiceItemId, string IsClone)
        {
            PurchaseInvoiceItem item = null;

            if (invoiceItemId.HasValue)
            {
                item = db.PurchaseInvoiceItems.Include(x => x.SKUMaster).Where(x => x.PurchaseInvoiceItemId == invoiceItemId).FirstOrDefault();
                //item.DiscountIDTEmp = item.DiscountId;
                item.AttributeLinkingList = db.GetInvoiceItemAttribute(invoiceItemId);
                if (item.AttributeLinkingList != null)
                {
                    foreach (var i in item.AttributeLinkingList)
                    {
                        i.NameList = i.AttributeSetIds.Split(',').ToList();
                        i.ValuesList = i.Value.Split(',').ToList();
                    }
                }
            }
            else
            {
                item = new PurchaseInvoiceItem
                {
                    PurchaseInvoiceId = invoiceId,
                    DiscountsAccessible = "",
                };
            }
            item.CategoryList = DropdownManager.GetCategoryList(item.CategoryId);
            item.SubcategoryList = DropdownManager.GetSubCategoryList(item.SubCategoryId);
            item.SKUList = DropdownManager.GetSKUList(item.SKUMasterId);
            item.HSNList = DropdownManager.GetHSNList(item.HSNId);
            item.BrandList = DropdownManager.GetBrandList(item.BrandId);
            item.LocationList = DropdownManager.GetLocationByBranch(SessionManager.GetSessionBranch().BranchId);
            item.DiscountCodeList = DropdownManager.GetDiscountsAccessibleList(item.DiscountsAccessible,null);
            item.BrandList = DropdownManager.GetBrandList(item.BrandId);
            item.GSTList = DropdownManager.GetGSTList(item.GSTId);

            Dictionary<AttributeSet, List<SelectListItem>> AttributeDictionayList = null;
            item.AttributeSetListNew = db.GetAttributeSets();

            AttributeDictionayList = new Dictionary<AttributeSet, List<SelectListItem>>();
            for (int i = 0; i < item.AttributeSetListNew.Count; i++)
            {
                int Id = item.AttributeSetListNew[i].AttributeSetId;
                item.AttributeSetListNew[i].AttributeValueList = DropdownManager.GetAttributeValueList(Id);
                AttributeDictionayList.Add(item.AttributeSetListNew[i], item.AttributeSetListNew[i].AttributeValueList);
            }
            item.AttributeDictionayList = AttributeDictionayList;
            SessionManager.EmptySessionList(Constants.ATTRIBUTE_LIST);
            if (!string.IsNullOrEmpty(IsClone))
            {
                item.IsClone = IsClone;
                item.PurchaseInvoiceItemId = 0;
                item.IsProductCreated = false;
                foreach (var AttributeItem in item.AttributeLinkingList)
                {                    
                    AttributeItem.PurchaseInvoiceItemId = 0;
                    SessionManager.SetAttribute(AttributeItem);
                }
                item.AttributeLinkingList = SessionManager.GetAttributeList();
            }
            if (item.IsProductCreated)
            {
                item.UnitList = DropdownManager.GetFreezedUnitList(item.UnitId);
            }
            else
            {
                item.UnitList = DropdownManager.GetUnitMasterList(item.UnitId);
            }
            return item;
        }

        public PurchaseInvoice GetPurchaseInvoiceForCreateEdit(int? id)
        {
            PurchaseInvoice purchaseinvoice = null;
            if (id.HasValue)
            {
                purchaseinvoice = db.PurchaseInvoices.Include("PurchaseInvoiceItemList").Where(x => x.PurchaseInvoiceId == id).FirstOrDefault();
                if (purchaseinvoice == null)
                {
                    purchaseinvoice = new PurchaseInvoice();
                }
                Dictionary<int, SKUMaster> idVsSKU = db.GetSKUMasterDictionary(purchaseinvoice.PurchaseInvoiceItemList.Select(x => x.SKUMasterId).ToList());
                Dictionary<int, GST> idVsGST = db.GetGSTMasterDictionary(purchaseinvoice.PurchaseInvoiceItemList.Select(x => x.GSTId).ToList());
                foreach (var item in purchaseinvoice.PurchaseInvoiceItemList)
                {
                    item.SKUMaster = idVsSKU[item.SKUMasterId];
                    item.GST = idVsGST[item.GSTId];
                    purchaseinvoice.ItemTotalQuantity = purchaseinvoice.ItemTotalQuantity + item.Quantity;
                    purchaseinvoice.ItemTotalBasicPrice = purchaseinvoice.ItemTotalBasicPrice + item.Rate;
                    purchaseinvoice.ItemTotalMRP = purchaseinvoice.ItemTotalMRP + item.MRP;                    
                    purchaseinvoice.ItemPurchasePrice = purchaseinvoice.ItemPurchasePrice + item.NetAmount.Value;
                    purchaseinvoice.ItemFinalTotal = purchaseinvoice.ItemFinalTotal + (item.Quantity * item.NetAmount.Value);
                    purchaseinvoice.FinalTotalAdj = purchaseinvoice.ItemFinalTotal;
                }
                purchaseinvoice.PurchaseOrderList = DropdownManager.GetPurchaseOrderList(null,Enums.PurchaseOrderStatus.Closed);
            }
            else
            {

                purchaseinvoice = new PurchaseInvoice
                {
                    CompanyId = db.CompanyId,
                    Type = Constants.PURCHASE_TYPE_PURCHASE_INVOICE,
                    InvoicingDate = Convert.ToDateTime(DateTime.Now),                   
                    EntryDate = Convert.ToDateTime(DateTime.Now),
                    PurchaseOrderList = DropdownManager.GetPurchaseOrderList(null, Enums.PurchaseOrderStatus.Open),
                };
                PurchaseInvoice LastPurchaseInvoice = db.PurchaseInvoices.Where(x => x.CompanyId == db.CompanyId).OrderByDescending(x => x.PurchaseInvoiceId).FirstOrDefault();
                if (LastPurchaseInvoice != null)
                {
                    purchaseinvoice.PurchaseAccountId = LastPurchaseInvoice.PurchaseAccountId;
                    purchaseinvoice.AccountId = LastPurchaseInvoice.AccountId;
                    purchaseinvoice.BranchId = LastPurchaseInvoice.BranchId;
                    purchaseinvoice.PurchaseTypeId = LastPurchaseInvoice.PurchaseTypeId;
                    purchaseinvoice.ShippingId = LastPurchaseInvoice.ShippingId;
                }
                purchaseinvoice.BranchId = SessionManager.GetSessionBranch().BranchId;
                purchaseinvoice.DivisionId = SessionManager.GetSessionDivision().DivisionId;
            }
            purchaseinvoice.ShippingTypeList = DropdownManager.GetShippingMasterList(null);
            purchaseinvoice.PurchaseMemoList = DropdownManager.GetPurchaseMemoList();
            purchaseinvoice.LocationList = DropdownManager.GetLocationMasterList(null);            
            purchaseinvoice.PurchaseAccountList = DropdownManager.GetLedgerListForType(null,Constants.LEDGER_TYPE_COMPANY);
            purchaseinvoice.AccountList = DropdownManager.GetLedgerListForType(null,Constants.LEDGER_TYPE_USER);
            purchaseinvoice.PurchaseTypeList = DropdownManager.GetLedgerListForType(null,Constants.LEDGER_TYPE_TAX);
            purchaseinvoice.PaymentTypeList = DropdownManager.GetPaymentTypeList(null);
            purchaseinvoice.DivisionList = DropdownManager.GetDivisionList(null);
            purchaseinvoice.BranchList = DropdownManager.GetBranchList(null);
            purchaseinvoice.LocationList = DropdownManager.GetLocationByBranch(purchaseinvoice.BranchId ?? 0);
            SessionManager.EmptySessionList(Constants.ATTRIBUTE_LIST);
            return purchaseinvoice;
        }

        private void CreateProducts1(int id,int CompanyId)
        {
            List<Product> productsToBeAdded = new List<Product>();
            List<PurchaseInvoiceItem> itemList = db.GetPurchaseInvoiceItems(id);
            if (itemList != null && itemList.Count > 0)
            {
                //Query AttributeLinkingList
                Dictionary<int?, AttributeLinking> itemVsLinking = db.GetAttributesByInvoiceItemId(itemList.Select(x => x.PurchaseInvoiceItemId).ToList());//new Dictionary<int, List<AttributeLinking>>();

                foreach (var item in itemList)
                {
                    if (!item.IsProductCreated)
                    {
                        item.AttributeLinkingList = itemVsLinking.Values.Where(x => x.PurchaseInvoiceItemId == item.PurchaseInvoiceItemId).ToList();

                        if (item.SellInPartial)
                        {
                            if (item.AttributeLinkingList == null || item.AttributeLinkingList.Count == 0)
                            {
                                Product product = new Product(item)
                                {
                                    Quantity = item.Quantity
                                };
                                productsToBeAdded.Add(product);
                            }
                            else
                            {
                                foreach (var attributes in item.AttributeLinkingList)
                                {
                                    Product product = new Product(item)
                                    {
                                        AttributeLinkingId = attributes.AttributeLinkingId,
                                        Quantity = attributes.Quantity
                                    };
                                    productsToBeAdded.Add(product);
                                }
                            }
                        }
                        else
                        {
                            if (item.AttributeLinkingList == null || item.AttributeLinkingList.Count == 0)
                            {
                                for (int i = 0; i < item.Quantity; i++)
                                {
                                    Product product = new Product(item)
                                    {
                                        Quantity = 1
                                    };
                                    productsToBeAdded.Add(product);
                                }

                            }
                            else
                            {
                                foreach (var attributes in item.AttributeLinkingList)
                                {
                                    for (int i = 0; i < attributes.Quantity; i++)
                                    {
                                        Product product = new Product(item)
                                        {
                                            AttributeLinkingId = attributes.AttributeLinkingId,
                                            Quantity = 1
                                        };
                                        productsToBeAdded.Add(product);
                                    }
                                }
                            }
                        }
                    }

                }
                if (productsToBeAdded.Count > 0)
                {
                    db.InsertProducts(productsToBeAdded,CompanyId);
                    db.MarkInvoiceItemsAsProductGenerated(itemList.Select(x => x.PurchaseInvoiceItemId).ToList());
                }
            }
        }

        private void CreateProducts(int id,int CompanyId)
        {
            List<Product> productsToBeAdded = new List<Product>();
            List<PurchaseInvoiceItem> itemList = db.GetPurchaseInvoiceItems(id);
            if (itemList != null && itemList.Count > 0)
            {
                foreach (var item in itemList)
                {
                    if (item.IsProductCreated != true && item.PurchaseInvoiceId > 0)
                    {
                        item.AttributeLinkingList = db.GetAttributeLinkingList(item.PurchaseInvoiceItemId);
                        #region Sale Without Partial
                        if (item.SellInPartial != true)
                        {
                            if (item.AttributeLinkingList != null && item.AttributeLinkingList.Count > 0)
                            {
                                int CheckQuantity = 0;
                                foreach (var AttributeItem in item.AttributeLinkingList)
                                {
                                    for (int i = 0; i < AttributeItem.Quantity; i++)
                                    {
                                        Product product = new Product(item)
                                        {
                                            AttributeLinkingId = AttributeItem.AttributeLinkingId,
                                            SKUMaster = db.GetSKUMasterById(item.SKUMasterId)
                                        };
                                        productsToBeAdded.Add(product);
                                    }
                                    CheckQuantity = CheckQuantity + Convert.ToInt32(AttributeItem.Quantity);
                                }
                                if ((item.Quantity - CheckQuantity) > 0)
                                {
                                    int NewQunatity = Convert.ToInt32(item.Quantity) - CheckQuantity;
                                    for (int i = 0; i < NewQunatity; i++)
                                    {
                                        Product product = new Product(item)
                                        {
                                            AttributeLinkingId = null
                                        };
                                        productsToBeAdded.Add(product);
                                    }
                                }
                            }
                            else
                            {
                                Product product = new Product(item);
                                for (int i = 0; i < item.Quantity; i++)
                                {
                                    product.SKUMaster = db.GetSKUMasterById(item.SKUMasterId);
                                    productsToBeAdded.Add(product);
                                }
                            }
                        }
                        #endregion
                        #region Sale in Partial
                        else
                        {
                            if (item.AttributeLinkingList != null && item.AttributeLinkingList.Count > 0)
                            {
                                decimal CheckQuantity = 0;
                                foreach (var AttributeItem in item.AttributeLinkingList)
                                {
                                    Product product = new Product(item)
                                    {
                                        Quantity = AttributeItem.Quantity
                                    };
                                    var AttributeLinkingId = AttributeItem.AttributeLinkingId;
                                    product.AttributeLinkingId = AttributeLinkingId;
                                    product.SKUMaster = db.GetSKUMasterById(item.SKUMasterId);
                                    productsToBeAdded.Add(product);
                                    CheckQuantity = CheckQuantity + AttributeItem.Quantity;
                                }
                                if ((item.Quantity - CheckQuantity) > 0)
                                {
                                    decimal NewQunatity = item.Quantity - CheckQuantity;
                                    Product product = new Product(item)
                                    {
                                        AttributeLinkingId = null,
                                        Quantity = NewQunatity,
                                        SKUMaster = db.GetSKUMasterById(item.SKUMasterId)
                                    };
                                    productsToBeAdded.Add(product);
                                }
                            }
                            else
                            {
                                Product product = new Product(item)
                                {
                                    SKUMaster = db.GetSKUMasterById(item.SKUMasterId)
                                };
                                productsToBeAdded.Add(product);
                            }
                        }
                        #endregion
                    }
                    else
                    {

                        var SoldItem = db.GetProductsListByInvoiceIdIsSoldTrue(item.PurchaseInvoiceItemId);
                        if (SoldItem.Count == 0)
                        {
                            ProductManager.UpdateProduct(item);
                        }
                    }
                }
            }
            if (productsToBeAdded.Count > 0)
            {
                db.InsertProducts(productsToBeAdded, CompanyId);
                db.MarkInvoiceItemsAsProductGenerated(itemList.Select(x => x.PurchaseInvoiceItemId).ToList());
            }
        }

        public void CalculatePurchaseInvoice(int? id)
        {
            try
            {
                PurchaseInvoice Invoice = db.PurchaseInvoices.Where(x => x.PurchaseInvoiceId == id).FirstOrDefault();
                Invoice.PurchaseInvoiceItemList = db.PurchaseInvoiceItems.Where(x => x.PurchaseInvoiceId == id).ToList();


                Invoice.TotalInvoiceValue = 0;
                foreach (var item in Invoice.PurchaseInvoiceItemList)
                {
                    Invoice.TotalInvoiceValue = Invoice.TotalInvoiceValue + (item.Quantity * item.NetAmount.Value);
                    Invoice.CGST = Invoice.CGST + ((item.CGST ?? 1) * item.Quantity);
                    Invoice.SGST = Invoice.SGST + ((item.SGST ?? 1) * item.Quantity);
                    Invoice.IGST = Invoice.IGST + ((item.IGST ?? 1) * item.Quantity);
                }
                Invoice.TotalTax = Invoice.CGST + Invoice.SGST + Invoice.IGST;
                Invoice.NotmappedInvoiceValue = Invoice.TotalInvoiceValue + (Invoice.Adjustment ?? 0) ;

                if(Invoice.IsLedgerUpdate == false)
                {
                    List<Voucher> VoucherList = db.Vouchers.Where(x => x.PurchaseInvoiceId == Invoice.PurchaseInvoiceId).ToList();
                    foreach (var item in VoucherList)
                    {
                        db.Vouchers.Remove(item);
                    }
                    db.SaveChanges();
                    if (Invoice.PaymentId != 7)
                    {
                        #region SupplierVoucherForCredit  
                        Voucher SupplierVoucherForCredit = db.Vouchers.Where(x => x.LedgerId == Invoice.AccountId && x.PurchaseInvoiceId == Invoice.PurchaseInvoiceId && x.Type == Constants.CR).FirstOrDefault();
                        if (SupplierVoucherForCredit != null)
                        {
                            SupplierVoucherForCredit.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_INVOICE;
                            SupplierVoucherForCredit.Total = Invoice.NotmappedInvoiceValue.Value;
                            db.SaveVauchers(SupplierVoucherForCredit);
                        }
                        else
                        {
                            SupplierVoucherForCredit = new Voucher();
                            SupplierVoucherForCredit.LedgerId = Invoice.AccountId;
                            SupplierVoucherForCredit.PurchaseInvoiceId = Invoice.PurchaseInvoiceId;
                            SupplierVoucherForCredit.Total = Invoice.NotmappedInvoiceValue.Value;
                            SupplierVoucherForCredit.Type = Constants.CR;
                            SupplierVoucherForCredit.EntryDate = Invoice.EntryDate ?? DateTime.Now;
                            SupplierVoucherForCredit.InvoiceNumber = Invoice.PurchaseInvoiceNo;
                            SupplierVoucherForCredit.Particulars = "Purchase Invoice Number :" + Invoice.PurchaseInvoiceNo;
                            SupplierVoucherForCredit.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_INVOICE;
                            db.SaveVauchers(SupplierVoucherForCredit);
                        }
                        #endregion

                        #region PurchaseAccountVoucherForDebit  
                        Voucher PurchaseAccountVoucherForDebit = db.Vouchers.Where(x => x.LedgerId == Invoice.PurchaseAccountId && x.PurchaseInvoiceId == Invoice.PurchaseInvoiceId && x.Type == Constants.DR).FirstOrDefault();
                        if (PurchaseAccountVoucherForDebit != null)
                        {
                            PurchaseAccountVoucherForDebit.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_INVOICE;
                            PurchaseAccountVoucherForDebit.Total = Invoice.NotmappedInvoiceValue.Value;
                            db.SaveVauchers(PurchaseAccountVoucherForDebit);
                        }
                        else
                        {
                            PurchaseAccountVoucherForDebit = new Voucher();
                            PurchaseAccountVoucherForDebit.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_INVOICE;
                            PurchaseAccountVoucherForDebit.LedgerId = Invoice.PurchaseAccountId;
                            PurchaseAccountVoucherForDebit.PurchaseInvoiceId = Invoice.PurchaseInvoiceId;
                            PurchaseAccountVoucherForDebit.Total = Invoice.NotmappedInvoiceValue.Value;
                            PurchaseAccountVoucherForDebit.Type = Constants.DR;
                            PurchaseAccountVoucherForDebit.EntryDate = Invoice.EntryDate ?? DateTime.Now;
                            PurchaseAccountVoucherForDebit.InvoiceNumber = Invoice.PurchaseInvoiceNo;
                            PurchaseAccountVoucherForDebit.Particulars = "Purchase Invoice Number :" + Invoice.PurchaseInvoiceNo;
                            db.SaveVauchers(PurchaseAccountVoucherForDebit);
                        }
                        #endregion

                        #region Supplier Voucher For Debit 
                        Voucher SupplierVoucherForDebit = db.Vouchers.Where(x => x.LedgerId == Invoice.AccountId && x.PurchaseInvoiceId == Invoice.PurchaseInvoiceId && x.Type == Constants.DR).FirstOrDefault();
                        if (SupplierVoucherForDebit != null)
                        {
                            SupplierVoucherForDebit.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_INVOICE;
                            SupplierVoucherForDebit.Total = Invoice.NotmappedInvoiceValue.Value;
                            db.SaveVauchers(SupplierVoucherForDebit);
                        }
                        else
                        {
                            SupplierVoucherForDebit = new Voucher();
                            SupplierVoucherForDebit.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_INVOICE;
                            SupplierVoucherForDebit.LedgerId = Invoice.AccountId;
                            SupplierVoucherForDebit.PurchaseInvoiceId = Invoice.PurchaseInvoiceId;
                            SupplierVoucherForDebit.Total = Invoice.NotmappedInvoiceValue.Value;
                            SupplierVoucherForDebit.Type = Constants.DR;
                            SupplierVoucherForDebit.EntryDate = Invoice.EntryDate ?? DateTime.Now;
                            SupplierVoucherForDebit.InvoiceNumber = Invoice.PurchaseInvoiceNo;
                            SupplierVoucherForDebit.Particulars = "Purchase Invoice Number :" + Invoice.PurchaseInvoiceNo;
                            db.SaveVauchers(SupplierVoucherForDebit);
                        }
                        #endregion

                        #region Purchase Account Voucher For Credit
                        Voucher PurchaseAccountVoucherCredit = db.Vouchers.Where(x => x.LedgerId == Invoice.PurchaseAccountId && x.PurchaseInvoiceId == Invoice.PurchaseInvoiceId && x.Type == Constants.CR).FirstOrDefault();
                        if (PurchaseAccountVoucherCredit != null)
                        {
                            PurchaseAccountVoucherCredit.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_INVOICE;
                            PurchaseAccountVoucherCredit.Total = Invoice.NotmappedInvoiceValue.Value;
                            db.SaveVauchers(PurchaseAccountVoucherCredit);
                        }
                        else
                        {
                            PurchaseAccountVoucherCredit = new Voucher();
                            PurchaseAccountVoucherCredit.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_INVOICE;
                            PurchaseAccountVoucherCredit.EntryDate = Invoice.EntryDate ?? DateTime.Now;
                            PurchaseAccountVoucherCredit.LedgerId = Invoice.PurchaseAccountId;
                            PurchaseAccountVoucherCredit.PurchaseInvoiceId = Invoice.PurchaseInvoiceId;
                            PurchaseAccountVoucherCredit.Total = Invoice.NotmappedInvoiceValue.Value;
                            PurchaseAccountVoucherCredit.Type = Constants.CR;
                            PurchaseAccountVoucherCredit.InvoiceNumber = Invoice.PurchaseInvoiceNo;
                            PurchaseAccountVoucherCredit.Particulars = "Purchase Invoice Number :" + Invoice.PurchaseInvoiceNo;
                            db.SaveVauchers(PurchaseAccountVoucherCredit);
                        }
                        #endregion                        
                    }
                    else
                    {
                        #region Supplier Voucher For Debit 
                        Voucher SupplierVoucherDebit = db.Vouchers.Where(x => x.LedgerId == Invoice.AccountId && x.PurchaseInvoiceId == Invoice.PurchaseInvoiceId && x.Type == Constants.CR).FirstOrDefault();
                        if (SupplierVoucherDebit != null)
                        {
                            SupplierVoucherDebit.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_INVOICE;
                            SupplierVoucherDebit.Total = Invoice.NotmappedInvoiceValue.Value;
                            db.SaveVauchers(SupplierVoucherDebit);
                        }
                        else
                        {
                            SupplierVoucherDebit = new Voucher();
                            SupplierVoucherDebit.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_INVOICE;
                            SupplierVoucherDebit.LedgerId = Invoice.AccountId;
                            SupplierVoucherDebit.PurchaseInvoiceId = Invoice.PurchaseInvoiceId;
                            SupplierVoucherDebit.Total = Invoice.NotmappedInvoiceValue.Value;
                            SupplierVoucherDebit.Type = Constants.CR;
                            SupplierVoucherDebit.EntryDate = Invoice.EntryDate ?? DateTime.Now;
                            SupplierVoucherDebit.InvoiceNumber = Invoice.PurchaseInvoiceNo;
                            SupplierVoucherDebit.Particulars = "Purchase Invoice Number :" + Invoice.PurchaseInvoiceNo;
                            db.SaveVauchers(SupplierVoucherDebit);
                        }
                        #endregion
                    }

                    #region TaxtVoucher  
                    Voucher TaxtVoucher = db.Vouchers.Where(x => x.LedgerId == Invoice.PurchaseTypeId && x.PurchaseInvoiceId == Invoice.PurchaseInvoiceId).FirstOrDefault();
                    if (TaxtVoucher != null)
                    {
                        TaxtVoucher.SGST = Invoice.SGST;
                        TaxtVoucher.CGST = Invoice.CGST;
                        TaxtVoucher.IGST = Invoice.IGST;
                        TaxtVoucher.Total = Invoice.TotalTax ?? 0;
                        TaxtVoucher.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_INVOICE;
                        db.SaveVauchers(TaxtVoucher);
                    }
                    else
                    {
                        TaxtVoucher = new Voucher();
                        TaxtVoucher.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_INVOICE;
                        TaxtVoucher.LedgerId = Invoice.PurchaseTypeId;
                        TaxtVoucher.PurchaseInvoiceId = Invoice.PurchaseInvoiceId;
                        TaxtVoucher.SGST = Invoice.SGST;
                        TaxtVoucher.CGST = Invoice.CGST;
                        TaxtVoucher.IGST = Invoice.IGST ;
                        TaxtVoucher.Total = Invoice.TotalTax ?? 0;
                        TaxtVoucher.Type = Constants.DR;
                        TaxtVoucher.EntryDate = Invoice.EntryDate ?? DateTime.Now;
                        TaxtVoucher.InvoiceNumber = Invoice.PurchaseInvoiceNo;
                        TaxtVoucher.Particulars = "Purchase Invoice Number :" + Invoice.PurchaseInvoiceNo;
                        db.SaveVauchers(TaxtVoucher);
                    }
                    #endregion
                }

                #region Discount Voucher
                if(Invoice.Adjustment.HasValue && Invoice.Adjustment != 0)
                {
                    Ledger ledger = db.Ledgers.Where(x => x.Name == Constants.LEDGER_TYPE_DISCOUNT).FirstOrDefault();
                    if (ledger != null)
                    {
                        Voucher DiscountVoucher = db.Vouchers.Where(x => x.LedgerId == ledger.LedgerId && x.PurchaseInvoiceId == Invoice.PurchaseInvoiceId).FirstOrDefault();
                        if(DiscountVoucher != null)
                        {
                            DiscountVoucher.Total = Math.Abs(Invoice.Adjustment.Value);
                            if (Invoice.Adjustment.Value > 0)
                            {
                                DiscountVoucher.Type = Constants.CR;
                            }
                            else
                            {
                                DiscountVoucher.Type = Constants.DR;
                            }
                            DiscountVoucher.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_INVOICE;
                            db.SaveVauchers(DiscountVoucher);
                        }
                        else
                        {
                            DiscountVoucher = new Voucher();
                            DiscountVoucher.LedgerId = ledger.LedgerId;
                            DiscountVoucher.PurchaseInvoiceId = Invoice.PurchaseInvoiceId;
                            DiscountVoucher.Total = Math.Abs(Invoice.Adjustment.Value);
                            if (Invoice.Adjustment.Value > 0)
                            {
                                DiscountVoucher.Type = Constants.CR;
                            }
                            else
                            {
                                DiscountVoucher.Type = Constants.DR;
                            }
                            DiscountVoucher.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_INVOICE;
                            DiscountVoucher.EntryDate = Invoice.EntryDate ?? DateTime.Now;
                            DiscountVoucher.InvoiceNumber = Invoice.PurchaseInvoiceNo;
                            DiscountVoucher.Particulars = "Purchase Invoice Number :" + Invoice.PurchaseInvoiceNo;
                            db.SaveVauchers(DiscountVoucher);
                        }
                    }
                }
                #endregion

                #region PurchaseLedger  
                Ledger PurchaseLedger = db.Ledgers.Where(x => x.Name == Constants.LEDGER_TYPE_PURCHASE_RETURN_LEDGER).FirstOrDefault();
                if (PurchaseLedger != null)
                {
                    Voucher PurchaseLedgerVoucher = db.Vouchers.Where(x => x.LedgerId == PurchaseLedger.LedgerId && x.PurchaseInvoiceId == Invoice.PurchaseInvoiceId && x.Type == Constants.CR).FirstOrDefault();
                    if (PurchaseLedgerVoucher != null)
                    {
                        PurchaseLedgerVoucher.Total = Invoice.NotmappedInvoiceValue.Value;
                        db.SaveVauchers(PurchaseLedgerVoucher);
                    }
                    else
                    {
                        PurchaseLedgerVoucher = new Voucher();
                        PurchaseLedgerVoucher.LedgerId = PurchaseLedger.LedgerId;
                        PurchaseLedgerVoucher.PurchaseInvoiceId = Invoice.PurchaseInvoiceId;
                        PurchaseLedgerVoucher.Total = Invoice.NotmappedInvoiceValue.Value; 
                        PurchaseLedgerVoucher.Type = Constants.CR;
                        PurchaseLedgerVoucher.InvoiceNumber = Invoice.PurchaseInvoiceNo;
                        PurchaseLedgerVoucher.EntryDate = Invoice.InvoicingDate ?? DateTime.Now;
                        PurchaseLedgerVoucher.Particulars = "Purchase Invoice Number :" + Invoice.PurchaseInvoiceNo;
                        PurchaseLedgerVoucher.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_INVOICE;
                        db.SaveVauchers(PurchaseLedgerVoucher);
                    }
                }
                #endregion

                db.Entry(Invoice).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
        }

        public void ChangePurchaseOrderStatus(int? id)
        {
            try
            {
                PurchaseInvoice Invoice = db.PurchaseInvoices.Where(x => x.PurchaseInvoiceId == id).FirstOrDefault();
                Invoice.PurchaseInvoiceItemList = db.PurchaseInvoiceItems.Where(x => x.PurchaseInvoiceId == id).ToList();
                if (Invoice.PurchaseOrderId > 0)
                {
                    PurchaseOrder PurchaseOrder = db.PurchaseOrders.Where(x => x.PurchaseOrderId == Invoice.PurchaseOrderId).FirstOrDefault();
                    PurchaseOrder.PurchaseOrderItemList = db.PurchaseOrderItems.Where(x => x.PurchaseOrderId == Invoice.PurchaseOrderId).ToList();
                    var ErrorText = 0;
                    foreach (var TextItem in PurchaseOrder.PurchaseOrderItemList)
                    {
                        if (TextItem.Quantity == Invoice.PurchaseInvoiceItemList.Where(x => x.SKUMasterId == TextItem.SKUMasterId).Select(x => x.Quantity).FirstOrDefault())
                        {

                        }
                        else
                        {
                            ErrorText = ErrorText + 1;
                        }
                    }
                    if (ErrorText == 0)
                    {
                        PurchaseOrder.OrderStatus = Enums.PurchaseOrderStatus.Closed;
                        db.Entry(PurchaseOrder).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else
                    {
                        PurchaseOrder.OrderStatus = Enums.PurchaseOrderStatus.Closed;
                        db.Entry(PurchaseOrder).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                }
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
        }
    }

}
