﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLayer.Models;
using System.Web.Mvc;
using System.Data.Entity;

namespace DataLayer.Utils
{
    public class PurchaseOrderHandler
    {
        DbConnector db = new DbConnector();

        public PurchaseOrder GetPurchaseOrderForCreateEdit(int? id)
        {
            PurchaseOrder purchaseOrder = null;
            if (id.HasValue)
            {
                purchaseOrder = db.PurchaseOrders.Include("PurchaseOrderItemList").Where(x => x.PurchaseOrderId == id).FirstOrDefault();


                if (purchaseOrder == null)
                {
                    purchaseOrder = new PurchaseOrder();
                }
                foreach (var item in purchaseOrder.PurchaseOrderItemList)
                {
                    item.SKUMaster = db.SKUMasters.Where(x => x.SKUMasterId == item.SKUMasterId).FirstOrDefault();
                    item.GST = db.GSTs.Where(x => x.GSTId == item.GSTId).FirstOrDefault();
                    purchaseOrder.ItemTotalQuantity = purchaseOrder.ItemTotalQuantity + item.Quantity;
                    purchaseOrder.ItemTotalBasicPrice = purchaseOrder.ItemTotalBasicPrice + item.Rate;
                    purchaseOrder.ItemTotalMRP = purchaseOrder.ItemTotalMRP + item.MRP;
                    purchaseOrder.ItemTotalTax = purchaseOrder.ItemTotalTax + (item.SGST.Value + item.CGST.Value + item.IGST.Value);
                    purchaseOrder.ItemPurchasePrice = purchaseOrder.ItemPurchasePrice + item.NetAmount.Value;
                    purchaseOrder.ItemFinalTotal = purchaseOrder.ItemFinalTotal + (item.Quantity * item.NetAmount.Value);
                    purchaseOrder.FinalTotalAdj = purchaseOrder.ItemFinalTotal;
                }
                if (purchaseOrder.Adjustment == null)
                {
                    purchaseOrder.Adjustment = 0;
                }
                purchaseOrder.TotalInvoiceValue = purchaseOrder.TotalInvoiceValue + purchaseOrder.Adjustment.Value;
            }
            else
            {
                purchaseOrder = new PurchaseOrder
                {
                    OrderStatus = Enums.PurchaseOrderStatus.Open
                };
                purchaseOrder.BranchId = SessionManager.GetSessionBranch().BranchId;
                purchaseOrder.DivisionId = SessionManager.GetSessionDivision().DivisionId;
            }
            purchaseOrder.ShippingTypeList = DropdownManager.GetShippingMasterList(null);
            purchaseOrder.PurchaseMemoList = DropdownManager.GetPurchaseMemoList();

            purchaseOrder.PurchaseAccountList = DropdownManager.GetLedgerListForType(null, Constants.LEDGER_TYPE_COMPANY);
            purchaseOrder.AccountList = DropdownManager.GetLedgerListForType(null, Constants.LEDGER_TYPE_USER);
            purchaseOrder.PurchaseTypeList = DropdownManager.GetLedgerListForType(null,Constants.LEDGER_TYPE_TAX);

            //purchaseOrder.ContactCategoryList = DropdownManager.GetContactCategoryList(null);
            purchaseOrder.PaymentTypeList = DropdownManager.GetPaymentTypeList(null);

            purchaseOrder.DivisionList = DropdownManager.GetDivisionList(null);
            purchaseOrder.BranchList = DropdownManager.GetBranchList(null);
            purchaseOrder.LocationList = DropdownManager.GetLocationByBranch(purchaseOrder.BranchId);

            return purchaseOrder;
        }

        public PurchaseOrderItem SavePurchaseOrderItem(PurchaseOrderItem purchaseOrderItem, string btn)
        {
            db.SavePurchaseOrderItem(purchaseOrderItem);
            purchaseOrderItem.AttributeLinkingList = SessionManager.GetAttributeList();
            if (purchaseOrderItem.AttributeLinkingList != null)
            {
                foreach (var item in purchaseOrderItem.AttributeLinkingList)
                {
                    if (item.PurchaseOrderItemId == null || item.PurchaseOrderItemId  == 0)
                    {
                        item.PurchaseOrderItemId = purchaseOrderItem.PurchaseOrderItemId;
                        item.AttributeLinkingId = 0;
                    }
                }
                db.SaveAttributeLinking(purchaseOrderItem.AttributeLinkingList);
                SessionManager.EmptySessionList(Constants.ATTRIBUTE_LIST);
            }

            CalculatePurchaseOrder(purchaseOrderItem.PurchaseOrderId);
            return purchaseOrderItem;
        }

        private void CalculatePurchaseOrder(int PurchaseOrderId)
        {
            try
            {
                PurchaseOrder Order = db.PurchaseOrders.Where(x => x.PurchaseOrderId == PurchaseOrderId).FirstOrDefault();
                Order.PurchaseOrderItemList = db.PurchaseOrderItems.Where(x => x.PurchaseOrderId == PurchaseOrderId).ToList();
                Order.TotalInvoiceValue = 0;
                foreach (var item in Order.PurchaseOrderItemList)
                {
                    Order.TotalInvoiceValue = Order.TotalInvoiceValue + (item.Quantity * item.NetAmount.Value);
                }
                db.Entry(Order).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
        }

        public PurchaseOrderItem GetOrderItemForCreateEdit(int orderId, int? orderItemId, string IsClone)
        {
            PurchaseOrderItem item = null;

            if (orderItemId.HasValue)
            {
                item = db.PurchaseOrderItems.Include(x => x.SKUMaster).Where(x => x.PurchaseOrderItemId == orderItemId).FirstOrDefault();
                item.AttributeLinkingList = db.AttributeLinkings.Where(x => x.PurchaseOrderItemId == orderItemId).ToList();
                if (item.AttributeLinkingList != null)
                {
                    foreach (var i in item.AttributeLinkingList)
                    {
                        i.NameList = i.AttributeSetIds.Split(',').ToList();
                        i.ValuesList = i.Value.Split(',').ToList();
                    }
                }
                //item.DiscountIDTEmp = item.DiscountId;
                Unit unit = db.GetUnitParents(item.UnitId);
                item.UnitList = DropdownManager.GetUnitConversionList(unit.UnitId);
            }
            else
            {
                item = new PurchaseOrderItem();
                item.PurchaseOrderId = orderId;
                item.DiscountsAccessible = "";
                item.UnitList = DropdownManager.GetUnitMasterList(item.UnitId);
            }
            item.CategoryList = DropdownManager.GetCategoryList(item.CategoryId);
            item.SubcategoryList = DropdownManager.GetSubCategoryList(item.SubCategoryId);
            item.SKUList = DropdownManager.GetSKUList(item.SKUMasterId);
            item.HSNList = DropdownManager.GetHSNList(item.HSNId);
            item.BrandList = DropdownManager.GetBrandList(item.BrandId);
            item.LocationList = DropdownManager.GetLocationMasterList(item.LocationId);
            item.DiscountCodeList = DropdownManager.GetDiscountsAccessibleList(item.DiscountsAccessible,null);
            item.BrandList = DropdownManager.GetBrandList(item.BrandId);
            item.GSTList = DropdownManager.GetGSTList(item.GSTId);
            ///To Do Ashvini Start
            Dictionary<AttributeSet, List<SelectListItem>> AttributeDictionayList = null;
            item.AttributeSetListNew = db.GetAttributeSets();

            AttributeDictionayList = new Dictionary<AttributeSet, List<SelectListItem>>();
            for (int i = 0; i < item.AttributeSetListNew.Count; i++)
            {
                int Id = item.AttributeSetListNew[i].AttributeSetId;
                //item.AttributeLinkingList = db.getAttributeLinkingList(Id);
                item.AttributeSetListNew[i].AttributeValueList = DropdownManager.GetAttributeValueList(Id);
                if (AttributeDictionayList == null)
                {
                    //  AttributeDictionayList.Add(0,)

                }
                AttributeDictionayList.Add(item.AttributeSetListNew[i], item.AttributeSetListNew[i].AttributeValueList);
            }
            item.AttributeDictionayList = AttributeDictionayList;
            SessionManager.EmptySessionList(Constants.ATTRIBUTE_LIST);
            if (!string.IsNullOrEmpty(IsClone))
            {
                item.IsClone = IsClone;
                item.PurchaseOrderItemId = 0;
                foreach (var AttributeItem in item.AttributeLinkingList)
                {
                    AttributeItem.PurchaseInvoiceItemId = 0;
                    SessionManager.SetAttribute(AttributeItem);
                }
                item.AttributeLinkingList = SessionManager.GetAttributeList();
            }
            return item;
        }

        public bool DeletePurchaseOrder(PurchaseOrder purchaseOrder)
        {
            bool result = false;
            PurchaseOrderDbUtil db = new PurchaseOrderDbUtil();
            List<int> purchaseOrderItemIds = purchaseOrder.PurchaseOrderItemList.Select(x => x.PurchaseOrderItemId).ToList();
            List<int> attributesToBeDeleted = db.GetProductAttributes(purchaseOrderItemIds);
            //Query Ledger

            //Deletion Process starts here

            //Ledger delete

            //Attributes Delete
            List<AttributeLinking> attributeLinkings = new List<AttributeLinking>();
            List<PurchaseOrderItem> purchaseOrderItemItems = new List<PurchaseOrderItem>();

            foreach (var item in purchaseOrderItemIds)
            {
                PurchaseOrderItem obj = new PurchaseOrderItem() { PurchaseOrderItemId = item };
                purchaseOrderItemItems.Add(obj);
            }
            foreach (var item in attributesToBeDeleted)
            {
                AttributeLinking obj = new AttributeLinking() { AttributeLinkingId = item };
                attributeLinkings.Add(obj);
            }

            if (db.DeletePurchaseOrder(purchaseOrderItemItems, purchaseOrder, attributeLinkings))
            {
                result = true;
            }
            return result;
        }
    }
}
