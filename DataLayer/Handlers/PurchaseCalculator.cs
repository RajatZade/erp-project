﻿using DataLayer.Models;
using DataLayer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Utils
{
    public class PurchaseCalculator
    {       
        public static Ledger GetLedgerOeningBalance(int id)
        {
            DbConnector db = new DbConnector();
            LedgerReportViewModel Model = new LedgerReportViewModel();
            Model.Ledger = db.Ledgers.Where(x => x.LedgerId == id).FirstOrDefault();
            if (Model.Ledger != null)
            {
                Model.Ledger.AccountCategory = db.ContactCategorys.Where(x => x.AccountCategoryId == Model.Ledger.AccountCategoryId).FirstOrDefault();
                if (Model.Ledger.AccountSubCategoryId > 0)
                {
                    Model.Ledger.AccountSubCategory = db.ContactSubCategorys.Where(x => x.AccountSubCategoryId == Model.Ledger.AccountSubCategoryId).FirstOrDefault();
                }
            }
            Model.VoucherList = db.Vouchers.Where(x => x.LedgerId == id && x.CompanyId == db.CompanyId).OrderBy(x => x.EntryDate).ToList();
            if (Model.VoucherList != null)
            {
                Model.StartingDate = Model.VoucherList.Select(x => x.EntryDate).FirstOrDefault();
                Model.LastDate = Model.VoucherList.Select(x => x.EntryDate).LastOrDefault();
                for (int i = 0; i < Model.VoucherList.Count; i++)
                {
                    if (Model.VoucherList[i].Type == Constants.DR)
                    {
                        Model.VoucherList[i].OpeningBalance = Model.LedgerOpeningBalanceForVoucher;
                        Model.VoucherList[i].ClosingBalance = Model.VoucherList[i].OpeningBalance - Model.VoucherList[i].Total;
                        Model.LedgerOpeningBalanceForVoucher = Model.VoucherList[i].ClosingBalance;
                        Model.TotalDebitAmmount = Model.TotalDebitAmmount + Model.VoucherList[i].Total;
                    }
                    else
                    {
                        Model.VoucherList[i].OpeningBalance = Model.LedgerOpeningBalanceForVoucher;
                        Model.VoucherList[i].ClosingBalance = Model.VoucherList[i].OpeningBalance + Model.VoucherList[i].Total;
                        Model.LedgerOpeningBalanceForVoucher = Model.VoucherList[i].ClosingBalance;
                        Model.TotalCreditAmmount = Model.TotalCreditAmmount + Model.VoucherList[i].Total;
                    }
                }
            }
            Ledger ledger = Model.Ledger;
            ledger.StartingBalance = Model.VoucherList.Where(x => x.VoucherFor == Constants.OPENING_BALANCE).Select(x => x.ClosingBalance).FirstOrDefault();
            ledger.CurrentBalance = Model.VoucherList.Select(x => x.ClosingBalance).LastOrDefault();
            return ledger;
        }

        public static Ledger GetLedgerOeningBalance(ReportViewModel model)
        {
            DbConnector db = new DbConnector();
            LedgerReportViewModel Model = new LedgerReportViewModel();
            Model.Ledger = db.Ledgers.Where(x => x.LedgerId == model.LedgerId).FirstOrDefault();
            if(model.FromDate.HasValue)
            {
                Model.VoucherList = db.Vouchers.Where(x => x.EntryDate >= model.FromDate && x.LedgerId == model.LedgerId && x.CompanyId == db.CompanyId).OrderBy(x => x.EntryDate).ToList();
            }
            else
            {
                Model.VoucherList = db.Vouchers.Where(x => x.LedgerId == model.LedgerId && x.CompanyId == db.CompanyId).OrderBy(x => x.EntryDate).ToList();
            }

            if(model.ToDate.HasValue)
            {
                Model.VoucherList = Model.VoucherList.Where(x => x.EntryDate <= model.ToDate).OrderBy(x => x.EntryDate).ToList();
            }

            if (Model.VoucherList != null)
            {
                for (int i = 0; i < Model.VoucherList.Count; i++)
                {
                    if (Model.VoucherList[i].Type == Constants.DR)
                    {
                        Model.VoucherList[i].OpeningBalance = Model.LedgerOpeningBalanceForVoucher;
                        Model.VoucherList[i].ClosingBalance = Model.VoucherList[i].OpeningBalance - Model.VoucherList[i].Total;
                        Model.LedgerOpeningBalanceForVoucher = Model.VoucherList[i].ClosingBalance;
                        Model.TotalDebitAmmount = Model.TotalDebitAmmount + Model.VoucherList[i].Total;
                    }
                    else
                    {
                        Model.VoucherList[i].OpeningBalance = Model.LedgerOpeningBalanceForVoucher;
                        Model.VoucherList[i].ClosingBalance = Model.VoucherList[i].OpeningBalance + Model.VoucherList[i].Total;
                        Model.LedgerOpeningBalanceForVoucher = Model.VoucherList[i].ClosingBalance;
                        Model.TotalCreditAmmount = Model.TotalCreditAmmount + Model.VoucherList[i].Total;
                    }
                    //int VoucherTypeId = Model.VoucherList[i].VoucherTypeId;
                    //Model.VoucherList[i].VoucherType = db.VoucherTypes.Where(x => x.VoucherTypeId == VoucherTypeId).FirstOrDefault();
                }
            }
            Ledger ledger = Model.Ledger;
            ledger.StartingBalance = Model.VoucherList.Where(x => x.VoucherFor == Constants.OPENING_BALANCE).Select(x => x.ClosingBalance).FirstOrDefault();
            ledger.CurrentBalance = Model.VoucherList.Select(x => x.ClosingBalance).LastOrDefault();
            ledger.OpeningBalanceType = Model.VoucherList.Where(x => x.VoucherFor == Constants.OPENING_BALANCE).Select(x => x.Type).FirstOrDefault();
            return ledger;
        }
    }
}
