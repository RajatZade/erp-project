﻿using BarcodeLib;
using System;
using System.Drawing;
using System.IO;

namespace BarCode.Models
{
    public class barcodecs
    {
        //public Byte[] getBarcodeImage(string barcode)
        //{
        //    try
        //    {
        //        BarCode39 _barcode = new BarCode39();
        //        int barSize = 16;
        //        string fontFile = HttpContext.Current.Server.MapPath("~/fonts/FREE3OF9.TTF");
        //        return (_barcode.Code39(barcode, barSize, true, "", fontFile));
        //    }
        //    catch (Exception ex)
        //    {
        //        //ErrorLog.WriteErrorLog("Barcode", ex.ToString(), ex.Message);
        //    }
        //    return null;
        //}

        //public string GenerateBarcodeString(string barcodeString)
        //{

        //    //BarcodeLib.Barcode barcode = new BarcodeLib.Barcode()
        //    //{
        //    //    IncludeLabel = false,
        //    //    Alignment = AlignmentPositions.CENTER,
        //    //    Width = 300,
        //    //    Height = 100,
        //    //    RotateFlipType = RotateFlipType.RotateNoneFlipNone,
        //    //    BackColor = Color.White,
        //    //    ForeColor = Color.Black,
        //    //};

        //    //Image img = barcode.Encode(TYPE.CODE128B, barcodeString);
        //    //Bitmap bitmap = new Bitmap(img);

        //    //using (MemoryStream ms = new MemoryStream())
        //    //{
        //    //    bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
        //    //    byte[] byteImage = ms.ToArray();

        //    //    Convert.ToBase64String(byteImage);
        //    //    return "data:image/png;base64," + Convert.ToBase64String(byteImage);
        //    //}
        //    //}

        //    //string output = "";
        //    //try
        //    //{
        //    //    BarCode39 _barcode = new BarCode39();
        //    //    int barSize = 16;
        //    //    string fontFile = HttpContext.Current.Server.MapPath("~/fonts/FREE3OF9.TTF");
        //    //    output = _barcode.Code39(barcode, barSize, true, "", fontFile);
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    //ErrorLog.WriteErrorLog("Barcode", ex.ToString(), ex.Message);
        //    //}
        //    //return output;
        //}
        public string GenerateBarcodeString(string barcodeString)
        {

            BarcodeLib.Barcode barcode = new BarcodeLib.Barcode()
            {
                IncludeLabel = false,
                Alignment = AlignmentPositions.CENTER,
                Width = 300,
                Height = 100,
                RotateFlipType = RotateFlipType.RotateNoneFlipNone,
                BackColor = Color.White,
                ForeColor = Color.Black,
            };

            Image img = barcode.Encode(TYPE.CODE128B, barcodeString);
            Bitmap bitmap = new Bitmap(img);

            using (MemoryStream ms = new MemoryStream())
            {
                bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                byte[] byteImage = ms.ToArray();

                Convert.ToBase64String(byteImage);
                return "data:image/png;base64," + Convert.ToBase64String(byteImage);
            }
            //}

            //string output = "";
            //try
            //{
            //    BarCode39 _barcode = new BarCode39();
            //    int barSize = 16;
            //    string fontFile = HttpContext.Current.Server.MapPath("~/fonts/FREE3OF9.TTF");
            //    output = _barcode.Code39(barcode, barSize, true, "", fontFile);
            //}
            //catch (Exception ex)
            //{
            //    //ErrorLog.WriteErrorLog("Barcode", ex.ToString(), ex.Message);
            //}
            //return output;
        }
        public string GenerateBarcodeString1(string barCode)
        {
            using (Bitmap bitMap = new Bitmap(barCode.Length * 40, 80))
            {
                using (Graphics graphics = Graphics.FromImage(bitMap))
                {
                    Font oFont = new Font("Free 3 of 9", 60, FontStyle.Regular, GraphicsUnit.Point);
                    //Font oFont = new Font(dfg, 16);
                    PointF point = new PointF(2f, 2f);
                    SolidBrush blackBrush = new SolidBrush(Color.Black);
                    SolidBrush whiteBrush = new SolidBrush(Color.White);
                    graphics.FillRectangle(whiteBrush, 0, 0, bitMap.Width, bitMap.Height);
                    graphics.DrawString("*" + barCode + "*", oFont, blackBrush, point);
                }
                using (MemoryStream ms = new MemoryStream())
                {
                    bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                    byte[] byteImage = ms.ToArray();

                    Convert.ToBase64String(byteImage);
                    return "data:image/png;base64," + Convert.ToBase64String(byteImage);
                }
            }


        }
    }
}
