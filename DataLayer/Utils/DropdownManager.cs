﻿using DataLayer.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using DataLayer;
using System;
using static DataLayer.Enums;
using System.Linq;

namespace DataLayer.Utils
{
    public class DropdownManager
    {

        #region CategoryList
        public static List<SelectListItem> GetCategoryList(int? contactTypeId)
        {
            DbConnector db = new DbConnector();
            List<Category> categoryList = db.GetCategoryLists();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (categoryList != null)
            {
                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });
                if (categoryList != null && categoryList.Count > 0)
                {
                    foreach (var item in categoryList)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.CategoryId.ToString(), Text = item.Name };
                        if (item.CategoryId == contactTypeId)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return (itemList);
        }

        public static List<SelectListItem> GetLocationByBranch(int BranchId)
        {
            DbConnector db = new DbConnector();
            List<Location> LocationList = db.GetLocationListByBranch(BranchId);
            List<SelectListItem> itemList = new List<SelectListItem>();
            itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });
            if (LocationList != null && LocationList.Count > 0)
            {
                foreach (var item in LocationList)
                {
                    SelectListItem selectListItem = new SelectListItem() { Text = item.LocationName, Value = item.LocationId.ToString() };
                    itemList.Add(selectListItem);
                }
            }
            return itemList;
        }

        public static List<SelectListItem> GetBranchByDivision(int DivisionId)
        {
            DbConnector db = new DbConnector();
            List<Branch> LocationList = db.GetBrandListByDivision(DivisionId);
            List<SelectListItem> itemList = new List<SelectListItem>();
            itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });
            if (LocationList != null && LocationList.Count > 0)
            {
                foreach (var item in LocationList)
                {
                    SelectListItem selectListItem = new SelectListItem() { Text = item.BranchName, Value = item.BranchId.ToString() };
                    itemList.Add(selectListItem);
                }
            }
            return itemList;
        }

        public static List<SelectListItem> GetBankAccountsList(bool isNewAction, int LedgerBankId)
        {
            DbConnector db = new DbConnector();
            List<Ledger> banksList = db.GetBankAccounts();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (banksList != null && banksList.Count > 0)
            {
                if (isNewAction)
                    itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "0" });

                foreach (var item in banksList)
                {
                    SelectListItem selectListItem = new SelectListItem() { Text = item.Name , Value = item.LedgerId.ToString() };
                    if (item.LedgerId == LedgerBankId)
                    {
                        selectListItem.Selected = true;
                    }
                    itemList.Add(selectListItem);
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }

        public static List<SelectListItem> GetBudgetingList()
        {
            DbConnector db = new DbConnector();
            List<BudgetSetup> BudgetSetupList = db.BudgetSetups.ToList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (BudgetSetupList != null && BudgetSetupList.Count > 0)
            {
                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "0" });
                foreach (var item in BudgetSetupList)
                {
                    SelectListItem selectListItem = new SelectListItem() { Text = item.Name, Value = item.BudgetSetupId.ToString() };
                    itemList.Add(selectListItem);
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }

        public static IList<SelectListItem> GetLedgerSelectList()
        {
            DbConnector db = new DbConnector();
            List<Ledger> LedgerList = db.GetLedgerList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (LedgerList != null && LedgerList.Count > 0)
            {
                foreach (var item in LedgerList)
                {
                    SelectListItem selectListItem = new SelectListItem() { Text = item.Name, Value = item.LedgerId.ToString() };
                    itemList.Add(selectListItem);
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }
        #endregion

        #region SubcategoryList
        public static List<SelectListItem> GetSubCategoryList(int? contactTypeId)
        {
            DbConnector db = new DbConnector();
            List<SubCategory> subCategoryList = db.GetSubCategoryList(contactTypeId);
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (subCategoryList != null)
            {

                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });

                if (subCategoryList != null && subCategoryList.Count > 0)
                {
                    foreach (var item in subCategoryList)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.SubCategoryId.ToString(), Text = item.Name };
                        if (item.SubCategoryId == contactTypeId)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }
        #endregion

        #region UnitMasterList
        public static List<SelectListItem> GetUnitMasterList(int? contactTypeId)
        {
            DbConnector db = new DbConnector();
            List<Unit> UnitMasterList = db.GetUnitMasterList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (UnitMasterList != null && UnitMasterList.Count > 0)
            {

                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });

                if (UnitMasterList != null && UnitMasterList.Count > 0)
                {
                    foreach (var item in UnitMasterList)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.UnitId.ToString(), Text = item.UnitValue };
                        if (item.UnitId == contactTypeId)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }
        #endregion

        #region UnitTypeList
        public static List<SelectListItem> GetUnitTypeList(int? id)
        {
            DbConnector db = new DbConnector();
            List<Unit> UnitMasterList = db.GetUnitMasterList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (UnitMasterList != null && UnitMasterList.Count > 0)
            {

                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });

                if (UnitMasterList != null && UnitMasterList.Count > 0)
                {
                    foreach (var item in UnitMasterList)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.UnitId.ToString(), Text = item.UnitValue };
                        if (item.UnitTypeId == id)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }
        #endregion

        #region HSNList
        public static List<SelectListItem> GetHSNList(int contactTypeId)
        {
            DbConnector db = new DbConnector();
            List<HSN> HSNMasterList = db.GetHSNMasterList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (HSNMasterList != null)
            {

                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });

                if (HSNMasterList != null && HSNMasterList.Count > 0)
                {
                    foreach (var item in HSNMasterList)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.HSNId.ToString(), Text = item.Code };
                        if (item.HSNId == contactTypeId)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;

        }

        public static List<SelectListItem> GetFreezedUnitList(int unitId)
        {
            DbConnector db = new DbConnector();
            List<Unit> UnitMasterList = db.UnitMasters.Where(x => x.UnitId == unitId).ToList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (UnitMasterList != null && UnitMasterList.Count > 0)
            {

                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });

                if (UnitMasterList != null && UnitMasterList.Count > 0)
                {
                    foreach (var item in UnitMasterList)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.UnitId.ToString(), Text = item.UnitValue };
                        if (item.UnitId == unitId)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }
        #endregion

        #region StoreMasterList
        public static List<SelectListItem> GetStoreList(int? contactTypeId)
        {
            DbConnector db = new DbConnector();
            List<StoreMaste> StoreList = db.GetStoreListList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (StoreList != null && StoreList.Count > 0)
            {

                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });

                if (StoreList != null && StoreList.Count > 0)
                {
                    foreach (var item in StoreList)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.StoreMasteId.ToString(), Text = item.StoreName };
                        if (item.StoreMasteId == contactTypeId)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }

        public static List<SelectListItem> GetAttributeValueList(int id)
        {
            AttributesDbUtil util = new AttributesDbUtil();
            AttributeSet model = util.GetAttribute(id);
            List<string> tempList = model.ApplicableValues.Split(',').ToList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            itemList.Insert(0, new SelectListItem() { Text = "None", Value = "-1" });
            if (tempList != null && tempList.Count > 0)
            {
                foreach (var item in tempList)
                {
                    itemList.Add(new SelectListItem() { Value = item.ToString(), Text = item, });
                }
            }
            return itemList;
        }
        #endregion

        #region BrandMasterList
        public static List<SelectListItem> GetBrandList(int? contactTypeId)
        {
            DbConnector db = new DbConnector();
            List<Brand> BrandList = db.GetBrandList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (BrandList != null)
            {

                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });

                if (BrandList != null && BrandList.Count > 0)
                {
                    foreach (var item in BrandList)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.BrandId.ToString(), Text = item.BrandName };
                        if (item.BrandId == contactTypeId)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }
        #endregion

        #region GSTList
        public static List<SelectListItem> GetGSTList(int? contactTypeId)
        {
            DbConnector db = new DbConnector();
            List<GST> GSTList = db.GetGSTvalueList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (GSTList != null && GSTList.Count > 0)
            {

                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });

                if (GSTList != null && GSTList.Count > 0)
                {
                    foreach (var item in GSTList)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.GSTId.ToString(), Text = item.Name };
                        if (item.GSTId == contactTypeId)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }
        #endregion

        #region ShippingList
        public static List<SelectListItem> GetShippingMasterList(int? contactTypeId)
        {
            DbConnector db = new DbConnector();
            List<Shipping> ShippingList = db.GetShippingList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (ShippingList != null && ShippingList.Count > 0)
            {

                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });

                if (ShippingList != null && ShippingList.Count > 0)
                {
                    foreach (var item in ShippingList)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.ShippingId.ToString(), Text = item.ShippingType };
                        if (item.ShippingId == contactTypeId)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }
        #endregion

        #region DesignationList
        public static List<SelectListItem> GetDesignationMasterList(int? contactTypeId)
        {
            DbConnector db = new DbConnector();
            List<Designation> DesignationList = db.GetDesignationList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (DesignationList != null && DesignationList.Count > 0)
            {

                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });
                if (DesignationList != null && DesignationList.Count > 0)
                {
                    foreach (var item in DesignationList)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.DesignationId.ToString(), Text = item.DesignationType };
                        if (item.DesignationId == contactTypeId)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }

            }
            else
            {
                itemList = new List<SelectListItem>();
            }

            return itemList;

        }
        #endregion

        #region SKUMasterList
        public static List<SelectListItem> GetSKUList(int contactTypeId)
        {
            DbConnector db = new DbConnector();
            List<SKUMaster> SKUMasterList = db.GetSKUMasterList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (SKUMasterList != null)
            {

                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });

                if (SKUMasterList != null && SKUMasterList.Count > 0)
                {
                    foreach (var item in SKUMasterList)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.SKUMasterId.ToString(), Text = item.SKU };
                        if (item.SKUMasterId == contactTypeId)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;

        }
        #endregion

        #region LocationList
        public static List<SelectListItem> GetLocationMasterList(int? contactTypeId)
        {
            DbConnector db = new DbConnector();
            List<Location> LocationList = db.GetLocationList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (LocationList != null && LocationList.Count > 0)
            {

                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });

                if (LocationList != null && LocationList.Count > 0)
                {
                    foreach (var item in LocationList)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.LocationId.ToString(), Text = item.LocationName };
                        if (item.LocationId == contactTypeId)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;

        }

        #endregion

        #region BranchList
        public static List<SelectListItem> GetBranchList(int? contactTypeId)
        {
            DbConnector db = new DbConnector();
            List<Branch> BranchList = db.GetBranchList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            itemList.Insert(0, new SelectListItem() { Text = "--Select Branch--", Value = "-1" });
            if (BranchList != null && BranchList.Count > 0)
            {
                foreach (var item in BranchList)
                {
                    SelectListItem selectItem = new SelectListItem() { Value = item.BranchId.ToString(), Text = item.BranchName };
                    if (item.BranchId == contactTypeId)
                    {
                        selectItem.Selected = true;
                    }
                    itemList.Add(selectItem);
                }
            }
            return itemList;

        }

        public static List<SelectListItem> GetBranchListByCompany(int? id)
        {
            DbConnector db = new DbConnector();
            List<Branch> BranchList = db.GetBranchListByCompany(id);
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (BranchList != null && BranchList.Count > 0)
            {

                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });

                if (BranchList != null && BranchList.Count > 0)
                {
                    foreach (var item in BranchList)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.BranchId.ToString(), Text = item.BranchName };
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;

        }
        #endregion

        #region PurchaseMemoList
        public static List<SelectListItem> GetPurchaseMemoList()
        {
            List<SelectListItem> selectList = new List<SelectListItem>()
            {
                new SelectListItem { Text="--Select One--",Value="-1"},
                new SelectListItem {Text="Memo1",Value="1" }
            };
            return selectList;
        }
        #endregion  

        #region PaymentType
        public static List<SelectListItem> GetPaymentTypeList(int? contactTypeId)
        {
            DbConnector db = new DbConnector();
            List<Models.PaymentType> PaymentTypeList = db.GetPaymentTypeList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (PaymentTypeList != null && PaymentTypeList.Count > 0)
            {

                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });

                if (PaymentTypeList != null && PaymentTypeList.Count > 0)
                {
                    foreach (var item in PaymentTypeList)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.PaymentTypeId.ToString(), Text = item.PaymentName };
                        if (item.PaymentTypeId == contactTypeId)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }

        public static List<SelectListItem> GetPaymentTypeWithLedger()
        {
            DbConnector db = new DbConnector();
            List<Models.PaymentType> PaymentTypeList = db.PaymentTypes.Where(x => x.LedgerId != null).ToList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (PaymentTypeList != null && PaymentTypeList.Count > 0)
            {

                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });
                if (PaymentTypeList != null && PaymentTypeList.Count > 0)
                {
                    foreach (var item in PaymentTypeList)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.PaymentTypeId.ToString(), Text = item.PaymentName };
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }
        #endregion


        #region DiscontList
        public static List<SelectListItem> GetDiscountsAccessibleList(string DiscountsAccessible,int? ProductId)
        {
            DbConnector db = new DbConnector();
            List<Discount> DiscountList = db.GetActiveDiscountList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            List<string> DiscountIds = (DiscountsAccessible ?? "").Split(',').ToList();
            if(!ProductId.HasValue)
            {
                foreach (var item in DiscountList)
                {
                    SelectListItem selectItem = new SelectListItem() { Value = item.DiscountId.ToString(), Text = item.DiscountCode };
                    if (DiscountIds.Contains(item.DiscountId.ToString()))
                    {
                        selectItem.Selected = true;
                    }
                    itemList.Add(selectItem);
                }
            }
            else
            {
                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });
                foreach (var item in DiscountList)
                {
                    SelectListItem selectItem = new SelectListItem() { Value = item.DiscountId.ToString(), Text = item.DiscountCode };
                    if (DiscountIds.Contains(item.DiscountId.ToString()))
                    {
                        itemList.Add(selectItem);
                    }                    
                }
            }
            return itemList;

        }
        #endregion

        #region DivisionList
        public static List<SelectListItem> GetDivisionList(int? contactTypeId)
        {
            DbConnector db = new DbConnector();
            List<Division> DivisionList = db.GetDivisionList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            itemList.Insert(0, new SelectListItem() { Text = "--Select Division--", Value = "-1" });
            if (DivisionList != null && DivisionList.Count > 0)
            {
                foreach (var item in DivisionList)
                {
                    SelectListItem selectItem = new SelectListItem() { Value = item.DivisionId.ToString(), Text = item.DivisionName };
                    if (item.DivisionId == contactTypeId)
                    {
                        selectItem.Selected = true;
                    }
                    itemList.Add(selectItem);
                }
            }
            return itemList;

        }

        public static List<SelectListItem> GetDivisionListByCompany(int? id)
        {
            DbConnector db = new DbConnector();
            List<Division> DivisionList = db.GetDivisionListByCompany(id);
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (DivisionList != null && DivisionList.Count > 0)
            {

                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });

                if (DivisionList != null && DivisionList.Count > 0)
                {
                    foreach (var item in DivisionList)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.DivisionId.ToString(), Text = item.DivisionName };
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;

        }
        #endregion

        #region SupplierTypeList
        public static List<SelectListItem> GetSupplierTypeList(int? contactTypeId)
        {
            List<SelectListItem> selectList = new List<SelectListItem>()
            {
                new SelectListItem { Text="--Select One--",Value="-1"},
                new SelectListItem {Text="Manufacturer",Value="1" },
                new SelectListItem { Text="Distributor",Value="2"},
                new SelectListItem { Text="Trader",Value="3"}
            };
            return selectList;
        }
        #endregion

        #region PurchaseOrderList  
        public static List<SelectListItem> GetPurchaseOrderList(int? contactTypeId, PurchaseOrderStatus PurchaseOrderStatus)
        {
            DbConnector db = new DbConnector();
            List<PurchaseOrder> PurchaseOrderList = db.PurchaseOrders.Where(x => x.OrderStatus == PurchaseOrderStatus && x.CompanyId == db.CompanyId).ToList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });
            if (PurchaseOrderList != null && PurchaseOrderList.Count > 0)
            {                
                if (PurchaseOrderList != null && PurchaseOrderList.Count > 0)
                {
                    foreach (var item in PurchaseOrderList)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.PurchaseOrderId.ToString(), Text = item.PurchaseOrderNo };
                        if (item.PurchaseOrderId == contactTypeId)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });
            }
            return itemList;
        }

        public static List<SelectListItem> GetPurchaseOrderList()
        {
            DbConnector db = new DbConnector();
            List<PurchaseOrder> PurchaseOrderList = db.PurchaseOrders.ToList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });
            if (PurchaseOrderList != null && PurchaseOrderList.Count > 0)
            {
                if (PurchaseOrderList != null && PurchaseOrderList.Count > 0)
                {
                    foreach (var item in PurchaseOrderList)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.PurchaseOrderId.ToString(), Text = item.PurchaseOrderNo };
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });
            }
            return itemList;
        }
        #endregion

        #region SaleOrderList  
        public static List<SelectListItem> GetSaleOrderList(int? contactTypeId, PurchaseOrderStatus? Status)
        {
            DbConnector db = new DbConnector();
            List<SalesOrder> OrdersList = null;
            List<SelectListItem> itemList = new List<SelectListItem>();
            if(Status.HasValue)
            {
                OrdersList = db.SalesOrders.Where(x => x.OrderStatus == Status && x.CompanyId == db.CompanyId).OrderByDescending(x => x.SalesOrderId).ToList();
            }
            else
            {
                OrdersList = db.SalesOrders.Where(x => x.CompanyId == db.CompanyId).OrderByDescending(x => x.SalesOrderId).ToList();
            }
            if (OrdersList != null && OrdersList.Count > 0)
            {
                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });
                foreach (var item in OrdersList)
                {
                    SelectListItem selectListItem = new SelectListItem() { Text = item.SaleOrderNumber, Value = item.SalesOrderId.ToString() };
                    itemList.Add(selectListItem);
                }
            }
            else
            {                
                itemList = new List<SelectListItem>();
                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });
            }
            return itemList;
        }
        #endregion

        public static List<SelectListItem> GetSaleInvoiceList(int? contactTypeId)
        {
            DbConnector db = new DbConnector();
            List<SaleInvoice> InvoiceList = db.SaleInvoices.Where(x => x.Type == Constants.SALE_TYPE_SALE_INVOICE && x.CompanyId == db.CompanyId).OrderByDescending(x => x.SaleInvoiceId).ToList(); 
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (InvoiceList != null && InvoiceList.Count > 0)
            {
                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });
                foreach (var item in InvoiceList)
                {
                    SelectListItem selectListItem = new SelectListItem() { Text = item.SaleInvoiceNo, Value = item.SaleInvoiceId.ToString() };
                    itemList.Add(selectListItem);
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });
            }
            return itemList;
        }
        #region SaleMemoList
        public static List<SelectListItem> GetDeliveryMemoList()
        {
            DbConnector db = new DbConnector();
            List<SaleInvoice> DeliveryMemoList = db.SaleInvoices.Where(x => x.Type == Constants.SALE_TYPE_DELIVERY_MEMO && x.CompanyId == db.CompanyId).ToList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });
            if (DeliveryMemoList != null && DeliveryMemoList.Count > 0)
            {
                foreach (var item in DeliveryMemoList)
                {
                    SelectListItem selectListItem = new SelectListItem() { Text = item.SaleInvoiceNo, Value = item.SaleInvoiceId.ToString() };
                    itemList.Add(selectListItem);
                }
            }
            return itemList;
        }
        #endregion 
        
        #region CompanyList
        public static List<SelectListItem> GetCompanyList(int? contactTypeId)
        {
            DbConnector db = new DbConnector();
            List<Company> CompanyList = db.GetCompanyList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (CompanyList != null && CompanyList.Count > 0)
            {

                itemList.Insert(0, new SelectListItem() { Text = "--Select Company--", Value = "-1" });

                if (CompanyList != null && CompanyList.Count > 0)
                {
                    foreach (var item in CompanyList)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.CompanyId.ToString(), Text = item.Name };
                        if (item.CompanyId == contactTypeId)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;

        }
        #endregion

        #region RolesList  
        public static List<SelectListItem> GetRolesList(int? contactTypeId)
        {
            DbConnector db = new DbConnector();
            List<Role> RoleList = db.GetRoleList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (RoleList != null && RoleList.Count > 0)
            {

                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });

                if (RoleList != null && RoleList.Count > 0)
                {
                    foreach (var item in RoleList)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.RoleId.ToString(), Text = item.Name };
                        if (item.RoleId == contactTypeId)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }
        #endregion

        #region ContactCategoryList
        public static List<SelectListItem> GetContactCategoryList(int? contactTypeId)
        {
            DbConnector db = new DbConnector();



            List<AccountCategory> list = db.GetContactCategoryList(null);

            List<SelectListItem> itemList = new List<SelectListItem>();
            if (list != null && list.Count > 0)
            {

                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });
                //itemList.Add(new SelectListItem() { Text = "Other", Value = "0" });
                if (list != null && list.Count > 0)
                {
                    foreach (var item in list)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.AccountCategoryId.ToString(), Text = item.Name };
                        if (item.AccountCategoryId == contactTypeId)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }
        #endregion

        public static List<SelectListItem> GetProductSelectList()
        {
            DbConnector db = new DbConnector();
            List<Product> ProductList = db.GetProductsList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (ProductList != null && ProductList.Count > 0)
            {
                foreach (var item in ProductList)
                {
                    SelectListItem selectListItem = new SelectListItem() { Text = item.SerialNumber, Value = item.ProductId.ToString() };
                    itemList.Add(selectListItem);
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }

        public static List<SelectListItem> GetSessionDivisionSelectList()
        {
            DbConnector db = new DbConnector();
            Division division = SessionManager.GetSessionDivision();
            List<Division> DivisionList = new List<Division>
            {
                division
            };
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (DivisionList != null && DivisionList.Count > 0)
            {
                foreach (var item in DivisionList)
                {
                    SelectListItem selectListItem = new SelectListItem() { Text = item.DivisionName, Value = item.DivisionId.ToString() };
                    itemList.Add(selectListItem);
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }

        public static List<SelectListItem> GetSessionBranchSelectList()
        {
            DbConnector db = new DbConnector();
            Branch branch = SessionManager.GetSessionBranch();
            List<Branch> BranchList = new List<Branch>
            {
                branch
            };
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (BranchList != null && BranchList.Count > 0)
            {
                foreach (var item in BranchList)
                {
                    SelectListItem selectListItem = new SelectListItem() { Text = item.BranchName, Value = item.BranchId.ToString() };
                    itemList.Add(selectListItem);
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }

        public static List<SelectListItem> GetSessionCompanySelectList()
        {
            DbConnector db = new DbConnector();
            Company company = SessionManager.GetSessionCompany();
            List<Company> CompanyList = new List<Company>
            {
                company
            };
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (CompanyList != null && CompanyList.Count > 0)
            {
                foreach (var item in CompanyList)
                {
                    SelectListItem selectListItem = new SelectListItem() { Text = item.Name, Value = item.CompanyId.ToString() };
                    itemList.Add(selectListItem);
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }

        #region Ledger
        public static List<SelectListItem> GetLedgerListForType(int? contactTypeId,string LedgerType)
        {
            DbConnector db = new DbConnector();
            List<Ledger> list = new List<Ledger>();
            if (LedgerType == Constants.LEDGER_TYPE_COMPANY || LedgerType == Constants.LEDGER_TYPE_TAX)
            {
                list = db.Ledgers.Where(x => x.LedgerType == LedgerType && (x.CompanyId == null || x.CompanyId == db.CompanyId)).OrderBy(x => x.Name).ToList();
            }
            else if(LedgerType == Constants.LEDGER_TYPE_USER)
            {
                list = db.Ledgers.Where(x => x.LedgerType != Constants.LEDGER_TYPE_COMPANY && x.LedgerType != Constants.LEDGER_TYPE_TAX && (x.CompanyId == null || x.CompanyId == db.CompanyId)).OrderBy(x => x.Name).ToList();
            }
            else if (LedgerType == Constants.LEDGER_TYPE_PAYMENTTYPE)
            {
                list = db.Ledgers.Where(x => x.LedgerType == LedgerType && (x.CompanyId == null || x.CompanyId == db.CompanyId)).OrderBy(x => x.Name).ToList();
            }
            else
            {
                list = db.Ledgers.Where(x => (x.CompanyId == null || x.CompanyId == db.CompanyId)).OrderBy(x => x.Name).ToList();
            }
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (list != null && list.Count > 0)
            {
                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });
                if (list != null && list.Count > 0)
                {
                    foreach (var item in list)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.LedgerId.ToString(), Text = item.Name };
                        if (item.AccountCategoryId == contactTypeId)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }
        #endregion

        public static List<SelectListItem> BankAccountList(int? contactTypeId)
        {
            DbConnector db = new DbConnector();
            List<Ledger> list = db.GetLedgerListofBank();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (list != null && list.Count > 0)
            {
                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });
                if (list != null && list.Count > 0)
                {
                    foreach (var item in list)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.LedgerId.ToString(), Text = item.Name };
                        if (item.LedgerId == contactTypeId)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }


        #region ContactSubCategory
        public static List<SelectListItem> GetContactSubCategoryList(int? contactTypeId)
        {
            List<AccountSubCategory> list = null;
            DbConnector db = new DbConnector();
            if (contactTypeId.HasValue && contactTypeId > 0)
            {
                list = db.GetContactSubCategoryList(contactTypeId.Value);
            }
            else
            {
                list = new List<AccountSubCategory>();
            }
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (list != null && list.Count > 0)
            {

                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });

                if (list != null && list.Count > 0)
                {
                    foreach (var item in list)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.AccountSubCategoryId.ToString(), Text = item.ContactSubCategoryName };
                        if (item.AccountCategoryId == contactTypeId)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }
        #endregion

        public static List<SelectListItem> AttributeSetList(int? contactTypeId)
        {
            DbConnector db = new DbConnector();
            List<AttributeSet> list = db.GetAttributeSets();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (list != null && list.Count > 0)
            {
                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });
                if (list != null && list.Count > 0)
                {
                    foreach (var item in list)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.AttributeSetId.ToString(), Text = item.Name };
                        if (item.AttributeSetId == contactTypeId)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }

        #region SubcategoryList
        public static List<SelectListItem> GetSubCategoryListByCategory(int? contactTypeId)
        {
            DbConnector db = new DbConnector();
            List<SubCategory> subCategoryList = db.GetSubCategoryListByCategoryId(contactTypeId);
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (subCategoryList != null && subCategoryList.Count > 0)
            {
                if (subCategoryList != null && subCategoryList.Count > 0)
                {
                    foreach (var item in subCategoryList)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.SubCategoryId.ToString(), Text = item.Name };
                        if (item.SubCategoryId == contactTypeId)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }
        #endregion

        #region UnitList
        public static List<SelectListItem> GetUnitConversionList(int contactTypeId)
        {
            DbConnector db = new DbConnector();
            List<Unit> UnitList = db.GetUnitTypeList(contactTypeId);
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (UnitList != null && UnitList.Count > 0)
            {
                if (UnitList != null && UnitList.Count > 0)
                {
                    foreach (var item in UnitList)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.UnitId.ToString(), Text = item.UnitValue };
                        if (item.UnitId == contactTypeId)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }
        #endregion

        public static List<SelectListItem> GetEmployeeList(int? contactTypeId)
        {
            DbConnector db = new DbConnector();
            List<Ledger> EmployeeList = db.Ledgers.Where(x => x.Contact.Type == "Employee" && (x.CompanyId == null || x.CompanyId == db.CompanyId)).OrderBy(x => x.Name).ToList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (EmployeeList != null && EmployeeList.Count > 0)
            {

                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });

                if (EmployeeList != null && EmployeeList.Count > 0)
                {
                    foreach (var item in EmployeeList)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.LedgerId.ToString(), Text = item.Name };
                        if (item.ContactId == contactTypeId)
                        {
                            selectItem.Selected = true;
                        }
                        //selectItem.Text += "/" + item.LastName;
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;

        }

        public static List<SelectListItem> GetMeasurmentTypeList(int? contactTypeId)
        {
            DbConnector db = new DbConnector();
            List<Measurment> MeasurmentList = db.GetMeasurmentList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (MeasurmentList != null && MeasurmentList.Count > 0)
            {

                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });

                if (MeasurmentList != null && MeasurmentList.Count > 0)
                {
                    foreach (var item in MeasurmentList)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.MeasurmentId.ToString(), Text = item.MeasurmentType };
                        if (item.MeasurmentId == contactTypeId)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }

        //public static List<SelectListItem> GetCustomerList(int? contactTypeId)
        //{
        //    DbConnector db = new DbConnector();
        //    List<Ledger> EmployeeList = db.Ledgers.Where(x => x.Contact.Type == "Customer").ToList();
        //    List<SelectListItem> itemList = new List<SelectListItem>();
        //    if (EmployeeList != null && EmployeeList.Count > 0)
        //    {

        //        itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });

        //        if (EmployeeList != null && EmployeeList.Count > 0)
        //        {
        //            foreach (var item in EmployeeList)
        //            {
        //                SelectListItem selectItem= new SelectListItem() { Value = item.LedgerId.ToString(), Text = item.Name };
                       
        //                if (item.ContactId == contactTypeId)
        //                {
        //                    selectItem.Selected = true;
        //                }
        //                itemList.Add(selectItem);
        //            }
        //        }
        //    }
        //    else
        //    {
        //        itemList = new List<SelectListItem>();
        //    }
        //    return itemList;
        //}

        public static List<SelectListItem> GetCustomerForCRM()
        {
            DbConnector db = new DbConnector();
            List<Ledger> EmployeeList = db.Ledgers.Where(x => x.Contact.Type == "Customer" || x.Contact.Type == "Supplier").ToList();
            List <SelectListItem> itemList = new List<SelectListItem>();
            if (EmployeeList != null && EmployeeList.Count > 0)
            {

                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });

                if (EmployeeList != null && EmployeeList.Count > 0)
                {
                    foreach (var item in EmployeeList)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.LedgerId.ToString(), Text = item.Name };
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }
        
        public static List<SelectListItem> GetProductionList(int? contactTypeId)
        {
            DbConnector db = new DbConnector();
            List<JobSheet> ProductionList = db.JobSheets.Where(x => x.CompanyId == db.CompanyId && x.JobSheetStatus != JobSheetStatus.FurtherProcess && x.JobSheetComplete != JobSheetComplete.Complete).ToList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (ProductionList != null && ProductionList.Count > 0)
            {

                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });

                if (ProductionList != null && ProductionList.Count > 0)
                {
                    foreach (var item in ProductionList)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.JobSheetId.ToString(), Text = item.ProductNumber };                        
                        if (item.JobSheetId == contactTypeId)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }

        #region CompanyAccessList
        public static List<SelectListItem> GetCompanyAccessList(int? RoleId)
        {
            DbConnector db = new DbConnector();
            Role role = db.GetRole(RoleId.Value);
            List<string> CompanyList;
            if (role.IsAdmin == true)
            {
                CompanyList = db.Companys.Select(x => x.CompanyId.ToString()).ToList();
            }
            else
            {
                CompanyList = role.CompaniesAccessible.Split(',').ToList();
            }                      
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (CompanyList != null && CompanyList.Count > 0)
            {
                itemList.Insert(0, new SelectListItem() { Text = "--Select Company--", Value = "-1" });
                if (CompanyList != null && CompanyList.Count > 0)
                {
                    foreach (var item in CompanyList)
                    {
                        int NewCompanyId = Int32.Parse(item);
                        Company company = db.Companys.Where(x => x.CompanyId == NewCompanyId).FirstOrDefault();
                        SelectListItem selectItem = new SelectListItem() { Value = company.CompanyId.ToString(), Text = company.Name };
                        if (company.CompanyId == RoleId)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }
        #endregion

        #region GetBranchAccessList
        public static List<SelectListItem> GetBranchAccessList(int? RoleId)
        {
            DbConnector db = new DbConnector();
            Role role = db.GetRole(RoleId.Value);
            List<string> BranchList;
            if (role.IsAdmin == true)
            {
                BranchList = db.Branchs.Select(x => x.BranchId.ToString()).ToList();
            }
            else
            {
                BranchList = role.BranchesAccessible.Split(',').ToList();
            }
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (BranchList != null && BranchList.Count > 0)
            {
                itemList.Insert(0, new SelectListItem() { Text = "--Select Company--", Value = "-1" });
                if (BranchList != null && BranchList.Count > 0)
                {
                    foreach (var item in BranchList)
                    {
                        int NewBranchId = Int32.Parse(item);
                        Branch branch = db.Branchs.Where(x => x.BranchId == NewBranchId).FirstOrDefault();                                                  
                        SelectListItem selectItem = new SelectListItem() { Value = branch.BranchId.ToString(), Text = branch.BranchName };
                        if (branch.CompanyId == RoleId)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }
        #endregion

        #region GetDivisionAccessList
        public static List<SelectListItem> GetDivisionAccessList(int? RoleId)
        {
            DbConnector db = new DbConnector();
            Role role = db.GetRole(RoleId.Value);
            List<string> DivisionList;
            if (role.IsAdmin == true)
            {
                DivisionList = db.Divisions.Select(x => x.DivisionId.ToString()).ToList();
            }
            else
            {
                DivisionList = role.DivisionsAccessible.Split(',').ToList();
            }
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (DivisionList != null && DivisionList.Count > 0)
            {
                itemList.Insert(0, new SelectListItem() { Text = "--Select Company--", Value = "-1" });
                if (DivisionList != null && DivisionList.Count > 0)
                {
                    foreach (var item in DivisionList)
                    {
                        int NewDivisionId = Int32.Parse(item);
                        Division division  = db.Divisions.Where(x => x.DivisionId == NewDivisionId).FirstOrDefault();                                                
                        SelectListItem selectItem = new SelectListItem() { Value = division.DivisionId.ToString(), Text = division.DivisionName };
                        if (division.DivisionId == RoleId)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }
        #endregion

        #region GetDivisionAccessListByCompany
        public static List<SelectListItem> GetDivisionAccessListByCompany(int? RoleId, int? CompanyId)
        {
            DbConnector db = new DbConnector();
            Role role = db.GetRole(RoleId.Value);
            List<string> DivisionList;
            if (role.IsAdmin == true)
            {
                DivisionList = db.Divisions.Select(x => x.DivisionId.ToString()).ToList();
            }
            else
            {
                DivisionList = role.DivisionsAccessible.Split(',').ToList();
            }
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (DivisionList != null && DivisionList.Count > 0)
            {
                if (DivisionList != null && DivisionList.Count > 0)
                {
                    foreach (var item in DivisionList)
                    {
                        int NewDivisionId = Int32.Parse(item);                         
                        if (CompanyId.HasValue)
                        {
                            Division division = db.Divisions.Where(x => x.DivisionId == NewDivisionId && x.CompanyId == CompanyId).FirstOrDefault();
                            if(division != null)
                            {
                                SelectListItem selectItem = new SelectListItem() { Value = division.DivisionId.ToString(), Text = division.DivisionName };
                                if (division.DivisionId == RoleId)
                                {
                                    selectItem.Selected = true;
                                }
                                itemList.Add(selectItem);
                            }
                        }

                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }
        #endregion

        #region GetBranchAccessListByCompany
        public static List<SelectListItem> GetBranchAccessListByDivision(int? RoleId, int? DivisionId)
        {
            DbConnector db = new DbConnector();
            Role role = db.GetRole(RoleId.Value);
            List<string> BranchList;
            if (role.IsAdmin == true)
            {
                BranchList = db.Branchs.Select(x => x.BranchId.ToString()).ToList();
            }
            else
            {
                BranchList = role.BranchesAccessible.Split(',').ToList();
            }
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (BranchList != null && BranchList.Count > 0)
            {
                if (BranchList != null && BranchList.Count > 0)
                {
                    foreach (var item in BranchList)
                    {
                        int NewBranchId = Int32.Parse(item);
                        if (DivisionId.HasValue)
                        {
                            Branch branch = db.Branchs.Where(x => x.BranchId == NewBranchId && x.DivisionId == DivisionId).FirstOrDefault();
                            if (branch != null)
                            {
                                SelectListItem selectItem = new SelectListItem() { Value = branch.BranchId.ToString(), Text = branch.BranchName };
                                if (branch.BranchId == RoleId)
                                {
                                    selectItem.Selected = true;
                                }
                                itemList.Add(selectItem);
                            }
                        }

                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }
        #endregion


        public static List<SelectListItem> ServiceTypeList(int? contactTypeId)
        {
            DbConnector db = new DbConnector();
            List<ServiceType> ServiceTypeList = db.ServiceTypes.ToList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (ServiceTypeList != null && ServiceTypeList.Count > 0)
            {

                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });

                if (ServiceTypeList != null && ServiceTypeList.Count > 0)
                {
                    foreach (var item in ServiceTypeList)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.ServiceTypeId.ToString(), Text = item.ServicesName };

                        if (item.ServiceTypeId == contactTypeId)
                        {
                            selectItem.Selected = true;
                        }
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }

        public static List<SelectListItem> GetCustomerForLead()
        {
            DbConnector db = new DbConnector();
            List<Contact> ContactList = db.Contacts.Where(x => x.Type == "Customer" || x.Type == "Supplier").ToList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (ContactList != null && ContactList.Count > 0)
            {

                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "-1" });

                if (ContactList != null && ContactList.Count > 0)
                {
                    foreach (var item in ContactList)
                    {
                        if(item.TitleList == Enums.TitleType.Ms)
                        {
                            SelectListItem selectItem = new SelectListItem() { Value = item.ContactId.ToString(), Text = item.CompanyName };
                            itemList.Add(selectItem);
                        }
                        else
                        {
                            SelectListItem selectItem = new SelectListItem() { Value = item.ContactId.ToString(), Text = (item.FirstName + item.LastName) };
                            itemList.Add(selectItem);
                        }

                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }

        public static List<SelectListItem> InProcessStatus()
        {
            DbConnector db = new DbConnector();
            List<JobSheetInProcessStatus> InProcessStatus = db.JobSheetInProcessStatuses.ToList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (InProcessStatus != null && InProcessStatus.Count > 0)
            {

                itemList.Insert(0, new SelectListItem() { Text = "--Select One--", Value = "" });

                if (InProcessStatus != null && InProcessStatus.Count > 0)
                {
                    foreach (var item in InProcessStatus)
                    {
                        SelectListItem selectItem = new SelectListItem() { Value = item.JobSheetInProcessStatusId.ToString(), Text = item.Status };
                        itemList.Add(selectItem);
                    }
                }
            }
            else
            {
                itemList = new List<SelectListItem>();
            }
            return itemList;
        }
    }
}

