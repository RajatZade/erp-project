﻿namespace DataLayer.Utils
{
    public class MessageStore
    {
        public const string ResponseMessage = "Success";
        public const string LoginError = "Please enter valid Email Id or Password";
        public const string PasseordChange = "PasswordChange Successfully";
        public const string DeleteError = "This item can not be deleted .Please remove all references to this item before deleting.";
        public const string DeleteItem = "Item Deleted Successfully";
        public const string CantDeleteProductSold = "Can Not Delete,Products are sold";

    }
}
