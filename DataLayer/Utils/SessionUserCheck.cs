﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;

namespace DataLayer.Utils
{
    public class SessionUserCheck
    {
        public static bool IsSessionUserNull()
        {
            LoginUser user = SessionManager.GetSessionUser();
            if(user == null)
            {
                return false;
            }
            return true;            
        }

        public static bool IsSessionCompanyNull()
        {
            Company company = SessionManager.GetSessionCompany();
            if (company == null)
            {
                return false;
            }
            return true;
        }
    }
}
