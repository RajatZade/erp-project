﻿using DataLayer.Models;
using DataLayer.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using DataLayer;

namespace ERP_System.Utils
{
    public class RBACUtil
    {
        public static List<int> CompaniesAccessible { get { return GetAccessibleCompanies(); } }

        public static List<int> BranchesAccessible { get { return GetAccessibleBranches(); } }

        public static List<int> DivisionsAccessible { get { return GetAccessibleDivisions(); } }

        public static bool HasAccess(string requiredPermission, int accessType)
        {
            LoginUser user = SessionManager.GetSessionUser();
            DbConnector db = new DbConnector();
            user.Role.Permissions = db.Permissions.ToList();
            user.Role.PermissionSetLinkings = db.PermissionLinkings.Where(x => x.RoleId == user.RoleId).ToList();
            if (user.Role.IsAdmin)
            {
                return true;
            }
            bool hasAccess = false;
            Permission permission = null;
            switch (accessType)
            {
                case 0:
                    {
                        permission = user.Role.Permissions.Where(x => x.CreateValue == requiredPermission).FirstOrDefault();
                        if (permission != null)
                        {
                            hasAccess = user.Role.PermissionSetLinkings.Any(x => x.IsCreate && x.PermissionId == permission.PermissionId);
                        }
                        break;
                    }
                case 1:
                    {
                        permission = user.Role.Permissions.Where(x => x.EditValue == requiredPermission).FirstOrDefault();
                        if (permission != null)
                        {
                            hasAccess = user.Role.PermissionSetLinkings.Any(x => x.IsEdit && x.PermissionId == permission.PermissionId);
                        }
                        break;
                    }
                case 2:
                    {
                        permission = user.Role.Permissions.Where(x => x.ReadValue == requiredPermission).FirstOrDefault();
                        if (permission != null)
                        {
                            hasAccess = user.Role.PermissionSetLinkings.Any(x => x.IsRead && x.PermissionId == permission.PermissionId);
                        }
                        break;
                    }
                case 3:
                    {
                        permission = user.Role.Permissions.Where(x => x.DeleteValue == requiredPermission).FirstOrDefault();
                        if (permission != null)
                        {
                            hasAccess = user.Role.PermissionSetLinkings.Any(x => x.IsDelete && x.PermissionId == permission.PermissionId);
                        }
                        break;
                    }
            }

            if (permission == null)
            {
                permission = user.Role.Permissions.Where(x => x.EditValue == requiredPermission).FirstOrDefault();
            }
            if (permission == null)
            {
                permission = user.Role.Permissions.Where(x => x.ReadValue == requiredPermission).FirstOrDefault();
            }
            if (permission == null)
            {
                permission = user.Role.Permissions.Where(x => x.DeleteValue == requiredPermission).FirstOrDefault();
            }

            return hasAccess;
        }

        public static List<int> GetAccessibleCompanies()
        {
            if(CompaniesAccessible != null)
            {
                return CompaniesAccessible;
            }
            List<int> output = new List<int>();
            if (SessionManager.GetSessionUser() != null)
            {
                Role role = SessionManager.GetSessionUser().Role;
                
                if (role != null && role.CompaniesAccessible != null)
                {
                    foreach (var item in role.CompaniesAccessible.Split(',').ToList())
                    {
                        output.Add(Convert.ToInt32(item));
                    }
                }
            }
            return output;
        }

        public static List<int> GetAccessibleBranches()
        {
            if (BranchesAccessible != null)
            {
                return BranchesAccessible;
            }
            List<int> output = new List<int>();
            if (SessionManager.GetSessionUser() != null)
            {
                Role role = SessionManager.GetSessionUser().Role;

                if (role != null && role.BranchesAccessible != null)
                {
                    foreach (var item in role.BranchesAccessible.Split(',').ToList())
                    {
                        output.Add(Convert.ToInt32(item));
                    }
                }
            }
            return output;
        }

        public static List<int> GetAccessibleDivisions()
        {
            if (DivisionsAccessible != null)
            {
                return DivisionsAccessible;
            }
            List<int> output = new List<int>();
            if (SessionManager.GetSessionUser() != null)
            {
                Role role = SessionManager.GetSessionUser().Role;

                if (role != null && role.DivisionsAccessible != null)
                {
                    foreach (var item in role.DivisionsAccessible.Split(',').ToList())
                    {
                        output.Add(Convert.ToInt32(item));
                    }
                }
            }
            return output;
        }
    }
}