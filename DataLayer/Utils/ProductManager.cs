﻿using DataLayer.Models;
using System;
using System.Linq;
using System.Collections.Generic;

namespace DataLayer.Utils
{
    public class ProductManager
    {
        public static SaleInvoiceItem AddInvoiceItem(Product product, int? saleInvoiceId, bool? IsWPS,string Type,string ProductStatus)
        {
            DbConnector db = new DbConnector();
            SaleInvoiceItem invoiceItem = new SaleInvoiceItem();
            invoiceItem.Product = product;
            if (invoiceItem.Product != null)
            {
                invoiceItem.Product.GST = db.GetGSTMasterById(invoiceItem.Product.GSTId);
                invoiceItem.ProductId = invoiceItem.Product.ProductId;
                invoiceItem.SalesmanCommision = invoiceItem.Product.SalesmanCommision;
                if(IsWPS == true)
                {
                    invoiceItem.MRP = (invoiceItem.Product.WSPAmount.HasValue ? invoiceItem.Product.WSPAmount.Value : 0);
                }
                else
                {
                    invoiceItem.MRP = invoiceItem.Product.MRP;
                }
                invoiceItem.GSTId = invoiceItem.Product.GSTId;
                invoiceItem.Quantity = 1;
                invoiceItem.Adjustment = invoiceItem.Adjustment;
                invoiceItem.SKUMasterId = invoiceItem.Product.SKUMasterId;
                invoiceItem.PurchaseTaxPerCent = ((invoiceItem.Product.GST.CGST.HasValue ? invoiceItem.Product.GST.CGST.Value : 0) + (invoiceItem.Product.GST.IGST.HasValue ? invoiceItem.Product.GST.IGST.Value : 0) + invoiceItem.Product.GST.SGST);
                invoiceItem.BasicPrice = Math.Round(invoiceItem.MRP / (1 + invoiceItem.PurchaseTaxPerCent.Value / 100) * 10000)/ 10000;
                invoiceItem.Discount = 0;
                invoiceItem.ApplicableTaxValue = Math.Round((((invoiceItem.BasicPrice.Value - invoiceItem.Discount) * (invoiceItem.PurchaseTaxPerCent.Value/ 100) * 100) / 100) * 10000) / 10000;
                invoiceItem.CGST = Math.Round((((invoiceItem.BasicPrice.Value - invoiceItem.Discount) * (((invoiceItem.Product.GST.CGST.HasValue ? invoiceItem.Product.GST.CGST.Value : 0) / 100)) * 100) / 100) * 10000) / 10000;
                invoiceItem.IGST = Math.Round((((invoiceItem.BasicPrice.Value - invoiceItem.Discount) * (((invoiceItem.Product.GST.IGST.HasValue ? invoiceItem.Product.GST.IGST.Value : 0) / 100)) * 100) / 100) * 10000) / 10000;
                invoiceItem.SGST = Math.Round((((invoiceItem.BasicPrice.Value - invoiceItem.Discount) * (((invoiceItem.Product.GST.SGST) / 100)) * 100) / 100) * 10000) / 10000;
                invoiceItem.SubTotal = Math.Round((((invoiceItem.BasicPrice.Value - invoiceItem.Discount + invoiceItem.ApplicableTaxValue) * 100) / 100) * 10000) / 10000;
                invoiceItem.Status = ProductStatus;
                invoiceItem.SaleInvoiceId = saleInvoiceId;
                invoiceItem.ConversionValue = 1;
                if (saleInvoiceId.HasValue && saleInvoiceId.Value > 0)
                {
                    db.SaveSaleInvoiceItem(invoiceItem);
                    invoiceItem.Product = product;
                }
                else
                {
                    SessionManager.AddSaleInvoiceItem(invoiceItem,Type);
                }
            }
            return invoiceItem;
        }

        public static JobSheetItem AddProductionItem(Product product, int? ProductionId)
        {
            DbConnector db = new DbConnector();
            JobSheetItem Item = new JobSheetItem();
            Item.Product = product;
            if (Item.Product != null)
            {
                Item.Product.GST = db.GetGSTMasterById(Item.Product.GSTId);
                Item.ProductId = Item.Product.ProductId;
                Item.MRP = Item.Product.MRP;
                Item.GSTId = Item.Product.GSTId;
                Item.Quantity = 1;
                Item.ApplicableTax = ((Item.Product.GST.CGST.HasValue ? Item.Product.GST.CGST.Value : 0) + (Item.Product.GST.IGST.HasValue ? Item.Product.GST.IGST.Value : 0) + Item.Product.GST.SGST);
                Item.BasicPrice =  (Convert.ToDecimal(Item.MRP / (1 + ((Item.Product.GST.CGST.HasValue ? Item.Product.GST.CGST.Value : 0) + (Item.Product.GST.IGST.HasValue ? Item.Product.GST.IGST.Value : 0) + Item.Product.GST.SGST) / 100)) * 100) / 100;
                Item.ApplicableTaxValue =  ((Item.BasicPrice.Value) * (((Item.Product.GST.CGST.HasValue ? Item.Product.GST.CGST.Value : 0) + (Item.Product.GST.IGST.HasValue ? Item.Product.GST.IGST.Value : 0) + Item.Product.GST.SGST) / 100) * 100) / 100;
                Item.SubTotal = Item.BasicPrice.Value + Item.ApplicableTaxValue;
                Item.Status = Constants.SALE_ITEM_SOLD;
                Item.JobSheetId = ProductionId;
                Item.ConversionValue = 1;
                Item.CGST = Item.Product.GST.CGST;
                Item.IGST = Item.Product.GST.IGST;
                Item.SGST = Item.Product.GST.SGST;
                if (ProductionId.HasValue && ProductionId.Value > 0)
                {
                    db.SaveProductionItem(Item);
                    Item.Product = product;
                }
                else
                {
                    SessionManager.AddJobSheetItem(Item);
                }
            }
            return Item;
        }

        public static void UpdateProduct(PurchaseInvoiceItem item)
        {
            DbConnector db = new DbConnector();

            List<Product> UpdateProductList = db.Products.Where(x => x.PurchaseInvoiceItemId == item.PurchaseInvoiceItemId).ToList();

            foreach(var ProductItem in UpdateProductList)
            {
                Product Product = db.Products.Where(x => x.ProductId == ProductItem.ProductId).FirstOrDefault();
                Product.PurchaseInvoiceItemId = item.PurchaseInvoiceItemId;
                Product.Rate = item.Rate;
                Product.NetAmount = item.NetAmount;
                Product.MRP = item.MRP;
                Product.SalesmanCommision = item.SalesCommission;
                Product.Remark = item.Remarks;
                Product.DiscountsAccessible = item.DiscountsAccessible;
                Product.Adjustment = item.Adjustment;
                Product.HSNId = item.HSNId;
                Product.UnitId = item.UnitId;
                Product.SKUMasterId = item.SKUMasterId;
                Product.BrandId = item.BrandId;
                //Product.DiscountPercent = item.DiscountPercent;
                //Product.DiscountedAmount = item.DiscountedAmount;
                //Product.Discount = item.Discount;
                Product.ApplicableTax = item.ApplicableTax;
                Product.SellInPartial = item.SellInPartial;
                Product.PurchaseInvoiceItemId = item.PurchaseInvoiceItemId;
                Product.LocationId = item.LocationId;
                Product.WSPAmount = item.WSPAmount;
                Product.WSPPercent = item.WSPPercent;
                Product.CGST = item.CGST;
                Product.SGST = item.SGST;
                Product.IGST = item.IGST;
                db.SaveProduct(Product);
            }
        }

        public static SaleInvoiceItem AddPurchaseReturnItem(Product product, int? saleInvoiceId, string Type, string ProductStatus)
        {
            DbConnector db = new DbConnector();
            SaleInvoiceItem invoiceItem = new SaleInvoiceItem();
            invoiceItem.Product = product;
            if (invoiceItem.Product != null)
            {
                invoiceItem.Product.GST = db.GetGSTMasterById(invoiceItem.Product.GSTId);
                invoiceItem.ProductId = invoiceItem.Product.ProductId;
                invoiceItem.SalesmanCommision = invoiceItem.Product.SalesmanCommision;
                invoiceItem.GSTId = invoiceItem.Product.GSTId;
                invoiceItem.Quantity = 1;
                invoiceItem.Adjustment = invoiceItem.Adjustment;
                invoiceItem.SKUMasterId = invoiceItem.Product.SKUMasterId;
                invoiceItem.PurchaseTaxPerCent = ((invoiceItem.Product.GST.CGST ?? 0) + (invoiceItem.Product.GST.IGST ?? 0) + invoiceItem.Product.GST.SGST);
                invoiceItem.BasicPrice = invoiceItem.Product.Rate;
                invoiceItem.MRP = invoiceItem.Product.Rate;
                invoiceItem.Discount = 0;
                invoiceItem.ApplicableTaxValue = Math.Round((((invoiceItem.BasicPrice.Value - invoiceItem.Discount) * (invoiceItem.PurchaseTaxPerCent.Value / 100) * 100) / 100) * 10000) / 10000;
                invoiceItem.CGST = Math.Round((((invoiceItem.BasicPrice.Value - invoiceItem.Discount) * (((invoiceItem.Product.GST.CGST ?? 0) / 100)) * 100) / 100) * 10000) / 10000;
                invoiceItem.IGST = Math.Round((((invoiceItem.BasicPrice.Value - invoiceItem.Discount) * (((invoiceItem.Product.GST.IGST ?? 0) / 100)) * 100) / 100) * 10000) / 10000;
                invoiceItem.SGST = Math.Round((((invoiceItem.BasicPrice.Value - invoiceItem.Discount) * (((invoiceItem.Product.GST.SGST) / 100)) * 100) / 100) * 10000) / 10000;
                invoiceItem.SubTotal = Math.Round((((invoiceItem.BasicPrice.Value - invoiceItem.Discount + invoiceItem.ApplicableTaxValue) * 100) / 100) * 10000) / 10000;
                invoiceItem.Status = ProductStatus;
                invoiceItem.SaleInvoiceId = saleInvoiceId;
                invoiceItem.ConversionValue = 1;
                if (saleInvoiceId.HasValue && saleInvoiceId.Value > 0)
                {
                    db.SaveSaleInvoiceItem(invoiceItem);
                    invoiceItem.Product = product;
                }
                else
                {
                    SessionManager.AddSaleInvoiceItem(invoiceItem, Type);
                }
            }
            return invoiceItem;
        }

        public static SaleInvoiceItem AddInvoiceItemForOrder(Product product, int SalesOrderId)
        {
            DbConnector db = new DbConnector();
            SaleInvoiceItem invoiceItem = new SaleInvoiceItem();
            invoiceItem.Product = product;
            if (invoiceItem.Product != null)
            {
                invoiceItem.Product.GST = db.GetGSTMasterById(invoiceItem.Product.GSTId);
                invoiceItem.ProductId = invoiceItem.Product.ProductId;
                invoiceItem.SalesmanCommision = invoiceItem.Product.SalesmanCommision;
                invoiceItem.MRP = invoiceItem.Product.MRP;
                invoiceItem.GSTId = invoiceItem.Product.GSTId;
                invoiceItem.Quantity = 1;
                invoiceItem.Adjustment = invoiceItem.Adjustment;
                invoiceItem.SKUMasterId = invoiceItem.Product.SKUMasterId;
                invoiceItem.PurchaseTaxPerCent = ((invoiceItem.Product.GST.CGST.HasValue ? invoiceItem.Product.GST.CGST.Value : 0) + (invoiceItem.Product.GST.IGST.HasValue ? invoiceItem.Product.GST.IGST.Value : 0) + invoiceItem.Product.GST.SGST);
                invoiceItem.BasicPrice = Math.Round(invoiceItem.MRP / (1 + invoiceItem.PurchaseTaxPerCent.Value / 100) * 10000) / 10000;
                invoiceItem.Discount = 0;
                invoiceItem.ApplicableTaxValue = Math.Round((((invoiceItem.BasicPrice.Value - invoiceItem.Discount) * (invoiceItem.PurchaseTaxPerCent.Value / 100) * 100) / 100) * 10000) / 10000;
                invoiceItem.CGST = Math.Round((((invoiceItem.BasicPrice.Value - invoiceItem.Discount) * (((invoiceItem.Product.GST.CGST.HasValue ? invoiceItem.Product.GST.CGST.Value : 0) / 100)) * 100) / 100) * 10000) / 10000;
                invoiceItem.IGST = Math.Round((((invoiceItem.BasicPrice.Value - invoiceItem.Discount) * (((invoiceItem.Product.GST.IGST.HasValue ? invoiceItem.Product.GST.IGST.Value : 0) / 100)) * 100) / 100) * 10000) / 10000;
                invoiceItem.SGST = Math.Round((((invoiceItem.BasicPrice.Value - invoiceItem.Discount) * (((invoiceItem.Product.GST.SGST) / 100)) * 100) / 100) * 10000) / 10000;
                invoiceItem.SubTotal = Math.Round((((invoiceItem.BasicPrice.Value - invoiceItem.Discount + invoiceItem.ApplicableTaxValue) * 100) / 100) * 10000) / 10000;
                invoiceItem.Status = Constants.SALE_ITEM_SAVE_FOR_ORDER;
                invoiceItem.SalesOrderId = SalesOrderId;
                invoiceItem.ConversionValue = 1;
                if (SalesOrderId > 0)
                {
                    db.SaveSaleInvoiceItem(invoiceItem);
                    invoiceItem.Product = product;
                }
                else
                {
                    SessionManager.AddSaleInvoiceItem(invoiceItem,Constants.SALE_TYPE_SALE_ORDER);
                }
            }
            return invoiceItem;
        }

        public static SaleInvoiceItem AddInternalTransferItem(Product product, int? InternalTransferId, string Type, string ProductStatus)
        {
            DbConnector db = new DbConnector();
            SaleInvoiceItem invoiceItem = new SaleInvoiceItem();
            invoiceItem.Product = product;
            if (invoiceItem.Product != null)
            {
                invoiceItem.Product.GST = db.GetGSTMasterById(invoiceItem.Product.GSTId);
                invoiceItem.ProductId = invoiceItem.Product.ProductId;
                invoiceItem.SalesmanCommision = invoiceItem.Product.SalesmanCommision;
                invoiceItem.MRP = invoiceItem.Product.MRP;
                invoiceItem.GSTId = invoiceItem.Product.GSTId;
                invoiceItem.Quantity = 1;
                invoiceItem.Adjustment = invoiceItem.Adjustment;
                invoiceItem.SKUMasterId = invoiceItem.Product.SKUMasterId;
                invoiceItem.PurchaseTaxPerCent = ((invoiceItem.Product.GST.CGST.HasValue ? invoiceItem.Product.GST.CGST.Value : 0) + (invoiceItem.Product.GST.IGST.HasValue ? invoiceItem.Product.GST.IGST.Value : 0) + invoiceItem.Product.GST.SGST);
                invoiceItem.BasicPrice = Math.Round(invoiceItem.MRP / (1 + invoiceItem.PurchaseTaxPerCent.Value / 100) * 10000) / 10000;
                invoiceItem.Discount = 0;
                invoiceItem.ApplicableTaxValue = Math.Round((((invoiceItem.BasicPrice.Value - invoiceItem.Discount) * (invoiceItem.PurchaseTaxPerCent.Value / 100) * 100) / 100) * 10000) / 10000;
                invoiceItem.CGST = Math.Round((((invoiceItem.BasicPrice.Value - invoiceItem.Discount) * (((invoiceItem.Product.GST.CGST.HasValue ? invoiceItem.Product.GST.CGST.Value : 0) / 100)) * 100) / 100) * 10000) / 10000;
                invoiceItem.IGST = Math.Round((((invoiceItem.BasicPrice.Value - invoiceItem.Discount) * (((invoiceItem.Product.GST.IGST.HasValue ? invoiceItem.Product.GST.IGST.Value : 0) / 100)) * 100) / 100) * 10000) / 10000;
                invoiceItem.SGST = Math.Round((((invoiceItem.BasicPrice.Value - invoiceItem.Discount) * (((invoiceItem.Product.GST.SGST) / 100)) * 100) / 100) * 10000) / 10000;
                invoiceItem.SubTotal = Math.Round((((invoiceItem.BasicPrice.Value - invoiceItem.Discount + invoiceItem.ApplicableTaxValue) * 100) / 100) * 10000) / 10000;
                invoiceItem.Status = ProductStatus;
                invoiceItem.InternalTransferId = InternalTransferId;
                invoiceItem.ConversionValue = 1;
                if (InternalTransferId.HasValue && InternalTransferId.Value > 0)
                {
                    db.SaveSaleInvoiceItem(invoiceItem);
                    invoiceItem.Product = product;
                }
                else
                {
                    SessionManager.AddSaleInvoiceItem(invoiceItem, Type);
                }
            }
            return invoiceItem;
        }
    }
}

