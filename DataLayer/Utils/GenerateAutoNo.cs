﻿using System;
using System.Linq;
using BarCode.Models;
using DataLayer.Models;
using System.Collections.Generic;

namespace DataLayer.Utils
{
    public class GenerateAutoNo
    {
        static DbConnector db = new DbConnector();

        public static string GenerateProductNo(int CompanyId)
        {
            barcodecs objbar = new barcodecs();
            string productnumber = db.Products.Where(x => x.CompanyId == CompanyId).Max(x => x.SerialNumber);
            if (productnumber == null)
            {
                productnumber = "1";
            }
            else
            {
                int id = Int32.Parse(productnumber);
                id = id + 1;
                productnumber = id.ToString();
            }
            return productnumber.PadLeft(10, '0');
        }

        public string GenerateEmployeeNumber()
        {
            string id = db.Contacts.Where(x => x.Type == "Employee").Max(x => x.EmployeeNumber);
            if (id == null)
            {
                id = "1";
            }
            else
            {
                id = id.Substring(3, 6);
                id = (int.Parse(id) + 1).ToString();
            }
            string EmployeeNo = id.ToString();
            EmployeeNo = "EP-" + EmployeeNo.PadLeft(6, '0');
            return EmployeeNo;
        }

        public string GenerateCategoryNo()
        {
            string id = db.GetLastCategoryNo();
            if (id == null)
            {
                id = "1";
            }
            else
            {
                int CategoryNumber = Int32.Parse(id);
                CategoryNumber = CategoryNumber + 1;
                id = CategoryNumber.ToString();
            }
            string CategoryNo = id.ToString();
            CategoryNo = CategoryNo.PadLeft(6, '0');
            return CategoryNo;
        }

        public string GenerateSubCategoryNo()
        {
            int id = db.GetLastSubCategoryNo();
            if (id == 0)
            {
                id = 1;
            }
            else
            {
                id = id + 1;
            }
            string SubCategoryNo = id.ToString();
            SubCategoryNo = SubCategoryNo.PadLeft(6, '0');
            return SubCategoryNo;
        }

        #region Customer
        public string GenerateCustomerNumber()
        {
            string id = db.Contacts.Where(x => x.Type == "Customer").Max(x => x.CustomerNumber);
            if (id == null)
            {
                id = "1";
            }
            else
            {
                id = id.Substring(3, 6);
                id = (int.Parse(id) + 1).ToString();
            }
            string CustomerNumber = id.ToString();
            CustomerNumber = "CN-" + CustomerNumber.PadLeft(6, '0');
            return CustomerNumber;
        }
        #endregion

        public static string GenerateVoucherNo()
        {
            DbConnector db = new DbConnector();
            Company company = db.Companys.Where(x => x.CompanyId == db.CompanyId).FirstOrDefault();
            DateTime FinacialYear = new DateTime(DateTime.Now.Year, company.FinacialYearStarting.Month, company.FinacialYearStarting.Day);
            string id = db.Vouchers.Where(x => x.CompanyId == db.CompanyId && x.EntryDate >= FinacialYear).Max(x => x.VoucherNumber);
            if (id == null)
            {
                id = "1";
            }
            else
            {
                id = (Int32.Parse(id) + 1).ToString();
            }
            return id;
        }

        public static string GenerateVoucherNoForType(string VoucherFor)
        {
            DbConnector db = new DbConnector();
            string id = db.Vouchers.AsEnumerable().Where(x => x.VoucherFor == VoucherFor && x.CompanyId == db.CompanyId && x.VoucherNumber != null).Distinct().OrderByDescending(x => int.Parse(x.VoucherNumber == null ? (0).ToString() : x.VoucherNumber))
                      .Select(x => x.VoucherNumber == null ? (0).ToString() : x.VoucherNumber).Take(500)
                   .FirstOrDefault();           
            if (id == null)
            {
                id = "1";
            }
            else
            {
                id = (Int32.Parse(id) + 1).ToString();
            }
            return id;
        }

        public string GeneratePurchaseOrderNo()
        {
            int id = db.GetLastPurchaseOrderNo();
            if (id == 0)
            {
                id = 1;
            }
            else
            {
                id = id + 1;
            }
            string PurchaseOrderNo = id.ToString();
            PurchaseOrderNo = "PO-" + PurchaseOrderNo.PadLeft(6, '0');
            return PurchaseOrderNo;
        }

        #region Sales Order
        public string GenerateSalesOrderNo(int CompanyId)
        {
            DbConnector db = new DbConnector();
            string SaleOrderNumber = db.SalesOrders.Where(x => x.CompanyId == CompanyId).Max(x => x.SaleOrderNumber);
            if (SaleOrderNumber == null)
            {
                SaleOrderNumber = "1";
            }
            else
            {
                int id = Int32.Parse(SaleOrderNumber);
                id = id + 1;
                SaleOrderNumber = id.ToString();
            }
            SaleOrderNumber = SaleOrderNumber.PadLeft(8 , '0');
            return SaleOrderNumber;
        }
        #endregion

        #region SupplierMaster Number
        public string GenerateSupplierNumber()
        {
            string id = db.Contacts.Where(x => x.Type == "Supplier").Max(x => x.SupplierNumber);
            if (id == null)
            {
                id = "1";
            }
            else
            {
                id = id.Substring(3, 6);
                id = (int.Parse(id) + 1).ToString();
            }
            string SupplierMasterNo = id.ToString();
            SupplierMasterNo = "SM-" + SupplierMasterNo.PadLeft(6, '0');
            return SupplierMasterNo;
        }
        #endregion

        public string GenerateJobSheetNumber(int ? CompanyId)
        {
            string ProductNumber = db.JobSheets.Where(x => x.CompanyId == CompanyId && x.JobSheetStatus != Enums.JobSheetStatus.FurtherProcess).Max(x => x.ProductNumber);
            if (ProductNumber == null)
            {
                ProductNumber = "1";
            }
            else
            {
                int id = Int32.Parse(ProductNumber);
                id = id + 1;
                ProductNumber = id.ToString();
            }
            ProductNumber = ProductNumber.PadLeft(7, '0');
            return ProductNumber;
        }

        #region Sale Invoice Number
        public string GenerateSaleInvoiceNo(DateTime Invoicedate)
        {
            DbConnector db = new DbConnector();
            Company company = db.Companys.Where(x => x.CompanyId == db.CompanyId).FirstOrDefault();
            DateTime FinacialYear =  new DateTime();
            DateTime FinacialYearList = new DateTime();
            if(Invoicedate.Month >= company.FinacialYearStarting.Month)
            {
                FinacialYear = new DateTime(Invoicedate.Year, company.FinacialYearStarting.Month, company.FinacialYearStarting.Day);
                FinacialYearList = FinacialYear.AddYears(1);
            }
            else
            {
                FinacialYearList = new DateTime(Invoicedate.Year, company.FinacialYearStarting.Month, company.FinacialYearStarting.Day);
                FinacialYear = FinacialYearList.AddYears(-1);
            }
            string SaleInvoiceNumber = db.SaleInvoices.Where(x => (x.Type == Constants.SALE_TYPE_SALE_INVOICE || x.Type == Constants.SALE_TYPE_SALE_VOUCHER || x.Type == Constants.SALE_TYPE_WPS) && x.CompanyId == db.CompanyId && FinacialYear <= x.InvoicingDate && x.InvoicingDate < FinacialYearList).Max(x => x.SaleInvoiceNo);
            if (SaleInvoiceNumber == null)
            {
                SaleInvoiceNumber = "1";
            }
            else
            {
                int id = Int32.Parse(SaleInvoiceNumber);
                id = id + 1;
                SaleInvoiceNumber = id.ToString();
            }
            SaleInvoiceNumber = SaleInvoiceNumber.PadLeft(8, '0');
            return SaleInvoiceNumber;
        }


        public string GenerateDeliveryMemoNo()
        {
            DbConnector db = new DbConnector();
            Company company = db.Companys.Where(x => x.CompanyId == db.CompanyId).FirstOrDefault();
            DateTime FinacialYear = new DateTime(DateTime.Now.Year, company.FinacialYearStarting.Month, company.FinacialYearStarting.Day);
            string SaleInvoiceNumber = db.SaleInvoices.Where(x => (x.Type == Constants.SALE_TYPE_DELIVERY_MEMO) && x.CompanyId == db.CompanyId && x.InvoicingDate >= FinacialYear).Max(x => x.SaleInvoiceNo);
            if (SaleInvoiceNumber == null)
            {
                SaleInvoiceNumber = "1";
            }
            else
            {
                int id = Int32.Parse(SaleInvoiceNumber);
                id = id + 1;
                SaleInvoiceNumber = id.ToString();
            }
            SaleInvoiceNumber = SaleInvoiceNumber.PadLeft(8, '0');
            return SaleInvoiceNumber;
        }

        public string GenerateInvoiceNoForType(SaleInvoice Invoicedate)
        {
            DbConnector db = new DbConnector();
            Company company = db.Companys.Where(x => x.CompanyId == Invoicedate.CompanyId).FirstOrDefault();
            DateTime FinacialYear = new DateTime();
            DateTime FinacialYearList = new DateTime();
            if (Invoicedate.InvoicingDate.Month >= company.FinacialYearStarting.Month)
            {
                FinacialYear = new DateTime(Invoicedate.InvoicingDate.Year, company.FinacialYearStarting.Month, company.FinacialYearStarting.Day);
                FinacialYearList = FinacialYear.AddYears(1);
            }
            else
            {
                FinacialYearList = new DateTime(Invoicedate.InvoicingDate.Year, company.FinacialYearStarting.Month, company.FinacialYearStarting.Day);
                FinacialYear = FinacialYearList.AddYears(-1);
            }
            string ReturnInvoiceNo = db.SaleInvoices.Where(x => (x.Type == Invoicedate.Type) && x.CompanyId == Invoicedate.CompanyId && FinacialYear <= x.InvoicingDate && x.InvoicingDate < FinacialYearList).Max(x => x.SaleInvoiceNo);
            if (ReturnInvoiceNo == null)
            {
                ReturnInvoiceNo = "1";
            }
            else
            {
                int id = Int32.Parse(ReturnInvoiceNo);
                id = id + 1;
                ReturnInvoiceNo = id.ToString();
            }
            ReturnInvoiceNo = ReturnInvoiceNo.PadLeft(8, '0');
            return ReturnInvoiceNo;
        }

        public string GenerateInternalTransferNo()
        {
            DbConnector db = new DbConnector();
            Company company = db.Companys.Where(x => x.CompanyId == db.CompanyId).FirstOrDefault();
            DateTime FinacialYear = new DateTime(DateTime.Now.Year, company.FinacialYearStarting.Day, company.FinacialYearStarting.Month);
            string TransfersNo = db.InternalTransfers.Where(x => x.CompanyId == db.CompanyId && x.TransferDate >= FinacialYear).Max(x => x.InternalTransferNo);
            if (TransfersNo == null)
            {
                TransfersNo = "1";
            }
            else
            {
                int id = Int32.Parse(TransfersNo);
                id = id + 1;
                TransfersNo = id.ToString();
            }
            TransfersNo = TransfersNo.PadLeft(8, '0');
            return TransfersNo;
        }
        #endregion

        public string GenerateFurtherJobSheetNumber(int JobSheetId,int? CompanyId)
        {
            string ProductNumber = db.JobSheets.Where(x => x.CompanyId == CompanyId && x.JobSheetStatus == Enums.JobSheetStatus.FurtherProcess && x.ParentJobSheetId == JobSheetId).Max(x => x.ProductNumber);
            if (ProductNumber == null)
            {
                ProductNumber = "1";
            }
            else
            {
                int id = Int32.Parse(ProductNumber);
                id = id + 1;
                ProductNumber = id.ToString();
            }
            ProductNumber = ProductNumber.PadLeft(5, '0');
            return ProductNumber;
        }
    }
}


