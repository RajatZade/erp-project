﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataLayer;
using DataLayer.Models;
namespace DataLayer.Utils
{
    public class SessionManager
    {
        private static DbConnector _db = new DbConnector();

        public static LoginUser GetSessionUser()
        {
            if (HttpContext.Current.Session[Constants.SESSION_USER] != null)
            {
                return HttpContext.Current.Session[Constants.SESSION_USER] as LoginUser;
            }
            return null;
        }

        public static void AbandonSession()
        {
            HttpContext.Current.Session.Abandon();
        }

        public static void SetSessionBranch(Branch branch)
        {
            branch.Company = null;
            HttpContext.Current.Session[Constants.SESSION_Branch] = branch;
        }

        public static void SetSessionCompany(Company company)
        {
            HttpContext.Current.Session[Constants.SESSION_Company] = company;
        }

        public static void SetSessionDivision(Division division)
        {
            division.Company = null;
            HttpContext.Current.Session[Constants.SESSION_Division] = division;
        }

        public static Branch GetSessionBranch()
        {
            if (HttpContext.Current.Session[Constants.SESSION_Branch] != null)
            {
                return HttpContext.Current.Session[Constants.SESSION_Branch] as Branch;
            }
            return null;
        }

        public static Company GetSessionCompany()
        {
            if (HttpContext.Current.Session[Constants.SESSION_Company] != null)
            {
                return HttpContext.Current.Session[Constants.SESSION_Company] as Company;
            }
            return null;
        }

        public static Division GetSessionDivision()
        {
            if (HttpContext.Current.Session[Constants.SESSION_Division] != null)
            {
                return HttpContext.Current.Session[Constants.SESSION_Division] as Division;
            }
            return null;
        }

        public static void EmptySessionCompany()
        {
            HttpContext.Current.Session[Constants.SESSION_Company] = null;
            HttpContext.Current.Session[Constants.SESSION_Division] = null;
            HttpContext.Current.Session[Constants.SESSION_Branch] = null;
            HttpContext.Current.Session[Constants.BARCODE_PRODUCTS] = null;
        }

        #region Budgeting
        public static List<Budgeting> GetBudgetList()
        {
            List<Budgeting> BudgetingList = HttpContext.Current.Session[Constants.BUDGETING_LIST] as List<Budgeting>;
            if (BudgetingList == null)
            {
                BudgetingList = new List<Budgeting>();
            }
            return BudgetingList;
        }

        public static void EmptyBudgetList()
        {
            HttpContext.Current.Session[Constants.BUDGETING_LIST] = null;
        }

        public static void AddBudget(Budgeting model)
        {
            DbConnector db = new DbConnector();
            List<Budgeting> BudgetingList = HttpContext.Current.Session[Constants.BUDGETING_LIST] as List<Budgeting>;
            if (BudgetingList == null || BudgetingList.Count == 0)
            {
                int id = db.Budgetings.Max(x => (int?)x.BudgetingId) ?? 0;
                model.BudgetingId = id + 1;
                BudgetingList = new List<Budgeting>
                {
                    model
                };
            }
            else
            {
                var item = BudgetingList.Where(x => x.BudgetingId == model.BudgetingId).FirstOrDefault();
                if (item == null)
                {
                    Budgeting Budgeting = BudgetingList.OrderByDescending(x => x.BudgetingId).FirstOrDefault();
                    model.BudgetingId = Budgeting.BudgetingId + 1;
                    BudgetingList.Add(model);
                }
                else
                {
                    item.LedgerId = model.LedgerId;
                    item.AnnualBudget = model.AnnualBudget;
                    item.MonthlyBudget = model.MonthlyBudget;
                    item.QuarterlyBudget = model.QuarterlyBudget;
                    item.Percent = model.Percent;
                }
            }
            HttpContext.Current.Session[Constants.BUDGETING_LIST] = BudgetingList;
        }
        #endregion

        #region Product & Barcode
        public static List<Product> GetWSPProductList()
        {
            List<Product> WSPProductList = HttpContext.Current.Session[Constants.WSP_PRODUCT_LIST] as List<Product>;
            if (WSPProductList == null)
            {
                WSPProductList = new List<Product>();
            }
            return WSPProductList;
        }

        public static void SetWSPProductList(List<Product> ProductList)
        {
            HttpContext.Current.Session[Constants.WSP_PRODUCT_LIST] = ProductList;
        }

        public static List<Product> GetBarcodeQueueList()
        {
            List<Product> ItemList = HttpContext.Current.Session[Constants.BARCODE_PRODUCTS] as List<Product>;
            if (ItemList == null)
            {
                ItemList = new List<Product>();
            }
            return ItemList;
        }

        public static void AddItemInBarcodeQueue(Product product)
        {
            List<Product> BarcodeQueue = HttpContext.Current.Session[Constants.BARCODE_PRODUCTS] as List<Product>;
            if (BarcodeQueue == null)
            {
                BarcodeQueue = new List<Product>
                        {
                            product
                        };
            }
            else
            {
                BarcodeQueue.Add(product);
            }

            HttpContext.Current.Session[Constants.BARCODE_PRODUCTS] = BarcodeQueue;
        }

        public static void RemoveItemFromBarcodeQueue(int productId)
        {
            List<Product> BarcodeQueue = HttpContext.Current.Session[Constants.BARCODE_PRODUCTS] as List<Product>;
            var item = BarcodeQueue.Where(x => x.ProductId == productId).FirstOrDefault();
            BarcodeQueue.Remove(item);
        }
        #endregion

        #region Lead Item
        public static void SetLeadItem(LeadItem model)
        {
            DbConnector db = new DbConnector();
            List<LeadItem> LeadItemList = HttpContext.Current.Session[Constants.LEAD_ITEM_LIST] as List<LeadItem>;
            if (LeadItemList == null || LeadItemList.Count == 0)
            {
                model.LeadItemId = db.GetLastLeadItemId();
                LeadItemList = new List<LeadItem>
                {
                    model
                };
            }
            else
            {
                var item = LeadItemList.Where(x => x.LeadItemId == model.LeadItemId).FirstOrDefault();
                if (item == null)
                {
                    LeadItem leaditem = LeadItemList.OrderByDescending(x => x.LeadItemId).FirstOrDefault();
                    model.LeadItemId = leaditem.LeadItemId + 1;
                    LeadItemList.Add(model);
                }
                else
                {
                    item.Rate = model.Rate;
                    item.LeadProoductStatus = model.LeadProoductStatus;
                    item.ProductName = model.ProductName;
                    item.Quantity = model.Quantity;
                }
            }
            HttpContext.Current.Session[Constants.LEAD_ITEM_LIST] = LeadItemList;
        }

        public static List<LeadItem> GetLeadItemList()
        {
            List<LeadItem> LeadItemList = HttpContext.Current.Session[Constants.LEAD_ITEM_LIST] as List<LeadItem>;
            if (LeadItemList == null)
            {
                LeadItemList = new List<LeadItem>();
            }
            return LeadItemList;
        }

        public static void RemoveLeadItem(int LeadItemId)
        {
            List<LeadItem> LeadItemList = HttpContext.Current.Session[Constants.LEAD_ITEM_LIST] as List<LeadItem>;
            if (LeadItemList != null)
            {
                LeadItem model = LeadItemList.Where(x => x.LeadItemId == LeadItemId).FirstOrDefault();
                LeadItemList.Remove(model);
            }
            HttpContext.Current.Session[Constants.LEAD_ITEM_LIST] = LeadItemList;
        }
        #endregion

        #region Measurment Set
        public static void SetMeasurmentSet(MeasurmentSet model)
        {
            DbConnector db = new DbConnector();
            List<MeasurmentSet> MeasurmentSetList = HttpContext.Current.Session[Constants.JOBSHEET_MEASURMENTSET] as List<MeasurmentSet>;
            if (MeasurmentSetList == null || MeasurmentSetList.Count == 0)
            {
                model.MeasurmentSetId = db.GetLastServiceIdNumber();
                MeasurmentSetList = new List<MeasurmentSet>
                {
                    model
                };
            }
            else
            {
                var item = MeasurmentSetList.Where(x => x.MeasurmentSetId == model.MeasurmentSetId).FirstOrDefault();
                if (item == null)
                {
                    MeasurmentSet measurmentSet = MeasurmentSetList.OrderByDescending(x => x.MeasurmentSetId).FirstOrDefault();
                    model.MeasurmentSetId = measurmentSet.MeasurmentSetId + 1;
                    MeasurmentSetList.Add(model);
                }
                else
                {
                    item.MeasurmentValues = model.MeasurmentValues;
                    item.Quantity = model.Quantity;
                }
            }
            HttpContext.Current.Session[Constants.JOBSHEET_MEASURMENTSET] = MeasurmentSetList;
        }

        public static List<MeasurmentSet> GetMeasurmentSetList()
        {
            List<MeasurmentSet> MeasurmentSetList = HttpContext.Current.Session[Constants.JOBSHEET_MEASURMENTSET] as List<MeasurmentSet>;
            if (MeasurmentSetList == null)
            {
                MeasurmentSetList = new List<MeasurmentSet>();
            }
            return MeasurmentSetList;
        }

        public static void RemoveMeasurmentSet(int MeasurmentSetId)
        {
            List<MeasurmentSet> MeasurmentSetList = HttpContext.Current.Session[Constants.JOBSHEET_MEASURMENTSET] as List<MeasurmentSet>;
            if (MeasurmentSetList != null)
            {
                var item = MeasurmentSetList.Where(x => x.MeasurmentSetId == MeasurmentSetId).FirstOrDefault();
                MeasurmentSetList.Remove(item);
            }
            HttpContext.Current.Session[Constants.JOBSHEET_MEASURMENTSET] = MeasurmentSetList;
        }

        public static int GetMeasurmente()
        {
            List<MeasurmentSet> AttributeList = HttpContext.Current.Session[Constants.MEASURMENT_ITEM_LIST] as List<MeasurmentSet>;
            MeasurmentSet Model = AttributeList.OrderByDescending(x => x.MeasurmentSetId).FirstOrDefault();
            int id = Model.MeasurmentSetId + 1;
            return id;
        }
        #endregion

        #region Job Sheet Item
        public static List<JobSheetItem> GetJobSheetItemList()
        {
            List<JobSheetItem> JobSheetItemList = HttpContext.Current.Session[Constants.JOBSHEET_ITEM_LIST] as List<JobSheetItem>;
            if (JobSheetItemList == null)
            {
                JobSheetItemList = new List<JobSheetItem>();
            }
            return JobSheetItemList;
        }

        public static void RemoveJobSheetItem(int ProductId)
        {
            List<JobSheetItem> JobSheetItemList = HttpContext.Current.Session[Constants.JOBSHEET_ITEM_LIST] as List<JobSheetItem>;
            if (JobSheetItemList != null)
            {
                var item = JobSheetItemList.Where(x => x.Product.ProductId == ProductId).FirstOrDefault();
                JobSheetItemList.Remove(item);
            }
            HttpContext.Current.Session[Constants.JOBSHEET_ITEM_LIST] = JobSheetItemList;
        }

        public static void UpdateJobSheetItem(JobSheetItem JobSheetItem)
        {
            DbConnector db = new DbConnector();
            List<JobSheetItem> ProductionItemList = HttpContext.Current.Session[Constants.JOBSHEET_ITEM_LIST] as List<JobSheetItem>;
            if (ProductionItemList != null)
            {
                var item = ProductionItemList.Where(x => x.Product.ProductId == JobSheetItem.ProductId).FirstOrDefault();
                if (item != null)
                {
                    item.UnitId = JobSheetItem.UnitId;
                    item.ConversionValue = JobSheetItem.ConversionValue;
                    item.Quantity = JobSheetItem.Quantity;
                }
            }
            HttpContext.Current.Session[Constants.JOBSHEET_ITEM_LIST] = ProductionItemList;
        }

        public static void AddJobSheetItem(JobSheetItem item)
        {
            List<JobSheetItem> ProductionItemList = HttpContext.Current.Session[Constants.JOBSHEET_ITEM_LIST] as List<JobSheetItem>;
            if (ProductionItemList == null)
            {
                ProductionItemList = new List<JobSheetItem>
                {
                    item
                };
            }
            else
            {
                ProductionItemList.Add(item);
            }
            HttpContext.Current.Session[Constants.JOBSHEET_ITEM_LIST] = ProductionItemList;
        }
        #endregion

        #region Job Sheet Services
        public static void SetJobService(JobSheetService model)
        {
            DbConnector db = new DbConnector();
            List<JobSheetService> ServiceList = HttpContext.Current.Session[Constants.JOBSHEET_SERVICE] as List<JobSheetService>;
            if (ServiceList == null || ServiceList.Count == 0)
            {
                model.JobSheetServiceId = db.GetLastServiceIdNumber();
                ServiceList = new List<JobSheetService>
                {
                    model
                };
            }
            else
            {
                var item = ServiceList.Where(x => x.JobSheetServiceId == model.JobSheetServiceId).FirstOrDefault();
                if (item == null)
                {
                    JobSheetService service = ServiceList.OrderByDescending(x => x.JobSheetServiceId).FirstOrDefault();
                    model.JobSheetServiceId = service.JobSheetServiceId + 1;
                    ServiceList.Add(model);
                }
                else
                {
                    item.Price = model.Price;
                    item.JobSheetId = model.JobSheetId;
                    item.ServiceTypeId = model.ServiceTypeId;
                    item.Quantity = model.Quantity;
                    item.JobSheetServiceId = model.JobSheetServiceId;
                    item.GSTId = model.GSTId;
                }
            }
            HttpContext.Current.Session[Constants.JOBSHEET_SERVICE] = ServiceList;
        }

        public static List<JobSheetService> GetServiceList()
        {
            List<JobSheetService> ServiceList = HttpContext.Current.Session[Constants.JOBSHEET_SERVICE] as List<JobSheetService>;
            return ServiceList;
        }

        public static void RemoveJobService(int JobSheetServiceId)
        {
            List<JobSheetService> ServiceList = HttpContext.Current.Session[Constants.JOBSHEET_SERVICE] as List<JobSheetService>;
            if (ServiceList != null)
            {
                JobSheetService model = ServiceList.Where(x => x.JobSheetServiceId == JobSheetServiceId).FirstOrDefault();
                ServiceList.Remove(model);
            }
            HttpContext.Current.Session[Constants.JOBSHEET_SERVICE] = ServiceList;
        }
        #endregion

        #region AttributeLinking
        public static List<AttributeLinking> GetAttributeList(int? PurchaseitemID)
        {
            List<AttributeLinking> AttributeList = null;
            DbConnector db = new DbConnector();
            if (PurchaseitemID > 0)
            {
                AttributeList = db.GetAttributeLinkingByID(PurchaseitemID.Value);
            }
            else
            {
                AttributeList = HttpContext.Current.Session[Constants.ATTRIBUTE_LIST] as List<AttributeLinking>;
            }
            return AttributeList;
        }

        public static void SetAttribute(AttributeLinking Attribute)
        {
            DbConnector db = new DbConnector();
            List<AttributeLinking> AttributeList = HttpContext.Current.Session[Constants.ATTRIBUTE_LIST] as List<AttributeLinking>;
            if (AttributeList == null || AttributeList.Count == 0)
            {
                Attribute.AttributeLinkingId = db.GetLastAttributeNumber();
                AttributeList = new List<AttributeLinking>
                {
                    Attribute
                };
            }
            else
            {
                var item = AttributeList.Where(x => x.AttributeLinkingId == Attribute.AttributeLinkingId).FirstOrDefault();
                if (item == null)
                {
                    Attribute.AttributeLinkingId = GetMaxAttribute();
                    AttributeList.Add(Attribute);
                }
                else
                {
                    item.Value = Attribute.Value;
                    item.Quantity = Attribute.Quantity;
                }
            }
            HttpContext.Current.Session[Constants.ATTRIBUTE_LIST] = AttributeList;
        }

        public static List<AttributeLinking> GetAttributeList()
        {
            List<AttributeLinking> AttributeList = HttpContext.Current.Session[Constants.ATTRIBUTE_LIST] as List<AttributeLinking>;
            return AttributeList;
        }

        public static int GetMaxAttribute()
        {
            List<AttributeLinking> AttributeList = HttpContext.Current.Session[Constants.ATTRIBUTE_LIST] as List<AttributeLinking>;
            AttributeLinking Attribute = AttributeList.OrderByDescending(x => x.AttributeLinkingId).FirstOrDefault();
            int id = Attribute.AttributeLinkingId + 1;
            return id;
        }

        public static void RemoveAttribute(int LinkingId)
        {
            List<AttributeLinking> AttributeList = HttpContext.Current.Session[Constants.ATTRIBUTE_LIST] as List<AttributeLinking>;
            if (AttributeList != null)
            {
                var item = AttributeList.Where(x => x.AttributeLinkingId == LinkingId).FirstOrDefault();
                AttributeList.Remove(item);
            }
            HttpContext.Current.Session[Constants.ATTRIBUTE_LIST] = AttributeList;
        }

        public static AttributeLinking GetAttributeList(int LinkingId)
        {

            List<AttributeLinking> AttributeList = HttpContext.Current.Session[Constants.ATTRIBUTE_LIST] as List<AttributeLinking>;
            AttributeLinking item = null;
            if (AttributeList == null)
            {

                item = _db.GetAttributeLinkingListByLinkingId(LinkingId);
            }
            else
            {
                item = AttributeList.Where(x => x.AttributeLinkingId == LinkingId).FirstOrDefault();
            }
            return item;
        }
        #endregion

        #region Sale Invoice Item
        public static List<SaleInvoiceItem> GetInvoiceItemList(string Type)
        {
            List<SaleInvoiceItem> SaleInvoiceItemList = HttpContext.Current.Session[Type] as List<SaleInvoiceItem>;
            if (SaleInvoiceItemList == null)
            {
                SaleInvoiceItemList = new List<SaleInvoiceItem>();
            }
            return SaleInvoiceItemList;
        }

        public static void UpdateOrderItem(SaleInvoiceItem SaleInvoiceItem, string Type)
        {
            DbConnector db = new DbConnector();
            List<SaleInvoiceItem> SaleInvoiceItemList = HttpContext.Current.Session[Type] as List<SaleInvoiceItem>;
            if (SaleInvoiceItemList != null)
            {
                var item = SaleInvoiceItemList.Where(x => x.Product.ProductId == SaleInvoiceItem.ProductId).FirstOrDefault();
                if (item != null)
                {
                    item.Product.SalesmanCommision = SaleInvoiceItem.SalesmanCommision.Value;
                    item.SalesmanCommision = SaleInvoiceItem.SalesmanCommision.Value;
                    item.Adjustment = SaleInvoiceItem.Adjustment;
                    item.MRP = SaleInvoiceItem.MRP;
                    item.Discount = SaleInvoiceItem.Discount;
                    item.Status = SaleInvoiceItem.Status;
                    item.GSTId = SaleInvoiceItem.GSTId;
                    item.UnitId = SaleInvoiceItem.UnitId;
                    item.ConversionValue = SaleInvoiceItem.ConversionValue;
                    item.DiscountId = SaleInvoiceItem.DiscountId ?? 0;
                    item.Quantity = SaleInvoiceItem.Quantity;
                    if (item.GSTId > 0)
                    {
                        item.GST = db.GetGSTMasterById(item.GSTId);
                        item.ApplicableTax = ((item.GST.CGST ?? 0) + (item.GST.IGST ?? 0) + item.GST.SGST);
                    }
                    else
                    {
                        item.GST = new GST
                        {
                            CGST = 0,
                            SGST = 0,
                            IGST = 0
                        };
                        item.ApplicableTax = 0;
                    }

                    if (item.Product.SellInPartial)
                    {
                        item.BasicPrice = Math.Round(((((item.MRP * item.ConversionValue.Value) / (1 + ((item.ApplicableTax) / 100))) * 100) / 100) * 10000) / 10000;
                    }
                    else
                    {
                        item.BasicPrice = Math.Round((((item.MRP / (1 + ((item.ApplicableTax) / 100))) * 100) / 100) * 10000) / 10000;
                    }

                    if (item.DiscountId.HasValue)
                    {
                        Decimal Percent = db.Discounts.Where(x => x.DiscountId == item.DiscountId).Select(x => x.DiscountPercent.Value).FirstOrDefault();
                        item.Discount = Math.Round((item.BasicPrice.Value * (Percent / 100)) * 10000) / 10000;
                    }
                    else
                    {
                        item.Discount = 0;
                    }
                    item.BasicPrice = item.BasicPrice - item.Discount;
                    item.ApplicableTaxValue = Math.Round((((item.BasicPrice.Value) * (item.ApplicableTax / 100))) * 10000) / 10000;
                    item.CGST = Math.Round((((item.BasicPrice.Value) * (item.GST.CGST ?? 0) / 100)) * 10000) / 10000;
                    item.SGST = Math.Round((((item.BasicPrice.Value) * (item.GST.SGST) / 100)) * 10000) / 10000;
                    item.IGST = Math.Round((((item.BasicPrice.Value) * (item.GST.IGST ?? 0) / 100)) * 10000) / 10000;
                    item.SubTotal = Math.Round(((((item.BasicPrice.Value + item.Adjustment) * 100) / 100) + item.ApplicableTaxValue) * 10000) / 10000;
                    item.Status = Constants.SALE_ITEM_SOLD;
                }
            }
            HttpContext.Current.Session[Type] = SaleInvoiceItemList;
        }

        public static void UpdatePucrahseReturnItem(SaleInvoiceItem SaleInvoiceItem, string Type)
        {
            DbConnector db = new DbConnector();
            List<SaleInvoiceItem> SaleInvoiceItemList = HttpContext.Current.Session[Type] as List<SaleInvoiceItem>;
            if (SaleInvoiceItemList != null)
            {
                var item = SaleInvoiceItemList.Where(x => x.Product.ProductId == SaleInvoiceItem.ProductId).FirstOrDefault();
                if (item != null)
                {
                    item.Product.SalesmanCommision = SaleInvoiceItem.SalesmanCommision.Value;
                    item.SalesmanCommision = SaleInvoiceItem.SalesmanCommision.Value;
                    item.Adjustment = SaleInvoiceItem.Adjustment;
                    item.MRP = SaleInvoiceItem.MRP;
                    item.Discount = SaleInvoiceItem.Discount;
                    item.Status = SaleInvoiceItem.Status;
                    item.GSTId = SaleInvoiceItem.GSTId;
                    item.UnitId = SaleInvoiceItem.UnitId;
                    item.ConversionValue = SaleInvoiceItem.ConversionValue;
                    item.DiscountId = SaleInvoiceItem.DiscountId ?? 0;
                    item.Quantity = SaleInvoiceItem.Quantity;
                    if (item.Product.SellInPartial)
                    {
                        item.BasicPrice = item.MRP * item.ConversionValue.Value;
                    }
                    else
                    {
                        item.BasicPrice = item.MRP;
                    }
                    if (item.GSTId > 0)
                    {
                        item.GST = db.GetGSTMasterById(item.GSTId);
                        item.ApplicableTax = ((item.GST.CGST ?? 0) + (item.GST.IGST ?? 0) + item.GST.SGST);
                    }
                    else
                    {
                        item.GST = new GST
                        {
                            CGST = 0,
                            SGST = 0,
                            IGST = 0
                        };
                        item.ApplicableTax = 0;
                    }


                    if (item.DiscountId.HasValue)
                    {
                        Decimal Percent = db.Discounts.Where(x => x.DiscountId == item.DiscountId).Select(x => x.DiscountPercent.Value).FirstOrDefault();
                        item.Discount = Math.Round((item.BasicPrice.Value * (Percent / 100)) * 10000) / 10000;
                    }
                    else
                    {
                        item.Discount = 0;
                    }
                    item.BasicPrice = item.BasicPrice - item.Discount;
                    item.ApplicableTaxValue = Math.Round((((item.BasicPrice.Value) * (item.ApplicableTax / 100))) * 10000) / 10000;
                    item.CGST = Math.Round((((item.BasicPrice.Value) * (item.GST.CGST ?? 0) / 100)) * 10000) / 10000;
                    item.SGST = Math.Round((((item.BasicPrice.Value) * (item.GST.SGST) / 100)) * 10000) / 10000;
                    item.IGST = Math.Round((((item.BasicPrice.Value) * (item.GST.IGST ?? 0) / 100)) * 10000) / 10000;
                    item.SubTotal = Math.Round(((((item.BasicPrice.Value + item.Adjustment) * 100) / 100) + item.ApplicableTaxValue) * 10000) / 10000;
                    item.Status = Constants.SALE_ITEM_SOLD;
                }
            }
            HttpContext.Current.Session[Type] = SaleInvoiceItemList;
        }

        public static void SetSaleInvoiceItemList(List<SaleInvoiceItem> SaleInvoiceItems)
        {
            HttpContext.Current.Session[Constants.SALE_TYPE_SALE_INVOICE] = SaleInvoiceItems;
        }

        public static void AddSaleInvoiceItem(SaleInvoiceItem SaleInvoiceItem, string Type)
        {
            List<SaleInvoiceItem> SaleInvoiceItemList = HttpContext.Current.Session[Type] as List<SaleInvoiceItem>;
            if (SaleInvoiceItemList == null)
            {
                SaleInvoiceItemList = new List<SaleInvoiceItem>
                {
                    SaleInvoiceItem
                };
            }
            else
            {
                SaleInvoiceItemList.Add(SaleInvoiceItem);
            }

            HttpContext.Current.Session[Type] = SaleInvoiceItemList;
        }

        public static void RemoveItemFromList(int ProductId, string Type)
        {
            List<SaleInvoiceItem> SaleInvoiceItemList = HttpContext.Current.Session[Type] as List<SaleInvoiceItem>;
            if (SaleInvoiceItemList != null)
            {
                var item = SaleInvoiceItemList.Where(x => x.Product.ProductId == ProductId).FirstOrDefault();
                SaleInvoiceItemList.Remove(item);
            }
            HttpContext.Current.Session[Type] = SaleInvoiceItemList;
        }
        #endregion

        #region Sale Order Item
        public static void RemoveSaleInvoiceItemForOrder(int ProductId)
        {
            List<SaleInvoiceItem> SaleInvoiceItemList = HttpContext.Current.Session[Constants.SALE_TYPE_SALE_ORDER] as List<SaleInvoiceItem>;
            if (SaleInvoiceItemList != null)
            {
                var item = SaleInvoiceItemList.Where(x => x.Product.ProductId == ProductId).FirstOrDefault();
                SaleInvoiceItemList.Remove(item);
            }
            HttpContext.Current.Session[Constants.SALE_TYPE_SALE_ORDER] = SaleInvoiceItemList;
        }

        public static void AddSaleOrderItem(SalesOrderItem model)
        {
            DbConnector db = new DbConnector();
            List<SalesOrderItem> OrderItemList = HttpContext.Current.Session[Constants.SALE_ORDER_ITEM] as List<SalesOrderItem>;
            if (OrderItemList == null || OrderItemList.Count == 0)
            {
                model.SalesOrderItemId = db.GetLastSaleOrderItemNumber();
                OrderItemList = new List<SalesOrderItem>
                {
                    model
                };
            }
            else
            {
                var item = OrderItemList.Where(x => x.SalesOrderItemId == model.SalesOrderItemId).FirstOrDefault();
                if (item == null)
                {
                    SalesOrderItem OrderItem = OrderItemList.OrderByDescending(x => x.SalesOrderItemId).FirstOrDefault();
                    model.SalesOrderItemId = OrderItem.SalesOrderItemId + 1;
                    OrderItemList.Add(model);
                }
                else
                {
                    item.Quantity = model.Quantity;
                    item.SKUMasterId = model.SKUMasterId;
                    item.EstimatedPrice = model.EstimatedPrice;
                    item.Remark = model.Remark;
                }
            }
            HttpContext.Current.Session[Constants.SALE_ORDER_ITEM] = OrderItemList;
        }

        public static List<SalesOrderItem> GetSalesOrderItem()
        {
            List<SalesOrderItem> OrderItemList = HttpContext.Current.Session[Constants.SALE_ORDER_ITEM] as List<SalesOrderItem>;
            if (OrderItemList == null)
            {
                OrderItemList = new List<SalesOrderItem>();
            }
            return OrderItemList;
        }

        public static void RemoveSalesOrderItem(int SalesOrderItemId)
        {
            List<SalesOrderItem> OrderItemList = HttpContext.Current.Session[Constants.SALE_ORDER_ITEM] as List<SalesOrderItem>;
            if (OrderItemList != null)
            {
                SalesOrderItem model = OrderItemList.Where(x => x.SalesOrderItemId == SalesOrderItemId).FirstOrDefault();
                OrderItemList.Remove(model);
            }
            HttpContext.Current.Session[Constants.SALE_ORDER_ITEM] = OrderItemList;
        }

        public static void UpdateOrderItemForOrder(SaleInvoiceItem SaleInvoiceItem)
        {
            DbConnector db = new DbConnector();
            List<SaleInvoiceItem> SaleInvoiceItemList = HttpContext.Current.Session[Constants.SALE_TYPE_SALE_ORDER] as List<SaleInvoiceItem>;
            if (SaleInvoiceItemList != null)
            {
                var item = SaleInvoiceItemList.Where(x => x.Product.ProductId == SaleInvoiceItem.ProductId).FirstOrDefault();
                if (item != null)
                {
                    item.Product.SalesmanCommision = SaleInvoiceItem.SalesmanCommision.Value;
                    item.SalesmanCommision = SaleInvoiceItem.SalesmanCommision.Value;
                    item.Adjustment = SaleInvoiceItem.Adjustment;
                    item.MRP = SaleInvoiceItem.MRP;
                    item.Discount = SaleInvoiceItem.Discount;
                    item.Status = SaleInvoiceItem.Status;
                    item.GSTId = SaleInvoiceItem.GSTId;
                    item.UnitId = SaleInvoiceItem.UnitId;
                    item.ConversionValue = SaleInvoiceItem.ConversionValue;
                    item.DiscountId = SaleInvoiceItem.DiscountId ?? 0;
                    item.Quantity = SaleInvoiceItem.Quantity;
                    if (item.GSTId > 0)
                    {
                        item.GST = db.GetGSTMasterById(item.GSTId);
                        item.ApplicableTax = ((item.GST.CGST ?? 0) + (item.GST.IGST ?? 0) + item.GST.SGST);
                    }
                    else
                    {
                        item.GST = new GST
                        {
                            CGST = 0,
                            SGST = 0,
                            IGST = 0
                        };
                        item.ApplicableTax = 0;
                    }

                    if (item.Product.SellInPartial)
                    {
                        item.BasicPrice = Math.Round(((((item.MRP * item.ConversionValue.Value) / (1 + ((item.ApplicableTax) / 100))) * 100) / 100) * 10000) / 10000;
                    }
                    else
                    {
                        item.BasicPrice = Math.Round((((item.MRP / (1 + ((item.ApplicableTax) / 100))) * 100) / 100) * 10000) / 10000;
                    }

                    if (item.DiscountId.HasValue)
                    {
                        Decimal Percent = db.Discounts.Where(x => x.DiscountId == item.DiscountId).Select(x => x.DiscountPercent.Value).FirstOrDefault();
                        item.Discount = Math.Round((item.BasicPrice.Value * (Percent / 100)) * 10000) / 10000;
                    }
                    else
                    {
                        item.Discount = 0;
                    }
                    item.ApplicableTaxValue = Math.Round((((item.BasicPrice.Value - item.Discount) * (item.ApplicableTax / 100))) * 10000) / 10000;
                    item.CGST = Math.Round((((item.BasicPrice.Value - item.Discount) * (item.GST.CGST ?? 0) / 100)) * 10000) / 10000;
                    item.SGST = Math.Round((((item.BasicPrice.Value - item.Discount) * (item.GST.SGST) / 100)) * 10000) / 10000;
                    item.IGST = Math.Round((((item.BasicPrice.Value - item.Discount) * (item.GST.IGST ?? 0) / 100)) * 10000) / 10000;
                    item.SubTotal = Math.Round(((((item.BasicPrice.Value + item.Adjustment) * 100) / 100) + item.ApplicableTaxValue) * 10000) / 10000;
                }
            }
            HttpContext.Current.Session[Constants.SALE_TYPE_SALE_ORDER] = SaleInvoiceItemList;
        }
        #endregion

        #region Product Voucher Item
        public static void ProductVoucher(ProductVoucher model, string Type)
        {
            DbConnector db = new DbConnector();
            List<ProductVoucher> ProductVoucherList = HttpContext.Current.Session[Type] as List<ProductVoucher>;
            if (ProductVoucherList == null || ProductVoucherList.Count == 0)
            {
                model.ProductVoucherId = db.GetLastProductVoucherIdNumber();
                ProductVoucherList = new List<ProductVoucher>
                {
                    model
                };
            }
            else
            {
                var item = ProductVoucherList.Where(x => x.ProductVoucherId == model.ProductVoucherId).FirstOrDefault();
                if (item == null)
                {
                    ProductVoucher service = ProductVoucherList.OrderByDescending(x => x.ProductVoucherId).FirstOrDefault();
                    model.ProductVoucherId = service.ProductVoucherId + 1;
                    ProductVoucherList.Add(model);
                }
                else
                {
                    item.GSTId = model.GSTId;
                    item.ProductName = model.ProductName;
                    item.Quantity = model.Quantity;
                    item.Rate = model.Rate;
                    item.Total = model.Total;
                    item.TotalTax = model.TotalTax;
                    item.UnitId = model.UnitId;
                    item.TotalDiscount = model.TotalDiscount;
                    item.CGST = model.CGST;
                    item.SGST = model.SGST;
                    item.IGST = model.IGST;
                    item.BasePrice = model.BasePrice;
                }
            }
            HttpContext.Current.Session[Type] = ProductVoucherList;
        }

        public static List<ProductVoucher> GetProductVoucherList(string Type)
        {
            List<ProductVoucher> ProductVoucherList = HttpContext.Current.Session[Type] as List<ProductVoucher>;
            return ProductVoucherList;
        }

        public static void RemoveProductVoucher(int ProductVoucherId, string Type)
        {
            List<ProductVoucher> ProductVoucherList = HttpContext.Current.Session[Type] as List<ProductVoucher>;
            if (ProductVoucherList != null)
            {
                var item = ProductVoucherList.Where(x => x.ProductVoucherId == ProductVoucherId).FirstOrDefault();
                ProductVoucherList.Remove(item);
            }
            HttpContext.Current.Session[Type] = ProductVoucherList;
        }
        #endregion

        public static void EmptySessionList(string Type)
        {
            HttpContext.Current.Session[Type] = null;
        }
    }
}
