﻿using System.ComponentModel.DataAnnotations;

namespace DataLayer
{
    public class Enums
    {
        public enum UserType { None, Admin, Customer }
        public enum SmsType { None, Email, SMS }
        public enum RecordType
        {
            [Display(Name = "Employee")]
            Employee = 1,
            [Display(Name = "Supplier")]
            Supplier = 2,
            [Display(Name = "Customer")]
            Customer = 3,
            [Display(Name = "Other")]
            Other = 4,
            [Display(Name = "Purchase Account")]
            PurchaseAccount = 5,
            [Display(Name = "Purchase Type")]
            PurchaseType = 6,
            [Display(Name = "Sale Type")]
            SaleType = 7
        }

        public enum PurchaseReportType
        {
            [Display(Name = "All")]
            All = 1,
            [Display(Name = "Purchase Book")]
            PurchaseBook = 2,
            [Display(Name = "Purchase Return")]
            PurchaseReturn = 3,
        }

        public enum SaleReportType
        {
            [Display(Name = "All")]
            All = 1,
            [Display(Name = "Sale Invoice/POS/Sale Vouchers")]
            SaleInvoice = 2,
            [Display(Name = "Sale Return/Exchange")]
            SaleReturn = 3,
        }

        public enum PaymentTypes
        {
            Credit = 1,
            Debit = 2,
        }
        public enum TitleType
        {
            [Display(Name = "Mr.")]
            Mr = 1,
            [Display(Name = "Mrs.")]
            Mrs = 2,
            [Display(Name = "Miss.")]
            Miss = 3,
            [Display(Name = "M/s.")]
            Ms = 4
        }

        public enum PurchaseOrderStatus
        {
            [Display(Name = "Open")]
            Open = 1,
            [Display(Name = "Force Closed")]
            ForceClosed = 2,
            [Display(Name = "Delivered")]
            Closed = 3,
            [Display(Name = "In Process")]
            InProcess = 4,
            [Display(Name = "Partialy Process")]
            PartialyProcess = 5,
        }

        public enum JobSheetStatus
        {
            [Display(Name = "New")]
            New = 1,
            [Display(Name = "Further Process")]
            FurtherProcess = 2,
            [Display(Name = "Alteration")]
            Alteration = 3,
        }

        public enum JobSheetComplete
        {
            [Display(Name = "Open")]
            Open = 1,
            [Display(Name = "Further Process")]
            FurtherProcess = 2,
            [Display(Name = "Complete")]
            Complete = 3,
            [Display(Name = "In Process")]
            InProcess = 4,
        }

        public enum Permission
        {
            [Display(Name = "Read")]
            Read = 1,
            [Display(Name = "Write")]
            Write = 2,
            [Display(Name = "Edit")]
            Edit = 3,
            [Display(Name = "Delete")]
            Delete = 4,
        }

        public enum MeasurementType
        {
            [Display(Name = "Readymade")]
            Readymade = 1,
            [Display(Name = "Custom")]
            Custom = 2,
        }

        public enum LeadProoductStatus
        {
            [Display(Name = "Open")]
            Open = 1,
            [Display(Name = "Closed")]
            Closed = 2,
        }

        public enum MemoStatus
        {
            [Display(Name = "Open")]
            Open = 1,
            [Display(Name = "Closed")]
            Closed = 2,
        }

        public enum MethodOfAdjustment
        {
            [Display(Name = "Advance")]
            Advance = 1,
            [Display(Name = "Against Reference")]
            Against = 2,
            [Display(Name = "On Account")]
            Account = 3,
        }

        public enum SaleOrderType
        {
            [Display(Name = "MTO")]
            MTO = 1,
            [Display(Name = "Traded Product")]
            TradedProduct = 2,
        }
    }
}
