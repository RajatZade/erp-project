﻿using System.Collections.Generic;

namespace DataLayer
{
    public class Constants
    {
        public const string SESSION_USER = "SessionUser";
        public const string NO_RECORDS_FOUND = "No records found";
        //public const string URL = "http://localhost:61035/";
        public const string URL = "http://Central.paramountlifestyles.in/";
        public const int PageSize = 15;
        public const string Order_Item_List = "Order";
        public const string BANK = "Bank";
        public const string SESSION_Division = "Division";
        public const string SESSION_Branch = "Branch";
        public const string SESSION_Company = "Company";
        public const string JOBSHEET_ITEM_LIST = "JobSheetItemList";
        public const string ATTRIBUTE_LIST = "AttributeList";
        public static string DRAFT = "Draft";
        public static string PAYMENT_CREDIT = "Payment CREDIT";
        public static string PAYMENT_DONE = "Payment Done";
        public static string CREDIT_SALE = "Credit Sale";
        public static string HOLD = "Hold";
        public static dynamic SET_Comany = "Set Branch And Division";
        public static string SALE_ITEM_SOLD = "Sold";
        public static string SALE_ITEM_EXCESSES = "Excesses";
        public static string SALE_ITEM_SAVE_FOR_ORDER = "Saved for Order";
        public static string SALE_ITEM_SAVE_FOR_DELIVERY_MEMO = "Saved for Delivery Memo";
        public static string SALE_ITEM_EXCHANGE = "Exchange";
        public static string SALE_ITEM_RETURN = "Return";
        public static string TitleType = "M/s";
        public static string AsOfDate = "AsOfDate";
        public static string DR = "DR";
        public static string CR = "CR";
        public static string BARCODE_PRODUCTS = "ProductsForBarcode";
        public static string ISCLONE = "IsClone";
        public static string MEASURMENT_ITEM_LIST = "MeasurmentSetList";
        public const string TEMPLATE_PATH = "Templates";
        public const string OPERATION_TYPE_SMS = "SMS";
        public const string OPERATION_TYPE_SMS_SELF = "SMS_SELF";
        public const string OPERATION_TYPE_EMAIL = "Email";
        public static string SALE_INVOICE_RETURN = "Return";
        public static string SALE_INVOICE_SOLD = "Sold";
        //public static string PURCHASE_RETURN = "Purchase Retun";
        public static string SALE_INVOICE_EXCHANGE = "Exchange";
        public static string SALE_INVOICE_FOR_ALTERATION = "Alteration";
        public static string JOBSHEET_SERVICE = "JobSheetService";
        public static string JOBSHEET_MEASURMENTSET = "MeasurmentSet";
        public static string BUDGETING_LIST = "Budgeting";
        public static string LEAD_ITEM_LIST = "LeadItemList";
        public static string ATTRIBUTE_ALREADY_PRESENT = "Attribute Already Present";
        public static string LEDGER_TYPE_COMPANY = "Company Ledger";
        public static string LEDGER_TYPE_TAX = "Tax Ledger";
        public static string LEDGER_TYPE_USER = "User Ledger";
        public static string LEDGER_TYPE_PAYMENTTYPE = "Payment Type";
        public static string LEDGER_TYPE_DISCOUNT = "Discounts";
        public static string PaymentReturn = "URL";
        public static string CASH = "CASH";

        public static string RECEIPT_VOUCHER = "Receipt Voucher";
        public static string PAYMENT_VOUCHER = "Payment Voucher";
        public static string JOURNAL_ENTRY = "Journal Entry";
        public static string CONTRA_ENTRY = "Contra entry";
        public static string OPENING_BALANCE = "Opening Balance";
        public static string SALE_ORDER_ITEM = "SaleOrderItem";

        public static string PURCHASE_TYPE_PURCHASE_INVOICE = "Purchase Invoice";
        public static string PURCHASE_TYPE_OPENING_STOCK = "Opening Stock";
        public static string PURCHASE_TYPE_PURCHASE_VOUCHER = "Purchase Voucher";
        public static string INTERNAL_TRANSFER = "Internal Transfer";
        public static string SALE_TYPE_PURCHASE_RETURN = "Purchase Return";
        public static string SALE_TYPE_SALE_INVOICE = "Sale Invoice";
        public static string SALE_TYPE_SALE_RETURN = "Sale Return";
        public static string SALE_TYPE_SALE_EXCHANGE = "Sale Exchange";
        public static string SALE_TYPE_SALE_VOUCHER = "Sale Voucher";
        public static string SALE_TYPE_SALE_ORDER = "Sale Order";
        public static string SALE_TYPE_DELIVERY_MEMO = "Delivery Memo";
        public static string SALE_TYPE_INTERNAL_CONSUMSSION = "Internal Consumssion";
        public static string SALE_TYPE_SHORTAGES = "Shortages";
        public static string SALE_TYPE_EXCESSES = "Excesses";
        public static string SALE_TYPE_WPS = "WPS";

        public static string WSP_PRODUCT_LIST = "WSPProductList";

        public const string OnSuccessOfSaleInvoice = URL + "SaleInvoice/Return";
        public const string OnSuccessOfSaleVoucher = URL + "SaleVoucher/Return";
        public const string OnFailure = URL + "PaymentGateWay/Failure";

        public const string OnSuccessOfSalesOrder = URL + "SalesOrder/Return";

        public const string LEDGER_TYPE_SALE_RETURN_LEDGER = "Sale Return Ledger";
        public const string LEDGER_TYPE_PURCHASE_RETURN_LEDGER = "Purchase Return Ledger";

    }
}
