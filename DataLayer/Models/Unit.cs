﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace DataLayer.Models
{
    public class Unit:BaseModel
    {
        public int UnitId { get; set; }
        [Required(ErrorMessage = "Required")]
        public string UnitValue { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.00}", ApplyFormatInEditMode = true)]
        public decimal? Conversion { get; set; }
        public int? UnitTypeId { get; set; }
        public bool DisableDelete { get; set; }
        public string UnitParents { get; set; }
        public bool SellInPartial { get; set; }
        [NotMapped]
        public List<SelectListItem> UnitTypeList { get; set; }
    }
}
