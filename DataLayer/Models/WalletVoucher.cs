﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class WalletVoucher : BaseModel
    {
        public int WalletVoucherId { get; set; }
        public int ContactId { get; set; }
        public decimal TotalValue { get; set; }
        public int SaleInvoiceId { get; set; }
    }
}
