﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DataLayer.Models
{
    public class EmailConfig : BaseModel
    {
        public int EmailConfigId { get; set; }

        //[Remote("IsUserExists", "Users", ErrorMessage = "User Email Id already in use")]
        [Required(ErrorMessage = "This field is required.", AllowEmptyStrings = false)]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email-Id")]
        public string UserEmailId { get; set; }

        [Required(ErrorMessage = "This field is required.", AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Set Default")]
        public bool IsDefault { get; set; }

        [Required(ErrorMessage = "This field is required.", AllowEmptyStrings = false)]
        [Display(Name = "Host")]
        public string Host { get; set; }

        [Required(ErrorMessage = "This field is required.", AllowEmptyStrings = false)]
        [Display(Name = "Port")]
        public int Port { get; set; }

        [Display(Name = "Enable SSL")]
        public bool EnableSsl { get; set; }

        [Display(Name = "Default Credentials")]
        public bool UsedDefaultCredentials { get; set; }
    }
}
