﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Models
{
    public class Discount : BaseModel
    {

        public int DiscountId { get; set; }

        public decimal? DiscountPercent { get; set; }

        public decimal? MaxDiscountAllowed { get; set; }

        [DisplayFormat(NullDisplayText = "-")]
        public string DiscountCode { get; set; }

        [DisplayFormat(NullDisplayText = "-")]
        public string Remark { get; set; }

        [DisplayFormat(NullDisplayText = "-")]
        public string PromoCode { get; set; }

        public bool IsActive { get; set; }

        [NotMapped]
        public List<DiscountAccessModel> DiscountList { get; set; }

        [NotMapped]
        public SKUMaster SKUMaster { get; set; }

        [NotMapped]
        public int SKUMasterId { get; set; }

        public Discount()
        {
            DiscountList = new List<DiscountAccessModel>();
        }


    }
}
