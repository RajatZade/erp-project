﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace DataLayer.Models
{
    public class AccountSubCategory : BaseModel
    {
        public int AccountSubCategoryId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Required")]
        public int? AccountCategoryId { get; set; }

        [DisplayFormat(NullDisplayText = "-")]
        [Required(ErrorMessage = "Required")]
        public string ContactSubCategoryName { get; set; }

        public virtual AccountCategory ContactCategory { get; set; }

        [NotMapped]
        public List<SelectListItem> ContactCategoryList { get; set; }
    }
}
