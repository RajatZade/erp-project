﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DataLayer.Models
{
    public class InternalTransfer : BaseModel
    {
        public int InternalTransferId { get; set; }
        public int CompanyId { get; set; }
        public string InternalTransferNo { get; set; }
        public decimal SubTotal { get; set; }
        public decimal TaxTotal { get; set; }
        public decimal DiscountTotal { get; set; }
        public decimal GrandTotal { get; set; }

        public int ToDivisionId { get; set; }
        public int FromDivisionId { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime TransferDate { get; set; }

        [ForeignKey("ToBranch")]
        public int ToBranchId { get; set; }
        public virtual Branch ToBranch { get; set; }

        [ForeignKey("FromBranch")]
        public int FromBranchId { get; set; }
        public virtual Branch FromBranch { get; set; }

        [ForeignKey("ToLocation")]
        public int ToLocationId { get; set; }
        public virtual Location ToLocation { get; set; }

        [ForeignKey("FromLocation")]
        public int FromLocationId { get; set; }
        public virtual Location FromLocation { get; set; }

        public List<SaleInvoiceItem> InternalTransfers { get; set; }

        [NotMapped]
        public string Search { get; set; }

        [NotMapped]
        public List<SelectListItem> DivisionList { get; set; }
        [NotMapped]
        public List<SelectListItem> ToBranchList { get; set; }
        [NotMapped]
        public List<SelectListItem> FromBranchList { get; set; }
        [NotMapped]
        public List<SelectListItem> ToLocationList { get; set; }
        [NotMapped]
        public List<SelectListItem> FromLocationList { get; set; }

        public InternalTransfer()
        {
            DivisionList = new List<SelectListItem>();
            ToBranchList = new List<SelectListItem>();
            FromBranchList = new List<SelectListItem>();
            ToLocationList = new List<SelectListItem>();
            FromLocationList = new List<SelectListItem>();
        }
    }
}
