﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace DataLayer.Models
{
    public class PurchaseOrderItem : BaseModel
    {
        public int PurchaseOrderItemId { get; set; }

        public int PurchaseOrderId { get; set; }

        public int SKUMasterId { get; set; }

        public int BrandId { get; set; }

        public int UnitId { get; set; }

        public int HSNId { get; set; }

        public decimal Quantity { get; set; }

        public decimal Rate { get; set; }
        public bool SellInPartial { get; set; }
        public decimal ApplicableTax { get; set; }
        public decimal? SubTotal { get; set; }

        public int GSTId { get; set; }
        public decimal? Adjustment { get; set; }
        public decimal? AdditionalPurchaseExpenses { get; set; }
        public decimal? NetAmount { get; set; }
        public decimal? WSPPercent { get; set; }
        public decimal? WSPAmount { get; set; }

        public int? AttributeSetId { get; set; }

        public int LocationId { get; set; }
        public decimal MRP { get; set; }
        public string DiscountsAccessible { get; set; }
        //public decimal DiscountPercent { get; set; }
        //public decimal DiscountedAmount { get; set; }
        //public decimal Discount { get; set; }

        //public int DiscountId { get; set; }

        public decimal SalesCommission { get; set; }
        public string Remarks { get; set; }
        public decimal? CGST { get; set; }
        public decimal? SGST { get; set; }
        public decimal? IGST { get; set; }
        public decimal? OtherTaxes { get; set; }

        public virtual SKUMaster SKUMaster { get; set; }
        public Brand Brand { get; set; }
        public Unit Unit { get; set; }
        public virtual HSN HSN { get; set; }
        public Location Location { get; set; }
        public GST GST { get; set; }
        public List<AttributeLinking> AttributeLinkingList { get; set; }

        #region Attributes
        [NotMapped]
        public int? ProductId { get; set; }
        //[NotMapped]
        //public int DiscountIDTEmp { get; set; }
        [NotMapped]
        public int CategoryId { get; set; }
        [NotMapped]
        public int SubCategoryId { get; set; }
        [NotMapped]
        public int? AttributeValueId { set; get; }
        [NotMapped]
        public decimal? Total { get; set; }
        [NotMapped]
        public int NotQuantity { get; set; }
        [NotMapped]
        public bool checkAllProduct { get; set; }
        [NotMapped]
        public string IsClone { get; internal set; }
        [NotMapped]
        public int[] DiscountIds { get; set; }
        #endregion

        #region NotMapped List
        [NotMapped]
        public AttributeSet AttributeSet { get; set; }
        [NotMapped]
        public AttributeLinking AttributeLinking { get; set; }
        [NotMapped]
        public List<SelectListItem> CategoryList { get; set; }
        [NotMapped]
        public List<SelectListItem> SubcategoryList { get; set; }
        [NotMapped]
        public List<SelectListItem> SKUList { get; set; }
        [NotMapped]
        public List<SelectListItem> HSNList { get; set; }
        [NotMapped]
        public List<SelectListItem> BrandList { get; set; }
        [NotMapped]
        public List<SelectListItem> DiscountCodeList { get; set; }
        [NotMapped]
        public List<SelectListItem> LocationList { get; set; }
        [NotMapped]
        public List<SelectListItem> UnitList { get; set; }
        [NotMapped]
        public List<AttributeSet> AttributeSetListNew { get; set; }
        [NotMapped]
        public List<SelectListItem> AttributeSetList { get; set; }
        [NotMapped]
        public List<SelectListItem> AttributeValueList { get; set; }
        [NotMapped]
        public List<SelectListItem> GSTList { get; set; }
        [NotMapped]
        public List<Product> ProductList { get; set; }
        [NotMapped]
        public List<Product> PurchaseInvoiceItemsListIsSoldList { get; set; }        
        [NotMapped]
        public Dictionary<AttributeSet, List<SelectListItem>> AttributeDictionayList { get; set; }
        [NotMapped]
        public Category Category { get; set; }
        [NotMapped]
        public SubCategory SubCategory { get; set; }
        [NotMapped]
        public PurchaseOrder PurchaseOrder { get; set; }
        #endregion

        public PurchaseOrderItem()
        {
            CategoryList = new List<SelectListItem>();
            SKUList = new List<SelectListItem>();
            DiscountCodeList = new List<SelectListItem>();
            BrandList = new List<SelectListItem>();
            HSNList = new List<SelectListItem>();
            LocationList = new List<SelectListItem>();
            AttributeSetList = new List<SelectListItem>();
            AttributeValueList = new List<SelectListItem>();
        }
    }
}
