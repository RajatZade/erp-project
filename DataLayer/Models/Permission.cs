﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Models
{
    public class Role : BaseModel
    {
        public int RoleId { get; set; }

        [Required]
        public string Name { get; set; }

        public bool IsAdmin { get; set; }

        public IList<PermissionSetLinking> PermissionSetLinkings { get; set; }

        public string CategoriesAccessible { get; set; }

        [NotMapped]
        public List<SubCategoryAccessModel> SubCategories { get; set; }
        [NotMapped]
        public List<Permission> Permissions { get; internal set; }
        [NotMapped]
        public List<CompanyAccessModel> Companies { get; set; }
        [NotMapped]
        public List<DivisionAccessModel> Divisions { get; set; }
        [NotMapped]
        public List<BranchAccessModel> Branches { get; set; }
        [NotMapped]
        public List<DashboardItemsAccessibleModel> DashboardItems { get; set; }

        public string CompaniesAccessible { get; set; }
        public string BranchesAccessible { get; set; }
        public string DivisionsAccessible { get; set; }
        public string DashboardItemsAccessible { get; set; }

        public Role()
        {
            Companies = new List<CompanyAccessModel>();
            Branches = new List<BranchAccessModel>();
            Divisions = new List<DivisionAccessModel>();
            DashboardItems = new List<DashboardItemsAccessibleModel>();
        }
    }

    public class Permission
    {
        public int PermissionId { get; set; }

        public string DisplayName { get; set; }

        public string ModuleName { get; set; }

        public string ReadValue { get; set; }

        public string CreateValue { get; set; }

        public string EditValue { get; set; }

        public string DeleteValue { get; set; }

        [NotMapped]
        public bool IsSelected { get; set; }
    }

    public class PermissionSetLinking
    {
        public int PermissionSetLinkingId { get; set; }
        public int RoleId { get; set; }
        public int PermissionId { get; set; }
        public Role Role { get; set; }
        public Permission Permission { get; set; }

        public bool IsRead { get; set; }
        public bool IsEdit { get; set; }
        public bool IsCreate { get; set; }
        public bool IsDelete { get; set; }
    }

    public class SubCategoryAccessModel
    {
        public int SubCategoryId { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
    }

    public class DivisionAccessModel
    {
        public int DivisionId { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
    }

    public class CompanyAccessModel
    {
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
    }

    public class BranchAccessModel
    {
        public int BranchId { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
    }

    public class CategoryAccessModel
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
    }

    public class BrandAccessModel
    {
        public int BrandId { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
    }


    public class DiscountAccessModel
    {
        public int DiscountId { get; set; }
        public string Code { get; set; }
        public bool IsSelected { get; set; }
        public decimal? DiscountPercent { get; set; }
    }

    public class DashboardItemsAccessibleModel
    {
        public int DashboardItemId { get; set; }
        public string ItemName { get; set; }
        public bool IsSelected { get; set; }
    }
    

    public class DiscountAccessDetailsModel
    {
        public int DiscountId { get; set; }
        public string Code { get; set; }
        public bool IsSelected { get; set; }
        public decimal? Percent { get; set; }
    }

    public class MeasurementAccessModel
    {
        public string MeasurementType { get; set; }
        public string MeasurementValue { get; set; }
    }
}
