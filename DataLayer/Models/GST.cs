﻿using System.ComponentModel.DataAnnotations;

namespace DataLayer.Models
{
    public class GST : BaseModel
    {
        public int GSTId { get; set; }
        [Required(ErrorMessage = "Required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Required")]
        public decimal SGST { get; set; }

        [DisplayFormat(NullDisplayText = "-")]
        public decimal? CGST { get; set; }

        [DisplayFormat(NullDisplayText = "-")]
        public decimal? IGST { get; set; }

        [DisplayFormat(NullDisplayText = "-")]
        public decimal? OtherTaxes { get; set; }

    }
}
