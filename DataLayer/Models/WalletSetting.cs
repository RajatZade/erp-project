﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class WalletSetting : BaseModel
    {
        public int WalletSettingId { get; set; }
        public decimal Conversion { get; set; }
    }
}
