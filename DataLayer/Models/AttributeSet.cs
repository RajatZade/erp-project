﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace DataLayer.Models
{
    public class AttributeSet : BaseModel
    {
        public int AttributeSetId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string ApplicableValues { get; set; }
        public bool IsActive { get; set; }
        [NotMapped]
        public List<SelectListItem> AttributeValueList { get; set; }
        [NotMapped]
        public List<string> AttributeListForDetails { get; set; }

    }

    public class AttributeLinking
    {
        public int? PurchaseInvoiceItemId { get; set; }
        public int? PurchaseOrderItemId { get; set; }
        public int AttributeLinkingId { get; set; }
        public string Value { get; set; }
        public decimal Quantity { get; set; }
        public string AttributeSetIds { get; set; }
        [NotMapped]
        public AttributeSet AttributeSet { get; set; }
        [NotMapped]
        public List<SelectListItem> AttributeValueList { get; set; }
        [NotMapped]
        public List<string> ValuesList { get; set; }
        [NotMapped]
        public List<string> NameList { get; set; }
    }
}
