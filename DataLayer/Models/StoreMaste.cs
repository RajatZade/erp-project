﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public  class StoreMaste:BaseModel
    {
        public int StoreMasteId { get; set; }
        //[Required(ErrorMessage = "Required")]
        public string StoreName { get; set; }

        //[Required(ErrorMessage = "Required")]
        public string City { get; set; }
        public string Address { get; set; }

        //[Required(ErrorMessage = "Required")]
        public string Landline { get; set; }

        [Required(ErrorMessage = "Required", AllowEmptyStrings = false)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Required")]
        public string State { get; set; }
    }
}
