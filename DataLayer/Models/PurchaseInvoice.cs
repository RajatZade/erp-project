﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace DataLayer.Models
{
    public class PurchaseInvoice : BaseModel
    {
        public int PurchaseInvoiceId { get; set; }

        public int? PurchaseOrderId { get; set; }

        public string PurchaseInvoiceNo { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? InvoicingDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? EntryDate { get; set; }

        public string Time { get; set; }

        public decimal? TotalInvoiceValue { get; set; }

        public int? PaymentId { get; set; }

        public string DueDate { get; set; }

        public decimal? Adjustment { get; set; }

        public int? ShippingId { get; set; }

        public decimal? TotalTax { get; set; }

        public string ShippingTrackingNumber { get; set; }

        public int? DivisionId { get; set; }

        public int? BranchId { get; set; }

        public int? CompanyId { get; set; }

        public int? LocationId { get; set; }

        public bool IsOpeningStock { get; set; }
        //public bool IsBarcodeGenerated { get; set; }
        public string Type { get; set; }
        public bool IsLedgerUpdate { get; set; }
        public List<PurchaseInvoiceItem> PurchaseInvoiceItemList { get; set; }

        [ForeignKey("Account")]
        public int? AccountId { get; set; }
        public virtual Ledger Account { get; set; }

        [ForeignKey("PurchaseType")]
        public int? PurchaseTypeId { get; set; }
        public virtual Ledger PurchaseType { get; set; }

        [ForeignKey("PurchaseAccount")]
        public int? PurchaseAccountId { get; set; }
        public virtual Ledger PurchaseAccount { get; set; }

        #region NotMapped Value
        [NotMapped]
        public decimal GrossAmount {
            get
            {
               return (TotalInvoiceValue ?? 0) - (TotalTax ?? 0);
            }
                 set { } }
        [NotMapped]
        public decimal ItemTotalQuantity { get; set; }

        [NotMapped]
        public string DueDays { get; set; }

        [NotMapped]
        public decimal? FinalTotalAdj { get; set; }

        [NotMapped]
        public decimal? NotmappedInvoiceValue { get; set; }
        [NotMapped]
        public decimal? NotMappedTotalTax { get; set; }
        [NotMapped]
        public decimal? AmmountPaid { get; set; }
        [NotMapped]
        public decimal? AmmountRemaining { get; set; }
        [NotMapped]
        public decimal ItemTotalBasicPrice { get; set; }

        [NotMapped]
        public decimal ItemTotalMRP { get; set; }

        [NotMapped]
        public int NoOfItem { get; set; }

        [NotMapped]
        public decimal ItemPurchasePrice { get; set; }

        [NotMapped]
        public decimal ItemSubTotal { get; set; }

        [NotMapped]
        public decimal ItemFinalTotal { get; set; }

        [NotMapped]
        public string Search { get; set; }
        [NotMapped]
        public decimal IGST { get; set; }
        [NotMapped]
        public decimal CGST { get; set; }
        [NotMapped]
        public decimal SGST { get; set; }
        #endregion

        #region NotMapped List
        [NotMapped]
        public List<SelectListItem> PaymentTypeList { get; set; }
        [NotMapped]
        public List<SelectListItem> ShippingTypeList { get; set; }
        [NotMapped]
        public List<SelectListItem> DivisionList { get; set; }
        [NotMapped]
        public List<SelectListItem> BranchList { get; set; }
        [NotMapped]
        public List<SelectListItem> LocationList { get; set; }
        [NotMapped]
        public List<SelectListItem> SalesOrderList { get; set; }
        [NotMapped]
        public List<SelectListItem> PurchaseAccountList { get; set; }
        [NotMapped]
        public List<SelectListItem> AccountList { get; set; }
        [NotMapped]
        public List<SelectListItem> PurchaseTypeList { get; set; }
        [NotMapped]
        public List<SelectListItem> PurchaseMemoList { get; set; }
        [NotMapped]
        public List<SelectListItem> PurchaseOrderList { get; set; }
        #endregion

        #region NotMapped Models
        [NotMapped]
        public PurchaseInvoiceItem PurchaseInvoiceItem { get; set; }

        [NotMapped]
        public SKUMaster SKUMaster { get; set; }

        [NotMapped]
        public GST GST { get; set; }

        [NotMapped]
        public virtual Ledger Ledger { get; set; }

        [NotMapped]
        public ProductVoucher ProductVoucher { get; set; }

        [NotMapped]
        public List<ProductVoucher> ProductVoucherItems { get; set; }
        #endregion

        public PurchaseInvoice()
        {
            #region Purchase Invoice List
            PaymentTypeList = new List<SelectListItem>();
            ShippingTypeList = new List<SelectListItem>();
            DivisionList = new List<SelectListItem>();
            BranchList = new List<SelectListItem>();
            SalesOrderList = new List<SelectListItem>();
            PurchaseAccountList = new List<SelectListItem>();
            PurchaseMemoList = new List<SelectListItem>();
            PurchaseOrderList = new List<SelectListItem>
            {
                new SelectListItem() { Text = "--Select One--", Value = "-1" }
            };
            #endregion
        }
    }
}

