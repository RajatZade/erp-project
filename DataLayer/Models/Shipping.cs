﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class Shipping:BaseModel
    {
        public int ShippingId { get; set; }
        //[Required(ErrorMessage = "Required")]
        public string ShippingType { get; set; }
    }
}