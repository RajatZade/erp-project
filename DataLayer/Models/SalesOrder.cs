﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace DataLayer.Models
{
    public class SalesOrder:BaseModel
    {
        public int SalesOrderId { get; set; }

        [Required(ErrorMessage ="Required")]
        [Range(1, int.MaxValue, ErrorMessage = "Required")]
        public int SalesAccountId { get; set; }

        public int CompanyId { get; set; }

        [Required(ErrorMessage = "Required")]
        [Range(1, int.MaxValue, ErrorMessage = "Required")]
        public int CustomerId { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime OrderDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? ExpectedCompletionDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? CommitedDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? PaymentDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? DeliveryDate { get; set; }

        [Required(ErrorMessage = "Required")]
        [Range(1, int.MaxValue, ErrorMessage = "Required")]
        public int DivisionId { get; set; }

        [Required(ErrorMessage = "Required")]
        [Range(1, int.MaxValue, ErrorMessage = "Required")]
        public int BranchId { get; set; }

        [Required(ErrorMessage = "Required")]
        [Range(1, int.MaxValue, ErrorMessage = "Required")]
        public int LocationId { get; set; }

        public string PackedBy { get; set; }

        public string SaleOrderNumber { get; set; }

        public string Type { get; set; }

        public string PaymentType { get; set; }

        public Enums.PurchaseOrderStatus OrderStatus { get; set; }

        public int? PurchaseOrderId { get; set; }
        public PurchaseOrder PurchaseOrder { get; set; }

        public int? PaymentTypeId { get; set; }

        public decimal? EstimatedTotal { get; set; }
        public decimal? AmountPaid { get; set; }
        public decimal? AmountRemaining { get; set; }

        #region NotMapped Modals
        [NotMapped]
        public PurchaseInvoiceItem PurchaseInvoiceItem { get; set; }
        [NotMapped]
        public SalesOrderItem SalesOrderItem { get; set; }
        public List<SalesOrderItem> SalesOrderItems { get; set; }
        [NotMapped]
        public List<SaleInvoiceItem> SaleInvoiceItemList { get; set; }
        [NotMapped]
        public List<SelectListItem> SalesAccountList { get; set; }

        [NotMapped]
        public List<SelectListItem> CustomerList { get; set; }

        [NotMapped]
        public List<SelectListItem> PurchaseOrderList { get; set; }

        [NotMapped]
        public List<SelectListItem> PaymentTypeList { get; set; }

        [NotMapped]
        public List<SelectListItem> DivisionList { get; set; }

        [NotMapped]
        public List<SelectListItem> BranchList { get; set; }

        [NotMapped]
        public List<SelectListItem> LocationList { get; set; }

        [NotMapped]
        public List<SelectListItem> SKUList { get; set; }
        [NotMapped]
        public Company Company { get; set; }
        [NotMapped]
        public Contact Contact { get; set; }
        [NotMapped]
        public Contact Salesman { get; set; }
        [NotMapped]
        public Branch Branch { get; set; }
        [NotMapped]
        public Ledger Ledger { get; set; }
        //[NotMapped]
        //public PaymentType PaymentType { get; set; }
        [NotMapped]
        public List<MeasurmentSet> MeasurmentSetListForCustome { get; set; }
        [NotMapped]
        public List<MeasurmentSet> MeasurmentSetListForReadymade { get; set; }
#endregion

        #region NotMapped Value
        [NotMapped]
        public decimal? FinalTotalAdj { get; set; }
        [NotMapped]
        public decimal TotalDiscountNotMapped { get; set; }
        [NotMapped]
        public decimal TotalTaxNotMapped { get; set; }
        [NotMapped]
        public decimal SubtotalNotMapped { get; set; }
        [NotMapped]
        public string SerialNumber { get; set; }
        [NotMapped]
        public Ledger Customer { get; set; }
        [NotMapped]
        public bool IsAvailableForSaleInvoice { get; set; }
        [NotMapped]
        public int? Quantity { get; set; }
        [NotMapped]
        public string MeasurmentType { get; set; }
        [NotMapped]
        public decimal? NewPayment { get; set; }
        [NotMapped]
        public string Checknumber { get; set; }
        #endregion

        public SalesOrder()
        {
            SalesAccountList = new List<SelectListItem>();
            CustomerList = new List<SelectListItem>();
            PaymentTypeList = new List<SelectListItem>();
            DivisionList = new List<SelectListItem>();
            BranchList = new List<SelectListItem>();
            LocationList = new List<SelectListItem>();
            PurchaseOrderList = new List<SelectListItem>();
            SKUList = new List<SelectListItem>();
        }
    }
}
