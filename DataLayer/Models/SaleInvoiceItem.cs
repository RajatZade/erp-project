﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace DataLayer.Models
{
    public class SaleInvoiceItem : BaseModel
    {
        public int SaleInvoiceItemId { get; set; }
        public int? SaleInvoiceId { get; set; }
        public int? SalesOrderId { get; set; }
        public int? JobSheetId { get; set; }
        public int? ReturnSaleInvoiceId { get; set; }
        public int? InternalTransferId { get; set; }
        public int ProductId { get; set; }
        public int GSTId { get; set; }
        public int? DiscountId { get; set; }
        public string Status { get; set; }
        public decimal Quantity { get; set; }
        public int? SKUMasterId { get; set; }
        public decimal Adjustment { get; set; }
        public decimal AdditionalPurchaseExpenses { get; set; }
        public decimal ApplicableTax { get; set; }
        public decimal? PurchaseTaxPerCent { get; set; }
        public decimal ApplicableTaxValue { get; set; }
        public decimal Discount { get; set; }
        public decimal MRP { get; set; }
        public decimal SubTotal { get; set; }
        public decimal? ConversionValue { get; set; }
        public decimal? CGST { get; set; }
        public decimal? IGST { get; set; }
        public decimal? SGST { get; set; }
        public int? UnitId { get; set; }
        public decimal? BasicPrice { get; set; }
        public decimal? SalesmanCommision { get; set; }

        public virtual Unit Unit { get; set; }
        public virtual Product Product { get; set; }
        public virtual GST GST { get; set; }
        public virtual SKUMaster SKUMaster { get; set; }
        public virtual SaleInvoice SaleInvoice { get; set; }

        [NotMapped]
        public string Size { get; set; }
        [NotMapped]
        public decimal? SaleDiscountPercent { get; set; }
        [NotMapped]
        public decimal PayableAmount { get; set; }       
        [NotMapped]
        public List<SelectListItem> DiscountCodeList { get; set; }
        [NotMapped]
        public List<SelectListItem> GSTList { get; set; }
        [NotMapped]
        public List<SelectListItem> UnitList { get; set; }


        public SaleInvoiceItem()
        {
            DiscountCodeList = new List<SelectListItem>();
        }

        public SaleInvoiceItem(SaleInvoiceItem item)
        {
            ProductId = item.ProductId;
            GSTId = item.GSTId;
            DiscountId = item.DiscountId;
            Status = item.Status;
            Quantity = item.Quantity;
            SKUMasterId = item.SKUMasterId;
            Adjustment = item.Adjustment;
            AdditionalPurchaseExpenses = item.AdditionalPurchaseExpenses;
            ApplicableTax = item.ApplicableTax;
            PurchaseTaxPerCent = item.PurchaseTaxPerCent;
            ApplicableTaxValue = item.ApplicableTaxValue;
            Discount = item.Discount;
            MRP = item.MRP;
            SubTotal = item.SubTotal;
            ConversionValue = item.ConversionValue;
            CGST = item.CGST;
            IGST = item.IGST;
            SGST = item.SGST;
            UnitId = item.UnitId;
            BasicPrice = item.BasicPrice;
            SalesmanCommision = item.SalesmanCommision;
            if(item.ReturnSaleInvoiceId.HasValue)
            {
                SaleInvoiceItemId = item.SaleInvoiceItemId;
                ReturnSaleInvoiceId = item.ReturnSaleInvoiceId;
                SaleInvoiceId = item.SaleInvoiceId;
            }
        }
    }
}
