﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace DataLayer.Models
{
    public class Voucher : BaseModel
    {
        public int VoucherId { get; set; }
        public int CompanyId { get; set; }
        public string VoucherNumber { get; set; }
        public int? PaymentTypeId { get; set; }
        public Enums.MethodOfAdjustment? MethodId { get; set; }
        public int? LedgerId { get; set; }
        public int? SaleInvoiceId { get; set; }
        public int? PurchaseInvoiceId { get; set; }
        public int? JobSheetServiceId { get; set; }
        public int? SalesOrderId { get; set; }
        public decimal? CGST { get; set; }
        public decimal? IGST { get; set; }
        public decimal? SGST { get; set; }
        public decimal Total { get; set; }
        public string Type { get; set; }
        public string VoucherFor { get; set; }
        public string BandAccount { get; set; }
        public string Checknumber { get; set; }
        public string Particulars { get; set; }
        public string InvoiceNumber { get; set; }
        public string Description { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime EntryDate { get; set; }
        [NotMapped]
        public decimal OpeningBalance { get; set; }
        [NotMapped]
        public decimal ClosingBalance { get; set; }

        public SaleInvoice SaleInvoice { get; set; }
        public PurchaseInvoice PurchaseInvoice { get; set; }
        public JobSheetService JobSheetService { get; set; }
        public SalesOrder SalesOrder { get; set; }
        public Ledger Ledger { get; set; }
        [NotMapped]
        public Ledger Ledger2 { get; set; }
        [NotMapped]
        public Company Company { get; set; }
        [NotMapped]
        public PaymentType ToPaymentType { get; set; }
        [NotMapped]
        public PaymentType FromPaymentType { get; set; }

        [NotMapped]
        public int VoucherNo
        {
            get
            {
                if (VoucherNumber != null && VoucherNumber != "")
                {
                    return Convert.ToInt32(VoucherNumber);
                }
                else
                {
                    return 0;
                }
            }
            set { }
        }
    }
    
}
