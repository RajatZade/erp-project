﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace DataLayer.Models
{
    public class JobSheetItem : BaseModel
    {
        public int JobSheetItemId { get; set; }
        public int? JobSheetId { get; set; }
        public int ProductId { get; set; }
        public int GSTId { get; set; }
        public string Status { get; set; }
        public decimal Quantity { get; set; }
        public decimal ApplicableTax { get; set; }
        public decimal ApplicableTaxValue { get; set; }
        public decimal MRP { get; set; }
        public decimal SubTotal { get; set; }
        public decimal? ConversionValue { get; set; }
        public decimal? CGST { get; set; }
        public decimal? IGST { get; set; }
        public decimal? SGST { get; set; }
        public int? UnitId { get; set; }
        public decimal? BasicPrice { get; set; }
        public bool QuantityNotAvailable { get; set; }

        [NotMapped]
        public virtual GST GST { get; set; }
        [NotMapped]
        public virtual Unit Unit { get; set; }
        [NotMapped]
        public List<SelectListItem> GSTList { get; set; }
        [NotMapped]
        public List<SelectListItem> UnitList { get; set; }
        [NotMapped]
        public List<string> Attributes { get; set; }
        public virtual Product Product { get; set; }
        public virtual JobSheet JobSheet { get; set; }

        [NotMapped]
        public string QuantityErrorMsg { get; set; }
    }
}
