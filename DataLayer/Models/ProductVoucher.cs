﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DataLayer.Models
{
    public class ProductVoucher : BaseModel
    {
        public int ProductVoucherId { get; set; }
        public int? PurchaseInvoiceId { get; set; }
        public int? SaleInvoiceId { get; set; }
        public int? GSTId { get; set; }
        public int? DiscountId { get; set; }
        public int? UnitId { get; set; }
        public decimal Quantity { get; set; }
        public decimal Rate { get; set; }
        public decimal Total { get; set; }
        public decimal TotalTax { get; set; }
        public decimal? TotalDiscount { get; set; }
        public string ProductName { get; set; }
        public decimal? BasePrice { get; set; }
        public decimal CGST { get; set; }
        public decimal IGST { get; set; }
        public decimal SGST { get; set; }
        public Discount Discount { get; set; }
        public PurchaseInvoice PurchaseInvoice { get; set; }
        public SaleInvoice SaleInvoice { get; set; }
        public GST GST { get; set; }
        public Unit Unit { get; set; }

        [NotMapped]
        public decimal? SaleDiscountPercent { get; set; }
        [NotMapped]
        public List<SelectListItem> GSTMasterList { get; set; }
        [NotMapped]
        public List<SelectListItem> UnitMasterList { get; set; }
        public ProductVoucher()
        {
            GSTMasterList = new List<SelectListItem>();
            UnitMasterList = new List<SelectListItem>();
        }
    }
}
