﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace DataLayer.Models
{
    public class Branch:BaseModel
    {
        public int BranchId { get; set; }
        [DisplayFormat(NullDisplayText = "-")]
        [Required(ErrorMessage = "Required")]
        public string BranchName { get; set; }

        [Required(ErrorMessage = "Required")]
        [Range(1,int.MaxValue,ErrorMessage =("Required"))]
        public int CompanyId { get; set; }

        public Company Company { get; set; }

        [Required(ErrorMessage = "Required")]
        [Range(1, int.MaxValue, ErrorMessage = ("Required"))]
        public int DivisionId { get; set; }

        public Division Division { get; set; }

        [NotMapped]
        public List<SelectListItem> CompanyList { get; set; }
        [NotMapped]
        public List<SelectListItem> BranchList { get; set; }
        [NotMapped]
        public List<SelectListItem> DivisionList { get; set; }
        public Branch()
        {
            CompanyList = new List<SelectListItem>();
            DivisionList = new List<SelectListItem>();
        }
    }
}
