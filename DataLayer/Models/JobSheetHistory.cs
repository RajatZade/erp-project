﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class JobSheetHistory : BaseModel
    {
        public int JobSheetHistoryId { get; set; }
        public int JobSheetId { get; set; }
        public int EmployeeId { get; set; }
    }
}
