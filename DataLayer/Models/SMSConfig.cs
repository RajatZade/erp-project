﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class SMSConfig : BaseModel
    {
        public int SmsConfigId { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "This field is required.", AllowEmptyStrings = false)]
        [Display(Name = "Auth Key")]
        public string AuthKey { get; set; }

        [Required(ErrorMessage = "This field is required.", AllowEmptyStrings = false)]
        [Display(Name = "Sender Id")]
        public string SenderId { get; set; }

        [Required(ErrorMessage = "This field is required.", AllowEmptyStrings = false)]
        [Display(Name = "SMS URL")]
        public string SendSMSUri { get; set; }

        [Required(ErrorMessage = "This field is required.", AllowEmptyStrings = false)]
        [Display(Name = "Route")]
        public string Route { get; set; }

        [Display(Name = "Set Default")]
        public bool IsDefault { get; set; }
    }
}
