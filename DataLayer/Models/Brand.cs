﻿using System.ComponentModel.DataAnnotations;

namespace DataLayer.Models
{
    public class Brand:BaseModel
    {
        public int BrandId { get; set; }

        [DisplayFormat(NullDisplayText = "-")]
        [Required(ErrorMessage = "Required")]
        public string BrandName { get; set; }

        public int CompanyId { get; set; }
    }
}
