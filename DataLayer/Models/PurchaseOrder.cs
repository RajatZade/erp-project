﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DataLayer.Utils;

namespace DataLayer.Models
{
    public class PurchaseOrder : BaseModel
    {
        public int PurchaseOrderId { get; set; }

        public Enums.PurchaseOrderStatus? OrderStatus { get; set; }

        [Required(ErrorMessage = "Required")]
        [Range(1, int.MaxValue, ErrorMessage = "Required")]
        public int PurchaseAccountId { get; set; }

        public string PurchaseOrderNo { get; set; }
        public int DivisionId { get; set; }

        public int BranchId { get; set; }

        public int LocationId { get; set; }

        [Required(ErrorMessage = "Required")]
        [Range(1, int.MaxValue, ErrorMessage = "Required")]
        public int PurchaseTypeID { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Required")]
        public int AccountID { get; set; }

        public decimal? Adjustment { get; set; }

        public decimal? TotalInvoiceValue { get; set; }

        [Required(ErrorMessage = "Required")]
        [Range(1, int.MaxValue, ErrorMessage = "Required")]
        public int PaymentId { get; set; }

        public int CompanyId { get; set; }

        public List<PurchaseOrderItem> PurchaseOrderItemList { get; set; }

        #region NotMapped Value
        [NotMapped]
        public decimal ItemTotalQuantity { get; set; }

        [NotMapped]
        public decimal ItemTotalBasicPrice { get; set; }

        [NotMapped]
        public decimal ItemTotalMRP { get; set; }

        [NotMapped]
        public decimal ItemTotalTax { get; set; }

        [NotMapped]
        public decimal ItemPurchasePrice { get; set; }

        [NotMapped]
        public decimal ItemSubTotal { get; set; }

        [NotMapped]
        public decimal ItemFinalTotal { get; set; }

        [NotMapped]
        public decimal? FinalTotalAdj { get; set; }

        [NotMapped]
        public decimal? IGST { get; set; }

        [NotMapped]
        public decimal? CGST { get; set; }

        [NotMapped]
        public decimal? SGST { get; set; }
        #endregion

        #region NotMapped List
        //[NotMapped]
        //public List<SelectListItem> ContactCategoryList { get; set; }
        [NotMapped]
        public List<SelectListItem> PaymentTypeList { get; set; }
        [NotMapped]
        public List<SelectListItem> ShippingTypeList { get; set; }
        [NotMapped]
        public List<SelectListItem> DivisionList { get; set; }
        [NotMapped]
        public List<SelectListItem> BranchList { get; set; }
        [NotMapped]
        public List<SelectListItem> LocationList { get; set; }
        [NotMapped]
        public List<SelectListItem> SalesOrderList { get; set; }
        [NotMapped]
        public List<SelectListItem> PurchaseAccountList { get; set; }
        [NotMapped]
        public List<SelectListItem> AccountList { get; set; }
        [NotMapped]
        public List<SelectListItem> PurchaseTypeList { get; set; }
        [NotMapped]
        public List<SelectListItem> PurchaseMemoList { get; set; }
        [NotMapped]
        public List<SelectListItem> PurchaseOrderList { get; set; }
        #endregion

        #region NotMapped Model
        [NotMapped]
        public SKUMaster SKUMaster { get; set; }
        [NotMapped]
        public GST GST { get; set; }
        [NotMapped]
        public PurchaseOrderItem PurchaseOrderItem { get; set; }
        #endregion

        public PurchaseOrder()
        {
            #region Purchase Invoice List
            //ContactCategoryList = new List<SelectListItem>();
            PaymentTypeList = new List<SelectListItem>();
            ShippingTypeList = new List<SelectListItem>();
            DivisionList = new List<SelectListItem>();
            BranchList = new List<SelectListItem>();
            SalesOrderList = new List<SelectListItem>();
            PurchaseAccountList = new List<SelectListItem>();
            PurchaseMemoList = new List<SelectListItem>();
            PurchaseOrderList = new List<SelectListItem>();
            #endregion
        }
    }
}
