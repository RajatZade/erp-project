﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace DataLayer.Models
{
    public class Company:BaseModel
    {
        public int CompanyId { get; set; }

        //[DisplayFormat(NullDisplayText = "-")]
        [Required]
        public string Name { get; set; }
        //[DisplayFormat(NullDisplayText = "-")]
        public string Address { get; set; }
        //[DisplayFormat(NullDisplayText = "-")]
        public string Address1 { get; set; }
        //[DisplayFormat(NullDisplayText = "-")]
        public string PANCard { get; set; }
        //[DisplayFormat(NullDisplayText = "-")]
        public string GST { get; set; }
        //[DisplayFormat(NullDisplayText = "-")]
        public string City { get; set; }
        //[DisplayFormat(NullDisplayText = "-")]
        public string State { get; set; }
        //[DisplayFormat(NullDisplayText = "-")]
        public int PINCode { get; set; }
        [RegularExpression(@"^((\+91-?)|0)?[0-9]{10}$", ErrorMessage = "Invalid number")]
        public string PhoneNo { get; set; }

        [RegularExpression(@"^((\+91-?)|0)?[0-9]{10}$", ErrorMessage = "Invalid number")]
        public string MobileNo { get; set; }

        [DataType(DataType.EmailAddress)]
        public string EmailId { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime FinacialYearStarting { get; set; }

        public int LedgerId { get; set; }

        [NotMapped]
        public List<SelectListItem> BankLedgerList { get; set; }

        public Company()
        {
            BankLedgerList = new List<SelectListItem>();
        }
    }
}
