﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace DataLayer.Models
{
    public class Lead:BaseModel
    {
        public int LeadId { get; set; }

        public string LeadOwner { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        [Required(ErrorMessage = "Required")]
        [RegularExpression(@"^((\+91-?)|0)?[0-9]{10}$", ErrorMessage = "Invalid Number")]
        public string PhoneNo { get; set; }

        [Required(ErrorMessage = "Required")]
        [RegularExpression(@"^((\+91-?)|0)?[0-9]{10}$", ErrorMessage = "Invalid Number")]
        public string MobileNo { get; set; }

        public string CompanyName { get; set; }

        public string Fax { get; set; }

        [Required(ErrorMessage = "Required", AllowEmptyStrings = false)]
        [DataType(DataType.EmailAddress)]
        public string EmailId { get; set; }

        public string Title { get; set; }

        [Required(ErrorMessage = "Required", AllowEmptyStrings = false)]
        [DataType(DataType.Url)]
        public string WebSite { get; set; }

        public string LeadSource { get; set; }

        public string LeadStatus { get; set; }

        public string Industry { get; set; }

        public string Rating { get; set; }

        public decimal AnnualRevenue { get; set; }

        public int NoOfEmployee { get; set; }

        public string Street { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        [Required(ErrorMessage = "Required")]
        [RegularExpression(@"^((\+91-?)|0)?[0-9]{6}$", ErrorMessage = "Required")]
        public string ZipCode { get; set; }

        public string Country { get; set; }

        public string ProductInterest { get; set; }

        public string SICCode { get; set; }
        public List<LeadItem> LeadItemList { get; set; }

        public int NumberOfLocations { get; set; }

        public string CurrentGenerators { get; set; }

        public string Primary { get; set; }

        public string Description { get; set; }
        [NotMapped]
        public int ContactId { get; set; }

        [NotMapped]
        public List<SelectListItem> ContactList { get; set; }
        [NotMapped]
        public string OtherCity { get; set; }

        [NotMapped]
        public bool Other { get; set; }

        [NotMapped]
        public LeadItem LeadItem { get; set; }

        [NotMapped]
        public List<SelectListItem> LeadSourceList { get; set; }

        [NotMapped]
        public Enums.LeadProoductStatus? LeadProoductStatus { get; set; }

        [NotMapped]
        public List<SelectListItem> LeadStatusList { get; set; }

        [NotMapped]
        public List<SelectListItem> IndustryList { get; set; }

        [NotMapped]
        public List<SelectListItem> RatingList { get; set; }

        [NotMapped]
        public List<SelectListItem> ProductInterestList { get; set; }

        [NotMapped]
        public List<SelectListItem> PrimaryList { get; set; }

        public Lead()
        {
            LeadSourceList = new List<SelectListItem>()
            {
                new SelectListItem() { Text="--Select One--",Value="-1"},
                new SelectListItem() { Text="Source1",Value="Source1"},
                new SelectListItem() { Text="Source2",Value="Source2"}
            };
            LeadStatusList = new List<SelectListItem>()
                {
                new SelectListItem() { Text="--Select One--",Value="-1"},
                new SelectListItem() { Text="Cold Call",Value="Cold Call"},
                new SelectListItem() { Text="Detailed Discussion Level",Value="Detailed Discussion Level"},
                new SelectListItem() { Text="Negotiation Level",Value="Negotiation Level"}
            };
            IndustryList = new List<SelectListItem>()
                {
                new SelectListItem() { Text="--Select One--",Value="-1"},
                new SelectListItem() { Text="Industry1",Value="Industry1"},
                new SelectListItem() { Text="Industry2",Value="Industry2"}
            };
            RatingList = new List<SelectListItem>()
                {
                new SelectListItem() { Text="--Select One--",Value="-1"},
                new SelectListItem() { Text="Rating1",Value="Rating1"},
                new SelectListItem() { Text="Rating2",Value="Rating2"}
            };
            ProductInterestList = new List<SelectListItem>()
                {
                new SelectListItem() { Text="--Select One--",Value="-1"},
                new SelectListItem() { Text="ProductInt1",Value="ProductInt1"},
                new SelectListItem() { Text="ProductInt2",Value="ProductInt2"}
            };
            PrimaryList = new List<SelectListItem>()
                {
                new SelectListItem() { Text="--Select One--",Value="-1"},
                new SelectListItem() { Text="Primary1",Value="Primary1"},
                new SelectListItem() { Text="Primary2",Value="Primary2"}
            };
            ContactList = new List<SelectListItem>();
        }
    }
}
