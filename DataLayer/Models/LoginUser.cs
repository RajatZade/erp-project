﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace DataLayer.Models
{
    public class LoginUser : BaseModel
    {
        public int LoginUserId { get; set; }
        public string Username { get; set; }
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public int ContactId { get; set; }
        public bool IsActive { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Required")]
        public int? RoleId { get; set; }
        
        public virtual Role Role { get; set; }
        
        public virtual Contact Contact { get; set; }
        [NotMapped]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
        [NotMapped]
        public List<SelectListItem> RolesList { get; set; }

        public LoginUser()
        {
            RolesList = new List<SelectListItem>();
        }
    }
}
