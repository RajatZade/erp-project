﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.Models
{
    public class BaseModel
    {
        public DateTime? LastModifiedDate { get; set; }

        [Display(Name = "Start Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }

    }
}
