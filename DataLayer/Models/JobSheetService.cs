﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class JobSheetService : BaseModel
    {
        public int JobSheetServiceId { get; set; }
        public int ServiceTypeId { get; set; }
        public int LedgerId { get; set; }
        public int? JobSheetId { get; set; }
        public int? Quantity { get; set; }
        public decimal? Price { get; set; }
        public int? GSTId { get; set; }
        public ServiceType ServiceType { get; set; }
        public GST GST { get; set; }
        public Ledger Ledger { get; set; }
        [NotMapped]
        public decimal? TotalIncludeGST { get; set; }
    }
}
