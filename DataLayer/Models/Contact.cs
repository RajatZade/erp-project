﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace DataLayer.Models
{
    public class Contact : BaseModel
    {
        public int ContactId { get; set; }

        public Enums.TitleType? TitleList { get; set; }

        [DisplayFormat(NullDisplayText = "-")]
        public string FirstName { get; set; }

        public string MiddleName { get; set; }
        public string LastName { get; set; }

        [DisplayFormat(NullDisplayText = "-")]
        public string CompanyName { get; set; }

        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }

        [RegularExpression(@"(^$)|(^[0-9a-fA-FxX]{10}$)", ErrorMessage = "Invalid Number")]
        public string Phone { get; set; }
        public string Mobile { get; set; }        
        public string SocialId { get; set; }

        [DisplayFormat(NullDisplayText = "-")]
        public string GSTNo { get; set; }

        public string EmployeeNumber { get; set; }
        public string CustomerNumber { get; set; }
        public string SupplierNumber { get; set; }
        public decimal? Salary { get; set; }

        [RegularExpression(@"(^$)|(^[0-9a-fA-FxX]{12}$)", ErrorMessage = "Invalid Number")]
        public string AadharCardNo { get; set; }
        public string IdentityType { get; set; }
        public string IdentityCardNo { get; set; }
        public string PanCardNo { get; set; }

        [DisplayFormat(NullDisplayText = "-")]
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }

        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }

        public bool IsVisible { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
        public string Type { get; set; }

        public int? DesignationId { get; set; }
        public int? BranchId { get; set; }
        public int? DivisionId { get; set; }

        public bool IsLedgerRequired { get; set; }
        public bool IsLoginRequired { get; set; }

        [NotMapped]
        public string DailyWage { get; set; }
        [NotMapped]
        public string NetAnnualPackage { get; set; }
        [NotMapped]
        public virtual Ledger Ledger { get; set; }
        [NotMapped]
        public virtual LoginUser LoginUser { get; set; }
        [NotMapped]
        public List<SelectListItem> IdentityTypeList { get; set; }
        [NotMapped]
        public List<SelectListItem> DivisionList { get; set; }
        [NotMapped]
        public List<SelectListItem> BranchList { get; set; }
        [NotMapped]
        public List<SelectListItem> DesignationList { get; set; }

        public Contact GetContact(Contact Existing,Contact New)
        {
            Existing.AddressLine1 = New.AddressLine1;
            Existing.AddressLine2 = New.AddressLine2;
            Existing.Country = New.Country;
            Existing.State = New.State;
            Existing.City = New.City;
            Existing.Mobile = New.Mobile;
            Existing.Phone = New.Phone;
            Existing.Email = New.Email;
            Existing.PanCardNo = New.PanCardNo;
            Existing.GSTNo = New.GSTNo;
            Existing.SocialId = New.SocialId;
            Existing.Type = New.Type;
            return Existing;
        }
        public Contact()
        {
            IdentityTypeList = new List<SelectListItem>();
            IdentityTypeList.Add(new SelectListItem() { Text = "--Select One--", Value = "-1" });
            IdentityTypeList.Add(new SelectListItem() { Text = "Driving licence", Value = "Driving_licence" });
            IdentityTypeList.Add(new SelectListItem() { Text = "Aadhar Card", Value = "Aadhar_Card" });
            IdentityTypeList.Add(new SelectListItem() { Text = "Voiting Card", Value = "Voiting_Card" });
            IdentityTypeList.Add(new SelectListItem() { Text = "Pan Card", Value = "Pan_Card" });
            IdentityTypeList.Add(new SelectListItem() { Text = "Passport", Value = "Passport" });

            DivisionList = new List<SelectListItem>();
            DesignationList = new List<SelectListItem>();
            BranchList = new List<SelectListItem>();
        }
    }
}
