﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class VoucherType
    {
        public int VoucherTypeId { get; set; }
        public string Name { get; set; }
        public bool DisableDelete { get; set; }
    }
}
