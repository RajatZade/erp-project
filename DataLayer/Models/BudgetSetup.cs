﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class BudgetSetup : BaseModel
    {
        public int BudgetSetupId { get; set; }
        public string Name { get; set; }
        public decimal AnnualSales { get; set; }
        public decimal? QuarterlySales { get; set; }
        public decimal? MonthlySales { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? Starting { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? EndingDate { get; set; }

        public List<Budgeting> BudgetingList = new List<Budgeting>();

        [NotMapped]
        public Budgeting Budgeting = new Budgeting();

    }
}
