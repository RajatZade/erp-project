﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace DataLayer.Models
{
    public class JobSheet : BaseModel
    {
        public int JobSheetId { get; set; }
        public string ProductNumber { get; set; }
        public string Description { get; set; }
        public int CustomerId { get; set; }
        public int? SKUMasterId { get; set; }
        public int CompanyId { get; set; }
        public decimal? Quantity { get; set; }
        public int? JobSheetInProcessStatusId { get; set; }
        public int? ParentJobSheetId { get; set; }
        public decimal TotalProductionCost { get; set; }
        public Enums.JobSheetComplete? JobSheetComplete { get; set; }
        public int? PurchaseOrderId { get; set; }
        public bool IsInventoryUpdate { get; set; }
        public bool IsProductCreated { get; set; }
        public virtual SKUMaster SKUMaster { get; set; }
        public virtual JobSheetInProcessStatus JobSheetInProcessStatus { get; set; }
        public List<JobSheetItem> JobSheetItemList { get; set; }
        public List<JobSheetService> JobSheetServicesList { get; set; }
        public string MeasurmentType { get; set; }
        public Enums.JobSheetStatus? JobSheetStatus { get; set; }
        public int? SaleInvoiceId { get; set; }
        public int? SaleOrderId { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? DeliveryDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime JobDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? JobCompletionDate { get; set; }

        [NotMapped]
        public decimal SubtotalNotMapped { get; set; }
        [NotMapped]
        public decimal TotalTaxNotMapped { get; set; }
        [NotMapped]
        public decimal TotalNotMapped { get; set; }
        [NotMapped]
        public string Search { get; set; }
        [NotMapped]
        public int CategoryId { get; set; }
        [NotMapped]
        public int SubCategoryId { get; set; }
        [NotMapped]
        public int MeasurmentSetId { get; set; }
        [NotMapped]
        public int ServicesListQuantity { get; set; }
        [NotMapped]
        public decimal TotalProductionCostNotMapped { get; set; }
        [NotMapped]
        public decimal TotalServiceCostNotMapped { get; set; }
        [NotMapped]
        public decimal ProductionCostPerUnit { get; set; }
        [NotMapped]
        public decimal FurtherProcessTotal { get; set; }
        [NotMapped]
        public bool IsCustomer { get; set; }
        [NotMapped]
        public bool IsTailor { get; set; }
        [NotMapped]
        public string TailorName { get; set; }
        [NotMapped]
        public string Name { get; set; }
        [NotMapped]
        public string ConatctNumber { get; set; }
        [NotMapped]
        public string Address { get; set; }
        [NotMapped]
        public string OrderNumber { get; set; }
        [NotMapped]
        public string BarcodeList { get; set; }
        [NotMapped]
        public int? SalesOrderItemId { get; set; }
        #region NotMapped SelectList
        [NotMapped]
        public List<SelectListItem> CustomerList { get; set; }
        [NotMapped]
        public List<SelectListItem> EmployeeList { get; set; }
        [NotMapped]
        public List<SelectListItem> MeasurmentList { get; set; }
        [NotMapped]
        public List<SelectListItem> ProductionList { get; set; }
        [NotMapped]
        public List<SelectListItem> CategoryList { get; set; }
        [NotMapped]
        public List<SelectListItem> SubcategoryList { get; set; }
        [NotMapped]
        public List<SelectListItem> SKUList { get; set; }
        [NotMapped]
        public List<SelectListItem> PurchaseOrderList { get; set; }
        [NotMapped]
        public List<SelectListItem> ServiceTypeList { get; set; }
        [NotMapped]
        public List<SelectListItem> GSTList { get; set; }
        [NotMapped]
        public List<SelectListItem> InProcessStatus { get; set; }
        [NotMapped]
        public List<SelectListItem> SaleInvoiceList { get; set; }
        [NotMapped]
        public List<SelectListItem> SaleOrderList { get; set; }
        #endregion

        #region NotMapped Model
        [NotMapped]
        public virtual MeasurmentSet MeasurmentSet { get; set; }
        [NotMapped]
        public virtual SaleInvoice SaleInvoice { get; set; }
        [NotMapped]
        public virtual SalesOrder SalesOrder { get; set; }
        [NotMapped]
        public JobSheetItem JobSheetItem { get; set; }
        [NotMapped]
        public JobSheetService JobSheetServices { get; set; }
        [NotMapped]
        public List<Measurment> NewMeasurmentList { get; set; }
        [NotMapped]
        public List<MeasurmentSet> MeasurmentSetListForReadymade { get; set; }        
        [NotMapped]
        public List<MeasurmentSet> MeasurmentSetListForCustome { get; set; }
        [NotMapped]
        public List<JobSheet> FutherProcessJobSheetList { get; set; }
        [NotMapped]
        public List<SaleInvoiceItem> SaleInvoiceItemList { get; set; }
        [NotMapped]
        public Ledger AssignedWorker { get; set; }
        [NotMapped]
        public Ledger Customer { get; set; }
        #endregion

        public JobSheet()
        {
            CategoryList = new List<SelectListItem>();
            SKUList = new List<SelectListItem>();
            CustomerList = new List<SelectListItem>();
            EmployeeList = new List<SelectListItem>();
            MeasurmentList = new List<SelectListItem>();
            ProductionList = new List<SelectListItem>();
            ServiceTypeList = new List<SelectListItem>();
            PurchaseOrderList = new List<SelectListItem>();
            GSTList = new List<SelectListItem>();
            SaleOrderList = new List<SelectListItem>();
            SaleInvoiceList = new List<SelectListItem>();
            InProcessStatus = new List<SelectListItem>();
            PurchaseOrderList.Add(new SelectListItem() { Text = "--Select One--", Value = "-1" });
            MeasurmentSetListForCustome = new List<MeasurmentSet>();
        }
    }
}
