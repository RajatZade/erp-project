﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Models
{
    public class SalesOrderItem : BaseModel
    {
        public int SalesOrderItemId { get; set; }
        public int SalesOrderId { get; set; }
        public decimal Quantity { get; set; }
        public int SKUMasterId { get; set; }
        public SKUMaster SKUMaster { get; set; }
        public string Remark { get; set; }
        public decimal EstimatedPrice { get; set; }
        public bool IsJobSheetCreated { get; set; }
        public int? JobSheetId { get; set; }
        public bool IsSaleInvoiceCreated { get; set; }
        public int? SaleInvoiceId { get; set; }

        [NotMapped]
        public string JobSheetNumber { get; set; }
        [NotMapped]
        public JobSheet JobSheet { get; set; }
        [NotMapped]
        public string SKU { get; set; }
        [NotMapped]
        public SaleInvoice SaleInvoice { get; set; }
    }
}
