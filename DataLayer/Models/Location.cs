﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace DataLayer.Models
{
    public class Location:BaseModel
    {
        public int LocationId { get; set; }

        [Required(ErrorMessage = "Required")]
        public string LocationName { get; set; }

        [Required(ErrorMessage ="Required")]
        [Range(1,int.MaxValue,ErrorMessage =("Required"))]
        public int BranchId { get; set; }

        public virtual Branch Branch { get; set; }

        [NotMapped]
        public List<SelectListItem> BranchList { get; set; }

        public Location()
        {
            BranchList = new List<SelectListItem>();
        }

    }
}
