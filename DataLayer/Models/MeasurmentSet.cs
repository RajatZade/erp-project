﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Models
{
    public class MeasurmentSet
    {        
        public int MeasurmentSetId { get; set; }
        public int JobSheetId { get; set; }
        public string MeasurmentValues { get; set; }
        public virtual JobSheet JobSheet { get; set; }
        public int? MeasurmentId { get; set; }
        public decimal? Quantity { get; set; }
        public Enums.MeasurementType Type { get; set; }
        [NotMapped]
        public List<MeasurementAccessModel> MeasurementList { get; set; }
        [NotMapped]
        public Measurment Measurment { get; set; }
    }
}
