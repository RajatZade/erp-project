﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class JobSheetInProcessStatus : BaseModel
    {
        public int JobSheetInProcessStatusId { get; set; }
        public string Status { get; set; }
    }
}
