﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace DataLayer.Models
{
    public class SaleInvoice : BaseModel
    {
        public int SaleInvoiceId { get; set; }
        [NotMapped]
        public string Barcode { get; set; }
        public string SaleInvoiceNo { get; set; }
        public string ReturnInvoiceNo { get; set; }
        public string Status { get; set; }

        public decimal TotalInvoiceValue { get; set; }
        public decimal TotalDiscount { get; set; }
        public decimal TotalTax { get; set; }
        public decimal Subtotal { get; set; }
        public decimal? SalesManCommision { get; set; }
        public int? LedgerBankId { get; set; }
        public bool IsPaymentDone { get; set; }
        public int CompanyId { get; set; }
        public bool? IsWPS { get; set; }
        public string Type { get; set; }
        public int? ReturnSaleInvoiceId { get; set; }
        public string ReturnSaleInvoiceNumber { get; set; }
        public decimal TotalWithAdjustment { get; set; }
        public decimal? DiscountWithAdjustment { get; set; }
        public decimal? Cash { get; set; }
        public decimal? TenderChange { get; set; }
        public string Time { get; set; }
        public decimal? Adjustment { get; set; }
        public string ShippingDetails { get; set; }
        public bool IsLedgerUpdate { get; set; }
        public bool IsInventoryUpdate { get; set; }
        public string ChequeNumber { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime InvoicingDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? DueDate { get; set; }
        //[Range(1, int.MaxValue, ErrorMessage = "Required")]
        //public int ShippingId { get; set; }

        public string ShippingType { get; set; }
         
        [Range(1, int.MaxValue, ErrorMessage = "Required")] 
        public int DivisionId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Required")]
        public int BranchId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Required")]
        public int LocationId { get; set; }

        public int? EmployeeId { get; set; }

        public int? SaleMemoId { get; set; }

        public int SaleOrderId { get; set; }

        [NotMapped]
        public SaleInvoiceItem SaleInvoiceItem { get; set; }

        public List<SaleInvoiceItem> SaleInvoiceItemList { get; set; }

        [NotMapped]
        public Product Product { get; set; }

        public int PurchaseOrderId { get; set; }

        [ForeignKey("SaleType")]
        public int? SaleTypeId { get; set; }
        public virtual Ledger SaleType { get; set; }

        [ForeignKey("SaleAccount")]
        public int? SaleAccountId { get; set; }
        public virtual Ledger SaleAccount { get; set; }

        [ForeignKey("Customer")]
        [Range(1, int.MaxValue, ErrorMessage = "Required")]
        public int CustomerId { get; set; }
        public virtual Ledger Customer { get; set; }

        public int? PaymentTypeId { get; set; }
        public PaymentType PaymentType { get; set; }
        #region NotMapped Value
        [NotMapped]
        public decimal? FinalTotalAdj { get; set; }
        [NotMapped]
        public decimal AmountPaid { get; set; }
        [NotMapped]
        public decimal AmountRemaining { get; set; }
        [NotMapped]
        public decimal? Points { get; set; }
        [NotMapped]
        public decimal? SalesManCommisionNotMap { get; set; }
        [NotMapped]
        public int? SalesOrderItemId { get; set; }
        [NotMapped]
        public string Search { get; set; }
        [NotMapped]
        public decimal TotalDiscountNotMapped { get; set; }
        [NotMapped]
        public decimal TotalTaxNotMapped { get; set; }
        [NotMapped]
        public decimal SubtotalNotMapped { get; set; }
        [NotMapped]
        public decimal TotalNotMapped { get; set; }
        [NotMapped]
        public decimal TotalSGST { get; set; }
        [NotMapped]
        public decimal TotalCGST { get; set; }
        [NotMapped]
        public decimal TotalIGST { get; set; }
        [NotMapped]
        public string TotalInWord { get; set; }
        [NotMapped]
        public virtual Ledger Ledger { get; set; }
        [NotMapped]
        public int NumberOfProduct { get; set; }
        [NotMapped]
        public string DeliveryMemoNumber { get; set; }
        [NotMapped]
        public decimal TotalQuantity { get; set; }
        [NotMapped]
        public bool ChangeHeader { get; set; }
        [NotMapped]
        public int SKUMasterId { get; set; }
        [NotMapped]
        public string SKU { get; set; }
        [NotMapped]
        public string ReturnType { get; set; }
        #endregion

        #region NotMapped List
        [NotMapped]
        public List<SelectListItem> PaymentTypeList { get; set; }
        //[NotMapped]
        //public List<SelectListItem> ShippingTypeList { get; set; }
        [NotMapped]
        public List<SelectListItem> DivisionList { get; set; }
        //[NotMapped]
        //public List<SelectListItem> BankAccountsList { get; set; }
        [NotMapped]
        public List<SelectListItem> BranchList { get; set; }
        [NotMapped]
        public List<SelectListItem> LocationList { get; set; }
        [NotMapped]
        public List<SelectListItem> SalesOrderList { get; set; }
        [NotMapped]
        public List<SelectListItem> SaleAccountList { get; set; }
        [NotMapped]
        public List<SelectListItem> CustomerAccountList { get; set; }
        [NotMapped]
        public List<SelectListItem> SaleTypeList { get; set; }
        [NotMapped]
        public List<SelectListItem> SaleMemoList { get; set; }
        [NotMapped]
        public List<SelectListItem> SaleOrderList { get; set; }
        [NotMapped]
        public List<SelectListItem> EmployeeAccountList { get; set; }
        [NotMapped]
        public List<ProductVoucher> ProductVoucherItems { get; set; }
        [NotMapped]
        public ProductVoucher ProductVoucher { get; set; }
        [NotMapped]
        public List<AttributeSet> AttributeSetListNew { get; set; }
        [NotMapped]
        public List<Product> WSPProductList { get; set; }
        #endregion 

        public SaleInvoice()
        {
            //ContactCategoryList = new List<SelectListItem>();
            PaymentTypeList = new List<SelectListItem>();
            //ShippingTypeList = new List<SelectListItem>();
            DivisionList = new List<SelectListItem>();
            BranchList = new List<SelectListItem>();
            LocationList = new List<SelectListItem>();
            SalesOrderList = new List<SelectListItem>();
            SaleAccountList = new List<SelectListItem>();
            SaleMemoList = new List<SelectListItem>();
            SaleOrderList = new List<SelectListItem>();
            SaleTypeList = new List<SelectListItem>();
            CustomerAccountList = new List<SelectListItem>();
            EmployeeAccountList = new List<SelectListItem>();
        }

        public SaleInvoice(SaleInvoice model)
        {
            InvoicingDate = model.InvoicingDate;
            ShippingType = model.ShippingType;
            DivisionId = model.DivisionId;
            BranchId = model.BranchId;
            LocationId = model.LocationId;
            EmployeeId = model.EmployeeId;
            SaleMemoId = model.SaleMemoId;
            SaleOrderId = model.SaleOrderId;
            SaleTypeId = model.SaleTypeId;
            SaleAccountId = model.SaleAccountId;
            CustomerId = model.CustomerId;
            PaymentTypeId = model.PaymentTypeId;
            ReturnSaleInvoiceId = model.SaleInvoiceId;
            ReturnSaleInvoiceNumber = model.SaleInvoiceNo;
        }
    }
}
