﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DataLayer.Models
{
    public class SKUMaster : BaseModel
    {
        public int SKUMasterId { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = ("Required"))]
        public int CategoryId { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = ("Required"))]
        public int SubCategoryId { get; set; }

        [Required(ErrorMessage = "Required")]
        public string SKU { get; set; }

        public int CompanyId { get; set; }

        [Required(ErrorMessage = "Required")]
        public string ProductName { get; set; }

        public string ShortendDescriptionBase { get; set; }

        public string GstforSelling { get; set; }

        public virtual Category Category { get; set; }

        public virtual SubCategory SubCategory { get; set; }

        [NotMapped]
        public string CategoryName { get; set; }

        [NotMapped]
        public string SubCategoryName { get; set; }

        [NotMapped]
        public List<SelectListItem> CategoryList { get; set; }


        [NotMapped]
        public List<SelectListItem> SubCategoryList { get; set; }

        //[NotMapped]
        //public int CountList { get; set; }

        public SKUMaster()
        {
            CategoryList = new List<SelectListItem>();
            SubCategoryList = new List<SelectListItem>();
        }

    }
}
