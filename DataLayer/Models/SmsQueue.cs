﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class SmsQueue : BaseModel
    {
        public int SmsQueueId { get; set; }
        public string To { get; set; }
        public string From { get; set; }
        public string CC { get; set; }
        public string Bcc { get; set; }
        public bool IsBodyHtml { get; set; }
        public string Subject { get; set; }

        [Required(ErrorMessage = "This filed is required.")]
        public string Body { get; set; }
        public string Response { get; set; }
        public bool IsProcess { get; set; }
        public string PhoneNo { get; set; }

        public string Destination { get; set; }

        public Enums.SmsType SmsType { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Send Date")]
        public DateTime? SendDate { get; set; }
    }
}
