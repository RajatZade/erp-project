﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace DataLayer.Models
{
    public class Category : BaseModel
    {
        public int CategoryId { get; set; }     
        
        [DisplayFormat(NullDisplayText = "-")]
        public string Name { get; set; }
        public string CategoryNumber { get; set; }

        public int CompanyId { get; set; }
    }
}
