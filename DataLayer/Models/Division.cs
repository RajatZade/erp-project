﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace DataLayer.Models
{
    public class Division:BaseModel
    {
        public int DivisionId { get; set; }
        [Required(ErrorMessage = "Required")]
        public string DivisionName { get; set; }

        [Required(ErrorMessage = "Required")]
        [Range(1,int.MaxValue,ErrorMessage ="Required")]
        public int CompanyId { get; set; }

        [NotMapped]
        public Company Company { get; set; }
        [NotMapped]
        public List<SelectListItem> DivisionList { get; set; }
        [NotMapped]
        public List<SelectListItem> CompanyList { get; set; }

        public Division()
        {
            CompanyList = new List<SelectListItem>();
        }
    }
}
