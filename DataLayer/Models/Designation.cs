﻿using System.ComponentModel.DataAnnotations;

namespace DataLayer.Models
{
    public class Designation : BaseModel
    {
        public int DesignationId { get; set; }
        //[Required(ErrorMessage = "Required")]
        [DisplayFormat(NullDisplayText = "-")]
        public string DesignationType { get; set; }
    }
}
