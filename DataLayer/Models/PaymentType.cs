﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DataLayer.Models
{
    public class PaymentType
    {
        public int PaymentTypeId { get; set; }
        [Required(ErrorMessage = "Required")]
        public string PaymentName { get; set; }
        public bool DisableDelete { get; set; }
        public int? LedgerId { get; set; }
        public Ledger Ledger { get; set; }
        public int? CompanyId { get; set; }
        [NotMapped]
        public List<SelectListItem> LedgerList { get; set; }

        public PaymentType()
        {
            LedgerList = new List<SelectListItem>();
        }
    }
}
