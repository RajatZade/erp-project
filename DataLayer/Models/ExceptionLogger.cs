﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.Models
{
    public class ExceptionLogger
    {
        public int ExceptionLoggerId { get; set; }

        [MaxLength]
        public string ExceptionMessage { get; set; }

        public DateTime Date { get; set; }

        [MaxLength]
        public string ClassName { get; set; }
    }
}
