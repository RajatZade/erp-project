﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace DataLayer.Models
{
    public class Ledger : BaseModel
    {
        public int LedgerId { get; set; }

        public int? ContactId { get; set; }

        public virtual Contact Contact { get; set; }
        public string Name { get; set; }

        [Range(1,int.MaxValue,ErrorMessage ="Required")]
        public int? AccountCategoryId { get; set; }

        public int? AccountSubCategoryId { get; set; }

        public decimal StartingBalance { get; set; }

        public decimal? WalletBalance { get; set; }

        [Required(ErrorMessage = "Required")]
        public string Type { get; set; }

        public Boolean IsBank { get; set; }

        public Boolean IsDisableEdit { get; set; }

        public virtual AccountCategory AccountCategory { get; set; }

        public virtual AccountSubCategory AccountSubCategory { get; set; }

        public string Number { get; set; }

        public string IFSC { get; set; }

        public string SwiftCode { get; set; }
        public string AccountNo { get; set; }
        public string Address { get; set; }
        public string BankName { get; set; }

        public string LedgerType { get; set; }

        public int? CompanyId { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? Date { get; set; }

        [NotMapped]
        public List<SelectListItem> ContactCategoryList { get; set; }

        [NotMapped]
        public List<SelectListItem> ContactSubCategoryList { get; set; }

        #region NotMapped Field
        [NotMapped]
        public decimal CurrentBalance { get; set; }
        [NotMapped]
        public SelectList TypesList { get; set; }
        [NotMapped]
        public string CategoryName { get; set; }
        [NotMapped]
        public string SubCategoryName { get; set; }
        [NotMapped]
        public decimal AvailableBalance { get; set; }
        [NotMapped]
        public string other { get; set; }
        [NotMapped]
        public string OpeningBalanceType { get; set; }
        [NotMapped]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? EntryDate { get; set; }
        [NotMapped]
        public SelectList ContactTypesList { get; set; }
        #endregion

        public Ledger()
        {
            ContactCategoryList = new List<SelectListItem>();
            ContactSubCategoryList = new List<SelectListItem>();
            TypesList = new SelectList(new[]
                      {
                            new SelectListItem {Text =null, Value="--Select One--" },
                           new SelectListItem {Text = "CR", Value = "CR"},
                           new SelectListItem {Text = "DR", Value = "DR"},
                       }, "Text", "Value");
            ContactTypesList = new SelectList(new[]
                {
                            new SelectListItem {Text =null, Value="--Select One--" },
                           new SelectListItem {Text = "Customer", Value = "Customer"},
                           new SelectListItem {Text = "Employee", Value = "Employee"},
                           new SelectListItem {Text = "Supplier", Value = "Supplier"},
                           new SelectListItem {Text = "Other", Value = "Other"},
                       }, "Text", "Value");
        }
    }
}
