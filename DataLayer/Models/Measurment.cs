﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Models
{
    public class Measurment
    {
        public int MeasurmentId { get; set; }
        [Required(ErrorMessage = "Required")]
        public string MeasurmentType { get; set; }
        public string MeasurementValues { get; set; }
        [NotMapped]
        public List<string> MeasurmentListForDetails { get; set; }
    }
}
