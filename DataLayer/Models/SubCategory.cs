﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace DataLayer.Models
{
    public class SubCategory : BaseModel
    {
        public int SubCategoryId { get; set; }

        [Required(ErrorMessage ="Required")]
        public string Name { get; set; }

        [ForeignKey("Category")]
        [Range(1, int.MaxValue, ErrorMessage = "Required")]
        public int CategoryId { get; set; }

        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }

        public string SubCategoriesNumber { get; set; }

        public int CompanyId { get; set; }

        [NotMapped]
        public List<SelectListItem> CategoryList { get; set; }

        public SubCategory()
        {
            CategoryList = new List<SelectListItem>();
        }
    }
}
