﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace DataLayer.Models
{
    public class Budgeting : BaseModel
    {
        public int BudgetingId { get; set; }
        public int LedgerId { get; set; }
        public int BudgetSetupId { get; set; }
        public decimal? AnnualBudget { get; set; }
        public decimal? QuarterlyBudget { get; set; }
        public decimal? MonthlyBudget { get; set; }
        public decimal? Percent { get; set; }

        [NotMapped]
        public virtual Ledger Ledger { get; set; }
        [NotMapped]
        public List<SelectListItem> LedgerList { get; set; }

        public Budgeting()
        {
            LedgerList = new List<SelectListItem>();
        }
    }
}
