﻿namespace DataLayer.Models
{
    public class AttributeType
    {
        public int AttributeTypeId { get; set; }
        public int PurchaseInvoiceItemId { get; set; }
        public int PurchaseOrderItemId { get; set; }
        public string AttributeTypeName { get; set; }
        public string AttributeValue { get; set; }
    }
}
