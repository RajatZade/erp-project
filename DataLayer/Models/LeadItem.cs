﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class LeadItem : BaseModel
    {
        public int LeadItemId { get; set; }
        public int LeadId { get; set; }
        public string ProductName { get; set; }
        public string Quantity { get; set; }
        public string Rate { get; set; }
        public Enums.LeadProoductStatus LeadProoductStatus { get; set; }
    }
}
