﻿using System.ComponentModel.DataAnnotations;

namespace DataLayer.Models
{
    public class AccountCategory:BaseModel
    {
        public int AccountCategoryId { get; set; }
        [DisplayFormat(NullDisplayText = "-")]
        [Required(ErrorMessage = "Required")]
        public string Name { get; set; }

        public bool DisableDelete { get; set; }

    }
}
