﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using DataLayer.Utils;

namespace DataLayer.Models
{
    public class Product : BaseModel
    {
        public int ProductId { get; set; }

        public int? AttributeLinkingId { get; set; }

        public decimal Rate { get; set; }

        public decimal? AdditionalPurchaseExpenses { get; set; }
        public int? FromLocation { get; set; }
        public decimal? NetAmount { get; set; }
        public decimal MRP { get; set; }
        public decimal SalesmanCommision { get; set; }
        public string Remark { get; set; }
        public decimal? Adjustment { get; set; }
        public decimal Quantity { get; set; }
        public int HSNId { get; set; }
        public int UnitId { get; set; }
        public int GSTId { get; set; }
        public int SKUMasterId { get; set; }
        public int BrandId { get; set; }
        public decimal? SubTotal { get; set; }
        public decimal ApplicableTax { get; set; }
        public int? JobSheetId { get; set; }
        public string JobSheetNumber { get; set; }
        public string DiscountsAccessible { get; set; }
        public bool SellInPartial { get; set; }
        public string Name { get; set; }
        public int PurchaseInvoiceItemId { get; set; }
        public string SerialNumber { get; set; }
        public int LocationId { get; set; }
        public bool IsSold { get; set; }
        public bool IsBooked { get; set; }

        public string BarcodeString { get; internal set; }

        public decimal? WSPPercent { get; set; }
        public decimal? WSPAmount { get; set; }
        public int DivisionId { get; set; }
        public int BranchId { get; set; }
        public int CompanyId { get; set; }

        public decimal? CGST { get; set; }
        public decimal? SGST { get; set; }
        public decimal? IGST { get; set; }
        public decimal? OtherTaxes { get; set; }
        public bool? IsOpeningStock { get; set; }


        public virtual SKUMaster SKUMaster { get; set; }     
        public virtual HSN HSN { get; set; }      
        public virtual GST GST { get; set; }
        public Brand Brand { get; set; }
        public Unit Unit { get; set; }
        public PurchaseInvoiceItem PurchaseInvoiceItem { get; set; }
        #region NotMapped Properties
        [NotMapped]
        public int? SubCategoryId { get; set; }
        [NotMapped]
        public int Agedays { get; set; }
        [NotMapped]
        public int CategoryId { get; set; }
        [NotMapped]
        public bool checkProduct { get; set; }
        [NotMapped]
        [DisplayFormat(NullDisplayText = "-")]
        public string PurchaseInvoiceNumber { get; set; }

        [NotMapped]
        public int PurchaseInvoiceId { get; set; }
        [NotMapped]
        public List<string> AttributeList { get; set; }
        [NotMapped]
        public int[] DiscountIds { get; set; }
        [NotMapped]
        public string Barcode { get; set; }
        [NotMapped]
        public virtual SubCategory SubCategory { get; set; }
        [NotMapped]
        public AttributeLinking AttributeLinking { get; set; }
        #endregion

        #region NotMapped SelectList
        [NotMapped]
        public List<SelectListItem> SubCategoryList { get; set; }
        [NotMapped]
        public List<SelectListItem> CategoryList { get; set; }
        [NotMapped]
        public List<SelectListItem> UnitList { get; set; }
        [NotMapped]
        public List<SelectListItem> HSNList { get; set; }
        [NotMapped]
        public List<SelectListItem> DiscountCodeList { get; set; }
        [NotMapped]
        public List<SelectListItem> BrandList { get; set; }
        [NotMapped]
        public List<SelectListItem> GSTList { get; set; }
        [NotMapped]
        public List<SelectListItem> SKUList { get; set; }
        [NotMapped]
        public List<SelectListItem> LocationList { get; set; }
        [NotMapped]
        public List<SaleInvoice> SaleInvoiceList { get; set; }
        #endregion

        public Product(PurchaseInvoiceItem item)
        {
            if (item.SellInPartial == true)
            {
                Quantity = item.Quantity;
            }
            else
            {
                Quantity = 1;
            }
            JobSheetNumber = item.JobSheetNumber;
            JobSheetId = item.JobsheetId;
            IsOpeningStock = item.IsOpeningStock;
            SellInPartial = item.SellInPartial;
            DiscountsAccessible = item.DiscountsAccessible;
            WSPPercent = item.WSPPercent;
            WSPAmount = item.WSPAmount;
            SKUMaster = item.SKUMaster;
            SubCategoryId = item.SubCategoryId;
            BrandId = item.BrandId;
            SKUMasterId = item.SKUMasterId;
            GSTId = item.GSTId;
            UnitId = item.UnitId;
            HSNId = item.HSNId;
            Rate = item.Rate;
            CGST = item.CGST;
            SGST = item.SGST;
            IGST = item.IGST;
            ApplicableTax = item.CGST.Value + item.SGST.Value + item.IGST.Value;
            Adjustment = item.Adjustment;
            AdditionalPurchaseExpenses = item.AdditionalPurchaseExpenses;
            NetAmount = item.NetAmount;
            LocationId = item.LocationId;
            MRP = item.MRP;
            SalesmanCommision = item.SalesCommission;
            Remark = item.Remarks;
            PurchaseInvoiceItemId = item.PurchaseInvoiceItemId;
            SKUMaster = item.SKUMaster;
            CompanyId = SessionManager.GetSessionCompany().CompanyId;
            BranchId = SessionManager.GetSessionBranch().BranchId;
            DivisionId = SessionManager.GetSessionDivision().DivisionId;
        }

        public Product()
        {
        }
    }
}
