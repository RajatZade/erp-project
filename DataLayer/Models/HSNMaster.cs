﻿using System.ComponentModel.DataAnnotations;

namespace DataLayer.Models
{
    public class HSN:BaseModel
    {
        public int HSNId { get; set; }

        [Required(ErrorMessage = "Required")]
        public string ProductName { get; set; }

        [Required(ErrorMessage = "Required")]
        public string Code { get; set; }

        public int CompanyId { get; set; }
    }
}
