﻿using System;

namespace DataLayer
{
    public class BaseDbUtil
    {
        public void SaveChanges(DbConnector context)
        {
            context.SaveChanges();
        }

        public void RegisterException(DbConnector context, Exception ex)
        {
            context.RegisterException(ex);
        }
    }
}
