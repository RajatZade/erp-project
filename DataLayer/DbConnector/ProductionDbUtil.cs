﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;
using DataLayer.Utils;

namespace DataLayer
{
    public class ProductionDbUtil : BaseDbUtil
    {
        DbConnector db = new DbConnector();

        internal bool DeleteProduction(List<JobSheetItem> productionItems,JobSheet jobsheet, List<MeasurmentSet> MeasurmentSetList, List<JobSheetService> ServiceList,List<Product> productList, List<SaleInvoiceItem> SaleInvoiceItemList)
        {
            foreach (var item in productList)
            {
                db.Entry(item).State = EntityState.Modified;
            }

            List<JobSheetItem> JobSheetItemList = db.JobSheetItems.Where(x => x.JobSheetId == jobsheet.JobSheetId).ToList();
            foreach (var itemToBeDeleted in JobSheetItemList)
            {
                db.JobSheetItems.Remove(itemToBeDeleted);
            }

            foreach (var itemToBeDeleted in SaleInvoiceItemList)
            {
                db.SaleInvoiceItems.Remove(itemToBeDeleted);
            }

            foreach (var item in ServiceList)
            {
                item.GST = db.GSTs.Where(x => x.GSTId == item.GSTId).FirstOrDefault();
                decimal TotalGST = (item.GST.CGST ?? 0) + (item.GST.IGST ?? 0) + item.GST.SGST;
                item.TotalIncludeGST = (item.Price.Value + ((item.Price.Value * TotalGST) / 100)) * item.Quantity;
                Voucher Voucher = db.Vouchers.Where(x => x.JobSheetServiceId == item.JobSheetServiceId).FirstOrDefault();
                if (Voucher != null)
                {
                    //Ledger Ledger = db.Ledgers.Where(x => x.LedgerId == item.LedgerId).FirstOrDefault();
                    //Ledger.CurrentBalance = Ledger.CurrentBalance - Voucher.Total;

                    //db.SaveLedger(Ledger);
                    db.RemoveVoucher(Voucher);
                    item.GST = null;
                    item.Ledger = null;
                }
                RemoveJobService(item);
            }

            foreach (var item in MeasurmentSetList)
            {
                db.MeasurmentSets.Attach(item);
                db.MeasurmentSets.Remove(item);
            }
            if(jobsheet.IsProductCreated)
            {
                PurchaseInvoiceItem PurchaseInvoiceItem = db.PurchaseInvoiceItems.Where(x => x.JobsheetId == jobsheet.JobSheetId).FirstOrDefault();
                if(PurchaseInvoiceItem != null)
                {
                    List<Product> ProductList = db.Products.Where(x => x.PurchaseInvoiceItemId == PurchaseInvoiceItem.PurchaseInvoiceItemId).ToList();
                    foreach (var itemToBeDeleted in ProductList)
                        db.Products.Remove(itemToBeDeleted);

                    List<AttributeLinking> Attributes = db.AttributeLinkings.Where(x => x.PurchaseInvoiceItemId == PurchaseInvoiceItem.PurchaseInvoiceItemId).ToList();
                    foreach (var itemToBeDeleted in Attributes)
                    {
                        db.AttributeLinkings.Remove(itemToBeDeleted);
                    }
                }
            }

            try
            {
                SaveChanges(db);
                JobSheet JobSheet = db.JobSheets.Where(x => x.JobSheetId == jobsheet.JobSheetId).FirstOrDefault();
                db.Entry(JobSheet).State = EntityState.Deleted;
                SaveChanges(db);
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
                return false;
            }
            return true;
        }

        internal void SaveMeasurment(JobSheet jobsheet)
        {
            try
            {
                if (jobsheet.MeasurmentType == Enums.MeasurementType.Custom.ToString())
                {
                    foreach (var MeasurmentItem in jobsheet.MeasurmentSetListForCustome)
                    {
                        foreach (var item in MeasurmentItem.MeasurementList)
                        {
                            if (MeasurmentItem.MeasurmentValues == null)
                            {
                                MeasurmentItem.MeasurmentValues = (item.MeasurementType + "=" + item.MeasurementValue);
                            }
                            else
                            {
                                MeasurmentItem.MeasurmentValues = MeasurmentItem.MeasurmentValues + "," + (item.MeasurementType + "=" + item.MeasurementValue);
                            }

                        }
                        MeasurmentItem.Type = Enums.MeasurementType.Custom;
                        MeasurmentItem.JobSheetId = jobsheet.JobSheetId;
                        db.SaveMeasurmentSet(MeasurmentItem);
                    }
                    List<MeasurmentSet> list = db.MeasurmentSets.Where(x => x.JobSheetId == jobsheet.JobSheetId && x.Type == Enums.MeasurementType.Readymade).ToList();
                    foreach(var item in list)
                    {
                        RemoveMeasurment(item);
                    }
                }
                else
                {
                    jobsheet.MeasurmentSetListForReadymade = SessionManager.GetMeasurmentSetList();
                    foreach (var MeasurmentItem in jobsheet.MeasurmentSetListForReadymade)
                    {
                        MeasurmentItem.MeasurmentSetId = 0;
                        MeasurmentItem.Type = Enums.MeasurementType.Readymade;
                        MeasurmentItem.JobSheetId = jobsheet.JobSheetId;
                        db.SaveMeasurmentSet(MeasurmentItem);
                    }
                    List<MeasurmentSet> list = db.MeasurmentSets.Where(x => x.JobSheetId == jobsheet.JobSheetId && x.Type == Enums.MeasurementType.Custom).ToList();
                    foreach (var item in list)
                    {
                        RemoveMeasurment(item);
                    }
                }

            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
        }

        public void SaveJobSheet(JobSheet model)
        {
            var context = new DbConnector();
            try
            {
                if (model.JobSheetId > 0)
                {
                    db.Entry(model).State = EntityState.Modified;
                    SaveChanges(db);
                }
                else
                {
                    model.ProductNumber = new GenerateAutoNo().GenerateJobSheetNumber(model.CompanyId);
                    db.JobSheets.Add(model);
                    SaveChanges(db);
                }
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
            }
        }

        public void SaveJobSheetCost(JobSheet model)
        {            
            var context = new DbConnector();
            context.Configuration.AutoDetectChangesEnabled = false;
            try
            {
                context.Entry(model).State = EntityState.Modified;
                SaveChanges(context);
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
            }
        }

        public void SaveFutherProcessJobSheet(JobSheet model)
        {
            try
            {
                if (model.JobSheetId > 0)
                {
                    db.Entry(model).State = EntityState.Modified;
                    SaveChanges(db);
                }
                else
                {
                    model.ProductNumber = new GenerateAutoNo().GenerateFurtherJobSheetNumber(model.ParentJobSheetId.Value, model.CompanyId);
                    db.JobSheets.Add(model);
                    SaveChanges(db);
                }
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
            }
        }
        public void SaveJobSheetItem(int JobSheetId)
        {
            try
            {
                List<JobSheetItem> JobSheetItemList = SessionManager.GetJobSheetItemList();
                if (JobSheetItemList != null)
                {
                    foreach (var items in JobSheetItemList)
                    {
                        JobSheetItem Val = db.JobSheetItems.Where(x => x.JobSheetItemId == items.JobSheetItemId).FirstOrDefault();
                        if (Val != null)
                        {

                            Val = null;
                            Val = items;
                            Val.Product = null;
                            Val.GSTList = null;
                            db.Entry(Val).State = EntityState.Modified;
                            SaveChanges(db);
                        }
                        else
                        {
                            var Id = JobSheetId;
                            items.Product = null;
                            items.GSTList = null;
                            items.JobSheetId = Id;
                            db.JobSheetItems.Add(items);
                            SaveChanges(db);
                        }

                    }
                }
                List<JobSheetItem> DeleteJobSheetItemList = db.JobSheetItems.Where(x => x.JobSheetId == JobSheetId && x.QuantityNotAvailable).ToList();
                foreach(var item in DeleteJobSheetItemList)
                {
                    db.JobSheetItems.Remove(item);
                    SaveChanges(db);
                }
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
            }
            SessionManager.EmptySessionList(Constants.JOBSHEET_ITEM_LIST);
        }

        public void SaveSaleInvoiceItemForAlteration(int JobSheetId)
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                List<SaleInvoiceItem> SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_SALE_INVOICE);
                if (SaleInvoiceItemList != null)
                {
                    foreach (var items in SaleInvoiceItemList.ToList())
                    {
                        items.JobSheetId = JobSheetId;
                        items.Product = null;
                        items.GST = null;
                        items.DiscountCodeList = null;
                        items.GSTList = null;
                        items.Unit = null;
                        items.SKUMaster = null;
                        db.SaleInvoiceItems.Add(items);
                        SaveChanges(db);
                    }
                }
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
            }
            SessionManager.EmptySessionList(Constants.JOBSHEET_ITEM_LIST);
        }

        public void SaveJobService(JobSheetService model)
        {
            try
            {
                if (model.JobSheetServiceId == 0)
                {
                    db.JobSheetServices.Add(model);
                }
                else
                {
                    db.Entry(model).State = EntityState.Modified;
                }
                SaveChanges(db);
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
            }
        }

        public void RemoveJobService(JobSheetService model)
        {
            try
            {
                db.JobSheetServices.Attach(model);
                db.JobSheetServices.Remove(model);
                SaveChanges(db);
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
            }
        }

        public void DeleteMeasurment(MeasurmentSet model)
        {
            try
            {
                db.MeasurmentSets.Attach(model);
                db.MeasurmentSets.Remove(model);
                SaveChanges(db);
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
            }
        }

        public void RemoveMeasurment(MeasurmentSet model)
        {
            try
            {
                db.MeasurmentSets.Remove(model);
                SaveChanges(db);
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
            }
        }

        public void SaveJobSheetService(int jobSheetId)
        {
            try
            {
                List<JobSheetService> ServiceList = SessionManager.GetServiceList();
                if (ServiceList != null)
                {
                    Dictionary<int, GST> idVsGST = db.GetGSTMasterDictionary(ServiceList.Select(x => x.GSTId.Value).ToList());
                    foreach (var items in ServiceList.ToList())
                    {
                        var Id = jobSheetId;
                        items.GST = null;
                        items.ServiceType = null;
                        items.Ledger = null;
                        items.JobSheetId = Id;
                        items.JobSheetServiceId = 0;
                        db.JobSheetServices.Add(items);

                        items.GST = idVsGST[items.GSTId.Value];

                        string JobSheetNo = db.JobSheets.Where(x => x.JobSheetId == jobSheetId).Select(x => x.ProductNumber).FirstOrDefault();
                        decimal TotalGST = (items.GST.CGST ?? 0) + (items.GST.IGST ?? 0) + items.GST.SGST;
                        items.TotalIncludeGST = (items.Price.Value + ((items.Price.Value * TotalGST) / 100)) * items.Quantity;
                        Voucher Voucher = new Voucher();
                        Voucher.JobSheetServiceId = items.JobSheetServiceId;
                        Voucher.LedgerId = items.LedgerId;
                        Voucher.Total = items.TotalIncludeGST.Value;
                        Voucher.Type = Constants.CR;
                        Voucher.EntryDate = DateTime.Now;
                        Voucher.VoucherFor = Constants.JOBSHEET_SERVICE;
                        Voucher.InvoiceNumber = JobSheetNo;
                        Voucher.Particulars = "JobSheet Number :" + JobSheetNo;
                        db.SaveVauchers(Voucher);
                    }
                }
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
            }
            SessionManager.EmptySessionList(Constants.JOBSHEET_SERVICE);
        }

        public void UpdateProducts(int JobSheetId)
        {
            List<JobSheetItem> JobSheetItemList = db.JobSheetItems.Where(x => x.JobSheetId == JobSheetId).ToList();
            foreach (var item in JobSheetItemList)
            {
                Product product = db.Products.Where(x => x.ProductId == item.ProductId).FirstOrDefault();
                if (product.SellInPartial == true)
                {
                    product.Quantity = product.Quantity - item.ConversionValue.Value;
                    if (product.Quantity < 0)
                    {
                        product.IsSold = true;
                    }
                    db.SaveProduct(product);
                }
                else
                {
                    product.IsSold = true;
                    db.SaveProduct(product);
                }
            }
        }
    }
}
