﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataLayer.Models;
using DataLayer.Utils;

namespace DataLayer
{
    public class PurchaseInvoiceDbUtil
    {
        DbConnector db = new DbConnector();

        internal Product GetUnsoldProductByPurchaseInvoiceItemId(List<int> purchaseInvoiceItemIds)
        {
            Product product = null;
            try
            {
                product = db.Products.Where(x => purchaseInvoiceItemIds.Contains(x.PurchaseInvoiceItemId) && x.IsSold).FirstOrDefault();
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
            return product;
        }

        internal List<int> GetProductIdsByInvoiceItems(List<int> purchaseInvoiceItemIds)
        {
            List<int> productList = null;
            try
            {
                productList = db.Products.Where(x => purchaseInvoiceItemIds.Contains(x.PurchaseInvoiceItemId)).Select(x => x.ProductId).ToList();
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
            return productList;
        }

        internal List<int> GetProductAttributes(List<int> productIds, List<int> purchaseInvoiceItemIds)
        {
            List<int> attributesList = null;
            try
            {
                attributesList = db.AttributeLinkings.Where(x => purchaseInvoiceItemIds.Contains(x.PurchaseInvoiceItemId ?? 0)).Select(x => x.AttributeLinkingId).ToList();
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
            return attributesList;
        }

        //internal List<Voucher> GetVoucherListForPurchaseInvoiceId(int purchaseInvoiceId)
        //{
        //    List<Voucher> VoucherList = null;
        //    try
        //    {
        //        VoucherList = db.Vouchers.Where(x => x.PurchaseInvoiceId == purchaseInvoiceId).ToList();
        //    }
        //    catch (Exception ex)
        //    {
        //        db.RegisterException(ex);
        //    }
        //    return VoucherList;
        //}

        internal bool DeletePurchaseInvoice(List<AttributeLinking> attributeLinkings, List<Product> products, List<PurchaseInvoiceItem> purchaseInvoiceItems, PurchaseInvoice purchaseInvoice)
        {

            foreach (var item in attributeLinkings)
            {
                db.AttributeLinkings.Attach(item);
                db.AttributeLinkings.Remove(item);
            }
            foreach (var item in products)
            {
                db.Products.Attach(item);
                db.Products.Remove(item);
            }
            foreach (var item in purchaseInvoiceItems)
            {
                db.PurchaseInvoiceItems.Attach(item);
                db.PurchaseInvoiceItems.Remove(item);
            }

            List<Voucher> VoucherList = VoucherList = db.Vouchers.Where(x => x.PurchaseInvoiceId == purchaseInvoice.PurchaseInvoiceId).ToList();
            foreach (var item in VoucherList)
            {
                db.Vouchers.Remove(item);
            }



            try
            {
                db.SaveChanges();
                PurchaseInvoice invoice = db.PurchaseInvoices.Where(x => x.PurchaseInvoiceId == purchaseInvoice.PurchaseInvoiceId).FirstOrDefault();
                db.Entry(invoice).State = EntityState.Deleted;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
                return false;
            }
            return true;
        }

        public bool RemoveInvoiceItem(PurchaseInvoiceItem item)
        {
            bool result = true;
            try
            {
                Product product = GetUnsoldProductByPurchaseInvoiceItemId(new List<int>() { item.PurchaseInvoiceItemId });
                if (product != null)
                {
                    return false;
                }
                db.RemoveProductUnsold(item.PurchaseInvoiceItemId);
                item.AttributeLinkingList = db.AttributeLinkings.Where(x => x.PurchaseInvoiceItemId == item.PurchaseInvoiceItemId).ToList();
                foreach (var item1 in item.AttributeLinkingList.ToList())
                {
                    db.AttributeLinkings.Remove(item1);
                }
                PurchaseInvoiceItem iteminvoice = db.GetPurchaseInvoiceItemById(item.PurchaseInvoiceItemId);
                db.PurchaseInvoiceItems.Remove(iteminvoice);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                db.RegisterException(ex);
            }
            return result;
        }

        public void SavePurchaseInvoice(PurchaseInvoice purchaseInvoice)
        {
            try
            {
                //purchaseInvoice.CompanyId = SessionManager.GetSessionCompany().CompanyId;
                if (purchaseInvoice.PurchaseInvoiceId > 0)
                {
                    db.Entry(purchaseInvoice).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {                    
                    db.PurchaseInvoices.Add(purchaseInvoice);
                    db.SaveChanges();
                    if (purchaseInvoice.PurchaseOrderId > 0)
                    {
                        AddPurchaseOrderItem(purchaseInvoice);
                    }
                }
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
        }

        public void AddPurchaseOrderItem(PurchaseInvoice purchaseInvoice)
        {
            try
            {
                List<PurchaseOrderItem> PurchaseOrderItemList = db.PurchaseOrderItems.Where(x => x.PurchaseOrderId == purchaseInvoice.PurchaseOrderId).ToList();
                foreach (var OrderItem in PurchaseOrderItemList)
                {
                    PurchaseInvoiceItem InvoiceItem = new PurchaseInvoiceItem
                    {
                        PurchaseInvoiceId = purchaseInvoice.PurchaseInvoiceId,
                        SKUMasterId = OrderItem.SKUMasterId,
                        BrandId = OrderItem.BrandId,
                        UnitId = OrderItem.UnitId,
                        HSNId = OrderItem.HSNId,
                        Quantity = OrderItem.Quantity,
                        Rate = OrderItem.Rate,
                        GSTId = OrderItem.GSTId,
                        IGST = OrderItem.IGST,
                        SGST = OrderItem.SGST,
                        CGST = OrderItem.CGST,
                        NetAmount = OrderItem.NetAmount,
                        WSPPercent = OrderItem.WSPPercent,
                        WSPAmount = OrderItem.WSPAmount,
                        LocationId = OrderItem.LocationId,
                        MRP = OrderItem.MRP,
                        //DiscountId = OrderItem.DiscountId,
                        SellInPartial = OrderItem.SellInPartial,
                        ApplicableTax = OrderItem.ApplicableTax,
                        SubTotal = OrderItem.SubTotal,
                        //Discount = OrderItem.Discount,
                        //DiscountPercent = OrderItem.DiscountPercent,
                        //DiscountedAmount = OrderItem.DiscountedAmount,
                        SalesCommission = OrderItem.SalesCommission,
                        IsProductCreated = false,
                        CompanyId = db.CompanyId ?? 0,
                    };
                    db.PurchaseInvoiceItems.Add(InvoiceItem);
                    db.SaveChanges();

                    List<AttributeLinking> AttributeList = db.AttributeLinkings.Where(x => x.PurchaseOrderItemId == OrderItem.PurchaseOrderItemId).ToList();
                    foreach (var AttributeItem in AttributeList)
                    {
                        AttributeLinking Attribute = new AttributeLinking
                        {
                            PurchaseInvoiceItemId = InvoiceItem.PurchaseInvoiceItemId,
                            Quantity = AttributeItem.Quantity,
                            Value = AttributeItem.Value,
                            AttributeSetIds = AttributeItem.AttributeSetIds
                        };
                        db.SaveSingleAttributeLinking(Attribute);
                    }
                }
                PurchaseOrder PurchaseOrder = db.PurchaseOrders.Where(x => x.PurchaseOrderId == purchaseInvoice.PurchaseOrderId).FirstOrDefault();
                PurchaseOrder.OrderStatus = Enums.PurchaseOrderStatus.Closed;
                db.SavePurchaseOrder(PurchaseOrder);
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
        }

        internal bool DeleteOpeningstock(List<AttributeLinking> attributeLinkings, List<Product> products, List<PurchaseInvoiceItem> purchaseInvoiceItems, PurchaseInvoice purchaseInvoice)
        {

            foreach (var item in attributeLinkings)
            {
                db.AttributeLinkings.Attach(item);
                db.AttributeLinkings.Remove(item);
            }
            foreach (var item in products)
            {
                db.Products.Attach(item);
                db.Products.Remove(item);
            }
            foreach (var item in purchaseInvoiceItems)
            {
                db.PurchaseInvoiceItems.Attach(item);
                db.PurchaseInvoiceItems.Remove(item);
            }

            try
            {
                db.SaveChanges();
                PurchaseInvoice invoice = db.PurchaseInvoices.Where(x => x.PurchaseInvoiceId == purchaseInvoice.PurchaseInvoiceId).FirstOrDefault();
                db.Entry(invoice).State = EntityState.Deleted;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
                return false;
            }
            return true;
        }
    }
}
