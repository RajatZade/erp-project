﻿using BarCode.Models;
using DataLayer.Models;
using DataLayer.Utils;
using DataLayer.ViewModels;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.IO;
using System.Linq;


namespace DataLayer
{
    public class DbConnector : DbContext
    {
        public int? CompanyId;
        public DbConnector() : base("DefaultConnection")
        {
            if (SessionManager.GetSessionCompany() != null)
            {
                CompanyId = SessionManager.GetSessionCompany().CompanyId;
            }
            //Configuration.ProxyCreationEnabled = false;
        }

        #region property  
        
        public DbSet<ProductVoucher> ProductVouchers { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<SubCategory> SubCategories { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<LoginUser> LoginUsers { get; set; }
        public DbSet<Unit> UnitMasters { get; set; }
        public DbSet<HSN> HSNMasters { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<StoreMaste> Stores { get; set; }
        public DbSet<GST> GSTs { get; set; }
        public DbSet<Shipping> Shippings { get; set; }
        public DbSet<Designation> Designations { get; set; }
        public DbSet<PurchaseOrder> PurchaseOrders { get; set; }
        public DbSet<SKUMaster> SKUMasters { get; set; }
        public DbSet<PurchaseInvoice> PurchaseInvoices { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<PurchaseInvoiceItem> PurchaseInvoiceItems { get; set; }
        public DbSet<PurchaseOrderItem> PurchaseOrderItems { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Discount> Discounts { get; set; }
        public DbSet<PaymentType> PaymentTypes { get; set; }
        public DbSet<SalesOrder> SalesOrders { get; set; }
        public DbSet<SmsQueue> smsQueues { get; set; }
        public DbSet<SalesOrderItem> SalesOrderItems { get; set; }
        public DbSet<Division> Divisions { get; set; }
        public DbSet<Branch> Branchs { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<PermissionSetLinking> PermissionLinkings { get; set; }
        public DbSet<Company> Companys { get; set; }
        public DbSet<SaleInvoice> SaleInvoices { get; set; }
        public DbSet<SaleInvoiceItem> SaleInvoiceItems { get; set; }
        public DbSet<Ledger> Ledgers { get; set; }
        public DbSet<AccountCategory> ContactCategorys { get; set; }
        public DbSet<Lead> Leads { get; set; }
        public DbSet<AccountSubCategory> ContactSubCategorys { get; set; }
        public DbSet<AttributeSet> AttributeSets { get; set; }
        public DbSet<AttributeLinking> AttributeLinkings { get; set; }
        public DbSet<ExceptionLogger> ExceptionLoggers { get; set; }
        public DbSet<JobSheet> JobSheets { get; set; }
        public DbSet<JobSheetItem> JobSheetItems { get; set; }
        public DbSet<Voucher> Vouchers { get; set; }
        public DbSet<Measurment> Measurments { get; set; }
        public DbSet<MeasurmentSet> MeasurmentSets { get; set; }
        public DbSet<VoucherType> VoucherTypes { get; set; }
        public DbSet<SMSConfig> SmsConfigs { get; set; }
        public DbSet<Budgeting> Budgetings { get; set; }
        public DbSet<EmailConfig> EmailConfigs { get; set; }
        public DbSet<ServiceType> ServiceTypes { get; set; }
        public DbSet<JobSheetService> JobSheetServices { get; set; }
        public DbSet<BudgetSetup> BudgetSetups { get; set; }
        public DbSet<DashboardItem> DashboardItems { get; set; }
        public DbSet<LeadItem> LeadItems { get; set; }
        public DbSet<WalletVoucher> WalletVouchers { get; set; }
        public DbSet<WalletSetting> WalletSettings { get; set; }
        public DbSet<JobSheetInProcessStatus> JobSheetInProcessStatuses { get; set; }
        public DbSet<InternalTransfer> InternalTransfers { get; set; }
        

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            //modelBuilder.Properties<decimal>().Configure(c => c.HasPrecision(18, 4));
            //modelBuilder.Properties<decimal?>().Configure(c => c.HasPrecision(18, 4));

            modelBuilder.Entity<Voucher>().Property(x => x.IGST).HasPrecision(12, 4);
            modelBuilder.Entity<Voucher>().Property(x => x.CGST).HasPrecision(12, 4);
            modelBuilder.Entity<Voucher>().Property(x => x.SGST).HasPrecision(12, 4);
            modelBuilder.Entity<Voucher>().Property(x => x.Total).HasPrecision(12, 4);

            modelBuilder.Entity<PurchaseOrder>().Property(x => x.Adjustment).HasPrecision(12, 4);
            modelBuilder.Entity<PurchaseOrder>().Property(x => x.TotalInvoiceValue).HasPrecision(12, 4);

            modelBuilder.Entity<PurchaseOrderItem>().Property(x => x.Quantity).HasPrecision(12, 4);
            modelBuilder.Entity<PurchaseOrderItem>().Property(x => x.Rate).HasPrecision(12, 4);
            modelBuilder.Entity<PurchaseOrderItem>().Property(x => x.ApplicableTax).HasPrecision(12, 4);
            modelBuilder.Entity<PurchaseOrderItem>().Property(x => x.SubTotal).HasPrecision(12, 4);
            modelBuilder.Entity<PurchaseOrderItem>().Property(x => x.Adjustment).HasPrecision(12, 4);
            modelBuilder.Entity<PurchaseOrderItem>().Property(x => x.AdditionalPurchaseExpenses).HasPrecision(12, 4);
            modelBuilder.Entity<PurchaseOrderItem>().Property(x => x.NetAmount).HasPrecision(12, 4);
            modelBuilder.Entity<PurchaseOrderItem>().Property(x => x.WSPPercent).HasPrecision(12, 4);
            modelBuilder.Entity<PurchaseOrderItem>().Property(x => x.WSPAmount).HasPrecision(12, 4);
            modelBuilder.Entity<PurchaseOrderItem>().Property(x => x.MRP).HasPrecision(12, 4);
            //modelBuilder.Entity<PurchaseOrderItem>().Property(x => x.DiscountPercent).HasPrecision(12, 4);
            //modelBuilder.Entity<PurchaseOrderItem>().Property(x => x.DiscountedAmount).HasPrecision(12, 4);
            //modelBuilder.Entity<PurchaseOrderItem>().Property(x => x.Discount).HasPrecision(12, 4);
            modelBuilder.Entity<PurchaseOrderItem>().Property(x => x.SalesCommission).HasPrecision(12, 4);
            modelBuilder.Entity<PurchaseOrderItem>().Property(x => x.CGST).HasPrecision(12, 4);
            modelBuilder.Entity<PurchaseOrderItem>().Property(x => x.SGST).HasPrecision(12, 4);
            modelBuilder.Entity<PurchaseOrderItem>().Property(x => x.IGST).HasPrecision(12, 4);

            modelBuilder.Entity<PurchaseInvoice>().Property(x => x.TotalInvoiceValue).HasPrecision(12, 4);
            modelBuilder.Entity<PurchaseInvoice>().Property(x => x.Adjustment).HasPrecision(12, 4);

            //modelBuilder.Entity<PurchaseInvoiceItem>().Property(x => x.DiscountPercent).HasPrecision(12, 4);
            //modelBuilder.Entity<PurchaseInvoiceItem>().Property(x => x.DiscountedAmount).HasPrecision(12, 4);
            //modelBuilder.Entity<PurchaseInvoiceItem>().Property(x => x.Discount).HasPrecision(12, 4);
            modelBuilder.Entity<PurchaseInvoiceItem>().Property(x => x.SalesCommission).HasPrecision(12, 4);
            modelBuilder.Entity<PurchaseInvoiceItem>().Property(x => x.CGST).HasPrecision(12, 4);
            modelBuilder.Entity<PurchaseInvoiceItem>().Property(x => x.SGST).HasPrecision(12, 4);
            modelBuilder.Entity<PurchaseInvoiceItem>().Property(x => x.IGST).HasPrecision(12, 4);
            modelBuilder.Entity<PurchaseInvoiceItem>().Property(x => x.Rate).HasPrecision(12, 4);
            modelBuilder.Entity<PurchaseInvoiceItem>().Property(x => x.ApplicableTax).HasPrecision(12, 4);
            modelBuilder.Entity<PurchaseInvoiceItem>().Property(x => x.SubTotal).HasPrecision(12, 4);
            modelBuilder.Entity<PurchaseInvoiceItem>().Property(x => x.Adjustment).HasPrecision(12, 4);
            modelBuilder.Entity<PurchaseInvoiceItem>().Property(x => x.AdditionalPurchaseExpenses).HasPrecision(12, 4);
            modelBuilder.Entity<PurchaseInvoiceItem>().Property(x => x.NetAmount).HasPrecision(12, 4);
            modelBuilder.Entity<PurchaseInvoiceItem>().Property(x => x.WSPPercent).HasPrecision(12, 4);
            modelBuilder.Entity<PurchaseInvoiceItem>().Property(x => x.WSPAmount).HasPrecision(12, 4);
            modelBuilder.Entity<PurchaseInvoiceItem>().Property(x => x.MRP).HasPrecision(12, 4);
            
            //modelBuilder.Entity<Product>().Property(x => x.DiscountPercent).HasPrecision(12, 4);
            //modelBuilder.Entity<Product>().Property(x => x.DiscountedAmount).HasPrecision(12, 4);
            //modelBuilder.Entity<Product>().Property(x => x.Discount).HasPrecision(12, 4);
            modelBuilder.Entity<Product>().Property(x => x.SalesmanCommision).HasPrecision(12, 4);
            modelBuilder.Entity<Product>().Property(x => x.CGST).HasPrecision(12, 4);
            modelBuilder.Entity<Product>().Property(x => x.SGST).HasPrecision(12, 4);
            modelBuilder.Entity<Product>().Property(x => x.IGST).HasPrecision(12, 4);
            modelBuilder.Entity<Product>().Property(x => x.Rate).HasPrecision(12, 4);
            modelBuilder.Entity<Product>().Property(x => x.ApplicableTax).HasPrecision(12, 4);
            modelBuilder.Entity<Product>().Property(x => x.SubTotal).HasPrecision(12, 4);
            modelBuilder.Entity<Product>().Property(x => x.Adjustment).HasPrecision(12, 4);
            modelBuilder.Entity<Product>().Property(x => x.AdditionalPurchaseExpenses).HasPrecision(12, 4);
            modelBuilder.Entity<Product>().Property(x => x.NetAmount).HasPrecision(12, 4);
            modelBuilder.Entity<Product>().Property(x => x.WSPPercent).HasPrecision(12, 4);
            modelBuilder.Entity<Product>().Property(x => x.WSPAmount).HasPrecision(12, 4);
            modelBuilder.Entity<Product>().Property(x => x.MRP).HasPrecision(12, 4);

            modelBuilder.Entity<JobSheetItem>().Property(x => x.ApplicableTaxValue).HasPrecision(12, 4);
            modelBuilder.Entity<JobSheetItem>().Property(x => x.ConversionValue).HasPrecision(12, 4);
            modelBuilder.Entity<JobSheetItem>().Property(x => x.BasicPrice).HasPrecision(12, 4);
            modelBuilder.Entity<JobSheetItem>().Property(x => x.CGST).HasPrecision(12, 4);
            modelBuilder.Entity<JobSheetItem>().Property(x => x.SGST).HasPrecision(12, 4);
            modelBuilder.Entity<JobSheetItem>().Property(x => x.IGST).HasPrecision(12, 4);
            modelBuilder.Entity<JobSheetItem>().Property(x => x.ApplicableTax).HasPrecision(12, 4);
            modelBuilder.Entity<JobSheetItem>().Property(x => x.SubTotal).HasPrecision(12, 4);
            modelBuilder.Entity<JobSheetItem>().Property(x => x.MRP).HasPrecision(12, 4);

            modelBuilder.Entity<Ledger>().Property(x => x.StartingBalance).HasPrecision(12, 4);
            //modelBuilder.Entity<Ledger>().Property(x => x.CurrentBalance).HasPrecision(12, 4);

            modelBuilder.Entity<SaleInvoice>().Property(x => x.TotalInvoiceValue).HasPrecision(12, 4);
            modelBuilder.Entity<SaleInvoice>().Property(x => x.TotalDiscount).HasPrecision(12, 4);
            modelBuilder.Entity<SaleInvoice>().Property(x => x.TotalTax).HasPrecision(12, 4);
            modelBuilder.Entity<SaleInvoice>().Property(x => x.Subtotal).HasPrecision(12, 4);
            modelBuilder.Entity<SaleInvoice>().Property(x => x.SalesManCommision).HasPrecision(12, 4);
            modelBuilder.Entity<SaleInvoice>().Property(x => x.Adjustment).HasPrecision(12, 4);

            modelBuilder.Entity<SaleInvoiceItem>().Property(x => x.Adjustment).HasPrecision(12, 4);
            modelBuilder.Entity<SaleInvoiceItem>().Property(x => x.AdditionalPurchaseExpenses).HasPrecision(12, 4);
            modelBuilder.Entity<SaleInvoiceItem>().Property(x => x.ApplicableTax).HasPrecision(12, 4);
            modelBuilder.Entity<SaleInvoiceItem>().Property(x => x.PurchaseTaxPerCent).HasPrecision(12, 4);
            modelBuilder.Entity<SaleInvoiceItem>().Property(x => x.ApplicableTaxValue).HasPrecision(12, 4);
            modelBuilder.Entity<SaleInvoiceItem>().Property(x => x.Discount).HasPrecision(12, 4);
            modelBuilder.Entity<SaleInvoiceItem>().Property(x => x.MRP).HasPrecision(12, 4);
            modelBuilder.Entity<SaleInvoiceItem>().Property(x => x.SubTotal).HasPrecision(12, 4);
            modelBuilder.Entity<SaleInvoiceItem>().Property(x => x.ConversionValue).HasPrecision(12, 4);
            modelBuilder.Entity<SaleInvoiceItem>().Property(x => x.CGST).HasPrecision(12, 4);
            modelBuilder.Entity<SaleInvoiceItem>().Property(x => x.IGST).HasPrecision(12, 4);
            modelBuilder.Entity<SaleInvoiceItem>().Property(x => x.SGST).HasPrecision(12, 4);
            modelBuilder.Entity<SaleInvoiceItem>().Property(x => x.BasicPrice).HasPrecision(12, 4);
            modelBuilder.Entity<SaleInvoiceItem>().Property(x => x.SalesmanCommision).HasPrecision(12, 4);            
        }

        internal void SavePurchaseInvoice(PurchaseInvoice purchaseInvoice)
        {
            try
            {
                purchaseInvoice.CompanyId = SessionManager.GetSessionCompany().CompanyId;
                if (purchaseInvoice.PurchaseInvoiceId > 0)
                {
                    Entry(purchaseInvoice).State = EntityState.Modified;
                    SaveChanges();
                }
                else
                {
                    PurchaseInvoices.Add(purchaseInvoice);
                    SaveChanges();
                }
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public void RemoveProductVoucher(ProductVoucher model)
        {
            try
            {
                ProductVouchers.Remove(model);
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        internal void SavePurchaseVoucherItem(int PurchaseInvoiceId)
        {
            try
            {
                List<ProductVoucher> ItemList = SessionManager.GetProductVoucherList(Constants.PURCHASE_TYPE_PURCHASE_VOUCHER);
                if(ItemList != null)
                {
                    foreach (var item in ItemList)
                    {
                        item.Unit = null;
                        item.GST = null;
                        item.PurchaseInvoiceId = PurchaseInvoiceId;
                        ProductVouchers.Add(item);
                        SaveChanges();
                    }
                }
                SessionManager.EmptySessionList(Constants.PURCHASE_TYPE_PURCHASE_VOUCHER);
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }
        #endregion

        #region AutoGenerated Number
        internal int GetLastAttributeNumber()
        {
            int id = AttributeLinkings.Max(x => (int?)x.AttributeLinkingId) ?? 0;
            id = id + 1;
            return id;
        }

        internal string GetLastCategoryNo()
        {
            string id = null;
            try
            {
                id = Categories.Where(x => x.CompanyId == (CompanyId ?? 0)).Max(x => x.CategoryNumber);
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return id;
        }

        internal int GetLastServiceIdNumber()
        {
            int? id = JobSheetServices.Max(x => (int?)x.JobSheetServiceId) ?? 0;
            id = id + 1;
            return id.Value;
        }

        internal int GetLastProductVoucherIdNumber()
        {
            int? id = ProductVouchers.Max(x => (int?)x.ProductVoucherId) ?? 0;
            id = id + 1;
            return id.Value;
        }

        internal int GetLastSaleOrderItemNumber()
        {
            int? id = SalesOrderItems.Max(x => (int?)x.SalesOrderItemId) ?? 0;
            id = id + 1;
            return id.Value;
        }

        internal int GetLastMeasurmentSetIdNumber()
        {
            int? id = MeasurmentSets.Max(x => (int?)x.MeasurmentSetId) ?? 0;
            id = id + 1;
            return id.Value;
        }

        internal int GetLastLeadItemId()
        {
            int? id = LeadItems.Max(x => (int?)x.LeadItemId) ?? 0;
            id = id + 1;
            return id.Value;
        }

        internal int GetLastPurchaseOrderNo()
        {
            int id = PurchaseOrders.Max(x => (int?)x.PurchaseOrderId) ?? 0;
            return id;
        }
        #endregion

        public List<CompanyAccessModel> GetAccessibleCompanies()
        {
            List<CompanyAccessModel> model = new List<CompanyAccessModel>();
            var list = Companys.ToList();
            foreach (var item in list)
            {
                model.Add(new CompanyAccessModel() { CompanyId = item.CompanyId, Name = item.Name });
            }
            return model;
        }

        public List<DashboardItemsAccessibleModel> GetDashboardItems()
        {
            List<DashboardItemsAccessibleModel> model = new List<DashboardItemsAccessibleModel>();
            var list = DashboardItems.ToList();
            foreach (var item in list)
            {
                model.Add(new DashboardItemsAccessibleModel() { DashboardItemId = item.DashboardItemId, ItemName = item.ItemName });
            }
            return model;
        }

        public List<BranchAccessModel> GetAccessibleBranches()
        {
            List<BranchAccessModel> model = new List<BranchAccessModel>();
            var list = Branchs.ToList();
            foreach (var item in list)
            {
                model.Add(new BranchAccessModel() { BranchId = item.BranchId, Name = item.BranchName });
            }
            return model;
        }

        public List<DivisionAccessModel> GetAccessibleDivisions()
        {
            List<DivisionAccessModel> model = new List<DivisionAccessModel>();
            var list = Divisions.ToList();
            foreach (var item in list)
            {
                model.Add(new DivisionAccessModel() { DivisionId = item.DivisionId, Name = item.DivisionName });
            }
            return model;
        }

        internal List<Ledger> GetBankAccounts()
        {
            List<Ledger> LedgerList = null;
            try
            {
                LedgerList = Ledgers.Where(x => x.IsBank).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return LedgerList;
        }

        public void SaveJobSheetStatus(JobSheetInProcessStatus model)
        {
            try
            {
                if (model.JobSheetInProcessStatusId > 0)
                {
                    Entry(model).State = EntityState.Modified;
                }
                else
                {
                    JobSheetInProcessStatuses.Add(model);
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public void SaveSaleOrderItem(SalesOrderItem Item)
        {
            try
            {
                if (Item.SalesOrderItemId > 0)
                {
                    Entry(Item).State = EntityState.Modified;
                }
                else
                {
                    SalesOrderItems.Add(Item);
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public Role GetRole(int id)
        {
            return Roles.Include("PermissionSetLinkings").Where(x => x.RoleId == id).FirstOrDefault();
        }

        public List<Permission> GetAllPermissions()
        {
            return Permissions.OrderBy(x => x.ModuleName).ToList();
        }

        public void SaveLeadItem(LeadItem model)
        {
            try
            {
                if (model.LeadItemId == 0)
                {
                    LeadItems.Add(model);
                }
                else
                {
                    Entry(model).State = EntityState.Modified;
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public void SaveServiceType(ServiceType model)
        {
            try
            {
                if (model.ServiceTypeId == 0)
                {
                    ServiceTypes.Add(model);
                }
                else
                {
                    Entry(model).State = EntityState.Modified;
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public IPagedList<SaleInvoice> GetReturnSaleInvoice(int? PageNo,string Type)
        {
            IPagedList<SaleInvoice> SaleInvoiceList = null;
            try
            {
                SaleInvoiceList = SaleInvoices.Where(x => x.CompanyId == CompanyId && x.Type == Type).OrderByDescending(x => x.SaleInvoiceId).ToList().ToPagedList(PageNo ?? 1, Constants.PageSize);
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return SaleInvoiceList;
        }

        public List<DiscountAccessModel> GetDiscountAccessModel()
        {
            List<DiscountAccessModel> model = new List<DiscountAccessModel>();
            var list = Discounts.Where(x => x.IsActive).ToList();
            foreach (var item in list)
            {
                model.Add(new DiscountAccessModel() { DiscountId = item.DiscountId, Code = item.DiscountCode, DiscountPercent = item.DiscountPercent });
            }
            return model;
        }

        public object GetRoles()
        {
            return Roles.ToList();
        }

        public List<AttributeSet> GetAttributeSets()
        {
            List<AttributeSet> AttributeSetList = null;
            try
            {
                AttributeSetList = AttributeSets.Where(x => x.IsActive).ToList();

            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return AttributeSetList;
        }

        public void RemoveLeadItem(LeadItem model)
        {
            try
            {
                LeadItems.Remove(model);
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public void RemoveSalesOrderItem(SalesOrderItem model)
        {
            try
            {
                SalesOrderItems.Remove(model);
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public List<Ledger> GetLedgerListofBank()
        {
            List<Ledger> BankList = null;
            try
            {
                BankList = Ledgers.Where(x => x.AccountCategoryId == 1).ToList();

            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return BankList;
        }

        public SaleInvoice SaveSaleInvoiceReturn(SaleInvoice saleinvoice)
        {
            try
            {
                if (saleinvoice.SaleInvoiceId > 0)
                {
                    Entry(saleinvoice).State = EntityState.Modified;
                }
                else
                {
                    saleinvoice.SaleInvoiceItemList = null;
                    TimeZoneInfo timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                    DateTime dateTime = TimeZoneInfo.ConvertTime(DateTime.UtcNow, timeZoneInfo);
                    saleinvoice.Time = dateTime.ToString("hh:mm tt");
                    GenerateAutoNo No = new GenerateAutoNo();
                    saleinvoice.SaleInvoiceNo = No.GenerateInvoiceNoForType(saleinvoice);
                    SaleInvoices.Add(saleinvoice);
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return saleinvoice;
        }

        public void DeleteBudget(Budgeting model)
        {
            try
            {
                Budgetings.Remove(model);
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public void SaveBudgetSetup(BudgetSetup model)
        {
            try
            {
                if (model.BudgetSetupId > 0)
                {
                    Entry(model).State = EntityState.Modified;
                }
                else
                {
                    BudgetSetups.Add(model);
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public void UpdateLoginUser(LoginUser loginUser)
        {
            try
            {
                Entry(loginUser).State = EntityState.Modified;
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public List<Location> GetLocationListByBranch(int BranchId)
        {
            List<Location> LocationList = null;
            try
            {
                LocationList = Locations.Where(x => x.BranchId == BranchId).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return LocationList;
        }

        internal List<Branch> GetBrandListByDivision(int DivisionId)
        {
            List<Branch> BranchList = null;
            try
            {
                BranchList = Branchs.Where(x => x.DivisionId == DivisionId).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return BranchList;
        }

        public void SaveBudget(Budgeting model)
        {
            try
            {
                if (model.BudgetingId > 0)
                {
                    Entry(model).State = EntityState.Modified;
                }
                else
                {
                    Budgetings.Add(model);
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public Ledger GetLedgerById(int LedgerId)
        {
            Ledger ledger = null;
            try
            {
                ledger = Ledgers.Include(x => x.Contact).Where(x => x.LedgerId == LedgerId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return ledger;
        }

        public PaymentType GetPaymentTypeById(int? PaymentTypeId)
        {
            PaymentType PaymentType = null;
            try
            {
                PaymentType = PaymentTypes.Where(x => x.PaymentTypeId == PaymentTypeId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return PaymentType;
        }
        
        public void SaveRole(Role role)
        {
            try
            {
                if (role.RoleId == 0)
                {
                    Roles.Add(role);
                }
                else
                {
                    if (role.PermissionSetLinkings != null && role.PermissionSetLinkings.Count > 0)
                    {
                        foreach (var item in role.PermissionSetLinkings)
                        {
                            item.RoleId = role.RoleId;
                            if (item.PermissionSetLinkingId == 0)
                            {
                                PermissionLinkings.Add(item);
                            }
                            else
                            {
                                Entry(item).State = EntityState.Modified;
                            }
                        }
                    }
                    Entry(role).State = EntityState.Modified;
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public void DeleteRole(int Id)
        {
            try
            {
                List<PermissionSetLinking> LinkingList = PermissionLinkings.Where(x => x.RoleId == Id).ToList();
                foreach (var item in LinkingList)
                {
                    PermissionLinkings.Remove(item);
                }
                Role roles = Roles.Where(x => x.RoleId == Id).FirstOrDefault();
                if(roles != null)
                {
                    Roles.Remove(roles);
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        #region Categories
        public List<Category> GetCategoryLists()
        {
            List<Category> categoryLists = null;
            try
            {
                categoryLists = Categories.Where(x => x.CompanyId == (CompanyId ?? 0)).OrderBy(x => x.Name).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return categoryLists;
        }

        public Category GetCategoryById(int CategoryId)
        {
            return Categories.Where(x => x.CategoryId == CategoryId && x.CompanyId == (CompanyId ?? 0)).FirstOrDefault();
        }

        public void SaveCategory(Category category)
        {
            try
            {
                category.CompanyId = CompanyId ?? 0;
                if (category.CategoryId == 0)
                {
                    GenerateAutoNo No = new GenerateAutoNo();
                    category.CategoryNumber = No.GenerateCategoryNo();
                    Categories.Add(category);
                }
                else
                {
                    Entry(category).State = EntityState.Modified;
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }

        }

        public bool DeleteCategory(Category category)
        {
            try
            {
                Categories.Remove(category);
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
                return false;
            }
            return true;
        }

        internal Dictionary<int?, AttributeLinking> GetAttributesByInvoiceItemId(List<int> list)
        {
            Dictionary<int?, AttributeLinking> Dictionary = new Dictionary<int?, AttributeLinking>();
            try
            {
                Dictionary = AttributeLinkings.Where(x => list.Contains(x.PurchaseInvoiceItemId ?? 0)).ToDictionary(x => x.PurchaseInvoiceItemId, x => x);
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return Dictionary;
        }

        internal List<AttributeLinking> GetAttributeLinkingByID(int purchaseItemID)
        {
            List<AttributeLinking> AttributeLinkingList = new List<AttributeLinking>();
            try
            {
                AttributeLinkingList = AttributeLinkings.Where(x => x.PurchaseInvoiceItemId == purchaseItemID).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return AttributeLinkingList;
        }

        //public List<CategoryAccessModel> GetAccessibleCategoris()
        //{
        //    List<CategoryAccessModel> model = new List<CompanyAccessModel>();
        //    var list = Categories.ToList();
        //    foreach (var item in list)
        //    {
        //        model.Add(new CompanyAccessModel() { CompanyId = item.CompanyId, Name = item.Name });
        //    }
        //    return model;
        //}

        public List<PurchaseInvoiceItem> GetPurchaseInvoiceItems(int id)
        {
            List<PurchaseInvoiceItem> purchaseInvoiceItemList = null;
            try
            {
                purchaseInvoiceItemList = PurchaseInvoiceItems.Where(x => x.PurchaseInvoiceId == id).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return purchaseInvoiceItemList;
        }

        public List<PurchaseInvoiceItem> GetPurchaseInvoiceItemsForProduct(int id)
        {
            List<PurchaseInvoiceItem> purchaseInvoiceItemList = null;
            try
            {
                purchaseInvoiceItemList = PurchaseInvoiceItems.Where(x => x.PurchaseInvoiceItemId == id).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return purchaseInvoiceItemList;
        }


        //public List<PurchaseInvoiceItem> GetPurchaseInvoiceItemsForProduct(int id)
        //{
        //    return PurchaseInvoiceItems.Where(x => x.PurchaseInvoiceItemId == id).ToList();
        //}

        public List<PurchaseInvoiceItem> GetPurchaseInvoiceItemsList()
        {
            List<PurchaseInvoiceItem> purchaseInvoiceItemLists = null;
            try
            {
                purchaseInvoiceItemLists = PurchaseInvoiceItems.Include(x => x.Unit).Include(x => x.AttributeLinkingList).Include("SKUMaster").OrderByDescending(x => x.PurchaseInvoiceItemId).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return purchaseInvoiceItemLists;
        }

        public List<PurchaseInvoiceItem> GetOpeningStockItemsList()
        {
            List<PurchaseInvoiceItem> purchaseInvoiceItemLists = null;
            try
            {
                purchaseInvoiceItemLists = PurchaseInvoiceItems.Include(x => x.Unit).Include(x => x.AttributeLinkingList).Include("SKUMaster").OrderByDescending(x => x.PurchaseInvoiceItemId).Where(x => x.IsOpeningStock == true && x.CompanyId == CompanyId).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return purchaseInvoiceItemLists;
        }

        public List<Product> GetProductByInvoiceId(int id)
        {
            return Products.Where(x => x.PurchaseInvoiceItemId == id).ToList();
        }
        #endregion

        #region SubCategories
        public List<SubCategory> GetSubCategoryList(int? categoryId)
        {
            List<SubCategory> subCategory = null;
            try
            {
                subCategory = SubCategories.Where(x => x.CompanyId == (CompanyId ?? 0)).OrderBy(x => x.Name).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return subCategory;
        }

        public List<SubCategory> GetSubCategoryListByCategoryId(int? categoryId)
        {
            List<SubCategory> subCategory = null;
            try
            {
                subCategory = SubCategories.Where(x => x.CategoryId == categoryId).OrderBy(x => x.Name).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return subCategory;
        }

        public SubCategory GetSubCategoryById(int value)
        {
            return SubCategories.Where(x => x.SubCategoryId == value && x.CompanyId == (CompanyId ?? 0)).FirstOrDefault();
        }


        public PurchaseInvoiceItem GetPurchaseInvoiceItemById(int id)
        {
            return PurchaseInvoiceItems.Where(x => x.PurchaseInvoiceItemId == id).FirstOrDefault();
        }

        internal int GetLastSubCategoryNo()
        {
            int id = 0;
            try
            {
                id = SubCategories.Where(x => x.CompanyId == (CompanyId ?? 0)).Max(x => x.SubCategoryId);
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return id;
        }

        public void SaveSubCategory(SubCategory subcategory)
        {
            try
            {
                subcategory.CompanyId = CompanyId ?? 0;
                subcategory.Category = GetCategoryById(subcategory.CategoryId);
                if (subcategory.SubCategoryId == 0)
                {
                    GenerateAutoNo No = new GenerateAutoNo();
                    subcategory.SubCategoriesNumber = No.GenerateSubCategoryNo();
                    SubCategories.Add(subcategory);
                }
                else
                {
                    Entry(subcategory).State = EntityState.Modified;
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public bool RemoveSubCategory(SubCategory subcategory)
        {
            try
            {
                SubCategories.Remove(subcategory);
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
                return false;
            }
            return true;
        }
        #endregion

        #region InsertProduct
        public bool InsertProducts(List<Product> productList,int CompanyId)
        {
            bool result = false;
            barcodecs objbar = new barcodecs();

            try
            {
                List<Product> productOutput = new List<Product>();
                foreach (var item in productList)
                {
                    item.SubCategoryId = null;
                    item.SerialNumber = GenerateAutoNo.GenerateProductNo(CompanyId);
                    Products.Add(item);
                    SaveChanges();
                }
                result = true;
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return result;
        }

        public List<AttributeLinking> GetAttributeLinkingList(int InvoiceItemId)
        {
            List<AttributeLinking> AttributeLinkingList = null;
            try
            {
                AttributeLinkingList = AttributeLinkings.Where(x => x.PurchaseInvoiceItemId == InvoiceItemId).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return AttributeLinkingList;
        }

        public void MarkInvoiceItemsAsProductGenerated(List<int> list)
        {
            try
            {
                var toBeUpdated = PurchaseInvoiceItems.Where(x => list.Contains(x.PurchaseInvoiceItemId));
                foreach (var item in toBeUpdated)
                {
                    item.IsProductCreated = true;
                    Entry(item).State = EntityState.Modified;
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public AttributeLinking GetAttributeLinkingListByLinkingId(int linkigId)
        {
            var context = new DbConnector();
            Configuration.ProxyCreationEnabled = false;
            AttributeLinking AttributeLinkingList = null;
            try
            {
                AttributeLinkingList = context.AttributeLinkings.Where(x => x.AttributeLinkingId == linkigId).Single();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return AttributeLinkingList;
        }
        #endregion

        public void RemoveAttribute(int linkingId)
        {
            AttributeLinking attributeLinking = new AttributeLinking();
            try
            {
                attributeLinking = AttributeLinkings.Find(linkingId);
                if (attributeLinking != null)
                {
                    AttributeLinkings.Remove(attributeLinking);
                    SaveChanges();
                }
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public void RemoveVoucher(Voucher Voucher)
        {
            try
            {
                Vouchers.Remove(Voucher);
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public void DeleteVoucher(string VoucherFor, string VoucherNumber)
        {
            try
            {
                List<Voucher> VoucherList = Vouchers.Where(x => x.VoucherFor == VoucherFor && x.VoucherNumber == VoucherNumber && x.CompanyId == CompanyId).ToList();
                foreach(var item in VoucherList)
                {
                    Vouchers.Remove(item);
                }                
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }
      
        #region Users





        private AttributeLinking GetAttributeLinkingById(int? attributeLinkingId)
        {
            return AttributeLinkings.Where(x => x.AttributeLinkingId == attributeLinkingId).FirstOrDefault();
        }

        
        #endregion

        public List<Measurment> GetMeasurmentList()
        {
            List<Measurment> MeasurmentList = null;
            try
            {
                MeasurmentList = Measurments.OrderByDescending(x => x.MeasurmentId).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return MeasurmentList;
        }

        #region UnitMaster
        public List<Unit> GetUnitMasterList()
        {
            List<Unit> UnitMasterList = null;
            try
            {
                UnitMasterList = UnitMasters.OrderByDescending(x => x.UnitId).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return UnitMasterList;
        }

        public List<Unit> GetUnitTypeList(int id)
        {
            List<Unit> UnitTypeList = null;
            UnitTypeList = UnitMasters.Where(x => x.UnitTypeId == id).ToList();
            return UnitTypeList;
        }

        public Unit GetUnitMasterById(int value)
        {
            return UnitMasters.Where(x => x.UnitId == value).FirstOrDefault();
        }

        public Unit GetUnitMasterByName(string Name)
        {
            if (UnitMasters.Where(x => x.UnitValue == Name).ToList().FirstOrDefault() == null)
            {
                return null;
            }
            else
            {
                return UnitMasters.Where(x => x.UnitValue == Name).FirstOrDefault();
            }
        }

        public Unit GetUnitParents(int unitId)
        {
            Unit Unit = UnitMasters.Where(x => x.UnitId == unitId).ToList().FirstOrDefault();
            return UnitMasters.Where(x => x.UnitId == Unit.UnitTypeId).ToList().FirstOrDefault();
        }

        public void SaveUnitMaster(Unit unitMaster)
        {
            try
            {
                if (unitMaster.UnitId == 0)
                {
                    UnitMasters.Add(unitMaster);
                }
                else
                {
                    Entry(unitMaster).State = EntityState.Modified;
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public bool RemoveUnit(Unit unit)
        {
            bool result = true;
            try
            {
                UnitMasters.Remove(unit);
                SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                RegisterException(ex);
            }
            return result;
        }
        #endregion

        #region HSNMaster
        public List<HSN> GetHSNMasterList()
        {
            List<HSN> HSNMasterList = null;
            HSNMasterList = HSNMasters.Where(x => x.CompanyId == (CompanyId ?? 0)).OrderByDescending(x => x.HSNId).ToList();
            return HSNMasterList;
        }

        public HSN GetHSNMasterById(int value)
        {
            return HSNMasters.Where(x => x.HSNId == value).FirstOrDefault();
        }

        public void SaveHSNMaster(HSN hsnMaster)
        {
            try
            {
                hsnMaster.CompanyId = (CompanyId ?? 0);
                if (hsnMaster.HSNId == 0)
                {
                    HSNMasters.Add(hsnMaster);
                }
                else
                {
                    Entry(hsnMaster).State = EntityState.Modified;
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public bool RemoveHSN(HSN hsn)
        {
            bool result = true;
            try
            {
                HSNMasters.Remove(hsn);
                SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                RegisterException(ex);
            }
            return result;

        }
        #endregion

        #region Product
        public List<Product> GetProductsList()
        {
            List<Product> product = null;
            product = Products.OrderByDescending(x => x.ProductId).ToList();
            return product;
        }

        public List<Product> GetProductsListByInvoiceId(int? id)
        {
            List<Product> product = new List<Product>();
            try
            {
                product = Products.Where(x => x.PurchaseInvoiceItemId == id).ToList();

            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return product;
        }

        public List<Product> GetProductsListByInvoiceIdIsSold(int? id)
        {
            List<Product> list = null;
            try
            {
                list = Products.Where(x => x.PurchaseInvoiceItemId == id && x.IsSold == false).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return list;
        }

        public List<Product> GetProductsListByInvoiceIdIsSoldTrue(int? id)
        {
            List<Product> list = null;
            try
            {
                list = Products.Where(x => x.PurchaseInvoiceItemId == id && x.IsSold == true).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return list;
        }

        public Product GetProductById(int? ProductId)
        {
            return Products.Where(x => x.ProductId == ProductId).FirstOrDefault();
        }

        public Product GetProductByNumber(string ProductId)
        {
            Product Product = null;
            {
                try
                {
                    Product = Products.Where(x => x.SerialNumber == ProductId && x.IsSold == false && x.IsBooked == false && x.CompanyId == CompanyId).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    RegisterException(ex);
                }
            }
            return Product;
        }

        public Product GetProductByNumberForExcesses(string ProductId)
        {
            Product Product = null;
            {
                try
                {
                    Product = Products.Where(x => x.SerialNumber == ProductId && x.CompanyId == CompanyId).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    RegisterException(ex);
                }
            }
            return Product;
        }

        public void SaveProduct(Product product)
        {           
            try
            {
                if (product.ProductId == 0)
                {
                    product.SerialNumber = GenerateAutoNo.GenerateProductNo(product.CompanyId);
                    Products.Add(product);
                }
                else
                {
                    product.LastModifiedDate = DateTime.Now;
                    Entry(product).State = EntityState.Modified;
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public void UpdateProduct(Product product)
        {
            var Product1 = Products.Where(x => x.ProductId == product.ProductId).FirstOrDefault();
            try
            {
                if (product == null)
                {
                    barcodecs objbar = new barcodecs();
                    product.SerialNumber = GenerateAutoNo.GenerateProductNo(product.CompanyId);
                    product.Barcode = objbar.GenerateBarcodeString(product.SerialNumber);
                    Products.Add(product);
                }
                else
                {
                    product.Barcode = Product1.Barcode;
                    product.SerialNumber = Product1.SerialNumber;
                    product.LastModifiedDate = DateTime.Now;
                    Entry(Product1).CurrentValues.SetValues(product);
                    //Entry(product).State = EntityState.Modified;
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }


        public bool RemoveProduct(Product product)
        {
            try
            {
                Products.Remove(product);
                PurchaseInvoiceItem purchaseInvoiceitem = GetPurchaseInvoiceItemById(product.PurchaseInvoiceItemId);
                purchaseInvoiceitem.Quantity = purchaseInvoiceitem.Quantity - product.Quantity;
                Entry(purchaseInvoiceitem).State = EntityState.Modified;
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
                return false;
            }
            return true;
        }


        public bool RemoveSingleProduct(Product product)
        {
            try
            {
                Products.Remove(product);
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
                return false;
            }
            return true;
        }
        #endregion

        #region Stores
        public List<StoreMaste> GetStoreListList()
        {
            List<StoreMaste> StoreList = null;
            StoreList = Stores.OrderByDescending(x => x.StoreMasteId).ToList();
            return StoreList;
        }

        public StoreMaste GetStoreById(int value)
        {
            return Stores.Where(x => x.StoreMasteId == value).FirstOrDefault();
        }

        public void SaveStore(StoreMaste store)
        {
            try
            {
                if (store.StoreMasteId == 0)
                {
                    Stores.Add(store);
                }
                else
                {
                    Entry(store).State = EntityState.Modified;
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }

        }

        public bool DeleteMeasurment(int id)
        {
            bool result = true;
            try
            {
                Measurment model = Measurments.Where(x => x.MeasurmentId == id).FirstOrDefault();
                if (model != null)
                {
                    Measurments.Remove(model);
                    SaveChanges();
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;
                RegisterException(ex);
            }
            return result;
        }

        public bool DeleteStore(StoreMaste store)
        {
            bool result = true;
            try
            {
                Stores.Remove(store);
                SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                RegisterException(ex);
            }
            return result;
        }
        #endregion

        #region GST
        public List<GST> GetGSTvalueList()
        {
            List<GST> GSTMasterList = null;
            GSTMasterList = GSTs.OrderByDescending(x => x.GSTId).ToList();
            return GSTMasterList;
        }

        public GST GetGSTMasterById(int value)
        {
            return GSTs.Where(x => x.GSTId == value).FirstOrDefault();
        }

        public void SaveGSTMaster(GST gst)
        {
            try
            {
                if (gst.GSTId == 0)
                {
                    GSTs.Add(gst);
                }
                else
                {
                    Entry(gst).State = EntityState.Modified;
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public bool RemoveGST(GST gst)
        {
            bool result = true;
            try
            {
                GSTs.Remove(gst);
                SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                RegisterException(ex);
            }
            return result;
        }
        #endregion

        #region Shipping
        public List<Shipping> GetShippingList()
        {
            List<Shipping> ShippingList = null;
            ShippingList = Shippings.OrderByDescending(x => x.ShippingId).ToList();
            return ShippingList;
        }

        public Shipping GetShippingById(int value)
        {
            return Shippings.Where(x => x.ShippingId == value).FirstOrDefault();
        }

        public void SaveShippingMaster(Shipping shipping)
        {
            try
            {
                if (shipping.ShippingId == 0)
                {
                    Shippings.Add(shipping);
                }
                else
                {
                    Entry(shipping).State = EntityState.Modified;
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }
        public bool RemoveShipping(Shipping shipping)
        {
            bool result = true;
            try
            {
                Shippings.Remove(shipping);
                SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                RegisterException(ex);
            }
            return result;

        }
        #endregion

        #region  Designation
        public List<Designation> GetDesignationList()
        {
            List<Designation> DesignationList = null;
            DesignationList = Designations.OrderByDescending(x => x.DesignationId).ToList();
            return DesignationList;
        }

        public Designation GetDesignationById(int value)
        {
            return Designations.Where(x => x.DesignationId == value).FirstOrDefault();
        }

        public void SaveDesignation(Designation designation)
        {
            try
            {
                if (designation.DesignationId == 0)
                {
                    Designations.Add(designation);
                }
                else
                {
                    Entry(designation).State = EntityState.Modified;
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public int GetDesignationIdByName(string name)
        {
            Designation designation = new Designation();
            try
            {
                designation.DesignationType = name;
                Designations.Add(designation);
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return designation.DesignationId;
        }

        public bool RemoveDesignation(Designation designation)
        {
            bool result = true;
            try
            {
                Designations.Remove(designation);
                SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                RegisterException(ex);
            }
            return result;

        }
        #endregion

        #region Location
        public List<Location> GetLocationList()
        {
            List<Location> locationList = null;
            locationList = Locations.OrderByDescending(x => x.LocationId).ToList();
            return locationList;
        }

        public Location GetLocatinListById(int value)
        {
            return Locations.Where(x => x.LocationId == value).FirstOrDefault();
        }

        public void SaveLocation(Location Location)
        {
            try
            {
                if (Location.LocationId == 0)
                {
                    Locations.Add(Location);
                }
                else
                {
                    Entry(Location).State = EntityState.Modified;
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public bool RemoveLocation(Location location)
        {
            try
            {
                Locations.Remove(location);
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
                return false;
            }
            return true;
        }
        #endregion

        #region PurchaseOrder
        public List<PurchaseOrder> GetPurchaseOrderlist()
        {
            List<PurchaseOrder> PurchaseOrderList = null;
            PurchaseOrderList = PurchaseOrders.OrderByDescending(x => x.PurchaseOrderId).ToList();
            return PurchaseOrderList;
        }

        public PurchaseOrder GetPurchaseOrderById(int id)
        {
            return PurchaseOrders.Where(x => x.PurchaseOrderId == id).FirstOrDefault();
        }

        public bool RemovePurchaseOrder(PurchaseOrder order)
        {
            bool result = true;
            try
            {
                PurchaseOrders.Remove(order);
                SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                RegisterException(ex);
            }
            return result;
        }

        public SalesOrder GetSalesOrderById(int id)
        {
            return SalesOrders.Where(x => x.SalesOrderId == id).FirstOrDefault();
        }

        public IPagedList<PurchaseOrder> GetPurchaseOrder(int? PageNo)
        {
            IPagedList<PurchaseOrder> PurchaseOrderList = null;
            try
            {
                PurchaseOrderList = PurchaseOrders.Where(x => x.CompanyId == CompanyId).OrderByDescending(x => x.PurchaseOrderId).ToList().ToPagedList(PageNo ?? 1, Constants.PageSize);
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return PurchaseOrderList;
        }

        public void SavePurchaseOrder(PurchaseOrder purchaseOrder)
        {
            try
            {
                if (purchaseOrder.PurchaseOrderId > 0)
                {
                    Entry(purchaseOrder).State = EntityState.Modified;
                }
                else
                {
                    purchaseOrder.CompanyId = CompanyId.Value;
                    PurchaseOrders.Add(purchaseOrder);
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }
        public void SavePurchaseOrderItem(PurchaseOrderItem OrderItem)
        {
            try
            {
                if (OrderItem.DiscountIds != null)
                {
                    List<int> templist = OrderItem.DiscountIds.OfType<int>().ToList();
                    OrderItem.DiscountsAccessible = string.Join(",", templist);
                }
                else
                {
                    OrderItem.DiscountsAccessible = "";
                }
                OrderItem.SellInPartial = UnitMasters.Where(x => x.UnitId == OrderItem.UnitId).Select(x => x.SellInPartial).FirstOrDefault();
                if (OrderItem.PurchaseOrderItemId > 0)
                {
                    Entry(OrderItem).State = EntityState.Modified;
                }
                else
                {
                    PurchaseOrderItems.Add(OrderItem);
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public PurchaseOrderItem GetPurchaseOrderItemById(int id)
        {
            return PurchaseOrderItems.Where(x => x.PurchaseOrderItemId == id).FirstOrDefault();
        }
        public void RemoveOrderItem(PurchaseOrderItem item)
        {
            PurchaseOrderItems.Remove(item);
            SaveChanges();
        }
        #endregion

        #region SKUMaster
        public List<SKUMaster> GetSKUMasterList()
        {
            List<SKUMaster> SKUMasterList = null;
            try
            {
                SKUMasterList = SKUMasters.Where(x => x.CompanyId == (CompanyId ?? 0)).OrderBy(x => x.SKU).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return SKUMasterList;
        }

        public List<SKUMaster> GetSKUListByCatIdSubId(SKUMaster skumaster)
        {
            List<SKUMaster> SKUMasterList = null;
            SKUMasterList = SKUMasters.Where(x => x.CategoryId == skumaster.CategoryId && x.SubCategoryId == skumaster.SubCategoryId && x.CompanyId == (CompanyId ?? 0)).OrderBy(x => x.SKU).ToList();
            return SKUMasterList;
        }

        public Dictionary<int, SalesOrder> GetSaleOrderDictionary(List<int> list)
        {
            return SalesOrders.Where(x => list.Contains(x.SalesOrderId) && x.CompanyId == (CompanyId ?? 0)).ToDictionary(x => x.SalesOrderId, x => x);
        }

        public Dictionary<int, SaleInvoice> GetSaleInvoiceDictionary(List<int> list)
        {
            return SaleInvoices.Where(x => list.Contains(x.SaleInvoiceId)).ToDictionary(x => x.SaleInvoiceId, x => x);
        }

        public Dictionary<int, SKUMaster> GetSKUMasterDictionary(List<int> list)
        {
            return SKUMasters.Where(x => list.Contains(x.SKUMasterId)).ToDictionary(x => x.SKUMasterId, x => x);
        }

        public Dictionary<int, GST> GetGSTMasterDictionary(List<int> list)
        {
            return GSTs.Where(x => list.Contains(x.GSTId)).ToDictionary(x => x.GSTId, x => x);
        }

        public Dictionary<int, Unit> GetUnitMasterDictionary(List<int> list)
        {
            return UnitMasters.Where(x => list.Contains(x.UnitId)).ToDictionary(x => x.UnitId, x => x);
        }

        public Dictionary<int, ServiceType> GetServiceTypeMasterDictionary(List<int> list)
        {
            return ServiceTypes.Where(x => list.Contains(x.ServiceTypeId)).ToDictionary(x => x.ServiceTypeId, x => x);
        }

        public Dictionary<int, Ledger> GetLedgerDictionary(List<int> list)
        {
            return Ledgers.Where(x => list.Contains(x.LedgerId)).ToDictionary(x => x.LedgerId, x => x);
        }

        public Dictionary<int, JobSheet> GetJobSheetDictionary(List<int> list)
        {
            return JobSheets.Where(x => list.Contains(x.JobSheetId)).ToDictionary(x => x.JobSheetId, x => x);
        }
        public Dictionary<int, Product> GetProductDictionary(List<int> list)
        {
            return Products.Where(x => list.Contains(x.ProductId)).ToDictionary(x => x.ProductId, x => x);
        }

        public Dictionary<int, string> GetTypeOfSaleInvoice(IEnumerable<int?> list)
        {
            return SaleInvoices.Where(x => list.Contains(x.SaleInvoiceId)).ToDictionary(x => x.SaleInvoiceId, x => x.Type);
        }

        public SKUMaster GetSKUMasterById(int value)
        {
            SKUMaster model = new SKUMaster();
            try
            {
                model = SKUMasters.Where(x => x.SKUMasterId == value && x.CompanyId == (CompanyId ?? 0)).FirstOrDefault();
                model.Category = GetCategoryById(model.CategoryId);
                model.SubCategory = GetSubCategoryById(model.SubCategoryId);

            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return model;
        }

        public void SaveSKUMaster(SKUMaster skumaster)
        {
            try
            {
                skumaster.CompanyId = (CompanyId ?? 0);
                if (skumaster.SKUMasterId == 0)
                {
                    SKUMasters.Add(skumaster);
                }
                else
                {
                    Entry(skumaster).State = EntityState.Modified;
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public bool RemoveSKU(SKUMaster skumaster)
        {
            bool result = true;
            try
            {
                SKUMasters.Remove(skumaster);
                SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                RegisterException(ex);
            }
            return result;

        }
        #endregion

        #region PurchaseInvoice
        public List<PurchaseInvoice> GetPurchaseInvoiceList()
        {
            List<PurchaseInvoice> PurchaseInvoiceList = null;
            PurchaseInvoiceList = PurchaseInvoices.OrderByDescending(x => x.PurchaseInvoiceId).ToList();
            return PurchaseInvoiceList;
        }

        public IPagedList<PurchaseInvoice> GetPurchaseInvoice(int? PageNo,string Type)
        {
            IPagedList<PurchaseInvoice> PurchaseInvoiceList = null;
            try
            {
                PurchaseInvoiceList = PurchaseInvoices.Where(x => x.CompanyId == CompanyId && x.Type == Type).OrderByDescending(x => x.PurchaseInvoiceId).ToList().ToPagedList(PageNo ?? 1, Constants.PageSize);
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return PurchaseInvoiceList;
        }

        public PurchaseInvoice GetPurchaseInvoiceById(int id)
        {
            return PurchaseInvoices.Where(x => x.PurchaseInvoiceId == id).FirstOrDefault();
        }

        public PurchaseInvoice GetPurchaseInvoice(int value)
        {
            return PurchaseInvoices.Where(x => x.PurchaseInvoiceId == value).FirstOrDefault();
        }


        public void RemoveAttributeLinkings(int id)
        {
            AttributeLinking item = AttributeLinkings.Where(x => x.AttributeLinkingId == id).FirstOrDefault();
            AttributeLinkings.Remove(item);
            SaveChanges();
        }

        public bool RemovePurchaseInvoiceItem(PurchaseInvoiceItem item)
        {
            bool result = true;
            try
            {
                item.AttributeLinkingList = AttributeLinkings.Where(x => x.PurchaseInvoiceItemId == item.PurchaseInvoiceItemId).ToList();
                foreach (var item1 in item.AttributeLinkingList.ToList())
                {
                    AttributeLinkings.Remove(item1);
                }
                PurchaseInvoiceItems.Remove(item);
                SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                RegisterException(ex);
            }
            return result;
        }



        public void RemoveProductUnsold(int? id)
        {
            try
            {
                List<Product> ProductList = Products.Where(x => x.PurchaseInvoiceItemId == id).ToList();
                if (ProductList != null && ProductList.Count != 0)
                {
                    foreach (var Product in ProductList)
                    {
                        Products.Remove(Product);
                    }
                    //SaveChanges();
                }
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        #endregion

        #region PurchaseAccount
        public List<Ledger> GetAccountList()
        {
            List<Ledger> purchaseAccountList = null;
            purchaseAccountList = Ledgers.OrderByDescending(x => x.LedgerId).ToList();
            return purchaseAccountList;
        }

        public Ledger GetPurchaseAccountListById(int value)
        {
            return Ledgers.Where(x => x.LedgerId == value).FirstOrDefault();
        }

        public void SavePurchaseAccount(Ledger PurchaseAccount)
        {
            try
            {
                if (PurchaseAccount.LedgerId == 0)
                {
                    Ledgers.Add(PurchaseAccount);
                }
                else
                {
                    Entry(PurchaseAccount).State = EntityState.Modified;
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }
        #endregion

        #region PurchaseInvoice Item
        public void SavePurchaseInvoiceItem(PurchaseInvoiceItem purchaseInvoiceItem)
        {
            try
            {
                if(purchaseInvoiceItem.DiscountIds != null)
                {
                    List<int> templist = purchaseInvoiceItem.DiscountIds.OfType<int>().ToList();
                    purchaseInvoiceItem.DiscountsAccessible = string.Join(",", templist);
                }
                else
                {
                    purchaseInvoiceItem.DiscountsAccessible = "";
                }
                purchaseInvoiceItem.SellInPartial = UnitMasters.Where(x => x.UnitId == purchaseInvoiceItem.UnitId).Select(x => x.SellInPartial).FirstOrDefault();
                purchaseInvoiceItem.CompanyId = CompanyId.Value;
                if (purchaseInvoiceItem.PurchaseInvoiceItemId > 0)
                {
                    Entry(purchaseInvoiceItem).State = EntityState.Modified;
                }
                else
                {
                    PurchaseInvoiceItems.Add(purchaseInvoiceItem);
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }
        #endregion

        #region BrandModule
        public List<Brand> GetBrandList()
        {
            List<Brand> BrandList = null;
            try
            {
                BrandList = Brands.Where(x => x.CompanyId == (CompanyId ?? 0)).OrderBy(x => x.BrandName).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return BrandList;
        }

        public Brand GetBrandListById(int value)
        {
            return Brands.Where(x => x.BrandId == value).FirstOrDefault();
        }

        public void SaveBrand(Brand brand)
        {
            brand.CompanyId = CompanyId ?? 0;
            if (brand.BrandId == 0)
            {
                Brands.Add(brand);
            }
            else
            {
                Entry(brand).State = EntityState.Modified;
            }
            SaveChanges();
        }

        public bool RemoveBrand(Brand brand)
        {
            bool result = true;
            try
            {
                Brands.Remove(brand);
                SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                RegisterException(ex);
            }
            return result;
        }
        #endregion

        #region Discount
        public List<Discount> GetDsicountList()
        {
            List<Discount> DiscountList = null;
            DiscountList = Discounts.OrderByDescending(x => x.DiscountId).ToList();
            return DiscountList;
        }

        public Discount GetDiscountById(int value)
        {
            return Discounts.Where(x => x.DiscountId == value).FirstOrDefault();
        }

        public List<Discount> GetActiveDiscountList()
        {
            List<Discount> DiscountList = null;
            DiscountList = Discounts.Where(x => x.IsActive == true).ToList();
            return DiscountList;
        }

        public List<Discount> GetSKUDiscountList(List<int> DiscountIds)
        {
            List<Discount> DiscountList = null;
            try
            {
                DiscountList = Discounts.Where(x => x.IsActive == true).ToList();
                DiscountList = Discounts.Where(x => DiscountIds.Contains(x.DiscountId)).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return DiscountList;
        }

        public void SaveDiscount(Discount discount)
        {
            try
            {
                if (discount.DiscountId == 0)
                {
                    Discounts.Add(discount);
                }
                else
                {
                    Entry(discount).State = EntityState.Modified;
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public bool DeleteDiscount(Discount discount)
        {
            bool result = true;
            try
            {
                Discounts.Remove(discount);
                SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                RegisterException(ex);
            }
            return result;
        }
        #endregion

        #region SalesOrder
        public List<SalesOrderItem> GetSalesOrderItemForSaleOrder(List<int> list)
        {
            return SalesOrderItems.Where(x => list.Contains(x.SalesOrderId)).ToList();
        }

        public List<SaleInvoiceItem> GetSaleInvoiceItemForSaleOrder(List<int> list)
        {
            return SaleInvoiceItems.Where(x => list.Contains(x.SalesOrderId ?? 0)).ToList();
        }

        public IPagedList<SalesOrder> GetSalesOrderLists(int? PageNo)
        {
            IPagedList<SalesOrder> SalesOrderOrderList = null;
            try
            {
                SalesOrderOrderList = SalesOrders.Where(x => x.CompanyId == CompanyId).OrderByDescending(x => x.SalesOrderId).ToList().ToPagedList(PageNo ?? 1, Constants.PageSize);
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return SalesOrderOrderList;
        }

        public void SaveSalesOrder(SalesOrder salesOrder)
        {
            try
            {
                if(salesOrder.PurchaseOrderId == -1)
                {
                    salesOrder.PurchaseOrderId = null;
                }
                if (salesOrder.SalesOrderId > 0)
                {
                    Entry(salesOrder).State = EntityState.Modified;
                }
                else
                {
                    GenerateAutoNo No = new GenerateAutoNo();
                    salesOrder.SaleOrderNumber = No.GenerateSalesOrderNo(salesOrder.CompanyId);
                    SalesOrders.Add(salesOrder);
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                RegisterException(ex);
            }
        }

        public PurchaseInvoiceItem GetPurchaseInvoiceItemBySKU(string value)
        {
            return PurchaseInvoiceItems.Where(x => x.SKUMaster.SKU == value).FirstOrDefault();
        }

        public List<SalesOrderItem> GetSalesOrderItems(int? SalesOrderId)
        {
            List<SalesOrderItem> salesOrderItems = null;
            salesOrderItems = SalesOrderItems.Where(x => x.SalesOrderId == SalesOrderId).OrderByDescending(x => x.SalesOrderId).ToList();
            return salesOrderItems;
        }


        public bool RemoveSalesOrder(SalesOrder order)
        {
            try
            {
                List<SalesOrderItem> salesOrderItems = order.SalesOrderItems;
                order.SalesOrderItems = null;
                foreach(var item in salesOrderItems)
                {
                    SalesOrderItems.Remove(item);
                }

                List<Voucher> VoucherLists = Vouchers.Where(x => x.SalesOrderId == order.SalesOrderId).ToList() ;
                foreach (var item in VoucherLists)
                {
                    Vouchers.Remove(item);
                }
                SalesOrders.Remove(order);
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
                return false;
            }
            return true;
        }
        #endregion  

        #region SalesOrderItem
        public List<SalesOrderItem> GetSalesOrderItemList()
        {
            List<SalesOrderItem> salesOrderItemList = null;
            salesOrderItemList = SalesOrderItems.OrderByDescending(x => x.SalesOrderItemId).ToList();
            return salesOrderItemList;
        }

        public SalesOrderItem GetSalesOrderItemById(int value)
        {
            return SalesOrderItems.Where(x => x.SalesOrderItemId == value).FirstOrDefault();
        }

        public SaleInvoice GetSalesInvoiceById(int? value)
        {
            SaleInvoice model = SaleInvoices.Include(x => x.SaleInvoiceItemList).Where(x => x.SaleInvoiceId == value).FirstOrDefault();
            return model;
        }

        public SaleInvoice GetSalesInvoiceByNumber(string number)
        {
            return SaleInvoices.Where(x => x.SaleInvoiceNo == number).FirstOrDefault();
        }



        public void SaveSalesOrderItem(SalesOrderItem salesOrderItem)
        {
            try
            {
                if (salesOrderItem.SalesOrderItemId > 0)
                {
                    Entry(salesOrderItem).State = EntityState.Modified;
                }
                else
                {
                    salesOrderItem.SKUMaster = null;
                    SalesOrderItems.Add(salesOrderItem);
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public void RemoveSalesItem(SalesOrderItem item)
        {
            SalesOrderItems.Remove(item);
            SaveChanges();
        }

        public IPagedList<SaleInvoice> GetSaleInvoice(int? PageNo,string Type)
        {
            IPagedList<SaleInvoice> SaleInvoiceList = null;
            try
            {
                SaleInvoiceList = SaleInvoices.Where(x => x.CompanyId == CompanyId && x.Type == Type).OrderByDescending(x => x.SaleInvoiceId).ToList().ToPagedList(PageNo ?? 1, Constants.PageSize);
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return SaleInvoiceList;
        }
        #endregion

        #region Payment Type
        public List<PaymentType> GetPaymentTypeList()
        {
            List<PaymentType> paymentTypeList = null;
            paymentTypeList = PaymentTypes.Where(x => x.CompanyId == null || x.CompanyId == CompanyId).OrderByDescending(x => x.PaymentTypeId).ToList();
            return paymentTypeList;
        }

        public List<ServiceType> GetServiceTypeList()
        {
            List<ServiceType> ServiceTypeList = null;
            ServiceTypeList = ServiceTypes.OrderByDescending(x => x.ServiceTypeId).ToList();
            return ServiceTypeList;
        }

        public PaymentType PaymentTypeById(int value)
        {
            return PaymentTypes.Where(x => x.PaymentTypeId == value).FirstOrDefault();
        }

        public void SavePaymentType(PaymentType PaymentType)
        {
            if (PaymentType.PaymentTypeId == 0)
            {
                PaymentTypes.Add(PaymentType);
            }
            else
            {
                Entry(PaymentType).State = EntityState.Modified;
            }
            SaveChanges();
        }

        public bool RemovePaymentType(PaymentType PaymentType)
        {
            try
            {
                PaymentTypes.Remove(PaymentType);
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
                return false;
            }
            return true;
        }
        #endregion

        public bool RemoveServiceType(ServiceType ServiceType)
        {
            try
            {
                ServiceTypes.Remove(ServiceType);
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
                return false;
            }
            return true;
        }

        #region Division
        public List<Division> GetDivisionList()
        {
            List<Division> DivisionList = null;
            DivisionList = Divisions.OrderByDescending(x => x.DivisionId).ToList();
            return DivisionList;
        }

        public List<Division> GetDivisionListByCompany(int? id)
        {
            List<Division> DivisionList = null;
            DivisionList = Divisions.Where(x => x.CompanyId == id).OrderByDescending(x => x.DivisionId).ToList();
            return DivisionList;
        }

        public Division GetDivisionListById(int value)
        {
            return Divisions.Where(x => x.DivisionId == value).FirstOrDefault();
        }

        public void SaveDivision(Division division)
        {
            if (division.DivisionId == 0)
            {
                Divisions.Add(division);
            }
            else
            {
                Entry(division).State = EntityState.Modified;
            }
            SaveChanges();
        }

        public void UpdateSaleOrderItems(SalesOrderItem salesorderItem)
        {
            try
            {
                Entry(salesorderItem).State = EntityState.Modified;
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public bool RemoveDivision(Division division)
        {
            bool result = true;
            try
            {
                Divisions.Remove(division);
                SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                RegisterException(ex);
            }
            return result;

        }
        #endregion 

        #region Branch
        public List<Branch> GetBranchList()
        {
            List<Branch> BranchList = null;
            BranchList = Branchs.OrderByDescending(x => x.BranchId).ToList();
            return BranchList;
        }

        public List<Branch> GetBranchListByCompany(int? id)
        {
            List<Branch> BranchList = null;
            BranchList = Branchs.Where(x => x.CompanyId == id).OrderByDescending(x => x.BranchId).ToList();
            return BranchList;
        }

        public Branch GetBranchListById(int value)
        {
            return Branchs.Where(x => x.BranchId == value).FirstOrDefault();
        }

        public void SaveBranch(Branch branch)
        {
            if (branch.BranchId == 0)
            {
                Branchs.Add(branch);
            }
            else
            {
                Entry(branch).State = EntityState.Modified;
            }
            SaveChanges();
        }

        public bool RemoveBranch(Branch branch)
        {
            bool result = true;
            try
            {
                Branchs.Remove(branch);
                SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                RegisterException(ex);
            }
            return result;
        }
        #endregion

        #region Company
        public List<Company> GetCompanyList()
        {
            List<Company> companyList = null;
            try
            {
                companyList = Companys.OrderByDescending(x => x.CompanyId).ToList();

            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return companyList;
        }

        public List<Company> GetSessionCompanyList()
        {
            List<Company> companyList = null;
            try
            {
                companyList = Companys.Where(x => x.CompanyId == CompanyId).OrderByDescending(x => x.CompanyId).ToList();

            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return companyList;
        }

        public Company GetCompanyById(int? companyId)
        {
            return Companys.Where(x => x.CompanyId == companyId).FirstOrDefault();
        }

        public void SaveCompany(Company company)
        {
            try
            {
                if (company.CompanyId == 0)
                {
                    //GenerateAutoNo No = new GenerateAutoNo();
                    //customer.CustomerNumber = No.GenerateCustomerNumber();
                    company.CreatedDate = DateTime.Now;
                    Companys.Add(company);
                }
                else
                {

                    company.LastModifiedDate = DateTime.Now;
                    Entry(company).State = EntityState.Modified;
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public bool RemoveCompany(Company company)
        {
            bool result = true;
            try
            {
                Companys.Remove(company);
                SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                RegisterException(ex);
            }
            return result;
        }
        #endregion

        #region RoleList
        public List<Role> GetRoleList()
        {
            List<Role> RoleList = null;
            try
            {
                RoleList = Roles.ToList();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return RoleList;
        }
        #endregion

        #region Sale Invoice
        public void SaveSaleInvoiceEdit(SaleInvoice saleInvoice)
        {
            try
            {
                if (saleInvoice.SaleInvoiceId > 0)
                {
                    Entry(saleInvoice).State = EntityState.Modified;
                    SaveChanges();
                }
                else
                {
                    GenerateAutoNo No = new GenerateAutoNo();
                    saleInvoice.SaleInvoiceNo = No.GenerateSaleInvoiceNo(saleInvoice.InvoicingDate);
                    SaleInvoices.Add(saleInvoice);
                    SaveChanges();
                }
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public void UpdateSaleInvoiceItem(SaleInvoiceItem item)
        {
            try
            {
                SaleInvoiceItem SaleInvoiceItem = SaleInvoiceItems.Where(x => x.SaleInvoiceItemId == item.SaleInvoiceItemId).FirstOrDefault();
                item.GST = null;
                item.Product = null;
                item.SKUMaster = null;
                Entry(SaleInvoiceItem).CurrentValues.SetValues(item);
                //Entry(item).State = EntityState.Modified;
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public void SaveSaleInvoiceItem(SaleInvoiceItem invoiceItem)
        {
            try
            {
                if(invoiceItem.SaleInvoiceItemId > 0)
                {
                    invoiceItem.Product = null;
                    //SaleInvoiceItems.Attach(invoiceItem);
                    Entry(invoiceItem).State = EntityState.Modified;
                    SaveChanges();
                }
                else
                {
                    invoiceItem.Product = null;
                    invoiceItem.GST = null;
                    invoiceItem.SKUMaster = null;
                    SaleInvoiceItems.Add(invoiceItem);
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public void ResetSaleInvoiceItem(Product Product, int? SaleInvoiceId, int ReturnSaleInvoiceId)
        {
            try
            {
                SaleInvoiceItem SaleInvoiceItem = SaleInvoiceItems.Where(x => x.SaleInvoiceId == ReturnSaleInvoiceId && x.ProductId == Product.ProductId && x.Status == Constants.SALE_ITEM_SOLD).FirstOrDefault();
                SaleInvoiceItem SaleInvoiceI = SaleInvoiceItems.Where(x => x.SaleInvoiceItemId == SaleInvoiceItem.SaleInvoiceItemId).FirstOrDefault();
                SaleInvoiceItem.Product = null;
                SaleInvoiceItem.Status = Constants.SALE_ITEM_RETURN;
                SaleInvoiceItem.ReturnSaleInvoiceId = SaleInvoiceId;
                SaleInvoiceItems.Remove(SaleInvoiceItem);
                SaveChanges();
                SaleInvoiceI.SaleInvoiceItemId = 0;
                SaleInvoiceItem.ReturnSaleInvoiceId = SaleInvoiceId;
                SaleInvoiceItem.SaleInvoiceId = ReturnSaleInvoiceId;
                SaleInvoiceItems.Add(SaleInvoiceI);
                //Entry(SaleInvoiceItem).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }
        #endregion

        public List<AttributeLinking> GetInvoiceItemAttribute(int? item)
        {
            Configuration.ProxyCreationEnabled = false;
            List<AttributeLinking> List = new List<AttributeLinking>();
            try
            {
                List = AttributeLinkings.Where(x => x.PurchaseInvoiceItemId == item).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return List;
        }

        #region Ledger
        public List<Ledger> GetLedgerList()
        {
            List<Ledger> ledgerList = null;
            try
            {
                ledgerList = Ledgers.Where(x => x.CompanyId == CompanyId || x.CompanyId == null).OrderBy(x => x.Name).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return ledgerList;
        }

        public bool SaveContact(Contact model)
        {
            try
            {
                //model.CompanyId = CompanyId;
                if (model.ContactId == 0)
                {
                    Contacts.Add(model);
                }
                else
                {
                    Entry(model).State = EntityState.Modified;
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
                return false;
            }
            return true;
        }


        public List<Ledger> LedgerListofCustomer()
        {
            List<Ledger> ledgerList = null;
            try
            {
                ledgerList = Ledgers.OrderByDescending(x => x.LedgerId).Where(x => x.Contact.Type == "Customer").ToList();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return ledgerList;
        }


        public List<Ledger> GetLedgerListByType(string Type)
        {
            List<Ledger> ledgerList = null;
            try
            {
                ledgerList = Ledgers.Where(x => x.Contact.Type == Type && (x.CompanyId == CompanyId || x.CompanyId == null)).OrderBy(x => x.Name).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return ledgerList;
        }

        public List<Ledger> LedgerListOfOther()
        {
            List<Ledger> ledgerList = null;
            try
            {
                ledgerList = Ledgers.Where(x => x.ContactId == null || x.Contact.Type == "Other" && (x.CompanyId == CompanyId || x.CompanyId == null)).OrderBy(x => x.Name).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return ledgerList;
        }

        public List<Ledger> LedgerListOfEmployee()
        {
            List<Ledger> ledgerList = null;
            try
            {
                ledgerList = Ledgers.Where(x => x.Contact.Type == "Employee").OrderByDescending(x => x.LedgerId).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return ledgerList;
        }

        public Ledger GetLedgerListById(int ledgerid)
        {
            Ledger ledger = null;
            ledger = Ledgers.Where(x => x.LedgerId == ledgerid).FirstOrDefault();
            return ledger;
        }

        public bool SaveLedger(Ledger ledger)
        {
            bool result = true;
            try
            {
                Contact contact = null;
                if (ledger.ContactId > 0)
                {
                    contact = Contacts.Where(x => x.ContactId == ledger.ContactId).FirstOrDefault();
                    if(contact != null)
                    {
                        contact = contact.GetContact(contact, ledger.Contact);
                        contact.IsLedgerRequired = true;
                    }
                    else
                    {
                        contact = ledger.Contact;
                        if(contact.Type == null)
                        {
                            contact.Type = "Other";
                        }
                        contact.IsLedgerRequired = true;
                    }
                    if (contact.FirstName == null && contact.CompanyName == null)
                    {
                        contact.FirstName = ledger.Name;
                    }
                }
                else
                {
                    contact = ledger.Contact;
                    if(contact.FirstName == null && contact.CompanyName == null)
                    {
                        contact.FirstName = ledger.Name;
                    }
                    if (contact.Type == null)
                    {
                        contact.Type = "Other";
                    }
                    contact.IsLedgerRequired = true;
                }
                SaveContact(contact);
                ledger.Contact = null;
                ledger.ContactId = contact.ContactId;


                if (ledger.AccountSubCategoryId == -1)
                {
                    ledger.AccountSubCategoryId = null;
                }
                ledger.CompanyId = CompanyId;
                if (ledger.LedgerId == 0)
                {
                    //ledger.CurrentBalance = ledger.StartingBalance;
                    Ledgers.Add(ledger);
                }
                else
                {
                    Entry(ledger).State = EntityState.Modified;
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                RegisterException(ex);
            }
            return result;
        }

        internal void UpdateLedger(Ledger ledger)
        {
            try
            {
                Entry(ledger).State = EntityState.Modified;
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public bool DeleteLedger(Ledger ledger)
        {
            bool result = true;
            try
            {
                if(Vouchers.Where(x => x.LedgerId == ledger.LedgerId).Count() > 1)
                {
                    throw new InvalidDataException("Multiple Vouchers Found");
                }
                else
                {
                    Voucher voucher = Vouchers.Where(x => x.LedgerId == ledger.LedgerId && x.Total == 0).FirstOrDefault();
                    if (voucher != null)
                    {
                        Vouchers.Remove(voucher);
                        SaveChanges();
                    }
                }
                Ledgers.Remove(ledger);
                SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                RegisterException(ex);
            }
            return result;

        }
        #endregion GetInvoiceItemAttribute

        #region ContactCategoryList
        public List<AccountCategory> GetContactCategoryList(int? contactTypeId)
        {
            List<AccountCategory> List = null;
            try
            {
                if (contactTypeId.HasValue && contactTypeId > 0)
                {
                    List = ContactCategorys.Where(x => x.AccountCategoryId == contactTypeId).OrderBy(x => x.Name).ToList();
                }
                else
                {
                    List = ContactCategorys.OrderBy(x => x.Name).ToList();
                }
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return List;
        }


        public AccountCategory GetContactCategoryById(int value)
        {
            return ContactCategorys.Where(x => x.AccountCategoryId == value).FirstOrDefault();
        }


        public bool SaveContactcategory(AccountCategory contactcategory)
        {
            bool result = true;
            try
            {
                if (contactcategory.AccountCategoryId == 0)
                {
                    ContactCategorys.Add(contactcategory);
                }
                else
                {
                    Entry(contactcategory).State = EntityState.Modified;
                }

                SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                RegisterException(ex);
            }
            return result;
        }

        public void SaveAttributeLinking(List<AttributeLinking> modelList)
        {
            try
            {
                foreach (var item in modelList.ToList())
                {
                    AttributeLinkings.Add(item);
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public void SaveSingleAttributeLinking(AttributeLinking model)
        {

            try
            {
                if (model.AttributeLinkingId == 0)
                {
                    AttributeLinkings.Add(model);
                }
                else
                {
                    Entry(model).State = EntityState.Modified;
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }

        }

        public int GetContactCategoryIdByName(string name)
        {
            AccountCategory contact = new AccountCategory();
            try
            {
                contact.Name = name;
                ContactCategorys.Add(contact);
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return contact.AccountCategoryId;
        }

        public bool RemoveContactCategory(AccountCategory contactcategory)
        {
            bool result = true;
            try
            {
                ContactCategorys.Remove(contactcategory);
                SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                RegisterException(ex);
            }
            return result;

        }
        #endregion

        #region Login
        public LoginUser IsValidUser(LoginUser LoginUser)
        {
            LoginUser validUser = null;
            bool checkUsername = true;
            bool checkPassword = true;
            try
            {
                validUser = LoginUsers.Where(x => x.Username == LoginUser.Username && x.Password == LoginUser.Password && x.IsActive == true && x.Contact.Type == Enums.RecordType.Employee.ToString()).FirstOrDefault();
                checkUsername = string.Equals(LoginUser.Username, validUser.Username);
                checkPassword = string.Equals(LoginUser.Password, validUser.Password);
            }

            catch (Exception ex)
            {
                RegisterException(ex);
            }
            if (checkUsername == true && checkPassword == true)
            {

            }
            else
            {
                validUser = null;
            }
            return validUser;
        }
        #endregion

        #region Lead

        public List<Lead> GetLeadList()
        {
            List<Lead> leadList = null;
            try
            {
                leadList = Leads.OrderByDescending(x => x.LeadId).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return leadList;
        }
       
        public bool SaveLead(Lead lead)
        {
            bool result = true;
            try
            {
                if (lead.LeadId == 0)
                {
                    lead.CreatedDate = DateTime.Now;
                    Leads.Add(lead);
                }
                else
                {
                    lead.LastModifiedDate = DateTime.Now;
                    Entry(lead).State = EntityState.Modified;
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                RegisterException(ex);
            }
            return result;
        }

        public bool RemoveLead(Lead lead)
        {
            bool result = true;
            try
            {
                Leads.Remove(lead);
                SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                RegisterException(ex);
            }
            return result;

        }
        #endregion

        #region ContactSubCategory
        public List<AccountSubCategory> GetContactSubCategoryList()
        {
            List<AccountSubCategory> List = null;
            try
            {
                List = ContactSubCategorys.Include(x => x.ContactCategory).OrderByDescending(x => x.AccountSubCategoryId).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return List;
        }

        public List<AccountSubCategory> GetContactSubCategoryList(int contactcategoryId)
        {
            List<AccountSubCategory> List = null;
            try
            {
                List = ContactSubCategorys.Where(x => x.AccountCategoryId == contactcategoryId).OrderBy(x => x.ContactSubCategoryName).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return List;
        }



        public AccountSubCategory GetContactSubCategoryById(int value)
        {
            return ContactSubCategorys.Where(x => x.AccountSubCategoryId == value).FirstOrDefault();
        }

        public bool SaveContactSubCategory(AccountSubCategory contactsubcategory)
        {
            bool result = true;
            try
            {
                if (contactsubcategory.AccountSubCategoryId == 0)
                {
                    contactsubcategory.CreatedDate = DateTime.Now;
                    ContactSubCategorys.Add(contactsubcategory);
                }
                else
                {
                    contactsubcategory.LastModifiedDate = DateTime.Now;
                    Entry(contactsubcategory).State = EntityState.Modified;
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                RegisterException(ex);
            }
            return result;
        }

        public bool RemoveContactSubCategory(AccountSubCategory contactsubcategory)
        {
            bool result = true;
            try
            {
                ContactSubCategorys.Remove(contactsubcategory);
                SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                RegisterException(ex);
            }
            return result;
        }
        #endregion

        #region Exception
        public void RegisterException(Exception exception)
        {
            ExceptionLogger exceptionLog = new ExceptionLogger
            {
                ExceptionMessage = exception.StackTrace,
                Date = DateTime.Now
            };
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(exception, true);
            exceptionLog.ClassName = trace.GetFrame(0).GetMethod().ReflectedType.FullName;
            try
            {
                ExceptionLoggers.Add(exceptionLog);
                SaveChanges();
            }
            catch (Exception ex)
            {
                //RegisterException(ex);
            }

        }
        #endregion

        public void SaveVauchers(Voucher Voucher)
         {
            try
            {
                Voucher.SaleInvoiceId = Voucher.SaleInvoiceId == -1 ? null : Voucher.SaleInvoiceId;
                Voucher.CompanyId = CompanyId ?? 0;
                if (Voucher.VoucherId == 0)
                {
                    if(Voucher.VoucherNumber == null)
                    {
                        Voucher.VoucherNumber = GenerateAutoNo.GenerateVoucherNo();                        
                    }
                    Vouchers.Add(Voucher);
                }
                else
                {
                    Entry(Voucher).State = EntityState.Modified;
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }

        }

        public void DeleteBudgetSetup(BudgetSetup model)
        {
            try
            {
                BudgetSetups.Remove(model);
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public List<SaleInvoiceItem> GetInvoiceItemsForOrder(int SalesOrderId)
        {
            List<SaleInvoiceItem> SaleInvoiceItemList = null;
            try
            {
                SaleInvoiceItemList = SaleInvoiceItems.Where(x => x.SalesOrderId == SalesOrderId).ToList();
                if (SaleInvoiceItemList == null)
                {
                    SaleInvoiceItemList = new List<SaleInvoiceItem>();
                }
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
            return SaleInvoiceItemList;
        }

        #region Production
        internal void SaveProductionItem(JobSheetItem Item)
        {
            try
            {
                Item.Product = null;
                JobSheetItems.Add(Item);
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public void SaveMeasurmentSet(MeasurmentSet model)
        {
            try
            {
                if (model.MeasurmentSetId == 0)
                {
                    MeasurmentSets.Add(model);
                }
                else
                {
                    Entry(model).State = EntityState.Modified;
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(ex);
            }
        }

        public void SaveMeasurment(Measurment Measurment)
        {
            if (Measurment.MeasurmentId == 0)
            {
                Measurments.Add(Measurment);
            }
            else
            {
                Entry(Measurment).State = EntityState.Modified;
            }
            SaveChanges();
        }
        #endregion

        #region BarCode List
        public List<BarcodeViewModel> GetBarcodes(int id)
        {
            barcodecs objbar = new barcodecs();
            List<int> items = PurchaseInvoiceItems.Where(x => x.PurchaseInvoiceId == id).Select(x => x.PurchaseInvoiceItemId).ToList();
            List<BarcodeViewModel> productList = Products.Include("SKUMaster").Where(x => items.Contains(x.PurchaseInvoiceItemId)).Select(x => new BarcodeViewModel() { SerialNumber = x.SerialNumber, SKUMasterId = x.SKUMasterId, PurchaseInvoiceItemId = x.PurchaseInvoiceItemId, MRP = x.MRP, GSTId = x.GSTId, AttributeLinkingId = x.AttributeLinkingId }).ToList();
            Dictionary<int, SKUMaster> idVsSKU = GetSKUMasterDictionary(productList.Select(x => x.SKUMasterId).ToList());
            Company company = SessionManager.GetSessionCompany();
            Dictionary<int, GST> idVsGST = GetGSTMasterDictionary(productList.Select(x => x.GSTId).ToList());
            for (int i = 0; i < productList.Count; i++)
            {
                productList[i].Barcode = objbar.GenerateBarcodeString(productList[i].SerialNumber);
                productList[i].SKUMaster = idVsSKU[productList[i].SKUMasterId];
                productList[i].Company = company;
                productList[i].GST = idVsGST[productList[i].GSTId];

                if (!productList[i].GST.IGST.HasValue) productList[i].GST.IGST = 0;
                if (!productList[i].GST.CGST.HasValue) productList[i].GST.CGST = 0;

                productList[i].GSTPercent = Convert.ToDecimal(productList[i].GST.CGST + productList[i].GST.IGST + productList[i].GST.SGST);
                productList[i].AttributeLinking = GetAttributeLinkingById(productList[i].AttributeLinkingId);
                if (productList[i].AttributeLinking != null)
                {
                    productList[i].AttributeValue = productList[i].AttributeLinking.Value.Split(',').ToList();
                    productList[i].AttributeList = productList[i].AttributeLinking.AttributeSetIds.Split(',').ToList();
                    productList[i].Attribute = new List<string>();
                    int z = 2;
                    for (int j = 0; j < z; j++)
                    {
                        if (productList[i].AttributeValue[j].ToString() != "None")
                        {
                            string AttributeDemo = productList[i].AttributeValue[j].ToString();

                            productList[i].Attribute.Add(AttributeDemo);
                        }
                    }
                    for (int j = 0; j < productList[i].AttributeList.Count; j++)
                    {
                        if (productList[i].AttributeList[j].ToString() == "Type")
                        {
                            if (productList[i].AttributeValue[j].ToString() != "None")
                            {
                                string AttributeDemo = productList[i].AttributeValue[j].ToString();

                                productList[i].Attribute.Add(AttributeDemo);
                            }
                        }
                        else
                        {
                            z++;
                        }
                    }
                }

            }
            return productList;
        }

        public List<BarcodeViewModel> GetBarcodesQueue(List<Product> BarcodeQueue)
        {
            barcodecs objbar = new barcodecs();
            List<BarcodeViewModel> productList = new List<BarcodeViewModel>();
            if(BarcodeQueue != null)
            {
                productList = BarcodeQueue.Select(x => new BarcodeViewModel() { SerialNumber = x.SerialNumber, PurchaseInvoiceItemId = x.PurchaseInvoiceItemId, MRP = x.MRP,  GSTId = x.GSTId, AttributeLinkingId = x.AttributeLinkingId, SKUMasterId = x.SKUMasterId }).ToList();
            }
            Dictionary<int, SKUMaster> idVsSKU = GetSKUMasterDictionary(productList.Select(x => x.SKUMasterId).ToList());
            Company company = SessionManager.GetSessionCompany();
            Dictionary<int, GST> idVsGST = GetGSTMasterDictionary(productList.Select(x => x.GSTId).ToList());

            for (int i = 0; i < productList.Count; i++)
            {
                productList[i].SKUMaster = idVsSKU[productList[i].SKUMasterId];
                productList[i].Company = company;
                productList[i].GST = idVsGST[productList[i].GSTId];
                productList[i].Barcode = objbar.GenerateBarcodeString(productList[i].SerialNumber);

                if (!productList[i].GST.IGST.HasValue) productList[i].GST.IGST = 0;
                if (!productList[i].GST.CGST.HasValue) productList[i].GST.CGST = 0;

                productList[i].GSTPercent = Convert.ToDecimal(productList[i].GST.CGST + productList[i].GST.IGST + productList[i].GST.SGST + productList[i].GST.OtherTaxes);
                productList[i].AttributeLinking = GetAttributeLinkingById(productList[i].AttributeLinkingId);
                if (productList[i].AttributeLinking != null)
                {
                    productList[i].AttributeValue = productList[i].AttributeLinking.Value.Split(',').ToList();
                    productList[i].AttributeList = productList[i].AttributeLinking.AttributeSetIds.Split(',').ToList();
                    productList[i].Attribute = new List<string>();
                    int z = 2;
                    for (int j = 0; j < z; j++)
                    {
                        if (productList[i].AttributeValue[j].ToString() != "None")
                        {
                            string AttributeDemo = productList[i].AttributeValue[j].ToString();

                            productList[i].Attribute.Add(AttributeDemo);
                        }
                    }
                    for (int j = 0; j < productList[i].AttributeList.Count; j++)
                    {
                        if (productList[i].AttributeList[j].ToString() == "Type")
                        {
                            if (productList[i].AttributeValue[j].ToString() != "None")
                            {
                                string AttributeDemo = productList[i].AttributeValue[j].ToString();

                                productList[i].Attribute.Add(AttributeDemo);
                            }
                        }
                        else
                        {
                            z++;
                        }
                    }
                }
            }
            return productList;
        }

        public List<BarcodeViewModel> GetBarcodesForProduct(int id)
        {
            barcodecs objbar = new barcodecs();

            List<int> items = PurchaseInvoiceItems.Where(x => x.PurchaseInvoiceItemId == id).Select(x => x.PurchaseInvoiceItemId).ToList();
            List<BarcodeViewModel> productList = Products.Where(x => items.Contains(x.PurchaseInvoiceItemId)).Select(x => new BarcodeViewModel() { SerialNumber = x.SerialNumber, PurchaseInvoiceItemId = x.PurchaseInvoiceItemId, MRP = x.MRP, GSTId = x.GSTId, AttributeLinkingId = x.AttributeLinkingId, SKUMasterId = x.SKUMasterId }).ToList();
            Dictionary<int, SKUMaster> idVsSKU = GetSKUMasterDictionary(productList.Select(x => x.SKUMasterId).ToList());
            Company company = SessionManager.GetSessionCompany();
            Dictionary<int, GST> idVsGST = GetGSTMasterDictionary(productList.Select(x => x.GSTId).ToList());

            for (int i = 0; i < productList.Count; i++)
            {
                productList[i].SKUMaster = idVsSKU[productList[i].SKUMasterId];
                productList[i].Company = company;
                productList[i].GST = idVsGST[productList[i].GSTId];
                productList[i].Barcode = objbar.GenerateBarcodeString(productList[i].SerialNumber);

                if (!productList[i].GST.IGST.HasValue) productList[i].GST.IGST = 0;
                if (!productList[i].GST.CGST.HasValue) productList[i].GST.CGST = 0;

                productList[i].GSTPercent = Convert.ToDecimal(productList[i].GST.CGST + productList[i].GST.IGST + productList[i].GST.SGST);
                productList[i].AttributeLinking = GetAttributeLinkingById(productList[i].AttributeLinkingId);
                if (productList[i].AttributeLinking != null)
                {
                    productList[i].AttributeValue = productList[i].AttributeLinking.Value.Split(',').ToList();
                    productList[i].AttributeList = productList[i].AttributeLinking.AttributeSetIds.Split(',').ToList();
                    productList[i].Attribute = new List<string>();
                    int z = 2;
                    for (int j = 0; j < z; j++)
                    {
                        if (productList[i].AttributeValue[j].ToString() != "None")
                        {
                            string AttributeDemo = productList[i].AttributeValue[j].ToString();

                            productList[i].Attribute.Add(AttributeDemo);
                        }
                    }
                    for (int j = 0; j < productList[i].AttributeList.Count; j++)
                    {
                        if (productList[i].AttributeList[j].ToString() == "Type")
                        {
                            if (productList[i].AttributeValue[j].ToString() != "None")
                            {
                                string AttributeDemo = productList[i].AttributeValue[j].ToString();

                                productList[i].Attribute.Add(AttributeDemo);
                            }
                        }
                        else
                        {
                            z++;
                        }
                    }
                }
            }
            return productList;
        }
        #endregion

        public override int SaveChanges()
        {
            var addEntityList = ChangeTracker.Entries().Where(x => x.Entity is BaseModel && x.State == EntityState.Added);

            var modifiedEntityList = ChangeTracker.Entries().Where(x => x.Entity is BaseModel && x.State == EntityState.Modified);

            var DeletedEntityList = ChangeTracker.Entries().Where(x => x.Entity is BaseModel && x.State == EntityState.Deleted);

            var company = SessionManager.GetSessionCompany();
            var user = SessionManager.GetSessionUser();

            foreach (var entity in addEntityList)
            {
                ((BaseModel)entity.Entity).CreatedDate = DateTime.Now;
                ((BaseModel)entity.Entity).CreatedBy = user.Username;
            }
            foreach (var entity in modifiedEntityList)
            {

                ((BaseModel)entity.Entity).LastModifiedDate = DateTime.Now;
                ((BaseModel)entity.Entity).ModifiedBy = user.Username;
            }
            return base.SaveChanges();
        }
    }
}

