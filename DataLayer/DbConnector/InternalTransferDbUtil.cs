﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;
using PagedList;
using System.Data.Entity;
using DataLayer.Utils;

namespace DataLayer
{
    public class InternalTransferDbUtil : BaseDbUtil
    {
        DbConnector db = new DbConnector();

        internal IPagedList<InternalTransfer> GetInternalTransfer(int? PageNo)
        {
            IPagedList<InternalTransfer> InternalTransfers = null;
            try
            {
                InternalTransfers = db.InternalTransfers.Where(x => x.CompanyId == db.CompanyId).OrderByDescending(x => x.InternalTransferId).ToList().ToPagedList(PageNo ?? 1, Constants.PageSize);
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
            return InternalTransfers;
        }

        public List<SaleInvoiceItem> GetTransferItems(int InternalTransferId)
        {
            List<SaleInvoiceItem> TransferItems = null;
            try
            {
                TransferItems = db.SaleInvoiceItems.Where(x => x.InternalTransferId == InternalTransferId).OrderByDescending(x => x.InternalTransferId).ToList();
                if (TransferItems == null)
                {
                    TransferItems = new List<SaleInvoiceItem>();
                }
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
            return TransferItems;
        }

        internal void SaveInternalTransfer(InternalTransfer transfer)
        {
            transfer.InternalTransfers = null;
            try
            {
                if (transfer.InternalTransferId > 0)
                {
                    db.Entry(transfer).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    TimeZoneInfo timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                    DateTime dateTime = TimeZoneInfo.ConvertTime(DateTime.UtcNow, timeZoneInfo);
                    transfer.CompanyId = SessionManager.GetSessionCompany().CompanyId;
                    GenerateAutoNo No = new GenerateAutoNo();
                    transfer.InternalTransferNo = No.GenerateInternalTransferNo();
                    db.InternalTransfers.Add(transfer);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
        }

        internal void SaveInternalTransferItem(int internalTransferId,int ToLocation, int FromLocation)
        {
            try
            {
                List<SaleInvoiceItem> TransferItems = SessionManager.GetInvoiceItemList(Constants.INTERNAL_TRANSFER);
                if (TransferItems != null)
                {
                    foreach (var items in TransferItems)
                    {
                        items.Product = null;
                        items.GST = null;
                        items.DiscountCodeList = null;
                        items.GSTList = null;
                        items.Unit = null;
                        items.SKUMaster = null;
                        items.Status = Constants.INTERNAL_TRANSFER;
                        items.InternalTransferId = internalTransferId;
                        db.SaleInvoiceItems.Add(items);
                        db.SaveChanges();
                    }
                }
                #region Reduce Products From Stock
                List<int> ProductIds = db.SaleInvoiceItems.Where(x => x.InternalTransferId == internalTransferId).Select(x => x.ProductId).ToList();
                List<Product> ProductList = db.Products.Where(x => ProductIds.Contains(x.ProductId)).ToList();
                foreach (var product in ProductList)
                {
                    product.FromLocation = FromLocation;
                    product.LocationId = ToLocation;
                    db.SaveProduct(product);
                }
                #endregion
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
            SessionManager.EmptySessionList(Constants.INTERNAL_TRANSFER);
        }

        public bool RemoveItem(int? InternalTransferId, int ProductId)
        {
            bool result = true;
            SaleInvoiceItem item;
            try
            {
                Product product = db.Products.Where(x => x.ProductId == ProductId).FirstOrDefault();
                if(product != null)
                {
                    product.LocationId = product.FromLocation ?? 0;
                    product.FromLocation = null;
                    db.SaveProduct(product);
                }
                item = db.SaleInvoiceItems.Where(x => x.ProductId == ProductId && x.InternalTransferId == InternalTransferId).FirstOrDefault();
                db.SaleInvoiceItems.Remove(item);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                db.RegisterException(ex);
            }
            return result;
        }

        public bool DeleteInternalTransfer(List<Product> productList,InternalTransfer transfer)
        {
            try
            {
                foreach (var item in productList)
                {
                    db.Entry(item).State = EntityState.Modified;
                }

                List<SaleInvoiceItem> saleInvoiceItemList = db.SaleInvoiceItems.Where(x => x.SaleInvoiceId == transfer.InternalTransferId).ToList();
                foreach (var itemToBeDeleted in saleInvoiceItemList)
                {
                    db.SaleInvoiceItems.Remove(itemToBeDeleted);
                }

                SaveChanges(db);
                InternalTransfer transfers = db.InternalTransfers.Where(x => x.InternalTransferId == transfer.InternalTransferId).FirstOrDefault();
                db.Entry(transfers).State = EntityState.Deleted;
                SaveChanges(db);
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
                return false;
            }
            return true;
        }
    }
}
