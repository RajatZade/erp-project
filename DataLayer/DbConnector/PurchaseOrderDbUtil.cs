﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;

namespace DataLayer
{
    public class PurchaseOrderDbUtil
    {
        DbConnector db = new DbConnector();
        public bool DeletePurchaseOrder(List<PurchaseOrderItem> purchaseOrderItemItems, PurchaseOrder purchaseOrder, List<AttributeLinking> attributeLinkings)
        {
            foreach (var item in purchaseOrderItemItems)
            {
                db.PurchaseOrderItems.Attach(item);
                db.PurchaseOrderItems.Remove(item);
            }

            foreach (var item in attributeLinkings)
            {
                db.AttributeLinkings.Attach(item);
                db.AttributeLinkings.Remove(item);
            }

            try
            {
                db.SaveChanges();
                PurchaseOrder order = db.PurchaseOrders.Where(x => x.PurchaseOrderId == purchaseOrder.PurchaseOrderId).FirstOrDefault();
                db.Entry(order).State = EntityState.Deleted;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
                return false;
            }
            return true;
        }

        internal List<int> GetProductAttributes(List<int> purchaseOrderItemIds)
        {
            List<int> attributesList = null;
            try
            {
                attributesList = db.AttributeLinkings.Where(x => purchaseOrderItemIds.Contains(x.PurchaseOrderItemId ?? 0)).Select(x => x.AttributeLinkingId).ToList();
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
            return attributesList;
        }
    }
}
