﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace DataLayer
{
    public class SaleInvoiceDbUtil : BaseDbUtil
    {
        DbConnector db = new DbConnector();

        internal List<Product> GetProductsBySaleInvoiceItems(List<int> productIds)
        {
            List<Product> productList = null;
            try
            {
                productList = db.Products.Where(x => productIds.Contains(x.ProductId)).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
            }
            return productList;
        }

        internal List<int> GetVoucherIdsBySalesInvoice(int saleInvoiceId)
        {
            List<int> voucherList = null;
            try
            {
                voucherList = db.Vouchers.Where(x => x.SaleInvoiceId == saleInvoiceId).Select(x => x.VoucherId).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
            }
            return voucherList;
        }

        public bool DeleteSaleInvoice(List<Product> productList, List<SaleInvoiceItem> saleInvoiceItems, SaleInvoice salesinvoice)
        { 
            try
            {
                List<Voucher> VoucherList = VoucherList = db.Vouchers.Where(x => x.SaleInvoiceId == salesinvoice.SaleInvoiceId).ToList();

                foreach (var item in VoucherList)
                {
                    db.Vouchers.Remove(item);
                }

                foreach (var item in productList)
                {
                    db.Entry(item).State = EntityState.Modified;
                }

                List<SaleInvoiceItem> saleInvoiceItemList = db.SaleInvoiceItems.Where(x => x.SaleInvoiceId == salesinvoice.SaleInvoiceId).ToList();
                List<SaleInvoiceItem> ItemList = db.SaleInvoiceItems.Where(x => x.ReturnSaleInvoiceId == salesinvoice.SaleInvoiceId).ToList();
                saleInvoiceItemList.AddRange(ItemList);
                foreach (var itemToBeDeleted in saleInvoiceItemList)
                {
                    if(itemToBeDeleted.Status == Constants.SALE_ITEM_SOLD)
                    {
                        db.SaleInvoiceItems.Remove(itemToBeDeleted);
                    }
                    else
                    {
                        itemToBeDeleted.Status = Constants.SALE_ITEM_SOLD;
                        itemToBeDeleted.ReturnSaleInvoiceId = null;
                        db.Entry(itemToBeDeleted).State = EntityState.Modified;
                    }
                }
                    

                SaveChanges(db);
                SaleInvoice invoice = db.SaleInvoices.Where(x => x.SaleInvoiceId == salesinvoice.SaleInvoiceId).FirstOrDefault();
                db.Entry(invoice).State = EntityState.Deleted;
                SaveChanges(db);
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
                return false;
            }
            return true;
        }

        public void UpdateSaleInvoiceItem(SaleInvoiceItem item)
        {
            DbConnector context = new DbConnector();
            try
            {
                item.GST = null;
                item.Product = null;
                db.Entry(item).State = EntityState.Modified;
                SaveChanges(context);
            }
            catch (Exception ex)
            {
                RegisterException(context, ex);
            }
        }

        public bool DeleteSaleVoucher(SaleInvoice salesinvoice)
        {
            try
            {
                List<ProductVoucher> ProductVouchers = db.ProductVouchers.Where(x => x.SaleInvoiceId == salesinvoice.SaleInvoiceId).ToList();
                List<Voucher> VoucherList = VoucherList = db.Vouchers.Where(x => x.SaleInvoiceId == salesinvoice.SaleInvoiceId).ToList();

                foreach (var item in VoucherList)
                {
                    db.Vouchers.Remove(item);
                }

                foreach (var item in ProductVouchers)
                {
                    db.ProductVouchers.Remove(item);
                }

                SaveChanges(db);
                SaleInvoice invoice = db.SaleInvoices.Where(x => x.SaleInvoiceId == salesinvoice.SaleInvoiceId).FirstOrDefault();
                db.Entry(invoice).State = EntityState.Deleted;
                SaveChanges(db);
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
                return false;
            }
            return true;
        }
    }
}
