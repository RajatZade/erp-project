﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLayer.Models;
using System.Web.Mvc;
using System.Data.Entity;

namespace DataLayer.Utils
{
    public class InventoryHandler
    {
        DbConnector db = new DbConnector();
        PurchaseInvoiceDbUtil DbUtil = new PurchaseInvoiceDbUtil();
        public bool DeletePurchaseInvoice(PurchaseInvoice purchaseInvoice)
        {
            bool result = false;
            PurchaseInvoiceDbUtil db = new PurchaseInvoiceDbUtil();
            List<int> purchaseInvoiceItemIds = purchaseInvoice.PurchaseInvoiceItemList.Select(x => x.PurchaseInvoiceItemId).ToList();
            Product product = db.GetUnsoldProductByPurchaseInvoiceItemId(purchaseInvoiceItemIds);
            if (product != null)
            {
                return result;
            }
            List<int> productIds = db.GetProductIdsByInvoiceItems(purchaseInvoiceItemIds);
            List<int> attributesToBeDeleted = db.GetProductAttributes(productIds, purchaseInvoiceItemIds);
            List<AttributeLinking> attributeLinkings = new List<AttributeLinking>();
            List<Product> products = new List<Product>();
            List<PurchaseInvoiceItem> purchaseInvoiceItems = new List<PurchaseInvoiceItem>();

            foreach (var item in attributesToBeDeleted)
            {
                AttributeLinking obj = new AttributeLinking() { AttributeLinkingId = item };
                attributeLinkings.Add(obj);
            }
            foreach (var item in productIds)
            {
                Product obj = new Product() { ProductId = item };
                products.Add(obj);
            }
            foreach (var item in purchaseInvoiceItemIds)
            {
                PurchaseInvoiceItem obj = new PurchaseInvoiceItem() { PurchaseInvoiceItemId = item };
                purchaseInvoiceItems.Add(obj);
            }

            if (DbUtil.DeleteOpeningstock(attributeLinkings, products, purchaseInvoiceItems, purchaseInvoice))
            {
                result = true;
            }
            return result;
        }

        public int SavePurchaseInvoice(PurchaseInvoice purchaseInvoice, string btn)
        {
            switch (btn)
            {
                case "Save":
                    {
                        DbUtil.SavePurchaseInvoice(purchaseInvoice);
                        break;
                    }
                case "Save and Print Barcode":
                    {
                        DbUtil.SavePurchaseInvoice(purchaseInvoice);
                        CreateProducts(purchaseInvoice.PurchaseInvoiceId, purchaseInvoice.CompanyId ?? 0);
                        break;
                    }
                case "Save Opening Stock":
                    {
                        DbUtil.SavePurchaseInvoice(purchaseInvoice);
                        break;
                    }
            }
            CalculatePurchaseInvoice(purchaseInvoice.PurchaseInvoiceId);
            return purchaseInvoice.PurchaseInvoiceId;
        }

        public PurchaseInvoiceItem SavePurchaseInvoiceItem(PurchaseInvoiceItem purchaseInvoiceItem, string btn)
        {
            db.SavePurchaseInvoiceItem(purchaseInvoiceItem);
            purchaseInvoiceItem.AttributeLinkingList = SessionManager.GetAttributeList();
            if (purchaseInvoiceItem.AttributeLinkingList != null)
            {
                foreach (var item in purchaseInvoiceItem.AttributeLinkingList)
                {
                    if (item.PurchaseInvoiceItemId == null || item.PurchaseInvoiceItemId == 0)
                    {
                        item.PurchaseInvoiceItemId = purchaseInvoiceItem.PurchaseInvoiceItemId;
                        item.AttributeLinkingId = 0;
                    }
                }
                db.SaveAttributeLinking(purchaseInvoiceItem.AttributeLinkingList);
                SessionManager.EmptySessionList(Constants.ATTRIBUTE_LIST);
            }

            CalculatePurchaseInvoice(purchaseInvoiceItem.PurchaseInvoiceId);
            return purchaseInvoiceItem;
        }

        public PurchaseInvoiceItem GetInvoiceItemForCreateEdit(int invoiceId, int? invoiceItemId, string IsClone)
        {
            PurchaseInvoiceItem item = null;

            if (invoiceItemId.HasValue)
            {
                item = db.PurchaseInvoiceItems.Include(x => x.SKUMaster).Where(x => x.PurchaseInvoiceItemId == invoiceItemId).FirstOrDefault();
                //item.DiscountIDTEmp = item.DiscountId;
                item.AttributeLinkingList = db.GetInvoiceItemAttribute(invoiceItemId);
                if (item.AttributeLinkingList != null)
                {
                    foreach (var i in item.AttributeLinkingList)
                    {
                        i.NameList = i.AttributeSetIds.Split(',').ToList();
                        i.ValuesList = i.Value.Split(',').ToList();
                    }
                }
            }
            else
            {
                item = new PurchaseInvoiceItem
                {
                    PurchaseInvoiceId = invoiceId,
                    DiscountsAccessible = "",
                };
            }
            item.CategoryList = DropdownManager.GetCategoryList(item.CategoryId);
            item.SubcategoryList = DropdownManager.GetSubCategoryList(item.SubCategoryId);
            item.SKUList = DropdownManager.GetSKUList(item.SKUMasterId);
            item.HSNList = DropdownManager.GetHSNList(item.HSNId);
            item.BrandList = DropdownManager.GetBrandList(item.BrandId);
            item.LocationList = DropdownManager.GetLocationByBranch(SessionManager.GetSessionBranch().BranchId);
            item.DiscountCodeList = DropdownManager.GetDiscountsAccessibleList(item.DiscountsAccessible,null);
            item.BrandList = DropdownManager.GetBrandList(item.BrandId);
            item.GSTList = DropdownManager.GetGSTList(item.GSTId);

            Dictionary<AttributeSet, List<SelectListItem>> AttributeDictionayList = null;
            item.AttributeSetListNew = db.GetAttributeSets();

            AttributeDictionayList = new Dictionary<AttributeSet, List<SelectListItem>>();
            for (int i = 0; i < item.AttributeSetListNew.Count; i++)
            {
                int Id = item.AttributeSetListNew[i].AttributeSetId;
                //item.AttributeLinkingList = db.getAttributeLinkingList(Id);
                item.AttributeSetListNew[i].AttributeValueList = DropdownManager.GetAttributeValueList(Id);
                if (AttributeDictionayList == null)
                {
                    //  AttributeDictionayList.Add(0,)

                }
                AttributeDictionayList.Add(item.AttributeSetListNew[i], item.AttributeSetListNew[i].AttributeValueList);
            }
            item.AttributeDictionayList = AttributeDictionayList;
            SessionManager.EmptySessionList(Constants.ATTRIBUTE_LIST);
            if (!string.IsNullOrEmpty(IsClone))
            {
                item.IsClone = IsClone;
                item.PurchaseInvoiceItemId = 0;
                item.IsProductCreated = false;
                foreach (var AttributeItem in item.AttributeLinkingList)
                {
                    AttributeItem.PurchaseInvoiceItemId = 0;
                    SessionManager.SetAttribute(AttributeItem);
                }
                item.AttributeLinkingList = SessionManager.GetAttributeList();
            }
            if (item.IsProductCreated)
            {
                item.UnitList = DropdownManager.GetFreezedUnitList(item.UnitId);
            }
            else
            {
                item.UnitList = DropdownManager.GetUnitMasterList(item.UnitId);
            }
            return item;
        }

        public PurchaseInvoice GetPurchaseInvoiceForCreateEdit(int? id)
        {
            PurchaseInvoice purchaseinvoice = null;
            if (id.HasValue)
            {
                purchaseinvoice = db.PurchaseInvoices.Include("PurchaseInvoiceItemList").Where(x => x.PurchaseInvoiceId == id).FirstOrDefault();
                if (purchaseinvoice == null)
                {
                    purchaseinvoice = new PurchaseInvoice();
                }
                Dictionary<int, SKUMaster> idVsSKU = db.GetSKUMasterDictionary(purchaseinvoice.PurchaseInvoiceItemList.Select(x => x.SKUMasterId).ToList());
                Dictionary<int, GST> idVsGST = db.GetGSTMasterDictionary(purchaseinvoice.PurchaseInvoiceItemList.Select(x => x.GSTId).ToList());
                foreach (var item in purchaseinvoice.PurchaseInvoiceItemList)
                {
                    item.SKUMaster = idVsSKU[item.SKUMasterId];
                    item.GST = idVsGST[item.GSTId];
                    purchaseinvoice.ItemTotalQuantity = purchaseinvoice.ItemTotalQuantity + item.Quantity;
                    purchaseinvoice.ItemTotalBasicPrice = purchaseinvoice.ItemTotalBasicPrice + item.Rate;
                    purchaseinvoice.ItemTotalMRP = purchaseinvoice.ItemTotalMRP + item.MRP;                    
                    purchaseinvoice.ItemPurchasePrice = purchaseinvoice.ItemPurchasePrice + item.NetAmount.Value;
                    purchaseinvoice.ItemFinalTotal = purchaseinvoice.ItemFinalTotal + (item.Quantity * item.NetAmount.Value);
                    purchaseinvoice.FinalTotalAdj = purchaseinvoice.ItemFinalTotal;
                }
            }
            else
            {
                purchaseinvoice = new PurchaseInvoice
                {
                    CompanyId = db.CompanyId,
                    IsOpeningStock = true,
                    Type = Constants.PURCHASE_TYPE_OPENING_STOCK,
                    InvoicingDate = Convert.ToDateTime(DateTime.Now),
                    EntryDate = Convert.ToDateTime(DateTime.Now),
                };
                purchaseinvoice.BranchId = SessionManager.GetSessionBranch().BranchId;
                purchaseinvoice.DivisionId = SessionManager.GetSessionDivision().DivisionId;
            }
            purchaseinvoice.ShippingTypeList = DropdownManager.GetShippingMasterList(null);
            purchaseinvoice.DivisionList = DropdownManager.GetDivisionList(null);
            purchaseinvoice.BranchList = DropdownManager.GetBranchList(null);
            purchaseinvoice.LocationList = DropdownManager.GetLocationByBranch(purchaseinvoice.BranchId ?? 0);
            SessionManager.EmptySessionList(Constants.ATTRIBUTE_LIST);
            return purchaseinvoice;
        }

        private void CreateProducts(int id,int CompanyId)
        {
            List<Product> productsToBeAdded = new List<Product>();
            List<PurchaseInvoiceItem> itemList = db.GetPurchaseInvoiceItems(id);
            if (itemList != null && itemList.Count > 0)
            {
                foreach (var item in itemList)
                {
                    item.IsOpeningStock = true;
                    if (item.IsProductCreated != true && item.PurchaseInvoiceId > 0)
                    {
                        item.AttributeLinkingList = db.GetAttributeLinkingList(item.PurchaseInvoiceItemId);
                        #region Sale Without Partial
                        if (item.SellInPartial != true)
                        {
                            if (item.AttributeLinkingList != null && item.AttributeLinkingList.Count > 0)
                            {
                                int CheckQuantity = 0;
                                foreach (var AttributeItem in item.AttributeLinkingList)
                                {
                                    for (int i = 0; i < AttributeItem.Quantity; i++)
                                    {
                                        Product product = new Product(item)
                                        {
                                            AttributeLinkingId = AttributeItem.AttributeLinkingId,
                                            SKUMaster = db.GetSKUMasterById(item.SKUMasterId)
                                        };
                                        productsToBeAdded.Add(product);
                                    }
                                    CheckQuantity = CheckQuantity + Convert.ToInt32(AttributeItem.Quantity);
                                }
                                if ((item.Quantity - CheckQuantity) > 0)
                                {
                                    int NewQunatity = Convert.ToInt32(item.Quantity) - CheckQuantity;
                                    for (int i = 0; i < NewQunatity; i++)
                                    {
                                        Product product = new Product(item)
                                        {
                                            AttributeLinkingId = null
                                        };
                                        productsToBeAdded.Add(product);
                                    }
                                }
                            }
                            else
                            {
                                Product product = new Product(item);
                                for (int i = 0; i < item.Quantity; i++)
                                {
                                    productsToBeAdded.Add(product);
                                }
                            }
                        }
                        #endregion
                        #region Sale in Partial
                        else
                        {
                            if (item.AttributeLinkingList != null && item.AttributeLinkingList.Count > 0)
                            {
                                decimal CheckQuantity = 0;
                                foreach (var AttributeItem in item.AttributeLinkingList)
                                {
                                    Product product = new Product(item)
                                    {
                                        Quantity = AttributeItem.Quantity
                                    };
                                    var AttributeLinkingId = AttributeItem.AttributeLinkingId;
                                    product.AttributeLinkingId = AttributeLinkingId;
                                    product.SKUMaster = db.GetSKUMasterById(item.SKUMasterId);
                                    productsToBeAdded.Add(product);
                                    CheckQuantity = CheckQuantity + AttributeItem.Quantity;
                                }
                                if ((item.Quantity - CheckQuantity) > 0)
                                {
                                    decimal NewQunatity = item.Quantity - CheckQuantity;
                                    Product product = new Product(item)
                                    {
                                        AttributeLinkingId = null,
                                        Quantity = NewQunatity,
                                        SKUMaster = db.GetSKUMasterById(item.SKUMasterId)
                                    };
                                    productsToBeAdded.Add(product);
                                }
                            }
                            else
                            {
                                Product product = new Product(item);
                                productsToBeAdded.Add(product);
                            }
                        }
                        #endregion
                    }
                    else
                    {

                        var SoldItem = db.GetProductsListByInvoiceIdIsSoldTrue(item.PurchaseInvoiceItemId);
                        if (SoldItem.Count == 0)
                        {
                            ProductManager.UpdateProduct(item);
                        }
                    }
                }
            }
            if (productsToBeAdded.Count > 0)
            {
                db.InsertProducts(productsToBeAdded, CompanyId);
                db.MarkInvoiceItemsAsProductGenerated(itemList.Select(x => x.PurchaseInvoiceItemId).ToList());
            }
        }

        public void CalculatePurchaseInvoice(int? id)
        {
            try
            {
                PurchaseInvoice Invoice = db.PurchaseInvoices.Where(x => x.PurchaseInvoiceId == id).FirstOrDefault();
                Invoice.PurchaseInvoiceItemList = db.PurchaseInvoiceItems.Where(x => x.PurchaseInvoiceId == id).ToList();


                Invoice.TotalInvoiceValue = 0;
                foreach (var item in Invoice.PurchaseInvoiceItemList)
                {
                    Invoice.TotalInvoiceValue = Invoice.TotalInvoiceValue + (item.Quantity * item.NetAmount.Value);
                }

                Invoice.NotmappedInvoiceValue = Invoice.TotalInvoiceValue + (Invoice.Adjustment ?? 0);

                db.Entry(Invoice).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
        }
    }
}
