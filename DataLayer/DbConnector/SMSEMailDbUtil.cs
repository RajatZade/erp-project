﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;

namespace DataLayer
{
    public class SMSEMailDbUtil : BaseDbUtil
    {
        DbConnector db = new DbConnector();

        public int SaveEmail(EmailConfig emailConfig)
        {
            int id = 0;
            try
            {
                db.EmailConfigs.Add(emailConfig);
                id = db.SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
            }
            return id;
        }

        public List<SMSConfig> GetSmsConfig()
        {
            List<SMSConfig> SmsConfigList = null;
            try
            {
                var SmsConfig = from n in db.SmsConfigs
                                select n;
                if (db.SmsConfigs != null)
                {
                    SmsConfigList = SmsConfig.ToList();
                }
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
            }
            return SmsConfigList;
        }

        public SMSConfig GetSmsConfigById(int? id)
        {
            SMSConfig smsConfig = null;
            try
            {
                smsConfig = db.SmsConfigs.Where(x => x.SmsConfigId == id).Single();
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
            }
            return smsConfig;
        }

        public int EditEmail(EmailConfig emailConfig)
        {
            int id = -1;
            try
            {
                db.Entry(emailConfig).State = EntityState.Modified;
                id = db.SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
            }
            return id;
        }

        public EmailConfig GetEmailDetails()
        {
            EmailConfig email = null;
            try
            {
                email = db.EmailConfigs.Where(x => x.IsDefault == true).Single();
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
            }
            return email;
        }

        public int SaveSmsConfig(SMSConfig smsConfig)
        {
            int id = -1;
            try
            {
                db.SmsConfigs.Add(smsConfig);
                id = db.SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
            }
            return id;
        }

        public int SaveSmsQueue(SmsQueue smsQueue)
        {
            DbConnector db = new DbConnector();
            int id = -1;
            try
            {
                smsQueue.CreatedDate = DateTime.Now;
                db.smsQueues.Add(smsQueue);
                db.SaveChanges();
                id = smsQueue.SmsQueueId;
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
            }
            return id;
        }

        public SMSConfig GetSmsDetails()
        {
            var query = db.SmsConfigs.Where(x => x.IsDefault == true).FirstOrDefault();
            return query;
        }

        public List<Contact> GetContactForSms()
        {
            List<Contact> ContactList = null;
            try
            {
                ContactList = db.Contacts.Include(X => X.AddressLine1).Where(x => x.Mobile != null).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
            }
            return ContactList;
        }

        public List<Contact> GetContactForEmailId()
        {
            List<Contact> ContactList = null;
            try
            {
                ContactList = db.Contacts.Where(x => x.Email != null).ToList();
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
            }
            return ContactList;
        }

        public int DelteEmailConfig(EmailConfig emailConfig)
        {
            int id = -1;
            try
            {
                db.EmailConfigs.Remove(emailConfig);
                id = db.SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
            }
            return id;
        }

        public int DelteSmsConfig(SMSConfig smsConfig)
        {
            int id = -1;
            try
            {
                db.SmsConfigs.Remove(smsConfig);
                id = db.SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
            }
            return id;
        }

        public int EditSmsConfig(SMSConfig smsConfig)
        {
            int id = -1;
            try
            {
                db.Entry(smsConfig).State = EntityState.Modified;
                id = db.SaveChanges();
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
            }
            return id;
        }

        public EmailConfig GetEmailById(int? id)
        {
            EmailConfig emailConfig = null;
            try
            {
                emailConfig = db.EmailConfigs.Where(x => x.EmailConfigId == id).Single();
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
            }
            return emailConfig;
        }

        public List<EmailConfig> GetEmail()
        {
            List<EmailConfig> EmailConfigList = null;
            try
            {
                var EmailConfig = from u in db.EmailConfigs select u;
                if (EmailConfig != null)
                {
                    EmailConfigList = EmailConfig.ToList();
                }
            }
            catch (Exception ex)
            {
                RegisterException(db, ex);
            }
            return EmailConfigList;
        }
    }
}
