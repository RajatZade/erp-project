﻿using DataLayer;
using DataLayer.Models;
using System.Linq;
using System;
using System.Data.Entity;

namespace DataLayer
{
    public class AttributesDbUtil
    {
        DbConnector db = new DbConnector();

        public object GetAllAttributes()
        {
            return db.AttributeSets.ToList();
        }

        public AttributeSet GetAttribute(int value)
        {
            return db.AttributeSets.Where(x => x.AttributeSetId == value).FirstOrDefault();
        }

        public bool DeleteAttribute(int id)
        {
            AttributeSet set = GetAttribute(id);
            try
            {
                db.AttributeSets.Remove(set);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void SaveAttribute(AttributeSet attributeSet)
        {
            if(attributeSet.AttributeSetId > 0)
            {
                db.Entry(attributeSet).State = EntityState.Modified;
            }
            else
            {
                db.AttributeSets.Add(attributeSet);
            }
            db.SaveChanges();
        }
    }
}
