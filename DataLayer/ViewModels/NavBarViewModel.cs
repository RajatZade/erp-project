﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class NavBarViewModel
    {
        public Division Division { get; set; }
        public Company Company { get; set; }
        public Branch Branch { get; set; }
        public LoginUser User { get; set; }
    }
}
