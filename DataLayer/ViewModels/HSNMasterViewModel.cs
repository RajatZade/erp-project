﻿using System.Collections.Generic;
using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class HSNMasterViewModel
    {
        public HSN HsnMaster { get; set; }
        public List<HSN> HSNMasterList { get; set; }
    }
}
