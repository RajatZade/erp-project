﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.ViewModels
{
    public class PrintVoucherViewModel
    {
        public Company Company { get; set; }
        public Branch Branch { get; set; }
        public Voucher Voucher { get; set; }
        public string TotalInWord { get; set; }
    }
}
