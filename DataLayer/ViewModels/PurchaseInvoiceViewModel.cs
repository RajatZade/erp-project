﻿using System.Collections.Generic;
using DataLayer.Models;
using PagedList;

namespace DataLayer.ViewModels
{
    public class PurchaseInvoiceViewModel
    {
        public PurchaseInvoice PurchaseInvoice { get; set; }
        public List<PurchaseInvoice> PurchaseInvoiceList { get; set; }
        public IPagedList<PurchaseInvoice> PurchaseInvoiceLists { get; set; }
    }
}
