﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class BudgetingViewModel
    {
        public Budgeting Budgeting { get; set; }
        public BudgetSetup BudgetSetup { get; set; }
        public List<BudgetSetup> BudgetSetupListS { get; set; }
        public List<Budgeting> BudgetingList { get; set; }
        public List<SelectListItem> BudgetSetupList { get; set; }
        public List<SelectListItem> LedgerList { get; set; }
        public decimal TotalSale { get; set; }
        public BudgetingViewModel()
        {
            BudgetSetupList = new List<SelectListItem>();
            LedgerList = new List<SelectListItem>();
        }
    }

    
}
