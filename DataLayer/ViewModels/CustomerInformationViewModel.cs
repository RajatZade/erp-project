﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class CustomerInformationViewModel
    {
        public Ledger Ledger { get; set; }
        public Contact Contact { get; set; }        
        public List<PurchaseInvoice> PurchaseInvoiceList { get; set; }
        public List<SaleInvoice> SaleInvoiceList { get; set; }
        public List<SelectListItem> ContactList { get; set; }
        public int ContactId { get; set; }
        public decimal TotalPurchaseValue { get; set; }
        public decimal TotalSaleValue { get; set; }
    }
}
