﻿using System.Collections.Generic;
using DataLayer.Models;
using System.Web.Mvc;

namespace DataLayer.ViewModels
{
    public class LedgerViewModel
    {
        public Ledger Ledger { get; set; }
        public List<Ledger> LedgerList { get; set; }
        public List<Ledger> LedgerListofCustomer { get; set; }
        public List<Ledger> LedgerListofSupplier { get; set; }
        public List<Ledger> LedgerListOfOther { get; set; }
        public List<Ledger> LedgerListOfEmployee { get; set; }
        
        public List<SelectListItem> SelectType { get; set; }
        public string Type { get; set; }
        public LedgerViewModel()
        {
            SelectType = new List<SelectListItem>()
            {
                new SelectListItem() { Text="Other",Value="Other"},
                new SelectListItem() { Text="Customer",Value="Customer"},
                new SelectListItem() { Text="Employee",Value="Employee"},
                new SelectListItem() { Text="Supplier",Value="Supplier"}
            };
        }
    }
}
