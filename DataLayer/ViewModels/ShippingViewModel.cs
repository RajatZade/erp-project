﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class ShippingViewModel
    {
        public Shipping Shipping { get; set; }
        public List<Shipping> ShippingLists { get; set; }
    }
}
