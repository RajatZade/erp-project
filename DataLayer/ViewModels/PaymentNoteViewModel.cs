﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DataLayer.Models;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.ViewModels
{
    public class PaymentNoteViewModel
    {
        public PurchaseInvoice PurchaseInvoice { get; set; }
        public SaleInvoice SaleInvoice { get; set; }
        public string VoucherNumber { get; set; }
        public int FromPaymentTypeId { get; set; }
        public int ToPaymentTypeId { get; set; }
        public int FromLedgerId { get; set; }
        public int ToLedgerId { get; set; }
        public int FromVoucherId { get; set; }
        public int ToVoucherId { get; set; }
        public decimal Payment { get; set; }
        public string Description { get; set; }
        public string Checknumber { get; set; }
        public string InvoiceNumber { get; set; }
        public int? InvoiceId { get; set; }
        public string Type { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? EntryDate { get; set; }
        public Enums.MethodOfAdjustment MethodList { get; set; }
        public List<SelectListItem> LedgerList { get; set; }
        public List<SelectListItem> PaymentTyprList { get; set; }
        public List<SelectListItem> InvoiceIdList { get; set; }
        public PaymentNoteViewModel()
        {
            LedgerList = new List<SelectListItem>();
            PaymentTyprList = new List<SelectListItem>();
            InvoiceIdList = new List<SelectListItem>();
        }
    }
}
