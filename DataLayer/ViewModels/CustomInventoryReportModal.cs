﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DataLayer.ViewModels
{
    public class CustomInventoryReportModal
    {
        public List<AttributeSet> AttributeSetList { get; set; }
        public Dictionary<AttributeSet, List<SelectListItem>> AttributeDictionayList { get; set; }
        public List<AttributeSet> AttributeSetListNew { get; set; }
        public List<SelectListItem> CategoryList { get; set; }
        public List<SelectListItem> SubcategoryList { get; set; }
        public List<SelectListItem> SKUList { get; set; }
        public List<SelectListItem> BrandList { get; set; }
        public List<SelectListItem> LocationList { get; set; }
        public InventoryReportViewModel InventoryReportViewModel { get; set; }
        public SKUMasterList SKUMasterList { get; set; }
        public int SKUId { get; set; }
        public int BrandId { get; set; }
        public int SubCategoryId { get; set; }
        public int CategoryId { get; set; }
        public int[] CategoryIds { get; set; }
        public int[] SubCategoryIds { get; set; }
        public int[] SKUIds { get; set; }
        public int[] BrandIds { get; set; }
        public int[] LocationIds { get; set; }
        public decimal? MRPTo { get; set; }
        public decimal? MRPFrom { get; set; }
        public int? FromAgeNo { get; set; }
        public int? ToAgeNo { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? ToDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? FromDate { get; set; }
    }

    public class CustomSKUList
    {
        public string SKU { get; set; }
        public decimal InWard { get; set; }
        public decimal OnWards { get; set; }
    }
}
