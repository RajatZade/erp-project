﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class PaymentGateWayViewModel
    {
        //public SaleInvoice SaleInvoice { get; set; }
        //public Contact Contact { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string OrderId { get; set; }
        public string Phone { get; set; }
        public string Amount { get; set; }
        public string OnSuccess { get; set; }
        public string OnFailure { get; set; }
        public string PaymentTypeId { get; set; }
    }
}
