﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class ReportViewModel
    {
        public string Search { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? FromDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? ToDate { get; set; }
        public int AccountCategoryId { get; set; }
        public int LedgerId { get; set; }
        public int? AccountSubCategoryId { get; set; }
        public decimal TotalTax { get; set; }
        public decimal Subtotal { get; set; }
        public decimal TotalDiscount { get; set; }
        public decimal GrandTotal { get; set; }
        public decimal TotalCGST { get; set; }
        public decimal TotalSGST { get; set; }
        public decimal TotalIGST { get; set; }
        public Company Company { get; set; }
        public Branch Branch { get; set; }
        public List<SaleInvoice> SaleInvoiceList { get; set; }
        public List<PurchaseInvoice> PurchaseInvoiceList { get; set; }
        public List<Ledger> LedgerList = new List<Ledger>();
        public List<SelectListItem> AccountCategoryList { get; set; }
        public List<SelectListItem> AccountSubCategoryList { get; set; }
        public ReportViewModel()
        {
            AccountCategoryList = new List<SelectListItem>();
            AccountSubCategoryList = new List<SelectListItem>();
        }
    }
}
