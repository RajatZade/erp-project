﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class DiscountViewModel
    {
        public Discount Discount { get; set; }
        public List<Discount> DiscountTypeList { get; set; }
    }
}
