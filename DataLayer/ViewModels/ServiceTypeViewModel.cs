﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class ServiceTypeViewModel
    {
        public List<ServiceType> ServiceTypeList;
        public ServiceType ServiceType;
    }
}
