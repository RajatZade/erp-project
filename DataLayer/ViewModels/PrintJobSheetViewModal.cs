﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DataLayer.ViewModels
{
    public class PrintJobSheetViewModal
    {
        public List<JobSheet> JobSheetList { get; set; }
        public List<SelectListItem> PrintOptions { get; set; }
        public int Option { get; set; }
        public int JobSheetId { get; set; }
        public PrintJobSheetViewModal()
        {
            PrintOptions = new List<SelectListItem>();
            PrintOptions.Add(new SelectListItem() { Text = "Customer Copy", Value = "-1" });
        }

    }
}
