﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class BarcodeQueueViewModel
    {
        public List<Product> ProductList = new List<Product>();
        public string SerialNumber;
    }
}
