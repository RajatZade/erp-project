﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class CompanyViewModel
    {
        public Company Company { get; set; }
        public List<Company> CompanyList { get; set; }
    }
}
