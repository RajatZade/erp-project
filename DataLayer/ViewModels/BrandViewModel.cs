﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class BrandViewModel
    {
        public Brand Brand { get; set; }
        public List<Brand> BrandList { get; set; }
    }
}
