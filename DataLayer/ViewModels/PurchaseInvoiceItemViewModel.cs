﻿using DataLayer.Models;
namespace DataLayer.ViewModels
{
    public class PurchaseInvoiceItemViewModel
    {
        public PurchaseInvoiceItem PurchaseInvoiceItem { get; set; }
        public PurchaseOrderItem PurchaseOrderItem { get; set; }
        public Product Product { get; set; }
        public Discount Discount { get; set; }
    }
}
