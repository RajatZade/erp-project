﻿using System.Collections.Generic;
using System.Web.Mvc;
using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class ContactViewModel
    {
        public Contact Contact { get; set; }
        public Ledger Ledger { get; set; }
        public List<Contact> ContactList { get; set; }
        public string Type { get; set; }
        public List<SelectListItem> SelectType { get; set; }
        public ContactViewModel()
        {
            SelectType = new List<SelectListItem>()
            {
                new SelectListItem() { Text="Customer",Value="Customer"},
                new SelectListItem() { Text="Employee",Value="Employee"},
                new SelectListItem() { Text="Supplier",Value="Supplier"}
            };
        }
    }
}
