﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;
using PagedList;

namespace DataLayer.ViewModels
{
    public class SalesOrderViewModel
    {
        public SalesOrder SalesOrder { get; set; }
        public List<SalesOrder> SalesOrderList { get; set; }
        public IPagedList<SalesOrder> SalesOrderLists { get; set; }
    }
}
