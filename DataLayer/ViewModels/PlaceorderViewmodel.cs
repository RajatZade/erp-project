﻿using DataLayer.Models;
using System.Collections.Generic;

namespace DataLayer.ViewModels
{
    public class PlaceorderViewmodel
    {
        public SaleInvoice SaleInvoice { get; set; }
        public List<SaleInvoiceItem> SaleInvoiceItemlist { get; set; }
    }
}
