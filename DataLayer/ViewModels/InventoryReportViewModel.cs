﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DataLayer.ViewModels
{
    public class InventoryReportViewModel
    {
        public List<CateogoryList> CateogoryList { get; set; }
        public decimal? TotalOpeningValue { get; set; }
        public decimal? TotalInWardsValue { get; set; }
        public decimal? TotalOutWardsValue { get; set; }
        public decimal? ClosingBalance { get; set; }
        public int Count { get; set; }
        public int LocationId { get; set; }
        public Company Company { get; set; }
        public Branch Branch { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)] 
        public DateTime? FromDate { get; set; }
        
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? ToDate { get; set; }

        public List<SelectListItem> LocationList { get; set; }
        public InventoryReportViewModel()
        {
            CateogoryList = new List<CateogoryList>();
            TotalInWardsValue = 0;
            TotalOutWardsValue = 0;
            ClosingBalance = 0;
            TotalOpeningValue = 0;
        }
    }

    public class CateogoryList
    {
        public string CategoryName { get; set; }
        public List<SubCategoryList> SubCategoryList { get; set; }
        public decimal InWardsCountTotal { get; set; }
        public decimal InWardsRateTotal { get; set; }
        public decimal InWardsValueTotal { get; set; }
        public CateogoryList()
        {
            SubCategoryList = new List<SubCategoryList>();
        }
        
    }

    public class SubCategoryList
    {
        public int Count { get; set; }
        public string SubCategoryName { get; set; }
        public List<SKUMasterList> SKUMasterList { get; set; }
        public decimal InWardsCountTotal { get; set; }
        public decimal InWardsRateTotal { get; set; }
        public decimal InWardsValueTotal { get; set; }

        public decimal OpeningCountTotal { get; set; }
        public decimal OpeningRateTotal { get; set; }
        public decimal OpeningValueTotal { get; set; }

        public decimal OnWardsCountTotal { get; set; }
        public decimal OnWardsRateTotal { get; set; }
        public decimal OnWardsValueTotal { get; set; }

        public decimal ClosingBalanceCountTotal { get; set; }
        public decimal ClosingBalanceRateTotal { get; set; }
        public decimal ClosingBalanceValueTotal { get; set; }

        public SubCategoryList()
        {
            SKUMasterList = new List<SKUMasterList>();
        }
    }

    public class SKUMasterList
    {
        public string SKUName { get; set; }
        public int SKUMasterId { get; set; }
        public string Unit { get; set; }
        public ProductQuantity OpeningBalance { get; set; }
        public ProductQuantity InWards { get; set; }
        public ProductQuantity OnWards { get; set; }
        public ProductQuantity ClosingBalance { get; set; }
        public List<ProductQuantity> SKUStockList { get; set; }
        public List<AttributeSet> AttributeSetList;
        public List<Product> ProductList { get; set; }
        public virtual SKUMaster SKUMaster { get; set; }
        public SKUMasterList()
        {
            InWards = new ProductQuantity();
            OnWards = new ProductQuantity();
            ClosingBalance = new ProductQuantity();
            SKUStockList = new List<ProductQuantity>();
        }
    }

    public class ProductQuantity
    {
        public bool? IsInword { get; set; }
        public bool? IsReturn { get; set; }
        public PurchaseInvoice PurchaseInvoice { get; set; }
        public SaleInvoice SaleInvoice { get; set; }
        public JobSheet JobSheet { get; set; }
        public decimal Count { get; set; }
        public decimal Rate { get; set; }
        public decimal Value { get; set; }
        public decimal MRP { get; set; }
    }


}
