﻿using DataLayer.Models;
using PagedList;

namespace DataLayer.ViewModels
{
    public class InternalTransferViewModel
    {
        public IPagedList<InternalTransfer> InternalTransferList { get; set; }
    }
}
