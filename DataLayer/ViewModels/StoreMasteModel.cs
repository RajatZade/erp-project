﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class StoreMasteModel
    {
        public StoreMaste Store { get; set; }
        public List<StoreMaste> StoreLists { get; set; }
    }
}
