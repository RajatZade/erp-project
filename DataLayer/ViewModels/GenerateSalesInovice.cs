﻿using System.Collections.Generic;
using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class GenerateSalesInovice
    {
        public SaleInvoice SalesInvoice { get; set; }
        public Company Company { get; set; }
        public Contact Contact { get; set; }
        public Contact Salesman { get; set; }
        public Branch Branch { get; set; }
        public Ledger Ledger { get; set; }
        public Ledger BankLedger { get; set; }
        public PaymentType PaymentType { get; set; }
        //public Shipping Shipping { get; set; }
        public List<POS_GST_LIST> GstList { get; set; }
        public decimal TotalBasePrice { get; set; }
        public string SaleOrderNumber { get; set; }
        public string SaleType { get; set; }
        public string SalesMan { get; set; }
    }

    public class POS_GST_LIST
    {
        public int GSTId { get; set; }
        public GST GST { get; set; }
        public decimal? Amount { get; set; }
        public decimal? CGST { get; set; }
        public decimal? IGST { get; set; }
        public decimal? SGST { get; set; }
        public string Status { get; set; }
    }
}
