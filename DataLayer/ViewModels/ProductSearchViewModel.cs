﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DataLayer.ViewModels
{
    public class ProductSearchViewModel
    {
        public int CategoryId { get; set; }
        public int SubCategoryId { get; set; }
        public int SKUMasterId { get; set; }
        public string SearchProduct { get; set; }
        public string SKU { get; set; }
        public string SearchType { get; set; }
        public SKUMasterList SKUMasterList { get; set; }
        public List<SelectListItem> CategoryList { get; set; }
        public List<SelectListItem> SubcategoryList { get; set; }
        public List<SelectListItem> SKUList { get; set; }
        public List<SelectListItem> SearchTypeList { get; set; }
        public LedgerReportViewModel LedgerReportViewModel { get; set; }
        public ProductSearchViewModel()
        {
            CategoryList = new List<SelectListItem>();
            SubcategoryList = new List<SelectListItem>();
            SKUList = new List<SelectListItem>();
            LedgerReportViewModel = new LedgerReportViewModel();
            SearchTypeList = new List<SelectListItem>
            {
                new SelectListItem() { Text = "--Select One--", Value = "-1" },
                new SelectListItem() { Text = "Product", Value = "Product" },
                new SelectListItem() { Text = "Ledger", Value = "Ledger" },
                new SelectListItem() { Text = "Entries", Value = "Entries" },
            };
        }
    }
}
