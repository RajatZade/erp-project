﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.ViewModels
{
    public class TradingReportViewModel
    {
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? StartingDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? LastDate { get; set; }

        public List<TradingReportItem> TradingReportItemList { get; set; }

        public Company Company { get; set; }
        public Branch Branch { get; set; }

        public decimal TotalSalesAmmount { get; set; }
        public decimal TotalPurchaseAmmount { get; set; }
        public decimal TotalProfiteLoss { get; set; }

        public TradingReportViewModel()
        {
            TradingReportItemList = new List<TradingReportItem>();
        }
    }


    public class TradingReportItem
    {
        public SaleInvoice SaleInvoice { get; set; }
        public decimal SalesAmmount { get; set; }
        public decimal PurchaseAmmount { get; set; }
        public decimal ProfiteLoss { get; set; }
    }
}
