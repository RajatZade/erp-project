﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class GSTViewModel
    {
        public GST GST { get; set; }
        public List<GST> GSTvalueList { get; set; }
    }
}
