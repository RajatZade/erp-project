﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.ViewModels
{
    public class CashFlowReportViewModel
    {
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime StartingDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime LastDate { get; set; }

        public Company Company { get; set; }
        public Branch Branch { get; set; }

        public decimal TotalInFlow { get; set; }
        public decimal TotalOutFlow { get; set; }
        public decimal TotalNetFlow { get; set; }

        public List<MonthlyCashMovement> MonthlyCashMovementList { get; set; }
        public List<AccountCategoryList> AccountCategoryList { get; set; }
    }

    public class MonthlyCashMovement
    {
        public string MonthString { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public decimal InFlow { get; set; }
        public decimal OutFlow { get; set; }
        public decimal NetFlow { get; set; }
    }

    public class AccountCategoryList
    {
        public string DivId { get; set; }
        public decimal Total { get; set; }
        public bool IsInFlow { get; set; }
        public AccountCategory AccountCategory { get; set; }
        public List<AccountSubCategoryList> AccountSubCategoryList { get; set; }

        public AccountCategoryList()
        {
            AccountSubCategoryList = new List<AccountSubCategoryList>();
        }
    }

    public class AccountSubCategoryList
    {
        public string DivId { get; set; }
        public decimal Total { get; set; }
        public AccountSubCategory AccountSubCategory { get; set; }
        public List<LedgerList> LedgerList { get; set; }

        public AccountSubCategoryList()
        {
            LedgerList = new List<LedgerList>();
        }
    }

    public class LedgerList
    {
        public string DivId { get; set; }
        public decimal Total { get; set; }
        public Ledger Ledger { get; set; }
        public List<InvoiceList> InvoiceList { get; set; }
        public LedgerList()
        {
            InvoiceList = new List<InvoiceList>();
        }
    }


    public class InvoiceList
    {
        public int InvoiceId { get; set; }
        public string Type { get; set; }
        public string InvoiceNumber { get; set; }
        public decimal Total { get; set; }
    }
}
