﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;
using PagedList;

namespace DataLayer.ViewModels
{
    public class PurchaseOrderViewModel
    {
        public PurchaseOrder PurchaseOrder { get; set; }
        public List<PurchaseOrder> PurchaseOrderList { get; set; }
        public IPagedList<PurchaseOrder> PurchaseOrderLists { get; set; }
    }
}
