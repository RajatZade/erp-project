﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class BranchViewModel
    {
        public Branch Branch { get; set; }
        public List<Branch> BranchList { get; set; }
    }
}
