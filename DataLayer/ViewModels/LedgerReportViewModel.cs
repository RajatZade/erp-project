﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class LedgerReportViewModel
    {
        public Ledger Ledger { get; set; }
        public List<Voucher> VoucherList { get; set; }
        public List<Voucher> VoucherList2 { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? StartingDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? LastDate { get; set; }

        public decimal TotalDebitAmmount { get; set; }
        public decimal TotalCreditAmmount { get; set; }

        public decimal LedgerOpeningBalanceForVoucher { get; set; }
        public decimal LedgerClosingBalanceForVoucher { get; set; }

        public string Type { get; set; }

        public Company Company { get; set; }

        public Branch Branch { get; set; }

        public int LedgerId { get; set; }
        public string LedgerName { get; set; }
    }
}
