﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class MeasurmentViewModel
    {
        public Measurment Measurment { get; set; }
        public List<Measurment> MeasurmentList { get; set; }
    }
}
