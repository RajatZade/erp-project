﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.ViewModels
{
    public class PurchaseReportViewModel
    {
        public List<PurchaseReportList> PurchaseReportList { get; set; }

        public Company Company { get; set; }

        public Branch Branch { get; set; }

        public decimal TotalTax { get; set; }

        public decimal Subtotal { get; set; }

        public decimal TotalDiscount { get; set; }

        public decimal GrandTotal { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? FromDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? ToDate { get; set; }

        public Enums.PurchaseReportType PurchaseReportType { get; set; }

        public Enums.SaleReportType SaleReportType { get; set; }
        
        public PurchaseReportViewModel()
        {
            PurchaseReportList = new List<PurchaseReportList>();
        }
    }

    public class PurchaseReportList
    {
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? EntryDate { get; set; }

        public string LedgerName { get; set; }

        public int NoOfItem { get; set; }
        public decimal GrossAmount { get; set; }

        public decimal TotalTax { get; set; }

        public decimal Adjustment { get; set; }

        public decimal TotalWithAdjustment { get; set; }

        public string Type { get; set; }

        public string InvoiceNumber { get; set; }

        public int InvoiceId { get; set; }
    }
}
