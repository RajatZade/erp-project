﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class LoginViewModel
    {
        public int CustomerId { get; set; }
        public bool IsOTPAction { get; set; }
        public string OTP { get; set; }

        [Required]
        [Display(Name = "Email/Phone")]
        public string Email { get; set; }
        public string EnteredOTP { get; set; }
        public string MobileNo { get; set; }
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
        public LoginUser LoginUser { get; set; }
        public Division Division { get; set; }
        public Company Company { get; set; }
        public Branch Branch { get; set; }

    }
}
