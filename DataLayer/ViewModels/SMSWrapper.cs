﻿using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class SMSWrapper
    {
        public LoginUser LoginUser { get; set; }
        public Contact Contact { get; set; }
        public SaleInvoice SaleInvoice { get; set; }
        public string TriggerPoint { get; set; }
        public string Template { get; set; }
        public string Subject { get; set; }
        public string TypeOfOperation { get; set; }
        public string Self_No { get; set; }
        public string body { get; set; }
    }
}
