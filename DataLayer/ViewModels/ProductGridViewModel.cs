﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class ProductGridViewModel
    {
        public List<ProductGridListViewModel> ProductGrid = new List<ProductGridListViewModel>();
        public List<SKUMaster> SKUIds = new List<SKUMaster>();
    }


    public class ProductGridListViewModel
    {
        public bool checkAllProduct { get; set; }
        public decimal PurchaseQuantity { get; set; }
        public List<Product> ProductList { get; set; }
        public string SKU { get; set; }
        public int SKUId { get; set; }
        public decimal Quantity { get; set; }
        public decimal AvailableStockCount { get; set; }
    }
}
