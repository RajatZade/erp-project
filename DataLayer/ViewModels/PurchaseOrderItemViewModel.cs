﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class PurchaseOrderItemViewModel
    {
        public PurchaseOrderItem PurchaseOrderItem { get; set; }
        public Discount Discount { get; set; }
    }
}
