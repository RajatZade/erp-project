﻿using System.Collections.Generic;
using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class UnitMasterViewModel
    {
        public Unit unitmaster { get; set; }
        public List<Unit> UnitMasterList { get; set; }

        //public UnitConversion UnitConversion { get; set; }
        //public List<UnitConversion> UnitConversionList { get; set; }
    }
}
