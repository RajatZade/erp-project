﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.ViewModels
{
    public class WSPPrintViewModel
    {
        public SaleInvoice SalesInvoice { get; set; }
        public Company Company { get; set; }
        public Contact Contact { get; set; }
        public Contact Salesman { get; set; }
        public Branch Branch { get; set; }
        public Ledger Ledger { get; set; }
        public Ledger BankLedger { get; set; }
        public PaymentType PaymentType { get; set; }
        //public Shipping Shipping { get; set; }
        public List<POS_GST_LIST> GstList { get; set; }
        public List<WSP_SKU_LIST> SKUList { get; set; }
        public decimal TotalBasePrice { get; set; }
        public string SaleOrderNumber { get; set; }
        public string SaleType { get; set; }
        public string SalesMan { get; set; }
    }

    public class WSP_SKU_LIST
    {
        public string SKU { get; set; }
        public int SKUId { get; set; }
        public decimal TotalQuantity { get; set; }
        public Product Product { get; set; }
        public GST GST { get; set; }
        public decimal WSP { get; set; }
        public decimal SaleDiscountPercent { get; set; }
        public decimal SGST { get; set; }
        public decimal CGST { get; set; }
        public decimal IGST { get; set; }
        public decimal BasicPrice { get; set; }                          
    }
}
