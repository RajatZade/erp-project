﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Drawing;
using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class BarcodeViewModel
    {
        public int PurchaseInvoiceItemId;
        public decimal MRP;
        public decimal DiscountedAmount;
        internal int GSTId;
        internal int? AttributeLinkingId;
        internal List<string> AttributeValue;
        public string Barcode { get; set; }
        public string SerialNumber { get; set; }
        public string SKU { get; set; }
        public decimal GSTPercent { get; set; }
        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }
        public Company Company { get; set; }
        public GST GST { get; set; }
        public AttributeLinking AttributeLinking { get; set; }
        public List<string> AttributeList { get; set; }
        public List<string> Attribute { get; set; }
        public SKUMaster SKUMaster { get; set; }
        public int SKUMasterId { get; set; }
    }
}
