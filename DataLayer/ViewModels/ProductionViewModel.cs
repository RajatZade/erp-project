﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;
using PagedList;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.ViewModels
{
    public class ProductionViewModel
    {
        public Company Company { get; set; }
        public Branch Branch { get; set; }
        public List<JobSheet> JobSheetList { get; set; }
        public IPagedList<JobSheet> JobSheetLists { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? FromDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? ToDate { get; set; }
    }
}
