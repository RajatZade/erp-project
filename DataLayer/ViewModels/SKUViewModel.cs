﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class SKUViewModel
    {
        public SKUMaster SKUMaster { get; set; }
        public List<SKUMaster> SKUList { get; set; }

    }
}
