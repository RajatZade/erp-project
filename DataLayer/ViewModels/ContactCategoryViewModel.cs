﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class ContactCategoryViewModel
    {
        public AccountCategory ContactCategory { get; set; }
        public AccountSubCategory ContactSubCategory { get; set; }
        public List<AccountCategory> ContactCategoryList { get; set; }
        public List<AccountSubCategory> ContactSubCategoryList { get; set; } 
    }
}
