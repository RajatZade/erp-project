﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class LocationViewModel
    {
        public Location Location { get; set; }
        public List<Location> LocationList { get; set; }

    }
}
