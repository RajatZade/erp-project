﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class PaymentTypeViewModel
    {
        public PaymentType PaymentType { get; set; }
        public List<PaymentType> PaymentTypeList { get; set; }
    }
}
