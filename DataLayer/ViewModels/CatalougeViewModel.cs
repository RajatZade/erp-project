﻿using DataLayer.Models;
using System.Collections.Generic;

namespace DataLayer.ViewModels
{
    public class CatalougeViewModel
    {
        public Category Category { get; set; }
        public List<Category> CategoryLists { get; set; }
        public SubCategory SubCategory { get; set; }
        public List<SubCategory> SubCategoryList { get; set; }
    }
}
