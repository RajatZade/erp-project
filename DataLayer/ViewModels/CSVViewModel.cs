﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace DataLayer.ViewModels
{
    public class CSVViewModel
    {
        public string TableNam { get; set; }
        public List<SelectListItem> TableList { get; set; }
        [NotMapped]
        public HttpPostedFileBase File { get; set; }
    }
    public class DataTable1
    {
        public string ColumnName { get; set; }
        public Type type { get; set; }
    }

    public class PageModel
    {
        public Collection<DataTable1> DataTable1s { get; set; }
    }
}

