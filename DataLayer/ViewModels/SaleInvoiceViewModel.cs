﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;
using PagedList;

namespace DataLayer.ViewModels
{
    public class SaleInvoiceViewModel
    {
        public SaleInvoice SaleInvoice { get; set; }
        public string Search { get; set; }
        public List<SaleInvoice> SaleInvoiceList { get; set; }
        public IPagedList<SaleInvoice> SaleInvoiceLists { get; set; }

    }
}
