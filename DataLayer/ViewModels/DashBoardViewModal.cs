﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.ViewModels
{
    public class DashBoardViewModal
    {
        public int SaleInvoiceCount { get; set; }
        public int SaleOrderCount { get; set; }
        public int ReceiptCount { get; set; }
        public int ProductionCount { get; set; }
    }
}
