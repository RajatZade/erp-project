﻿using System.Collections.Generic;
using DataLayer.Models;

namespace DataLayer.ViewModels
{
    public class ProductViewModel
    {
        public Product Product { get; set; }

        public PurchaseInvoiceItem PurchaseInvoiceItem { get; set; }

        public List<PurchaseInvoiceItem> PurchaseInvoiceItemList { get; set; }

        public List<Product> ProductList { get; set; }

        public Discount Discount { get; set; }
    }
}
