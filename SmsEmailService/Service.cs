﻿using System;
using System.IO;
using System.Web;
using System.Net;
using System.Net.Mail;
using DataLayer.Models;
using DataLayer;
using DataLayer.ViewModels;
using DataLayer.Utils;

namespace SmsEmailService
{
    public class Service
    {
        DbConnector db = new DbConnector();
        SMSEMailDbUtil smsdb = new SMSEMailDbUtil();
        public int SendEmailSMS(SMSWrapper smsWrapper)
        {
            //query SMSEMAILCONFIGURATION
            int id = -1;
            string body = string.Empty;
            //var replyTemplate = new TemplateService();
            var templateFolderPath1 = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Constants.TEMPLATE_PATH);
            var templateFilePath1 = Path.Combine(templateFolderPath1, smsWrapper.Template);

            SmsQueue smsQueue = new SmsQueue
            {
                Body = body,
                SendDate = DateTime.Now
            };
            if (smsWrapper.TypeOfOperation == Constants.OPERATION_TYPE_SMS)
            {
                smsQueue.PhoneNo = smsWrapper.Contact.Mobile;
                smsQueue.SmsType = Enums.SmsType.SMS;
                smsQueue = SendSms(smsQueue);
                id = smsdb.SaveSmsQueue(smsQueue);
            }
            if (smsWrapper.TypeOfOperation == Constants.OPERATION_TYPE_SMS_SELF)
            {
                smsQueue.PhoneNo = smsWrapper.Self_No;
                smsQueue.SmsType = Enums.SmsType.SMS;
                smsQueue = SendSms(smsQueue);
                id = smsdb.SaveSmsQueue(smsQueue);
            }
            if (smsWrapper.TypeOfOperation == Constants.OPERATION_TYPE_EMAIL)
            {
                smsQueue.SmsType = Enums.SmsType.Email;
                smsQueue.To = smsWrapper.Contact.Email;
                smsQueue.Subject = smsWrapper.Subject;
                smsQueue.IsBodyHtml = true;
                id = SendEmail(smsQueue);
                if (id > 0)
                {
                    smsQueue.IsProcess = true;
                }
                else
                {
                    smsQueue.IsProcess = false;
                }

                id = smsdb.SaveSmsQueue(smsQueue);
            }
            return id;
        }

        public SmsQueue SendSms(SmsQueue item)
        {
            SMSConfig SmsDetails = smsdb.GetSmsDetails();
            try
            {
                //Your authentication key
                string authKey = SmsDetails.AuthKey;
                //Multiple mobiles numbers separated by comma
                string mobileNumber = item.PhoneNo.ToString();
                //Sender ID,While using route4 sender id should be 6 characters long.
                string senderId = SmsDetails.SenderId;
                //Your message to send, Add URL encoding here.
                string message = HttpUtility.UrlEncode(item.Body);
                string sendSMSUri = SmsDetails.SendSMSUri;
                string _createURL = sendSMSUri + "authkey=" + authKey + "&mobiles=" + mobileNumber + "&message=" + message + "&sender=" + senderId + "&route=" + SmsDetails.Route;
                //Create HTTPWebrequest
                WebRequest httpWReq = WebRequest.Create(_createURL);
                WebResponse response = httpWReq.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                item.Response = reader.ReadToEnd();
                //Close the response
                reader.Close();
                response.Close();
                item.IsProcess = true;

                //id = db.EditSmsQueue(item);
            }
            catch (Exception ex)
            {
                item.IsProcess = false;
                item.Response = ex.StackTrace;
                // db.EditSmsQueue(item);
                db.RegisterException(ex);
            }
            return item;
        }
        public int SendEmail(SmsQueue item)
        {
            int id = -1;
            EmailConfig EmailDeatils = smsdb.GetEmailDetails();
            //try
            //{
            SmtpClient mail = new SmtpClient
            {
                EnableSsl = false,//EmailDeatils.EnableSsl;
                Host = EmailDeatils.Host,
                Port = EmailDeatils.Port,
                UseDefaultCredentials = EmailDeatils.UsedDefaultCredentials,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = new NetworkCredential(EmailDeatils.UserEmailId, EmailDeatils.Password)
            };
            MailMessage mm = new MailMessage();
            if (!string.IsNullOrEmpty(item.To))
            {
                mm.To.Add(new MailAddress(EmailDeatils.UserEmailId));
            }
            if (!string.IsNullOrEmpty(item.CC))
            {
                mm.CC.Add(new MailAddress(item.CC));
            }
            if (!string.IsNullOrEmpty(item.Bcc))
            {
                mm.Bcc.Add(new MailAddress(item.Bcc));
            }
            mm.From = new MailAddress(EmailDeatils.UserEmailId);

            //mm.Body = string.Format("This is {0},I would like to have a word with you for further processing.{1},{2},{3},{4},{5},{6}", contactInfo.Name, contactInfo.Organization, contactInfo.Subject, contactInfo.City, contactInfo.Email, contactInfo.Contact, contactInfo.Message);

            mm.IsBodyHtml = item.IsBodyHtml; //Or false if it isn't a HTML email.
            mm.Subject = item.Subject;
            mm.Body = item.Body;
            mail.Send(mm);
            item.IsProcess = true;
            item.Response = MessageStore.ResponseMessage;
            id = 1;// db.EditSmsQueue(item);
            //}
            //catch (Exception ex)
            //{
            //    item.IsProcess = false;
            //    item.Response = ex.StackTrace;
            //    id = db.EditSmsQueue(item);
            //    db.RegisterException(ex);
            //}
            return id;
        }
    }
}
