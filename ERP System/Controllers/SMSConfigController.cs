﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using DataLayer;
using DataLayer.Models;
using ERP_System.Utils;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class SMSConfigController : Controller
    {
        DbConnector db = new DbConnector();
        SMSEMailDbUtil smsdb = new SMSEMailDbUtil();
        // GET: SmsConfigs
        public ActionResult Index()
        {
            return View(db.SmsConfigs.ToList());
        }

        // GET: SmsConfigs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SMSConfig smsConfig = db.SmsConfigs.Find(id);
            if (smsConfig == null)
            {
                return HttpNotFound();
            }
            return View(smsConfig);
        }

        // GET: SmsConfigs/Create
        public ActionResult Create(int? id)
       {
            ViewData["Operation"] = id;
            ViewData["users"] = smsdb.GetSmsConfig();
            SMSConfig sms = smsdb.GetSmsConfigById(id);
            return View(sms);
        }

        // POST: SmsConfigs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        // [ValidateAntiForgeryToken]
        public ActionResult Create(SMSConfig smsConfig)
        {
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (smsConfig.Name != null)
            {
                if (smsConfig.SmsConfigId == 0)
                {
                    if (smsdb.SaveSmsConfig(smsConfig) < 0)
                    {

                    }
                }
                else
                {
                    if (smsdb.EditSmsConfig(smsConfig) < 0)
                    {

                    }
                }
            }
            return RedirectToAction("Create", new { id = 0 });
        }

        // GET: SmsConfigs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SMSConfig smsConfig = db.SmsConfigs.Find(id);
            if (smsConfig == null)
            {
                return HttpNotFound();
            }
            return View(smsConfig);
        }

        // POST: SmsConfigs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SmsConfigId,Name,AuthKey,SenderId,SendSMSUri,Route,IsDefault")] SMSConfig smsConfig)
        {
            if (ModelState.IsValid)
            {
                db.Entry(smsConfig).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(smsConfig);
        }

        // GET: SmsConfigs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SMSConfig smsConfig = db.SmsConfigs.Find(id);
            if (smsConfig == null)
            {
                return HttpNotFound();
            }
            if (smsdb.DelteSmsConfig(smsConfig) < 0)
            {

            }
            return RedirectToAction("Create");
        }

        // POST: SmsConfigs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SMSConfig smsConfig = db.SmsConfigs.Find(id);
            db.SmsConfigs.Remove(smsConfig);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}