﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DataLayer.Models;
using DataLayer.ViewModels;
using ERP_System.Utils;
using DataLayer.Utils;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class PurchaseReturnLedgerReportController : Controller
    {
        // GET: PurchaseReport
        DbConnector db = new DbConnector();

        [RBAC(AccessType = 2)]
        public ActionResult Index()
        {
            PurchaseReportViewModel model = new PurchaseReportViewModel();
            model.PurchaseReportType = Enums.PurchaseReportType.All;
            Ledger PurchaseLedger = db.Ledgers.Where(x => x.Name == Constants.LEDGER_TYPE_PURCHASE_RETURN_LEDGER).FirstOrDefault();
            if (PurchaseLedger != null)
            {
                List<Voucher> VoucherList = db.Vouchers.Where(x => x.LedgerId == PurchaseLedger.LedgerId && x.VoucherFor !=  Constants.OPENING_BALANCE && x.CompanyId == db.CompanyId).OrderBy(x => x.EntryDate).ToList();
                model = GetReport(VoucherList);
            }            
            return View(model);

        }


        [HttpPost]
        public ActionResult _InvoiceList(PurchaseReportViewModel ViewModel)
        {

            Ledger PurchaseLedger = db.Ledgers.Where(x => x.Name == Constants.LEDGER_TYPE_PURCHASE_RETURN_LEDGER).FirstOrDefault();
            if (PurchaseLedger != null)
            {
                List<Voucher> VoucherList = null;

                if(ViewModel.PurchaseReportType == Enums.PurchaseReportType.All)
                {
                    VoucherList = db.Vouchers.Where(x => x.LedgerId == PurchaseLedger.LedgerId && x.VoucherFor != Constants.OPENING_BALANCE && x.CompanyId == db.CompanyId).ToList();
                }
                else if (ViewModel.PurchaseReportType == Enums.PurchaseReportType.PurchaseBook)
                {
                    VoucherList = db.Vouchers.Where(x => x.LedgerId == PurchaseLedger.LedgerId && (x.VoucherFor == Constants.PURCHASE_TYPE_PURCHASE_INVOICE || x.VoucherFor == Constants.PURCHASE_TYPE_PURCHASE_VOUCHER) && x.VoucherFor != Constants.OPENING_BALANCE && x.CompanyId == db.CompanyId).ToList();
                }
                else if (ViewModel.PurchaseReportType == Enums.PurchaseReportType.PurchaseReturn)
                {
                    VoucherList = db.Vouchers.Where(x => x.LedgerId == PurchaseLedger.LedgerId && (x.VoucherFor == Constants.SALE_TYPE_PURCHASE_RETURN || x.VoucherFor == Constants.JOURNAL_ENTRY) && x.VoucherFor != Constants.OPENING_BALANCE && x.CompanyId == db.CompanyId).ToList();
                }
                    
                if (ViewModel.FromDate.HasValue)
                {
                    VoucherList = VoucherList.Where(x => x.EntryDate >= ViewModel.FromDate).OrderBy(x => x.EntryDate).ToList();
                }

                if (ViewModel.ToDate.HasValue)
                {
                    VoucherList = VoucherList.Where(x => x.EntryDate <= ViewModel.ToDate).OrderBy(x => x.EntryDate).ToList();
                }
                ViewModel = GetReport(VoucherList);
            }
            return PartialView(ViewModel);
        }

        public PurchaseReportViewModel GetReport(List<Voucher> VoucherList)
        {
            PurchaseReportViewModel model = new PurchaseReportViewModel();
            model.Company = SessionManager.GetSessionCompany();
            model.Branch = SessionManager.GetSessionBranch();
            List<int?> PurchaseInvoiceIds = VoucherList.Select(x => x.PurchaseInvoiceId).ToList();
            List<int?> SaleInvoiceIds = VoucherList.Select(x => x.SaleInvoiceId).ToList();
            List<PurchaseInvoice> PurchaseInvoiceList = db.PurchaseInvoices.Where(x => PurchaseInvoiceIds.Contains(x.PurchaseInvoiceId)).ToList();
            List<SaleInvoice> SaleInvoiceList = db.SaleInvoices.Where(x => SaleInvoiceIds.Contains(x.SaleInvoiceId)).ToList();
            foreach(var item in VoucherList)
            {
                PurchaseReportList ReportItem = new PurchaseReportList();
                if(item.VoucherFor == Constants.PURCHASE_TYPE_PURCHASE_INVOICE || item.VoucherFor == Constants.PURCHASE_TYPE_PURCHASE_VOUCHER)
                {
                    PurchaseInvoice PurchaseInvoice = PurchaseInvoiceList.Where(x => x.PurchaseInvoiceId == item.PurchaseInvoiceId).FirstOrDefault();
                    if (PurchaseInvoice != null)
                    {
                        if (item.VoucherFor == Constants.PURCHASE_TYPE_PURCHASE_INVOICE)
                        {
                            ReportItem.NoOfItem = db.PurchaseInvoiceItems.Where(x => x.PurchaseInvoiceId == item.PurchaseInvoiceId).Count();
                        }
                        else
                        {
                            ReportItem.NoOfItem = db.ProductVouchers.Where(x => x.PurchaseInvoiceId == item.PurchaseInvoiceId).Count();
                        }                            
                        ReportItem.InvoiceId = item.PurchaseInvoiceId ?? 0;
                        ReportItem.Type = item.VoucherFor;
                        ReportItem.EntryDate = item.EntryDate;
                        ReportItem.InvoiceNumber = PurchaseInvoice.PurchaseInvoiceNo;
                        ReportItem.GrossAmount = PurchaseInvoice.GrossAmount;
                        ReportItem.Adjustment =  PurchaseInvoice.Adjustment ?? 0;
                        ReportItem.TotalTax = PurchaseInvoice.TotalTax ?? 0;
                        ReportItem.TotalWithAdjustment = (PurchaseInvoice.TotalInvoiceValue ?? 0) + (PurchaseInvoice.Adjustment ?? 0);
                        model.PurchaseReportList.Add(ReportItem);
                    }
                }
                else if (item.VoucherFor == Constants.SALE_TYPE_PURCHASE_RETURN)
                {
                    SaleInvoice SaleInvoice = SaleInvoiceList.Where(x => x.SaleInvoiceId == item.SaleInvoiceId).FirstOrDefault();
                    if (SaleInvoice != null)
                    {
                        ReportItem.NoOfItem = db.SaleInvoiceItems.Where(x => x.SaleInvoiceId == item.SaleInvoiceId).Count();
                        ReportItem.InvoiceId = item.SaleInvoiceId ?? 0;
                        ReportItem.Type = item.VoucherFor;
                        ReportItem.EntryDate = item.EntryDate;
                        ReportItem.InvoiceNumber = SaleInvoice.SaleInvoiceNo;
                        ReportItem.GrossAmount = -SaleInvoice.Subtotal;
                        ReportItem.Adjustment = -SaleInvoice.Adjustment ?? 0;
                        ReportItem.TotalTax = -SaleInvoice.TotalTax;
                        ReportItem.TotalWithAdjustment = -SaleInvoice.TotalWithAdjustment;
                        model.PurchaseReportList.Add(ReportItem);
                    }
                }
                else
                {
                    ReportItem.InvoiceNumber = item.VoucherNumber;
                    ReportItem.Type = item.VoucherFor;
                    ReportItem.EntryDate = item.EntryDate;
                    ReportItem.GrossAmount = -item.Total;
                    ReportItem.Adjustment = 0;
                    ReportItem.TotalTax = 0;
                    ReportItem.TotalWithAdjustment = -item.Total;
                    model.PurchaseReportList.Add(ReportItem);
                }
            }
            model.Subtotal = model.PurchaseReportList.Sum(x => x.GrossAmount);
            model.TotalTax = model.PurchaseReportList.Sum(x => x.TotalTax);
            model.TotalDiscount = model.PurchaseReportList.Sum(x => x.Adjustment);
            model.GrandTotal = model.PurchaseReportList.Sum(x => x.TotalWithAdjustment);
            return model;
        }
    }
}