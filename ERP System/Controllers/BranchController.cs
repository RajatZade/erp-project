﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer.Models;
using DataLayer;
using DataLayer.ViewModels;
using DataLayer.Utils;
using ERP_System.Utils;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class BranchController : Controller
    {
        DbConnector db = new DbConnector();
        // GET: Branch
        [RBAC(AccessType = 2)]
        public ActionResult Index(int? id,string ErrorMsg)
        {
            BranchViewModel model = new BranchViewModel();
            if (model.BranchList == null)
            {
                model.BranchList = db.GetBranchListByCompany(db.CompanyId);
            }
            if (id.HasValue)
            {
                model.Branch = db.GetBranchListById(id.Value);
            }
            else
            {
                model.Branch = new Branch();
            }
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            model.Branch.CompanyId = db.CompanyId ?? 0;
            model.Branch.DivisionList = DropdownManager.GetDivisionListByCompany(db.CompanyId);
            return View(model);
        }

        [RBAC(AccessType = 0)]
        [HttpPost]
        public ActionResult Create(Branch branch)
        {
            db.SaveBranch(branch);
            return RedirectToAction("Index");
        }

        [RBAC(AccessType = 3)]
        public ActionResult Delete(int id)
        {
            Branch branch = db.GetBranchListById(id);
           
           
            if (branch == null)
            {
                return HttpNotFound();
            }
            else
            {
                if (db.RemoveBranch(branch))
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index", new { ErrorMsg = MessageStore.DeleteError });
        }
    }
}