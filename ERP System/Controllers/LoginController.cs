﻿using System.Web.Mvc;
using DataLayer.Models;
using DataLayer.ViewModels;
using DataLayer;
using DataLayer.Utils;
using System.Linq;
using System;
//using SmsEmailService;
using ERP_System.Utils;
using System.Collections.Generic;
//using SmsEmailService;

namespace ERP_System.Controllers
{
    
    public class LoginController : Controller
    {        
        DbConnector db = new DbConnector();
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login(string Error)
        {
            if (SessionManager.GetSessionUser() != null)
            {
                if (SessionManager.GetSessionCompany() == null)
                {
                    return RedirectToAction("SetCompany", "Login");
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            if (!string.IsNullOrEmpty(Error))
            {
                ViewBag.Error = Error;
            }
            LoginViewModel model = new LoginViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel user)
        {
            LoginUser loggedInUser = db.IsValidUser(user.LoginUser);
            if (loggedInUser != null)
            {
                Session[Constants.SESSION_USER] = loggedInUser;
                return RedirectToAction("SetCompany");
            }
            LoginViewModel model = new LoginViewModel();
            model.LoginUser = user.LoginUser;
            ViewBag.ErrorMsg = "Invalid User Name or Password";
            return View(model);
        }


        public ActionResult LogOut()
        {
            SessionManager.AbandonSession();
            Session[Constants.SESSION_USER] = null;
            return RedirectToAction("Login");
        }

        public ActionResult SetCompany()
        {
            NavBarViewModel Model = new NavBarViewModel();
            Model.User = SessionManager.GetSessionUser();
            Model.Division = new Division();
            Model.Branch = new Branch();
            Model.Division.CompanyList = DropdownManager.GetCompanyAccessList(Model.User.RoleId);
            Model.Division.DivisionList = DropdownManager.GetDivisionAccessList(Model.User.RoleId);
            Model.Branch.BranchList = DropdownManager.GetBranchAccessList(Model.User.RoleId);
            return View(Model);
        }

        [HttpPost]
        public JsonResult DivisionList(int CompanyId, int RoleId)
        {
            List<SelectListItem> DivisionList = DropdownManager.GetDivisionAccessListByCompany(RoleId, CompanyId);
            return Json(new SelectList(DivisionList, "Value", "Text"));
        }

        [HttpPost]
        public JsonResult BranchList(int DivisionId, int RoleId)
        {
            List<SelectListItem> BranchList = DropdownManager.GetBranchAccessListByDivision(RoleId, DivisionId);
            return Json(new SelectList(BranchList, "Value", "Text"));
        }

        [HttpPost]
        public ActionResult SetSessionValue(NavBarViewModel Model)
        {
            NavBarViewModel TextModel = new NavBarViewModel();
            TextModel.Division = db.GetDivisionListById(Model.Division.DivisionId);
            TextModel.Company = db.GetCompanyById(Model.Company.CompanyId);
            TextModel.Branch = db.GetBranchListById(Model.Branch.BranchId);
            SessionManager.SetSessionBranch(TextModel.Branch);
            SessionManager.SetSessionCompany(TextModel.Company);
            SessionManager.SetSessionDivision(TextModel.Division);
            return RedirectToAction("Index", "Home");
        }

        public ActionResult ChangeSessionCompany()
        {
            SessionManager.EmptySessionCompany();
            return RedirectToAction("SetCompany");
        }

        [HttpPost]
        public JsonResult ForgetPassword(string username)
        {
            db.Configuration.ProxyCreationEnabled = false;
            //Service _service = new Service();
            LoginUser user = db.LoginUsers.Include("Contact").Where(x => x.Username == username).FirstOrDefault();
            if(user != null)
            {
                if (user.Contact != null)
                { 
                    SMSWrapper smsWrapper = new SMSWrapper();
                    smsWrapper.Contact = user.Contact;
                    smsWrapper.LoginUser = user;
                    smsWrapper.TypeOfOperation = Constants.OPERATION_TYPE_SMS;
                    smsWrapper.Template = "ForgetPassword.cshtml";
                    //int SmsValue = _service.SendEmailSMS(smsWrapper);
                    string data = "Password Send To Mobile Number";
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string data = "User Not Found For Given UserName";
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                string data = "User Not Found For Given UserName";
                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }

        
        public ActionResult ChangePassword()
        {
            db.Configuration.ProxyCreationEnabled = false;
            LoginViewModel model = new LoginViewModel();
            model.LoginUser = SessionManager.GetSessionUser();
            model.LoginUser.Password = "";
            model.Name = model.LoginUser.Username;
            return View(model);
        }

        [HttpPost]
        public JsonResult CheckPassword(string UserName,string Password)
        {
            LoginUser user = db.LoginUsers.Where(x => x.Username == UserName && x.Password == Password).FirstOrDefault();
            if(user == null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ChangePassword(LoginViewModel model)
        {
            db.Configuration.ProxyCreationEnabled = false;
            LoginUser user = db.LoginUsers.Where(x => x.Username == model.Name).FirstOrDefault();
            user.Password = model.LoginUser.Password;
            db.UpdateLoginUser(user);
            model.Name = model.LoginUser.Username;
            SessionManager.AbandonSession();
            Session[Constants.SESSION_USER] = null;
            string Error = "Password Updated";
            return RedirectToAction("Login", "Login",new { Error = Error });
        }
    }
}