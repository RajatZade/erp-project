﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DataLayer.Models;
using DataLayer.ViewModels;
using DataLayer.Utils;
using ERP_System.Utils;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class StoreMasteController : Controller
    {
        DbConnector db = new DbConnector();
        // GET: UnitMaster
        [RBAC(AccessType = 2)]
        public ActionResult Index(int? id,string  ErrorMsg)
        {
            StoreMasteModel model = new StoreMasteModel();
            if (model.StoreLists == null)
            {
                model.StoreLists = db.GetStoreListList();
            }
            if (id.HasValue)
            {
                model.Store = db.GetStoreById(id.Value);
            }
            else
            {
                model.Store = new StoreMaste();
            }
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            return View(model);
        }

        [RBAC(AccessType = 0)]
        [HttpPost]
        public ActionResult Create(StoreMaste store)
        {
            db.SaveStore(store);
            return RedirectToAction("Index");
        }

        [RBAC(AccessType = 3)]
        public ActionResult Delete(int id)
        {
            StoreMaste store = db.Stores.Find(id);
            db.DeleteStore(store);
            if (store == null)
            {
                return HttpNotFound();
            }
            else
            {
                if (db.DeleteStore(store))
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index", new { ErrorMsg = MessageStore.DeleteError });
        }
    }
}