﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DataLayer.Models;
using DataLayer.ViewModels;
using DataLayer.Utils;
using ERP_System.Utils;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class DiscountController : Controller
    {
        DbConnector db = new DbConnector();
        // GET: UnitMaster
        [RBAC(AccessType = 2)]
        public ActionResult Index(int? id,string ErrorMsg)
        {
            DiscountViewModel model = new DiscountViewModel();
            if (model.DiscountTypeList == null)
            {
                model.DiscountTypeList = db.GetDsicountList();
            }
            if (id.HasValue)
            {
                model.Discount = db.GetDiscountById(id.Value);
            }
            else
            {
                model.Discount = new Discount();
            }
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            return View(model);
        }

        [RBAC(AccessType = 0)]
        [HttpPost]
        public ActionResult Create(Discount discount)
        {
            db.SaveDiscount(discount);
            return RedirectToAction("Index");
        }

        [RBAC(AccessType = 3)]
        public ActionResult Delete(int id)
        {
            Discount Discount = db.Discounts.Find(id);
            if (Discount == null)
            {
                return HttpNotFound();
            }
            else
            {
                if (db.DeleteDiscount(Discount))
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index", new { ErrorMsg = MessageStore.DeleteError });
        }

        [HttpPost]
        public JsonResult ExistingDiscountCode(string DiscountCode)
        {
            var Discounts = db.Discounts.Where(x => x.DiscountCode == DiscountCode).FirstOrDefault();
            if (Discounts == null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ExistingPromoCode(string PromoCode)
        {
            var Discounts = db.Discounts.Where(x => x.PromoCode == PromoCode).FirstOrDefault();
            if (Discounts == null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}