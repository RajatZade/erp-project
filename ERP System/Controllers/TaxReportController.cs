﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DataLayer.Models;
using DataLayer.ViewModels;
using ERP_System.Utils;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class TaxReportController : Controller
    {
        // GET: TaxReport
        DbConnector db = new DbConnector();
        // GET: Report
        [RBAC(AccessType = 2)]
        public ActionResult Index()
        {
            ReportViewModel model = new ReportViewModel();
            model.PurchaseInvoiceList = db.PurchaseInvoices.Include("PurchaseInvoiceItemList").OrderBy(x => x.InvoicingDate).ToList();
            foreach (var item in model.PurchaseInvoiceList)
            {
                item.CGST = 0;
                item.IGST = 0;
                item.SGST = 0;
                foreach (var ITEM1 in item.PurchaseInvoiceItemList)
                {
                    item.CGST = item.CGST + (ITEM1.CGST ?? 0);
                    item.IGST = item.IGST + (ITEM1.IGST ?? 0);
                    item.SGST = item.SGST + (ITEM1.SGST ?? 0);
                }
                model.GrandTotal = model.GrandTotal + item.TotalInvoiceValue.Value;
                model.TotalCGST = model.TotalCGST + item.CGST;
                model.TotalSGST = model.TotalSGST + item.SGST;
                model.TotalIGST = model.TotalIGST + item.CGST;                
            }
            model.TotalTax = model.TotalCGST + model.TotalSGST + model.TotalIGST;
            return View(model);
        }

        public ActionResult Details(int PurchaseInvoiceId)
        {
            PurchaseInvoice model = db.PurchaseInvoices.Include("PurchaseInvoiceItemList").Where(x => x.PurchaseInvoiceId == PurchaseInvoiceId).FirstOrDefault();
            Dictionary<int, SKUMaster> idVsSKU = db.GetSKUMasterDictionary(model.PurchaseInvoiceItemList.Select(x => x.SKUMasterId).ToList());
            Dictionary<int, GST> idVsGST = db.GetGSTMasterDictionary(model.PurchaseInvoiceItemList.Select(x => x.GSTId).ToList());
            foreach (var item in model.PurchaseInvoiceItemList)
            {
                item.SKUMaster = idVsSKU[item.SKUMasterId];
                item.GST = idVsGST[item.GSTId];
                model.ItemTotalQuantity = model.ItemTotalQuantity + item.Quantity;
                model.ItemTotalBasicPrice = model.ItemTotalBasicPrice + item.Rate;
                model.ItemTotalMRP = model.ItemTotalMRP + item.MRP;                
                model.ItemPurchasePrice = model.ItemPurchasePrice + item.NetAmount.Value;
                model.ItemFinalTotal = model.ItemFinalTotal + (item.Quantity * item.NetAmount.Value);
                model.FinalTotalAdj = model.ItemFinalTotal;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult _InvoiceList(ReportViewModel model)
        {
            if (model.FromDate != null && model.ToDate != null && model.Search != null)
            {
                model.PurchaseInvoiceList = db.PurchaseInvoices.Include("PurchaseInvoiceItemList").Where(x => x.InvoicingDate >= model.FromDate && x.InvoicingDate <= model.ToDate && x.Account.Name == model.Search).Where(x => x.IsLedgerUpdate == true).OrderBy(x => x.InvoicingDate).ToList();
            }
            else if (model.FromDate != null && model.ToDate != null)
            {
                model.PurchaseInvoiceList = db.PurchaseInvoices.Include("PurchaseInvoiceItemList").Where(x => x.InvoicingDate >= model.FromDate && x.InvoicingDate <= model.ToDate).Where(x => x.IsLedgerUpdate == true).OrderBy(x => x.InvoicingDate).ToList();

            }
            else if (model.Search != null)
            {
                model.PurchaseInvoiceList = db.PurchaseInvoices.Include("PurchaseInvoiceItemList").Where(x => x.Account.Name == model.Search).Where(x => x.IsLedgerUpdate == true).OrderBy(x => x.InvoicingDate).ToList();
            }
            foreach (var item in model.PurchaseInvoiceList)
            {
                item.CGST = 0;
                item.IGST = 0;
                item.SGST = 0;
                foreach (var ITEM1 in item.PurchaseInvoiceItemList)
                {
                    item.CGST = item.CGST + (ITEM1.CGST ?? 0);
                    item.IGST = item.IGST + (ITEM1.IGST ?? 0);
                    item.SGST = item.SGST + (ITEM1.SGST ?? 0);
                }                
                model.GrandTotal = model.GrandTotal + item.TotalInvoiceValue.Value;
                model.TotalCGST = model.TotalCGST + item.CGST;
                model.TotalSGST = model.TotalSGST + item.SGST;
                model.TotalIGST = model.TotalIGST + item.CGST;
            }
            model.TotalTax = model.TotalCGST + model.TotalSGST + model.TotalIGST;
            return PartialView(model);
        }
    }
}