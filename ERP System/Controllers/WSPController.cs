﻿using DataLayer;
using DataLayer.Models;
using DataLayer.Utils;
using DataLayer.ViewModels;
using ERP_System.Utils;
using PaymentGateway;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class WSPController : BaseController
    {
        // GET: WSP
        DbConnector db = new DbConnector();
        WPSHandler handler = new WPSHandler();

        // GET: PurchaseInvoice
        [RBAC(AccessType = 2)]
        public ActionResult Index(int? PageNo, string ErrorMsg)
        {
            SaleInvoiceViewModel model = new SaleInvoiceViewModel
            {
                SaleInvoiceLists = db.GetSaleInvoice(PageNo, Constants.SALE_TYPE_WPS)
            };
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            return View(model);
        }

        [RBAC(AccessType = 2)]
        public ActionResult Create(int? id, bool? IsWPS, string NewInvoice, int? SalesOrderId, int? SalesOrderItemId)
        {
            if (!string.IsNullOrEmpty(NewInvoice))
            {
                SessionManager.EmptySessionList(Constants.SALE_TYPE_WPS);
            }
            SaleInvoice saleinvoice = handler.GetSaleInvoiceForCreateEdit(id, IsWPS, SalesOrderId, SalesOrderItemId);
            if (id.HasValue)
            {
                saleinvoice.TotalNotMapped = saleinvoice.TotalInvoiceValue;
            }
            return View(saleinvoice);
        }

        [HttpPost]
        public ActionResult Create(SaleInvoice saleInvoice, string btn)
        {
            saleInvoice = handler.SaveSaleInvoice(saleInvoice, btn);
            switch (btn)
            {
                case "Hold":
                    {
                        return RedirectToAction("Index");
                    }
                case "Save":
                    {
                        return RedirectToAction("Index");
                    }
                case "Make Payment":
                    {
                        return RedirectToAction("PlaceOrder", new { id = saleInvoice.SaleInvoiceId });
                    }
                case "Email":
                    {
                        break;
                    }
                case "SMS":
                    {
                        break;
                    }
            }
            return RedirectToAction("Create", new { id = saleInvoice.SaleInvoiceId });
        }

        [RBAC(AccessType = 3)]
        public ActionResult DeleteInvoice(int id)
        {
            string errorMessage = handler.DeleteInvoice(id);
            return RedirectToAction("Index", new { ErrorMsg = errorMessage });
        }

        [HttpPost]
        public ActionResult GetSaleOrder(int id)
        {
            SalesOrder model = db.GetSalesOrderById(id);
            SaleInvoice saleInvoice = new SaleInvoice
            {
                CustomerId = model.CustomerId,
                BranchId = model.BranchId,
                LocationId = model.LocationId,
                DivisionId = model.DivisionId,
                SaleAccountId = model.SalesAccountId,
                ChangeHeader = true,
            };
            SessionManager.EmptySessionList(Constants.SALE_TYPE_WPS);
            saleInvoice.SaleInvoiceItemList = new List<SaleInvoiceItem>();
            List<SaleInvoiceItem> SaleInvoiceItemList = db.GetInvoiceItemsForOrder(id);
            foreach (var items in SaleInvoiceItemList)
            {
                SessionManager.AddSaleInvoiceItem(new SaleInvoiceItem(items), Constants.SALE_TYPE_WPS);
            }
            List<SalesOrderItem> SalesOrderItemList = db.SalesOrderItems.Where(x => x.SalesOrderId == id).ToList();
            List<int> SkuId = SalesOrderItemList.Select(x => x.SKUMasterId).ToList();
            List<Product> ProductToAdded = db.Products.Where(x => SkuId.Contains(x.SKUMasterId) && x.CompanyId == db.CompanyId && x.IsSold == false).ToList();

            foreach (var item in SalesOrderItemList)
            {
                List<Product> ProductVsSKU = ProductToAdded.Where(x => x.SKUMasterId == item.SKUMasterId).ToList();
                if (ProductVsSKU.Count() >= item.Quantity)
                {
                    for (int i = 0; i < item.Quantity; i++)
                    {
                        saleInvoice.SaleInvoiceItemList.Add(ProductManager.AddInvoiceItem(ProductVsSKU[i], null, null, Constants.SALE_TYPE_WPS, Constants.SALE_ITEM_SOLD));
                    }
                }
                else
                {
                    for (int i = 0; i < ProductVsSKU.Count; i++)
                    {
                        saleInvoice.SaleInvoiceItemList.Add(ProductManager.AddInvoiceItem(ProductVsSKU[i], null, null, Constants.SALE_TYPE_WPS, Constants.SALE_ITEM_SOLD));
                    }
                }
            }

            saleInvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_WPS);
            saleInvoice = SaleCalculator.Calculate(saleInvoice);
            return PartialView("_ItemList", saleInvoice);
        }

        [HttpPost]
        public ActionResult GetDeliveryMemo(int id)
        {
            SaleInvoice SaleMemo = db.SaleInvoices.Where(x => x.SaleInvoiceId == id).FirstOrDefault();
            List<SaleInvoiceItem> SaleInvoiceItemList = db.SaleInvoiceItems.Where(x => x.SaleInvoiceId == id).ToList();
            SessionManager.EmptySessionList(Constants.SALE_TYPE_WPS);
            SaleInvoice SaleInvoiceNew = new SaleInvoice
            {
                CustomerId = SaleMemo.CustomerId,
                LocationId = SaleMemo.LocationId,
                SaleAccountId = SaleMemo.SaleAccountId,
                SaleTypeId = SaleMemo.SaleTypeId,
                ChangeHeader = true,
            };
            SaleInvoiceNew.SaleInvoiceItemList = new List<SaleInvoiceItem>();
            foreach (var item in SaleInvoiceItemList)
            {
                SaleInvoiceItem SaleInvoiceItem = new SaleInvoiceItem(item);
                SessionManager.AddSaleInvoiceItem(SaleInvoiceItem, Constants.SALE_TYPE_WPS);
            }

            SaleInvoiceNew.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_WPS);
            SaleInvoiceNew = SaleCalculator.Calculate(SaleInvoiceNew);
            return PartialView("_ItemList", SaleInvoiceNew);
        }

        [HttpPost]
        public ActionResult _ProductList(int SKUMasterId,string AttributeName)
        {
            SaleInvoice saleInvoice = new SaleInvoice();
            List<string> AttributeSet = AttributeName.Split(',').ToList();
            AttributeSet = AttributeSet.Where(x => x != "None").ToList();
            List<int> AttributeIds = new List<int>();
            List<int> ProductIds = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_WPS).Select(x => x.ProductId).ToList();
            List<AttributeLinking> AttributeList = new List<AttributeLinking>();
            if (AttributeSet.Count() > 0)
            {
                AttributeList  = db.AttributeLinkings.ToList();
                foreach (var item in AttributeList)
                {
                    bool IsContainAllElement = true;
                    item.ValuesList = item.Value.Split(',').ToList();
                    foreach (var Value in AttributeSet)
                    {
                        if (!item.ValuesList.Contains(Value))
                        {
                            IsContainAllElement = false;
                            break;
                        }
                    }
                    if (IsContainAllElement == true)
                    {
                        AttributeIds.Add(item.AttributeLinkingId);
                    }
                }
            }
            if (AttributeIds.Count() == 0)
            {
                saleInvoice.WSPProductList = db.Products.Where(x => x.SKUMasterId == SKUMasterId && x.IsSold == false && x.CompanyId == db.CompanyId && !ProductIds.Contains(x.ProductId)).ToList();
            }
            else
            {
                saleInvoice.WSPProductList = db.Products.Where(x => x.SKUMasterId == SKUMasterId && x.IsSold == false && x.CompanyId == db.CompanyId && AttributeIds.Contains(x.AttributeLinkingId ?? 0) && !ProductIds.Contains(x.ProductId)).ToList();
            }

            List<int> AttributeLinkingIds = saleInvoice.WSPProductList.Select(x => x.AttributeLinkingId ?? 0).ToList();
            List<AttributeLinking> AttributeLinkingList = db.AttributeLinkings.Where(x => AttributeLinkingIds.Contains(x.AttributeLinkingId)).ToList();

            foreach (var item in saleInvoice.WSPProductList)
            {
                item.AttributeLinking = AttributeLinkingList.Where(x => x.AttributeLinkingId == item.AttributeLinkingId).FirstOrDefault();
                if (item.AttributeLinking != null)
                {
                    item.AttributeList = item.AttributeLinking.Value.Split(',').ToList();
                }
                else
                {
                    int Count = db.AttributeSets.Count();
                    item.AttributeList = new List<string>();
                    for (int i = 0; i < Count; i++)
                    {
                        item.AttributeList.Add("None");
                    }
                }
            }
            saleInvoice.AttributeSetListNew = db.GetAttributeSets();

            for (int i = 0; i < saleInvoice.AttributeSetListNew.Count; i++)
            {
                int Id = saleInvoice.AttributeSetListNew[i].AttributeSetId;
                saleInvoice.AttributeSetListNew[i].AttributeValueList = DropdownManager.GetAttributeValueList(Id);
            }
            SessionManager.SetWSPProductList(saleInvoice.WSPProductList);
            if(saleInvoice.WSPProductList.Count() == 0)
            {
                ViewBag.ErrorMsg = "No Product Found";
            }
            return PartialView(saleInvoice);
        }

        [HttpPost]
        public ActionResult AddProduct(int ProductId, int? SalesInvoiceId)
        {
            SaleInvoice saleInvoice = new SaleInvoice();
            //Query from appropriate channel depending upon SalesInvoiceid
            if (SalesInvoiceId.HasValue && SalesInvoiceId.Value == 0)
            {
                saleInvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_WPS);
            }
            else
            {
                saleInvoice.SaleInvoiceItemList = handler.GetSaleInvoiceItems(SalesInvoiceId);
            }

            saleInvoice.WSPProductList = SessionManager.GetWSPProductList();

            Product product = saleInvoice.WSPProductList.Where(x => x.ProductId == ProductId).FirstOrDefault();
            if(product != null)
            {
                saleInvoice.WSPProductList.Remove(product);
                SessionManager.SetWSPProductList(saleInvoice.WSPProductList);
                if (SalesInvoiceId.HasValue && SalesInvoiceId.Value == 0)
                {
                    saleInvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_WPS);
                }
                else
                {
                    saleInvoice.SaleInvoiceItemList = handler.GetSaleInvoiceItems(SalesInvoiceId);
                }

                saleInvoice = AddProductinInvoice(product, saleInvoice, SalesInvoiceId);
            }
            saleInvoice.AttributeSetListNew = db.GetAttributeSets();

            for (int i = 0; i < saleInvoice.AttributeSetListNew.Count; i++)
            {
                int Id = saleInvoice.AttributeSetListNew[i].AttributeSetId;
                saleInvoice.AttributeSetListNew[i].AttributeValueList = DropdownManager.GetAttributeValueList(Id);
            }
            return PartialView("_ProductList", saleInvoice);
        }

        [HttpPost]
        public ActionResult RemoveProduct()
        {
            SaleInvoice saleInvoice = new SaleInvoice();
            saleInvoice.WSPProductList = new List<Product>(); ;
            return PartialView("_ProductList", saleInvoice);
        }

        [HttpPost]
        public ActionResult UpdateView(int SalesInvoiceId)
        {
            SaleInvoice saleInvoice = new SaleInvoice();
            if (SalesInvoiceId > 0)
            {
                saleInvoice.SaleInvoiceItemList = handler.GetSaleInvoiceItems(SalesInvoiceId);                
            }
            else
            {
                saleInvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_WPS);
            }
            saleInvoice = SaleCalculator.Calculate(saleInvoice);
            return PartialView("_ItemList", saleInvoice);
        }

        [HttpPost]
        public ActionResult _ItemList(string ProdId, int? SalesInvoiceId, bool? IsWPS)
        {
            string message = string.Empty;
            SaleInvoice saleInvoice = new SaleInvoice();
            //Query from appropriate channel depending upon SalesInvoiceid
            if (SalesInvoiceId.HasValue && SalesInvoiceId.Value == 0)
            {
                saleInvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_WPS);
            }
            else
            {
                saleInvoice.SaleInvoiceItemList = handler.GetSaleInvoiceItems(SalesInvoiceId);
            }
            //Check if product is already available in the current invoice
            //Check if product is not sold

            Product product = db.GetProductByNumber(ProdId);
            if (product == null)
            {
                ViewBag.ErrorMsg = "The product is unavailable or already sold";
            }
            else
            {
                saleInvoice = AddProductinInvoice(product, saleInvoice, SalesInvoiceId);
            }
            saleInvoice = SaleCalculator.Calculate(saleInvoice);
            return PartialView(saleInvoice);
        }

        public SaleInvoice AddProductinInvoice(Product product, SaleInvoice saleInvoice,int? SalesInvoiceId)
        {
            bool isAdded = saleInvoice.SaleInvoiceItemList.Any(x => x.ProductId == product.ProductId);
            if (isAdded)
            {
                ViewBag.ErrorMsg = "This product is already added in this invoice";
            }
            else
            {
                //Add item to invoice
                if (SalesInvoiceId.HasValue && SalesInvoiceId.Value == 0)
                {
                    ProductManager.AddInvoiceItem(product, SalesInvoiceId, true, Constants.SALE_TYPE_WPS, Constants.SALE_ITEM_SOLD);
                    saleInvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_WPS);
                }

                else
                {
                    saleInvoice.SaleInvoiceItemList.Add(ProductManager.AddInvoiceItem(product, SalesInvoiceId, true, Constants.SALE_TYPE_WPS, Constants.SALE_ITEM_SOLD));
                }
            }
            return saleInvoice;
        }
        [HttpPost]
        public ActionResult UpdateProduct(SaleInvoiceItem model)
        {
            SaleInvoice saleinvoice = new SaleInvoice();
            if (model.SaleInvoiceItemId > 0)
            {
                int SaleInvoiceId = handler.UpDateSaleInvoiceItem(model);
                saleinvoice.SaleInvoiceItemList = handler.GetSaleInvoiceItems(model.SaleInvoiceId);
            }
            else
            {
                SessionManager.UpdateOrderItem(model, Constants.SALE_TYPE_WPS);
                saleinvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_WPS);
            }

            if (saleinvoice.SaleInvoiceItemList != null)
            {
                saleinvoice = SaleCalculator.Calculate(saleinvoice);
            }
            return PartialView("_ItemList", saleinvoice);
        }

        public ActionResult PlaceOrder(int? id)
        {
            SaleInvoice saleinvoice = null;
            if (id.HasValue)
            {
                saleinvoice = db.SaleInvoices.Include("SaleInvoiceItemList").Where(x => x.SaleInvoiceId == id).FirstOrDefault();
            }
            foreach (var item in saleinvoice.SaleInvoiceItemList)
            {
                item.GST = db.GetGSTMasterById(item.GSTId);
            }
            if (saleinvoice.CustomerId == 61)
            {
                saleinvoice.PaymentTypeId = 1;
            }
            saleinvoice.PaymentTypeList = DropdownManager.GetPaymentTypeList(null);
            //saleinvoice.BankAccountsList = DropdownManager.BankAccountList(null);
            return View(saleinvoice);
        }

        [HttpPost]
        public ActionResult RemoveItem(int ProductId, int? SaleInvoiceId)
        {
            SaleInvoice saleinvoice = new SaleInvoice();
            if (SaleInvoiceId == null || SaleInvoiceId == 0)
            {
                SessionManager.RemoveItemFromList(ProductId, Constants.SALE_TYPE_WPS);
                saleinvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_WPS);
            }
            else
            {
                handler.RemoveSalesInvoiceItem(SaleInvoiceId, null, null, ProductId);
                saleinvoice.SaleInvoiceItemList = handler.GetSaleInvoiceItems(SaleInvoiceId);
            }

            if (saleinvoice.SaleInvoiceItemList != null)
            {
                saleinvoice = SaleCalculator.Calculate(saleinvoice);
            }
            return PartialView("_ItemList", saleinvoice);
        }

        [HttpPost]
        public ActionResult PlaceOrder(SaleInvoice SaleInvoice, string btn)
        {
            SaleInvoice item = db.GetSalesInvoiceById(SaleInvoice.SaleInvoiceId);
            if (item.IsLedgerUpdate == false)
            {

                if (SaleInvoice.PaymentTypeId == 5)
                {
                    PaymentGateWay model = new PaymentGateWay();
                    PaymentGateWayViewModel ViewModel = new PaymentGateWayViewModel();
                    Contact Contact = db.Contacts.Where(x => x.ContactId == item.Customer.ContactId).FirstOrDefault();
                    ViewModel.Email = Contact.Email;
                    ViewModel.Name = Contact.FirstName;
                    ViewModel.Phone = Contact.Phone;
                    ViewModel.Amount = item.TotalWithAdjustment.ToString();
                    ViewModel.OrderId = item.SaleInvoiceId.ToString();
                    ViewModel.OnSuccess = Constants.OnSuccessOfSaleInvoice;
                    ViewModel.OnFailure = Constants.OnFailure;
                    ViewModel.PaymentTypeId = SaleInvoice.PaymentTypeId.ToString();
                    model.GotoPayment(ViewModel, btn);
                }
                else if (SaleInvoice.PaymentTypeId == 7)
                {
                    handler.LedgerUpdate(SaleInvoice.SaleInvoiceId, SaleInvoice.PaymentTypeId ?? 0);
                    item.IsLedgerUpdate = true;
                    item.PaymentTypeId = SaleInvoice.PaymentTypeId;
                    item.Status = Constants.CREDIT_SALE;
                }
                else
                {
                    handler.LedgerUpdate(SaleInvoice.SaleInvoiceId, SaleInvoice.PaymentTypeId ?? 0);
                    item.IsLedgerUpdate = true;
                    item.PaymentTypeId = SaleInvoice.PaymentTypeId;
                    item.Status = Constants.PAYMENT_DONE;
                    item.Cash = SaleInvoice.Cash;
                    item.TenderChange = SaleInvoice.TenderChange;
                    item.ChequeNumber = SaleInvoice.ChequeNumber;
                }
                db.SaveSaleInvoiceEdit(item);
                db.SaveChanges();
            }
            switch (btn)
            {
                case "Print Invoice":
                    {
                        return RedirectToAction("GenerateInvoice", new { InvoiceId = SaleInvoice.SaleInvoiceId });
                    }
                case "Print WSP":
                    {
                        return RedirectToAction("GenerateInvoice", new { InvoiceId = SaleInvoice.SaleInvoiceId });
                    }
                case "Print POS":
                    {
                        return RedirectToAction("POS", new { InvoiceId = SaleInvoice.SaleInvoiceId });
                    }
                case "Save":
                    {
                        return RedirectToAction("Index");
                    }
            }
            return null;
        }

        [HttpPost]
        public ActionResult Return(FormCollection form)
        {
            PaymentGateWay model = new PaymentGateWay();
            try
            {
                string[] merc_hash_vars_seq;
                string merc_hash_string = string.Empty;
                string merc_hash = string.Empty;
                string SaleInvoiceId = string.Empty;
                string mode = string.Empty;
                string payuMoneyId = string.Empty;
                string txnid = string.Empty;
                string udf1 = string.Empty;
                string hash_seq = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
                string Status = form["status"].ToString();
                if (Status == "success")
                {

                    merc_hash_vars_seq = hash_seq.Split('|');
                    Array.Reverse(merc_hash_vars_seq);
                    merc_hash_string = ConfigurationManager.AppSettings["SALT"] + "|" + form["status"].ToString();
                    foreach (string merc_hash_var in merc_hash_vars_seq)
                    {
                        merc_hash_string += "|";
                        merc_hash_string = merc_hash_string + (form[merc_hash_var] ?? "");
                    }
                    Response.Write(merc_hash_string);
                    merc_hash = model.Generatehash512(merc_hash_string).ToLower();
                    if (merc_hash != form["hash"])
                    {
                        Response.Write("Hash value did not matched");
                    }
                    else
                    {
                        mode = Request.Form["mode"];
                        payuMoneyId = Request.Form["payuMoneyId"];
                        txnid = Request.Form["txnid"];
                        SaleInvoiceId = Request.Form["udf1"];
                        string btn = Request.Form["udf2"];
                        SaleInvoice SaleInvoice = db.GetSalesInvoiceById(Convert.ToInt32(SaleInvoiceId));
                        SaleInvoice.PaymentTypeId = 5;
                        SaleInvoice.Status = Constants.PAYMENT_DONE;
                        SaleInvoice.IsLedgerUpdate = true;
                        db.SaveSaleInvoiceEdit(SaleInvoice);
                        switch (btn)
                        {
                            case "Print Invoice":
                                {
                                    return RedirectToAction("GenerateInvoice", new { InvoiceId = SaleInvoice.SaleInvoiceId });
                                }
                            case "Print WSP":
                                {
                                    return RedirectToAction("GenerateInvoice", new { InvoiceId = SaleInvoice.SaleInvoiceId });
                                }
                            case "Print POS":
                                {
                                    return RedirectToAction("POS", new { InvoiceId = SaleInvoice.SaleInvoiceId });
                                }
                            case "Save":
                                {
                                    return RedirectToAction("Index");
                                }
                        }
                    }

                }
                else
                {
                    SaleInvoiceId = Request.Form["udf1"];
                    return RedirectToAction("Failure", "Checkout", new { id = SaleInvoiceId });
                }
            }

            catch (Exception ex)
            {
                Response.Write("<span style='color:red'>" + ex.Message + "</span>");

            }
            return null;
        }

        public ActionResult GenerateInvoice(int InvoiceId)
        {
            WSPPrintViewModel model = new WSPPrintViewModel
            {
                SalesInvoice = db.GetSalesInvoiceById(InvoiceId),
                SKUList = new List<WSP_SKU_LIST>(),
            };
            model.SalesInvoice.SaleInvoiceItemList = handler.GetSaleInvoiceItems(InvoiceId);
            List<int> SKUIds = model.SalesInvoice.SaleInvoiceItemList.Select(x => x.SKUMasterId ?? 0).ToList();
            List<SKUMaster> SkuList = db.SKUMasters.Where(x => SKUIds.Contains(x.SKUMasterId)).ToList();
            model.SalesInvoice.TotalQuantity = 0;
            foreach (var Item in model.SalesInvoice.SaleInvoiceItemList)
            {
                if (Item.Product.SellInPartial != true)
                {
                    model.SalesInvoice.TotalQuantity = model.SalesInvoice.TotalQuantity + Item.Quantity;
                }
                else
                {
                    model.SalesInvoice.TotalQuantity = model.SalesInvoice.TotalQuantity + Item.ConversionValue.Value;
                }
                Item.GST = db.GetGSTMasterById(Item.GSTId);
                if (Item.SGST != null)
                {
                    model.SalesInvoice.TotalSGST += Item.SGST.Value;
                }
                if (Item.CGST != null)
                {
                    model.SalesInvoice.TotalCGST += Item.CGST.Value;
                }
                if (Item.IGST != null)
                {
                    model.SalesInvoice.TotalIGST += Item.IGST.Value;
                }
                if (Item.DiscountId == -1)
                {
                    Item.SaleDiscountPercent = 0;
                }
                else
                {
                    Item.SaleDiscountPercent = db.Discounts.Where(x => x.DiscountId == Item.DiscountId).Select(x => x.DiscountPercent).FirstOrDefault();
                }

                WSP_SKU_LIST ListItem = model.SKUList.Where(x => x.SKUId == Item.SKUMasterId && x.SaleDiscountPercent == (Item.SaleDiscountPercent ?? 0) && x.WSP == Item.MRP && x.GST.GSTId == Item.GSTId).FirstOrDefault();
                if (ListItem == null)
                {
                    ListItem = new WSP_SKU_LIST
                    {
                        SKU = Item.SKUMaster.SKU,
                        SKUId = Item.SKUMaster.SKUMasterId,
                        IGST = Item.IGST ?? 0,
                        SGST = Item.SGST ?? 0,
                        CGST = Item.CGST ?? 0,
                        BasicPrice = Item.BasicPrice ?? 0,
                        WSP = Item.MRP,
                        SaleDiscountPercent = Item.SaleDiscountPercent ?? 0,
                        Product = Item.Product,
                        GST = Item.GST,
                    };
                    if (Item.Product.SellInPartial)
                    {
                        ListItem.TotalQuantity = Item.ConversionValue ?? 0;
                    }
                    else
                    {
                        ListItem.TotalQuantity = Item.Quantity;
                    }
                    
                    model.SKUList.Add(ListItem);
                }
                else
                {
                    ListItem.IGST = ListItem.IGST + Item.IGST ?? 0;
                    ListItem.SGST = ListItem.SGST + Item.SGST ?? 0;
                    ListItem.CGST = ListItem.CGST + Item.CGST ?? 0;
                    ListItem.BasicPrice = ListItem.BasicPrice + Item.BasicPrice ?? 0;
                    ListItem.TotalQuantity = ListItem.TotalQuantity + Item.Quantity;
                }
            }
            model.Ledger = db.Ledgers.Where(x => x.LedgerId == model.SalesInvoice.CustomerId).FirstOrDefault();
            model.Contact = db.Contacts.Where(x => x.ContactId == model.Ledger.ContactId).FirstOrDefault();
            if (model.Contact == null)
            {
                model.Contact = new Contact();
            }
            model.SaleOrderNumber = db.SalesOrders.Where(x => x.SalesOrderId == model.SalesInvoice.SaleOrderId).Select(x => x.SaleOrderNumber).FirstOrDefault();
            model.SalesInvoice.DeliveryMemoNumber = db.SaleInvoices.Where(x => x.SaleInvoiceId == model.SalesInvoice.SaleMemoId).Select(x => x.SaleInvoiceNo).FirstOrDefault();
            model.Company = SessionManager.GetSessionCompany();
            model.Branch = SessionManager.GetSessionBranch();
            model.PaymentType = db.GetPaymentTypeById(model.SalesInvoice.PaymentTypeId);
            model.SalesInvoice.TotalInWord = NumberToWords(Convert.ToInt32(model.SalesInvoice.TotalWithAdjustment));
            model.SaleType = db.Ledgers.Where(x => x.LedgerId == model.SalesInvoice.SaleTypeId).Select(x => x.Name).FirstOrDefault();
            model.SalesMan = db.Ledgers.Where(x => x.LedgerId == model.SalesInvoice.EmployeeId).Select(x => x.Name).FirstOrDefault();
            model.BankLedger = db.Ledgers.Where(x => x.LedgerId == model.Company.LedgerId).FirstOrDefault();
            return View(model);
        }

        public static string NumberToWords(int number)
        {
            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + NumberToWords(Math.Abs(number));

            string words = "";
            if ((number / 10000000) > 0)
            {
                words += NumberToWords(number / 10000000) + " Crore ";
                number %= 10000000;
            }

            if ((number / 100000) > 0)
            {
                words += NumberToWords(number / 100000) + " Lakh ";
                number %= 100000;
            }

            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000) + " Thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " Hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "and ";

                var unitsMap = new[] { "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
                var tensMap = new[] { "Zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsMap[number % 10];
                }
            }

            return words;
        }

        public ActionResult MakePayment(int SaleInvoiceId, int PaymentTypeId)
        {
            handler.LedgerUpdate(SaleInvoiceId, PaymentTypeId);
            return RedirectToAction("Index");
        }
    }
}