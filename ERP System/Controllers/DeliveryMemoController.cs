﻿using DataLayer;
using DataLayer.Utils;
using DataLayer.Models;
using DataLayer.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;
using ERP_System.Utils;
using System.Collections.Generic;
using PaymentGateway;
using System.Configuration;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class DeliveryMemoController : BaseController
    {
        DbConnector db = new DbConnector();
        DeliveryMemoHandler handler = new DeliveryMemoHandler();

        // GET: PurchaseInvoice
        [RBAC(AccessType = 2)]
        public ActionResult Index(int? PageNo, string ErrorMsg)
        {
            SaleInvoiceViewModel model = new SaleInvoiceViewModel
            {
                SaleInvoiceLists = db.GetSaleInvoice(PageNo,Constants.SALE_TYPE_DELIVERY_MEMO)
            };
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            return View(model);
        }

        [RBAC(AccessType = 0)]
        public ActionResult Create(int? id)
        {
            SaleInvoice saleinvoice = handler.GetDeliveryMemo(id);
            if (id.HasValue)
            {
                saleinvoice.TotalNotMapped = saleinvoice.TotalInvoiceValue;
            }
            return View(saleinvoice);
        }

        [RBAC(AccessType = 3)]
        public ActionResult Delete(int id)
        {
            string errorMessage = handler.DeleteInvoice(id);
            return RedirectToAction("Index", new { ErrorMsg = errorMessage });
        }

        [RBAC(AccessType = 1)]
        public ActionResult Edit(int id)
        {
            return RedirectToAction("Create", new { id = id });
        }

        [HttpPost]
        public ActionResult Create(SaleInvoice saleInvoice, string btn)
        {
            saleInvoice = handler.SaveSaleInvoice(saleInvoice, btn);
            switch (btn)
            {
                case "Save":
                    {
                        return RedirectToAction("Index");
                    }
                case "Save and Print":
                    {
                        return RedirectToAction("GenerateInvoice", new { InvoiceId = saleInvoice.SaleInvoiceId });
                    }
                case "Email":
                    {
                        break;
                    }
                case "SMS":
                    {
                        break;
                    }
            }
            return RedirectToAction("Create", new { id = saleInvoice.SaleInvoiceId });
        }

        [HttpPost]
        public ActionResult _ItemList(string ProdId, int? SalesInvoiceId, bool? IsWPS)
        {
            string message = string.Empty;
            SaleInvoice saleInvoice = new SaleInvoice();
            //Query from appropriate channel depending upon SalesInvoiceid
            if (SalesInvoiceId > 0)
            {
                saleInvoice.SaleInvoiceItemList = handler.GetSaleInvoiceItems(SalesInvoiceId);
            }
            else
            {
                saleInvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_DELIVERY_MEMO);
            }
            //Check if product is already available in the current invoice
            //Check if product is not sold
            Product product = db.GetProductByNumber(ProdId);
            if (product == null)
            {
                ViewBag.ErrorMsg = "The product is unavailable or already sold";
            }
            else
            {
                bool isAdded = saleInvoice.SaleInvoiceItemList.Any(x => x.ProductId == product.ProductId);
                if (isAdded)
                {
                    ViewBag.ErrorMsg = "This product is already added in this invoice";
                }
                else
                {
                    //Add item to invoice
                    if (SalesInvoiceId.HasValue && SalesInvoiceId.Value == 0)
                    {
                        ProductManager.AddInvoiceItem(product, SalesInvoiceId, IsWPS, Constants.SALE_TYPE_DELIVERY_MEMO,Constants.SALE_ITEM_SAVE_FOR_DELIVERY_MEMO);
                        saleInvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_DELIVERY_MEMO);
                    }

                    else
                    {
                        saleInvoice.SaleInvoiceItemList.Add(ProductManager.AddInvoiceItem(product, SalesInvoiceId, IsWPS,Constants.SALE_TYPE_DELIVERY_MEMO, Constants.SALE_ITEM_SAVE_FOR_DELIVERY_MEMO));
                    }
                }
            }
            saleInvoice = SaleCalculator.Calculate(saleInvoice);
            return PartialView(saleInvoice);
        }

        [HttpPost]
        public ActionResult UpdateProduct(SaleInvoiceItem model)
        {
            SaleInvoice saleinvoice = new SaleInvoice();
            if (model.SaleInvoiceItemId > 0)
            {
                int SaleInvoiceId = handler.UpDateSaleInvoiceItem(model);
                saleinvoice.SaleInvoiceItemList = handler.GetSaleInvoiceItems(model.SaleInvoiceId);
            }
            else
            {
                SessionManager.UpdateOrderItem(model,Constants.SALE_TYPE_DELIVERY_MEMO);
                saleinvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_DELIVERY_MEMO);
            }

            if (saleinvoice.SaleInvoiceItemList != null)
            {
                saleinvoice = SaleCalculator.Calculate(saleinvoice);
            }
            return PartialView("_ItemList", saleinvoice);
        }

        [HttpPost]
        public ActionResult RemoveItem(int ProductId, int? SaleInvoiceId)
        {
            SaleInvoice saleinvoice = new SaleInvoice();
            if (SaleInvoiceId == null || SaleInvoiceId == 0)
            {
                SessionManager.RemoveItemFromList(ProductId,Constants.SALE_TYPE_DELIVERY_MEMO);
                saleinvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_DELIVERY_MEMO);
            }
            else
            {
                handler.RemoveSalesInvoiceItem(SaleInvoiceId, null,null, ProductId);
                saleinvoice.SaleInvoiceItemList = handler.GetSaleInvoiceItems(SaleInvoiceId);
            }

            if (saleinvoice.SaleInvoiceItemList != null)
            {
                saleinvoice = SaleCalculator.Calculate(saleinvoice);
            }
            return PartialView("_ItemList", saleinvoice);
        }

        public ActionResult GenerateInvoice(int InvoiceId)
        {
            GenerateSalesInovice model = new GenerateSalesInovice
            {
                SalesInvoice = db.GetSalesInvoiceById(InvoiceId)
            };
            model.SalesInvoice.SaleInvoiceItemList = handler.GetSaleInvoiceItems(InvoiceId);
            foreach (var Item in model.SalesInvoice.SaleInvoiceItemList)
            {
                Item.GST = db.GetGSTMasterById(Item.GSTId);
                if (Item.SGST != null)
                {
                    model.SalesInvoice.TotalSGST += Item.SGST.Value;
                }
                if (Item.CGST != null)
                {
                    model.SalesInvoice.TotalCGST += Item.CGST.Value;
                }
                if (Item.IGST != null)
                {
                    model.SalesInvoice.TotalIGST += Item.IGST.Value;
                }
                if (Item.DiscountId == -1)
                {
                    Item.SaleDiscountPercent = 0;
                }
                else
                {
                    Item.SaleDiscountPercent = db.Discounts.Where(x => x.DiscountId == Item.DiscountId).Select(x => x.DiscountPercent).FirstOrDefault();
                }
            }
            model.Ledger = db.Ledgers.Where(x => x.LedgerId == model.SalesInvoice.CustomerId).FirstOrDefault();
            model.Contact = db.Contacts.Where(x => x.ContactId == model.Ledger.ContactId).FirstOrDefault();
            if (model.Contact == null)
            {
                model.Contact = new Contact();
            }
            model.SaleOrderNumber = db.SalesOrders.Where(x => x.SalesOrderId == model.SalesInvoice.SaleOrderId).Select(x => x.SaleOrderNumber).FirstOrDefault();
            //model.Shipping = db.GetShippingById(model.SalesInvoice.ShippingId);
            model.Company = SessionManager.GetSessionCompany();
            model.Branch = SessionManager.GetSessionBranch();
            model.PaymentType = db.GetPaymentTypeById(model.SalesInvoice.PaymentTypeId);
            model.SalesInvoice.TotalInWord = NumberToWords(Convert.ToInt32(model.SalesInvoice.TotalWithAdjustment));
            model.SaleType = db.Ledgers.Where(x => x.LedgerId == model.SalesInvoice.SaleTypeId).Select(x => x.Name).FirstOrDefault();
            model.SalesMan = db.Ledgers.Where(x => x.LedgerId == model.SalesInvoice.EmployeeId).Select(x => x.Name).FirstOrDefault();
            model.BankLedger = db.Ledgers.Where(x => x.LedgerId == model.Company.LedgerId).FirstOrDefault();
            return View(model);
        }

        public static string NumberToWords(int number)
        {
            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + NumberToWords(Math.Abs(number));

            string words = "";
            if ((number / 10000000) > 0)
            {
                words += NumberToWords(number / 10000000) + " Crore ";
                number %= 10000000;
            }

            if ((number / 100000) > 0)
            {
                words += NumberToWords(number / 100000) + " Lakh ";
                number %= 100000;
            }

            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000) + " Thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " Hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "and ";

                var unitsMap = new[] { "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
                var tensMap = new[] { "Zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsMap[number % 10];
                }
            }

            return words;
        }


        public ActionResult POS(int InvoiceId)
        {
            GenerateSalesInovice model = new GenerateSalesInovice
            {
                SalesInvoice = db.GetSalesInvoiceById(InvoiceId)
            };
            model.SalesInvoice.SaleInvoiceItemList = handler.GetSaleInvoiceItems(InvoiceId);
            model.GstList = new List<POS_GST_LIST>();
            foreach (var Item in model.SalesInvoice.SaleInvoiceItemList)
            {
                Item.GST = db.GetGSTMasterById(Item.GSTId);
                Item.SKUMaster = db.SKUMasters.Where(x => x.SKUMasterId == Item.SKUMasterId).FirstOrDefault();
                if (Item.Product.AttributeLinkingId.HasValue)
                {
                    string Size = db.AttributeLinkings.Where(x => x.AttributeLinkingId == Item.Product.AttributeLinkingId).Select(x => x.Value).FirstOrDefault();
                    if (Size != null)
                    {
                        List<string> Values = Size.Split(',').ToList();
                        if (Values.Count > 2)
                        {
                            Item.Size = Values[1];
                        }
                    }
                }
                Item.PayableAmount = Item.BasicPrice.Value - Item.Discount;
                if (Item.SGST != null)
                {
                    model.SalesInvoice.TotalSGST += Item.SGST.Value;
                }
                if (Item.CGST != null)
                {
                    model.SalesInvoice.TotalCGST += Item.CGST.Value;
                }
                if (Item.IGST != null)
                {
                    model.SalesInvoice.TotalIGST += Item.IGST.Value;
                }
                if (Item.DiscountId == -1)
                {
                    Item.SaleDiscountPercent = 0;
                }
                else
                {
                    Item.SaleDiscountPercent = db.Discounts.Where(x => x.DiscountId == Item.DiscountId).Select(x => x.DiscountPercent).FirstOrDefault();
                }
                //model.TotalBasePrice = model.TotalBasePrice + Item.BasicPrice.Value;
                model.SalesInvoice.TotalNotMapped = model.SalesInvoice.TotalNotMapped + Item.SubTotal;
                POS_GST_LIST ListItem = model.GstList.Where(x => x.GSTId == Item.GSTId).FirstOrDefault();
                if (ListItem == null)
                {
                    ListItem = new POS_GST_LIST
                    {
                        GST = Item.GST,
                        GSTId = Item.GSTId,
                        IGST = Item.IGST,
                        SGST = Item.SGST,
                        CGST = Item.CGST,
                        Amount = Item.BasicPrice.Value - Item.Discount
                    };
                    model.TotalBasePrice = model.TotalBasePrice + ListItem.Amount.Value;
                    model.GstList.Add(ListItem);
                }
                else
                {
                    ListItem.IGST = ListItem.IGST + Item.IGST;
                    ListItem.SGST = ListItem.SGST + Item.SGST;
                    ListItem.CGST = ListItem.CGST + Item.CGST;
                    ListItem.Amount = ListItem.Amount + Item.BasicPrice.Value - Item.Discount;
                    model.TotalBasePrice = model.TotalBasePrice + Item.BasicPrice.Value - Item.Discount;
                }
            }
            model.Salesman = db.Ledgers.Where(x => x.ContactId == model.SalesInvoice.EmployeeId.Value).Select(x => x.Contact).FirstOrDefault();
            model.GstList = model.GstList.OrderByDescending(x => x.GST.SGST).ToList();
            model.Ledger = db.Ledgers.Where(x => x.LedgerId == model.SalesInvoice.CustomerId).FirstOrDefault();
            model.Contact = SessionManager.GetSessionUser().Contact;
            if (model.Contact == null)
            {
                model.Contact = new Contact();
            }
            //model.Shipping = db.GetShippingById(model.SalesInvoice.ShippingId);
            model.Company = SessionManager.GetSessionCompany();
            model.Branch = SessionManager.GetSessionBranch();
            model.PaymentType = db.GetPaymentTypeById(model.SalesInvoice.PaymentTypeId);
            model.SalesInvoice.TotalInWord = NumberToWords(Convert.ToInt32(model.SalesInvoice.TotalInvoiceValue));
            return View(model);
        }
    }
}