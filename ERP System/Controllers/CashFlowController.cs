﻿using DataLayer;
using DataLayer.Models;
using DataLayer.ViewModels;
using ERP_System.Utils;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class CashFlowController : Controller
    {
        DbConnector db = new DbConnector();

        [RBAC(AccessType = 2)]
        public ActionResult Index()
        {
            CashFlowReportViewModel Model = new CashFlowReportViewModel();
            return View(Model);
        }

        [HttpPost]
        public ActionResult _MonthlyList(CashFlowReportViewModel ViewModel)
        {
            ViewModel.TotalInFlow = 0;
            ViewModel.TotalNetFlow = 0;
            ViewModel.TotalOutFlow = 0;
            ViewModel.Company = db.Companys.Where(x => x.CompanyId == db.CompanyId).FirstOrDefault();

            ViewModel.StartingDate = new DateTime(ViewModel.StartingDate.Year, ViewModel.StartingDate.Month, 1);
            ViewModel.LastDate = new DateTime(ViewModel.LastDate.Year, ViewModel.LastDate.Month, DateTime.DaysInMonth(ViewModel.LastDate.Year, ViewModel.LastDate.Month));


            List<int?> LedgerForPayments = db.PaymentTypes.Select(x => x.LedgerId).ToList();
            List<Voucher> VoucherList = db.Vouchers.Where(x => LedgerForPayments.Contains(x.LedgerId) && x.CompanyId == db.CompanyId && ViewModel.StartingDate <= x.EntryDate && x.EntryDate <= ViewModel.LastDate && x.VoucherFor != Constants.CONTRA_ENTRY && x.VoucherFor != Constants.OPENING_BALANCE).ToList();
           
            ViewModel.MonthlyCashMovementList = new List<MonthlyCashMovement>();
            while (ViewModel.StartingDate.Year < ViewModel.LastDate.Year || (ViewModel.StartingDate.Year == ViewModel.LastDate.Year && ViewModel.StartingDate.Month <= ViewModel.LastDate.Month))
            {
                MonthlyCashMovement cashMovement = new MonthlyCashMovement();
                cashMovement.Month = ViewModel.StartingDate.Month;
                cashMovement.Year = ViewModel.StartingDate.Year;
                cashMovement.InFlow = VoucherList.Where(x => x.EntryDate.Month == ViewModel.StartingDate.Month && x.EntryDate.Year == ViewModel.StartingDate.Year && x.Type == Constants.DR && (x.VoucherFor == Constants.SALE_TYPE_SALE_INVOICE || x.VoucherFor == Constants.SALE_TYPE_SALE_EXCHANGE || x.VoucherFor == Constants.SALE_TYPE_SALE_VOUCHER || x.VoucherFor == Constants.SALE_TYPE_SALE_ORDER || x.VoucherFor == Constants.SALE_TYPE_WPS || x.VoucherFor == Constants.RECEIPT_VOUCHER || x.VoucherFor == Constants.SALE_TYPE_PURCHASE_RETURN)).Sum(x => x.Total);
                cashMovement.OutFlow = VoucherList.Where(x => x.EntryDate.Month == ViewModel.StartingDate.Month && x.EntryDate.Year == ViewModel.StartingDate.Year && x.Type == Constants.CR && (x.VoucherFor == Constants.PURCHASE_TYPE_PURCHASE_INVOICE || x.VoucherFor == Constants.PURCHASE_TYPE_PURCHASE_VOUCHER || x.VoucherFor == Constants.SALE_INVOICE_EXCHANGE || x.VoucherFor == Constants.PAYMENT_VOUCHER)).Sum(x => x.Total);
                cashMovement.NetFlow = cashMovement.InFlow - cashMovement.OutFlow;
                cashMovement.MonthString = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(ViewModel.StartingDate.Month) + '-' + ViewModel.StartingDate.Year;
                ViewModel.TotalInFlow = ViewModel.TotalInFlow + cashMovement.InFlow;
                ViewModel.TotalNetFlow = ViewModel.TotalNetFlow + cashMovement.NetFlow;
                ViewModel.TotalOutFlow = ViewModel.TotalOutFlow + cashMovement.OutFlow;
                ViewModel.StartingDate = ViewModel.StartingDate.AddMonths(1);
                ViewModel.MonthlyCashMovementList.Add(cashMovement);
            }
            return PartialView(ViewModel);
        }

        //[RBAC(AccessType = 2)]
        //public ActionResult MonthlySummary(int Month,int Year)
        //{
        //    CashFlowReportViewModel Model = new CashFlowReportViewModel();
        //    Model.Company = db.Companys.Where(x => x.CompanyId == db.CompanyId).FirstOrDefault();

        //    Model.StartingDate = new DateTime(Year, Month, 1);
        //    Model.LastDate = new DateTime(Year, Month, DateTime.DaysInMonth(Year, Month));

        //    List<int?> LedgerForPayments = db.PaymentTypes.Where(x => x.CompanyId == db.CompanyId || x.CompanyId == null).Select(x => x.LedgerId).ToList();
        //    List<Ledger> LedgerList = db.Ledgers.Where(x => LedgerForPayments.Contains(x.LedgerId)).ToList();
        //    List<Voucher> VoucherList = db.Vouchers.Where(x => ((LedgerForPayments.Contains(x.LedgerId) && x.VoucherFor != Constants.PAYMENT_VOUCHER) || (x.VoucherFor == Constants.PAYMENT_VOUCHER && x.PaymentTypeId == null)) && x.CompanyId == db.CompanyId && Month == x.EntryDate.Month && x.EntryDate.Year == Year && x.VoucherFor != Constants.CONTRA_ENTRY && x.VoucherFor != Constants.OPENING_BALANCE).ToList();
        //    List<int?> LedgerIds = VoucherList.Select(x => x.LedgerId).ToList();
        //    LedgerList = db.Ledgers.Where(x => LedgerIds.Contains(x.LedgerId)).ToList();
        //    List<int?> AccountCategoryIds = LedgerList.Select(x => x.AccountCategoryId).ToList(); 
        //    List<AccountCategory> AccountCategoryList = db.ContactCategorys.Where(x => AccountCategoryIds.Contains(x.AccountCategoryId)).ToList();

        //    Model.LedgerTypeList = new List<LedgerTypeList>();

        //    foreach(var item in AccountCategoryList)
        //    {
        //        LedgerTypeList LedgerTypeInFlow = new LedgerTypeList();
        //        LedgerTypeInFlow.IsInFlow = true;
        //        LedgerTypeInFlow.AccountCategory = item;
        //        LedgerTypeInFlow.DivId = "InFlow" + item.AccountCategoryId;

        //        LedgerTypeList LedgerTypeOutFlow = new LedgerTypeList();
        //        LedgerTypeOutFlow.AccountCategory = item;
        //        LedgerTypeOutFlow.DivId = "OutFlow" + item.AccountCategoryId;

        //        List<Ledger> LedgerListNew = LedgerList.Where(x => x.AccountCategoryId == item.AccountCategoryId).ToList();   
                
        //        foreach (var Ledger in LedgerListNew)
        //        {
        //            List<Voucher> VoucherLisInFlow = VoucherList.Where(x => x.LedgerId == Ledger.LedgerId && x.Type == Constants.DR && (x.VoucherFor == Constants.SALE_TYPE_SALE_INVOICE || x.VoucherFor == Constants.SALE_TYPE_SALE_EXCHANGE || x.VoucherFor == Constants.SALE_TYPE_SALE_VOUCHER || x.VoucherFor == Constants.SALE_TYPE_SALE_ORDER || x.VoucherFor == Constants.SALE_TYPE_WPS || x.VoucherFor == Constants.SALE_TYPE_PURCHASE_RETURN || x.VoucherFor == Constants.RECEIPT_VOUCHER)).ToList();
        //            List<Voucher> VoucherLisOutFlow = VoucherList.Where(x => x.LedgerId == Ledger.LedgerId && ((x.Type == Constants.CR && (x.VoucherFor == Constants.PURCHASE_TYPE_PURCHASE_INVOICE || x.VoucherFor == Constants.PURCHASE_TYPE_PURCHASE_VOUCHER || x.VoucherFor == Constants.SALE_INVOICE_EXCHANGE)) || (x.Type == Constants.DR && x.VoucherFor == Constants.PAYMENT_VOUCHER))).ToList();
        //            decimal InFlow = VoucherLisInFlow.Sum(x => x.Total);
        //            decimal OutFlow = VoucherLisOutFlow.Sum(x => x.Total);
        //            if(InFlow > 0)
        //            {
        //                LedgerList ListItem = new LedgerList();
        //                ListItem.DivId = "InLedger" + Ledger.LedgerId;
        //                ListItem.Ledger = Ledger;
        //                ListItem.Total = InFlow;
        //                LedgerTypeInFlow.Total = LedgerTypeInFlow.Total + ListItem.Total;
        //                foreach(var Voucher in VoucherLisInFlow)
        //                {
        //                    InvoiceList InvoiceListItem = new InvoiceList();                            
        //                    if(Voucher.VoucherFor == Constants.PURCHASE_TYPE_PURCHASE_INVOICE || Voucher.VoucherFor == Constants.PURCHASE_TYPE_PURCHASE_VOUCHER)
        //                    {
        //                        InvoiceListItem.InvoiceNumber = Voucher.InvoiceNumber;
        //                        InvoiceListItem.InvoiceId = Voucher.PurchaseInvoiceId ?? 0;
        //                    }
        //                    else if (Voucher.VoucherFor == Constants.SALE_TYPE_SALE_INVOICE || Voucher.VoucherFor == Constants.SALE_TYPE_SALE_EXCHANGE || Voucher.VoucherFor == Constants.SALE_TYPE_SALE_VOUCHER || Voucher.VoucherFor == Constants.SALE_TYPE_WPS || Voucher.VoucherFor == Constants.SALE_TYPE_PURCHASE_RETURN)
        //                    {
        //                        InvoiceListItem.InvoiceNumber = Voucher.InvoiceNumber;
        //                        InvoiceListItem.InvoiceId = Voucher.SaleInvoiceId ?? 0;
        //                    }
        //                    else if (Voucher.VoucherFor == Constants.SALE_TYPE_SALE_ORDER)
        //                    {
        //                        InvoiceListItem.InvoiceNumber = Voucher.InvoiceNumber;
        //                        InvoiceListItem.InvoiceId = Voucher.SalesOrderId ?? 0;
        //                    }
        //                    else if (Voucher.VoucherFor == Constants.PAYMENT_VOUCHER || Voucher.VoucherFor == Constants.RECEIPT_VOUCHER || Voucher.VoucherFor == Constants.JOURNAL_ENTRY || Voucher.VoucherFor == Constants.CONTRA_ENTRY)
        //                    {
        //                        InvoiceListItem.InvoiceNumber = Voucher.VoucherNumber;
        //                    }
        //                    InvoiceListItem.Type = Voucher.VoucherFor;
        //                    InvoiceListItem.Total = Voucher.Total;
        //                    ListItem.InvoiceList.Add(InvoiceListItem);
        //                }
        //                LedgerTypeInFlow.LedgerList.Add(ListItem);
        //            }
        //            if (OutFlow > 0)
        //            {
        //                LedgerList ListItem = new LedgerList();
        //                ListItem.DivId = "OutLedger" + Ledger.LedgerId;
        //                ListItem.Ledger = Ledger;
        //                ListItem.Total = OutFlow;
        //                LedgerTypeOutFlow.Total = LedgerTypeOutFlow.Total + ListItem.Total;
        //                foreach (var Voucher in VoucherLisOutFlow)
        //                {
        //                    InvoiceList InvoiceListItem = new InvoiceList();
        //                    if (Voucher.VoucherFor == Constants.PURCHASE_TYPE_PURCHASE_INVOICE || Voucher.VoucherFor == Constants.PURCHASE_TYPE_PURCHASE_VOUCHER)
        //                    {
        //                        InvoiceListItem.InvoiceNumber = Voucher.InvoiceNumber;
        //                        InvoiceListItem.InvoiceId = Voucher.PurchaseInvoiceId ?? 0;
        //                    }
        //                    else if (Voucher.VoucherFor == Constants.SALE_TYPE_SALE_INVOICE || Voucher.VoucherFor == Constants.SALE_TYPE_SALE_EXCHANGE || Voucher.VoucherFor == Constants.SALE_TYPE_SALE_VOUCHER || Voucher.VoucherFor == Constants.SALE_TYPE_WPS || Voucher.VoucherFor == Constants.SALE_TYPE_PURCHASE_RETURN)
        //                    {
        //                        InvoiceListItem.InvoiceNumber = Voucher.InvoiceNumber;
        //                        InvoiceListItem.InvoiceId = Voucher.SaleInvoiceId ?? 0;
        //                    }
        //                    else if (Voucher.VoucherFor == Constants.SALE_TYPE_SALE_ORDER)
        //                    {
        //                        InvoiceListItem.InvoiceNumber = Voucher.InvoiceNumber;
        //                        InvoiceListItem.InvoiceId = Voucher.SalesOrderId ?? 0;
        //                    }
        //                    else if (Voucher.VoucherFor == Constants.PAYMENT_VOUCHER || Voucher.VoucherFor == Constants.RECEIPT_VOUCHER || Voucher.VoucherFor == Constants.JOURNAL_ENTRY || Voucher.VoucherFor == Constants.CONTRA_ENTRY)
        //                    {
        //                        InvoiceListItem.InvoiceNumber = Voucher.VoucherNumber;
        //                    }
        //                    InvoiceListItem.Type = Voucher.VoucherFor;
        //                    InvoiceListItem.Total = Voucher.Total;
        //                    ListItem.InvoiceList.Add(InvoiceListItem);
        //                }
        //                LedgerTypeOutFlow.LedgerList.Add(ListItem);
        //            }
        //        }
        //        if(LedgerTypeInFlow.Total > 0)
        //        {
        //            Model.LedgerTypeList.Add(LedgerTypeInFlow);
        //        }
        //        if (LedgerTypeOutFlow.Total > 0)
        //        {
        //            Model.LedgerTypeList.Add(LedgerTypeOutFlow);
        //        }
        //    }
        //    Model.TotalInFlow = Model.LedgerTypeList.Where(x => x.IsInFlow).Sum(x => x.Total);
        //    Model.TotalOutFlow = Model.LedgerTypeList.Where(x => !x.IsInFlow).Sum(x => x.Total);
        //    Model.TotalNetFlow = Model.TotalInFlow - Model.TotalOutFlow;
        //    return View(Model);
        //}

        [RBAC(AccessType = 2)]
        public ActionResult MonthlySummary(int Month, int Year)
        {
            int Count = 0;
            CashFlowReportViewModel Model = new CashFlowReportViewModel();
            Model.Company = db.Companys.Where(x => x.CompanyId == db.CompanyId).FirstOrDefault();

            Model.StartingDate = new DateTime(Year, Month, 1);
            Model.LastDate = new DateTime(Year, Month, DateTime.DaysInMonth(Year, Month));

            List<int?> LedgerForPayments = db.PaymentTypes.Where(x => x.CompanyId == db.CompanyId || x.CompanyId == null).Select(x => x.LedgerId).ToList();
            List<Ledger> LedgerList = db.Ledgers.Where(x => LedgerForPayments.Contains(x.LedgerId)).ToList();
            List<Voucher> VoucherList = db.Vouchers.Where(x => ((LedgerForPayments.Contains(x.LedgerId) && x.VoucherFor != Constants.PAYMENT_VOUCHER) || (x.VoucherFor == Constants.PAYMENT_VOUCHER && x.PaymentTypeId == null)) && x.CompanyId == db.CompanyId && Month == x.EntryDate.Month && x.EntryDate.Year == Year && x.VoucherFor != Constants.CONTRA_ENTRY && x.VoucherFor != Constants.OPENING_BALANCE).ToList();
            List<int?> LedgerIds = VoucherList.Select(x => x.LedgerId).ToList();
            LedgerList = db.Ledgers.Where(x => LedgerIds.Contains(x.LedgerId)).ToList();
            List<int?> AccountCategoryIds = LedgerList.Select(x => x.AccountCategoryId).ToList();
            List<AccountCategory> AccountCategoryList = db.ContactCategorys.Where(x => AccountCategoryIds.Contains(x.AccountCategoryId)).ToList();

            List<int?> AccountSubCategoryIds = LedgerList.Select(x => x.AccountSubCategoryId).ToList();
            List<AccountSubCategory> AccountSubCategoryList = db.ContactSubCategorys.Where(x => AccountSubCategoryIds.Contains(x.AccountSubCategoryId)).ToList();
            Model.AccountCategoryList = new List<AccountCategoryList>();

            foreach (var item in AccountCategoryList)
            {
                AccountCategoryList LedgerTypeInFlow = new AccountCategoryList();
                LedgerTypeInFlow.IsInFlow = true;
                LedgerTypeInFlow.AccountCategory = item;
                LedgerTypeInFlow.DivId = "InFlow" + item.AccountCategoryId;

                AccountCategoryList LedgerTypeOutFlow = new AccountCategoryList();
                LedgerTypeOutFlow.AccountCategory = item;
                LedgerTypeOutFlow.DivId = "OutFlow" + item.AccountCategoryId;

                foreach(var SubCategory in AccountSubCategoryList.Where(x => x.AccountCategoryId == item.AccountCategoryId))
                {
                    AccountSubCategoryList SubCategoryIn = new AccountSubCategoryList();
                    SubCategoryIn.AccountSubCategory = SubCategory;
                    SubCategoryIn.DivId = "SubIn" + SubCategory.AccountSubCategoryId;

                    AccountSubCategoryList SubCategoryOut = new AccountSubCategoryList();
                    SubCategoryOut.AccountSubCategory = SubCategory;
                    SubCategoryOut.DivId = "SubOut" + SubCategory.AccountSubCategoryId;

                    List<Ledger> LedgerListNew = LedgerList.Where(x => x.AccountCategoryId == item.AccountCategoryId && x.AccountSubCategoryId == SubCategory.AccountSubCategoryId).ToList();

                foreach (var Ledger in LedgerListNew)
                {

                    List<Voucher> VoucherLisInFlow = VoucherList.Where(x => x.LedgerId == Ledger.LedgerId && x.Type == Constants.DR && (x.VoucherFor == Constants.SALE_TYPE_SALE_INVOICE || x.VoucherFor == Constants.SALE_TYPE_SALE_EXCHANGE || x.VoucherFor == Constants.SALE_TYPE_SALE_VOUCHER || x.VoucherFor == Constants.SALE_TYPE_SALE_ORDER || x.VoucherFor == Constants.SALE_TYPE_WPS || x.VoucherFor == Constants.SALE_TYPE_PURCHASE_RETURN || x.VoucherFor == Constants.RECEIPT_VOUCHER)).ToList();
                    List<Voucher> VoucherLisOutFlow = VoucherList.Where(x => x.LedgerId == Ledger.LedgerId && ((x.Type == Constants.CR && (x.VoucherFor == Constants.PURCHASE_TYPE_PURCHASE_INVOICE || x.VoucherFor == Constants.PURCHASE_TYPE_PURCHASE_VOUCHER || x.VoucherFor == Constants.SALE_INVOICE_EXCHANGE)) || (x.Type == Constants.DR && x.VoucherFor == Constants.PAYMENT_VOUCHER))).ToList();
                    decimal InFlow = VoucherLisInFlow.Sum(x => x.Total);
                    decimal OutFlow = VoucherLisOutFlow.Sum(x => x.Total);
                    if (InFlow > 0)
                    {
                        LedgerList ListItem = new LedgerList();
                        ListItem.DivId = "InLedger" + Ledger.LedgerId;
                        ListItem.Ledger = Ledger;
                        ListItem.Total = InFlow;
                            SubCategoryIn.Total = SubCategoryIn.Total + ListItem.Total;
                        foreach (var Voucher in VoucherLisInFlow)
                        {
                            InvoiceList InvoiceListItem = new InvoiceList();
                            if (Voucher.VoucherFor == Constants.PURCHASE_TYPE_PURCHASE_INVOICE || Voucher.VoucherFor == Constants.PURCHASE_TYPE_PURCHASE_VOUCHER)
                            {
                                InvoiceListItem.InvoiceNumber = Voucher.InvoiceNumber;
                                InvoiceListItem.InvoiceId = Voucher.PurchaseInvoiceId ?? 0;
                            }
                            else if (Voucher.VoucherFor == Constants.SALE_TYPE_SALE_INVOICE || Voucher.VoucherFor == Constants.SALE_TYPE_SALE_EXCHANGE || Voucher.VoucherFor == Constants.SALE_TYPE_SALE_VOUCHER || Voucher.VoucherFor == Constants.SALE_TYPE_WPS || Voucher.VoucherFor == Constants.SALE_TYPE_PURCHASE_RETURN)
                            {
                                InvoiceListItem.InvoiceNumber = Voucher.InvoiceNumber;
                                InvoiceListItem.InvoiceId = Voucher.SaleInvoiceId ?? 0;
                            }
                            else if (Voucher.VoucherFor == Constants.SALE_TYPE_SALE_ORDER)
                            {
                                InvoiceListItem.InvoiceNumber = Voucher.InvoiceNumber;
                                InvoiceListItem.InvoiceId = Voucher.SalesOrderId ?? 0;
                            }
                            else if (Voucher.VoucherFor == Constants.PAYMENT_VOUCHER || Voucher.VoucherFor == Constants.RECEIPT_VOUCHER || Voucher.VoucherFor == Constants.JOURNAL_ENTRY || Voucher.VoucherFor == Constants.CONTRA_ENTRY)
                            {
                                InvoiceListItem.InvoiceNumber = Voucher.VoucherNumber;
                            }
                            InvoiceListItem.Type = Voucher.VoucherFor;
                            InvoiceListItem.Total = Voucher.Total;
                            ListItem.InvoiceList.Add(InvoiceListItem);
                        }
                            SubCategoryIn.LedgerList.Add(ListItem);
                    }
                    if (OutFlow > 0)
                    {
                        LedgerList ListItem = new LedgerList();
                        ListItem.DivId = "OutLedger" + Ledger.LedgerId;
                        ListItem.Ledger = Ledger;
                        ListItem.Total = OutFlow;
                            SubCategoryOut.Total = SubCategoryOut.Total + ListItem.Total;
                        foreach (var Voucher in VoucherLisOutFlow)
                        {
                            InvoiceList InvoiceListItem = new InvoiceList();
                            if (Voucher.VoucherFor == Constants.PURCHASE_TYPE_PURCHASE_INVOICE || Voucher.VoucherFor == Constants.PURCHASE_TYPE_PURCHASE_VOUCHER)
                            {
                                InvoiceListItem.InvoiceNumber = Voucher.InvoiceNumber;
                                InvoiceListItem.InvoiceId = Voucher.PurchaseInvoiceId ?? 0;
                            }
                            else if (Voucher.VoucherFor == Constants.SALE_TYPE_SALE_INVOICE || Voucher.VoucherFor == Constants.SALE_TYPE_SALE_EXCHANGE || Voucher.VoucherFor == Constants.SALE_TYPE_SALE_VOUCHER || Voucher.VoucherFor == Constants.SALE_TYPE_WPS || Voucher.VoucherFor == Constants.SALE_TYPE_PURCHASE_RETURN)
                            {
                                InvoiceListItem.InvoiceNumber = Voucher.InvoiceNumber;
                                InvoiceListItem.InvoiceId = Voucher.SaleInvoiceId ?? 0;
                            }
                            else if (Voucher.VoucherFor == Constants.SALE_TYPE_SALE_ORDER)
                            {
                                InvoiceListItem.InvoiceNumber = Voucher.InvoiceNumber;
                                InvoiceListItem.InvoiceId = Voucher.SalesOrderId ?? 0;
                            }
                            else if (Voucher.VoucherFor == Constants.PAYMENT_VOUCHER || Voucher.VoucherFor == Constants.RECEIPT_VOUCHER || Voucher.VoucherFor == Constants.JOURNAL_ENTRY || Voucher.VoucherFor == Constants.CONTRA_ENTRY)
                            {
                                InvoiceListItem.InvoiceNumber = Voucher.VoucherNumber;
                            }
                            InvoiceListItem.Type = Voucher.VoucherFor;
                            InvoiceListItem.Total = Voucher.Total;
                            ListItem.InvoiceList.Add(InvoiceListItem);
                        }
                            SubCategoryOut.LedgerList.Add(ListItem);
                    }
                }
                if (SubCategoryIn.Total > 0)
                {
                        LedgerTypeInFlow.Total = LedgerTypeInFlow.Total + SubCategoryIn.Total;
                        LedgerTypeInFlow.AccountSubCategoryList.Add(SubCategoryIn);
                }
                if (SubCategoryOut.Total > 0)
                {
                        LedgerTypeOutFlow.Total = LedgerTypeOutFlow.Total + SubCategoryOut.Total;
                        LedgerTypeOutFlow.AccountSubCategoryList.Add(SubCategoryOut);
                }
                }


                List<Ledger> LedgerListTemp = LedgerList.Where(x => x.AccountCategoryId == item.AccountCategoryId && x.AccountSubCategoryId == null).ToList();
                if(LedgerListTemp.Count() > 0)
                {
                    Count++;

                    AccountSubCategoryList NewSubCategoryIn = new AccountSubCategoryList();
                    NewSubCategoryIn.AccountSubCategory = new AccountSubCategory();
                    NewSubCategoryIn.AccountSubCategory.ContactSubCategoryName = "No Account SubCategory";
                    NewSubCategoryIn.DivId = "NewSubIn" + Count;

                    AccountSubCategoryList NewSubCategoryOut = new AccountSubCategoryList();
                    NewSubCategoryOut.AccountSubCategory = new AccountSubCategory();
                    NewSubCategoryOut.AccountSubCategory.ContactSubCategoryName = "No Account SubCategory";
                    NewSubCategoryOut.DivId = "NewSubOut" + Count;

                    foreach (var Ledger in LedgerListTemp)
                    {

                        List<Voucher> VoucherLisInFlow = VoucherList.Where(x => x.LedgerId == Ledger.LedgerId && x.Type == Constants.DR && (x.VoucherFor == Constants.SALE_TYPE_SALE_INVOICE || x.VoucherFor == Constants.SALE_TYPE_SALE_EXCHANGE || x.VoucherFor == Constants.SALE_TYPE_SALE_VOUCHER || x.VoucherFor == Constants.SALE_TYPE_SALE_ORDER || x.VoucherFor == Constants.SALE_TYPE_WPS || x.VoucherFor == Constants.SALE_TYPE_PURCHASE_RETURN || x.VoucherFor == Constants.RECEIPT_VOUCHER)).ToList();
                        List<Voucher> VoucherLisOutFlow = VoucherList.Where(x => x.LedgerId == Ledger.LedgerId && ((x.Type == Constants.CR && (x.VoucherFor == Constants.PURCHASE_TYPE_PURCHASE_INVOICE || x.VoucherFor == Constants.PURCHASE_TYPE_PURCHASE_VOUCHER || x.VoucherFor == Constants.SALE_INVOICE_EXCHANGE)) || (x.Type == Constants.DR && x.VoucherFor == Constants.PAYMENT_VOUCHER))).ToList();
                        decimal InFlow = VoucherLisInFlow.Sum(x => x.Total);
                        decimal OutFlow = VoucherLisOutFlow.Sum(x => x.Total);
                        if (InFlow > 0)
                        {
                            LedgerList ListItem = new LedgerList();
                            ListItem.DivId = "InLedger" + Ledger.LedgerId;
                            ListItem.Ledger = Ledger;
                            ListItem.Total = InFlow;
                            NewSubCategoryIn.Total = NewSubCategoryIn.Total + ListItem.Total;
                            foreach (var Voucher in VoucherLisInFlow)
                            {
                                InvoiceList InvoiceListItem = new InvoiceList();
                                if (Voucher.VoucherFor == Constants.PURCHASE_TYPE_PURCHASE_INVOICE || Voucher.VoucherFor == Constants.PURCHASE_TYPE_PURCHASE_VOUCHER)
                                {
                                    InvoiceListItem.InvoiceNumber = Voucher.InvoiceNumber;
                                    InvoiceListItem.InvoiceId = Voucher.PurchaseInvoiceId ?? 0;
                                }
                                else if (Voucher.VoucherFor == Constants.SALE_TYPE_SALE_INVOICE || Voucher.VoucherFor == Constants.SALE_TYPE_SALE_EXCHANGE || Voucher.VoucherFor == Constants.SALE_TYPE_SALE_VOUCHER || Voucher.VoucherFor == Constants.SALE_TYPE_WPS || Voucher.VoucherFor == Constants.SALE_TYPE_PURCHASE_RETURN)
                                {
                                    InvoiceListItem.InvoiceNumber = Voucher.InvoiceNumber;
                                    InvoiceListItem.InvoiceId = Voucher.SaleInvoiceId ?? 0;
                                }
                                else if (Voucher.VoucherFor == Constants.SALE_TYPE_SALE_ORDER)
                                {
                                    InvoiceListItem.InvoiceNumber = Voucher.InvoiceNumber;
                                    InvoiceListItem.InvoiceId = Voucher.SalesOrderId ?? 0;
                                }
                                else if (Voucher.VoucherFor == Constants.PAYMENT_VOUCHER || Voucher.VoucherFor == Constants.RECEIPT_VOUCHER || Voucher.VoucherFor == Constants.JOURNAL_ENTRY || Voucher.VoucherFor == Constants.CONTRA_ENTRY)
                                {
                                    InvoiceListItem.InvoiceNumber = Voucher.VoucherNumber;
                                }
                                InvoiceListItem.Type = Voucher.VoucherFor;
                                InvoiceListItem.Total = Voucher.Total;
                                ListItem.InvoiceList.Add(InvoiceListItem);
                            }
                            NewSubCategoryIn.LedgerList.Add(ListItem);
                        }
                        if (OutFlow > 0)
                        {
                            LedgerList ListItem = new LedgerList();
                            ListItem.DivId = "OutLedger" + Ledger.LedgerId;
                            ListItem.Ledger = Ledger;
                            ListItem.Total = OutFlow;
                            NewSubCategoryOut.Total = NewSubCategoryOut.Total + ListItem.Total;
                            foreach (var Voucher in VoucherLisOutFlow)
                            {
                                InvoiceList InvoiceListItem = new InvoiceList();
                                if (Voucher.VoucherFor == Constants.PURCHASE_TYPE_PURCHASE_INVOICE || Voucher.VoucherFor == Constants.PURCHASE_TYPE_PURCHASE_VOUCHER)
                                {
                                    InvoiceListItem.InvoiceNumber = Voucher.InvoiceNumber;
                                    InvoiceListItem.InvoiceId = Voucher.PurchaseInvoiceId ?? 0;
                                }
                                else if (Voucher.VoucherFor == Constants.SALE_TYPE_SALE_INVOICE || Voucher.VoucherFor == Constants.SALE_TYPE_SALE_EXCHANGE || Voucher.VoucherFor == Constants.SALE_TYPE_SALE_VOUCHER || Voucher.VoucherFor == Constants.SALE_TYPE_WPS || Voucher.VoucherFor == Constants.SALE_TYPE_PURCHASE_RETURN)
                                {
                                    InvoiceListItem.InvoiceNumber = Voucher.InvoiceNumber;
                                    InvoiceListItem.InvoiceId = Voucher.SaleInvoiceId ?? 0;
                                }
                                else if (Voucher.VoucherFor == Constants.SALE_TYPE_SALE_ORDER)
                                {
                                    InvoiceListItem.InvoiceNumber = Voucher.InvoiceNumber;
                                    InvoiceListItem.InvoiceId = Voucher.SalesOrderId ?? 0;
                                }
                                else if (Voucher.VoucherFor == Constants.PAYMENT_VOUCHER || Voucher.VoucherFor == Constants.RECEIPT_VOUCHER || Voucher.VoucherFor == Constants.JOURNAL_ENTRY || Voucher.VoucherFor == Constants.CONTRA_ENTRY)
                                {
                                    InvoiceListItem.InvoiceNumber = Voucher.VoucherNumber;
                                }
                                InvoiceListItem.Type = Voucher.VoucherFor;
                                InvoiceListItem.Total = Voucher.Total;
                                ListItem.InvoiceList.Add(InvoiceListItem);
                            }
                            NewSubCategoryOut.LedgerList.Add(ListItem);
                        }
                    }
                    if (NewSubCategoryIn.Total > 0)
                    {
                        LedgerTypeInFlow.Total = LedgerTypeInFlow.Total + NewSubCategoryIn.Total;
                        LedgerTypeInFlow.AccountSubCategoryList.Add(NewSubCategoryIn);
                    }
                    if (NewSubCategoryOut.Total > 0)
                    {
                        LedgerTypeOutFlow.Total = LedgerTypeOutFlow.Total + NewSubCategoryOut.Total;
                        LedgerTypeOutFlow.AccountSubCategoryList.Add(NewSubCategoryOut);
                    }
                }
                

                if (LedgerTypeInFlow.Total > 0)
                {
                    Model.AccountCategoryList.Add(LedgerTypeInFlow);
                }
                if (LedgerTypeOutFlow.Total > 0)
                {
                    Model.AccountCategoryList.Add(LedgerTypeOutFlow);
                }
            }
            Model.TotalInFlow = Model.AccountCategoryList.Where(x => x.IsInFlow).Sum(x => x.Total);
            Model.TotalOutFlow = Model.AccountCategoryList.Where(x => !x.IsInFlow).Sum(x => x.Total);
            Model.TotalNetFlow = Model.TotalInFlow - Model.TotalOutFlow;
            return View(Model);
        }
    }
}