﻿using System.Web.Mvc;
using DataLayer;
using DataLayer.Models;
using DataLayer.Utils;
using DataLayer.ViewModels;
using System.Linq;
using System.Collections.Generic;
using System;
using ERP_System.Utils;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class ProductController : Controller
    {
        DbConnector db = new DbConnector();
        // GET: Product       
        [RBAC(AccessType = 2)]
        public ActionResult Index(string ErrorMsg)
        {
            ProductGridViewModel model = new ProductGridViewModel();
            List<int> SKUIds = db.Products.Where(x => x.CompanyId == db.CompanyId).Select(x => x.SKUMasterId).ToList();
            List<SKUMaster> SKUList = db.SKUMasters.Where(x => SKUIds.Contains(x.SKUMasterId)).ToList();
            List<Product> ProductList = db.Products.Where(x => SKUIds.Contains(x.SKUMasterId) && x.CompanyId == db.CompanyId).ToList();
            if (model.SKUIds != null)
            {
                foreach (var item in SKUList)
                {
                    ProductGridListViewModel ViewModel = new ProductGridListViewModel();
                    if (ProductList != null && ProductList.Count > 0)
                    {
                        ViewModel.PurchaseQuantity = ProductList.Where(x => x.SKUMasterId == item.SKUMasterId).Count();
                        ViewModel.SKU = item.SKU;
                        ViewModel.SKUId = item.SKUMasterId;
                        ViewModel.Quantity = ProductList.Where(x => x.SKUMasterId == item.SKUMasterId && x.IsSold == false).Count();
                        model.ProductGrid.Add(ViewModel);
                    }
                }
            }
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            return View(model);
        }

        public ActionResult Create(int? id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            Product Product = db.GetProductById(id.Value);
            Product.CategoryList = DropdownManager.GetCategoryList((id ?? 0));
            Product.SubCategoryList = DropdownManager.GetSubCategoryList((id ?? 0));
            Product.UnitList = DropdownManager.GetFreezedUnitList((Product.UnitId));
            Product.HSNList = DropdownManager.GetHSNList((id ?? 0));
            Product.LocationList = DropdownManager.GetLocationMasterList((id ?? 0));
            Product.BrandList = DropdownManager.GetBrandList((id ?? 0));
            Product.GSTList = DropdownManager.GetGSTList((id ?? 0));
            Product.SKUList = DropdownManager.GetSKUList((id ?? 0));
            Product.DiscountCodeList = DropdownManager.GetDiscountsAccessibleList(Product.DiscountsAccessible,null);
            return View(Product);
        }

        [HttpPost]
        public ActionResult Create(Product product)
        {
            if (product.DiscountIds != null)
            {
                List<int> templist = product.DiscountIds.OfType<int>().ToList();
                product.DiscountsAccessible = string.Join(",", templist);
            }
            else
            {
                product.DiscountsAccessible = "";
            }
            db.SaveProduct(product);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            PurchaseInvoiceItem item = db.GetPurchaseInvoiceItemById(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            else
            {
                PurchaseInvoiceDbUtil util = new PurchaseInvoiceDbUtil();
                if (util.RemoveInvoiceItem(item))
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index", new { ErrorMsg = MessageStore.DeleteError });
        }
        //[HttpGet]
        //public ActionResult AddInvoiceItem(int? invoiceId, int? invoiceItemId, bool? OpeningStock)
        //{
        //    PurchaseInvoiceItemViewModel Model = new PurchaseInvoiceItemViewModel();
        //    Model.PurchaseInvoiceItem = null;

        //    if (invoiceItemId.HasValue)
        //    {
        //        //db fetch
        //        Model.PurchaseInvoiceItem = db.PurchaseInvoiceItems.Where(x => x.PurchaseInvoiceItemId == invoiceItemId).FirstOrDefault();
        //        Model.PurchaseInvoiceItem.AttributeLinkingList = db.GetInvoiceItemAttribute(invoiceItemId);
        //        //Model.PurchaseInvoiceItem.DiscountIDTEmp = Model.PurchaseInvoiceItem.DiscountId;
        //        foreach (var i in Model.PurchaseInvoiceItem.AttributeLinkingList)
        //        {
        //            i.NameList = i.AttributeSetIds.Split(',').ToList();
        //            i.ValuesList = i.Value.Split(',').ToList();
        //        }
        //        Unit unit = db.GetUnitParents(Model.PurchaseInvoiceItem.UnitId);
        //        Model.PurchaseInvoiceItem.UnitList = DropdownManager.GetUnitConversionList(unit.UnitId);
        //    }
        //    else
        //    {
        //        Model.PurchaseInvoiceItem = new PurchaseInvoiceItem();
        //        if (invoiceId != null && invoiceId > 0)
        //        {
        //            Model.PurchaseInvoiceItem.PurchaseInvoiceId = invoiceId;
        //        }
        //        Model.PurchaseInvoiceItem.UnitList = DropdownManager.GetUnitMasterList(Model.PurchaseInvoiceItem.UnitId);
        //    }
        //    if (OpeningStock == true)
        //    {
        //        Model.PurchaseInvoiceItem.IsOpeningStock = true;
        //    }

        //    //populate select list
        //    Model.PurchaseInvoiceItem.CategoryList = DropdownManager.GetCategoryList(Model.PurchaseInvoiceItem.CategoryId);
        //    Model.PurchaseInvoiceItem.SubcategoryList = DropdownManager.GetSubCategoryList(Model.PurchaseInvoiceItem.SubCategoryId);
        //    Model.PurchaseInvoiceItem.SKUList = DropdownManager.GetSKUList(Model.PurchaseInvoiceItem.SKUMasterId);
        //    Model.PurchaseInvoiceItem.HSNList = DropdownManager.GetHSNList(Model.PurchaseInvoiceItem.HSNId);
        //    Model.PurchaseInvoiceItem.BrandList = DropdownManager.GetBrandList(Model.PurchaseInvoiceItem.BrandId);
        //    Model.PurchaseInvoiceItem.LocationList = DropdownManager.GetLocationMasterList(Model.PurchaseInvoiceItem.LocationId);
        //    Model.PurchaseInvoiceItem.DiscountCodeList = DropdownManager.GetDiscountsAccessibleList(null);
        //    Model.PurchaseInvoiceItem.BrandList = DropdownManager.GetBrandList(Model.PurchaseInvoiceItem.BrandId);
        //    Model.PurchaseInvoiceItem.GSTList = DropdownManager.GetGSTList(Model.PurchaseInvoiceItem.GSTId);
        //    Model.PurchaseInvoiceItem.ProductList = db.GetProductsListByInvoiceId(Model.PurchaseInvoiceItem.PurchaseInvoiceItemId);
        //    Dictionary<AttributeSet, List<SelectListItem>> AttributeDictionayList = null;
        //    Model.PurchaseInvoiceItem.AttributeSetListNew = db.GetAttributeSets();

        //    AttributeDictionayList = new Dictionary<AttributeSet, List<SelectListItem>>();
        //    for (int i = 0; i < Model.PurchaseInvoiceItem.AttributeSetListNew.Count; i++)
        //    {
        //        int Id = Model.PurchaseInvoiceItem.AttributeSetListNew[i].AttributeSetId;
        //        Model.PurchaseInvoiceItem.AttributeSetListNew[i].AttributeValueList = DropdownManager.GetAttributeValueList(Id);
        //        AttributeDictionayList.Add(Model.PurchaseInvoiceItem.AttributeSetListNew[i], Model.PurchaseInvoiceItem.AttributeSetListNew[i].AttributeValueList);
        //    }
        //    Model.PurchaseInvoiceItem.AttributeDictionayList = AttributeDictionayList;
        //    SessionManager.EmptyAttributeList();
        //    SessionManager.EmptyAttributeList();
        //    Model.Discount = new Discount();
        //    Model.Discount.DiscountList = db.GetDiscountAccessModel();
        //    return View(Model);


        //}

        //[HttpPost]
        //public ActionResult AddInvoiceItem(PurchaseInvoiceItemViewModel model, string btn)
        //{
        //    IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
        //    db.SavePurchaseInvoiceItem(model.PurchaseInvoiceItem);
        //    model.PurchaseInvoiceItem.AttributeLinkingList = SessionManager.GetAttributeList();
        //    if (model.PurchaseInvoiceItem.AttributeLinkingList != null)
        //    {
        //        foreach (var item in model.PurchaseInvoiceItem.AttributeLinkingList)
        //        {
        //            item.PurchaseInvoiceItemId = model.PurchaseInvoiceItem.PurchaseInvoiceItemId;
        //            item.AttributeLinkingId = 0;
        //        }
        //        db.SaveAttributeLinking(model.PurchaseInvoiceItem.AttributeLinkingList);
        //    }
        //    SessionManager.EmptyAttributeList();

        //    switch (btn)
        //    {
        //        case "Save":
        //            {
        //                return RedirectToAction("Index");
        //            }
        //        case "Save and Print Barcode":
        //            {
        //                InventoryController ControlObj = new InventoryController();
        //                //ControlObj.CreateProducts(model.PurchaseInvoiceItem.PurchaseInvoiceItemId);
        //                return RedirectToAction("GenerateBarcode", new { id = model.PurchaseInvoiceItem.PurchaseInvoiceItemId });
        //            }
        //    }
        //    return RedirectToAction("Index");
        //}

        public ActionResult Details(int id)
        {
            ProductGridListViewModel item = new ProductGridListViewModel();
            SKUMaster SKUMaster = db.SKUMasters.Where(x => x.SKUMasterId == id).FirstOrDefault();
            item.ProductList = db.Products.Where(x => x.SKUMasterId == id && x.CompanyId == db.CompanyId).ToList();
            List<int> PurchaseInvoiceItemIds = item.ProductList.Select(x => x.PurchaseInvoiceItemId).ToList();
            List<PurchaseInvoiceItem> PurchaseInvoiceItemList = db.PurchaseInvoiceItems.Where(x => PurchaseInvoiceItemIds.Contains(x.PurchaseInvoiceItemId)).ToList();
            List<int> PurchaseInvoiceIds = PurchaseInvoiceItemList.Select(x => x.PurchaseInvoiceId.HasValue ? x.PurchaseInvoiceId.Value : 0).ToList();
            List<PurchaseInvoice> PurchaseInvoiceList = db.PurchaseInvoices.Where(x => PurchaseInvoiceIds.Contains(x.PurchaseInvoiceId)).ToList();
            foreach (var item1 in item.ProductList)
            {
                item1.SKUMaster = SKUMaster;
                PurchaseInvoiceItem PurchaseInvoiceItem = PurchaseInvoiceItemList.Where(x => x.PurchaseInvoiceItemId == item1.PurchaseInvoiceItemId).FirstOrDefault();
                if (PurchaseInvoiceItem != null)
                {
                    item1.PurchaseInvoiceNumber = PurchaseInvoiceList.Where(x => x.PurchaseInvoiceId == PurchaseInvoiceItem.PurchaseInvoiceId).Select(x => x.PurchaseInvoiceNo).FirstOrDefault();
                    item1.PurchaseInvoiceId = PurchaseInvoiceList.Where(x => x.PurchaseInvoiceId == PurchaseInvoiceItem.PurchaseInvoiceId).Select(x => x.PurchaseInvoiceId).FirstOrDefault();
                }
            }
            return View(item);
        }
        
        public ActionResult GenerateBarcode(int id)
        {
            List<BarcodeViewModel> productList = db.GetBarcodesForProduct(id);
            return View(productList);
        }

        [HttpPost]
        public ActionResult _AddAttributes(AttributeLinking model, decimal? Quantity)
        {
            AttributesDbUtil util = new AttributesDbUtil();
            if (model.PurchaseInvoiceItemId > 0)
            {
                db.SaveSingleAttributeLinking(model);
            }
            else
            {
                SessionManager.SetAttribute(model);
            }
            decimal NewQuantity = 0;
            PurchaseInvoiceItemViewModel viewmodel = new PurchaseInvoiceItemViewModel();
            viewmodel.PurchaseInvoiceItem = new PurchaseInvoiceItem();
            viewmodel.PurchaseInvoiceItem.AttributeLinkingList = SessionManager.GetAttributeList(model.PurchaseInvoiceItemId);
            if (viewmodel.PurchaseInvoiceItem.AttributeLinkingList != null)
            {
                foreach (var item1 in viewmodel.PurchaseInvoiceItem.AttributeLinkingList)
                {
                    item1.ValuesList = item1.Value.Split(',').ToList();
                    item1.NameList = item1.AttributeSetIds.Split(',').ToList();
                    NewQuantity = NewQuantity + item1.Quantity;
                }
            }

            if (Quantity < NewQuantity)
            {
                if (model.PurchaseInvoiceItemId > 0)
                {
                    db.RemoveAttribute(model.AttributeLinkingId);
                }
                else
                {
                    SessionManager.RemoveAttribute(model.AttributeLinkingId);
                }
                viewmodel.PurchaseInvoiceItem.AttributeLinkingList = SessionManager.GetAttributeList(model.PurchaseInvoiceItemId);
                ViewBag.ErrorMsg = "Quantity Should Be Equal";
            }
            return PartialView(viewmodel);
        }

        [HttpPost]
        public ActionResult RemoveLinking(int LinkingId, int InvoiceId)
        {
            PurchaseInvoiceItemViewModel Model = new PurchaseInvoiceItemViewModel();
            Model.PurchaseInvoiceItem = new PurchaseInvoiceItem();
            Model.PurchaseInvoiceItem.AttributeLinkingList = SessionManager.GetAttributeList();
            if (Model.PurchaseInvoiceItem.AttributeLinkingList != null)
            {
                SessionManager.RemoveAttribute(LinkingId);
            }
            else
            {
                db.RemoveAttribute(LinkingId);
                Model.PurchaseInvoiceItem.AttributeLinkingList = db.GetInvoiceItemAttribute(InvoiceId);
                if (Model.PurchaseInvoiceItem.AttributeLinkingList != null)
                {
                    foreach (var i in Model.PurchaseInvoiceItem.AttributeLinkingList)
                    {
                        i.NameList = i.AttributeSetIds.Split(',').ToList();
                        i.ValuesList = i.Value.Split(',').ToList();
                    }
                }
            }
            return PartialView("_AddAttributes", Model);
        }

        [HttpPost]
        public JsonResult EditLinking(int LinkingId, int? InvoiceId)
        {
            AttributeLinking model = null;
            if (InvoiceId != 0)
            {
                //    model = db.AttributeLinkings.Where(x => x.AttributeLinkingId == LinkingId).FirstOrDefault();
                //}
                //else
                //{

            }
            model = SessionManager.GetAttributeList(LinkingId);
            return Json(model);
        }

        public ActionResult DeleteProduct(PurchaseInvoiceItem Model, string btn)
        {
            if (Model.ProductList != null)
            {
                List<int> ProductIds = Model.ProductList.Where(x => x.checkProduct == true).Select(x => x.ProductId).ToList();
                List<Product> ProductList = db.Products.Where(x => ProductIds.Contains(x.ProductId)).ToList();
                if (btn == "Delete")
                {
                    foreach (var item in ProductList.ToList())
                    {
                        int PurchaseInvoiceItemId = item.PurchaseInvoiceItemId;
                        db.RemoveSingleProduct(item);
                        if (db.Products.Where(x => x.PurchaseInvoiceItemId == PurchaseInvoiceItemId).Count() == 0)
                        {
                            db.Configuration.ProxyCreationEnabled = false;
                            PurchaseInvoiceItem PurchaseInvoiceItem = db.PurchaseInvoiceItems.Where(x => x.PurchaseInvoiceItemId == PurchaseInvoiceItemId).FirstOrDefault();
                            if (PurchaseInvoiceItem != null)
                            {
                                PurchaseInvoiceItem.IsProductCreated = false;
                                db.SavePurchaseInvoiceItem(PurchaseInvoiceItem);
                            }
                        }
                    }
                }
                else if (btn == "Add To Barcode Queue")
                {
                    foreach (var item in ProductList)
                    {
                        List<Product> ProductForBarcode = SessionManager.GetBarcodeQueueList();
                        bool isAdded = ProductForBarcode.Any(x => x.ProductId == item.ProductId);
                        if (!isAdded)
                        {
                            SessionManager.AddItemInBarcodeQueue(item);
                        }
                    }
                }
                else if (btn == "Print Barcode")
                {
                    List<Product> ProductForBarcode = new List<Product>();
                    foreach (var item in ProductList)
                    {
                        ProductForBarcode.Add(item);
                    }
                    TempData["List"] = ProductForBarcode;
                    return RedirectToAction("PrintBarcode", "Product");
                }
            }
            return RedirectToAction("Index");
        }

        public ActionResult PrintBarcodeQueue()
        {
            List<Product> BarcodeQueue = SessionManager.GetBarcodeQueueList();
            TempData["List"] = BarcodeQueue;
            return RedirectToAction("PrintBarcode");
        }

        public ActionResult PrintBarcode()
        {
            List<Product> BarcodeQueue = TempData["List"] as List<Product>;
            List<BarcodeViewModel> productList = db.GetBarcodesQueue(BarcodeQueue);
            return View(productList);
        }

        public ActionResult EmptyBarcodeQueue()
        {
            SessionManager.EmptySessionList(Constants.BARCODE_PRODUCTS);
            return RedirectToAction("Index");
        }

        public ActionResult BarcodeQueue()
        {
            BarcodeQueueViewModel model = new BarcodeQueueViewModel();
            model.ProductList = SessionManager.GetBarcodeQueueList();
            return View(model);
        }

        [HttpPost]
        public ActionResult BarcodeQueue(string SerialNumber)
        {
            BarcodeQueueViewModel model = new BarcodeQueueViewModel();
            model.ProductList = SessionManager.GetBarcodeQueueList();
            Product product = db.GetProductByNumber(SerialNumber);
            if (product == null)
            {
                ViewBag.ErrorMsg = "Product Not Found";
            }
            else
            {
                bool isAdded = model.ProductList.Any(x => x.ProductId == product.ProductId);
                if (isAdded)
                {
                    ViewBag.ErrorMsg = "Product is Already in Queue";
                }
                else
                {
                    SessionManager.AddItemInBarcodeQueue(product);
                    ViewBag.ErrorMsg = "Product Added to Queue";
                }
            }
            model.SerialNumber = "";
            model.ProductList = SessionManager.GetBarcodeQueueList();
            return View(model);
        }

        [HttpPost]
        public ActionResult RemoveItem(int ProductId)
        {
            BarcodeQueueViewModel model = new BarcodeQueueViewModel();
            SessionManager.RemoveItemFromBarcodeQueue(ProductId);
            model.ProductList = SessionManager.GetBarcodeQueueList();
            return PartialView("_BarcodeProductList", model);
        }
    }
}