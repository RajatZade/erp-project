﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer.ViewModels;
using DataLayer.Models;
using DataLayer;
using DataLayer.Utils;
using ERP_System.Utils;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class PaymentTypeController : Controller
    {
        DbConnector db = new DbConnector();
        [RBAC(AccessType = 0)]
        public ActionResult Index(int? id, string ErrorMsg)
        {
            PaymentTypeViewModel model = new PaymentTypeViewModel();
            if (model.PaymentTypeList == null)
            {
                model.PaymentTypeList = db.GetPaymentTypeList();
                Dictionary<int, Ledger> idVsLedger = db.GetLedgerDictionary(model.PaymentTypeList.Select(x => x.LedgerId ?? 0).ToList());
                foreach(var item in model.PaymentTypeList)
                {
                    if(item.LedgerId.HasValue)
                    {
                        item.Ledger = idVsLedger[item.LedgerId.Value];
                    }
                }
            }
            if (id.HasValue)
            {
                model.PaymentType = db.PaymentTypeById(id.Value);
            }
            else
            {
                model.PaymentType = new PaymentType();
            }
            model.PaymentType.LedgerList = DropdownManager.GetLedgerListForType(null,null);
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            return View(model);
        }

        [RBAC(AccessType = 2)]
        [HttpPost]
        public ActionResult Create(PaymentType PaymentType)
        {
            db.SavePaymentType(PaymentType);
            return RedirectToAction("Index");
        }

        [RBAC(AccessType = 3)]
        public ActionResult Delete(int id)
        {
            PaymentType PaymentType = db.PaymentTypeById(id);

            if (PaymentType == null)
            {
                return HttpNotFound();
            }
            else
            {
                if (db.RemovePaymentType(PaymentType))
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index", new { ErrorMsg = MessageStore.DeleteError });
        }
    }
}