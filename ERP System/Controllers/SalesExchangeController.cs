﻿using System;
using System.Linq;
using System.Web.Mvc;
using DataLayer;
using DataLayer.Utils;
using DataLayer.Models;
using ERP_System.Utils;
using System.Collections.Generic;
using DataLayer.ViewModels;
using System.Data.Entity;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class SalesExchangeController : Controller
    {
        SalesInvoiceHandler InvoiceHandler = new SalesInvoiceHandler();
        DbConnector db = new DbConnector();
        SaleExchangeHandler exchangeHandler = new SaleExchangeHandler();

        [RBAC(AccessType = 2)]
        public ActionResult Index(int? PageNo, string ErrorMsg)
        {
            SaleInvoiceViewModel model = new SaleInvoiceViewModel();
            model.SaleInvoiceLists = db.GetReturnSaleInvoice(PageNo, Constants.SALE_TYPE_SALE_EXCHANGE);
            Dictionary<int, string> IdVsType = db.GetTypeOfSaleInvoice(model.SaleInvoiceLists.Select(x => x.ReturnSaleInvoiceId));
            foreach(var item in model.SaleInvoiceLists)
            {
                if (IdVsType.ContainsKey(item.ReturnSaleInvoiceId ?? 0))
                {
                    item.ReturnType = IdVsType[item.ReturnSaleInvoiceId.Value];
                }
            }
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            return View(model);
        }

        // GET: SaleReturn
        [RBAC(AccessType = 0)]
        public ActionResult Exchange(int? id)
        {
            SaleInvoiceViewModel model = new SaleInvoiceViewModel();
            if (id.HasValue)
            {
                model.SaleInvoice = exchangeHandler.GetSaleInvoiceForCreateEdit(id.Value);
            }
            SessionManager.EmptySessionList(Constants.SALE_TYPE_SALE_EXCHANGE);
            return View(model);
        }

        [RBAC(AccessType = 1)]
        public ActionResult Edit(int id)
        {
            return RedirectToAction("Exchange", new { id = id });
        }

        [RBAC(AccessType = 3)]
        public ActionResult Delete(int id)
        {
            string errorMessage = exchangeHandler.DeleteInvoice(id);
            return RedirectToAction("Index", new { ErrorMsg = errorMessage });
        }
        [HttpPost]
        public ActionResult Exchange(SaleInvoice model)
        {
            db.Configuration.ProxyCreationEnabled = false;
            SaleInvoiceViewModel viewModel = new SaleInvoiceViewModel();
            SaleInvoiceItem SaleInvoiceItem = db.SaleInvoiceItems.Where(x => x.Product.SerialNumber == model.Search && x.SaleInvoice.CompanyId == db.CompanyId && x.Status == Constants.SALE_ITEM_SOLD).FirstOrDefault();
            if (SaleInvoiceItem != null)
            {
                model.Search = db.SaleInvoices.Where(x => x.SaleInvoiceId == SaleInvoiceItem.SaleInvoiceId && x.CompanyId == db.CompanyId).Select(x => x.SaleInvoiceNo).FirstOrDefault();
                viewModel.SaleInvoice = db.SaleInvoices.Where(x => x.SaleInvoiceId == SaleInvoiceItem.SaleInvoiceId && x.CompanyId == db.CompanyId && (x.Type == Constants.SALE_TYPE_SALE_INVOICE || x.Type == Constants.SALE_TYPE_SALE_EXCHANGE)).OrderByDescending(q => q.SaleInvoiceId).FirstOrDefault();
            }
            else
            {
                viewModel.SaleInvoice = db.SaleInvoices.Where(x => x.SaleInvoiceNo == model.Search && x.CompanyId == db.CompanyId && x.Type == Constants.SALE_TYPE_SALE_INVOICE).OrderByDescending(q => q.SaleInvoiceId).FirstOrDefault();
            }

            if (viewModel.SaleInvoice != null)
            {
                viewModel.SaleInvoice.ReturnSaleInvoiceId = viewModel.SaleInvoice.SaleInvoiceId;
                viewModel.SaleInvoice.ReturnSaleInvoiceNumber = viewModel.SaleInvoice.SaleInvoiceNo;
                viewModel.SaleInvoice.SaleInvoiceNo = null;
                viewModel.SaleInvoice.SaleInvoiceId = 0;
                viewModel.SaleInvoice.CompanyId = db.CompanyId ?? 0;
                viewModel.SaleInvoice.Type = Constants.SALE_TYPE_SALE_EXCHANGE;
                viewModel.SaleInvoice.SaleInvoiceItemList = null;
                viewModel.SaleInvoice.SaleMemoList = DropdownManager.GetDeliveryMemoList();
                viewModel.SaleInvoice.LocationList = DropdownManager.GetLocationMasterList(null);
                viewModel.SaleInvoice.SaleOrderList = DropdownManager.GetSaleOrderList(null, Enums.PurchaseOrderStatus.Closed);
                viewModel.SaleInvoice.SaleAccountList = DropdownManager.GetLedgerListForType(null, Constants.LEDGER_TYPE_COMPANY);
                viewModel.SaleInvoice.CustomerAccountList = DropdownManager.GetLedgerListForType(null, Constants.LEDGER_TYPE_USER);
                viewModel.SaleInvoice.SaleTypeList = DropdownManager.GetLedgerListForType(null, Constants.LEDGER_TYPE_TAX);
                viewModel.SaleInvoice.EmployeeAccountList = DropdownManager.GetEmployeeList(null);
                viewModel.SaleInvoice.PaymentTypeList = DropdownManager.GetPaymentTypeList(null);
                viewModel.SaleInvoice.DivisionList = DropdownManager.GetSessionDivisionSelectList();
                viewModel.SaleInvoice.BranchList = DropdownManager.GetSessionBranchSelectList();
                viewModel.SaleInvoice.TotalInvoiceValue = 0;
                viewModel.SaleInvoice.TotalTax = 0;
                viewModel.SaleInvoice.TotalDiscount = 0;
                viewModel.SaleInvoice.Adjustment = 0;
                viewModel.SaleInvoice.Subtotal = 0;
                viewModel.SaleInvoice.InvoicingDate = DateTime.Now;
                viewModel.SaleInvoice.IsInventoryUpdate = false;
                viewModel.SaleInvoice.IsLedgerUpdate = false;
                viewModel.SaleInvoice.IsPaymentDone = false;
            }
            else
            {
                ViewBag.ErrorMsg = "SaleInvoice Not Found";
            }
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AddItem(string ProdId, int? SaleInvoiceId, bool? IsWPS, int? ReturnSaleInvoiceId)
        {
            db.Configuration.ProxyCreationEnabled = false;
            SalesInvoiceHandler handler = new SalesInvoiceHandler();
            string message = string.Empty;
            SaleInvoiceViewModel model = new SaleInvoiceViewModel();
            model.SaleInvoice = new SaleInvoice();
            if (SaleInvoiceId > 0)
            {
                model.SaleInvoice.SaleInvoiceItemList = db.SaleInvoiceItems.Where(x => x.ReturnSaleInvoiceId == ReturnSaleInvoiceId && x.Status == Constants.SALE_ITEM_RETURN).ToList();
                var item = db.SaleInvoiceItems.Where(x => x.SaleInvoiceId == SaleInvoiceId).ToList();
                if (item != null)
                {
                    for (int i = 0; i < item.Count; i++)
                    {
                        model.SaleInvoice.SaleInvoiceItemList.Add(item[i]);
                    }
                }
            }
            else
            {
                model.SaleInvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_SALE_EXCHANGE);
            }
            Product product = db.GetProductByNumber(ProdId);
            if (product == null)
            {
                ViewBag.ErrorMsg = "The product is unavailable or already sold";
            }
            else
            {
                bool isAdded = model.SaleInvoice.SaleInvoiceItemList.Any(x => x.ProductId == product.ProductId);
                if (isAdded)
                {
                    ViewBag.ErrorMsg = "This product is already added in this invoice";
                }
                else
                {
                    if (SaleInvoiceId > 0)
                    {
                        ProductManager.AddInvoiceItem(product, SaleInvoiceId, IsWPS, Constants.SALE_TYPE_SALE_EXCHANGE,Constants.SALE_ITEM_SOLD);
                        model.SaleInvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_SALE_EXCHANGE);
                        model.SaleInvoice.SaleInvoiceItemList = db.SaleInvoiceItems.Where(x => x.ReturnSaleInvoiceId == ReturnSaleInvoiceId && x.Status == Constants.SALE_ITEM_RETURN).ToList();
                        var item = db.SaleInvoiceItems.Where(x => x.SaleInvoiceId == SaleInvoiceId).ToList();
                        if (item != null)
                        {
                            for (int i = 0; i < item.Count; i++)
                            {
                                model.SaleInvoice.SaleInvoiceItemList.Add(item[i]);
                            }
                        }
                    }
                    else
                    {
                        ProductManager.AddInvoiceItem(product, null, IsWPS, Constants.SALE_TYPE_SALE_EXCHANGE,Constants.SALE_ITEM_SOLD);
                        model.SaleInvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_SALE_EXCHANGE);
                    }
                }
            }
            model.SaleInvoice = exchangeHandler.Calculations(model.SaleInvoice);
            return PartialView("_ItemList", model);
        }

        [HttpPost]
        public ActionResult ReturnItem(string ProdId, int? SaleInvoiceId, int ReturnSaleInvoiceId)
        {
            db.Configuration.ProxyCreationEnabled = false;
            SaleInvoiceViewModel model = new SaleInvoiceViewModel();
            model.SaleInvoice = new SaleInvoice();
            if (SaleInvoiceId > 0)
            {
                model.SaleInvoice.SaleInvoiceItemList = InvoiceHandler.GetSaleInvoiceItems(SaleInvoiceId);
                List<SaleInvoiceItem> SaleInvoiceItemList = db.SaleInvoiceItems.Where(x => x.ReturnSaleInvoiceId == SaleInvoiceId).ToList();
                model.SaleInvoice.SaleInvoiceItemList.AddRange(SaleInvoiceItemList);
            }
            else
            {
                model.SaleInvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_SALE_EXCHANGE);
            }
            Product Product = db.Products.Where(x => x.SerialNumber == ProdId && x.CompanyId == db.CompanyId).FirstOrDefault();
            if (Product != null)
            {
                SaleInvoiceItem Item = model.SaleInvoice.SaleInvoiceItemList.Where(x => x.ProductId == Product.ProductId).FirstOrDefault();
                if (Item == null)
                {
                    Item = db.SaleInvoiceItems.Where(x => x.SaleInvoiceId == ReturnSaleInvoiceId && x.ProductId == Product.ProductId && x.Status == Constants.SALE_ITEM_SOLD).FirstOrDefault();
                    if (Item != null)
                    {
                        Item.Status = Constants.SALE_ITEM_RETURN;
                        Item.Product = Product;
                        if (SaleInvoiceId > 0)
                        {
                            SaleInvoiceItem SaleInvoiceItem = db.SaleInvoiceItems.Where(x => x.SaleInvoiceId == Item.SaleInvoiceId).FirstOrDefault();
                            SaleInvoiceItem.ReturnSaleInvoiceId = SaleInvoiceId;
                            Item = null;
                            db.SaveSaleInvoiceItem(SaleInvoiceItem);
                            model.SaleInvoice.SaleInvoiceItemList = InvoiceHandler.GetSaleInvoiceItems(SaleInvoiceId);
                            List<SaleInvoiceItem> SaleInvoiceItemList = db.SaleInvoiceItems.Where(x => x.ReturnSaleInvoiceId == SaleInvoiceId).ToList();
                            model.SaleInvoice.SaleInvoiceItemList.AddRange(SaleInvoiceItemList);
                        }
                        else
                        {
                            SessionManager.AddSaleInvoiceItem(Item, Constants.SALE_TYPE_SALE_EXCHANGE);
                            model.SaleInvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_SALE_EXCHANGE);
                        }

                    }
                    else
                    {
                        ViewBag.ErrorMsg1 = "The product is unavailable";
                    }
                }
                else
                {
                    ViewBag.ErrorMsg1 = "The product is Already in List";
                }
            }
            else
            {
                ViewBag.ErrorMsg1 = "The product is unavailable";
            }
            model.SaleInvoice = exchangeHandler.Calculations(model.SaleInvoice);
            return PartialView("_ItemList", model);
        }

        [HttpPost]
        public ActionResult RemoveReturnItem(int ProductId, int? SaleInvoiceId, int? ReturnSaleInvoiceId)
        {
            SaleInvoiceViewModel model = new SaleInvoiceViewModel();
            model.SaleInvoice = new SaleInvoice();

            if (ReturnSaleInvoiceId > 0)
            {
                SaleInvoiceItem saleInvoiceItem = db.SaleInvoiceItems.Where(x => x.ProductId == ProductId && x.ReturnSaleInvoiceId == ReturnSaleInvoiceId).FirstOrDefault();
                saleInvoiceItem.Status = Constants.SALE_ITEM_SOLD;
                saleInvoiceItem.ReturnSaleInvoiceId = null;
                db.SaveSaleInvoiceItem(saleInvoiceItem);
                model.SaleInvoice.SaleInvoiceItemList = InvoiceHandler.GetSaleInvoiceItems(ReturnSaleInvoiceId);
                List<SaleInvoiceItem> SaleInvoiceItemList = db.SaleInvoiceItems.Where(x => x.ReturnSaleInvoiceId == ReturnSaleInvoiceId).ToList();
            }
            else
            {
                SessionManager.RemoveItemFromList(ProductId, Constants.SALE_TYPE_SALE_EXCHANGE);
                model.SaleInvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_SALE_EXCHANGE);
            }
            model.SaleInvoice = exchangeHandler.Calculations(model.SaleInvoice);
            return PartialView("_ItemList", model);
        }

        [HttpPost]
        public ActionResult RemoveNewItem(int ProductId, int? SaleInvoiceId)
        {
            SaleInvoiceViewModel model = new SaleInvoiceViewModel();
            model.SaleInvoice = new SaleInvoice();
            if (SaleInvoiceId > 0)
            {
                InvoiceHandler.RemoveSalesInvoiceItem(SaleInvoiceId, null, null, ProductId);
                model.SaleInvoice.SaleInvoiceItemList = InvoiceHandler.GetSaleInvoiceItems(SaleInvoiceId);
                List<SaleInvoiceItem> SaleInvoiceItemList = db.SaleInvoiceItems.Where(x => x.ReturnSaleInvoiceId == SaleInvoiceId).ToList();
            }
            else
            {
                SessionManager.RemoveItemFromList(ProductId, Constants.SALE_TYPE_SALE_EXCHANGE);
                model.SaleInvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_SALE_EXCHANGE);
            }

            if (model.SaleInvoice.SaleInvoiceItemList != null)
            {
                model.SaleInvoice = SaleCalculator.Calculate(model.SaleInvoice);
            }
            return PartialView("_ItemList", model);
        }

        [HttpPost]
        public ActionResult UpdateProduct(SaleInvoiceItem model)
        {
            db.Configuration.ProxyCreationEnabled = false;
            SaleInvoiceViewModel model1 = new SaleInvoiceViewModel();
            model1.SaleInvoice = new SaleInvoice();
            if(model.SaleInvoiceId > 0)
            {
                int SaleInvoiceId = InvoiceHandler.UpDateSaleInvoiceItem(model);
                model1.SaleInvoice.SaleInvoiceItemList = InvoiceHandler.GetSaleInvoiceItems(model.SaleInvoiceId);
                List<SaleInvoiceItem> SaleInvoiceItemList = db.SaleInvoiceItems.Where(x => x.ReturnSaleInvoiceId == model.SaleInvoiceId).ToList();
                model1.SaleInvoice.SaleInvoiceItemList.AddRange(SaleInvoiceItemList);
            }
            else
            {
                SessionManager.UpdateOrderItem(model, Constants.SALE_TYPE_SALE_EXCHANGE);
                model1.SaleInvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_SALE_EXCHANGE);
            }
            model1.SaleInvoice = exchangeHandler.Calculations(model1.SaleInvoice);
            return PartialView("_ItemList", model1);
        }

        [RBAC(AccessType = 0)]
        public ActionResult SaveInvoice(SaleInvoice saleinvoice, string btn)
        {
            db.Configuration.ProxyCreationEnabled = false;
            switch (btn)
            {
                case "Hold":
                    {
                        saleinvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_SALE_EXCHANGE);
                        //saleinvoice = exchangeHandler.Calculations(saleinvoice);
                        saleinvoice = db.SaveSaleInvoiceReturn(saleinvoice);
                        exchangeHandler.UpdateSaleInvoiceExchange(saleinvoice.SaleInvoiceId);
                        return RedirectToAction("Index");
                    }
                case "Save":
                    {
                        saleinvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_SALE_EXCHANGE);
                        //saleinvoice = exchangeHandler.Calculations(saleinvoice);
                        saleinvoice = db.SaveSaleInvoiceReturn(saleinvoice);
                        exchangeHandler.UpdateSaleInvoiceExchange(saleinvoice.SaleInvoiceId);
                        exchangeHandler.SaveSaleInvoiceItemWhenExchange(saleinvoice.SaleInvoiceId);
                        return RedirectToAction("Index");
                    }
                case "Make Payment":
                    {
                        saleinvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_SALE_EXCHANGE);
                        //saleinvoice = exchangeHandler.Calculations(saleinvoice);
                        saleinvoice.SaleInvoiceItemList = null;
                        saleinvoice = db.SaveSaleInvoiceReturn(saleinvoice);
                        exchangeHandler.UpdateSaleInvoiceExchange(saleinvoice.SaleInvoiceId);
                        exchangeHandler.SaveSaleInvoiceItemWhenExchange(saleinvoice.SaleInvoiceId);
                        return RedirectToAction("PlaceOrder", new { id = saleinvoice.SaleInvoiceId });
                    }
            }
            return RedirectToAction("PlaceOrder", new { id = saleinvoice.SaleInvoiceId });
        }

        public ActionResult PlaceOrder(int? id)
        {
            SaleInvoice saleinvoice = null;
            if (id.HasValue)
            {
                saleinvoice = db.SaleInvoices.Include("SaleInvoiceItemList").Where(x => x.SaleInvoiceId == id).FirstOrDefault();
                List<SaleInvoiceItem> SaleInvoiceItemList = db.SaleInvoiceItems.Where(x => x.ReturnSaleInvoiceId == id).ToList();
                saleinvoice.SaleInvoiceItemList.AddRange(SaleInvoiceItemList);
            }
            saleinvoice.PaymentTypeList = DropdownManager.GetPaymentTypeList(null);
            //saleinvoice.BankAccountsList = DropdownManager.BankAccountList(null);
            return View(saleinvoice);
        }

        public ActionResult POS(int InvoiceId)
        {
            GenerateSalesInovice model = new GenerateSalesInovice();
            model.SalesInvoice = db.SaleInvoices.Include("SaleInvoiceItemList").Where(x => x.SaleInvoiceId == InvoiceId).FirstOrDefault();
            List<SaleInvoiceItem> SaleInvoiceItemList = db.SaleInvoiceItems.Where(x => x.ReturnSaleInvoiceId == InvoiceId).ToList();
            model.SalesInvoice.SaleInvoiceItemList.AddRange(SaleInvoiceItemList);

            Dictionary<int, SKUMaster> idVsSKU = db.GetSKUMasterDictionary(model.SalesInvoice.SaleInvoiceItemList.Select(x => x.SKUMasterId.Value).ToList());
            Dictionary<int, GST> idVsGST = db.GetGSTMasterDictionary(model.SalesInvoice.SaleInvoiceItemList.Select(x => x.GSTId).ToList());

            model.GstList = new List<POS_GST_LIST>();
            foreach (var Item in model.SalesInvoice.SaleInvoiceItemList)
            {
                Item.SKUMaster = idVsSKU[Item.SKUMasterId.Value];
                Item.GST = idVsGST[Item.GSTId];
                if(Item.Status == Constants.SALE_ITEM_SOLD)
                {
                    model.SalesInvoice.TotalSGST += (Item.SGST ?? 0);
                    model.SalesInvoice.TotalCGST += (Item.CGST ?? 0);
                    model.SalesInvoice.TotalIGST += (Item.IGST ?? 0);
                    if (Item.DiscountId == -1)
                    {
                        Item.SaleDiscountPercent = 0;
                    }
                    else
                    {
                        Item.SaleDiscountPercent = db.Discounts.Where(x => x.DiscountId == Item.DiscountId).Select(x => x.DiscountPercent).FirstOrDefault();
                    }
                    Item.PayableAmount = Item.BasicPrice.Value - Item.Discount;
                    model.TotalBasePrice = model.TotalBasePrice + Item.BasicPrice.Value;
                    model.SalesInvoice.TotalNotMapped = model.SalesInvoice.TotalNotMapped + Item.SubTotal;
                    POS_GST_LIST ListItem = model.GstList.Where(x => x.GSTId == Item.GSTId  && x.Status == Constants.SALE_ITEM_SOLD).FirstOrDefault();
                    if (ListItem == null)
                    {
                        ListItem = new POS_GST_LIST();
                        ListItem.Status = Constants.SALE_ITEM_SOLD;
                        ListItem.GST = Item.GST;
                        ListItem.GSTId = Item.GSTId;
                        ListItem.IGST = Item.IGST;
                        ListItem.SGST = Item.SGST;
                        ListItem.CGST = Item.CGST;
                        ListItem.Amount = Item.BasicPrice;
                        model.GstList.Add(ListItem);
                    }
                    else
                    {
                        ListItem.IGST = ListItem.IGST + Item.IGST;
                        ListItem.SGST = ListItem.SGST + Item.SGST;
                        ListItem.CGST = ListItem.CGST + Item.CGST;
                        ListItem.Amount = ListItem.Amount + Item.BasicPrice;
                    }
                    model.SalesInvoice.NumberOfProduct++;
                }
                else if(Item.Status == Constants.SALE_ITEM_RETURN)
                {
                    model.SalesInvoice.TotalSGST -= (Item.SGST ?? 0);
                    model.SalesInvoice.TotalCGST -= (Item.CGST ?? 0);
                    model.SalesInvoice.TotalIGST -= (Item.IGST ?? 0);
                    if (Item.DiscountId == -1)
                    {
                        Item.SaleDiscountPercent = 0;
                    }
                    else
                    {
                        Item.SaleDiscountPercent = db.Discounts.Where(x => x.DiscountId == Item.DiscountId).Select(x => x.DiscountPercent).FirstOrDefault();
                    }
                    Item.PayableAmount = Item.BasicPrice.Value - Item.Discount;
                    model.TotalBasePrice = model.TotalBasePrice - Item.BasicPrice.Value;
                    model.SalesInvoice.TotalNotMapped = model.SalesInvoice.TotalNotMapped + Item.SubTotal;
                    POS_GST_LIST ListItem = model.GstList.Where(x => x.GSTId == Item.GSTId && x.Status == Constants.SALE_ITEM_RETURN).FirstOrDefault();
                    if (ListItem == null)
                    {
                        ListItem = new POS_GST_LIST();
                        ListItem.GST = Item.GST;
                        ListItem.Status = Constants.SALE_ITEM_RETURN;
                        ListItem.GSTId = Item.GSTId;
                        ListItem.IGST = Item.IGST;
                        ListItem.SGST = Item.SGST;
                        ListItem.CGST = Item.CGST;
                        ListItem.Amount = Item.BasicPrice;
                        model.GstList.Add(ListItem);
                    }
                    else
                    {
                        ListItem.IGST = ListItem.IGST + Item.IGST;
                        ListItem.SGST = ListItem.SGST + Item.SGST;
                        ListItem.CGST = ListItem.CGST + Item.CGST;
                        ListItem.Amount = ListItem.Amount + Item.BasicPrice;
                    }
                    model.SalesInvoice.NumberOfProduct--;
                }
            }
            model.Salesman = db.Contacts.Where(x => x.ContactId == model.SalesInvoice.EmployeeId.Value).FirstOrDefault();
            model.GstList = model.GstList.OrderByDescending(x => x.GST.SGST).ToList();
            model.Ledger = db.Ledgers.Where(x => x.LedgerId == model.SalesInvoice.CustomerId).FirstOrDefault();
            model.Contact = db.Contacts.Where(x => x.ContactId == model.Ledger.ContactId).FirstOrDefault();
            if (model.Contact == null)
            {
                model.Contact = new Contact();
            }
            model.Company = SessionManager.GetSessionCompany();
            model.Branch = SessionManager.GetSessionBranch();
            model.PaymentType = db.GetPaymentTypeById(model.SalesInvoice.PaymentTypeId);
            model.SalesInvoice.TotalInWord = CommonUtils.NumberToWords(Convert.ToInt32(model.SalesInvoice.TotalWithAdjustment));
            return View(model);
        }

        public ActionResult GenerateInvoice(int InvoiceId)
        {
            GenerateSalesInovice model = new GenerateSalesInovice();
            model.SalesInvoice = db.SaleInvoices.Include("SaleInvoiceItemList").Where(x => x.SaleInvoiceId == InvoiceId).FirstOrDefault();
            List<SaleInvoiceItem> SaleInvoiceItemList = db.SaleInvoiceItems.Where(x => x.ReturnSaleInvoiceId == InvoiceId).ToList();
            model.SalesInvoice.SaleInvoiceItemList.AddRange(SaleInvoiceItemList);

            Dictionary<int, SKUMaster> idVsSKU = db.GetSKUMasterDictionary(model.SalesInvoice.SaleInvoiceItemList.Select(x => x.SKUMasterId.Value).ToList());
            Dictionary<int, GST> idVsGST = db.GetGSTMasterDictionary(model.SalesInvoice.SaleInvoiceItemList.Select(x => x.GSTId).ToList());

            foreach (var Item in model.SalesInvoice.SaleInvoiceItemList)
            {
                Item.SKUMaster = idVsSKU[Item.SKUMasterId.Value];
                Item.GST = idVsGST[Item.GSTId];
                if (Item.Status == Constants.SALE_ITEM_SOLD)
                {
                    model.SalesInvoice.TotalSGST += (Item.SGST ?? 0);
                    model.SalesInvoice.TotalCGST += (Item.CGST ?? 0);
                    model.SalesInvoice.TotalIGST += (Item.IGST ?? 0);
                }
                else if (Item.Status == Constants.SALE_ITEM_RETURN)
                {
                    model.SalesInvoice.TotalSGST -= (Item.SGST ?? 0);
                    model.SalesInvoice.TotalCGST -= (Item.CGST ?? 0);
                    model.SalesInvoice.TotalIGST -= (Item.IGST ?? 0);
                }
                if (Item.DiscountId == -1)
                {
                    Item.SaleDiscountPercent = 0;
                }
                else
                {
                    Item.SaleDiscountPercent = db.Discounts.Where(x => x.DiscountId == Item.DiscountId).Select(x => x.DiscountPercent).FirstOrDefault();
                }
            }
            model.Ledger = db.Ledgers.Where(x => x.LedgerId == model.SalesInvoice.CustomerId).FirstOrDefault();
            model.Contact = db.Contacts.Where(x => x.ContactId == model.Ledger.ContactId).FirstOrDefault();
            if (model.Contact == null)
            {
                model.Contact = new Contact();
            }
            //model.Shipping = db.GetShippingById(model.SalesInvoice.ShippingId);
            model.Company = SessionManager.GetSessionCompany();
            model.Branch = SessionManager.GetSessionBranch();
            model.PaymentType = db.GetPaymentTypeById(model.SalesInvoice.PaymentTypeId);
            model.SalesInvoice.TotalInWord = CommonUtils.NumberToWords(Convert.ToInt32(model.SalesInvoice.TotalInvoiceValue));
            model.SaleType = db.Ledgers.Where(x => x.LedgerId == model.SalesInvoice.SaleTypeId).Select(x => x.Name).FirstOrDefault();
            model.SalesMan = db.Ledgers.Where(x => x.LedgerId == model.SalesInvoice.EmployeeId).Select(x => x.Name).FirstOrDefault();
            model.BankLedger = db.Ledgers.Where(x => x.LedgerId == model.Company.LedgerId).FirstOrDefault();
            return View(model);
        }

        [HttpPost]
        public ActionResult PlaceOrder(SaleInvoice SaleInvoice, string btn)
        {
            SaleExchangeHandler Handler = new SaleExchangeHandler();
            Handler.UpdateLedger(SaleInvoice.SaleInvoiceId);
            switch (btn)
            {
                case "Print Invoice":
                    {
                        return RedirectToAction("GenerateInvoice", new { InvoiceId = SaleInvoice.SaleInvoiceId });
                    }
                case "Print WSP":
                    {
                        return RedirectToAction("GenerateInvoice", new { InvoiceId = SaleInvoice.SaleInvoiceId });
                    }
                case "Print POS":
                    {
                        return RedirectToAction("POS", new { InvoiceId = SaleInvoice.SaleInvoiceId });
                    }
                case "Save":
                    {
                        return RedirectToAction("Index");
                    }
            }
            SaleInvoice.PaymentTypeList = DropdownManager.GetPaymentTypeList(null);
            //SaleInvoice.BankAccountsList = DropdownManager.BankAccountList(null);
            return View(SaleInvoice);
        }

        public void Update(int id)
        {
            SaleInvoice saleinvoice = db.SaleInvoices.Where(x => x.SaleInvoiceId == id).FirstOrDefault();
            #region SaleLedger  
            Ledger SaleLedger = db.Ledgers.Where(x => x.Name == Constants.LEDGER_TYPE_SALE_RETURN_LEDGER).FirstOrDefault();
            if (SaleLedger != null)
            {
                Voucher SaleLedgerVoucher = new Voucher();
                SaleLedgerVoucher.LedgerId = SaleLedger.LedgerId;
                SaleLedgerVoucher.SaleInvoiceId = saleinvoice.SaleInvoiceId;
                if (saleinvoice.TotalWithAdjustment > 0)
                {
                    SaleLedgerVoucher.Total = saleinvoice.TotalWithAdjustment;
                    SaleLedgerVoucher.Type = Constants.CR;
                }
                else
                {
                    SaleLedgerVoucher.Total = Math.Abs(saleinvoice.TotalWithAdjustment);
                    SaleLedgerVoucher.Type = Constants.DR;
                }
                SaleLedgerVoucher.InvoiceNumber = saleinvoice.SaleInvoiceNo;
                SaleLedgerVoucher.EntryDate = saleinvoice.InvoicingDate;
                SaleLedgerVoucher.Particulars = "Sale Exchange Number :" + saleinvoice.SaleInvoiceNo;
                SaleLedgerVoucher.VoucherFor = Constants.SALE_TYPE_SALE_EXCHANGE;
                db.SaveVauchers(SaleLedgerVoucher);
            }
            #endregion
        }
    }
}