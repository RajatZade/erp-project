﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DataLayer.Utils;
using DataLayer.Models;
using DataLayer.ViewModels;
using ERP_System.Utils;
using PagedList;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class PurchaseOrderController : Controller
    {
        DbConnector db = new DbConnector();
        PurchaseOrderHandler handler = new PurchaseOrderHandler();
        AttributeHandler attributeHandler = new AttributeHandler();

        [RBAC(AccessType = 2)]
        public ActionResult Index(int? PageNo, string ErrorMsg)
        {
            PurchaseOrderViewModel model = new PurchaseOrderViewModel();
            model.PurchaseOrderLists = db.GetPurchaseOrder(PageNo);
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            return View(model);
        }

        [RBAC(AccessType = 0)]
        public ActionResult Create(int? id, string ErrorMsg)
        {
            PurchaseOrder purchaseOrder = handler.GetPurchaseOrderForCreateEdit(id);
            return View(purchaseOrder);
        }

        [RBAC(AccessType = 1)]
        public ActionResult Edit(int id)
        {
            return RedirectToAction("Create", new { id = id });
        }

        [RBAC(AccessType = 3)]
        public ActionResult Delete(int id)
        {
            PurchaseOrder purchaseOrder = db.PurchaseOrders.Include("PurchaseOrderItemList").Where(x => x.PurchaseOrderId == id).FirstOrDefault();
            if (purchaseOrder == null)
            {
                return HttpNotFound();
            }
            else
            {
                if (handler.DeletePurchaseOrder(purchaseOrder))
                {                    
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index", new { ErrorMsg = MessageStore.CantDeleteProductSold });
        }

        public ActionResult AddOrderItem(int orderId, int? orderItemId, string IsClone)
        {
            PurchaseOrderItem PurchaseOrderItem = handler.GetOrderItemForCreateEdit(orderId, orderItemId, IsClone);
            return View(PurchaseOrderItem);
        }

        [HttpPost]
        public ActionResult Create(PurchaseOrder purchaseOrder, string btn)
        {
            db.SavePurchaseOrder(purchaseOrder);
            if (btn == "Save")
            {
                return RedirectToAction("Create", new { id = purchaseOrder.PurchaseOrderId });
            }
            else if (btn == "Save Order")
            {
                return RedirectToAction("Index", new { id = purchaseOrder.PurchaseOrderId });
            }
            return RedirectToAction("Create", new { id = purchaseOrder.PurchaseOrderId });
        }

        [HttpPost]
        public ActionResult AddOrderItem(PurchaseOrderItem Modal, string btn)
        {
            Modal = handler.SavePurchaseOrderItem(Modal, btn);
            switch (btn)
            {
                case "Save":
                    {
                        return RedirectToAction("Create", new { id = Modal.PurchaseOrderId});
                    }
                case "Save and New":
                    {
                        ViewBag.ItemSaved = "Item Saved";
                        return RedirectToAction("AddOrderItem", new { orderId = Modal.PurchaseOrderId });
                    }
            }
            return null;
        }

        public ActionResult DeleteOrderItem(int id)
        {
            PurchaseOrderItem item = db.GetPurchaseOrderItemById(id);
            db.RemoveOrderItem(item);
            return RedirectToAction("Create", new { id = item.PurchaseOrderId });
        }

        [HttpPost]
        public ActionResult RemoveLinking(int LinkingId, int? OrderItemId)
        {
            PurchaseOrderItem PurchaseOrderItem = new PurchaseOrderItem();
            if (!OrderItemId.HasValue && OrderItemId.Value == 0)
            {
                SessionManager.RemoveAttribute(LinkingId);
                PurchaseOrderItem.AttributeLinkingList = SessionManager.GetAttributeList();
            }
            else
            {
                db.RemoveAttribute(LinkingId);
                PurchaseOrderItem.AttributeLinkingList = db.AttributeLinkings.Where(x => x.PurchaseOrderItemId == OrderItemId).ToList();
            }
            if (PurchaseOrderItem.AttributeLinkingList != null)
            {
                foreach (var i in PurchaseOrderItem.AttributeLinkingList)
                {
                    i.NameList = i.AttributeSetIds.Split(',').ToList();
                    i.ValuesList = i.Value.Split(',').ToList();
                }
            }
            return PartialView("_AddAttributes", PurchaseOrderItem);
        }

        [HttpPost]
        public ActionResult _AddAttributes(AttributeLinking model, decimal? Quantity)
        {
            PurchaseOrderItem PurchaseOrderItem = new PurchaseOrderItem();
            AttributesDbUtil util = new AttributesDbUtil();
            if (model.PurchaseOrderItemId > 0)
            {
                int PurchaseOrderItemId = model.PurchaseOrderItemId.Value;
                db.SaveSingleAttributeLinking(model);
                PurchaseOrderItem.AttributeLinkingList = db.AttributeLinkings.Where(x => x.PurchaseOrderItemId == PurchaseOrderItemId).ToList();
            }
            else
            {
                SessionManager.SetAttribute(model);
                PurchaseOrderItem.AttributeLinkingList = SessionManager.GetAttributeList(null);
            }
            decimal NewQuantity = 0;

            if (PurchaseOrderItem.AttributeLinkingList != null)
            {
                foreach (var item1 in PurchaseOrderItem.AttributeLinkingList)
                {
                    item1.ValuesList = item1.Value.Split(',').ToList();
                    item1.NameList = item1.AttributeSetIds.Split(',').ToList();
                    NewQuantity = NewQuantity + item1.Quantity;
                }
            }

            if (Quantity < NewQuantity)
            {
                if (model.PurchaseOrderItemId > 0)
                {
                    db.RemoveAttribute(model.AttributeLinkingId);                    
                }
                else
                {
                    SessionManager.RemoveAttribute(model.AttributeLinkingId);
                }
                PurchaseOrderItem.AttributeLinkingList.Remove(model);
                ViewBag.ErrorMsg = "Quantity Should Be Equal";
            }
            return PartialView(PurchaseOrderItem);
        }

        [HttpPost]
        public JsonResult EditLinking(int LinkingId, int? OrderItemId)
        {
            AttributeLinking model = null;
            if((OrderItemId ?? 0) > 0)
            {
                model = db.AttributeLinkings.Where(x => x.AttributeLinkingId == LinkingId).FirstOrDefault();
            }
            else
            {
                model = SessionManager.GetAttributeList(LinkingId);
            }            
            return Json(model);
        }
    }
}