﻿using DataLayer;
using DataLayer.Utils;
using DataLayer.Models;
using DataLayer.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;
using ERP_System.Utils;
using System.Collections.Generic;
using PaymentGateway;
using System.Configuration;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class SaleInvoiceController : BaseController
    {
        DbConnector db = new DbConnector();
        SalesInvoiceHandler handler = new SalesInvoiceHandler();

        // GET: PurchaseInvoice
        [RBAC(AccessType = 2)]
        public ActionResult Index(int? PageNo, string ErrorMsg)
        {
            SaleInvoiceViewModel model = new SaleInvoiceViewModel
            {
                SaleInvoiceLists = db.GetSaleInvoice(PageNo,Constants.SALE_TYPE_SALE_INVOICE)
            };
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            return View(model);
        }

        [RBAC(AccessType = 0)]
        public ActionResult Create(int? id, bool? IsWPS, string NewInvoice,int? SalesOrderId,int? SalesOrderItemId)
        {
            if (!string.IsNullOrEmpty(NewInvoice))
            {
                SessionManager.EmptySessionList(Constants.SALE_TYPE_SALE_INVOICE);
            }
            SaleInvoice saleinvoice = handler.GetSaleInvoiceForCreateEdit(id, IsWPS, SalesOrderId,SalesOrderItemId);
            if (id.HasValue)
            {
                saleinvoice.TotalNotMapped = saleinvoice.TotalInvoiceValue;
            }
            return View(saleinvoice);
        }

        [HttpPost]
        public ActionResult Create(SaleInvoice saleInvoice, string btn)
        {
            saleInvoice = handler.SaveSaleInvoice(saleInvoice, btn);
            switch (btn)
            {
                case "Hold":
                    {
                        return RedirectToAction("Index");
                    }
                case "Save":
                    {
                        return RedirectToAction("Index");
                    }
                case "Make Payment":
                    {
                        return RedirectToAction("PlaceOrder", new { id = saleInvoice.SaleInvoiceId });
                    }
                case "Email":
                    {
                        break;
                    }
                case "SMS":
                    {
                        break;
                    }
            }
            return RedirectToAction("Create", new { id = saleInvoice.SaleInvoiceId });
        }

        [RBAC(AccessType = 3)]
        public ActionResult DeleteInvoice(int id)
        {
            string errorMessage = handler.DeleteInvoice(id);
            return RedirectToAction("Index", new { ErrorMsg = errorMessage });
        }

        [HttpPost]
        public ActionResult GetSaleOrder(int id)
        {
            SalesOrder model = db.GetSalesOrderById(id);
            SaleInvoice saleInvoice = new SaleInvoice
            {
                CustomerId = model.CustomerId,
                BranchId = model.BranchId,
                LocationId = model.LocationId,
                DivisionId = model.DivisionId,
                SaleAccountId = model.SalesAccountId,
                ChangeHeader = true,
            };
            SessionManager.EmptySessionList(Constants.SALE_TYPE_SALE_INVOICE);
            saleInvoice.SaleInvoiceItemList = new List<SaleInvoiceItem>();
            List<SaleInvoiceItem> SaleInvoiceItemList = db.GetInvoiceItemsForOrder(id);
            foreach (var items in SaleInvoiceItemList)
            {
                SessionManager.AddSaleInvoiceItem(new SaleInvoiceItem(items), Constants.SALE_TYPE_SALE_INVOICE);
            }
            List<SalesOrderItem> SalesOrderItemList = db.SalesOrderItems.Where(x => x.SalesOrderId == id).ToList();
            List<int> SkuId = SalesOrderItemList.Select(x => x.SKUMasterId).ToList();
            List<Product> ProductToAdded = db.Products.Where(x => SkuId.Contains(x.SKUMasterId) && x.CompanyId == db.CompanyId && x.IsSold == false).ToList();
            
            foreach (var item in SalesOrderItemList)
            {
                List<Product> ProductVsSKU = ProductToAdded.Where(x => x.SKUMasterId == item.SKUMasterId).ToList();
                if (ProductVsSKU.Count() >= item.Quantity)
                {
                    for (int i = 0; i < item.Quantity; i++)
                    {
                        saleInvoice.SaleInvoiceItemList.Add(ProductManager.AddInvoiceItem(ProductVsSKU[i], null, null,Constants.SALE_TYPE_SALE_INVOICE, Constants.SALE_ITEM_SOLD));
                    }
                }
                else
                {
                    for (int i = 0; i < ProductVsSKU.Count; i++)
                    {
                        saleInvoice.SaleInvoiceItemList.Add(ProductManager.AddInvoiceItem(ProductVsSKU[i], null, null,Constants.SALE_TYPE_SALE_INVOICE, Constants.SALE_ITEM_SOLD));
                    }
                }
            }

            saleInvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_SALE_INVOICE);
            saleInvoice = SaleCalculator.Calculate(saleInvoice);
            return PartialView("_ItemList", saleInvoice);
        }

        [HttpPost]
        public ActionResult GetDeliveryMemo(int id)
        {
            SaleInvoice SaleMemo = db.SaleInvoices.Where(x => x.SaleInvoiceId == id).FirstOrDefault();
            List<SaleInvoiceItem> SaleInvoiceItemList = db.SaleInvoiceItems.Where(x => x.SaleInvoiceId == id).ToList();
            SessionManager.EmptySessionList(Constants.SALE_TYPE_SALE_INVOICE);
            SaleInvoice SaleInvoiceNew = new SaleInvoice
            {
                CustomerId = SaleMemo.CustomerId,
                LocationId = SaleMemo.LocationId,
                SaleAccountId = SaleMemo.SaleAccountId,
                SaleTypeId = SaleMemo.SaleTypeId,
                ChangeHeader = true,
            };
            SaleInvoiceNew.SaleInvoiceItemList = new List<SaleInvoiceItem>();
            foreach (var item in SaleInvoiceItemList)
            {
                SaleInvoiceItem SaleInvoiceItem = new SaleInvoiceItem(item);
                SessionManager.AddSaleInvoiceItem(SaleInvoiceItem, Constants.SALE_TYPE_SALE_INVOICE);
            }

            SaleInvoiceNew.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_SALE_INVOICE);
            SaleInvoiceNew = SaleCalculator.Calculate(SaleInvoiceNew);
            return PartialView("_ItemList", SaleInvoiceNew);
        }

        [HttpPost]
        public ActionResult _ItemList(string ProdId, int? SalesInvoiceId, bool? IsWPS)
        {
            string message = string.Empty;
            SaleInvoice saleInvoice = new SaleInvoice();
            //Query from appropriate channel depending upon SalesInvoiceid
            if (SalesInvoiceId.HasValue && SalesInvoiceId.Value == 0)
            {
                saleInvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_SALE_INVOICE);
            }
            else
            {
                saleInvoice.SaleInvoiceItemList = handler.GetSaleInvoiceItems(SalesInvoiceId);
            }
            //Check if product is already available in the current invoice
            //Check if product is not sold
            Product product = db.GetProductByNumber(ProdId);
            if (product == null)
            {
                ViewBag.ErrorMsg = "The product is unavailable or already sold";
            }
            else
            {
                bool isAdded = saleInvoice.SaleInvoiceItemList.Any(x => x.ProductId == product.ProductId);
                if (isAdded)
                {
                    ViewBag.ErrorMsg = "This product is already added in this invoice";
                }
                else
                {
                    //Add item to invoice
                    if (SalesInvoiceId.HasValue && SalesInvoiceId.Value == 0)
                    {
                        ProductManager.AddInvoiceItem(product, SalesInvoiceId, IsWPS, Constants.SALE_TYPE_SALE_INVOICE, Constants.SALE_ITEM_SOLD);
                        saleInvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_SALE_INVOICE);
                    }

                    else
                    {
                        saleInvoice.SaleInvoiceItemList.Add(ProductManager.AddInvoiceItem(product, SalesInvoiceId, IsWPS,Constants.SALE_TYPE_SALE_INVOICE, Constants.SALE_ITEM_SOLD));
                    }
                }
            }
            saleInvoice = SaleCalculator.Calculate(saleInvoice);
            return PartialView(saleInvoice);
        }

        [HttpPost]
        public ActionResult UpdateProduct(SaleInvoiceItem model)
        {
            SaleInvoice saleinvoice = new SaleInvoice();
            if (model.SaleInvoiceItemId > 0)
            {
                int SaleInvoiceId = handler.UpDateSaleInvoiceItem(model);
                saleinvoice.SaleInvoiceItemList = handler.GetSaleInvoiceItems(model.SaleInvoiceId);
            }
            else
            {
                SessionManager.UpdateOrderItem(model,Constants.SALE_TYPE_SALE_INVOICE);
                saleinvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_SALE_INVOICE);
            }

            if (saleinvoice.SaleInvoiceItemList != null)
            {
                saleinvoice = SaleCalculator.Calculate(saleinvoice);
            }
            return PartialView("_ItemList", saleinvoice);
        }

        public ActionResult PlaceOrder(int? id)
        {
            SaleInvoice saleinvoice = null;
            if (id.HasValue)
            {
                saleinvoice = db.SaleInvoices.Include("SaleInvoiceItemList").Where(x => x.SaleInvoiceId == id).FirstOrDefault();
            }
            foreach (var item in saleinvoice.SaleInvoiceItemList)
            {
                item.GST = db.GetGSTMasterById(item.GSTId);
            }
            if(saleinvoice.CustomerId == 61)
            {
                saleinvoice.PaymentTypeId = 1;
            }
            saleinvoice.PaymentTypeList = DropdownManager.GetPaymentTypeList(null);
            //saleinvoice.BankAccountsList = DropdownManager.BankAccountList(null);
            return View(saleinvoice);
        }

        [HttpPost]
        public ActionResult RemoveItem(int ProductId, int? SaleInvoiceId)
        {
            SaleInvoice saleinvoice = new SaleInvoice();
            if (SaleInvoiceId == null || SaleInvoiceId == 0)
            {
                SessionManager.RemoveItemFromList(ProductId,Constants.SALE_TYPE_SALE_INVOICE);
                saleinvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_SALE_INVOICE);
            }
            else
            {
                handler.RemoveSalesInvoiceItem(SaleInvoiceId, null,null, ProductId);
                saleinvoice.SaleInvoiceItemList = handler.GetSaleInvoiceItems(SaleInvoiceId);
            }

            if (saleinvoice.SaleInvoiceItemList != null)
            {
                saleinvoice = SaleCalculator.Calculate(saleinvoice);
            }
            return PartialView("_ItemList", saleinvoice);
        }

        [HttpPost]
        public ActionResult PlaceOrder(SaleInvoice SaleInvoice, string btn)
        {
            SaleInvoice item = db.GetSalesInvoiceById(SaleInvoice.SaleInvoiceId);
            if(item.IsLedgerUpdate == false)
            {

                if (SaleInvoice.PaymentTypeId == 5)
                {
                    PaymentGateWay model = new PaymentGateWay();
                    PaymentGateWayViewModel ViewModel = new PaymentGateWayViewModel();
                    Contact Contact = db.Contacts.Where(x => x.ContactId == item.Customer.ContactId).FirstOrDefault();
                    ViewModel.Email = Contact.Email;
                    ViewModel.Name = Contact.FirstName;
                    ViewModel.Phone = Contact.Phone;
                    ViewModel.Amount = item.TotalWithAdjustment.ToString();
                    ViewModel.OrderId = item.SaleInvoiceId.ToString();
                    ViewModel.OnSuccess = Constants.OnSuccessOfSaleInvoice;
                    ViewModel.OnFailure = Constants.OnFailure;
                    ViewModel.PaymentTypeId = SaleInvoice.PaymentTypeId.ToString();
                    model.GotoPayment(ViewModel, btn);
                }
                else if (SaleInvoice.PaymentTypeId == 7)
                {
                    handler.LedgerUpdate(SaleInvoice.SaleInvoiceId, SaleInvoice.PaymentTypeId ?? 0);
                    item.IsLedgerUpdate = true;
                    item.PaymentTypeId = SaleInvoice.PaymentTypeId;
                    item.Status = Constants.CREDIT_SALE;
                }
                else
                {
                    handler.LedgerUpdate(SaleInvoice.SaleInvoiceId, SaleInvoice.PaymentTypeId ?? 0);
                    item.IsLedgerUpdate = true;
                    item.PaymentTypeId = SaleInvoice.PaymentTypeId;
                    item.Status = Constants.PAYMENT_DONE;
                    item.Cash = SaleInvoice.Cash;
                    item.TenderChange = SaleInvoice.TenderChange;
                    item.ChequeNumber = SaleInvoice.ChequeNumber;
                }
                db.SaveSaleInvoiceEdit(item);
                db.SaveChanges();
            }
            switch (btn)
            {
                case "Print Invoice":
                    {
                        return RedirectToAction("GenerateInvoice", new { InvoiceId = SaleInvoice.SaleInvoiceId });
                    }
                case "Print WSP":
                    {
                        return RedirectToAction("GenerateInvoice", new { InvoiceId = SaleInvoice.SaleInvoiceId });
                    }
                case "Print POS":
                    {
                        return RedirectToAction("POS", new { InvoiceId = SaleInvoice.SaleInvoiceId });
                    }
                case "Save":
                    {
                        return RedirectToAction("Index");
                    }
            }
            return null;
        }

        [HttpPost]
        public ActionResult Return(FormCollection form)
        {
            PaymentGateWay model = new PaymentGateWay();
            try
            {
                string[] merc_hash_vars_seq;
                string merc_hash_string = string.Empty;
                string merc_hash = string.Empty;
                string SaleInvoiceId = string.Empty;
                string mode = string.Empty;
                string payuMoneyId = string.Empty;
                string txnid = string.Empty;
                string udf1 = string.Empty;
                string hash_seq = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
                string Status = form["status"].ToString();
                if (Status == "success")
                {

                    merc_hash_vars_seq = hash_seq.Split('|');
                    Array.Reverse(merc_hash_vars_seq);
                    merc_hash_string = ConfigurationManager.AppSettings["SALT"] + "|" + form["status"].ToString();
                    foreach (string merc_hash_var in merc_hash_vars_seq)
                    {
                        merc_hash_string += "|";
                        merc_hash_string = merc_hash_string + (form[merc_hash_var] ?? "");
                    }
                    Response.Write(merc_hash_string);
                    merc_hash = model.Generatehash512(merc_hash_string).ToLower();
                    if (merc_hash != form["hash"])
                    {
                        Response.Write("Hash value did not matched");
                    }
                    else
                    {
                        mode = Request.Form["mode"];
                        payuMoneyId = Request.Form["payuMoneyId"];
                        txnid = Request.Form["txnid"];
                        SaleInvoiceId = Request.Form["udf1"];
                        string btn = Request.Form["udf2"];
                        SaleInvoice SaleInvoice = db.GetSalesInvoiceById(Convert.ToInt32(SaleInvoiceId));
                        SaleInvoice.PaymentTypeId = 5;
                        SaleInvoice.Status = Constants.PAYMENT_DONE;
                        SaleInvoice.IsLedgerUpdate = true;
                        db.SaveSaleInvoiceEdit(SaleInvoice);
                        switch (btn)
                        {
                            case "Print Invoice":
                                {
                                    return RedirectToAction("GenerateInvoice", new { InvoiceId = SaleInvoice.SaleInvoiceId });
                                }
                            case "Print WSP":
                                {
                                    return RedirectToAction("GenerateInvoice", new { InvoiceId = SaleInvoice.SaleInvoiceId });
                                }
                            case "Print POS":
                                {
                                    return RedirectToAction("POS", new { InvoiceId = SaleInvoice.SaleInvoiceId });
                                }
                            case "Save":
                                {
                                    return RedirectToAction("Index");
                                }
                        }
                    }

                }
                else
                {
                    SaleInvoiceId = Request.Form["udf1"];
                    return RedirectToAction("Failure", "Checkout", new { id = SaleInvoiceId });
                }
            }

            catch (Exception ex)
            {
                Response.Write("<span style='color:red'>" + ex.Message + "</span>");

            }
            return null;
        }

        public ActionResult GenerateInvoice(int InvoiceId)
        {
            GenerateSalesInovice model = new GenerateSalesInovice
            {
                SalesInvoice = db.GetSalesInvoiceById(InvoiceId)
            };
            model.SalesInvoice.SaleInvoiceItemList = handler.GetSaleInvoiceItems(InvoiceId);
            model.SalesInvoice.TotalQuantity = 0;
            foreach (var Item in model.SalesInvoice.SaleInvoiceItemList)
            {
                if (Item.Product.SellInPartial != true)
                {
                    model.SalesInvoice.TotalQuantity = model.SalesInvoice.TotalQuantity + Item.Quantity;
                }
                else
                {
                    model.SalesInvoice.TotalQuantity = model.SalesInvoice.TotalQuantity + Item.ConversionValue.Value;
                }
                Item.GST = db.GetGSTMasterById(Item.GSTId);
                if (Item.SGST != null)
                {
                    model.SalesInvoice.TotalSGST += Item.SGST.Value;
                }
                if (Item.CGST != null)
                {
                    model.SalesInvoice.TotalCGST += Item.CGST.Value;
                }
                if (Item.IGST != null)
                {
                    model.SalesInvoice.TotalIGST += Item.IGST.Value;
                }
                if (Item.DiscountId == -1)
                {
                    Item.SaleDiscountPercent = 0;
                }
                else
                {
                    Item.SaleDiscountPercent = db.Discounts.Where(x => x.DiscountId == Item.DiscountId).Select(x => x.DiscountPercent).FirstOrDefault();
                }
            }
            model.Ledger = db.Ledgers.Where(x => x.LedgerId == model.SalesInvoice.CustomerId).FirstOrDefault();
            model.Contact = db.Contacts.Where(x => x.ContactId == model.Ledger.ContactId).FirstOrDefault();
            if (model.Contact == null)
            {
                model.Contact = new Contact();
            }
            model.SaleOrderNumber = db.SalesOrders.Where(x => x.SalesOrderId == model.SalesInvoice.SaleOrderId).Select(x => x.SaleOrderNumber).FirstOrDefault();
            model.SalesInvoice.DeliveryMemoNumber = db.SaleInvoices.Where(x => x.SaleInvoiceId == model.SalesInvoice.SaleMemoId).Select(x => x.SaleInvoiceNo).FirstOrDefault(); 
            model.Company = SessionManager.GetSessionCompany();
            model.Branch = SessionManager.GetSessionBranch();
            model.PaymentType = db.GetPaymentTypeById(model.SalesInvoice.PaymentTypeId);
            model.SalesInvoice.TotalInWord = CommonUtils.NumberToWords(Convert.ToInt32(model.SalesInvoice.TotalWithAdjustment));
            model.SaleType = db.Ledgers.Where(x => x.LedgerId == model.SalesInvoice.SaleTypeId).Select(x => x.Name).FirstOrDefault();
            model.SalesMan = db.Ledgers.Where(x => x.LedgerId == model.SalesInvoice.EmployeeId).Select(x => x.Name).FirstOrDefault();
            model.BankLedger = db.Ledgers.Where(x => x.LedgerId == model.Company.LedgerId).FirstOrDefault();
            return View(model);
        }

        
        [HttpPost]
        public JsonResult GetUnit(int id)
        {
            Unit model = db.GetUnitMasterById(id);
            var data = new { UnitParents = model.UnitParents, Conversion = model.Conversion, };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult POS(int InvoiceId)
        {
            GenerateSalesInovice model = new GenerateSalesInovice
            {
                SalesInvoice = db.GetSalesInvoiceById(InvoiceId)
            };
            model.SalesInvoice.SaleInvoiceItemList = handler.GetSaleInvoiceItems(InvoiceId);
            model.GstList = new List<POS_GST_LIST>();
            model.SalesInvoice.TotalQuantity = 0;
            foreach (var Item in model.SalesInvoice.SaleInvoiceItemList)
            {
                if(Item.Product.SellInPartial != true)
                {
                    model.SalesInvoice.TotalQuantity = model.SalesInvoice.TotalQuantity + Item.Quantity;
                }
                else
                {
                    model.SalesInvoice.TotalQuantity = model.SalesInvoice.TotalQuantity + Item.ConversionValue.Value;
                }
                Item.GST = db.GetGSTMasterById(Item.GSTId);
                Item.SKUMaster = db.SKUMasters.Where(x => x.SKUMasterId == Item.SKUMasterId).FirstOrDefault();
                if (Item.Product.AttributeLinkingId.HasValue)
                {
                    string Size = db.AttributeLinkings.Where(x => x.AttributeLinkingId == Item.Product.AttributeLinkingId).Select(x => x.Value).FirstOrDefault();
                    if (Size != null)
                    {
                        List<string> Values = Size.Split(',').ToList();
                        if (Values.Count > 2)
                        {
                            Item.Size = Values[1];
                        }
                    }
                }
                Item.PayableAmount = Item.BasicPrice.Value - Item.Discount;
                if (Item.SGST != null)
                {
                    model.SalesInvoice.TotalSGST += Item.SGST.Value;
                }
                if (Item.CGST != null)
                {
                    model.SalesInvoice.TotalCGST += Item.CGST.Value;
                }
                if (Item.IGST != null)
                {
                    model.SalesInvoice.TotalIGST += Item.IGST.Value;
                }
                if (Item.DiscountId == -1)
                {
                    Item.SaleDiscountPercent = 0;
                }
                else
                {
                    Item.SaleDiscountPercent = db.Discounts.Where(x => x.DiscountId == Item.DiscountId).Select(x => x.DiscountPercent).FirstOrDefault();
                }
                //model.TotalBasePrice = model.TotalBasePrice + Item.BasicPrice.Value;
                model.SalesInvoice.TotalNotMapped = model.SalesInvoice.TotalNotMapped + Item.SubTotal;
                POS_GST_LIST ListItem = model.GstList.Where(x => x.GSTId == Item.GSTId).FirstOrDefault();
                if (ListItem == null)
                {
                    ListItem = new POS_GST_LIST
                    {
                        GST = Item.GST,
                        GSTId = Item.GSTId,
                        IGST = Item.IGST,
                        SGST = Item.SGST,
                        CGST = Item.CGST,
                        Amount = Item.BasicPrice.Value,
                    };
                    model.TotalBasePrice = model.TotalBasePrice + ListItem.Amount.Value;
                    model.GstList.Add(ListItem);
                }
                else
                {
                    ListItem.IGST = ListItem.IGST + Item.IGST;
                    ListItem.SGST = ListItem.SGST + Item.SGST;
                    ListItem.CGST = ListItem.CGST + Item.CGST;
                    ListItem.Amount = ListItem.Amount + Item.BasicPrice.Value;
                    model.TotalBasePrice = model.TotalBasePrice + Item.BasicPrice.Value;
                }
            }
            model.Salesman = db.Ledgers.Where(x => x.ContactId == model.SalesInvoice.EmployeeId.Value).Select(x => x.Contact).FirstOrDefault();
            model.GstList = model.GstList.OrderByDescending(x => x.GST.SGST).ToList();
            model.Ledger = db.Ledgers.Where(x => x.LedgerId == model.SalesInvoice.CustomerId).FirstOrDefault();
            model.Contact = SessionManager.GetSessionUser().Contact;
            if (model.Contact == null)
            {
                model.Contact = new Contact();
            }
            //model.Shipping = db.GetShippingById(model.SalesInvoice.ShippingId);
            model.Company = SessionManager.GetSessionCompany();
            model.Branch = SessionManager.GetSessionBranch();
            model.PaymentType = db.GetPaymentTypeById(model.SalesInvoice.PaymentTypeId);
            model.SalesInvoice.TotalInWord = CommonUtils.NumberToWords(Convert.ToInt32(model.SalesInvoice.TotalInvoiceValue));
            return View(model);
        }

        public ActionResult MakePayment(int SaleInvoiceId, int PaymentTypeId)
        {
            handler.LedgerUpdate(SaleInvoiceId, PaymentTypeId);
            return RedirectToAction("Index");
        }

        public void start()
        {
            List<int> list = db.SaleInvoices.Where(x => x.CompanyId == db.CompanyId && x.Type == Constants.SALE_TYPE_SALE_INVOICE).Select(x => x.SaleInvoiceId).ToList();
            foreach(var item in list)
            {
                Update(item);
            }
        }
        public void Update(int id)
        {
            SaleInvoice item = db.GetSalesInvoiceById(id);
            #region SaleLedger  
            Ledger SaleLedger = db.Ledgers.Where(x => x.Name == Constants.LEDGER_TYPE_SALE_RETURN_LEDGER).FirstOrDefault();
            if (SaleLedger != null)
            {
                Voucher SaleLedgerVoucher = db.Vouchers.Where(x => x.LedgerId == SaleLedger.LedgerId && x.SaleInvoiceId == item.SaleInvoiceId && x.Type == Constants.CR).FirstOrDefault();
                if (SaleLedgerVoucher != null)
                {
                    SaleLedgerVoucher.Total = item.TotalWithAdjustment;
                    db.SaveVauchers(SaleLedgerVoucher);
                }
                else
                {
                    SaleLedgerVoucher = new Voucher();
                    SaleLedgerVoucher.LedgerId = SaleLedger.LedgerId;
                    SaleLedgerVoucher.SaleInvoiceId = item.SaleInvoiceId;
                    SaleLedgerVoucher.Total = item.TotalWithAdjustment;
                    SaleLedgerVoucher.Type = Constants.CR;
                    SaleLedgerVoucher.InvoiceNumber = item.SaleInvoiceNo;
                    SaleLedgerVoucher.EntryDate = item.InvoicingDate;
                    SaleLedgerVoucher.Particulars = "Sale Invoice Number :" + item.SaleInvoiceNo;
                    SaleLedgerVoucher.VoucherFor = Constants.SALE_TYPE_SALE_INVOICE;
                    db.SaveVauchers(SaleLedgerVoucher);
                }
            }
            #endregion
        }
    }
}