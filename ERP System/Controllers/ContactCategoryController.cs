﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer.ViewModels;
using DataLayer;
using DataLayer.Models;
using DataLayer.Utils;
using ERP_System.Utils;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class ContactCategoryController : Controller
    {
        DbConnector db = new DbConnector();
        // GET: ContactCategory
        [RBAC(AccessType = 2)]
        public ActionResult Index(int? AccountCategoryId, int? ContactSubCategoryid,string ErrorMsg)
        {
            ContactCategoryViewModel model = new ContactCategoryViewModel();
            if (model.ContactCategoryList == null)
            {
                model.ContactCategoryList = db.GetContactCategoryList(0);
            }
            if (model.ContactSubCategoryList == null)
            {
                model.ContactSubCategoryList = db.GetContactSubCategoryList();
            }
            if (AccountCategoryId.HasValue)
            {
                model.ContactCategory = db.GetContactCategoryById(AccountCategoryId.Value);
            }
            else
            {
                model.ContactCategory = new AccountCategory();
            }
            if (ContactSubCategoryid.HasValue)
            {
                model.ContactSubCategory = db.GetContactSubCategoryById(ContactSubCategoryid.Value);
            }
            else
            {
                model.ContactSubCategory = new AccountSubCategory();
            }
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            model.ContactSubCategory.ContactCategoryList = DropdownManager.GetContactCategoryList(model.ContactSubCategory.AccountCategoryId);
            return View(model);
        }

        [RBAC(AccessType = 0)]
        [HttpPost]
        public ActionResult CreateContactCategory(AccountCategory contactcategory)
        {
            db.SaveContactcategory(contactcategory);
            return RedirectToAction("Index");
        }

        [RBAC(AccessType = 3)]
        public ActionResult DeleteContactCategory(int? id)
        {
            AccountCategory contactcategory = db.ContactCategorys.Find(id);
            if (contactcategory == null)
            {
                return HttpNotFound();
            }
            else
            {
                if (db.RemoveContactCategory(contactcategory))
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index", new { ErrorMsg = MessageStore.DeleteError });
        }

        [RBAC(AccessType = 3)]
        public ActionResult DeleteContactSubCategory(int id)
        {
            AccountSubCategory contactsubcategory = db.ContactSubCategorys.Find(id);
            if (db.RemoveContactSubCategory(contactsubcategory))
            {
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index", new { ErrorMsg = MessageStore.DeleteError });
        }

        [RBAC(AccessType = 0)]
        [HttpPost]
        public ActionResult CreateContactSubCategory(AccountSubCategory contactSubCategory)
        {
            db.SaveContactSubCategory(contactSubCategory);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public JsonResult ExistingCategory(ContactCategoryViewModel model)
        {
            model.ContactCategory = db.ContactCategorys.Where(x => x.Name == model.ContactCategory.Name).FirstOrDefault();
            if (model.ContactCategory == null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ExistingSubCategory(ContactCategoryViewModel model)
        {
            model.ContactSubCategory = db.ContactSubCategorys.Where(x => x.ContactSubCategoryName == model.ContactSubCategory.ContactSubCategoryName).FirstOrDefault();
            if (model.ContactSubCategory == null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}