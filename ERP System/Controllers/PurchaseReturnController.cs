﻿using DataLayer;
using DataLayer.Utils;
using DataLayer.Models;
using DataLayer.ViewModels;
using ERP_System.Utils;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Collections.Generic;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class PurchaseReturnController : Controller
    {
        DbConnector db = new DbConnector();
        PurchaseReturnHandler handler = new PurchaseReturnHandler();

        // GET: PurchaseInvoice
        [RBAC(AccessType = 2)]
        public ActionResult Index(int? PageNo, string ErrorMsg)
        {
            SaleInvoiceViewModel model = new SaleInvoiceViewModel
            {
                SaleInvoiceLists = db.GetSaleInvoice(PageNo,Constants.SALE_TYPE_PURCHASE_RETURN)
            };
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            return View(model);
        }
        
        [RBAC(AccessType = 0)]
        public ActionResult Create(int? id)
        {
            SaleInvoice saleinvoice = handler.GetPurchaseReturn(id,Constants.SALE_TYPE_PURCHASE_RETURN);
            saleinvoice.TotalNotMapped = saleinvoice.TotalInvoiceValue;
            return View(saleinvoice);
        }

        [RBAC(AccessType = 1)]
        public ActionResult Edit(int id)
        {
            return RedirectToAction("Create", new { id = id });
        }

        [RBAC(AccessType = 3)]
        public ActionResult Delete(int id)
        {
            SalesInvoiceHandler salesInvoice = new SalesInvoiceHandler();
            string errorMessage = salesInvoice.DeleteInvoice(id);
            return RedirectToAction("Index", new { ErrorMsg = errorMessage });
        }

        [HttpPost]
        public ActionResult Create(SaleInvoice saleInvoice, string btn)
        {
            saleInvoice = handler.SaveSaleInvoice(saleInvoice, btn);
            switch (btn)
            {
                case "Hold":
                    {
                        return RedirectToAction("Index");
                    }
                case "Save":
                    {
                        return RedirectToAction("Index");
                    }
                case "Save & Print Invoice":
                    {
                        return RedirectToAction("GenerateInvoice", new { InvoiceId = saleInvoice.SaleInvoiceId });
                    }
                case "Email":
                    {
                        break;
                    }
                case "SMS":
                    {
                        break;
                    }
            }
            return RedirectToAction("Create", new { id = saleInvoice.SaleInvoiceId });
        }

        [HttpPost]
        public ActionResult _ItemList(string ProdId, int? SalesInvoiceId)
        {
            string message = string.Empty;
            SaleInvoice saleInvoice = new SaleInvoice();
            //Query from appropriate channel depending upon SalesInvoiceid
            if (SalesInvoiceId.HasValue && SalesInvoiceId.Value == 0)
            {
                saleInvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_PURCHASE_RETURN);
            }
            else
            {
                saleInvoice.SaleInvoiceItemList = handler.GetSaleInvoiceItems(SalesInvoiceId);
            }
            //Check if product is already available in the current invoice
            //Check if product is not sold
            Product product = db.GetProductByNumber(ProdId);
            if (product == null)
            {
                ViewBag.ErrorMsg = "The product is unavailable or already sold";
            }
            else
            {
                bool isAdded = saleInvoice.SaleInvoiceItemList.Any(x => x.ProductId == product.ProductId);
                if (isAdded)
                {
                    ViewBag.ErrorMsg = "This product is already added in this invoice";
                }
                else
                {
                    //Add item to invoice
                    if (SalesInvoiceId.HasValue && SalesInvoiceId.Value == 0)
                    {
                        ProductManager.AddPurchaseReturnItem(product, SalesInvoiceId, Constants.SALE_TYPE_PURCHASE_RETURN, Constants.SALE_ITEM_SOLD);
                        saleInvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_PURCHASE_RETURN);
                    }

                    else
                    {
                        saleInvoice.SaleInvoiceItemList.Add(ProductManager.AddPurchaseReturnItem(product, SalesInvoiceId,Constants.SALE_TYPE_PURCHASE_RETURN, Constants.SALE_ITEM_SOLD));
                    }
                }
            }
            saleInvoice = SaleCalculator.Calculate(saleInvoice);
            return PartialView(saleInvoice);
        }


        [HttpPost]
        public ActionResult RemoveItem(int ProductId, int? SaleInvoiceId)
        {
            SaleInvoice saleinvoice = new SaleInvoice();
            if (SaleInvoiceId == null || SaleInvoiceId == 0)
            {
                SessionManager.RemoveItemFromList(ProductId,Constants.SALE_TYPE_PURCHASE_RETURN);
                saleinvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_PURCHASE_RETURN);
            }
            else
            {
                handler.RemoveSalesInvoiceItem(SaleInvoiceId, null, ProductId);
                saleinvoice.SaleInvoiceItemList = handler.GetSaleInvoiceItems(SaleInvoiceId);
            }

            if (saleinvoice.SaleInvoiceItemList != null)
            {
                saleinvoice = SaleCalculator.Calculate(saleinvoice);
            }
            return PartialView("_ItemList", saleinvoice);
        }

        [HttpPost]
        public ActionResult UpdateProduct(SaleInvoiceItem model)
        {
            SaleInvoice saleinvoice = new SaleInvoice();
            if (model.SaleInvoiceItemId > 0)
            {
                int SaleInvoiceId = handler.UpDateSaleInvoiceItem(model);
                saleinvoice.SaleInvoiceItemList = handler.GetSaleInvoiceItems(model.SaleInvoiceId);
            }
            else
            {
                SessionManager.UpdatePucrahseReturnItem(model, Constants.SALE_TYPE_PURCHASE_RETURN);
                saleinvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_PURCHASE_RETURN);
            }

            if (saleinvoice.SaleInvoiceItemList != null)
            {
                saleinvoice = SaleCalculator.Calculate(saleinvoice);
            }
            return PartialView("_ItemList", saleinvoice);
        }

        public ActionResult GenerateInvoice(int InvoiceId)
        {
            GenerateSalesInovice model = new GenerateSalesInovice
            {
                SalesInvoice = db.GetSalesInvoiceById(InvoiceId)
            };
            model.SalesInvoice.SaleInvoiceItemList = handler.GetSaleInvoiceItems(InvoiceId);
            foreach (var Item in model.SalesInvoice.SaleInvoiceItemList)
            {
                Item.GST = db.GetGSTMasterById(Item.GSTId);
                if (Item.SGST != null)
                {
                    model.SalesInvoice.TotalSGST += Item.SGST.Value;
                }
                if (Item.CGST != null)
                {
                    model.SalesInvoice.TotalCGST += Item.CGST.Value;
                }
                if (Item.IGST != null)
                {
                    model.SalesInvoice.TotalIGST += Item.IGST.Value;
                }
                if (Item.DiscountId == -1)
                {
                    Item.SaleDiscountPercent = 0;
                }
                else
                {
                    Item.SaleDiscountPercent = db.Discounts.Where(x => x.DiscountId == Item.DiscountId).Select(x => x.DiscountPercent).FirstOrDefault();
                }
            }
            model.Ledger = db.Ledgers.Where(x => x.LedgerId == model.SalesInvoice.CustomerId).FirstOrDefault();
            model.Contact = db.Contacts.Where(x => x.ContactId == model.Ledger.ContactId).FirstOrDefault();
            if (model.Contact == null)
            {
                model.Contact = new Contact();
            }
            model.SaleOrderNumber = db.SalesOrders.Where(x => x.SalesOrderId == model.SalesInvoice.SaleOrderId).Select(x => x.SaleOrderNumber).FirstOrDefault();
            //model.Shipping = db.GetShippingById(model.SalesInvoice.ShippingId);
            model.Company = SessionManager.GetSessionCompany();
            model.Branch = SessionManager.GetSessionBranch();
            model.PaymentType = db.GetPaymentTypeById(model.SalesInvoice.PaymentTypeId);
            model.SalesInvoice.TotalInWord = CommonUtils.NumberToWords(Convert.ToInt32(model.SalesInvoice.TotalWithAdjustment));
            model.SaleType = db.Ledgers.Where(x => x.LedgerId == model.SalesInvoice.SaleTypeId).Select(x => x.Name).FirstOrDefault();
            model.SalesMan = db.Ledgers.Where(x => x.LedgerId == model.SalesInvoice.EmployeeId).Select(x => x.Name).FirstOrDefault();
            model.BankLedger = db.Ledgers.Where(x => x.LedgerId == model.Company.LedgerId).FirstOrDefault();
            return View(model);
        }

        public void Update(int InvoiceId)
        {
            handler.TempLedgerUpdate(InvoiceId);
        }

        [HttpPost]
        public ActionResult _ProductList(int SKUMasterId, string AttributeName)
        {
            SaleInvoice saleInvoice = new SaleInvoice();
            List<string> AttributeSet = AttributeName.Split(',').ToList();
            AttributeSet = AttributeSet.Where(x => x != "None").ToList();
            List<int> AttributeIds = new List<int>();
            List<int> ProductIds = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_WPS).Select(x => x.ProductId).ToList();
            List<AttributeLinking> AttributeList = new List<AttributeLinking>();
            if (AttributeSet.Count() > 0)
            {
                AttributeList = db.AttributeLinkings.ToList();
                foreach (var item in AttributeList)
                {
                    bool IsContainAllElement = true;
                    item.ValuesList = item.Value.Split(',').ToList();
                    foreach (var Value in AttributeSet)
                    {
                        if (!item.ValuesList.Contains(Value))
                        {
                            IsContainAllElement = false;
                            break;
                        }
                    }
                    if (IsContainAllElement == true)
                    {
                        AttributeIds.Add(item.AttributeLinkingId);
                    }
                }
            }
            if (AttributeIds.Count() == 0)
            {
                saleInvoice.WSPProductList = db.Products.Where(x => x.SKUMasterId == SKUMasterId && x.IsSold == false && x.CompanyId == db.CompanyId && !ProductIds.Contains(x.ProductId)).ToList();
            }
            else
            {
                saleInvoice.WSPProductList = db.Products.Where(x => x.SKUMasterId == SKUMasterId && x.IsSold == false && x.CompanyId == db.CompanyId && AttributeIds.Contains(x.AttributeLinkingId ?? 0) && !ProductIds.Contains(x.ProductId)).ToList();
            }

            List<int> AttributeLinkingIds = saleInvoice.WSPProductList.Select(x => x.AttributeLinkingId ?? 0).ToList();
            List<AttributeLinking> AttributeLinkingList = db.AttributeLinkings.Where(x => AttributeLinkingIds.Contains(x.AttributeLinkingId)).ToList();

            foreach (var item in saleInvoice.WSPProductList)
            {
                item.AttributeLinking = AttributeLinkingList.Where(x => x.AttributeLinkingId == item.AttributeLinkingId).FirstOrDefault();
                if (item.AttributeLinking != null)
                {
                    item.AttributeList = item.AttributeLinking.Value.Split(',').ToList();
                }
                else
                {
                    int Count = db.AttributeSets.Count();
                    item.AttributeList = new List<string>();
                    for (int i = 0; i < Count; i++)
                    {
                        item.AttributeList.Add("None");
                    }
                }
            }
            saleInvoice.AttributeSetListNew = db.GetAttributeSets();

            for (int i = 0; i < saleInvoice.AttributeSetListNew.Count; i++)
            {
                int Id = saleInvoice.AttributeSetListNew[i].AttributeSetId;
                saleInvoice.AttributeSetListNew[i].AttributeValueList = DropdownManager.GetAttributeValueList(Id);
            }
            SessionManager.SetWSPProductList(saleInvoice.WSPProductList);
            if (saleInvoice.WSPProductList.Count() == 0)
            {
                ViewBag.ErrorMsg = "No Product Found";
            }
            return PartialView(saleInvoice);
        }

        [HttpPost]
        public ActionResult AddProduct(int ProductId, int? SalesInvoiceId)
        {
            SaleInvoice saleInvoice = new SaleInvoice();
            //Query from appropriate channel depending upon SalesInvoiceid
            if (SalesInvoiceId.HasValue && SalesInvoiceId.Value == 0)
            {
                saleInvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_PURCHASE_RETURN);
            }
            else
            {
                saleInvoice.SaleInvoiceItemList = handler.GetSaleInvoiceItems(SalesInvoiceId);
            }

            saleInvoice.WSPProductList = SessionManager.GetWSPProductList();

            Product product = saleInvoice.WSPProductList.Where(x => x.ProductId == ProductId).FirstOrDefault();
            if (product != null)
            {
                saleInvoice.WSPProductList.Remove(product);
                SessionManager.SetWSPProductList(saleInvoice.WSPProductList);
                if (SalesInvoiceId.HasValue && SalesInvoiceId.Value == 0)
                {
                    saleInvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_PURCHASE_RETURN);
                }
                else
                {
                    saleInvoice.SaleInvoiceItemList = handler.GetSaleInvoiceItems(SalesInvoiceId);
                }

                saleInvoice = AddProductinInvoice(product, saleInvoice, SalesInvoiceId);
            }
            saleInvoice.AttributeSetListNew = db.GetAttributeSets();

            for (int i = 0; i < saleInvoice.AttributeSetListNew.Count; i++)
            {
                int Id = saleInvoice.AttributeSetListNew[i].AttributeSetId;
                saleInvoice.AttributeSetListNew[i].AttributeValueList = DropdownManager.GetAttributeValueList(Id);
            }
            return PartialView("_ProductList", saleInvoice);
        }

        public SaleInvoice AddProductinInvoice(Product product, SaleInvoice saleInvoice, int? SalesInvoiceId)
        {
            bool isAdded = saleInvoice.SaleInvoiceItemList.Any(x => x.ProductId == product.ProductId);
            if (isAdded)
            {
                ViewBag.ErrorMsg = "This product is already added in this invoice";
            }
            else
            {
                //Add item to invoice
                if (SalesInvoiceId.HasValue && SalesInvoiceId.Value == 0)
                {
                    ProductManager.AddPurchaseReturnItem(product, SalesInvoiceId, Constants.SALE_TYPE_PURCHASE_RETURN, Constants.SALE_ITEM_SOLD);
                    saleInvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_PURCHASE_RETURN);
                }

                else
                {
                    saleInvoice.SaleInvoiceItemList.Add(ProductManager.AddPurchaseReturnItem(product, SalesInvoiceId,Constants.SALE_TYPE_PURCHASE_RETURN, Constants.SALE_ITEM_SOLD));
                }
            }
            return saleInvoice;
        }

        [HttpPost]
        public ActionResult UpdateView(int SalesInvoiceId)
        {
            SaleInvoice saleInvoice = new SaleInvoice();
            if (SalesInvoiceId > 0)
            {
                saleInvoice.SaleInvoiceItemList = handler.GetSaleInvoiceItems(SalesInvoiceId);
            }
            else
            {
                saleInvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_PURCHASE_RETURN);
            }
            saleInvoice = SaleCalculator.Calculate(saleInvoice);
            return PartialView("_ItemList", saleInvoice);
        }

        [HttpPost]
        public ActionResult RemoveProduct()
        {
            SaleInvoice saleInvoice = new SaleInvoice();
            saleInvoice.WSPProductList = new List<Product>(); ;
            return PartialView("_ProductList", saleInvoice);
        }
    }
}