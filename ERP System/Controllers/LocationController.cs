﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer.Models;
using DataLayer.ViewModels;
using DataLayer;
using DataLayer.Utils;
using ERP_System.Utils;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class LocationController : Controller
    {
        DbConnector db = new DbConnector();
        // GET: Location
        [RBAC(AccessType = 2)]
        public ActionResult Index(int? id, string ErrorMsg)
        {
            LocationViewModel model = new LocationViewModel();
            model.LocationList = db.GetLocationList();
            if (model.LocationList == null)
            {
                model.LocationList = new List<Location>();
            }

            if (id.HasValue)
            {
                model.Location = db.GetLocatinListById(id.Value);
            }
            else
            {
                model.Location = new Location();
            }

            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            model.Location.BranchList = DropdownManager.GetBranchListByCompany(db.CompanyId);
            return View(model);
        }

        [RBAC(AccessType = 0)]
        [HttpPost]
        public ActionResult Create(Location Location)
        {
            db.SaveLocation(Location);
            return RedirectToAction("Index");
        }

        [RBAC(AccessType = 4)]
        public ActionResult Delete(int id)
        {
            Location location = db.Locations.Find(id);
            if (db.RemoveLocation(location))
            {
                return RedirectToAction("Index", new { ErrorMsg = MessageStore.DeleteItem });
            }
            return RedirectToAction("Index", new { ErrorMsg = MessageStore.DeleteError });
        }


    }
}