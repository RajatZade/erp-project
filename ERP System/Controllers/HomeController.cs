﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DataLayer;
using DataLayer.Models;
using DataLayer.Utils;
using DataLayer.ViewModels;
using ERP_System.Utils;

namespace Astrology.Areas.Console.Controllers
{
   //[CheckSession]
    public class HomeController : Controller
    {
        DbConnector db = new DbConnector();
        AttributesDbUtil util = new AttributesDbUtil();
        // GET: Console/Home

        [CheckSession]
        public ActionResult Index()
        {
            DashBoardViewModal modal = new DashBoardViewModal();
            modal.ProductionCount = db.JobSheets.Where(x => x.CompanyId == db.CompanyId).Count();
            modal.SaleInvoiceCount = db.SaleInvoices.Where(x => x.CompanyId == db.CompanyId).Count();
            modal.SaleOrderCount = db.SalesOrders.Where(x => x.CompanyId == db.CompanyId).Count();
            modal.ReceiptCount = db.Vouchers.Where(x => x.VoucherFor == Constants.RECEIPT_VOUCHER && x.CompanyId == db.CompanyId && x.Type == Constants.DR).Count();
            return View(modal);
        }
      
        //[ChildActionOnly]
        public ActionResult _NavBar(int? id)
        {
            NavBarViewModel Model = new NavBarViewModel();
            try
            {

                Model.Division = new Division();
                Model.Branch = new Branch();
                Company company1 = SessionManager.GetSessionCompany();
                Model.Division.CompanyList = DropdownManager.GetCompanyList(company1.CompanyId);
                if (id == null)
                {
                    Division Division1 = SessionManager.GetSessionDivision();
                    Branch branch1 = SessionManager.GetSessionBranch();
                    Model.Division.DivisionList = DropdownManager.GetDivisionList(Division1.DivisionId);
                    Model.Branch.BranchList = DropdownManager.GetBranchList(branch1.BranchId);
                }
                else
                {
                    Model.Division.DivisionList = DropdownManager.GetDivisionListByCompany(id);
                    Model.Branch.BranchList = DropdownManager.GetBranchListByCompany(id);
                }
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
            return PartialView(Model);
        }

        public ActionResult _NavBar1(int? id)
        {
            NavBarViewModel Model = new NavBarViewModel();
            Model.Division = new Division();
            Model.Branch = new Branch();
            Company company1 = SessionManager.GetSessionCompany();
            Model.Division.CompanyList = DropdownManager.GetCompanyList(company1.CompanyId);
            if (id == null)
            {
                Division Division1 = SessionManager.GetSessionDivision();
                Branch branch1 = SessionManager.GetSessionBranch();
                Model.Division.DivisionList = DropdownManager.GetDivisionList(Division1.DivisionId);
                Model.Branch.BranchList = DropdownManager.GetBranchList(branch1.BranchId);
            }
            else
            {
                Model.Division.DivisionList = DropdownManager.GetDivisionListByCompany(id);
                Model.Branch.BranchList = DropdownManager.GetBranchListByCompany(id);
            }
            return PartialView(Model);
        }

        public ActionResult SetSessionValue(int DivisionID, int BranchID, int CompanyID)
        {
            NavBarViewModel Model = new NavBarViewModel();
            Model.Division = db.GetDivisionListById(DivisionID);
            Model.Company = db.GetCompanyById(CompanyID);
            Model.Branch = db.GetBranchListById(BranchID);
            SessionManager.SetSessionBranch(Model.Branch);
            SessionManager.SetSessionCompany(Model.Company);
            SessionManager.SetSessionDivision(Model.Division);
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddCustomer(Ledger model)
        {
            var AccountCategory = db.ContactCategorys.Where(x => x.Name == model.CategoryName).FirstOrDefault();
            if (AccountCategory == null)
            {
                AccountCategory category = new AccountCategory();
                category.AccountCategoryId = 0;
                category.Name = model.CategoryName;
                db.SaveContactcategory(category);
                model.AccountCategoryId = category.AccountCategoryId;
            }
            else
            {
                model.AccountCategoryId = AccountCategory.AccountCategoryId;
            }

            if (model.SubCategoryName != null)
            {
                var AccountSubCategory = db.ContactSubCategorys.Where(x => x.ContactSubCategoryName == model.SubCategoryName).FirstOrDefault();
                if (AccountSubCategory == null)
                {
                    AccountSubCategory accountSubCategory = new AccountSubCategory();
                    accountSubCategory.AccountSubCategoryId = 0;
                    accountSubCategory.AccountCategoryId = model.AccountCategoryId;
                    accountSubCategory.ContactSubCategoryName = model.SubCategoryName;
                    db.SaveContactSubCategory(accountSubCategory);
                    model.AccountSubCategoryId = accountSubCategory.AccountSubCategoryId;
                }
                else
                {
                    model.AccountSubCategoryId = AccountSubCategory.AccountSubCategoryId;
                }
            }
            else
            {
                model.AccountSubCategoryId = null;
            }
            if (model.AccountCategoryId == 1 || model.AccountCategoryId == 47)
            {
                model.LedgerType = Constants.LEDGER_TYPE_PAYMENTTYPE;
            }
            if (model.Contact == null)
            {
                model.Contact = new Contact();
            }

            model.LedgerId = 0;
            var ID = string.Empty;
            List<SelectListItem> itemList = new List<SelectListItem>();
            model.Contact.FirstName = model.Name;
            db.SaveLedger(model);
            Voucher Voucher = new Voucher();
            Voucher.LedgerId = model.LedgerId;
            Voucher.Total = model.StartingBalance;
            Voucher.Type = model.Type;
            Voucher.VoucherFor = Constants.OPENING_BALANCE;
            Voucher.Particulars = Constants.OPENING_BALANCE;
            Voucher.EntryDate = DateTime.Now;
            db.SaveVauchers(Voucher);
            itemList.Add(new SelectListItem() { Value = model.LedgerId.ToString(), Text = model.Name });
            ID = model.LedgerId.ToString();
            return Json(new SelectList(itemList, "Value", "Text", ID));
        }

        [HttpPost]
        public JsonResult AddSKU(SKUMaster model)
        {
            if (model.CategoryId == 0)
            {
                Category Category = new Category();
                Category.Name = model.CategoryName;
                db.SaveCategory(Category);
                model.CategoryId = Category.CategoryId;
            }
            if (model.SubCategoryId == 0)
            {
                SubCategory SubCategory = new SubCategory();
                SubCategory.Name = model.SubCategoryName;
                SubCategory.CategoryId = model.CategoryId;
                db.SaveSubCategory(SubCategory);
                model.SubCategoryId = SubCategory.SubCategoryId;
            }
            //model.DiscountsAccessible = "";
            db.SaveSKUMaster(model);
            SKUMaster SKUMaster = db.GetSKUMasterById(model.SKUMasterId);

            List<SelectListItem> SKUList = new List<SelectListItem>();
            SKUList.Add(new SelectListItem() { Value = SKUMaster.SKUMasterId.ToString(), Text = model.SKU });

            List<SelectListItem> CategoryList1 = new List<SelectListItem>();
            CategoryList1.Add(new SelectListItem() { Value = SKUMaster.Category.CategoryId.ToString(), Text = SKUMaster.Category.Name });

            List<SelectListItem> SubCategoryList1 = new List<SelectListItem>();
            SubCategoryList1.Add(new SelectListItem() { Value = SKUMaster.SubCategory.SubCategoryId.ToString(), Text = SKUMaster.SubCategory.Name });

            var data = new { SKUList = new SelectList(SKUList, "Value", "Text"), CategoryList1 = new SelectList(CategoryList1, "Value", "Text"), SubCategoryList1 = new SelectList(SubCategoryList1, "Value", "Text") };

            return Json(data);
        }

        [HttpPost]
        public JsonResult AddLocation(Location model)
        {
            db.SaveLocation(model);
            List<SelectListItem> itemList = new List<SelectListItem>();
            itemList.Add(new SelectListItem() { Value = model.LocationId.ToString(), Text = model.LocationName });
            return Json(new SelectList(itemList, "Value", "Text"));
        }

        [HttpPost]
        public JsonResult AddInProcessStatus(JobSheetInProcessStatus model)
        {
            db.SaveJobSheetStatus(model);
            List<SelectListItem> itemList = new List<SelectListItem>();
            itemList.Add(new SelectListItem() { Value = model.JobSheetInProcessStatusId.ToString(), Text = model.Status });
            return Json(new SelectList(itemList, "Value", "Text"));
        }
        
        [HttpPost]
        public JsonResult AddServiceType(ServiceType model)
        {
            db.SaveServiceType(model);
            List<SelectListItem> itemList = new List<SelectListItem>();
            itemList.Add(new SelectListItem() { Value = model.ServiceTypeId.ToString(), Text = model.ServicesName });
            return Json(new SelectList(itemList, "Value", "Text"));
        }

        [HttpPost]
        public JsonResult AddSupplier(Ledger model)
        {
            var AccountCategory = db.ContactCategorys.Where(x => x.Name == model.CategoryName).FirstOrDefault();
            if (AccountCategory == null)
            {
                AccountCategory category = new AccountCategory();
                category.AccountCategoryId = 0;
                category.Name = model.CategoryName;
                db.SaveContactcategory(category);
                model.AccountCategoryId = category.AccountCategoryId;
            }
            else
            {
                model.AccountCategoryId = AccountCategory.AccountCategoryId;
            }

            if (model.SubCategoryName != null)
            {
                var AccountSubCategory = db.ContactSubCategorys.Where(x => x.ContactSubCategoryName == model.SubCategoryName).FirstOrDefault();
                if (AccountSubCategory == null)
                {
                    AccountSubCategory accountSubCategory = new AccountSubCategory();
                    accountSubCategory.AccountSubCategoryId = 0;
                    accountSubCategory.AccountCategoryId = model.AccountCategoryId;
                    accountSubCategory.ContactSubCategoryName = model.SubCategoryName;
                    db.SaveContactSubCategory(accountSubCategory);
                    model.AccountSubCategoryId = accountSubCategory.AccountSubCategoryId;
                }
                else
                {
                    model.AccountSubCategoryId = AccountSubCategory.AccountSubCategoryId;
                }               
            }
            else
            {
                model.AccountSubCategoryId = null;
            }
            if (model.AccountCategoryId == 1 || model.AccountCategoryId == 47)
            {
                model.LedgerType = Constants.LEDGER_TYPE_PAYMENTTYPE;
            }
            if (model.Contact == null)
            {
                model.Contact = new Contact();
            }

            model.LedgerId = 0;
            var ID = string.Empty;
            List<SelectListItem> itemList = new List<SelectListItem>();
            model.Name = model.Name;
            db.SaveLedger(model);
            Voucher Voucher = new Voucher();
            Voucher.LedgerId = model.LedgerId;
            Voucher.Total = model.StartingBalance;
            Voucher.Type = model.Type;
            Voucher.VoucherFor = Constants.OPENING_BALANCE;
            Voucher.Particulars = Constants.OPENING_BALANCE;
            Voucher.EntryDate = DateTime.Now;
            db.SaveVauchers(Voucher);
            itemList.Add(new SelectListItem() { Value = model.LedgerId.ToString(), Text = model.Name });
            ID = model.LedgerId.ToString();
            return Json(new SelectList(itemList, "Value", "Text", ID));
        }

        [HttpPost]
        public JsonResult AddGST(GST model)
        {
            db.SaveGSTMaster(model);
            List<SelectListItem> itemList = new List<SelectListItem>();
            itemList.Add(new SelectListItem() { Value = model.GSTId.ToString(), Text = model.Name });
            return Json(new SelectList(itemList, "Value", "Text", model.GSTId));
        }

        [HttpPost]
        public JsonResult AddAccountCategory(Ledger model)
        {
            var AccountCategory = db.ContactCategorys.Where(x => x.Name == model.CategoryName).FirstOrDefault();
            if (AccountCategory == null)
            {
                AccountCategory category = new AccountCategory();
                category.AccountCategoryId = 0;
                category.Name = model.CategoryName;
                db.SaveContactcategory(category);
                AccountCategory = db.ContactCategorys.Where(x => x.Name == model.CategoryName).FirstOrDefault();

            }
            var AccountSubCategory = db.ContactSubCategorys.Where(x => x.ContactSubCategoryName == model.SubCategoryName).FirstOrDefault();
            if (AccountSubCategory == null)
            {
                AccountSubCategory accountSubCategory = new AccountSubCategory();
                accountSubCategory.AccountSubCategoryId = 0;
                accountSubCategory.AccountCategoryId = AccountCategory.AccountCategoryId;
                accountSubCategory.ContactSubCategoryName = model.SubCategoryName;
                db.SaveContactSubCategory(accountSubCategory);
                AccountSubCategory = db.ContactSubCategorys.Where(x => x.ContactSubCategoryName == model.SubCategoryName).FirstOrDefault();
            }
            model.AccountCategoryId = AccountCategory.AccountCategoryId;
            model.AccountSubCategoryId = AccountSubCategory.AccountSubCategoryId;
            model.Name = model.Name;
            db.SaveLedger(model);

            List<SelectListItem> itemList = new List<SelectListItem>();
            itemList.Add(new SelectListItem() { Value = model.LedgerId.ToString(), Text = model.Name });
            return Json(new SelectList(itemList, "Value", "Text", model.LedgerId));

        }

        [HttpPost]
        public JsonResult AddBrand(Brand model)
        {
            var Brand = db.Brands.Where(x => x.BrandName == model.BrandName && x.CompanyId == db.CompanyId).FirstOrDefault();
            if (Brand == null)
            {
                db.SaveBrand(model);
                List<SelectListItem> itemList = new List<SelectListItem>();
                itemList.Add(new SelectListItem() { Value = model.BrandId.ToString(), Text = model.BrandName });
                return Json(new SelectList(itemList, "Value", "Text", model.BrandId));
            }
            return Json(false);
        }

        [HttpPost]
        public JsonResult AddHSN(HSN model)
        {
            db.SaveHSNMaster(model);
            List<SelectListItem> itemList = new List<SelectListItem>();
            itemList.Add(new SelectListItem() { Value = model.HSNId.ToString(), Text = model.Code });
            return Json(new SelectList(itemList, "Value", "Text", model.HSNId));
        }

        [HttpPost]
        public JsonResult AddUnit(Unit model)
        {
            model.UnitParents = model.UnitValue;
            model.Conversion = 1;
            db.SaveUnitMaster(model);
            model.UnitTypeId = model.UnitId;
            db.SaveUnitMaster(model);
            List<SelectListItem> itemList = new List<SelectListItem>();
            itemList.Add(new SelectListItem() { Value = model.UnitId.ToString(), Text = model.UnitValue });
            return Json(new SelectList(itemList, "Value", "Text", model.UnitId));
        }

        //[HttpPost]
        //public JsonResult AddDiscount(PurchaseInvoiceItemViewModel viewModel)
        //{
        //    int? DiscountId = null;
        //    List<int> templist = viewModel.Discount.DiscountList.Where(y => y.IsSelected).Select(x => x.DiscountId).ToList();
        //    if (viewModel.Discount.DiscountPercent.HasValue && viewModel.Discount.DiscountPercent.HasValue && viewModel.Discount.DiscountCode != null)
        //    {
        //        viewModel.Discount.IsActive = true;
        //        db.SaveDiscount(viewModel.Discount);
        //        templist.Add(viewModel.Discount.DiscountId);
        //        DiscountId = viewModel.Discount.DiscountId;
        //    }

        //    SKUMaster sSKUMaster = db.GetSKUMasterById(viewModel.Discount.SKUMaster.SKUMasterId);
        //    //sSKUMaster.DiscountsAccessible = string.Join(",", templist);
        //    db.SaveSKUMaster(sSKUMaster);
        //    //sSKUMaster.DiscountCodeList = DropdownManager.GetDiscountListBySKU(DiscountId, viewModel.Discount.SKUMaster.SKUMasterId);
        //    //var data = new
        //    //{
        //    //    DiscountCodeList = new SelectList(sSKUMaster.DiscountCodeList, "Value", "Text"),
        //    //    DiscountId = viewModel.Discount.DiscountId,
        //    //    Discount = viewModel.Discount,
        //    //};
        //    return Json(data);
        //}

        public ActionResult _SaleAccount(int? id)
        {
            Ledger ledger = new Ledger();
            //ledger.UserList = DropdownManager.GetUserList(null);
            ledger.ContactCategoryList = DropdownManager.GetContactCategoryList(null);
            return PartialView(ledger);
        }

        public JsonResult CreateLedger(Ledger ledger)
        {
            db.SaveLedger(ledger);
            List<SelectListItem> itemList = new List<SelectListItem>();
            itemList.Add(new SelectListItem() { Value = ledger.LedgerId.ToString(), Text = ledger.Name });
            return Json(new SelectList(itemList, "Value", "Text", ledger.LedgerId));
        }

        //[HttpPost]
        //public ActionResult _Discount(int id)
        //{
        //    PurchaseInvoiceItemViewModel item = new PurchaseInvoiceItemViewModel();
        //    item.PurchaseInvoiceItem = new PurchaseInvoiceItem();
        //    item.Discount = new Discount
        //    {
        //        SKUMaster = db.SKUMasters.Where(x => x.SKUMasterId == id).FirstOrDefault(),
        //        DiscountList = db.GetDiscountAccessModel()
        //    };
        //    List<string> SkuDiscountList = item.Discount.SKUMaster.DiscountsAccessible.Split(',').ToList();
        //    foreach (var Discount in item.Discount.DiscountList)
        //    {
        //        if (SkuDiscountList.Contains(Discount.DiscountId.ToString()))
        //        {
        //            Discount.IsSelected = true;
        //        }
        //    }
        //    return PartialView(item);
        //}

        [HttpPost]
        public JsonResult AddShippingType(Shipping model)
        {
            var Shipping = db.Shippings.Where(x => x.ShippingType == model.ShippingType).FirstOrDefault();
            if (Shipping == null)
            {
                db.SaveShippingMaster(model);
                List<SelectListItem> itemList = new List<SelectListItem>();
                itemList.Add(new SelectListItem() { Value = model.ShippingId.ToString(), Text = model.ShippingType });
                return Json(new SelectList(itemList, "Value", "Text"));
            }
            return Json(false);
        }

        public JsonResult AddMeasurment(Measurment model)
        {
            var Measurment = db.Measurments.Where(x => x.MeasurmentType == model.MeasurmentType).FirstOrDefault();
            if (Measurment == null)
            {
                db.SaveMeasurment(model);
                List<SelectListItem> itemList = new List<SelectListItem>();
                itemList.Add(new SelectListItem() { Value = model.MeasurmentId.ToString(), Text = model.MeasurmentType });
                return Json(new SelectList(itemList, "Value", "Text"));
            }
            return Json(false);
        }
        [HttpPost]
        public JsonResult AccountCategory(string Prefix)
        {
            //Note : you can bind same list from database  
            List<AccountCategory> ObjList = db.GetContactCategoryList(null);
            var result = new List<KeyValuePair<string, string>>();
            IList<SelectListItem> List = DropdownManager.GetContactCategoryList(0);
            foreach (var item in List)
            {
                result.Add(new KeyValuePair<string, string>(item.Value.ToString(), item.Text));
            }
            var result3 = result.Where(s => s.Value.ToLower().Contains(Prefix.ToLower())).Select(w => w).Take(10).ToList();
            return Json(result3, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AccountSubCategory(string Prefix, int? CategoryId)
        {
            var result = new List<KeyValuePair<string, string>>();
            IList<SelectListItem> List = DropdownManager.GetContactSubCategoryList(CategoryId);
            foreach (var item in List)
            {
                result.Add(new KeyValuePair<string, string>(item.Value.ToString(), item.Text));
            }
            var result3 = result.Where(s => s.Value.ToLower().Contains(Prefix.ToLower())).Select(w => w).Take(10).ToList();
            return Json(result3, JsonRequestBehavior.AllowGet);
        }



        public JsonResult GetCategoryList(string Prefix)
        {
            var result = new List<KeyValuePair<string, string>>();
            IList<SelectListItem> List = DropdownManager.GetCategoryList(0);
            foreach (var item in List)
            {
                result.Add(new KeyValuePair<string, string>(item.Value.ToString(), item.Text));
            }
            var result3 = result.Where(s => s.Value.ToLower().Contains(Prefix.ToLower())).Select(w => w).Take(10).ToList();
            return Json(result3, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSubCategoryList(string Prefix, int? CategoryId)
        {
            var result = new List<KeyValuePair<string, string>>();
            IList<SelectListItem> List = DropdownManager.GetSubCategoryListByCategory(CategoryId);
            foreach (var item in List)
            {
                result.Add(new KeyValuePair<string, string>(item.Value.ToString(), item.Text));
            }
            var result3 = result.Where(s => s.Value.ToLower().Contains(Prefix.ToLower())).Select(w => w).Take(10).ToList();
            return Json(result3, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddAccountCategory(AccountCategory model)
        {
            bool otpt = false;
            otpt = db.SaveContactcategory(model);
            //var result = new List<KeyValuePair<string, string>>();
            List<SelectListItem> itemList = new List<SelectListItem>();
            itemList.Add(new SelectListItem() { Value = model.AccountCategoryId.ToString(), Text = model.Name });
            return Json(new SelectList(itemList, "Value", "Text"));
        }

        public JsonResult AddAttribute(AttributeSet model)
        {
            AttributesDbUtil dbUtil = new AttributesDbUtil();
            AttributeSet NewAttributeset = db.AttributeSets.Where(x => x.AttributeSetId == model.AttributeSetId).FirstOrDefault();
            NewAttributeset.AttributeListForDetails = NewAttributeset.ApplicableValues.Split(',').ToList();
            if (!NewAttributeset.AttributeListForDetails.Any(x => x == model.ApplicableValues))
            {
                NewAttributeset.ApplicableValues = NewAttributeset.ApplicableValues + "," + model.ApplicableValues;
                dbUtil.SaveAttribute(NewAttributeset);
                var data = new
                {
                    AttributeValue = model.ApplicableValues,
                    AttributeId = model.AttributeSetId,
                };
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(Constants.ATTRIBUTE_ALREADY_PRESENT, JsonRequestBehavior.AllowGet);
            }            
        }

        public ActionResult Attribute(int? id)
        {
            AttributeSet attribute = null;
            if (id.HasValue)
            {
                attribute = util.GetAttribute(id.Value);
                string[] lines = attribute.ApplicableValues.Split(new string[] { "," }, StringSplitOptions.None);
                attribute.ApplicableValues = string.Join(Environment.NewLine, lines);
            }
            else
            {
                attribute = new AttributeSet();
            }
            return View(attribute);
        }

        public JsonResult GetSKUList(string term)
        {
            var result = new List<KeyValuePair<string, string>>();
            List<SKUMaster> List = db.GetSKUMasterList();
            foreach (var item in List)
            {
                result.Add(new KeyValuePair<string, string>(item.SKUMasterId.ToString(), item.SKU));
            }
            var result3 = result.Where(s => s.Value.ToLower().Contains(term.ToLower())).Select(w => w).Take(10).ToList();
            return Json(result3, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetLocationList(int BranchId)
        {
            List<SelectListItem> itemList = DropdownManager.GetLocationByBranch(BranchId);
            return Json(new SelectList(itemList, "Value", "Text"));
        }
        [HttpPost]
        public JsonResult GetBranchList(int DivisionId)
        {
            List<SelectListItem> itemList = DropdownManager.GetBranchByDivision(DivisionId);
            return Json(new SelectList(itemList, "Value", "Text"));
        }
    }
}