﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using DataLayer;
using DataLayer.Models;

namespace ERP_System.Controllers
{
    public class SMSQueueController : Controller
    {
        DbConnector db = new DbConnector();
        SMSEMailDbUtil smsdb = new SMSEMailDbUtil();
        // GET: Console/SMSQueueAP
        public ActionResult Index()
        {
            return View(db.smsQueues.ToList());
        }

        // GET: SMSQueues/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SmsQueue sMSQueue = db.smsQueues.Find(id);
            if (sMSQueue == null)
            {
                return HttpNotFound();
            }
            return View(sMSQueue);
        }

        // GET: SMSQueues/Create
        public ActionResult Create(string ErrorMessage)
        {
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMsg = ErrorMessage;
            }
            return View();
        }

        // POST: SMSQueues/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(SmsQueue smsQueue)
        {
            if (ModelState.IsValid)
            {
                List<Contact> Customer = smsdb.GetContactForEmailId();
                List<string> StringForEmailId = new List<string>();
                if (Customer.Count <= 0)
                {
                    //ViewBag.Error = MessageStore.EmailSendAllError;
                    return RedirectToAction("Create", new { ErrorMessage = ViewBag.Error });
                }
                foreach (var item in Customer)
                {
                    StringForEmailId.Add(item.Email);
                }
                string BCCValue = string.Join(" , ", StringForEmailId);
                SmsQueue NewSmsQueue = new SmsQueue();
                NewSmsQueue.Bcc = BCCValue;
                NewSmsQueue.Body = smsQueue.Body;
                NewSmsQueue.IsBodyHtml = true;
                NewSmsQueue.Subject = smsQueue.Subject;
                NewSmsQueue.SmsType = DataLayer.Enums.SmsType.Email;

                if (smsQueue.SendDate != null)
                {
                    NewSmsQueue.SendDate = smsQueue.SendDate;
                }
                else
                {
                    NewSmsQueue.SendDate = DateTime.Now;
                }

                if (smsdb.SaveSmsQueue(NewSmsQueue) < 0)
                {
                    //ViewBag.Error = MessageStore.EmailSendAllError;
                    return RedirectToAction("Create", new { ErrorMessage = ViewBag.Error });
                    // break;
                }


                return RedirectToAction("Create");
            }

            return View(smsQueue);
        }

        // GET: SMSQueues/Edit/5
        public ActionResult Edit(string ErrorMessage)
        {
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMsg = ErrorMessage;
            }
            SmsQueue smsQueue = new SmsQueue();
            smsQueue.Subject = "Send SMS";
            return View(smsQueue);
        }

        // POST: SMSQueues/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(SmsQueue smsQueue)
        {

            var Error = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                List<Contact> Customer = smsdb.GetContactForSms();
                List<string> StringForSms = new List<string>();
                if (Customer.Count <= 0)
                {
                    //ViewBag.Error = MessageStore.SmsSendAllError;
                    return RedirectToAction("Edit", new { ErrorMessage = ViewBag.Error });
                }
                foreach (var item in Customer)
                {
                    StringForSms.Add(item.Mobile);
                }
                string MobileNo = string.Join(",", StringForSms);
                SmsQueue NewSmsQueue = new SmsQueue();
                NewSmsQueue.PhoneNo = MobileNo;
                NewSmsQueue.Body = smsQueue.Body;
                NewSmsQueue.SmsType = DataLayer.Enums.SmsType.SMS;
                if (smsQueue.SendDate != null)
                {
                    NewSmsQueue.SendDate = smsQueue.SendDate;
                }
                else
                {
                    NewSmsQueue.SendDate = DateTime.Now;
                }
                //smsQueue.Destination = "9371579204";
                if (smsdb.SaveSmsQueue(NewSmsQueue) < 0)
                {
                    //ViewBag.Error = MessageStore.SmsSendAllError;
                    return RedirectToAction("Edit", new { ErrorMessage = ViewBag.Error });
                    // return View(item);
                    // break;
                }
                return RedirectToAction("Edit");

            }
            return View(smsQueue);
        }

        // GET: SMSQueues/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SmsQueue sMSQueue = db.smsQueues.Find(id);
            if (sMSQueue == null)
            {
                return HttpNotFound();
            }
            return View(sMSQueue);
        }

        // POST: SMSQueues/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SmsQueue sMSQueue = db.smsQueues.Find(id);
            db.smsQueues.Remove(sMSQueue);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}