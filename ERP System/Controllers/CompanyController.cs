﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer.Models;
using DataLayer;
using DataLayer.ViewModels;
using DataLayer.Utils;
using ERP_System.Utils;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class CompanyController : Controller
    {
        DbConnector db = new DbConnector();
        // GET: Company
        [RBAC(AccessType = 2)]
        public ActionResult Index(string ErrorMsg)
        {
            CompanyViewModel model = new CompanyViewModel();
            if (model.CompanyList == null)
            {
                model.CompanyList = db.GetSessionCompanyList();
            }
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            return View(model);
        }

        [RBAC(AccessType = 0)]
        public ActionResult Create(int? id)
        {
            Company company = null;
            if (id.HasValue && id.Value > 0)
            {
                company = db.GetCompanyById(id.Value);
            }
            else
            {
                company = new Company();
            }
            company.BankLedgerList = DropdownManager.GetBankAccountsList(true,0);
            return View(company);
        }

        [RBAC(AccessType = 0)]
        [HttpPost]
        public ActionResult Create(Company company)
        {
            db.SaveCompany(company);
            return RedirectToAction("Index");
        }

        [RBAC(AccessType = 3)]
        public ActionResult Delete(int id)
        {
            Company company = null;
            company = db.GetCompanyById(id);
            if (company == null)
            {
                return HttpNotFound();
            }
            else
            {
                if (db.RemoveCompany(company))
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index", new { ErrorMsg = MessageStore.DeleteError });
        }
    }
}