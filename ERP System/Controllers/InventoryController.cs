﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DataLayer;
using DataLayer.Models;
using DataLayer.Utils;
using DataLayer.ViewModels;
using ERP_System.Utils;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class InventoryController : Controller
    {
        DbConnector db = new DbConnector();
        InventoryHandler handler = new InventoryHandler();
        AttributeHandler attributeHandler = new AttributeHandler();

        // GET: PurchaseInvoice
        [RBAC(AccessType = 2)]
        public ActionResult Index(int? PageNo, string ErrorMsg)
        {
            PurchaseInvoiceViewModel model = new PurchaseInvoiceViewModel
            {
                PurchaseInvoiceLists = db.GetPurchaseInvoice(PageNo, Constants.PURCHASE_TYPE_OPENING_STOCK)
            };
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            return View(model);
        }

        [RBAC(AccessType = 0)]
        public ActionResult Create(int? id, string ErrorMsg)
        {
            PurchaseInvoice purchaseinvoice = handler.GetPurchaseInvoiceForCreateEdit(id);
            return View(purchaseinvoice);
        }

        [RBAC(AccessType = 1)]
        public ActionResult Edit(int id)
        {
            return RedirectToAction("Create", new { id = id });
        }

        [RBAC(AccessType = 3)]
        public ActionResult Delete(int id)
        {
            PurchaseInvoice purchaseInvoice = db.PurchaseInvoices.Include("PurchaseInvoiceItemList").Where(x => x.PurchaseInvoiceId == id).FirstOrDefault();
            if (purchaseInvoice == null)
            {
                return HttpNotFound();
            }
            else
            {
                if (handler.DeletePurchaseInvoice(purchaseInvoice))
                {
                    handler.CalculatePurchaseInvoice(purchaseInvoice.PurchaseInvoiceId);
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index", new { ErrorMsg = MessageStore.CantDeleteProductSold });
        }

        [HttpPost]
        public ActionResult Create(PurchaseInvoice purchaseInvoice, string btn)
        {
            int id = handler.SavePurchaseInvoice(purchaseInvoice, btn);
            if (btn == "Save and Print Barcode")
            {
                return RedirectToAction("Barcode", new { id = id });
            }
            else if (btn == "Save Opening Stock")
            {
                return RedirectToAction("Index");
            }
            return RedirectToAction("Create", new { id = id });
        }

        public ActionResult DeleteInvoiceItem(int invoiceId, int invoiceItemId)
        {
            PurchaseInvoiceDbUtil util = new PurchaseInvoiceDbUtil();
            PurchaseInvoiceItem item = db.GetPurchaseInvoiceItemById(invoiceItemId);
            if (util.RemoveInvoiceItem(item))
            {
                handler.CalculatePurchaseInvoice(item.PurchaseInvoiceId);
                return RedirectToAction("Create", new { id = invoiceId });
            }
            return RedirectToAction("Create", new { id = invoiceId, ErrorMsg = MessageStore.CantDeleteProductSold });
        }

        public ActionResult Barcode(int id)
        {
            ViewBag.InvoiceId = id;
            List<BarcodeViewModel> productList = db.GetBarcodes(id);
            return View(productList);
        }

        public ActionResult Details(int id)
        {
            PurchaseInvoiceItem item = new PurchaseInvoiceItem();
            item.PurchaseInvoiceItemId = id;
            item.PurchaseInvoiceId = db.PurchaseInvoiceItems.Where(x => x.PurchaseInvoiceItemId == id).Select(x => x.PurchaseInvoiceId).FirstOrDefault();
            item.ProductList = db.Products.Where(x => x.PurchaseInvoiceItemId == id).ToList();
            return View(item);
        }

        public ActionResult DeleteProduct(PurchaseInvoiceItem Model, string btn)
        {
            if (Model.ProductList != null)
            {
                List<int> ProductIds = Model.ProductList.Where(x => x.checkProduct == true).Select(x => x.ProductId).ToList();
                List<Product> ProductList = db.Products.Where(x => ProductIds.Contains(x.ProductId)).ToList();
                if (btn == "Delete")
                {
                    foreach (var item in ProductList.ToList())
                    {
                        db.RemoveSingleProduct(item);
                    }
                    if (db.Products.Where(x => x.PurchaseInvoiceItemId == Model.PurchaseInvoiceItemId).Count() == 0)
                    {
                        db.Configuration.ProxyCreationEnabled = false;
                        PurchaseInvoiceItem PurchaseInvoiceItem = db.PurchaseInvoiceItems.Where(x => x.PurchaseInvoiceItemId == Model.PurchaseInvoiceItemId).FirstOrDefault();
                        if (PurchaseInvoiceItem != null)
                        {
                            PurchaseInvoiceItem.IsProductCreated = false;
                            db.SavePurchaseInvoiceItem(PurchaseInvoiceItem);
                        }
                    }
                    handler.CalculatePurchaseInvoice(Model.PurchaseInvoiceId);
                }
                else if (btn == "Add To Barcode Queue")
                {
                    foreach (var item in ProductList)
                    {
                        List<Product> ProductForBarcode = SessionManager.GetBarcodeQueueList();
                        bool isAdded = ProductForBarcode.Any(x => x.ProductId == item.ProductId);
                        if (!isAdded)
                        {
                            SessionManager.AddItemInBarcodeQueue(item);
                        }
                    }
                }
                else if (btn == "Print Barcode")
                {
                    List<Product> ProductForBarcode = new List<Product>();
                    foreach (var item in ProductList)
                    {
                        ProductForBarcode.Add(item);
                    }
                    TempData["List"] = ProductForBarcode;
                    return RedirectToAction("PrintBarcode", "Product");
                }
            }
            return RedirectToAction("Create", new { id = Model.PurchaseInvoiceId });
        }

        public ActionResult ClonePurchaseInvoiceItem(int invoiceId, int? invoiceItemId)
        {
            string IsClone = Constants.ISCLONE;
            return RedirectToAction("AddInvoiceItem", new { invoiceId = invoiceId, invoiceItemId = invoiceItemId, IsClone = IsClone });
        }

        public ActionResult EditProduct(int? id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            Product Product = db.GetProductById(id.Value);
            Product.CategoryList = DropdownManager.GetCategoryList((id ?? 0));
            Product.SubCategoryList = DropdownManager.GetSubCategoryList((id ?? 0));
            Product.UnitList = DropdownManager.GetFreezedUnitList((Product.UnitId));
            Product.HSNList = DropdownManager.GetHSNList((id ?? 0));
            Product.LocationList = DropdownManager.GetLocationByBranch(SessionManager.GetSessionBranch().BranchId);
            Product.BrandList = DropdownManager.GetBrandList((id ?? 0));
            Product.GSTList = DropdownManager.GetGSTList((id ?? 0));
            Product.SKUList = DropdownManager.GetSKUList((id ?? 0));
            Product.DiscountCodeList = DropdownManager.GetDiscountsAccessibleList(Product.DiscountsAccessible,null);
            return View(Product);
        }

        [HttpPost]
        public ActionResult EditProduct(Product product)
        {
            if (product.DiscountIds != null)
            {
                List<int> templist = product.DiscountIds.OfType<int>().ToList();
                product.DiscountsAccessible = string.Join(",", templist);
            }
            else
            {
                product.DiscountsAccessible = "";
            }
            db.SaveProduct(product);
            return RedirectToAction("Details", new { id = product.PurchaseInvoiceItemId });
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpGet]
        public ActionResult AddInvoiceItem(int invoiceId, int? invoiceItemId, string IsClone)
        {
            PurchaseInvoiceItem PurchaseInvoiceItem = handler.GetInvoiceItemForCreateEdit(invoiceId, invoiceItemId, IsClone);
            return View(PurchaseInvoiceItem);
        }

        [HttpPost]
        public ActionResult AddInvoiceItem(PurchaseInvoiceItem viewModel, string btn)
        {
            viewModel = handler.SavePurchaseInvoiceItem(viewModel, btn);
            switch (btn)
            {
                case "Save":
                    {
                        return RedirectToAction("Create", new { id = viewModel.PurchaseInvoiceId });
                    }
                case "Save and New":
                    {
                        ViewBag.ItemSaved = "Item Saved";
                        return RedirectToAction("AddInvoiceItem", new { invoiceId = viewModel.PurchaseInvoiceId });
                    }
            }
            return null;
        }
    }
}