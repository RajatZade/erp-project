﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer.ViewModels;
using DataLayer.Models;
using DataLayer;
using DataLayer.Utils;
using ERP_System.Utils;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class DivisionController : Controller
    {
        DbConnector db = new DbConnector();
        // GET: Division
        [RBAC(AccessType = 2)]
        public ActionResult Index(int? id,string ErrorMsg)
        {
            DivisionViewModel model = new DivisionViewModel();
            if (model.DivisionList == null)
            {
                model.DivisionList = db.GetDivisionListByCompany(db.CompanyId);
            }
            if (id.HasValue)
            {
                model.Division = db.GetDivisionListById(id.Value);

            }
            else
            {
                model.Division = new Division();
            }
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            model.Division.CompanyId = db.CompanyId ?? 0;
            return View(model);
        }

        [RBAC(AccessType = 0)]
        [HttpPost]
        public ActionResult Create(Division division)
        {
            db.SaveDivision(division);
            return RedirectToAction("Index");
        }

        [RBAC(AccessType = 3)]
        public ActionResult Delete(int id)
        {
            Division division = db.Divisions.Find(id);
            if (division == null)
            {
                return HttpNotFound();
            }
            else
            {
                if (db.RemoveDivision(division))
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index", new { ErrorMsg = MessageStore.DeleteError });
        }
    }
}