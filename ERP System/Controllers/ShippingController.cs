﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DataLayer.Models;
using DataLayer.ViewModels;
using DataLayer.Utils;
using ERP_System.Utils;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class ShippingController : Controller
    {
        DbConnector db = new DbConnector();
        // GET: Shipping
        [RBAC(AccessType = 2)]
        public ActionResult Index(int? id,string ErrorMsg)
        {
            ShippingViewModel model = new ShippingViewModel();
            if (model.ShippingLists == null)
            {
                model.ShippingLists = db.GetShippingList();
            }
            if (id.HasValue)
            {
                model.Shipping = db.GetShippingById(id.Value);
            }
            else
            {
                model.Shipping = new Shipping();
            }
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            return View(model);
        }

        [RBAC(AccessType = 0)]
        [HttpPost]
        public ActionResult Create(Shipping shipping)
        {
            db.SaveShippingMaster(shipping);
            return RedirectToAction("Index");
        }

        [RBAC(AccessType = 3)]
        public ActionResult Delete(int id)
        {
            Shipping shipping = db.Shippings.Find(id);

            if (shipping == null)
            {
                return HttpNotFound();
            }

            else
            {
                if (db.RemoveShipping(shipping))
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index", new { ErrorMsg = MessageStore.DeleteError });
        }

    }
}