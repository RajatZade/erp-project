﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DataLayer.Models;
using DataLayer.Utils;
using DataLayer.ViewModels;
using ERP_System.Utils;
using System.IO;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class SaleReportController : Controller
    {
        DbConnector db = new DbConnector();

        [RBAC(AccessType = 2)]
        public ActionResult Index()
        {
            PurchaseReportViewModel model = new PurchaseReportViewModel();
            Ledger SaleLedger = db.Ledgers.Where(x => x.Name == Constants.LEDGER_TYPE_SALE_RETURN_LEDGER).FirstOrDefault();
            if (SaleLedger != null)
            {
                List<Voucher> VoucherList = db.Vouchers.Where(x => x.LedgerId == SaleLedger.LedgerId && x.VoucherFor != Constants.OPENING_BALANCE && x.CompanyId == db.CompanyId).OrderBy(x => x.EntryDate).ToList();
                model = GetReport(VoucherList);
            }
            return View(model);
        }


        [HttpPost]
        public ActionResult _InvoiceList(PurchaseReportViewModel ViewModel)
        {

            Ledger PurchaseLedger = db.Ledgers.Where(x => x.Name == Constants.LEDGER_TYPE_SALE_RETURN_LEDGER).FirstOrDefault();
            if (PurchaseLedger != null)
            {
                List<Voucher> VoucherList = db.Vouchers.Where(x => x.LedgerId == PurchaseLedger.LedgerId && x.VoucherFor != Constants.OPENING_BALANCE && x.CompanyId == db.CompanyId).ToList();

                if (ViewModel.SaleReportType == Enums.SaleReportType.All)
                {
                    VoucherList = db.Vouchers.Where(x => x.LedgerId == PurchaseLedger.LedgerId && x.VoucherFor != Constants.OPENING_BALANCE && x.CompanyId == db.CompanyId).ToList();
                }
                else if (ViewModel.SaleReportType == Enums.SaleReportType.SaleInvoice)
                {
                    VoucherList = db.Vouchers.Where(x => x.LedgerId == PurchaseLedger.LedgerId && (x.VoucherFor == Constants.SALE_TYPE_SALE_INVOICE || x.VoucherFor == Constants.SALE_TYPE_SALE_VOUCHER || x.VoucherFor == Constants.SALE_TYPE_WPS) && x.VoucherFor != Constants.OPENING_BALANCE && x.CompanyId == db.CompanyId).ToList();
                }
                else if (ViewModel.SaleReportType == Enums.SaleReportType.SaleReturn)
                {
                    VoucherList = db.Vouchers.Where(x => x.LedgerId == PurchaseLedger.LedgerId && (x.VoucherFor == Constants.SALE_TYPE_SALE_EXCHANGE || x.VoucherFor == Constants.JOURNAL_ENTRY) && x.VoucherFor != Constants.OPENING_BALANCE && x.CompanyId == db.CompanyId).ToList();
                }

                if (ViewModel.FromDate.HasValue)
                {
                    VoucherList = VoucherList.Where(x => x.EntryDate >= ViewModel.FromDate).OrderBy(x => x.EntryDate).ToList();
                }

                if (ViewModel.ToDate.HasValue)
                {
                    VoucherList = VoucherList.Where(x => x.EntryDate <= ViewModel.ToDate).OrderBy(x => x.EntryDate).ToList();
                }
                ViewModel = GetReport(VoucherList);
            }
            return PartialView(ViewModel);
        }


        public PurchaseReportViewModel GetReport(List<Voucher> VoucherList)
        {
            PurchaseReportViewModel model = new PurchaseReportViewModel();
            model.Company = SessionManager.GetSessionCompany();
            model.Branch = SessionManager.GetSessionBranch();
            List<int?> SaleInvoiceIds = VoucherList.Select(x => x.SaleInvoiceId).ToList();
            List<SaleInvoice> SaleInvoiceList = db.SaleInvoices.Where(x => SaleInvoiceIds.Contains(x.SaleInvoiceId)).ToList();
            foreach (var item in VoucherList)
            {
                PurchaseReportList ReportItem = new PurchaseReportList();
                if (item.VoucherFor == Constants.SALE_TYPE_SALE_INVOICE || item.VoucherFor == Constants.SALE_TYPE_SALE_EXCHANGE || item.VoucherFor == Constants.SALE_TYPE_SALE_VOUCHER || item.VoucherFor == Constants.SALE_TYPE_WPS)
                {
                    SaleInvoice SaleInvoice = SaleInvoiceList.Where(x => x.SaleInvoiceId == item.SaleInvoiceId).FirstOrDefault();
                    if (SaleInvoice != null)
                    {
                        if (item.VoucherFor == Constants.SALE_TYPE_SALE_INVOICE || item.VoucherFor == Constants.SALE_TYPE_SALE_EXCHANGE || item.VoucherFor == Constants.SALE_TYPE_WPS)
                        {
                            ReportItem.NoOfItem = db.SaleInvoiceItems.Where(x => x.SaleInvoiceId == item.SaleInvoiceId && x.Status == Constants.SALE_ITEM_SOLD).Count();
                        }
                        else
                        {
                            ReportItem.NoOfItem = db.ProductVouchers.Where(x => x.SaleInvoiceId == item.SaleInvoiceId).Count();
                        }
                        ReportItem.InvoiceId = item.SaleInvoiceId ?? 0;
                        ReportItem.Type = item.VoucherFor;
                        ReportItem.EntryDate = item.EntryDate;
                        ReportItem.InvoiceNumber = SaleInvoice.SaleInvoiceNo;
                        ReportItem.GrossAmount = SaleInvoice.Subtotal;
                        ReportItem.Adjustment = SaleInvoice.Adjustment ?? 0;
                        ReportItem.TotalTax = SaleInvoice.TotalTax;
                        ReportItem.TotalWithAdjustment = SaleInvoice.TotalWithAdjustment;
                        model.PurchaseReportList.Add(ReportItem);
                    }
                }
                else
                {
                    ReportItem.InvoiceNumber = item.VoucherNumber;
                    ReportItem.Type = item.VoucherFor;
                    ReportItem.EntryDate = item.EntryDate;
                    ReportItem.GrossAmount = -item.Total;
                    ReportItem.Adjustment = 0;
                    ReportItem.TotalTax = 0;
                    ReportItem.TotalWithAdjustment = -item.Total;
                    model.PurchaseReportList.Add(ReportItem);
                }
            }
            model.Subtotal = model.PurchaseReportList.Sum(x => x.GrossAmount);
            model.TotalTax = model.PurchaseReportList.Sum(x => x.TotalTax);
            model.TotalDiscount = model.PurchaseReportList.Sum(x => x.Adjustment);
            model.GrandTotal = model.PurchaseReportList.Sum(x => x.TotalWithAdjustment);
            return model;
        }


        [HttpPost]
        public FileContentResult FileContentResult(PurchaseReportViewModel ViewModel)
        {
            string header = "Sale Report";
            if (ViewModel.FromDate.HasValue)
            {
                header = header + " From " + ViewModel.FromDate.Value.ToString("dd'/'MM'/'yyyy");
            }
            if (ViewModel.ToDate.HasValue)
            {
                header = header + " To " + ViewModel.ToDate.Value.ToString("dd'/'MM'/'yyyy");
            }
            Ledger PurchaseLedger = db.Ledgers.Where(x => x.Name == Constants.LEDGER_TYPE_PURCHASE_RETURN_LEDGER).FirstOrDefault();
            if (PurchaseLedger != null)
            {
                List<Voucher> VoucherList = null;

                if (ViewModel.SaleReportType == Enums.SaleReportType.All)
                {
                    VoucherList = db.Vouchers.Where(x => x.LedgerId == PurchaseLedger.LedgerId && x.VoucherFor != Constants.OPENING_BALANCE && x.CompanyId == db.CompanyId).ToList();
                }
                else if (ViewModel.SaleReportType == Enums.SaleReportType.SaleInvoice)
                {
                    VoucherList = db.Vouchers.Where(x => x.LedgerId == PurchaseLedger.LedgerId && (x.VoucherFor == Constants.SALE_TYPE_SALE_INVOICE || x.VoucherFor == Constants.SALE_TYPE_SALE_VOUCHER || x.VoucherFor == Constants.SALE_TYPE_WPS) && x.VoucherFor != Constants.OPENING_BALANCE && x.CompanyId == db.CompanyId).ToList();
                }
                else if (ViewModel.SaleReportType == Enums.SaleReportType.SaleReturn)
                {
                    VoucherList = db.Vouchers.Where(x => x.LedgerId == PurchaseLedger.LedgerId && (x.VoucherFor == Constants.SALE_TYPE_SALE_EXCHANGE || x.VoucherFor == Constants.JOURNAL_ENTRY) && x.VoucherFor != Constants.OPENING_BALANCE && x.CompanyId == db.CompanyId).ToList();
                }

                if (ViewModel.FromDate.HasValue)
                {
                    VoucherList = VoucherList.Where(x => x.EntryDate >= ViewModel.FromDate).OrderBy(x => x.EntryDate).ToList();
                }

                if (ViewModel.ToDate.HasValue)
                {
                    VoucherList = VoucherList.Where(x => x.EntryDate <= ViewModel.ToDate).OrderBy(x => x.EntryDate).ToList();
                }
                ViewModel = GetReport(VoucherList);
            }

            //var LedgerList = db.GetLedgerList(model);
            StringWriter sw = new StringWriter();

            sw.WriteLine("\"\",\"\",\"\",\"\",\"" + header + "\",\"\",\"\",\"\"");
            sw.WriteLine("\"SR NO.\",\"Invoice Number\",\"Invoicing Date\",\"No. Of Item\",\"Gross Amount\",\"Total tax\",\"Adjustment\",\"Total\",\"Type\"");
            int i = 1;
            foreach (var item in ViewModel.PurchaseReportList)
            {
                sw.WriteLine(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\"", i, item.InvoiceNumber, item.EntryDate.Value.ToString("dd'/'MM'/'yyyy"), item.NoOfItem, item.GrossAmount.ToString("0.00"), item.TotalTax.ToString("0.00"), item.Adjustment.ToString("0.00"), item.TotalWithAdjustment.ToString("0.00"), item.Type, ""));
                i++;
            }
            sw.WriteLine(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\"", "", "", "", "Total", ViewModel.Subtotal.ToString("0.00"), ViewModel.TotalTax.ToString("0.00"), ViewModel.TotalDiscount.ToString("0.00"), ViewModel.GrandTotal.ToString("0.00"), "", ""));
            var fileName = "Purchase Report.csv";
            return File(new System.Text.UTF8Encoding().GetBytes(sw.ToString()), "text/csv", fileName);
        }
    }
}