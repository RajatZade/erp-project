﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DataLayer.Utils;
using DataLayer.ViewModels;
using ERP_System.Utils;
using DataLayer.Models;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class LedgerReportController : Controller
    {
        DbConnector db = new DbConnector();
        // GET: LedgerReport
        [RBAC(AccessType = 2)]
        public ActionResult Index()
        {
            ReportViewModel model = new ReportViewModel();
            model.AccountCategoryList = DropdownManager.GetContactCategoryList(null);
            return View(model);
        }

        [HttpPost]
        public ActionResult _InvoiceList(ReportViewModel model)
        {            
            model.LedgerList = db.Ledgers.Where(x => x.AccountCategoryId == model.AccountCategoryId).ToList();
            if(model.AccountSubCategoryId > 0)
            {
                model.LedgerList = model.LedgerList.Where(x => x.AccountSubCategoryId == model.AccountSubCategoryId).ToList();
            }
            foreach (var item in model.LedgerList)
            {
                model.LedgerId = item.LedgerId;
                Ledger ledger = PurchaseCalculator.GetLedgerOeningBalance(model);
                item.StartingBalance = ledger.StartingBalance;
                item.CurrentBalance = ledger.CurrentBalance;
                item.OpeningBalanceType = ledger.OpeningBalanceType;
                model.GrandTotal = model.GrandTotal + item.CurrentBalance;
            }
            return PartialView(model);
        }

        public ActionResult Details(int LedgerId)
        {
            LedgerReportViewModel Model = new LedgerReportViewModel();
            Model.Ledger = db.Ledgers.Where(x => x.LedgerId == LedgerId).FirstOrDefault();
            if (Model.Ledger != null)
            {
                Model.Ledger.AccountCategory = db.ContactCategorys.Where(x => x.AccountCategoryId == Model.Ledger.AccountCategoryId).FirstOrDefault();
                if (Model.Ledger.AccountSubCategoryId > 0)
                {
                    Model.Ledger.AccountSubCategory = db.ContactSubCategorys.Where(x => x.AccountSubCategoryId == Model.Ledger.AccountSubCategoryId).FirstOrDefault();
                }
            }
            Model.VoucherList = db.Vouchers.Where(x => x.LedgerId == LedgerId && x.CompanyId == db.CompanyId).OrderBy(x => x.EntryDate).ToList();
            if (Model.VoucherList != null)
            {
                Model.StartingDate = Model.VoucherList.Select(x => x.EntryDate).FirstOrDefault();
                Model.LastDate = Model.VoucherList.Select(x => x.EntryDate).LastOrDefault();
                foreach(var item in Model.VoucherList)
                {
                    if (item.Type == Constants.DR)
                    {
                        item.OpeningBalance = Model.LedgerOpeningBalanceForVoucher;
                        item.ClosingBalance = item.OpeningBalance - item.Total;
                        Model.LedgerOpeningBalanceForVoucher = item.ClosingBalance;
                        Model.TotalDebitAmmount = Model.TotalDebitAmmount + item.Total;
                    }
                    else
                    {
                        item.OpeningBalance = Model.LedgerOpeningBalanceForVoucher;
                        item.ClosingBalance = item.OpeningBalance + item.Total;
                        Model.LedgerOpeningBalanceForVoucher = item.ClosingBalance;
                        Model.TotalCreditAmmount = Model.TotalCreditAmmount + item.Total;
                    }                                      
                }
            }
            Model.Ledger.OpeningBalanceType = Model.VoucherList.Where(x => x.VoucherFor == Constants.OPENING_BALANCE).Select(x => x.Type).FirstOrDefault();
            Model.Ledger.StartingBalance = Model.VoucherList.Where(x => x.VoucherFor == Constants.OPENING_BALANCE).Select(x => x.ClosingBalance).FirstOrDefault();
            Model.Ledger.CurrentBalance = Model.VoucherList.Select(x => x.ClosingBalance).LastOrDefault();
            return View(Model);
        }

        [RBAC(AccessType = 2)]
        public ActionResult LedgerIndex(string ErrorMsg, string Type,int? LedgerId)
        {
            LedgerReportViewModel Model = new LedgerReportViewModel();
            if(LedgerId.HasValue)
            {
                Model.LedgerId = LedgerId.Value;
                Model.LedgerName = db.Ledgers.Where(x => x.LedgerId == LedgerId).Select(x => x.Name).FirstOrDefault();
            }
            Model.Company = SessionManager.GetSessionCompany();
            Model.Branch = SessionManager.GetSessionBranch();
            return View(Model);
        }

        [HttpPost]
        public ActionResult LedgerDetails(LedgerReportViewModel Model)
        {
            Model.Ledger = db.Ledgers.Where(x => x.LedgerId == Model.Ledger.LedgerId).FirstOrDefault();
            if (Model.Ledger != null)
            {
                Model.Ledger.AccountCategory = db.ContactCategorys.Where(x => x.AccountCategoryId == Model.Ledger.AccountCategoryId).FirstOrDefault();
                if (Model.Ledger.AccountSubCategoryId > 0)
                {
                    Model.Ledger.AccountSubCategory = db.ContactSubCategorys.Where(x => x.AccountSubCategoryId == Model.Ledger.AccountSubCategoryId).FirstOrDefault();
                }
            }
            Model.VoucherList = db.Vouchers.Where(x => x.LedgerId == Model.Ledger.LedgerId && x.CompanyId == db.CompanyId).OrderBy(x => x.EntryDate).ToList();
            foreach (var item in Model.VoucherList)
            {
                if (item.Type == Constants.DR)
                {
                    item.OpeningBalance = Model.LedgerOpeningBalanceForVoucher;
                    item.ClosingBalance = item.OpeningBalance - item.Total;
                    Model.LedgerOpeningBalanceForVoucher = item.ClosingBalance;
                    Model.TotalDebitAmmount = Model.TotalDebitAmmount + item.Total;
                }
                else
                {
                    item.OpeningBalance = Model.LedgerOpeningBalanceForVoucher;
                    item.ClosingBalance = item.OpeningBalance + item.Total;
                    Model.LedgerOpeningBalanceForVoucher = item.ClosingBalance;
                    Model.TotalCreditAmmount = Model.TotalCreditAmmount + item.Total;
                }
            }
            Model.VoucherList = Model.VoucherList.Where(x => x.EntryDate < Model.LastDate && x.EntryDate > Model.StartingDate).ToList();
            Model.Ledger.StartingBalance = Model.VoucherList.Where(x => x.VoucherFor == Constants.OPENING_BALANCE).Select(x => x.ClosingBalance).FirstOrDefault();
            Model.Ledger.CurrentBalance = Model.VoucherList.Select(x => x.ClosingBalance).LastOrDefault();
            Model.Ledger.OpeningBalanceType = Model.VoucherList.Where(x => x.VoucherFor == Constants.OPENING_BALANCE).Select(x => x.Type).FirstOrDefault();
            return View(Model);
        }

        public ActionResult LedgerDetails(int id)
        {
            LedgerReportViewModel Model = new LedgerReportViewModel();
            Model.Ledger = db.Ledgers.Where(x => x.LedgerId == id).FirstOrDefault();
            if (Model.Ledger != null)
            {
                Model.Ledger.AccountCategory = db.ContactCategorys.Where(x => x.AccountCategoryId == Model.Ledger.AccountCategoryId).FirstOrDefault();
                if (Model.Ledger.AccountSubCategoryId > 0)
                {
                    Model.Ledger.AccountSubCategory = db.ContactSubCategorys.Where(x => x.AccountSubCategoryId == Model.Ledger.AccountSubCategoryId).FirstOrDefault();
                }
            }
            Model.VoucherList = db.Vouchers.Where(x => x.LedgerId == id && x.CompanyId == db.CompanyId).OrderBy(x => x.EntryDate).ToList();
            if (Model.VoucherList != null)
            {
                Model.StartingDate = Model.VoucherList.Select(x => x.EntryDate).FirstOrDefault();
                Model.LastDate = Model.VoucherList.Select(x => x.EntryDate).LastOrDefault();
                foreach (var item in Model.VoucherList)
                {
                    if (item.Type == Constants.DR)
                    {
                        item.OpeningBalance = Model.LedgerOpeningBalanceForVoucher;
                        item.ClosingBalance = item.OpeningBalance - item.Total;
                        Model.LedgerOpeningBalanceForVoucher = item.ClosingBalance;
                        Model.TotalDebitAmmount = Model.TotalDebitAmmount + item.Total;
                    }
                    else
                    {
                        item.OpeningBalance = Model.LedgerOpeningBalanceForVoucher;
                        item.ClosingBalance = item.OpeningBalance + item.Total;
                        Model.LedgerOpeningBalanceForVoucher = item.ClosingBalance;
                        Model.TotalCreditAmmount = Model.TotalCreditAmmount + item.Total;
                    }
                }
                Model.Ledger.OpeningBalanceType = Model.VoucherList.Where(x => x.VoucherFor == Constants.OPENING_BALANCE).Select(x => x.Type).FirstOrDefault();
                Model.Ledger.StartingBalance = Model.VoucherList.Where(x => x.VoucherFor == Constants.OPENING_BALANCE).Select(x => x.ClosingBalance).FirstOrDefault();
                Model.Ledger.CurrentBalance = Model.VoucherList.Select(x => x.ClosingBalance).LastOrDefault();
            }            
            return View(Model);
        }

        [HttpPost]
        public ActionResult _LedgerList(ReportViewModel ViewModel)
        {
            LedgerReportViewModel Model = new LedgerReportViewModel();
            Model.StartingDate = ViewModel.FromDate;
            Model.LastDate = ViewModel.ToDate;
            Model.Company = SessionManager.GetSessionCompany();
            Model.Branch = SessionManager.GetSessionBranch();
            Model.Ledger = db.Ledgers.Where(x => x.LedgerId == ViewModel.LedgerId).FirstOrDefault();
            Model.VoucherList = db.Vouchers.Where(x => x.LedgerId == ViewModel.LedgerId && x.CompanyId == db.CompanyId).OrderBy(x => x.EntryDate).ThenBy(x => x.VoucherId).ToList();

            foreach (var item in Model.VoucherList)
            {
                if (item.Type == Constants.DR)
                {
                    item.OpeningBalance = Model.LedgerOpeningBalanceForVoucher;
                    item.ClosingBalance = item.OpeningBalance - item.Total;
                    Model.LedgerOpeningBalanceForVoucher = item.ClosingBalance;
                    Model.TotalDebitAmmount = Model.TotalDebitAmmount + item.Total;
                }
                else
                {
                    item.OpeningBalance = Model.LedgerOpeningBalanceForVoucher;
                    item.ClosingBalance = item.OpeningBalance + item.Total;
                    Model.LedgerOpeningBalanceForVoucher = item.ClosingBalance;
                    Model.TotalCreditAmmount = Model.TotalCreditAmmount + item.Total;
                }
            }
            if (ViewModel.FromDate.HasValue)
            {
                Model.VoucherList = db.Vouchers.Where(x => x.EntryDate >= ViewModel.FromDate && x.LedgerId == ViewModel.LedgerId && x.CompanyId == db.CompanyId).OrderBy(x => x.EntryDate).ThenBy(x => x.VoucherId).ToList();
            }

            if (ViewModel.ToDate.HasValue)
            {
                Model.VoucherList = Model.VoucherList.Where(x => x.EntryDate <= ViewModel.ToDate).OrderBy(x => x.EntryDate).ThenBy(x => x.VoucherId).ToList();
            }
            if(Model.VoucherList.Any(x => x.VoucherFor == Constants.OPENING_BALANCE))
            {
                Model.Ledger.StartingBalance = Model.VoucherList.Where(x => x.VoucherFor == Constants.OPENING_BALANCE).Select(x => x.ClosingBalance).FirstOrDefault();
                Model.Ledger.OpeningBalanceType = Model.VoucherList.Select(x => x.Type).FirstOrDefault();
            }
            else
            {
                Model.Ledger.StartingBalance = Model.VoucherList.Select(x => x.OpeningBalance).FirstOrDefault();
                Model.Ledger.OpeningBalanceType = Model.VoucherList.Select(x => x.Type).FirstOrDefault();
            }
            Model.Ledger.CurrentBalance = Model.VoucherList.Select(x => x.ClosingBalance).LastOrDefault();
            return PartialView(Model);
        }

        [HttpPost]
        public JsonResult LedgerList(string Prefix)
        {
            var result = new List<KeyValuePair<string, string>>();
            IList<SelectListItem> List = DropdownManager.GetLedgerSelectList();
            foreach (var item in List)
            {
                result.Add(new KeyValuePair<string, string>(item.Value.ToString(), item.Text));
            }
            var result3 = result.Where(s => s.Value.ToLower().Contains(Prefix.ToLower())).Select(w => w).ToList();
            return Json(result3, JsonRequestBehavior.AllowGet);
        }
    }
}