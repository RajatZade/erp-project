﻿using DataLayer;
using DataLayer.Utils;
using DataLayer.Models;
using DataLayer.ViewModels;
using ERP_System.Utils;
using System;
using System.Linq;
using System.Web.Mvc;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class ExcessesController : Controller
    {
        // GET: Excesses
        DbConnector db = new DbConnector();
        PurchaseReturnHandler handler = new PurchaseReturnHandler();
        ExcessesHandler ExcessesHandler = new ExcessesHandler();

        [RBAC(AccessType = 2)]
        public ActionResult Index(int? PageNo, string ErrorMsg, string Type)
        {
            SaleInvoiceViewModel model = new SaleInvoiceViewModel
            {
                SaleInvoiceLists = db.GetSaleInvoice(PageNo, Type),
                Search = Type
            };
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            return View(model);
        }


        [RBAC(AccessType = 0)]
        public ActionResult Create(int? id, string Type)
        {
            SaleInvoice saleinvoice = handler.GetPurchaseReturn(id, Type);
            saleinvoice.TotalNotMapped = saleinvoice.TotalInvoiceValue;
            return View(saleinvoice);
        }

        [RBAC(AccessType = 1)]
        public ActionResult Edit(int id)
        {
            return RedirectToAction("Create", new { id = id });
        }

        [RBAC(AccessType = 3)]
        public ActionResult Delete(int id, string Type)
        {
            SalesInvoiceHandler salesInvoice = new SalesInvoiceHandler();
            string errorMessage = salesInvoice.DeleteInvoice(id);
            return RedirectToAction("Index", new { ErrorMsg = errorMessage, Type = Type });
        }

        [HttpPost]
        public ActionResult Create(SaleInvoice saleInvoice, string btn)
        {
            saleInvoice = ExcessesHandler.SaveSaleInvoice(saleInvoice, btn);
            switch (btn)
            {
                case "Hold":
                    {
                        return RedirectToAction("Index", new { Type = saleInvoice.Type });
                    }
                case "Save":
                    {
                        return RedirectToAction("Index", new { Type = saleInvoice.Type });
                    }
                case "Save & Print Invoice":
                    {
                        return RedirectToAction("GenerateInvoice", new { InvoiceId = saleInvoice.SaleInvoiceId });
                    }
                case "Email":
                    {
                        break;
                    }
                case "SMS":
                    {
                        break;
                    }
            }
            return RedirectToAction("Create", new { id = saleInvoice.SaleInvoiceId });
        }

        [HttpPost]
        public ActionResult _ItemList(string ProdId, int? SalesInvoiceId, string Type)
        {
            string message = string.Empty;
            SaleInvoice saleInvoice = new SaleInvoice();
            
            //Query from appropriate channel depending upon SalesInvoiceid
            if (SalesInvoiceId.HasValue && SalesInvoiceId.Value == 0)
            {
                saleInvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Type);
            }
            else
            {
                saleInvoice.SaleInvoiceItemList = handler.GetSaleInvoiceItems(SalesInvoiceId);
            }
            //Check if product is already available in the current invoice
            //Check if product is not sold
            Product product = db.GetProductByNumberForExcesses(ProdId);
            if (product == null)
            {
                ViewBag.ErrorMsg = "The product is unavailable or already sold";
            }
            else
            {
                bool isAdded = saleInvoice.SaleInvoiceItemList.Any(x => x.ProductId == product.ProductId);
                if (isAdded)
                {
                    ViewBag.ErrorMsg = "This product is already added in this invoice";
                }
                else
                {
                    //Add item to invoice
                    if (SalesInvoiceId.HasValue && SalesInvoiceId.Value == 0)
                    {
                        ProductManager.AddPurchaseReturnItem(product, SalesInvoiceId,Type, Constants.SALE_ITEM_EXCESSES);
                        saleInvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Type);
                    }

                    else
                    {
                        saleInvoice.SaleInvoiceItemList.Add(ProductManager.AddPurchaseReturnItem(product, SalesInvoiceId, Type, Constants.SALE_ITEM_EXCESSES));
                    }
                }
            }
            saleInvoice = SaleCalculator.Calculate(saleInvoice);
            return PartialView(saleInvoice);
        }


        [HttpPost]
        public ActionResult RemoveItem(int ProductId, int? SaleInvoiceId, string Type)
        {
            SaleInvoice saleinvoice = new SaleInvoice();
            if (SaleInvoiceId == null || SaleInvoiceId == 0)
            {
                SessionManager.RemoveItemFromList(ProductId, Type);
                saleinvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Type);
            }
            else
            {
                handler.RemoveSalesInvoiceItem(SaleInvoiceId, null, ProductId);
                saleinvoice.SaleInvoiceItemList = handler.GetSaleInvoiceItems(SaleInvoiceId);
            }

            if (saleinvoice.SaleInvoiceItemList != null)
            {
                saleinvoice = SaleCalculator.Calculate(saleinvoice);
            }
            return PartialView("_ItemList", saleinvoice);
        }

        [HttpPost]
        public ActionResult UpdateProduct(SaleInvoiceItem model, string Type)
        {
            SaleInvoice saleinvoice = new SaleInvoice();
            if (model.SaleInvoiceItemId > 0)
            {
                int SaleInvoiceId = handler.UpDateSaleInvoiceItem(model);
                saleinvoice.SaleInvoiceItemList = handler.GetSaleInvoiceItems(model.SaleInvoiceId);
            }
            else
            {
                SessionManager.UpdatePucrahseReturnItem(model, Type);
                saleinvoice.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Type);
            }

            if (saleinvoice.SaleInvoiceItemList != null)
            {
                saleinvoice = SaleCalculator.Calculate(saleinvoice);
            }
            return PartialView("_ItemList", saleinvoice);
        }

        public ActionResult GenerateInvoice(int InvoiceId)
        {
            GenerateSalesInovice model = new GenerateSalesInovice
            {
                SalesInvoice = db.GetSalesInvoiceById(InvoiceId)
            };
            model.SalesInvoice.SaleInvoiceItemList = handler.GetSaleInvoiceItems(InvoiceId);
            foreach (var Item in model.SalesInvoice.SaleInvoiceItemList)
            {
                Item.GST = db.GetGSTMasterById(Item.GSTId);
                if (Item.SGST != null)
                {
                    model.SalesInvoice.TotalSGST += Item.SGST.Value;
                }
                if (Item.CGST != null)
                {
                    model.SalesInvoice.TotalCGST += Item.CGST.Value;
                }
                if (Item.IGST != null)
                {
                    model.SalesInvoice.TotalIGST += Item.IGST.Value;
                }
                if (Item.DiscountId == -1)
                {
                    Item.SaleDiscountPercent = 0;
                }
                else
                {
                    Item.SaleDiscountPercent = db.Discounts.Where(x => x.DiscountId == Item.DiscountId).Select(x => x.DiscountPercent).FirstOrDefault();
                }
            }
            model.Ledger = db.Ledgers.Where(x => x.LedgerId == model.SalesInvoice.CustomerId).FirstOrDefault();
            model.Contact = db.Contacts.Where(x => x.ContactId == model.Ledger.ContactId).FirstOrDefault();
            if (model.Contact == null)
            {
                model.Contact = new Contact();
            }
            model.SaleOrderNumber = db.SalesOrders.Where(x => x.SalesOrderId == model.SalesInvoice.SaleOrderId).Select(x => x.SaleOrderNumber).FirstOrDefault();
            //model.Shipping = db.GetShippingById(model.SalesInvoice.ShippingId);
            model.Company = SessionManager.GetSessionCompany();
            model.Branch = SessionManager.GetSessionBranch();
            model.PaymentType = db.GetPaymentTypeById(model.SalesInvoice.PaymentTypeId);
            model.SalesInvoice.TotalInWord = CommonUtils.NumberToWords(Convert.ToInt32(model.SalesInvoice.TotalWithAdjustment));
            return View(model);
        }
    }
}