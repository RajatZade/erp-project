﻿using DataLayer;
using DataLayer.Models;
using DataLayer.Utils;
using DataLayer.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System;
using ERP_System.Utils;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class PurchaseInvoiceController : Controller
    {
        DbConnector db = new DbConnector();
        PurchaseInvoiceHandler handler = new PurchaseInvoiceHandler();
        AttributeHandler attributeHandler = new AttributeHandler();

        // GET: PurchaseInvoice
        [RBAC(AccessType = 2)]
        public ActionResult Index(int? PageNo, string ErrorMsg)
        {
            PurchaseInvoiceViewModel model = new PurchaseInvoiceViewModel
            {
                PurchaseInvoiceLists = db.GetPurchaseInvoice(PageNo, Constants.PURCHASE_TYPE_PURCHASE_INVOICE)
            };
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            return View(model);
        }

        [RBAC(AccessType = 0)]
        public ActionResult Create(int? id, string ErrorMsg)
        {
            PurchaseInvoice purchaseinvoice = handler.GetPurchaseInvoiceForCreateEdit(id);
            return View(purchaseinvoice);
        }

        [RBAC(AccessType = 1)]
        public ActionResult Edit(int id, string ErrorMsg)
        {
            return RedirectToAction("Create", new { id = id });
        }

        [HttpPost]
        public ActionResult Create(PurchaseInvoice purchaseInvoice, string btn)
        {
            int id = handler.SavePurchaseInvoice(purchaseInvoice, btn);
            if (btn == "Save and Print Barcode")
            {
                return RedirectToAction("Barcode", new { id = id });
            }
            else if (btn == "Save Invoice")
            {
                return RedirectToAction("Index");
            }
            return RedirectToAction("Create", new { id = id });
        }


        [RBAC(AccessType = 3)]
        public ActionResult Delete(int id)
        {
            PurchaseInvoice purchaseInvoice = db.PurchaseInvoices.Include("PurchaseInvoiceItemList").Where(x => x.PurchaseInvoiceId == id).FirstOrDefault();
            if (purchaseInvoice == null)
            {
                return HttpNotFound();
            }
            else
            {
                if (handler.DeletePurchaseInvoice(purchaseInvoice))
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index", new { ErrorMsg = MessageStore.CantDeleteProductSold });
        }

        [HttpGet]
        public ActionResult AddInvoiceItem(int invoiceId, int? invoiceItemId, string IsClone)
        {
            PurchaseInvoiceItem PurchaseInvoiceItem = handler.GetInvoiceItemForCreateEdit(invoiceId, invoiceItemId, IsClone);
            return View(PurchaseInvoiceItem);
        }


        [HttpPost]
        public ActionResult AddInvoiceItem(PurchaseInvoiceItem viewModel, string btn)
        {
            viewModel = handler.SavePurchaseInvoiceItem(viewModel, btn);
            switch (btn)
            {
                case "Save":
                    {
                        return RedirectToAction("Create", new { id = viewModel.PurchaseInvoiceId });
                    }
                case "Save and New":
                    {
                        ViewBag.ItemSaved = "Item Saved";
                        return RedirectToAction("AddInvoiceItem", new { invoiceId = viewModel.PurchaseInvoiceId });
                    }
            }
            return null;
        }


        public ActionResult DeleteInvoiceItem(int invoiceId, int invoiceItemId)
        {
            PurchaseInvoiceDbUtil util = new PurchaseInvoiceDbUtil();
            PurchaseInvoiceItem item = db.GetPurchaseInvoiceItemById(invoiceItemId);
            if (util.RemoveInvoiceItem(item))
            {
                handler.CalculatePurchaseInvoice(item.PurchaseInvoiceId);
                return RedirectToAction("Create", new { id = invoiceId });
            }
            return RedirectToAction("Create", new { id = invoiceId, ErrorMsg = MessageStore.CantDeleteProductSold });
        }

        public ActionResult Barcode(int id)
        {
            ViewBag.InvoiceId = id;
            List<BarcodeViewModel> productList = db.GetBarcodes(id);
            return View(productList);
        }

        [HttpPost]
        public JsonResult GetPurchaseOrder(int id)
        {
            PurchaseOrder model = db.PurchaseOrders.Where(x => x.PurchaseOrderId == id).FirstOrDefault();
            var data = new
            {
                BranchId = model.BranchId,
                LocationId = model.LocationId,
                DivisionId = model.DivisionId,
                PurchaseAccountId = model.PurchaseAccountId,
                AccountID = model.AccountID,
                PurchaseTypeID = model.PurchaseTypeID,
            };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GenerateValueList(int id)
        {
            AttributesDbUtil util = new AttributesDbUtil();
            AttributeSet model = util.GetAttribute(id);
            List<string> tempList = model.ApplicableValues.Split(',').ToList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (tempList != null && tempList.Count > 0)
            {
                foreach (var item in tempList)
                {
                    itemList.Add(new SelectListItem() { Value = item.ToString(), Text = item, });
                }
            }
            return Json(new SelectList(itemList, "Value", "Text"));
        }

        [HttpPost]
        public ActionResult _AddAttributes(AttributeLinking model, decimal? Quantity)
        {
            AttributesDbUtil util = new AttributesDbUtil();
            if (model.PurchaseInvoiceItemId > 0)
            {
                db.SaveSingleAttributeLinking(model);
            }
            else
            {
                SessionManager.SetAttribute(model);
            }
            decimal NewQuantity = 0;
            PurchaseInvoiceItem PurchaseInvoiceItem = new PurchaseInvoiceItem();
            PurchaseInvoiceItem.AttributeLinkingList = SessionManager.GetAttributeList(model.PurchaseInvoiceItemId);
            if (PurchaseInvoiceItem.AttributeLinkingList != null)
            {
                foreach (var item1 in PurchaseInvoiceItem.AttributeLinkingList)
                {
                    item1.ValuesList = item1.Value.Split(',').ToList();
                    item1.NameList = item1.AttributeSetIds.Split(',').ToList();
                    NewQuantity = NewQuantity + item1.Quantity;
                }
            }

            if (Quantity < NewQuantity)
            {
                int? PurchaseInvoiceItemId = model.PurchaseInvoiceItemId;
                if (model.PurchaseInvoiceItemId > 0)
                {
                    db.RemoveAttribute(model.AttributeLinkingId);
                }
                else
                {
                    SessionManager.RemoveAttribute(model.AttributeLinkingId);
                }
                PurchaseInvoiceItem.AttributeLinkingList = SessionManager.GetAttributeList(PurchaseInvoiceItemId);
                if (PurchaseInvoiceItem.AttributeLinkingList != null)
                {
                    foreach (var item1 in PurchaseInvoiceItem.AttributeLinkingList)
                    {
                        item1.ValuesList = item1.Value.Split(',').ToList();
                        item1.NameList = item1.AttributeSetIds.Split(',').ToList();
                    }
                }
                ViewBag.ErrorMsg = "Quantity Should Be Equal";
            }
            return PartialView(PurchaseInvoiceItem);
        }

        [HttpPost]
        public ActionResult RemoveLinking(int LinkingId, int InvoiceId)
        {
            PurchaseInvoiceItem PurchaseInvoiceItem = new PurchaseInvoiceItem
            {
                AttributeLinkingList = SessionManager.GetAttributeList()
            };
            if (PurchaseInvoiceItem.AttributeLinkingList != null)
            {
                SessionManager.RemoveAttribute(LinkingId);
            }
            else
            {
                db.RemoveAttribute(LinkingId);
                PurchaseInvoiceItem.AttributeLinkingList = db.GetInvoiceItemAttribute(InvoiceId);
                if (PurchaseInvoiceItem.AttributeLinkingList != null)
                {
                    foreach (var i in PurchaseInvoiceItem.AttributeLinkingList)
                    {
                        i.NameList = i.AttributeSetIds.Split(',').ToList();
                        i.ValuesList = i.Value.Split(',').ToList();
                    }
                }
            }
            return PartialView("_AddAttributes", PurchaseInvoiceItem);
        }

        [HttpPost]
        public JsonResult EditLinking(int LinkingId, int? InvoiceId)
        {
            AttributeLinking model = null;
            model = SessionManager.GetAttributeList(LinkingId);
            return Json(model);
        }

        [HttpPost]
        public JsonResult GetGST(int id)
        {
            GST model = db.GSTs.Where(x => x.GSTId == id).FirstOrDefault();
            var data = new { CGST = model.CGST, SGST = model.SGST, IGST = model.IGST, Other = model.OtherTaxes };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetDiscount(int id)
        {
            Discount model = db.GetDiscountById(id);
            var data = new { Percent = model.DiscountPercent, MaxDiscount = model.MaxDiscountAllowed };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DueDays(int DueDays, DateTime EntryDate)
        {
            var DueDate = EntryDate.AddDays(DueDays);
            return Json(DueDate, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SubCategoryList(int categoryid)
        {
            List<SubCategory> sublist = db.GetSubCategoryListByCategoryId(categoryid);
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (sublist != null && sublist.Count > 0)
            {
                foreach (var item in sublist)
                {
                    itemList.Add(new SelectListItem() { Text = item.Name, Value = item.SubCategoryId.ToString() });
                }
            }
            return Json(new SelectList(itemList, "Value", "Text"));
        }

        [HttpPost]
        public JsonResult UnitFractionValue(int UnitId)
        {
            Unit unit = db.GetUnitMasterById(UnitId);
            if (unit.SellInPartial == true)
            {
                return Json(true);
            }
            return Json(false);
        }

        [HttpPost]
        public JsonResult SKUList(SKUMaster skumaster)
        {
            List<SKUMaster> sublist = db.GetSKUListByCatIdSubId(skumaster);
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (sublist != null && sublist.Count > 0)
            {
                foreach (var item in sublist)
                {
                    itemList.Add(new SelectListItem() { Text = item.SKU, Value = item.SKUMasterId.ToString() });
                }
            }
            return Json(new SelectList(itemList, "Value", "Text"));
        }

        [HttpPost]
        public JsonResult GetInvoiceNumber(string Number)
        {
            var Item = db.PurchaseInvoices.Where(x => x.PurchaseInvoiceNo == Number).FirstOrDefault();
            if (Item == null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Details(int id)
        {
            PurchaseInvoiceItem item = new PurchaseInvoiceItem
            {
                PurchaseInvoiceItemId = id,
                PurchaseInvoiceId = db.PurchaseInvoiceItems.Where(x => x.PurchaseInvoiceItemId == id).Select(x => x.PurchaseInvoiceId).FirstOrDefault(),
                ProductList = db.Products.Where(x => x.PurchaseInvoiceItemId == id).ToList()
            };
            return View(item);
        }

        public ActionResult DeleteProduct(PurchaseInvoiceItem Model, string btn)
        {
            if (Model.ProductList != null)
            {
                List<int> ProductIds = Model.ProductList.Where(x => x.checkProduct == true).Select(x => x.ProductId).ToList();
                List<Product> ProductList = db.Products.Where(x => ProductIds.Contains(x.ProductId)).ToList();
                if (btn == "Delete")
                {
                    foreach (var item in ProductList.ToList())
                    {
                        db.RemoveSingleProduct(item);
                    }
                    if (db.Products.Where(x => x.PurchaseInvoiceItemId == Model.PurchaseInvoiceItemId).Count() == 0)
                    {
                        PurchaseInvoiceDbUtil util = new PurchaseInvoiceDbUtil();
                        PurchaseInvoiceItem item = db.GetPurchaseInvoiceItemById(Model.PurchaseInvoiceItemId);
                        if (util.RemoveInvoiceItem(item))
                        {
                            //handler.CalculatePurchaseInvoice(item.PurchaseInvoiceId);
                        }
                        //db.Configuration.ProxyCreationEnabled = false;
                        //PurchaseInvoiceItem PurchaseInvoiceItem = db.PurchaseInvoiceItems.Where(x => x.PurchaseInvoiceItemId == Model.PurchaseInvoiceItemId).FirstOrDefault();
                        //if (PurchaseInvoiceItem != null)
                        //{
                        //    PurchaseInvoiceItem.IsProductCreated = false;
                        //    db.SavePurchaseInvoiceItem(PurchaseInvoiceItem);
                        //}
                    }
                    handler.CalculatePurchaseInvoice(Model.PurchaseInvoiceId);
                }
                else if (btn == "Add To Barcode Queue")
                {
                    foreach (var item in ProductList)
                    {
                        List<Product> ProductForBarcode = SessionManager.GetBarcodeQueueList();
                        bool isAdded = ProductForBarcode.Any(x => x.ProductId == item.ProductId);
                        if (!isAdded)
                        {
                            SessionManager.AddItemInBarcodeQueue(item);
                        }
                    }
                }
                else if (btn == "Print Barcode")
                {
                    List<Product> ProductForBarcode = new List<Product>();
                    foreach (var item in ProductList)
                    {
                        ProductForBarcode.Add(item);
                    }
                    TempData["List"] = ProductForBarcode;
                    return RedirectToAction("PrintBarcode", "Product");
                }
            }
            return RedirectToAction("Create", new { id = Model.PurchaseInvoiceId });
        }

        public ActionResult ClonePurchaseInvoiceItem(int invoiceId, int? invoiceItemId)
        {
            string IsClone = Constants.ISCLONE;
            return RedirectToAction("AddInvoiceItem", new { invoiceId = invoiceId, invoiceItemId = invoiceItemId, IsClone = IsClone });
        }

        public ActionResult EditProduct(int? id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            Product Product = db.GetProductById(id.Value);
            Product.CategoryList = DropdownManager.GetCategoryList((id ?? 0));
            Product.SubCategoryList = DropdownManager.GetSubCategoryList((id ?? 0));
            Product.UnitList = DropdownManager.GetFreezedUnitList((Product.UnitId));
            Product.HSNList = DropdownManager.GetHSNList((id ?? 0));
            Product.LocationList = DropdownManager.GetLocationByBranch(SessionManager.GetSessionBranch().BranchId);
            Product.BrandList = DropdownManager.GetBrandList((id ?? 0));
            Product.GSTList = DropdownManager.GetGSTList((id ?? 0));
            Product.SKUList = DropdownManager.GetSKUList((id ?? 0));
            Product.DiscountCodeList = DropdownManager.GetDiscountsAccessibleList(Product.DiscountsAccessible,null);
            return View(Product);
        }

        [HttpPost]
        public ActionResult EditProduct(Product product)
        {
            if (product.DiscountIds != null)
            {
                List<int> templist = product.DiscountIds.OfType<int>().ToList();
                product.DiscountsAccessible = string.Join(",", templist);
            }
            else
            {
                product.DiscountsAccessible = "";
            }
            db.SaveProduct(product);
            return RedirectToAction("Details", new { id = product.PurchaseInvoiceItemId });
        }

        [HttpPost]
        public JsonResult ListFromSKU(int id)
        {
            PurchaseInvoiceItemViewModel model = new PurchaseInvoiceItemViewModel();
            SKUMaster SkuMaster = db.GetSKUMasterById(id);
            return Json(SkuMaster);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public void Update(int id)
        {
            PurchaseInvoice Invoice = db.PurchaseInvoices.Where(x => x.PurchaseInvoiceId == id).FirstOrDefault();
            Invoice.PurchaseInvoiceItemList = db.PurchaseInvoiceItems.Where(x => x.PurchaseInvoiceId == id).ToList();
            Invoice.NotmappedInvoiceValue = Invoice.TotalInvoiceValue + (Invoice.Adjustment ?? 0);

            #region PurchaseLedger  
            Ledger PurchaseLedger = db.Ledgers.Where(x => x.Name == Constants.LEDGER_TYPE_PURCHASE_RETURN_LEDGER).FirstOrDefault();
            if (PurchaseLedger != null)
            {
                Voucher PurchaseLedgerVoucher = db.Vouchers.Where(x => x.LedgerId == PurchaseLedger.LedgerId && x.PurchaseInvoiceId == Invoice.PurchaseInvoiceId && x.Type == Constants.CR).FirstOrDefault();
                if (PurchaseLedgerVoucher != null)
                {
                    PurchaseLedgerVoucher.Total = Invoice.NotmappedInvoiceValue.Value;
                    db.SaveVauchers(PurchaseLedgerVoucher);
                }
                else
                {
                    PurchaseLedgerVoucher = new Voucher();
                    PurchaseLedgerVoucher.LedgerId = PurchaseLedger.LedgerId;
                    PurchaseLedgerVoucher.PurchaseInvoiceId = Invoice.PurchaseInvoiceId;
                    PurchaseLedgerVoucher.Total = Invoice.NotmappedInvoiceValue.Value;
                    PurchaseLedgerVoucher.Type = Constants.CR;
                    PurchaseLedgerVoucher.InvoiceNumber = Invoice.PurchaseInvoiceNo;
                    PurchaseLedgerVoucher.EntryDate = Invoice.InvoicingDate ?? DateTime.Now;
                    PurchaseLedgerVoucher.Particulars = "Purchase Invoice Number :" + Invoice.PurchaseInvoiceNo;
                    PurchaseLedgerVoucher.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_INVOICE;
                    db.SaveVauchers(PurchaseLedgerVoucher);
                }
            }
            #endregion
        }
    }
}

