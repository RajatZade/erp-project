﻿using System;
using System.Web.Mvc;
using DataLayer;
using DataLayer.Models;
using DataLayer.ViewModels;
using DataLayer.Utils;
using System.Collections.Generic;
using System.Linq;
using ERP_System.Utils;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class SKUController : Controller
    {
        DbConnector db = new DbConnector();

        [RBAC(AccessType = 2)]
        public ActionResult Index(int? id, string ErrorMsg)
        {
            SKUViewModel model = new SKUViewModel();

            if (model.SKUList == null)
            {
                model.SKUList = db.GetSKUMasterList();
            }
            if (id.HasValue)
            {
                model.SKUMaster = db.GetSKUMasterById(id.Value);
                model.SKUMaster.CategoryList = DropdownManager.GetCategoryList(model.SKUMaster.CategoryId);
                model.SKUMaster.SubCategoryList = DropdownManager.GetSubCategoryList(model.SKUMaster.SubCategoryId);
            }
            else
            {
                model.SKUMaster = new SKUMaster();
                model.SKUMaster.CategoryList = DropdownManager.GetCategoryList(null);
            }
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }

            return View(model);
        }


        [RBAC(AccessType = 0)]
        [HttpPost]
        public ActionResult Create(SKUMaster SKUMaster)
        {
            db.SaveSKUMaster(SKUMaster);
            return RedirectToAction("Index");
        }

        [RBAC(AccessType = 3)]
        public ActionResult Delete(int id)
        {
            SKUMaster SKUMaster = db.SKUMasters.Find(id);

            if (SKUMaster == null)
            {
                return HttpNotFound();
            }
            else
            {
                if (db.RemoveSKU(SKUMaster))
                {
                    return RedirectToAction("Index");
                }

            }

            return RedirectToAction("Index", new { ErrorMsg = MessageStore.DeleteError });
        }

        [HttpPost]
        public JsonResult SubCategoryList(int categoryid)
        {
            List<SubCategory> sublist = db.GetSubCategoryListByCategoryId(categoryid);
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (sublist != null && sublist.Count > 0)
            {
                foreach (var item in sublist)
                {
                    itemList.Add(new SelectListItem() { Text = item.Name, Value = item.SubCategoryId.ToString() });
                }
            }
            return Json(new SelectList(itemList, "Value", "Text"));
        }

        [HttpPost]
        public JsonResult GetSku(string SKU)
        {
            var Item = db.SKUMasters.Where(x => x.SKU == SKU && x.CompanyId == (db.CompanyId ?? 0)).FirstOrDefault();
            if (Item == null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}