﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer.ViewModels;
using DataLayer.Models;
using DataLayer;
using DataLayer.Utils;
using ERP_System.Utils;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class BrandController : Controller
    {
        DbConnector db = new DbConnector();
        // GET: Brand
        [RBAC(AccessType = 2)]
        public ActionResult Index(int? id,string ErrorMsg)
        {
            BrandViewModel model = new BrandViewModel();
            if (model.BrandList == null)
            {
                model.BrandList = db.GetBrandList();
            }
            if (id.HasValue)
            {
                model.Brand = db.GetBrandListById(id.Value);
            }
            else
            {
                model.Brand = new Brand();
            }
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            return View(model);
        }

        [RBAC(AccessType = 0)]
        [HttpPost]
        public ActionResult Create(Brand brand)
        {
            db.SaveBrand(brand);
            return RedirectToAction("Index");
        }

        [RBAC(AccessType = 3)]
        public ActionResult Delete(int id)
        {
            Brand brand = db.GetBrandListById(id);
           
            if (brand == null)
            {
                return HttpNotFound();
            }
            else
            {
                if (db.RemoveBrand(brand))
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index", new { ErrorMsg = MessageStore.DeleteError });
        }

        [HttpPost]
        public JsonResult ExistingBrand(string Name)
        {
            var Brand  = db.Brands.Where(x => x.BrandName == Name && x.CompanyId == (db.CompanyId ?? 0)).FirstOrDefault();
            if (Brand == null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}