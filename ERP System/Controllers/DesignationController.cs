﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DataLayer.Models;
using DataLayer.ViewModels;
using DataLayer.Utils;
using ERP_System.Utils;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class DesignationController : Controller
    {
        DbConnector db = new DbConnector();
        // GET: UnitMaster
        public ActionResult Index(int? id,string ErrorMsg)
        {
            DesignationViewModel model = new DesignationViewModel();
            if (model.DesignationList == null)
            {
                model.DesignationList = db.GetDesignationList();
            }
            if (id.HasValue)
            {
                model.Desigantion = db.GetDesignationById(id.Value);
            }
            else
            {
                model.Desigantion = new Designation();
            }
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(Designation Desigantion)
        {
            db.SaveDesignation(Desigantion);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            Designation designation = db.Designations.Find(id);
            if (designation == null)
            {
                return HttpNotFound();
            }
            else
            {
                if (db.RemoveDesignation(designation))
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index", new { ErrorMsg = MessageStore.DeleteError });
        }
    }
}