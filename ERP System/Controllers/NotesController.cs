﻿using DataLayer;
using DataLayer.Models;
using DataLayer.Utils;
using DataLayer.ViewModels;
using ERP_System.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class NotesController : Controller
    {
        DbConnector db = new DbConnector();

        [RBAC(AccessType = 2)]
        public ActionResult PaymentList(string VoucherFor)
        {
            LedgerReportViewModel model = new LedgerReportViewModel();
            List<Ledger> LedgerList = db.GetLedgerList();
            model.Type = VoucherFor;
            List<PaymentType> PaymentTypeList = db.PaymentTypes.ToList();
            if (VoucherFor == Constants.PAYMENT_VOUCHER || VoucherFor == Constants.RECEIPT_VOUCHER)
            {
                model.VoucherList = db.Vouchers.Where(x => x.VoucherFor == VoucherFor && x.CompanyId == db.CompanyId && x.Type == Constants.DR).OrderBy(x => x.VoucherId).ToList();
                model.VoucherList2 = db.Vouchers.Where(x => x.VoucherFor == VoucherFor && x.CompanyId == db.CompanyId && x.Type == Constants.CR).OrderBy(x => x.VoucherId).ToList();
            }
            else if (VoucherFor == Constants.JOURNAL_ENTRY || VoucherFor == Constants.CONTRA_ENTRY)
            {
                model.VoucherList = db.Vouchers.Where(x => x.VoucherFor == VoucherFor && x.CompanyId == db.CompanyId && x.Type == Constants.CR).OrderBy(x => x.VoucherId).ToList();
                model.VoucherList2 = db.Vouchers.Where(x => x.VoucherFor == VoucherFor && x.CompanyId == db.CompanyId && x.Type == Constants.DR).OrderBy(x => x.VoucherId).ToList();
            }

            for (int i = 0; i < model.VoucherList.Count; i++)
            {
                model.VoucherList[i].Ledger = LedgerList.Where(x => x.LedgerId == model.VoucherList[i].LedgerId).FirstOrDefault();
                model.VoucherList[i].Ledger2 = LedgerList.Where(x => x.LedgerId == model.VoucherList2[i].LedgerId).FirstOrDefault();
                model.VoucherList[i].ToPaymentType = PaymentTypeList.Where(x => x.PaymentTypeId == model.VoucherList[i].PaymentTypeId).FirstOrDefault();
                model.VoucherList[i].FromPaymentType = PaymentTypeList.Where(x => x.PaymentTypeId == model.VoucherList2[i].PaymentTypeId).FirstOrDefault();
            }
            model.VoucherList = model.VoucherList.OrderBy(x => x.VoucherNo).ToList();
            return View(model);
        }

        [RBAC(AccessType = 0)]
        public ActionResult CreateNote(string VoucherNumber,string Type)
        {
            PaymentNoteViewModel model = new PaymentNoteViewModel();
            model.Type = Type;
            if (!string.IsNullOrEmpty(VoucherNumber))
            {
                model.VoucherNumber = VoucherNumber;
                Voucher FromVoucher = db.Vouchers.Where(x => x.VoucherNumber == VoucherNumber && x.Type == Constants.CR && x.VoucherFor == Type && x.CompanyId == db.CompanyId).FirstOrDefault();
                if (FromVoucher != null)
                {
                    model.Description = FromVoucher.Description;
                    model.FromLedgerId = FromVoucher.LedgerId ?? 0;
                    model.Payment = FromVoucher.Total;
                    model.FromVoucherId = FromVoucher.VoucherId;
                    model.EntryDate = FromVoucher.EntryDate;
                    model.Checknumber = FromVoucher.Checknumber;
                    model.FromPaymentTypeId = FromVoucher.PaymentTypeId ?? 0;
                    model.MethodList = FromVoucher.MethodId ?? 0;
                }
                Voucher ToVoucher = db.Vouchers.Where(x => x.VoucherNumber == VoucherNumber && x.Type == Constants.DR && x.VoucherFor == Type && x.CompanyId == db.CompanyId).FirstOrDefault();
                if (ToVoucher != null)
                {
                    model.ToLedgerId = ToVoucher.LedgerId ?? 0;
                    model.ToVoucherId = ToVoucher.VoucherId;
                    model.ToPaymentTypeId = ToVoucher.PaymentTypeId ?? 0;
                }
            }
            else
            {
                model.VoucherNumber = GenerateAutoNo.GenerateVoucherNoForType(Type);
                model.EntryDate = DateTime.Now;
            }            
            model.LedgerList = DropdownManager.GetLedgerListForType(null, null);
            model.PaymentTyprList = DropdownManager.GetPaymentTypeWithLedger();
            return View(model);
        }

        [RBAC(AccessType = 1)]
        public ActionResult EditNote(string VoucherNumber, string Type)
        {
            return RedirectToAction("CreateNote", new { VoucherNumber = VoucherNumber, Type = Type });
        }

        [RBAC(AccessType = 3)]
        public ActionResult Delete(string VoucherFor,string VoucherNumber)
        {            
            db.DeleteVoucher(VoucherFor, VoucherNumber);
            return RedirectToAction("PaymentList", new { VoucherFor = VoucherFor });
        }

        [HttpPost]
        public JsonResult GetInvoice(int LedgerId)
        {
            List<PurchaseInvoice> PurchaseInvoiceList = db.PurchaseInvoices.Where(x => x.AccountId == LedgerId && x.CompanyId == db.CompanyId && (x.Type == Constants.PURCHASE_TYPE_PURCHASE_INVOICE || x.Type == Constants.PURCHASE_TYPE_PURCHASE_VOUCHER)).ToList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (PurchaseInvoiceList != null && PurchaseInvoiceList.Count > 0)
            {
                foreach (var item in PurchaseInvoiceList)
                {
                    itemList.Add(new SelectListItem() { Text = item.PurchaseInvoiceNo, Value = item.PurchaseInvoiceId.ToString() });
                }
            }
            return Json(new SelectList(itemList, "Value", "Text"));
        }

        [HttpPost]
        public JsonResult GetSaleInvoice(int LedgerId)
        {
            List<SaleInvoice> SaleInvoiceList = db.SaleInvoices.Where(x => x.CustomerId == LedgerId && x.CompanyId == db.CompanyId && (x.Type == Constants.SALE_TYPE_SALE_INVOICE || x.Type == Constants.SALE_TYPE_SALE_VOUCHER)).ToList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (SaleInvoiceList != null && SaleInvoiceList.Count > 0)
            {
                foreach (var item in SaleInvoiceList)
                {
                    itemList.Add(new SelectListItem() { Text = item.SaleInvoiceNo, Value = item.SaleInvoiceId.ToString() });
                }
            }
            return Json(new SelectList(itemList, "Value", "Text"));
        }

        [HttpPost]
        public ActionResult CreateNote(PaymentNoteViewModel model,string btn)
        {
            if (model.Type == Constants.PAYMENT_VOUCHER || model.Type == Constants.CONTRA_ENTRY)
            {
                #region FromPaymentType
                Ledger LedgerForPaymentType = db.Ledgers.Where(x => x.LedgerId == db.PaymentTypes.Where(y => y.PaymentTypeId == model.FromPaymentTypeId).Select(y => y.LedgerId).FirstOrDefault()).FirstOrDefault();
                if (LedgerForPaymentType != null)
                {
                    Voucher FromVoucher = new Voucher
                    {
                        MethodId = model.MethodList,
                        PaymentTypeId = model.FromPaymentTypeId,
                        VoucherId = model.FromVoucherId,
                        LedgerId = LedgerForPaymentType.LedgerId,
                        SaleInvoiceId = model.InvoiceId,
                        Total = model.Payment,
                        Type = Constants.CR,
                        EntryDate = model.EntryDate ?? DateTime.Now,
                        Particulars = model.Description,
                        Checknumber = model.Checknumber,
                        VoucherFor = model.Type,
                        VoucherNumber = model.VoucherNumber,
                        Description = model.Description,
                    };
                    FromVoucher.Particulars = model.Type + " : " + model.VoucherNumber;
                    db.SaveVauchers(FromVoucher);
                }
                #endregion
            }
            if (model.Type == Constants.PAYMENT_VOUCHER || model.Type == Constants.JOURNAL_ENTRY)
            {
                PurchaseInvoice purchaseInvoice = db.PurchaseInvoices.Where(x => x.PurchaseInvoiceId == model.InvoiceId).FirstOrDefault();                
                #region ToVoucher
                Voucher ToVoucher = new Voucher();
                ToVoucher.VoucherId = model.ToVoucherId;
                ToVoucher.LedgerId = model.ToLedgerId;
                ToVoucher.SaleInvoiceId = model.InvoiceId;
                ToVoucher.MethodId = model.MethodList;
                ToVoucher.Total = model.Payment;
                ToVoucher.Type = Constants.DR;
                ToVoucher.EntryDate = model.EntryDate ?? DateTime.Now;
                ToVoucher.Particulars = model.Description;
                ToVoucher.Checknumber = model.Checknumber;
                ToVoucher.VoucherFor = model.Type;
                ToVoucher.VoucherNumber = model.VoucherNumber;
                ToVoucher.Particulars = model.Type + " : " + model.VoucherNumber;
                ToVoucher.Description = model.Description;
                db.SaveVauchers(ToVoucher);
                #endregion
            }
            if (model.Type == Constants.RECEIPT_VOUCHER || model.Type == Constants.JOURNAL_ENTRY)
            {
                #region FromVoucher
                Voucher FromVoucher = new Voucher();
                FromVoucher.VoucherId = model.FromVoucherId;
                FromVoucher.LedgerId = model.FromLedgerId;
                FromVoucher.SaleInvoiceId = model.InvoiceId;
                FromVoucher.MethodId = model.MethodList;
                FromVoucher.Total = model.Payment;
                FromVoucher.Type = Constants.CR;
                FromVoucher.EntryDate = model.EntryDate ?? DateTime.Now;
                FromVoucher.Particulars = model.Description;
                FromVoucher.Checknumber = model.Checknumber;
                FromVoucher.VoucherFor = model.Type;
                FromVoucher.VoucherNumber = model.VoucherNumber;
                FromVoucher.Particulars = model.Type + " : " + model.VoucherNumber;
                FromVoucher.Description = model.Description;
                db.SaveVauchers(FromVoucher);
                #endregion
            }
            if (model.Type == Constants.RECEIPT_VOUCHER || model.Type == Constants.CONTRA_ENTRY)
            {
                #region ToPaymentType
                Ledger LedgerForPaymentType = db.Ledgers.Where(x => x.LedgerId == db.PaymentTypes.Where(y => y.PaymentTypeId == model.ToPaymentTypeId).Select(y => y.LedgerId).FirstOrDefault()).FirstOrDefault();
                if (LedgerForPaymentType != null)
                {
                    Voucher ToVoucher = new Voucher();
                    ToVoucher.VoucherId = model.ToVoucherId;
                    ToVoucher.MethodId = model.MethodList;
                    ToVoucher.LedgerId = LedgerForPaymentType.LedgerId;
                    ToVoucher.PaymentTypeId = model.ToPaymentTypeId;
                    ToVoucher.SaleInvoiceId = model.InvoiceId;
                    ToVoucher.Total = model.Payment;
                    ToVoucher.Type = Constants.DR;
                    ToVoucher.EntryDate = model.EntryDate ?? DateTime.Now;
                    ToVoucher.Particulars = model.Description;
                    ToVoucher.Checknumber = model.Checknumber;
                    ToVoucher.VoucherFor = model.Type;
                    ToVoucher.VoucherNumber = model.VoucherNumber;
                    ToVoucher.Particulars = model.Type + " : " + model.VoucherNumber;
                    ToVoucher.Description = model.Description;
                    db.SaveVauchers(ToVoucher);
                }
                #endregion
            }
            if(btn == "Save and Print")
            {
                if (model.Type == Constants.RECEIPT_VOUCHER)
                {
                    return RedirectToAction("PrintReceipt", new { VoucherNumber = model.VoucherNumber, Type = model.Type });
                }
                else if (model.Type == Constants.PAYMENT_VOUCHER)
                {
                    return RedirectToAction("PrintPayment", new { VoucherNumber = model.VoucherNumber, Type = model.Type });
                }
            }
            return RedirectToAction("CreateNote", new {  Type = model.Type });
        }


        public ActionResult Print(string VoucherFor, string VoucherNumber)
        {
            if(VoucherFor == Constants.PAYMENT_VOUCHER)
            {
                return RedirectToAction("PrintPayment", new { VoucherNumber = VoucherNumber, VoucherFor = VoucherFor });
            }
            if (VoucherFor == Constants.RECEIPT_VOUCHER)
            {
                return RedirectToAction("PrintReceipt", new { VoucherNumber = VoucherNumber, VoucherFor = VoucherFor });
            }
            Voucher voucher = new Voucher();
            Voucher voucher1 = new Voucher();
            if (VoucherFor == Constants.PAYMENT_VOUCHER || VoucherFor == Constants.RECEIPT_VOUCHER)
            {
                voucher = db.Vouchers.Where(x => x.VoucherFor == VoucherFor && x.CompanyId == db.CompanyId && x.Type == Constants.DR).FirstOrDefault();
                voucher1 = db.Vouchers.Where(x => x.VoucherFor == VoucherFor && x.CompanyId == db.CompanyId && x.Type == Constants.CR).FirstOrDefault();
            }
            else if (VoucherFor == Constants.JOURNAL_ENTRY || VoucherFor == Constants.CONTRA_ENTRY)
            {
                voucher = db.Vouchers.Where(x => x.VoucherFor == VoucherFor && x.CompanyId == db.CompanyId && x.Type == Constants.CR).FirstOrDefault();
                voucher1 = db.Vouchers.Where(x => x.VoucherFor == VoucherFor && x.CompanyId == db.CompanyId && x.Type == Constants.DR).FirstOrDefault();
            }
            voucher.Ledger = db.Ledgers.Where(x => x.LedgerId == voucher.LedgerId).FirstOrDefault();
            voucher.Ledger2 = db.Ledgers.Where(x => x.LedgerId == voucher1.LedgerId).FirstOrDefault();
            voucher.Company = SessionManager.GetSessionCompany();
            return View(voucher);
        }

        public ActionResult PrintReceipt(string VoucherFor, string VoucherNumber)
        {
            PrintVoucherViewModel Model = new PrintVoucherViewModel();
            Model.Company = SessionManager.GetSessionCompany();
            Model.Branch = SessionManager.GetSessionBranch();
            Voucher voucher = db.Vouchers.Where(x => x.VoucherNumber == VoucherNumber && x.VoucherFor == VoucherFor && x.CompanyId == db.CompanyId && x.Type == Constants.DR).FirstOrDefault();
            Voucher voucher1 = db.Vouchers.Where(x => x.VoucherNumber == VoucherNumber && x.VoucherFor == VoucherFor && x.CompanyId == db.CompanyId && x.Type == Constants.CR).FirstOrDefault();
            voucher.Ledger = db.Ledgers.Where(x => x.LedgerId == voucher.LedgerId).FirstOrDefault();
            voucher.Ledger2 = db.Ledgers.Where(x => x.LedgerId == voucher1.LedgerId).FirstOrDefault();
            if(voucher.MethodId == Enums.MethodOfAdjustment.Against && voucher.SaleInvoiceId.HasValue)
            {
                voucher.SaleInvoice = db.SaleInvoices.Where(x => x.SaleInvoiceId == voucher.SaleInvoiceId).FirstOrDefault();                
            }
            voucher.FromPaymentType = db.PaymentTypes.Where(x => x.PaymentTypeId == voucher.PaymentTypeId).FirstOrDefault();
            Model.Voucher = voucher;
            Model.TotalInWord = CommonUtils.NumberToWords(Convert.ToInt32(Model.Voucher.Total));
            return View(Model);
        }


        public ActionResult PrintPayment(string VoucherFor, string VoucherNumber)
        {
            PrintVoucherViewModel Model = new PrintVoucherViewModel();
            Model.Company = SessionManager.GetSessionCompany();
            Model.Branch = SessionManager.GetSessionBranch();
            Voucher voucher = db.Vouchers.Where(x => x.VoucherNumber == VoucherNumber && x.VoucherFor == VoucherFor && x.CompanyId == db.CompanyId && x.Type == Constants.DR).FirstOrDefault();
            Voucher voucher1 = db.Vouchers.Where(x => x.VoucherNumber == VoucherNumber && x.VoucherFor == VoucherFor && x.CompanyId == db.CompanyId && x.Type == Constants.CR).FirstOrDefault();
            voucher.Ledger = db.Ledgers.Where(x => x.LedgerId == voucher.LedgerId).FirstOrDefault();
            voucher.Ledger2 = db.Ledgers.Where(x => x.LedgerId == voucher1.LedgerId).FirstOrDefault();
            Model.Voucher = voucher;
            Model.TotalInWord = CommonUtils.NumberToWords(Convert.ToInt32(Model.Voucher.Total));
            return View(Model);
        }
    }
}