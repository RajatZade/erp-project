﻿using DataLayer;
using DataLayer.Models;
using DataLayer.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace ERP_System.Controllers
{
    public class BaseController : Controller
    {
        DbConnector db = new DbConnector();

        [HttpPost]
        public JsonResult GetGST(int id)
        {
            GST model = db.GetGSTMasterById(id);
            var data = new { CGST = model.CGST, SGST = model.SGST, IGST = model.IGST, Other = model.OtherTaxes };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetDiscount(int id)
        {
            Discount model = db.GetDiscountById(id);
            var data = new { Percent = model.DiscountPercent, MaxDiscount = model.MaxDiscountAllowed };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DueDays(int DueDays, DateTime EntryDate)
        {
            var DueDate = EntryDate.AddDays(DueDays);
            return Json(DueDate, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SubCategoryList(int categoryid)
        {

            List<SubCategory> sublist = db.GetSubCategoryListByCategoryId(categoryid);
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (sublist != null && sublist.Count > 0)
            {
                foreach (var item in sublist)
                {
                    itemList.Add(new SelectListItem() { Text = item.Name, Value = item.SubCategoryId.ToString() });
                }
            }
            return Json(new SelectList(itemList, "Value", "Text"));
        }

        [HttpPost]
        public JsonResult UnitFractionValue(int UnitId)
        {
            Unit unit = db.GetUnitMasterById(UnitId);
            if (unit.SellInPartial == true)
            {
                return Json(true);
            }
            return Json(false);
        }

        [HttpPost]
        public JsonResult SKUList(SKUMaster skumaster)
        {

            List<SKUMaster> sublist = db.GetSKUListByCatIdSubId(skumaster);
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (sublist != null && sublist.Count > 0)
            {
                foreach (var item in sublist)
                {
                    itemList.Add(new SelectListItem() { Text = item.SKU, Value = item.SKUMasterId.ToString() });
                }
            }
            return Json(new SelectList(itemList, "Value", "Text"));
        }

        [HttpPost]
        public JsonResult ListFromSKU(int id)
        {
            SKUMaster SkuMaster = db.GetSKUMasterById(id);
            return Json(SkuMaster);
        }

        //public JsonResult GetUserList(string term)
        //{
        //    var result = new List<KeyValuePair<string, string>>();
        //    IList<SelectListItem> List = DropdownManager.GetCustomerMasterList(0);
        //    foreach (var item in List)
        //    {
        //        result.Add(new KeyValuePair<string, string>(item.Value.ToString(), item.Text));
        //    }
        //    var result3 = result.Where(s => s.Value.ToLower().Contains(term.ToLower())).Select(w => w).ToList();
        //    return Json(result3, JsonRequestBehavior.AllowGet);
        //}
    }
}