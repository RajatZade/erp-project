﻿using DataLayer;
using DataLayer.Models;
using DataLayer.Utils;
using System;
using System.Web.Mvc;
using System.Linq;
using ERP_System.Utils;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class AttributeController : Controller
    {        
        AttributesDbUtil util = new AttributesDbUtil();
        // GET: Attribute
        [RBAC(AccessType = 2)]
        public ActionResult Index(string ErrorMsg)
        {
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            return View(util.GetAllAttributes());
        }

        [RBAC(AccessType = 1)]
        public ActionResult Create(int? id)
        {
            AttributeSet attribute = null;
            if(id.HasValue)
            {
                attribute = util.GetAttribute(id.Value);
                string[] lines = attribute.ApplicableValues.Split(new string[] { "," }, StringSplitOptions.None);
                attribute.ApplicableValues = string.Join(Environment.NewLine, lines);
            }
            else
            {
                attribute = new AttributeSet();
            }
            return View(attribute);
        }

        [RBAC(AccessType = 0)]
        [HttpPost]
        public ActionResult Create(AttributeSet attributeSet)
        {
            string[] lines = attributeSet.ApplicableValues.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            attributeSet.ApplicableValues = string.Join(",", lines);
            util.SaveAttribute(attributeSet);
            return RedirectToAction("Index");
        }

        [RBAC(AccessType = 3)]
        public ActionResult Delete(int id)
        {
            if(util.DeleteAttribute(id))
            {
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index", new { ErrorMsg = MessageStore.DeleteError });
            }
            
        }

        public ActionResult Details(int? id)
        {
            AttributeSet attribute = null;
            if (id.HasValue)
            {
                attribute = util.GetAttribute(id.Value);
                attribute.AttributeListForDetails = attribute.ApplicableValues.Split(',').ToList();
            }            
            return View(attribute);
        }
    }
}