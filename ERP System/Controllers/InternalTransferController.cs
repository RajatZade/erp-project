﻿using DataLayer;
using DataLayer.Models;
using DataLayer.Utils;
using DataLayer.ViewModels;
using ERP_System.Utils;
using System.Linq;
using System.Web.Mvc;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class InternalTransferController : Controller
    {
        InternalTransferHandler Handler = new InternalTransferHandler();
        InternalTransferDbUtil DbUtil = new InternalTransferDbUtil();
        DbConnector db = new DbConnector();

        // GET: InternalTransfer
        [RBAC(AccessType = 2)]
        public ActionResult Index(int? PageNo)
        {
            InternalTransferViewModel ViewModel = new InternalTransferViewModel();
            ViewModel.InternalTransferList = Handler.GetInternalTransferList(PageNo);
            return View(ViewModel);
        }

        [RBAC(AccessType = 0)]
        public ActionResult Create(int? id)
        {
            InternalTransfer Transfer = Handler.GetInternalTransfer(id);
            return View(Transfer);
        }

        [RBAC(AccessType = 1)]
        public ActionResult Edit(int id)
        {
            return RedirectToAction("Create", new { id = id });
        }

        [RBAC(AccessType = 3)]
        public ActionResult Delete(int id)
        {
            string errorMessage = Handler.DeleteTransfer(id);
            return RedirectToAction("Index", new { ErrorMsg = errorMessage });
        }

        [HttpPost]
        public ActionResult Create(InternalTransfer Transfer, string btn)
        {
            Transfer = Handler.SaveInternalTransfer(Transfer);
            switch (btn)
            {
                case "Save":
                    {
                        return RedirectToAction("Index");
                    }
                case "Save & Print Invoice":
                    {
                        return RedirectToAction("GenerateInvoice", new { InvoiceId = Transfer.InternalTransferId });
                    }
            }
            return RedirectToAction("Create", new { id = Transfer.InternalTransferId });
        }

        [HttpPost]
        public ActionResult _ItemList(string ProdId, int InternalTransferId)
        {
            string message = string.Empty;
            InternalTransfer Transfer = new InternalTransfer();
            //Query from appropriate channel depending upon SalesInvoiceid
            if (InternalTransferId > 0)
            {
                Transfer.InternalTransfers = DbUtil.GetTransferItems(InternalTransferId);                
            }
            else
            {
                Transfer.InternalTransfers = SessionManager.GetInvoiceItemList(Constants.INTERNAL_TRANSFER);
            }
            //Check if product is already available in the current invoice
            //Check if product is not sold
            Product product = db.GetProductByNumber(ProdId);
            if (product == null)
            {
                ViewBag.ErrorMsg = "The product is unavailable or already sold";
            }
            else
            {
                bool isAdded = Transfer.InternalTransfers.Any(x => x.ProductId == product.ProductId);
                if (isAdded)
                {
                    ViewBag.ErrorMsg = "This product is already added in this invoice";
                }
                else
                {
                    //Add item to invoice
                    if (InternalTransferId > 0)
                    {
                        Transfer.InternalTransfers.Add(ProductManager.AddInternalTransferItem(product, InternalTransferId, Constants.INTERNAL_TRANSFER, Constants.INTERNAL_TRANSFER));
                    }
                    else
                    {
                        ProductManager.AddInternalTransferItem(product, InternalTransferId, Constants.INTERNAL_TRANSFER, Constants.INTERNAL_TRANSFER);
                        Transfer.InternalTransfers = SessionManager.GetInvoiceItemList(Constants.INTERNAL_TRANSFER);
                    }
                }
            }
            Transfer = SaleCalculator.CalculateInternalTransfers(Transfer);
            return PartialView(Transfer);
        }

        [HttpPost]
        public ActionResult RemoveItem(int ProductId, int? InternalTransferId)
        {
            InternalTransfer Transfer = new InternalTransfer();
            if (InternalTransferId == null || InternalTransferId == 0)
            {
                SessionManager.RemoveItemFromList(ProductId, Constants.INTERNAL_TRANSFER);
                Transfer.InternalTransfers = SessionManager.GetInvoiceItemList(Constants.INTERNAL_TRANSFER);
            }
            else
            {
                DbUtil.RemoveItem(InternalTransferId,ProductId);
                Transfer.InternalTransfers = DbUtil.GetTransferItems(InternalTransferId ?? 0);
            }

            if (Transfer.InternalTransfers != null)
            {
                Transfer = SaleCalculator.CalculateInternalTransfers(Transfer);
            }
            return PartialView("_ItemList", Transfer);
        }

        [HttpPost]
        public ActionResult UpdateProduct(SaleInvoiceItem model)
        {
            SalesInvoiceHandler SaleHandler = new SalesInvoiceHandler();
            InternalTransfer Transfer = new InternalTransfer();
            if (model.InternalTransferId > 0)
            {
                int SaleInvoiceId = SaleHandler.UpDateSaleInvoiceItem(model);
                Transfer.InternalTransfers = DbUtil.GetTransferItems(model.InternalTransferId ?? 0);
            }
            else
            {
                SessionManager.UpdateOrderItem(model, Constants.INTERNAL_TRANSFER);
                Transfer.InternalTransfers = SessionManager.GetInvoiceItemList(Constants.INTERNAL_TRANSFER);
            }

            if (Transfer.InternalTransfers != null)
            {
                Transfer = SaleCalculator.CalculateInternalTransfers(Transfer);
            }
            return PartialView("_ItemList", Transfer);
        }
    }
}