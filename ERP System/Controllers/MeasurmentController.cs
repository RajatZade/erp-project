﻿using System;
using System.Linq;
using System.Web.Mvc;
using DataLayer;
using DataLayer.Models;
using DataLayer.Utils;
using ERP_System.Utils;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class MeasurmentController : Controller
    {
        DbConnector db = new DbConnector();

        public ActionResult Index(string ErrorMsg)
        {
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            return View(db.GetMeasurmentList());
        }

        [RBAC(AccessType = 1)]
        public ActionResult Create(int? id)
        {
            Measurment model = null;
            if (id.HasValue)
            {
                model = db.Measurments.Where(x => x.MeasurmentId == id.Value).FirstOrDefault();
                string[] lines = model.MeasurementValues.Split(new string[] { "," }, StringSplitOptions.None);
                model.MeasurementValues = string.Join(Environment.NewLine, lines);
            }
            else
            {
                model = new Measurment();
            }
            return View(model);
        }

        [RBAC(AccessType = 0)]
        [HttpPost]
        public ActionResult Create(Measurment model)
        {
            string[] lines = model.MeasurementValues.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            model.MeasurementValues = string.Join(",", lines);
            db.SaveMeasurment(model);
            return RedirectToAction("Index");
        }

        [RBAC(AccessType = 3)]
        public ActionResult Delete(int id)
        {
            if (db.DeleteMeasurment(id))
            {
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index", new { ErrorMsg = MessageStore.DeleteError });
            }

        }

        public ActionResult Details(int? id)
        {
            Measurment model = null;
            if (id.HasValue)
            {
                model = db.Measurments.Where(x => x.MeasurmentId == id.Value).FirstOrDefault();
                model.MeasurmentListForDetails = model.MeasurementValues.Split(',').ToList();
            }
            return View(model);
        }
    }
}