﻿using DataLayer;
using DataLayer.Utils;
using DataLayer.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using DataLayer.ViewModels;
using Microsoft.Ajax.Utilities;
using System.Linq;
using ERP_System.Utils;
using System;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class ContactController : Controller
    {
        ContactHandler handler = new ContactHandler();
        DbConnector db = new DbConnector();
        // GET: Contact
        [RBAC(AccessType = 2)]
        public ActionResult Index(string Type, string ErrorMsg)
        {
            ContactViewModel model = new ContactViewModel();

            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }

            if (!string.IsNullOrEmpty(Type))
            {
                model.Type = Type;
            }
            else
            {
                model.Type = "Supplier";
            }
            model.ContactList = handler.GetContactList(model.Type);
            return View(model);
        }

        [RBAC(AccessType = 0)]
        public ActionResult Create(string Type, int? id)
        {
            Contact contact = new Contact();
            contact.Type = Type;
            if (id == null)
            {
                contact.Country = "India";               
                contact.Ledger = new Ledger();
                contact.Ledger.EntryDate = DateTime.Now;
                contact.LoginUser = new LoginUser();
                contact.Ledger.ContactSubCategoryList = DropdownManager.GetContactSubCategoryList(null);
            }
            else
            {
                contact = handler.GetContactById(id);
            }
            contact.Ledger.ContactCategoryList = DropdownManager.GetContactCategoryList(null);
            contact.LoginUser.RolesList = DropdownManager.GetRolesList(null);
            contact.DesignationList = DropdownManager.GetDesignationMasterList(null);
            contact.DivisionList = DropdownManager.GetDivisionList(null);
            contact.BranchList = DropdownManager.GetBranchList(null);
            return View(contact);
        }

        [HttpPost]
        public ActionResult Create(Contact contact)
        {
            string errorMessage = handler.CreateContact(contact);
            if(string.IsNullOrEmpty(errorMessage))
            {
                return RedirectToAction("Index", new { Type = contact.Type });
            }
            else
            {
                
            }
            return View(contact);
        }

        public ActionResult _CreateCustomer()
        {
            return View();
        }

        public ActionResult _CreateEmployee()
        {
            return View();
        }

        public ActionResult _CreateSupplier()
        {
            return View();
        }

        public ActionResult _CreateLedger()
        {
            return View();
        }

        public ActionResult _CreateUser()
        {
            return View();
        }

        [HttpPost]
        public ActionResult _ItemList(string Type)
        {                        
            ContactViewModel model = new ContactViewModel();
            model.Type = Type;
            model.ContactList = handler.GetContactList(model.Type);
            return PartialView(model);
        }

        [RBAC(AccessType = 3)]
        public ActionResult Delete(int id, string Type)
        {
            if(id == SessionManager.GetSessionUser().ContactId)
            {
                return RedirectToAction("Index", new { ErrorMsg = "User is Currently Using Software,Cant be Deleted." });
            }
            if (handler.DeleteContact(id))
            {
                return RedirectToAction("Index", new { Type = Type });
            }
            return RedirectToAction("Index", new { ErrorMsg = MessageStore.DeleteError });
        }

        [HttpPost]
        public JsonResult ExistingUser(string UserName)
        {
            DbConnector dbObject = new DbConnector();
            LoginUser model  = dbObject.LoginUsers.Where(x => x.Username == UserName).FirstOrDefault();
            if (model == null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Details(int id)
        {
            return View();
        }
    }
}