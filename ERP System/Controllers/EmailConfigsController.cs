﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using DataLayer;
using DataLayer.Models;
using ERP_System.Utils;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class EmailConfigsController : Controller
    {
        DbConnector db = new DbConnector();
        SMSEMailDbUtil smsdb = new SMSEMailDbUtil();
        // GET: EmailConfigs
        public ActionResult Index()
        {
            return View(db.EmailConfigs.ToList());
        }

        // GET: EmailConfigs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmailConfig emailConfig = db.EmailConfigs.Find(id);
            if (emailConfig == null)
            {
                return HttpNotFound();
            }
            return View(emailConfig);
        }

        // GET: EmailConfigs/Create
        public ActionResult Create(int? id)
        {
            ViewData["Operation"] = id;
            ViewData["users"] = smsdb.GetEmail();
            EmailConfig email = smsdb.GetEmailById(id);
            return View(email);
        }


        // POST: EmailConfigs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Create(EmailConfig emailConfig)
        {
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if ((emailConfig.Password != null) && (emailConfig.UserEmailId != null))
            {
                if (emailConfig.EmailConfigId == 0)
                {
                    if (smsdb.SaveEmail(emailConfig) < 0)
                    {

                    }
                }
                else
                {
                    if (smsdb.EditEmail(emailConfig) < 0)
                    {

                    }
                }
            }
            return RedirectToAction("Create", new { id = 0 });
        }

        // GET: EmailConfigs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmailConfig emailConfig = db.EmailConfigs.Find(id);
            if (emailConfig == null)
            {
                return HttpNotFound();
            }
            return View(emailConfig);
        }

        // POST: EmailConfigs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EmailConfigId,UserEmailId,Password,IsDefault,Host,Port,EnableSsl,UsedDefaultCredentials")] EmailConfig emailConfig)
        {
            if (ModelState.IsValid)
            {
                db.Entry(emailConfig).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(emailConfig);
        }

        // GET: EmailConfigs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmailConfig emailConfig = db.EmailConfigs.Find(id);
            if (emailConfig == null)
            {
                return HttpNotFound();
            }
            if (smsdb.DelteEmailConfig(emailConfig) < 0)
            {

            }
            return RedirectToAction("Create");

        }

        // POST: EmailConfigs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EmailConfig emailConfig = db.EmailConfigs.Find(id);
            db.EmailConfigs.Remove(emailConfig);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}