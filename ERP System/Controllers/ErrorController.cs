﻿using System.Web.Mvc;

namespace ERP_System.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult UnauthorizedAccess()
        {
            return View();
        }

        public ActionResult ErrorDescription()
        {
            return View();
        }
    }
}