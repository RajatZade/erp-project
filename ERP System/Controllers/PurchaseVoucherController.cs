﻿using DataLayer;
using DataLayer.Models;
using DataLayer.Utils;
using DataLayer.ViewModels;
using ERP_System.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class PurchaseVoucherController : Controller
    {
        // GET: PurchaseVoucher
        DbConnector db = new DbConnector();
        PurchaseVoucherHandler handler = new PurchaseVoucherHandler();

        [RBAC(AccessType = 2)]
        public ActionResult Index(int? PageNo, string ErrorMsg)
        {
            PurchaseInvoiceViewModel model = new PurchaseInvoiceViewModel();
            model.PurchaseInvoiceLists = db.GetPurchaseInvoice(PageNo,Constants.PURCHASE_TYPE_PURCHASE_VOUCHER);
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            return View(model);
        }

        [RBAC(AccessType = 0)]
        public ActionResult Create(int? id, string ErrorMsg)
        {
            PurchaseInvoice purchaseinvoice = handler.GetPurchaseInvoiceForCreateEdit(id);
            return View(purchaseinvoice);
        }

        [RBAC(AccessType = 1)]
        public ActionResult Edit(int id)
        {
            return RedirectToAction("Create", new { id = id });
        }

        [RBAC(AccessType = 3)]
        public ActionResult Delete(int id)
        {
            PurchaseInvoice purchaseInvoice = db.PurchaseInvoices.Where(x => x.PurchaseInvoiceId == id).FirstOrDefault();
            if (purchaseInvoice == null)
            {
                return HttpNotFound();
            }
            else
            {
                if (handler.DeletePurchaseInvoice(purchaseInvoice))
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index", new { ErrorMsg = MessageStore.CantDeleteProductSold });
        }

        [HttpPost]
        public ActionResult Create(PurchaseInvoice purchaseInvoice, string btn)
        {
            int id = handler.SavePurchaseInvoice(purchaseInvoice, btn);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult _AddProductVoucher(ProductVoucher model)
        {
            PurchaseInvoice invoice = new PurchaseInvoice();
            if (model.PurchaseInvoiceId > 0)
            {
                handler.SaveProductVoucher(model);
                invoice.ProductVoucherItems = handler.GetProductVoucherList(model.PurchaseInvoiceId);

            }
            else
            {
                SessionManager.ProductVoucher(model, Constants.PURCHASE_TYPE_PURCHASE_VOUCHER);
                invoice.ProductVoucherItems = SessionManager.GetProductVoucherList(Constants.PURCHASE_TYPE_PURCHASE_VOUCHER);
            }

            if (invoice.ProductVoucherItems != null)
            {
                Dictionary<int, GST> idVsGST = db.GetGSTMasterDictionary(invoice.ProductVoucherItems.Select(x => x.GSTId.Value).ToList());
                foreach (var Newitem in invoice.ProductVoucherItems)
                {
                    invoice.ItemSubTotal = invoice.ItemSubTotal + (Newitem.Quantity * Newitem.Rate);
                    invoice.NotMappedTotalTax = (invoice.NotMappedTotalTax ?? 0) + Newitem.TotalTax;
                    Newitem.GST = idVsGST[Newitem.GSTId.Value];
                }
            }
            return PartialView(invoice);
        }


        [HttpPost]
        public JsonResult EditProductVoucher(int ProductVoucherId, int? PurchaseInvoiceId)
        {
            ProductVoucher model = null;
            if (PurchaseInvoiceId > 0)
            {
                model = db.ProductVouchers.Where(x => x.ProductVoucherId == ProductVoucherId).FirstOrDefault();
            }
            else
            {
                List<ProductVoucher> ProductVoucherList = SessionManager.GetProductVoucherList(Constants.PURCHASE_TYPE_PURCHASE_VOUCHER);
                if (ProductVoucherList != null)
                {
                    model = ProductVoucherList.Where(x => x.ProductVoucherId == ProductVoucherId).FirstOrDefault();
                }
            }
            return Json(model);
        }

        public ActionResult RemoveProductVoucher(int ProductVoucherId, int? PurchaseInvoiceId)
        {
            PurchaseInvoice invoice = new PurchaseInvoice();
            if (PurchaseInvoiceId > 0)
            {
                ProductVoucher model = db.ProductVouchers.Where(x => x.ProductVoucherId == ProductVoucherId).FirstOrDefault();
                db.RemoveProductVoucher(model);
                invoice.ProductVoucherItems = handler.GetProductVoucherList(model.PurchaseInvoiceId);
            }
            else
            {
                SessionManager.RemoveProductVoucher(ProductVoucherId,Constants.PURCHASE_TYPE_PURCHASE_VOUCHER);
                invoice.ProductVoucherItems = SessionManager.GetProductVoucherList(Constants.PURCHASE_TYPE_PURCHASE_VOUCHER);
            }
            if (invoice.ProductVoucherItems != null)
            {
                Dictionary<int, GST> idVsGST = db.GetGSTMasterDictionary(invoice.ProductVoucherItems.Select(x => x.GSTId.Value).ToList());
                foreach (var Newitem in invoice.ProductVoucherItems)
                {
                    invoice.ItemSubTotal = invoice.ItemSubTotal + (Newitem.Quantity * Newitem.Rate);
                    invoice.NotMappedTotalTax = (invoice.NotMappedTotalTax ?? 0) + Newitem.TotalTax;
                    Newitem.GST = idVsGST[Newitem.GSTId.Value];
                }
            }
            return PartialView("_AddProductVoucher", invoice);
        }

        public void Update(int id)
        {
            PurchaseInvoice Invoice = db.PurchaseInvoices.Where(x => x.PurchaseInvoiceId == id).FirstOrDefault();




            #region PurchaseLedger  
            Ledger PurchaseLedger = db.Ledgers.Where(x => x.Name == Constants.LEDGER_TYPE_PURCHASE_RETURN_LEDGER).FirstOrDefault();
            if (PurchaseLedger != null)
            {
                Voucher PurchaseLedgerVoucher = db.Vouchers.Where(x => x.LedgerId == PurchaseLedger.LedgerId && x.PurchaseInvoiceId == Invoice.PurchaseInvoiceId && x.Type == Constants.CR).FirstOrDefault();
                if (PurchaseLedgerVoucher != null)
                {
                    PurchaseLedgerVoucher.Total = Invoice.TotalInvoiceValue.Value;
                    db.SaveVauchers(PurchaseLedgerVoucher);
                }
                else
                {
                    PurchaseLedgerVoucher = new Voucher();
                    PurchaseLedgerVoucher.LedgerId = PurchaseLedger.LedgerId;
                    PurchaseLedgerVoucher.PurchaseInvoiceId = Invoice.PurchaseInvoiceId;
                    PurchaseLedgerVoucher.Total = Invoice.TotalInvoiceValue.Value;
                    PurchaseLedgerVoucher.Type = Constants.CR;
                    PurchaseLedgerVoucher.InvoiceNumber = Invoice.PurchaseInvoiceNo;
                    PurchaseLedgerVoucher.EntryDate = Invoice.InvoicingDate ?? DateTime.Now;
                    PurchaseLedgerVoucher.Particulars = "Purchase Voucher Number :" + Invoice.PurchaseInvoiceNo;
                    PurchaseLedgerVoucher.VoucherFor = Constants.PURCHASE_TYPE_PURCHASE_VOUCHER;
                    db.SaveVauchers(PurchaseLedgerVoucher);
                }
            }
            #endregion
        }
    }
}