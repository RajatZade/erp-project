﻿using DataLayer.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace ERP_System.Controllers
{
    public class CSVController  : Controller
    {
        // GET: CSV
        //public ActionResult Index()
        //{
        //    CSVViewModel model = new CSVViewModel();
        //    model.TableList = new List<SelectListItem>();
        //    List<string> TableNames = GetTables();
        //    if (TableNames != null && TableNames.Count > 0)
        //    {
        //        foreach (var item in TableNames)
        //        {
        //            SelectListItem selectItem = new SelectListItem() { Value = item, Text = item };
        //            model.TableList.Add(selectItem);
        //        }
        //    }
        //    //string Connection = "Data Source=WMPC-3;Initial Catalog=ERP09;Persist Security Info=True;User ID=sa;Password=root";
        //    //CreateBackup _backup = new CreateBackup(Connection, ConfigurationManager.AppSettings["BackupFolder"]);
        //    return View(model);
        //}

        public static List<string> GetTables()
        {
            using (SqlConnection connection = new SqlConnection(@"Data Source=WMPC-3;Initial Catalog=ERP09;Persist Security Info=True;User ID=sa;Password=root;"))
            {
                connection.Open();

                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText =
                        @"SELECT s.name, o.name
                FROM sys.objects o WITH(NOLOCK)
                JOIN sys.schemas s WITH(NOLOCK)
                ON o.schema_id = s.schema_id
                WHERE o.is_ms_shipped = 0 AND RTRIM(o.type) = 'U'
                ORDER BY s.name ASC, o.name ASC";

                    List<string> TableNames = new List<string>();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string schemaName = reader.GetString(0);
                            string tableName = reader.GetString(1);
                            TableNames.Add(tableName);
                            // your code goes here...
                        }
                    }
                    return TableNames;
                }
            }
        }

        //    [HttpPost]
        //    public ActionResult Index(CSVViewModel model)
        //    {
        //        if (Request.Files.Count > 0)
        //        {
        //            HttpPostedFileBase file = Request.Files[0];
        //            StreamReader csvreader = new StreamReader(file.InputStream);
        //            PageModel ColumnList = GetTable(model.TableNam);
        //            DataTable dt = new DataTable(model.TableNam);
        //            for (int i = 0; i < ColumnList.DataTable1s.Count; i++)
        //            {
        //                dt.Columns.Add(ColumnList.DataTable1s[i].ColumnName, ColumnList.DataTable1s[i].type);
        //            }
        //            while (!csvreader.EndOfStream)
        //            {
        //                var line = csvreader.ReadLine();
        //                var values = line.Split(';');
        //                List<string> ValueList = line.Split(',').ToList();
        //                List<object> NewList = new List<object>();
        //                for (int i = 0; i < ColumnList.DataTable1s.Count; i++)
        //                {
        //                    Type type = ColumnList.DataTable1s[i].type;
        //                    if (type.Name == "DateTime")
        //                    {
        //                        //DateTime date = DateTime.ParseExact(ValueList[i], "dd/MM/yyyy", null);
        //                        DateTime date = Convert.ToDateTime(DateTime.Now);
        //                        var propValue = Convert.ChangeType(date, type);
        //                        NewList.Add(propValue);
        //                        //dt.Rows.Add(propValue);
        //                    }
        //                    else
        //                    {
        //                        var propValue = Convert.ChangeType(ValueList[i], type);
        //                        NewList.Add(propValue);
        //                        //dt.Rows.Add(propValue);
        //                    }                                                
        //                }
        //                dt.Rows.Add(new object[] { NewList });
        //            }
        //            string sqlselectQuery = "select * from " + model.TableNam + "";
        //        }
        //        return RedirectToAction("Index");
        //    }

        //    public static PageModel GetTable(string TableName)
        //    {
        //        using (SqlConnection connection = new SqlConnection(@"Data Source=WMPC-3;Initial Catalog=ERP09;Persist Security Info=True;User ID=sa;Password=root;"))
        //        {
        //            connection.Open();
        //            using (SqlCommand command = connection.CreateCommand())
        //            {
        //                command.CommandText =
        //                    @"select * from " + TableName + "";

        //                PageModel intList = new PageModel();

        //                using (SqlDataReader reader = command.ExecuteReader())
        //                {
        //                    intList.DataTable1s = new Collection<DataTable1>();
        //                    for (int i = 0; i < reader.FieldCount; i++)
        //                    {                            
        //                        Type type = reader.GetType();
        //                        DataTable1 model = new DataTable1();
        //                        model.ColumnName = reader.GetName(i);
        //                        model.type = reader.GetFieldType(i);
        //                        var columnName = reader.GetName(i);
        //                        //var value = reader[i];
        //                        //var dotNetType = reader.GetFieldType(i);
        //                        //var sqlType = reader.GetDataTypeName(i);
        //                        //var specificType = reader.GetProviderSpecificFieldType(i);
        //                        intList.DataTable1s.Add(model);                            
        //                    }
        //                }
        //                return intList;
        //            }
        //        }
        //    }
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(HttpPostedFileBase file)
        {
            DataSet ds = new DataSet();
            if (Request.Files["file"].ContentLength > 0)
            {
                string fileExtension =
                                     System.IO.Path.GetExtension(Request.Files["file"].FileName);

                if (fileExtension == ".xls" || fileExtension == ".xlsx")
                {
                    string fileLocation = Server.MapPath("~/Content/") + Request.Files["file"].FileName;
                    if (System.IO.File.Exists(fileLocation))
                    {

                        System.IO.File.Delete(fileLocation);
                    }
                    Request.Files["file"].SaveAs(fileLocation);
                    string excelConnectionString = string.Empty;
                    excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                    fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    //connection String for xls file format.
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" +
                        fileLocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    }
                    //connection String for xlsx file format.
                    else if (fileExtension == ".xlsx")
                    {
                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                        fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }
                    //Create Connection to Excel work book and add oledb namespace
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                    excelConnection.Open();
                    DataTable dt = new DataTable();

                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    String[] excelSheets = new String[dt.Rows.Count];
                    int t = 0;
                    //excel data saves in temp file here.
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);


                    string query = string.Format("Select * from [{0}]", excelSheets[0]);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }
                }
                if (fileExtension.ToString().ToLower().Equals(".xml"))
                {
                    string fileLocation = Server.MapPath("~/Content/") + Request.Files["FileUpload"].FileName;
                    if (System.IO.File.Exists(fileLocation))
                    {
                        System.IO.File.Delete(fileLocation);
                    }

                    Request.Files["FileUpload"].SaveAs(fileLocation);
                    XmlTextReader xmlreader = new XmlTextReader(fileLocation);
                    // DataSet ds = new DataSet();
                    ds.ReadXml(xmlreader);
                    xmlreader.Close();
                }

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string conn = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    SqlConnection con = new SqlConnection(conn);
                    string query = "Insert into Person(Name,Email,Mobile) Values('" +
                    ds.Tables[0].Rows[i][0].ToString() + "','" + ds.Tables[0].Rows[i][1].ToString() +
                    "','" + ds.Tables[0].Rows[i][2].ToString() + "')";
                    con.Open();
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            return View();
        }
    }
}
