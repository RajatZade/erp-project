﻿using DataLayer;
using DataLayer.Models;
using DataLayer.Utils;
using DataLayer.ViewModels;
using ERP_System.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PaymentGateway;
using System.Configuration;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class SaleVoucherController : Controller
    {
        DbConnector db = new DbConnector();
        SaleVoucherHandler handler = new SaleVoucherHandler();

        // GET: PurchaseInvoice
        [RBAC(AccessType = 2)]
        public ActionResult Index(int? PageNo, string ErrorMsg)
        {
            SaleInvoiceViewModel model = new SaleInvoiceViewModel
            {
                SaleInvoiceLists = db.GetSaleInvoice(PageNo, Constants.SALE_TYPE_SALE_VOUCHER)
            };
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            return View(model);
        }


        [RBAC(AccessType = 0)]
        public ActionResult Create(int? id)
        {
            SaleInvoice saleinvoice = handler.GetSaleVoucher(id);
            return View(saleinvoice);
        }

        [RBAC(AccessType = 1)]
        public ActionResult Edit(int id)
        {
            return RedirectToAction("Create", new { id = id });
        }

        [RBAC(AccessType = 3)]
        public ActionResult Delete(int id)
        {
            string errorMessage = handler.DeleteInvoice(id);
            return RedirectToAction("Index", new { ErrorMsg = errorMessage });
        }

        [HttpPost]
        public ActionResult Create(SaleInvoice saleInvoice, string btn)
        {
            saleInvoice = handler.SaveSaleInvoice(saleInvoice, btn);
            switch (btn)
            {
                case "Hold":
                    {
                        return RedirectToAction("Index");
                    }
                case "Save":
                    {
                        return RedirectToAction("Index");
                    }
                case "Make Payment":
                    {
                        return RedirectToAction("PlaceOrder", new { SaleInvoiceId = saleInvoice.SaleInvoiceId });
                    }
                case "Email":
                    {
                        break;
                    }
                case "SMS":
                    {
                        break;
                    }
            }
            return RedirectToAction("Create", new { id = saleInvoice.SaleInvoiceId });
        }

        [HttpPost]
        public ActionResult _AddProductVoucher(ProductVoucher model)
        {
            SaleInvoice invoice = new SaleInvoice();
            if (model.SaleInvoiceId > 0)
            {
                handler.SaveProductVoucher(model);
                invoice.ProductVoucherItems = handler.GetProductVoucherList(model.SaleInvoiceId);

            }
            else
            {
                SessionManager.ProductVoucher(model, Constants.SALE_TYPE_SALE_VOUCHER);
                invoice.ProductVoucherItems = SessionManager.GetProductVoucherList(Constants.SALE_TYPE_SALE_VOUCHER);
            }

            invoice = handler.Calculate(invoice);
            return PartialView(invoice);
        }


        [HttpPost]
        public JsonResult EditProductVoucher(int ProductVoucherId, int? SaleInvoiceId)
        {
            ProductVoucher model = null;
            if (SaleInvoiceId > 0)
            {
                model = db.ProductVouchers.Where(x => x.ProductVoucherId == ProductVoucherId).FirstOrDefault();
            }
            else
            {
                List<ProductVoucher> ProductVoucherList = SessionManager.GetProductVoucherList(Constants.SALE_TYPE_SALE_VOUCHER);
                if (ProductVoucherList != null)
                {
                    model = ProductVoucherList.Where(x => x.ProductVoucherId == ProductVoucherId).FirstOrDefault();
                }
            }
            return Json(model);
        }

        public ActionResult RemoveProductVoucher(int ProductVoucherId, int? SaleInvoiceId)
        {
            SaleInvoice invoice = new SaleInvoice();
            if (SaleInvoiceId > 0)
            {
                ProductVoucher model = db.ProductVouchers.Where(x => x.ProductVoucherId == ProductVoucherId).FirstOrDefault();
                db.RemoveProductVoucher(model);
                invoice.ProductVoucherItems = handler.GetProductVoucherList(SaleInvoiceId);
            }
            else
            {
                SessionManager.RemoveProductVoucher(ProductVoucherId, Constants.SALE_TYPE_SALE_VOUCHER);
                invoice.ProductVoucherItems = SessionManager.GetProductVoucherList(Constants.SALE_TYPE_SALE_VOUCHER);
            }
            invoice = handler.Calculate(invoice);
            return PartialView("_AddProductVoucher", invoice);
        }

        public ActionResult PlaceOrder(int? SaleInvoiceId)
        {
            SaleInvoice saleinvoice = null;
            if (SaleInvoiceId.HasValue)
            {
                saleinvoice = db.SaleInvoices.Include("SaleInvoiceItemList").Where(x => x.SaleInvoiceId == SaleInvoiceId).FirstOrDefault();
                saleinvoice.ProductVoucherItems = handler.GetProductVoucherList(SaleInvoiceId);
            }
            foreach (var item in saleinvoice.SaleInvoiceItemList)
            {
                item.GST = db.GetGSTMasterById(item.GSTId);
            }
            if (saleinvoice.CustomerId == 61)
            {
                saleinvoice.PaymentTypeId = 1;
            }
            saleinvoice.PaymentTypeList = DropdownManager.GetPaymentTypeList(null);
            //saleinvoice.BankAccountsList = DropdownManager.BankAccountList(null);
            if (saleinvoice.ProductVoucherItems != null)
            {
                Dictionary<int, GST> idVsGST = db.GetGSTMasterDictionary(saleinvoice.ProductVoucherItems.Select(x => x.GSTId.Value).ToList());
                foreach (var Newitem in saleinvoice.ProductVoucherItems)
                {
                    //invoice.ItemSubTotal = invoice.ItemSubTotal + (Newitem.Quantity * Newitem.Rate);
                    //invoice.NotMappedTotalTax = (invoice.NotMappedTotalTax ?? 0) + Newitem.TotalTax;
                    Newitem.GST = idVsGST[Newitem.GSTId.Value];
                }
            }
            return View(saleinvoice);
        }

        [HttpPost]
        public ActionResult PlaceOrder(SaleInvoice SaleInvoice, string btn)
        {
            SaleInvoice item = db.GetSalesInvoiceById(SaleInvoice.SaleInvoiceId);
            if (item.IsLedgerUpdate == false)
            {
                if (SaleInvoice.PaymentTypeId == 5)
                {
                    PaymentGateWay model = new PaymentGateWay();
                    PaymentGateWayViewModel ViewModel = new PaymentGateWayViewModel();
                    Contact Contact = db.Contacts.Where(x => x.ContactId == item.Customer.ContactId).FirstOrDefault();
                    ViewModel.Email = Contact.Email;
                    ViewModel.Name = Contact.FirstName;
                    ViewModel.Phone = Contact.Phone;
                    ViewModel.Amount = item.TotalWithAdjustment.ToString();
                    ViewModel.OrderId = item.SaleInvoiceId.ToString();
                    ViewModel.OnSuccess = Constants.OnSuccessOfSaleVoucher;
                    ViewModel.OnFailure = Constants.OnFailure;
                    model.GotoPayment(ViewModel, btn);
                }
                else if (SaleInvoice.PaymentTypeId == 7)
                {
                    handler.LedgerUpdate(SaleInvoice.SaleInvoiceId, SaleInvoice.PaymentTypeId ?? 0);
                    item.IsLedgerUpdate = true;
                    item.PaymentTypeId = SaleInvoice.PaymentTypeId;
                    item.Status = Constants.CREDIT_SALE;
                }
                else
                {
                    handler.LedgerUpdate(SaleInvoice.SaleInvoiceId, SaleInvoice.PaymentTypeId ?? 0);
                    item.IsLedgerUpdate = true;
                    item.PaymentTypeId = SaleInvoice.PaymentTypeId;
                    item.Status = Constants.PAYMENT_DONE;
                    item.Cash = SaleInvoice.Cash;
                    item.TenderChange = SaleInvoice.TenderChange;
                    item.ChequeNumber = SaleInvoice.ChequeNumber;
                }
                db.SaveSaleInvoiceEdit(item);
                db.SaveChanges();
            }
            switch (btn)
            {
                case "Print Invoice":
                    {
                        return RedirectToAction("GenerateInvoice", new { InvoiceId = SaleInvoice.SaleInvoiceId });
                    }
                case "Print POS":
                    {
                        return RedirectToAction("POS", new { InvoiceId = SaleInvoice.SaleInvoiceId });
                    }
                case "Save":
                    {
                        return RedirectToAction("Index");
                    }
            }
            return null;
        }


        [HttpPost]
        public ActionResult Return(FormCollection form)
        {
            PaymentGateWay model = new PaymentGateWay();
            try
            {
                string[] merc_hash_vars_seq;
                string merc_hash_string = string.Empty;
                string merc_hash = string.Empty;
                string SaleInvoiceId = string.Empty;
                string mode = string.Empty;
                string payuMoneyId = string.Empty;
                string txnid = string.Empty;
                string udf1 = string.Empty;
                string hash_seq = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
                string Status = form["status"].ToString();
                if (Status == "success")
                {

                    merc_hash_vars_seq = hash_seq.Split('|');
                    Array.Reverse(merc_hash_vars_seq);
                    merc_hash_string = ConfigurationManager.AppSettings["SALT"] + "|" + form["status"].ToString();
                    foreach (string merc_hash_var in merc_hash_vars_seq)
                    {
                        merc_hash_string += "|";
                        merc_hash_string = merc_hash_string + (form[merc_hash_var] ?? "");
                    }
                    Response.Write(merc_hash_string);
                    merc_hash = model.Generatehash512(merc_hash_string).ToLower();
                    if (merc_hash != form["hash"])
                    {
                        Response.Write("Hash value did not matched");
                    }
                    else
                    {
                        mode = Request.Form["mode"];
                        payuMoneyId = Request.Form["payuMoneyId"];
                        txnid = Request.Form["txnid"];
                        SaleInvoiceId = Request.Form["udf1"];
                        string btn = Request.Form["udf2"];
                        SaleInvoice SaleInvoice = db.GetSalesInvoiceById(Convert.ToInt32(SaleInvoiceId));
                        SaleInvoice.PaymentTypeId = 5;
                        SaleInvoice.Status = Constants.PAYMENT_DONE;
                        SaleInvoice.IsLedgerUpdate = true;
                        handler.LedgerUpdate(SaleInvoice.SaleInvoiceId, SaleInvoice.PaymentTypeId ?? 0);
                        db.SaveSaleInvoiceEdit(SaleInvoice);
                        switch (btn)
                        {
                            case "Print Invoice":
                                {
                                    return RedirectToAction("GenerateInvoice", new { InvoiceId = SaleInvoice.SaleInvoiceId });
                                }
                            case "Print WSP":
                                {
                                    return RedirectToAction("GenerateInvoice", new { InvoiceId = SaleInvoice.SaleInvoiceId });
                                }
                            case "Print POS":
                                {
                                    return RedirectToAction("POS", new { InvoiceId = SaleInvoice.SaleInvoiceId });
                                }
                            case "Save":
                                {
                                    return RedirectToAction("Index");
                                }
                        }
                    }

                }
                else
                {
                    SaleInvoiceId = Request.Form["udf1"];
                    return RedirectToAction("Failure", "Checkout", new { id = SaleInvoiceId });
                }
            }

            catch (Exception ex)
            {
                Response.Write("<span style='color:red'>" + ex.Message + "</span>");

            }
            return null;
        }

        public ActionResult POS(int InvoiceId)
        {
            GenerateSalesInovice model = new GenerateSalesInovice
            {
                SalesInvoice = db.GetSalesInvoiceById(InvoiceId)
            };
            model.SalesInvoice.ProductVoucherItems = db.ProductVouchers.Where(x => x.SaleInvoiceId == InvoiceId).ToList();
            model.GstList = new List<POS_GST_LIST>();
            Dictionary<int, GST> idVsGST = db.GetGSTMasterDictionary(model.SalesInvoice.ProductVoucherItems.Select(x => x.GSTId.Value).ToList());
            Dictionary<int, Unit> idVsUnit = db.GetUnitMasterDictionary(model.SalesInvoice.ProductVoucherItems.Select(x => x.UnitId ?? 0).ToList());
            model.SalesInvoice.TotalQuantity = 0;
            foreach (var Item in model.SalesInvoice.ProductVoucherItems)
            {
                model.SalesInvoice.TotalQuantity = model.SalesInvoice.TotalQuantity + Item.Quantity;
                Item.GST = idVsGST[Item.GSTId.Value];
                if (Item.UnitId.HasValue && Item.UnitId != 0 && Item.UnitId != -1)
                {
                    Item.Unit = idVsUnit[Item.UnitId.Value];
                }

                model.SalesInvoice.TotalSGST += Item.SGST;
                model.SalesInvoice.TotalCGST += Item.CGST;
                model.SalesInvoice.TotalIGST += Item.IGST;

                //model.SalesInvoice.TotalNotMapped = model.SalesInvoice.TotalNotMapped + Item.SubTotal;

                POS_GST_LIST ListItem = model.GstList.Where(x => x.GSTId == Item.GSTId).FirstOrDefault();
                if (ListItem == null)
                {
                    ListItem = new POS_GST_LIST
                    {
                        GST = Item.GST,
                        GSTId = Item.GSTId.Value,
                        IGST = Item.IGST,
                        SGST = Item.SGST,
                        CGST = Item.CGST,
                        Amount = Item.Rate * Item.Quantity - (Item.TotalDiscount ?? 0),
                    };
                    model.TotalBasePrice = model.TotalBasePrice + ListItem.Amount.Value;
                    model.GstList.Add(ListItem);
                }
                else
                {
                    ListItem.IGST = ListItem.IGST + Item.IGST;
                    ListItem.SGST = ListItem.SGST + Item.SGST;
                    ListItem.CGST = ListItem.CGST + Item.CGST;
                    ListItem.Amount = ListItem.Amount + Item.Rate * Item.Quantity - (Item.TotalDiscount ?? 0);
                    model.TotalBasePrice = model.TotalBasePrice + Item.Rate * Item.Quantity - (Item.TotalDiscount ?? 0);
                }
            }
            model.Salesman = db.Ledgers.Where(x => x.ContactId == model.SalesInvoice.EmployeeId.Value).Select(x => x.Contact).FirstOrDefault();
            model.GstList = model.GstList.OrderByDescending(x => x.GST.SGST).ToList();
            model.Ledger = db.Ledgers.Where(x => x.LedgerId == model.SalesInvoice.CustomerId).FirstOrDefault();
            model.Contact = SessionManager.GetSessionUser().Contact;
            if (model.Contact == null)
            {
                model.Contact = new Contact();
            }
            //model.Shipping = db.GetShippingById(model.SalesInvoice.ShippingId);
            model.Company = SessionManager.GetSessionCompany();
            model.Branch = SessionManager.GetSessionBranch();
            model.PaymentType = db.GetPaymentTypeById(model.SalesInvoice.PaymentTypeId);
            model.SalesInvoice.TotalInWord = CommonUtils.NumberToWords(Convert.ToInt32(model.SalesInvoice.TotalInvoiceValue));
            return View(model);
        }


        public ActionResult GenerateInvoice(int InvoiceId)
        {
            GenerateSalesInovice model = new GenerateSalesInovice
            {
                SalesInvoice = db.GetSalesInvoiceById(InvoiceId)
            };
            model.SalesInvoice.ProductVoucherItems = db.ProductVouchers.Where(x => x.SaleInvoiceId == InvoiceId).ToList();
            model.SalesInvoice.TotalQuantity = 0;
            foreach (var Item in model.SalesInvoice.ProductVoucherItems)
            {
                model.SalesInvoice.TotalQuantity = model.SalesInvoice.TotalQuantity + Item.Quantity;
                Item.GST = db.GetGSTMasterById(Item.GSTId.Value);
                model.SalesInvoice.TotalSGST += Item.SGST;
                model.SalesInvoice.TotalCGST += Item.CGST;
                model.SalesInvoice.TotalIGST += Item.IGST;
                if (Item.DiscountId == -1)
                {
                    Item.SaleDiscountPercent = 0;
                }
                else
                {
                    Item.SaleDiscountPercent = db.Discounts.Where(x => x.DiscountId == Item.DiscountId).Select(x => x.DiscountPercent).FirstOrDefault();
                }
            }
            model.Ledger = db.Ledgers.Where(x => x.LedgerId == model.SalesInvoice.CustomerId).FirstOrDefault();
            model.Contact = db.Contacts.Where(x => x.ContactId == model.Ledger.ContactId).FirstOrDefault();
            if (model.Contact == null)
            {
                model.Contact = new Contact();
            }
            model.SaleOrderNumber = db.SalesOrders.Where(x => x.SalesOrderId == model.SalesInvoice.SaleOrderId).Select(x => x.SaleOrderNumber).FirstOrDefault();
            model.Company = SessionManager.GetSessionCompany();
            model.Branch = SessionManager.GetSessionBranch();
            model.PaymentType = db.GetPaymentTypeById(model.SalesInvoice.PaymentTypeId);
            model.SalesInvoice.TotalInWord = CommonUtils.NumberToWords(Convert.ToInt32(model.SalesInvoice.TotalWithAdjustment));
            model.SaleType = db.Ledgers.Where(x => x.LedgerId == model.SalesInvoice.SaleTypeId).Select(x => x.Name).FirstOrDefault();
            model.SalesMan = db.Ledgers.Where(x => x.LedgerId == model.SalesInvoice.EmployeeId).Select(x => x.Name).FirstOrDefault();
            model.BankLedger = db.Ledgers.Where(x => x.LedgerId == model.Company.LedgerId).FirstOrDefault();
            return View(model);
        }

        public void Update(int id)
        {
            SaleInvoice item = db.GetSalesInvoiceById(id);
            #region SaleLedger  
            Ledger SaleLedger = db.Ledgers.Where(x => x.Name == Constants.LEDGER_TYPE_SALE_RETURN_LEDGER).FirstOrDefault();
            if (SaleLedger != null)
            {
                Voucher SaleLedgerVoucher = db.Vouchers.Where(x => x.LedgerId == SaleLedger.LedgerId && x.SaleInvoiceId == item.SaleInvoiceId && x.Type == Constants.CR).FirstOrDefault();
                if (SaleLedgerVoucher != null)
                {
                    SaleLedgerVoucher.Total = item.TotalWithAdjustment;
                    db.SaveVauchers(SaleLedgerVoucher);
                }
                else
                {
                    SaleLedgerVoucher = new Voucher();
                    SaleLedgerVoucher.LedgerId = SaleLedger.LedgerId;
                    SaleLedgerVoucher.SaleInvoiceId = item.SaleInvoiceId;
                    SaleLedgerVoucher.Total = item.TotalWithAdjustment;
                    SaleLedgerVoucher.Type = Constants.CR;
                    SaleLedgerVoucher.InvoiceNumber = item.SaleInvoiceNo;
                    SaleLedgerVoucher.EntryDate = item.InvoicingDate;
                    SaleLedgerVoucher.Particulars = "Sale Voucher Number :" + item.SaleInvoiceNo;
                    SaleLedgerVoucher.VoucherFor = Constants.SALE_TYPE_SALE_VOUCHER;
                    db.SaveVauchers(SaleLedgerVoucher);
                }
            }
            #endregion
        }
    }
}