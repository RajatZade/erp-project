﻿using DataLayer.Models;
using DataLayer;
using DataLayer.Utils;
using ERP_System.Utils;
using System.Web.Mvc;
using DataLayer.ViewModels;
using System.Linq;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class ServiceTypeController : Controller
    {
        // GET: ServiceType
        DbConnector db = new DbConnector();
        [RBAC(AccessType = 0)]
        public ActionResult Index(int? id, string ErrorMsg)
        {
            ServiceTypeViewModel model = new ServiceTypeViewModel();
            if (model.ServiceTypeList == null)
            {
                model.ServiceTypeList = db.GetServiceTypeList();
            }
            if (id.HasValue)
            {
                model.ServiceType = db.ServiceTypes.Where(x => x.ServiceTypeId == id.Value).FirstOrDefault();
            }
            else
            {
                model.ServiceType = new ServiceType();
            }
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            return View(model);
        }

        [RBAC(AccessType = 2)]
        [HttpPost]
        public ActionResult Create(ServiceType ServiceType)
        {
            db.SaveServiceType(ServiceType);
            return RedirectToAction("Index");
        }

        [RBAC(AccessType = 3)]
        public ActionResult Delete(int id)
        {
            ServiceType ServiceType = db.ServiceTypes.Where(x => x.ServiceTypeId == id).FirstOrDefault();

            if (ServiceType == null)
            {
                return HttpNotFound();
            }
            else
            {
                if (db.RemoveServiceType(ServiceType))
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index", new { ErrorMsg = MessageStore.DeleteError });
        }
    }
}