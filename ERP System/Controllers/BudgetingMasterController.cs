﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DataLayer.Models;
using DataLayer.Utils;
using DataLayer.ViewModels;
using ERP_System.Utils;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class BudgetingMasterController : Controller
    {
        DbConnector db = new DbConnector();
        // GET: BudgetingMaster
        public ActionResult Index()
        {
            BudgetingViewModel model = new BudgetingViewModel();
            model.BudgetSetupListS = db.BudgetSetups.ToList();
            return View(model);
        }

        public ActionResult BudgetForm(int? BudgetSetupId)
        {
            BudgetSetup model = new BudgetSetup();
            if(BudgetSetupId.HasValue)
            {
                model = db.BudgetSetups.Where(x => x.BudgetSetupId == BudgetSetupId).FirstOrDefault();
                model.BudgetingList = db.Budgetings.Where(x => x.BudgetSetupId == BudgetSetupId).ToList();
                foreach(var item in model.BudgetingList)
                {
                    item.Ledger = db.Ledgers.Where(x => x.LedgerId == item.LedgerId).FirstOrDefault();
                }
            }
            model.Budgeting = new Budgeting();
            model.Budgeting.LedgerList = DropdownManager.GetLedgerListForType(null,null);
            return View(model);
        }

        [HttpPost]
        public ActionResult BudgetForm(BudgetSetup model)
        {
            db.SaveBudgetSetup(model);
            List<Budgeting> BudgetingList = SessionManager.GetBudgetList();
            foreach(var item in BudgetingList)
            {
                item.BudgetSetupId = model.BudgetSetupId;
                item.BudgetingId = 0;
                db.SaveBudget(item);
            }
            SessionManager.EmptyBudgetList();
            return RedirectToAction("BudgetForm" );
        }

        public ActionResult DeleteBudgetSetup(int BudgetSetupId)
        {
            BudgetSetup model = db.BudgetSetups.Where(x => x.BudgetSetupId == BudgetSetupId).FirstOrDefault();
            if(model != null)
            {
                model.BudgetingList = db.Budgetings.Where(x => x.BudgetSetupId == BudgetSetupId).ToList();
                foreach(var item in model.BudgetingList.ToList())
                {
                    db.DeleteBudget(item);
                }
                db.DeleteBudgetSetup(model);
            }
            
            return RedirectToAction("Index"); 
        }

        public ActionResult BudgetReport()
        {
            BudgetingViewModel model = new BudgetingViewModel();
            model.BudgetSetupList = DropdownManager.GetBudgetingList();
            model.LedgerList = DropdownManager.GetLedgerListForType(null, null);
            return View(model);
        }

        
        [HttpPost]
        public ActionResult _BudgetingList(Budgeting model)
        {
            BudgetSetup NewModel = new BudgetSetup();
            if (model.BudgetSetupId > 0)
            {
                db.SaveBudget(model);
                NewModel.BudgetingList = db.Budgetings.Where(x => x.BudgetSetupId == model.BudgetSetupId).ToList();
            }
            else
            {
                SessionManager.AddBudget(model);
                NewModel.BudgetingList = SessionManager.GetBudgetList();
            }
            foreach(var item in NewModel.BudgetingList)
            {
                item.Ledger = db.Ledgers.Where(x => x.LedgerId == item.LedgerId).FirstOrDefault();
            }
            return PartialView(NewModel);
        }

        [HttpPost]
        public JsonResult ProjectedSales(int id,DateTime? StartDate,DateTime? EndDate)
        {
            BudgetSetup model = db.BudgetSetups.Where(x => x.BudgetSetupId == id).FirstOrDefault();
            model.BudgetingList = db.Budgetings.Where(x => x.BudgetSetupId == id).ToList();
            decimal TotalExpenses = 0;
            decimal TotalPercent = 0;
            string ToDate;
            foreach (var item in model.BudgetingList)
            {
                TotalExpenses = TotalExpenses + item.AnnualBudget.Value;
                TotalPercent = TotalPercent + item.Percent.Value;
            }

            if ((StartDate != null && EndDate != null) && EndDate > StartDate && EndDate < model.EndingDate && StartDate > model.Starting)
            {
                TimeSpan t1 = EndDate.Value.Subtract(StartDate.Value);
                TimeSpan t2 = model.EndingDate.Value.Subtract(model.Starting.Value);
                decimal multiplier = Convert.ToDecimal(t1.TotalDays / t2.TotalDays);
                TotalExpenses = TotalExpenses * multiplier;
                ToDate = "From " + StartDate.Value.ToString("dd MMM yyyy") + " To " + EndDate.Value.ToString("dd MMM yyyy");
            }
            else
            {
                ToDate = "From " + model.Starting.Value.ToString("dd MMM yyyy") + " To " + model.EndingDate.Value.ToString("dd MMM yyyy");
            }
            var data = new
            {
                TotalPercent = TotalPercent,
                TotalExpenses = TotalExpenses,
                ToDate = ToDate,
                StartDate = StartDate,
            };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult _LedgerBudgetList(int id, DateTime? StartDate, DateTime? EndDate)
        {
            BudgetingViewModel model = new BudgetingViewModel();
            BudgetSetup BudgetSetup = db.BudgetSetups.Where(x => x.BudgetSetupId == id).FirstOrDefault();
            model.BudgetingList = db.Budgetings.Where(x => x.BudgetSetupId == id).ToList();
            if ((StartDate != null && EndDate != null) && EndDate > StartDate)
            {
                foreach (var item in model.BudgetingList)
                {
                    List<Voucher> VoucherList = db.Vouchers.Where(x => x.LedgerId == item.LedgerId && x.CreatedDate < EndDate && x.CreatedDate > StartDate).ToList();
                    if (VoucherList.Count > 0)
                    {
                        foreach (var item1 in VoucherList)
                        {
                            if (item1.Type == Constants.CR)
                            {
                                item.Percent = (item1.Total / item.AnnualBudget) * 100;
                                item.AnnualBudget = item1.Total;
                                model.TotalSale = model.TotalSale + item1.Total;
                            }
                            else
                            {
                                item.Percent = (item1.Total / item.AnnualBudget) * 100;
                                item.AnnualBudget = item1.Total;
                                model.TotalSale = model.TotalSale - item1.Total;
                            }
                        }
                    }
                    else
                    {
                        item.AnnualBudget = 0;
                    }
                    item.Ledger = db.Ledgers.Where(x => x.LedgerId == item.LedgerId).FirstOrDefault();
                }

            }
            else
            {
                foreach (var item in model.BudgetingList)
                {
                    List<Voucher> VoucherList = db.Vouchers.Where(x => x.LedgerId == item.LedgerId).ToList();
                    if(VoucherList.Count > 0)
                    {
                        foreach (var item1 in VoucherList)
                        {
                            if (item1.Type == Constants.CR)
                            {
                                item.Percent = (item1.Total / item.AnnualBudget) * 100;
                                item.AnnualBudget = item1.Total;
                                model.TotalSale = model.TotalSale + item1.Total;
                            }
                            else
                            {
                                item.Percent = (item1.Total / item.AnnualBudget) * 100;
                                item.AnnualBudget = item1.Total;
                                model.TotalSale = model.TotalSale - item1.Total;
                            }
                        }
                    }
                    else
                    {
                        item.AnnualBudget = 0;
                    }

                    item.Ledger = db.Ledgers.Where(x => x.LedgerId == item.LedgerId).FirstOrDefault();
                }
            }               
            return PartialView(model);
        }
    }
}