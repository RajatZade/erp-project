﻿using DataLayer;
using DataLayer.Models;
using ERP_System.Utils;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class RoleController : Controller
    {
        DbConnector db = new DbConnector();
        // GET: Role

        [RBAC(AccessType = 2)]
        public ActionResult Index()
        {
            return View(db.GetRoles());
        }

        [RBAC(AccessType = 0)]
        public ActionResult CreateRole(int? id)
        {
            Role role = new Role();
            List<Permission> permissionList = db.GetAllPermissions();
            List<SubCategory> subCategoryList = db.GetSubCategoryList(null);
            
            if (id.HasValue)
            {
                role = db.GetRole(id.Value);
                role.Companies = db.GetAccessibleCompanies();
                role.Divisions = db.GetAccessibleDivisions();
                role.Branches = db.GetAccessibleBranches();
                role.DashboardItems = db.GetDashboardItems();

                foreach (var item in permissionList)
                {
                    if (!role.PermissionSetLinkings.Any(x => x.PermissionId == item.PermissionId))
                    {
                        PermissionSetLinking linking = new PermissionSetLinking();
                        linking.Permission = item;
                        linking.PermissionId = item.PermissionId;
                        role.PermissionSetLinkings.Add(linking);
                    }
                }

                List<string> tempList = role.CompaniesAccessible.Split(',').ToList();
                foreach (var item in role.Companies)
                {
                    if(tempList.Contains(item.CompanyId.ToString()))
                    {
                        item.IsSelected = true;
                    }
                }

                tempList = role.BranchesAccessible.Split(',').ToList();
                foreach (var item in role.Branches)
                {
                    if (tempList.Contains(item.BranchId.ToString()))
                    {
                        item.IsSelected = true;
                    }
                }

                tempList = role.DivisionsAccessible.Split(',').ToList();
                foreach (var item in role.Divisions)
                {
                    if (tempList.Contains(item.DivisionId.ToString()))
                    {
                        item.IsSelected = true;
                    }
                }

                tempList = role.DashboardItemsAccessible.Split(',').ToList();
                foreach (var item in role.DashboardItems)
                {
                    if (tempList.Contains(item.DashboardItemId.ToString()))
                    {
                        item.IsSelected = true;
                    }
                }
            }
            else
            {
                role.Companies = db.GetAccessibleCompanies();
                role.Divisions = db.GetAccessibleDivisions();
                role.Branches = db.GetAccessibleBranches();
                role.DashboardItems = db.GetDashboardItems();

                role.PermissionSetLinkings = new List<PermissionSetLinking>();
                role.SubCategories = new List<SubCategoryAccessModel>();
                

                foreach (var item in permissionList)
                {
                    PermissionSetLinking linking = new PermissionSetLinking();
                    linking.Permission = item;
                    linking.PermissionId = item.PermissionId;
                    role.PermissionSetLinkings.Add(linking);
                }
                foreach (var item in subCategoryList)
                {
                    SubCategoryAccessModel model = new SubCategoryAccessModel();
                    model.Name = item.Name;
                    model.SubCategoryId = item.SubCategoryId;
                    model.IsSelected = false;
                    role.SubCategories.Add(model);
                }
            }


            return View(role);
        }

        [HttpPost]
        public ActionResult CreateRole(Role role)
        {
            
            List<int> templist = role.Companies.Where(y => y.IsSelected).Select(x => x.CompanyId).ToList();
            role.CompaniesAccessible = string.Join(",", templist);

            templist = role.Branches.Where(y => y.IsSelected).Select(x => x.BranchId).ToList();
            role.BranchesAccessible = string.Join(",", templist);

            templist = role.Divisions.Where(y => y.IsSelected).Select(x => x.DivisionId).ToList();
            role.DivisionsAccessible = string.Join(",", templist);

            templist = role.DashboardItems.Where(y => y.IsSelected).Select(x => x.DashboardItemId).ToList();
            role.DashboardItemsAccessible = string.Join(",", templist);

            db.SaveRole(role);
            return RedirectToAction("Index");
        }

        [RBAC(AccessType = 1)]
        public ActionResult Edit(int id)
        {
            return RedirectToAction("CreateRole", new { id = id });
        }

        [RBAC(AccessType = 3)]
        public ActionResult Delete(int id)
        {
            db.DeleteRole(id);
            return RedirectToAction("Create");
        }
    }
}