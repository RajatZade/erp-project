﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DataLayer.Models;
using DataLayer.ViewModels;
using DataLayer.Utils;
using ERP_System.Utils;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class HSNMasterController : Controller
    {
        DbConnector db = new DbConnector();
        // GET: UnitMaster
        [RBAC(AccessType = 2)]
        public ActionResult Index(int? id, string ErrorMsg)
        {
            HSNMasterViewModel model = new HSNMasterViewModel();
            if (model.HSNMasterList == null)
            {
                model.HSNMasterList = db.GetHSNMasterList();
            }
            if (id.HasValue)
            {
                model.HsnMaster = db.GetHSNMasterById(id.Value);
            }
            else
            {
                model.HsnMaster = new HSN();
            }
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            return View(model);
        }

        [RBAC(AccessType = 0)]
        [HttpPost]
        public ActionResult Create(HSN hsnMaster)
        {
            db.SaveHSNMaster(hsnMaster);
            return RedirectToAction("Index");
        }

        [RBAC(AccessType = 3)]
        public ActionResult Delete(int id)
        {
            HSN hsnMaster = db.HSNMasters.Find(id);

            if (hsnMaster == null)
            {
                return HttpNotFound();
            }
            else
            {
                if (db.RemoveHSN(hsnMaster))
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index", new { ErrorMsg = MessageStore.DeleteError });
        }

        [HttpPost]
        public JsonResult ExistingHSNCode(string Code)
        {
            var HSNMasters = db.HSNMasters.Where(x => x.Code == Code && x.CompanyId == (db.CompanyId ?? 0)).FirstOrDefault();
            if (HSNMasters == null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

    }
}