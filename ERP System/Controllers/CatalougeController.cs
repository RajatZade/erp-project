﻿using DataLayer;
using DataLayer.Models;
using DataLayer.ViewModels;
using System.Web.Mvc;
using DataLayer.Utils;
using System.Linq;
using ERP_System.Utils;

namespace Astrology.Areas.Console.Controllers
{
    [CheckSession]
    public class CatalougeController : Controller
    {
        DbConnector db = new DbConnector();
        [RBAC(AccessType = 2)]
        public ActionResult Index(int? Categoryid, int? SubCategoryid, string ErrorMsg)
        {
            CatalougeViewModel model = new CatalougeViewModel();
            if (model.CategoryLists == null)
            {
                model.CategoryLists = db.GetCategoryLists();
            }
            if (model.SubCategoryList == null)
            {
                model.SubCategoryList = db.GetSubCategoryList(Categoryid);
            }
            if (Categoryid.HasValue)
            {
                model.Category = db.GetCategoryById(Categoryid.Value);
            }
            else
            {
                model.Category = new Category();
            }
            if (SubCategoryid.HasValue)
            {
                model.SubCategory = db.GetSubCategoryById(SubCategoryid.Value);
            }
            else
            {
                model.SubCategory = new SubCategory();
            }
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            model.SubCategory.CategoryList = DropdownManager.GetCategoryList(model.SubCategory.CategoryId);
            return View(model);
        }

        [RBAC(AccessType = 0)]
        [HttpPost]
        public ActionResult Createcategory(Category category)
        {
            db.SaveCategory(category);
            return RedirectToAction("Index");
        }

        [RBAC(AccessType = 0)]
        [HttpPost]
        public ActionResult CreatesubCategory(SubCategory subCategory)
        {
            db.SaveSubCategory(subCategory);
            return RedirectToAction("Index");
        }

        [RBAC(AccessType = 3)]
        public ActionResult DeletesubCategory(int SubCategoryid)
        {
            SubCategory subcategory = db.SubCategories.Find(SubCategoryid);
            if (db.RemoveSubCategory(subcategory))
            {
                return RedirectToAction("Index", new { ErrorMsg = MessageStore.DeleteItem });
            }
            return RedirectToAction("Index", new { ErrorMsg = MessageStore.DeleteError });
        }

        [RBAC(AccessType = 3)]
        public ActionResult Deletecategory(int Categoryid)
        {
            Category category = db.GetCategoryById(Categoryid);
            if (db.DeleteCategory(category))
            {
                return RedirectToAction("Index", new { ErrorMsg = MessageStore.DeleteItem });
            }
            return RedirectToAction("Index", new { ErrorMsg = MessageStore.DeleteError });
        }

        [HttpPost]
        public JsonResult ExistingCategory(Category model)
        {
            Category Category = db.Categories.Where(x => x.Name == model.Name && x.CompanyId == (db.CompanyId ?? 0)).FirstOrDefault();
            if (Category == null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ExistingSubCategory(SubCategory model)
        {
            var SubCategory = db.SubCategories.Where(x => x.Name == model.Name && x.CompanyId == (db.CompanyId ?? 0)).FirstOrDefault();
            if (SubCategory == null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}