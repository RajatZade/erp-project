﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer.Models;
using DataLayer;
using DataLayer.Utils;
using DataLayer.ViewModels;
using ERP_System.Utils;
using PaymentGateway;
using System.Configuration;
using System.Data.Entity;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class SalesOrderController : Controller
    {
        DbConnector db = new DbConnector();
        // GET: SalesOrder

        #region SalesOrder
        [RBAC(AccessType = 2)]
        public ActionResult Index(int? PageNo, string ErrorMsg)
        {
            SalesOrderViewModel model = new SalesOrderViewModel();
            model.SalesOrderLists = db.GetSalesOrderLists(PageNo);
            Dictionary<int, Ledger> idVsLedger = db.GetLedgerDictionary(model.SalesOrderLists.Select(x => x.CustomerId).ToList());
            List<SalesOrderItem> SalesOrderItemList = db.GetSalesOrderItemForSaleOrder(model.SalesOrderLists.Select(x => x.SalesOrderId).ToList());
            List<SaleInvoiceItem> SaleInvoiceItemList = db.GetSaleInvoiceItemForSaleOrder(model.SalesOrderLists.Select(x => x.SalesOrderId).ToList());
            foreach (var item in model.SalesOrderLists)
            {
                item.Customer = idVsLedger[item.CustomerId];
                if(item.OrderStatus != Enums.PurchaseOrderStatus.Closed)
                {
                    item.IsAvailableForSaleInvoice = true;
                    item.SalesOrderItems = SalesOrderItemList.Where(x => x.SalesOrderId == item.SalesOrderId).ToList();
                    foreach (var item1 in item.SalesOrderItems)
                    {
                        decimal Item1Quantity = item1.Quantity;
                        decimal ProductAvailableQuantity = db.Products.Where(x => x.IsBooked == false && x.IsSold == false && x.CompanyId == db.CompanyId && x.SKUMasterId == item1.SKUMasterId).ToList().Sum(x => x.Quantity);
                        if (ProductAvailableQuantity >= Item1Quantity)
                        {

                        }
                        else
                        {
                            item.IsAvailableForSaleInvoice = false;
                            break;
                        }

                    }
                }
            }
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            return View(model);
        }

        [RBAC(AccessType = 0)]
        public ActionResult Create(int? id)
        {
            SalesOrder salesorder = null;
            if (id.HasValue)
            {
                salesorder = db.SalesOrders.Include("SalesOrderItems").Where(x => x.SalesOrderId == id).FirstOrDefault();
                salesorder.SalesOrderItems = db.GetSalesOrderItems(id);
                Dictionary<int, SKUMaster> idVsSKU = db.GetSKUMasterDictionary(salesorder.SalesOrderItems.Select(x => x.SKUMasterId).ToList());
                Dictionary<int, JobSheet> idVsJobSheet = db.GetJobSheetDictionary(salesorder.SalesOrderItems.Select(x => x.JobSheetId ?? 0).ToList());
                Dictionary<int, SaleInvoice> idVsSaleInvoice = db.GetSaleInvoiceDictionary(salesorder.SalesOrderItems.Select(x => x.SaleInvoiceId ?? 0).ToList());
                
                salesorder.EstimatedTotal = 0;
                foreach (var item in salesorder.SalesOrderItems)
                {
                    if(item.IsJobSheetCreated)
                    {
                        item.JobSheetNumber = db.JobSheets.Where(x => x.JobSheetId == item.JobSheetId).Select(x => x.ProductNumber).FirstOrDefault();
                    }
                    item.SKUMaster = idVsSKU[item.SKUMasterId];
                    if(item.JobSheetId.HasValue && idVsJobSheet.ContainsKey(item.JobSheetId.Value))
                    {
                        item.JobSheet = idVsJobSheet[item.JobSheetId.Value];
                    }
                    if(item.SaleInvoiceId.HasValue)
                    {
                        item.SaleInvoice = idVsSaleInvoice[item.SaleInvoiceId.Value];
                    }
                    salesorder.EstimatedTotal = salesorder.EstimatedTotal + item.EstimatedPrice;
                }
                salesorder.SaleInvoiceItemList = db.GetInvoiceItemsForOrder(id.Value);
                //foreach (var item in salesorder.SaleInvoiceItemList)
                //{
                //    salesorder.SubtotalNotMapped = salesorder.SubtotalNotMapped + item.BasicPrice.Value;
                //    salesorder.TotalTaxNotMapped = salesorder.TotalTaxNotMapped + item.ApplicableTaxValue;
                //    salesorder.TotalDiscountNotMapped = (salesorder.TotalDiscountNotMapped + item.Discount);
                //    salesorder.FinalTotalAdj = (salesorder.SubtotalNotMapped - salesorder.TotalDiscountNotMapped + salesorder.TotalTaxNotMapped);
                //}
                //salesorder.EstimatedTotal = salesorder.SubtotalNotMapped + salesorder.FinalTotalAdj;
                }
            else
            {
                salesorder = new SalesOrder();
                salesorder.OrderStatus = Enums.PurchaseOrderStatus.Open;
                salesorder.OrderDate = Convert.ToDateTime(DateTime.Now);
                GenerateAutoNo No = new GenerateAutoNo();
                salesorder.SaleOrderNumber = No.GenerateSalesOrderNo(db.CompanyId ?? 0);
                salesorder.BranchId = SessionManager.GetSessionBranch().BranchId;
                salesorder.DivisionId = SessionManager.GetSessionDivision().DivisionId;
                salesorder.CompanyId = db.CompanyId ?? 0;
            }
            salesorder.SalesOrderItem = new SalesOrderItem();
            salesorder.PurchaseOrderList = DropdownManager.GetPurchaseOrderList();
            salesorder.CustomerList = DropdownManager.GetLedgerListForType(null, Constants.LEDGER_TYPE_USER);
            salesorder.SalesAccountList = DropdownManager.GetLedgerListForType(null, Constants.LEDGER_TYPE_COMPANY);
            salesorder.DivisionList = DropdownManager.GetDivisionList(null);
            salesorder.BranchList = DropdownManager.GetBranchList(null);
            salesorder.LocationList = DropdownManager.GetLocationByBranch(salesorder.BranchId);
            salesorder.SKUList = DropdownManager.GetSKUList(0);
            SessionManager.EmptySessionList(Constants.SALE_ORDER_ITEM);
            salesorder = SaleCalculator.CalculateForOrder(salesorder);
            return View(salesorder);
        }

        [RBAC(AccessType = 1)]
        public ActionResult Edit(int id)
        {
            return RedirectToAction("Create", new { id = id });
        }

        [RBAC(AccessType = 3)]
        public ActionResult Delete(int id)
        {
            SalesOrder salesorder = db.SalesOrders.Include("SalesOrderItems").Where(x => x.SalesOrderId == id).FirstOrDefault();
            if(db.RemoveSalesOrder(salesorder))
            {
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index", new { ErrorMsg = MessageStore.DeleteError });
        }
        #endregion

        [HttpPost]
        public ActionResult Create(SalesOrder salesOrder, string btn)
        {
            db.SaveSalesOrder(salesOrder);
            List<SalesOrderItem> SaleOrderItemList = SessionManager.GetSalesOrderItem();
            foreach (var Item in SaleOrderItemList)
            {
                Item.SalesOrderId = salesOrder.SalesOrderId;
                Item.SalesOrderItemId = 0;
                db.SaveSalesOrderItem(Item);
            }
            List<SaleInvoiceItem> SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_SALE_ORDER);
            if (SaleInvoiceItemList != null)
            {
                foreach (var items in SaleInvoiceItemList)
                {
                    items.Product = null;
                    items.GST = null;
                    items.DiscountCodeList = null;
                    items.GSTList = null;
                    items.Unit = null;
                    items.SKUMaster = null;
                    items.Status = Constants.SALE_ITEM_SAVE_FOR_ORDER;
                    items.SalesOrderId = salesOrder.SalesOrderId;
                    db.SaleInvoiceItems.Add(items);
                    db.SaveChanges();
                }
            }
            SessionManager.EmptySessionList(Constants.SALE_ORDER_ITEM);
            SessionManager.EmptySessionList(Constants.SALE_TYPE_SALE_ORDER);

            if (btn == "Save & Create JobSheet")
            {
                return RedirectToAction("Create", new { id = salesOrder.SalesOrderId });
            }
            if (btn == "Save and Print Order")
            {
                return RedirectToAction("OrderPrint", new { id = salesOrder.SalesOrderId });
            }
            if (btn == "Save and Make Payment")
            {
                return RedirectToAction("MakePayment", new { id = salesOrder.SalesOrderId });
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult _ItemList(SalesOrderItem model)
        {
            SalesOrder salesorder = new SalesOrder();
            if(model.SalesOrderId > 0)
            {
                db.SaveSaleOrderItem(model);
                salesorder.SalesOrderItems = db.GetSalesOrderItems(model.SalesOrderId);
            }
            else
            {
                SessionManager.AddSaleOrderItem(model);
                salesorder.SalesOrderItems = SessionManager.GetSalesOrderItem();
            }
            Dictionary<int, JobSheet> idVsJobSheet = db.GetJobSheetDictionary(salesorder.SalesOrderItems.Select(x => x.JobSheetId ?? 0).ToList());
            Dictionary<int, SKUMaster> idVsSKU = db.GetSKUMasterDictionary(salesorder.SalesOrderItems.Select(x => x.SKUMasterId).ToList());
            foreach (var item in salesorder.SalesOrderItems)
            {
                if (item.IsJobSheetCreated)
                {
                    item.JobSheetNumber = db.JobSheets.Where(x => x.JobSheetId == item.JobSheetId).Select(x => x.ProductNumber).FirstOrDefault();
                }
                if (item.JobSheetId.HasValue)
                {
                    item.JobSheet = idVsJobSheet[item.JobSheetId.Value];
                }
                item.SKUMaster = idVsSKU[item.SKUMasterId];
                salesorder.EstimatedTotal = (salesorder.EstimatedTotal ?? 0) + item.EstimatedPrice;
            }
            return PartialView(salesorder);
        }

        public ActionResult RemoveOrderItem(int SalesOrderItemId, int SalesOrderId)
        {
            SalesOrder salesorder = new SalesOrder();
            if (SalesOrderId > 0)
            {
                SalesOrderItem model = db.SalesOrderItems.Where(x => x.SalesOrderItemId == SalesOrderItemId).FirstOrDefault();
                db.RemoveSalesOrderItem(model);
                salesorder.SalesOrderItems = db.GetSalesOrderItems(SalesOrderId);
            }
            else
            {
                SessionManager.RemoveSalesOrderItem(SalesOrderItemId);
                salesorder.SalesOrderItems = SessionManager.GetSalesOrderItem();
            }
            Dictionary<int, JobSheet> idVsJobSheet = db.GetJobSheetDictionary(salesorder.SalesOrderItems.Select(x => x.JobSheetId ?? 0).ToList());
            Dictionary<int, SKUMaster> idVsSKU = db.GetSKUMasterDictionary(salesorder.SalesOrderItems.Select(x => x.SKUMasterId).ToList());
            foreach (var item in salesorder.SalesOrderItems)
            {
                if (item.IsJobSheetCreated)
                {
                    item.JobSheetNumber = db.JobSheets.Where(x => x.JobSheetId == item.JobSheetId).Select(x => x.ProductNumber).FirstOrDefault();
                }
                if (item.JobSheetId.HasValue)
                {
                    item.JobSheet = idVsJobSheet[item.JobSheetId.Value];
                }
                item.SKUMaster = idVsSKU[item.SKUMasterId];
                salesorder.EstimatedTotal = (salesorder.EstimatedTotal ?? 0) + item.EstimatedPrice;
            }
            return PartialView("_ItemList", salesorder);
        }

        [HttpPost]
        public ActionResult _InvoiceItemList(string ProdId, int SalesOrderId)
        {
            string message = string.Empty;
            SalesOrder SalesOrder = new SalesOrder();
            //Query from appropriate channel depending upon SalesInvoiceid
            if (SalesOrderId > 0)
            {
                SalesOrder.SaleInvoiceItemList = db.GetInvoiceItemsForOrder(SalesOrderId);
            }
            else
            {
                SalesOrder.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_SALE_ORDER);

            }
            
          //Check if product is already available in the current invoice
          //Check if product is not sold
            Product product = db.GetProductByNumber(ProdId);
            if (product == null)
            {
                ViewBag.ErrorMsg = "The product is unavailable or already sold";
            }
            else
            {
                bool isAdded = SalesOrder.SaleInvoiceItemList.Any(x => x.ProductId == product.ProductId);
                if (isAdded)
                {
                    ViewBag.ErrorMsg = "This product is already added in this invoice";
                }
                else
                {
                    //Add item to invoice
                    if (SalesOrderId > 0)
                    {
                        SalesOrder.SaleInvoiceItemList.Add(ProductManager.AddInvoiceItemForOrder(product, SalesOrderId));
                    }
                    else
                    {
                        ProductManager.AddInvoiceItemForOrder(product, SalesOrderId);
                        SalesOrder.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_SALE_ORDER);
                    }
                }
            }
            SalesOrder = SaleCalculator.CalculateForOrder(SalesOrder);
            return PartialView(SalesOrder);
        }

        public ActionResult PurchaseOrderItem(int PurchaseOrderId)
        {
            SessionManager.EmptySessionList(Constants.SALE_ORDER_ITEM);
            List<PurchaseOrderItem> ItemList = db.PurchaseOrderItems.Where(x => x.PurchaseOrderId == PurchaseOrderId).ToList();
            foreach (var item in ItemList)
            {
                SalesOrderItem model = new SalesOrderItem();
                model.Quantity = item.Quantity;
                model.SKUMasterId = item.SKUMasterId;          
                SessionManager.AddSaleOrderItem(model);
            }
            SalesOrder salesorder = new SalesOrder();
            salesorder.SalesOrderItems = SessionManager.GetSalesOrderItem();
            Dictionary<int, SKUMaster> idVsSKU = db.GetSKUMasterDictionary(salesorder.SalesOrderItems.Select(x => x.SKUMasterId).ToList());
            foreach (var item in salesorder.SalesOrderItems)
            {
                item.SKUMaster = idVsSKU[item.SKUMasterId];
            }
            return PartialView("_ItemList", salesorder);
        }

        [HttpPost]
        public JsonResult EditItem(int SalesOrderItemId, int SalesOrderId)
        {
            SalesOrderItem model = null;
            if (SalesOrderId > 0)
            {
                model = db.SalesOrderItems.Where(x => x.SalesOrderItemId == SalesOrderItemId).FirstOrDefault();
            }
            else
            {
                List<SalesOrderItem> SalesOrderList = SessionManager.GetSalesOrderItem();
                if (SalesOrderList != null)
                {
                    model = SalesOrderList.Where(x => x.SalesOrderItemId == SalesOrderItemId).FirstOrDefault();
                }
            }
            model.SKU = db.SKUMasters.Where(x => x.SKUMasterId == model.SKUMasterId).Select(x => x.SKU).FirstOrDefault();
            return Json(model);
        }

        public ActionResult OrderPrint(int id)
        {
            SalesOrder salesorder = db.SalesOrders.Include("SalesOrderItems").Where(x => x.SalesOrderId == id).FirstOrDefault();
            if(salesorder != null)
            {
                salesorder.SalesOrderItems = db.GetSalesOrderItems(id);
                salesorder.Company = SessionManager.GetSessionCompany();
                salesorder.Branch = SessionManager.GetSessionBranch();
                Dictionary<int, SKUMaster> idVsSKU = db.GetSKUMasterDictionary(salesorder.SalesOrderItems.Select(x => x.SKUMasterId).ToList());
                Dictionary<int, JobSheet> idVsJobSheet = db.GetJobSheetDictionary(salesorder.SalesOrderItems.Select(x => x.JobSheetId ?? 0).ToList());
                salesorder.Customer = db.Ledgers.Where(x => x.LedgerId == salesorder.CustomerId).FirstOrDefault();
                if(salesorder.Customer != null)
                {
                    salesorder.Contact = salesorder.Customer.Contact;
                    if(salesorder.Contact == null)
                    {
                        salesorder.Contact = new Contact();
                    }
                }
                else
                {
                    salesorder.Contact = new Contact();
                }
                //salesorder.EstimatedTotal = 0;
                //foreach (var item in salesorder.SalesOrderItems)
                //{
                //    if (item.IsJobSheetCreated)
                //    {
                //        item.JobSheetNumber = db.JobSheets.Where(x => x.JobSheetId == item.JobSheetId).Select(x => x.ProductNumber).FirstOrDefault();
                //    }
                //    item.SKUMaster = idVsSKU[item.SKUMasterId];
                //    if (item.JobSheetId.HasValue)
                //    {
                //        item.JobSheet = idVsJobSheet[item.JobSheetId.Value];
                //    }
                //    salesorder.EstimatedTotal = (salesorder.EstimatedTotal ?? 0) + item.EstimatedPrice;
                //}
                salesorder.SaleInvoiceItemList = db.GetInvoiceItemsForOrder(id);
                //salesorder = SaleCalculator.CalculateForOrder(salesorder);
                //salesorder.EstimatedTotal = (salesorder.EstimatedTotal ?? 0) + salesorder.FinalTotalAdj;
                JobSheet jobsheet = null;
                foreach (var item in salesorder.SalesOrderItems)
                {
                    if(item.JobSheetId.HasValue)
                    {
                        jobsheet = db.JobSheets.Where(x => x.JobSheetId == item.JobSheetId.Value).FirstOrDefault();
                        break;
                    }
                }
               
                if (jobsheet != null)
                {
                    salesorder.MeasurmentType = jobsheet.MeasurmentType;
                    jobsheet.NewMeasurmentList = db.Measurments.ToList();
                    jobsheet.MeasurmentSet = db.MeasurmentSets.Where(x => x.JobSheetId == jobsheet.JobSheetId).FirstOrDefault();
                    if (jobsheet.MeasurmentType == Enums.MeasurementType.Custom.ToString())
                    {
                        salesorder.MeasurmentSetListForCustome = db.MeasurmentSets.Where(x => x.JobSheetId == jobsheet.JobSheetId && x.Type == Enums.MeasurementType.Custom).ToList();
                        foreach (var item1 in salesorder.MeasurmentSetListForCustome)
                        {
                            item1.MeasurementList = new List<MeasurementAccessModel>();
                            item1.Measurment = db.Measurments.Where(x => x.MeasurmentId == item1.MeasurmentId).FirstOrDefault();
                            List<string> ValueList = item1.MeasurmentValues.Split(',').ToList();
                            foreach (var value in ValueList)
                            {
                                MeasurementAccessModel AccessModel = new MeasurementAccessModel
                                {
                                    MeasurementType = value.Substring(0, value.IndexOf('=')),
                                    MeasurementValue = value.Substring(value.LastIndexOf('=') + 1)
                                };
                                item1.MeasurementList.Add(AccessModel);
                            }
                        }
                    }
                    else if (jobsheet.MeasurmentType == Enums.MeasurementType.Readymade.ToString())
                    {
                        salesorder.MeasurmentSetListForReadymade = db.MeasurmentSets.Where(x => x.JobSheetId == jobsheet.JobSheetId && x.Type == Enums.MeasurementType.Readymade).ToList();
                    }
                }
            }
            return View(salesorder);
        }

        public ActionResult MakePayment(int? id)
        {
            SalesOrder salesorder = db.SalesOrders.Include("SalesOrderItems").Where(x => x.SalesOrderId == id).FirstOrDefault();
            salesorder.PaymentTypeList = DropdownManager.GetPaymentTypeList(null);
            if(salesorder.PaymentDate == null)
            {
                salesorder.PaymentDate = DateTime.Now;
            }
            if (!salesorder.AmountPaid.HasValue)
            {
                salesorder.AmountPaid = 0;
            }
            salesorder.AmountRemaining = (salesorder.EstimatedTotal ?? 0) - (salesorder.AmountPaid ?? 0);
            return View(salesorder);
        }

        [HttpPost]
        public ActionResult MakePayment(SalesOrder modal, string btn)
        {
            SalesOrder salesOrder = db.SalesOrders.Where(x => x.SalesOrderId == modal.SalesOrderId).FirstOrDefault();
            if (salesOrder != null)
            {
                salesOrder.AmountRemaining = modal.AmountRemaining;
                salesOrder.AmountPaid = modal.AmountPaid;
                salesOrder.EstimatedTotal = modal.EstimatedTotal;
                salesOrder.PaymentDate = modal.PaymentDate;
                salesOrder.PaymentTypeId = modal.PaymentTypeId;
                salesOrder.PaymentType = db.PaymentTypes.Where(x => x.PaymentTypeId == modal.PaymentTypeId).Select(x => x.PaymentName).FirstOrDefault();
                db.SaveSalesOrder(salesOrder);
            }

            List<Voucher> VoucherList = db.Vouchers.Where(x => x.SalesOrderId == salesOrder.SalesOrderId).ToList();
            foreach (var item in VoucherList)
            {
                db.Vouchers.Remove(item);
            }
            db.SaveChanges();
            if (salesOrder.CustomerId != 61)
            {
                #region FromVoucher
                Voucher FromVoucher = new Voucher();
                FromVoucher.LedgerId = salesOrder.CustomerId;
                FromVoucher.SalesOrderId = salesOrder.SalesOrderId;
                FromVoucher.Total = modal.AmountPaid ?? 0;
                FromVoucher.Type = Constants.CR;
                FromVoucher.EntryDate = modal.PaymentDate ?? DateTime.Now;
                FromVoucher.Checknumber = modal.Checknumber;
                FromVoucher.VoucherFor = Constants.SALE_TYPE_SALE_ORDER;
                FromVoucher.InvoiceNumber = salesOrder.SaleOrderNumber;
                FromVoucher.Particulars = Constants.SALE_TYPE_SALE_ORDER + " : " + salesOrder.SaleOrderNumber;
                db.SaveVauchers(FromVoucher);
                #endregion
            }


            #region ToVoucher
            Ledger LedgerForPaymentType = db.Ledgers.Where(x => x.LedgerId == db.PaymentTypes.Where(y => y.PaymentTypeId == modal.PaymentTypeId).Select(y => y.LedgerId).FirstOrDefault()).FirstOrDefault();
            if (LedgerForPaymentType != null)
            {
                Voucher ToVoucher = new Voucher();
                ToVoucher.SalesOrderId = salesOrder.SalesOrderId;
                ToVoucher.LedgerId = LedgerForPaymentType.LedgerId;
                ToVoucher.Total = modal.AmountPaid ?? 0;
                ToVoucher.Type = Constants.DR;
                ToVoucher.EntryDate = modal.PaymentDate ?? DateTime.Now;
                ToVoucher.Checknumber = modal.Checknumber;
                ToVoucher.VoucherFor = Constants.SALE_TYPE_SALE_ORDER;
                ToVoucher.InvoiceNumber = salesOrder.SaleOrderNumber;
                ToVoucher.Particulars = Constants.SALE_TYPE_SALE_ORDER + " : " + salesOrder.SaleOrderNumber;
                db.SaveVauchers(ToVoucher);
            }
            #endregion

            if (modal.PaymentTypeId == 5)
            {
                PaymentGateWay model = new PaymentGateWay();
                PaymentGateWayViewModel ViewModel = new PaymentGateWayViewModel();
                Contact Contact = db.Ledgers.Where(x => x.LedgerId == salesOrder.CustomerId).Select(x => x.Contact).FirstOrDefault();
                if(Contact != null)
                {
                    ViewModel.Email = Contact.Email;
                    ViewModel.Name = Contact.FirstName;
                    ViewModel.Phone = Contact.Phone;
                }
                ViewModel.Amount = (modal.AmountPaid ?? 0).ToString();
                ViewModel.OrderId = salesOrder.SalesOrderId.ToString();
                ViewModel.OnSuccess = Constants.OnSuccessOfSalesOrder;
                ViewModel.OnFailure = Constants.OnFailure;
                model.GotoPayment(ViewModel, btn);
            }


            switch (btn)
            {
                case "Make Payment and Print Order":
                    {
                        return RedirectToAction("OrderPrint", new { id = modal.SalesOrderId });
                    }
                case "Make Payment":
                    {
                        return RedirectToAction("Index");
                    }
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult UpdateProduct(SaleInvoiceItem model)
        {
            SalesOrder SalesOrder = new SalesOrder();
            if (model.SaleInvoiceItemId > 0)
            {
                SalesInvoiceHandler handler = new SalesInvoiceHandler();
                int SaleInvoiceId = handler.UpDateSaleInvoiceItem(model);
                SalesOrder.SaleInvoiceItemList = db.GetInvoiceItemsForOrder(model.SalesOrderId ?? 0);
            }
            else
            {
                SessionManager.UpdateOrderItemForOrder(model);
                SalesOrder.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_SALE_ORDER);
            }

            if (SalesOrder.SaleInvoiceItemList != null)
            {
                SalesOrder = SaleCalculator.CalculateForOrder(SalesOrder);
            }
            return PartialView("_InvoiceItemList", SalesOrder);
        }

        [HttpPost]
        public ActionResult RemoveItem(int ProductId, int? SalesOrderId)
        {
            SalesInvoiceHandler handler = new SalesInvoiceHandler();
            SalesOrder SalesOrder = new SalesOrder();
            if (SalesOrderId == null || SalesOrderId == 0)
            {
                SessionManager.RemoveSaleInvoiceItemForOrder(ProductId);
                SalesOrder.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_SALE_ORDER);
            }
            else
            {
                handler.RemoveSalesInvoiceItem(null, null, SalesOrderId, ProductId);
                SalesOrder.SaleInvoiceItemList = db.GetInvoiceItemsForOrder(SalesOrderId ?? 0);
            }

            if (SalesOrder.SaleInvoiceItemList != null)
            {
                SalesOrder = SaleCalculator.CalculateForOrder(SalesOrder);
            }
            return PartialView("_InvoiceItemList", SalesOrder);
        }

        [HttpPost]
        public ActionResult Return(FormCollection form)
        {
            PaymentGateWay model = new PaymentGateWay();
            try
            {
                string[] merc_hash_vars_seq;
                string merc_hash_string = string.Empty;
                string merc_hash = string.Empty;
                string SaleInvoiceId = string.Empty;
                string mode = string.Empty;
                string payuMoneyId = string.Empty;
                string txnid = string.Empty;
                string udf1 = string.Empty;
                string hash_seq = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
                string Status = form["status"].ToString();
                if (Status == "success")
                {

                    merc_hash_vars_seq = hash_seq.Split('|');
                    Array.Reverse(merc_hash_vars_seq);
                    merc_hash_string = ConfigurationManager.AppSettings["SALT"] + "|" + form["status"].ToString();
                    foreach (string merc_hash_var in merc_hash_vars_seq)
                    {
                        merc_hash_string += "|";
                        merc_hash_string = merc_hash_string + (form[merc_hash_var] ?? "");
                    }
                    Response.Write(merc_hash_string);
                    merc_hash = model.Generatehash512(merc_hash_string).ToLower();
                    if (merc_hash != form["hash"])
                    {
                        Response.Write("Hash value did not matched");
                    }
                    else
                    {
                        mode = Request.Form["mode"];
                        payuMoneyId = Request.Form["payuMoneyId"];
                        txnid = Request.Form["txnid"];
                        SaleInvoiceId = Request.Form["udf1"];
                        string btn = Request.Form["udf2"];
                        SalesOrder salesOrder = db.SalesOrders.Where(x => x.SalesOrderId == Convert.ToInt32(SaleInvoiceId)).FirstOrDefault();
                        
                        switch (btn)
                        {
                            case "Make Payment and Print Order":
                                {
                                    return RedirectToAction("OrderPrint", new { id = SaleInvoiceId });
                                }
                            case "Make Payment":
                                {
                                    return RedirectToAction("Index");
                                }
                        }
                    }

                }
                else
                {
                    SaleInvoiceId = Request.Form["udf1"];
                    return RedirectToAction("Failure", "Checkout", new { id = SaleInvoiceId });
                }
            }

            catch (Exception ex)
            {
                Response.Write("<span style='color:red'>" + ex.Message + "</span>");

            }
            return null;
        }
    }
}