﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DataLayer;
using DataLayer.Utils;
using DataLayer.Models;
using DataLayer.ViewModels;
using ERP_System.Utils;
using System;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class ProductionController : BaseController
    {
        DbConnector db = new DbConnector();
        ProductionHandler handler = new ProductionHandler();
        ProductionDbUtil PrdDb = new ProductionDbUtil();

        // GET: Production
        [RBAC(AccessType = 2)]
        public ActionResult Index(int? PageNo)
        {
            ProductionViewModel model = new ProductionViewModel
            {
                JobSheetLists = handler.GetJobSheetList(PageNo)
            };
            Dictionary<int, SalesOrder> idVsSalesOrder = db.GetSaleOrderDictionary(model.JobSheetLists.Select(x => x.SaleOrderId ?? 0).ToList());
            foreach (var item in model.JobSheetLists)
            {
                if(item.SaleOrderId.HasValue && item.SaleOrderId != -1 && idVsSalesOrder.ContainsKey(item.SaleOrderId.Value))
                {
                    item.SalesOrder = idVsSalesOrder[item.SaleOrderId.Value];
                }
                JobSheetService service = db.JobSheetServices.Where(x => x.JobSheetId == item.JobSheetId).FirstOrDefault();
                if(service != null)
                {
                    item.AssignedWorker = db.Ledgers.Where(x => x.LedgerId == service.LedgerId).FirstOrDefault();
                }
                item.Customer = db.Ledgers.Where(x => x.LedgerId == item.CustomerId).FirstOrDefault();
            }
            return View(model);
        }

        [RBAC(AccessType = 0)]
        public ActionResult Create(Enums.JobSheetStatus? Status, int? id,int? SalesOrderItemId)
        {
            JobSheet production = handler.GetProductionForCreateEdit(id, Status, SalesOrderItemId);
            return View(production);
        }

        [HttpPost]
        public ActionResult Create(JobSheet model, string btn)
        {
            model = handler.SaveJobSheet(model);
            if (btn == "Save and Create Product")
            {
                return RedirectToAction("CreateProduct", new { JobSheetId = model.JobSheetId });
            }
            else if (btn == "Save and Print")
            {
                return RedirectToAction("Measurement", new { JobSheetId = model.JobSheetId });
            }
            else if (model.SalesOrderItemId.HasValue)
            {
                return RedirectToAction("Create", "SalesOrder", new { id = model.SaleOrderId });
            }
            return RedirectToAction("Index");
        }

        public ActionResult SelectProduction(JobSheet model)
        {
            return RedirectToAction("FutherProcess", new { ParentJobSheetId = model.JobSheetId });
        }

        public ActionResult FutherProcess(int? JobSheetId,int? ParentJobSheetId)
        {
            JobSheet production = handler.GetProductionForFutherProcess(JobSheetId, ParentJobSheetId);
            return View(production);
        }

        [HttpPost]
        public ActionResult SaveFutherProcess(JobSheet model, string btn)
        {
            model = handler.SaveFutherProcessJobSheet(model);
            return RedirectToAction("Create", new { id = model.ParentJobSheetId });
        }

        [RBAC(AccessType = 3)]
        public ActionResult DeleteJobSheet(int JobSheetId)
        {
            JobSheet jobsheet = db.JobSheets.Include("JobSheetServicesList").Include("JobSheetItemList").Where(x => x.JobSheetId == JobSheetId).FirstOrDefault();
            if (jobsheet == null)
            {
                return HttpNotFound();
            }
            else
            {
                jobsheet.FutherProcessJobSheetList = db.JobSheets.Where(x => x.ParentJobSheetId == JobSheetId).ToList();
                if(jobsheet.FutherProcessJobSheetList != null && jobsheet.FutherProcessJobSheetList.Count > 0)
                {
                    foreach(var item in jobsheet.FutherProcessJobSheetList)
                    {
                        item.JobSheetServicesList = db.JobSheetServices.Where(x => x.JobSheetId == item.JobSheetId).ToList();
                        item.JobSheetItemList = db.JobSheetItems.Where(x => x.JobSheetId == item.JobSheetId).ToList();
                        handler.DeleteJobSheet(item);
                    }
                }
                if (handler.DeleteJobSheet(jobsheet))
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index", new { ErrorMsg = MessageStore.CantDeleteProductSold });
        }



        [HttpPost]
        public JsonResult GetPurchaseOrder(int id)
        {
            List<SalesOrder> OrderList = db.SalesOrders.Where(x => x.CustomerId == id).ToList();
            List<SelectListItem> itemList = new List<SelectListItem>();
            foreach (var item in OrderList)
            {
                itemList.Add(new SelectListItem() { Text = item.SaleOrderNumber, Value = item.SalesOrderId.ToString() });
            }
            return Json(new SelectList(itemList, "Value", "Text"));
        }

        public ActionResult Measurement(int JobSheetId)
        {
            PrintJobSheetViewModal modal = new PrintJobSheetViewModal();
            modal.JobSheetList = new List<JobSheet>();
            modal.JobSheetId = JobSheetId;
            Dictionary<int, Ledger> ServicesVsLedger = db.GetLedgerDictionary(db.JobSheetServices.Where(x => x.JobSheetId == JobSheetId).Select(x => x.LedgerId).ToList());
            foreach (var item in ServicesVsLedger)
            {
                modal.PrintOptions.Add(new SelectListItem() { Text = item.Value.Name , Value = item.Key.ToString() });
            }
            return View(modal);
        }

        [HttpPost]
        public ActionResult _PrintJobSheet(int Option,int JobSheetId)
        {
            PrintJobSheetViewModal modal = new PrintJobSheetViewModal();
            modal.JobSheetList = new List<JobSheet>();
            JobSheet jobsheet = db.JobSheets.Where(x => x.JobSheetId == JobSheetId).FirstOrDefault();
            List<string> templist = db.JobSheetItems.Where(x => x.JobSheetId == JobSheetId).Select(x => x.Product.SerialNumber).ToList();
            jobsheet.JobSheetItemList = db.JobSheetItems.Include("Product").Where(x => x.JobSheetId == JobSheetId).ToList();
            foreach (var item in jobsheet.JobSheetItemList)
            {
                if (item.Product.AttributeLinkingId.HasValue)
                {
                    AttributeLinking attributeLinking = db.AttributeLinkings.Where(x => x.AttributeLinkingId == item.Product.AttributeLinkingId).FirstOrDefault();
                    if (attributeLinking != null)
                    {
                        item.Attributes = attributeLinking.Value.Split(',').ToList();
                    }
                }
            }
            jobsheet.BarcodeList = string.Join(",", templist);
            jobsheet.SKUMaster = db.SKUMasters.Where(x => x.SKUMasterId == jobsheet.SKUMasterId).FirstOrDefault();
            jobsheet.NewMeasurmentList = db.Measurments.ToList();
            if (jobsheet.MeasurmentType == Enums.MeasurementType.Custom.ToString())
            {
                jobsheet.MeasurmentSetListForCustome = db.MeasurmentSets.Where(x => x.JobSheetId == JobSheetId && x.Type == Enums.MeasurementType.Custom).ToList();
                foreach (var item1 in jobsheet.MeasurmentSetListForCustome)
                {
                    item1.MeasurementList = new List<MeasurementAccessModel>();
                    item1.Measurment = db.Measurments.Where(x => x.MeasurmentId == item1.MeasurmentId).FirstOrDefault();
                    List<string> ValueList = item1.MeasurmentValues.Split(',').ToList();
                    foreach (var value in ValueList)
                    {
                        MeasurementAccessModel AccessModel = new MeasurementAccessModel
                        {
                            MeasurementType = value.Substring(0, value.IndexOf('=')),
                            MeasurementValue = value.Substring(value.LastIndexOf('=') + 1)
                        };
                        item1.MeasurementList.Add(AccessModel);
                    }
                }
            }
            else if (jobsheet.MeasurmentType == Enums.MeasurementType.Readymade.ToString())
            {
                jobsheet.MeasurmentSetListForReadymade = db.MeasurmentSets.Where(x => x.JobSheetId == JobSheetId && x.Type == Enums.MeasurementType.Readymade).ToList();
            }
            jobsheet.OrderNumber = db.SalesOrders.Where(x => x.SalesOrderId == jobsheet.SaleOrderId).Select(x => x.SaleOrderNumber).FirstOrDefault();

            if (Option == -2)
            {                                                             
                List<JobSheetService> JobSheetServiceList = db.JobSheetServices.Where(x => x.JobSheetId == JobSheetId).ToList();
                if (JobSheetServiceList != null && JobSheetServiceList.Count > 0)
                {
                    Dictionary<int, Ledger> idVsLedger = db.GetLedgerDictionary(JobSheetServiceList.Select(x => x.LedgerId).ToList());
                    foreach (var item in JobSheetServiceList)
                    {
                        JobSheet Newjobsheet = new JobSheet();
                        Newjobsheet.DeliveryDate = jobsheet.DeliveryDate;
                        Newjobsheet.ProductNumber = jobsheet.ProductNumber;
                        Newjobsheet.JobDate = jobsheet.JobDate;
                        Newjobsheet.Quantity = jobsheet.Quantity;
                        Newjobsheet.Description = jobsheet.Description;
                        Newjobsheet.MeasurmentSetListForReadymade = jobsheet.MeasurmentSetListForReadymade;
                        Newjobsheet.MeasurmentSetListForCustome = jobsheet.MeasurmentSetListForCustome;
                        Newjobsheet.NewMeasurmentList = jobsheet.NewMeasurmentList;
                        Newjobsheet.BarcodeList = jobsheet.BarcodeList;
                        Newjobsheet.JobSheetItemList = jobsheet.JobSheetItemList;
                        Newjobsheet.OrderNumber = jobsheet.OrderNumber;
                        Contact contact = idVsLedger[item.LedgerId].Contact;
                        Newjobsheet.Name = idVsLedger[item.LedgerId].Name;
                        if (contact != null)
                        {
                            Newjobsheet.ConatctNumber = contact.Mobile;
                            Newjobsheet.Address = contact.AddressLine1 + " " + contact.AddressLine2;
                        }
                        Newjobsheet.SKUMaster = jobsheet.SKUMaster;
                        Newjobsheet.IsTailor = true;
                        Newjobsheet.IsCustomer = false;

                        modal.JobSheetList.Add(Newjobsheet);
                    }
                }

                if (jobsheet.CustomerId > -1)
                {
                    jobsheet.IsCustomer = true;
                    Contact contact = db.Ledgers.Where(x => x.LedgerId == jobsheet.CustomerId).Select(x => x.Contact).FirstOrDefault();
                    jobsheet.Name = db.Ledgers.Where(x => x.LedgerId == jobsheet.CustomerId).Select(x => x.Name).FirstOrDefault();
                    if (contact != null)
                    {
                        jobsheet.ConatctNumber = contact.Mobile;
                        jobsheet.Address = contact.AddressLine1 + " " + contact.AddressLine2 + "," + contact.City;
                    }
                    

                    jobsheet.NewMeasurmentList = db.Measurments.ToList();
                    jobsheet.MeasurmentSet = db.MeasurmentSets.Where(x => x.JobSheetId == JobSheetId).FirstOrDefault();
                    if (jobsheet.MeasurmentType == Enums.MeasurementType.Custom.ToString())
                    {
                        jobsheet.MeasurmentSetListForCustome = db.MeasurmentSets.Where(x => x.JobSheetId == JobSheetId && x.Type == Enums.MeasurementType.Custom).ToList();
                        foreach (var item1 in jobsheet.MeasurmentSetListForCustome)
                        {
                            item1.MeasurementList = new List<MeasurementAccessModel>();
                            item1.Measurment = db.Measurments.Where(x => x.MeasurmentId == item1.MeasurmentId).FirstOrDefault();
                            List<string> ValueList = item1.MeasurmentValues.Split(',').ToList();
                            foreach (var value in ValueList)
                            {
                                MeasurementAccessModel AccessModel = new MeasurementAccessModel
                                {
                                    MeasurementType = value.Substring(0, value.IndexOf('=')),
                                    MeasurementValue = value.Substring(value.LastIndexOf('=') + 1)
                                };
                                item1.MeasurementList.Add(AccessModel);
                            }
                        }
                    }
                    else if (jobsheet.MeasurmentType == Enums.MeasurementType.Readymade.ToString())
                    {
                        jobsheet.MeasurmentSetListForReadymade = db.MeasurmentSets.Where(x => x.JobSheetId == JobSheetId && x.Type == Enums.MeasurementType.Readymade).ToList();
                    }
                    modal.JobSheetList.Add(jobsheet);
                }
            }
            else if(Option == -1)
            {
                if (jobsheet.CustomerId > -1)
                {
                    jobsheet.IsCustomer = true;
                    Contact contact = db.Ledgers.Where(x => x.LedgerId == jobsheet.CustomerId).Select(x => x.Contact).FirstOrDefault();
                    jobsheet.Name = db.Ledgers.Where(x => x.LedgerId == jobsheet.CustomerId).Select(x => x.Name).FirstOrDefault();
                    if (contact != null)
                    {
                        jobsheet.ConatctNumber = contact.Mobile;
                        jobsheet.Address = contact.AddressLine1 + " " + contact.AddressLine2 + "," + contact.City;
                    }
                    jobsheet.OrderNumber = db.SalesOrders.Where(x => x.SalesOrderId == jobsheet.SaleOrderId).Select(x => x.SaleOrderNumber).FirstOrDefault();

                    jobsheet.NewMeasurmentList = db.Measurments.ToList();
                    jobsheet.MeasurmentSet = db.MeasurmentSets.Where(x => x.JobSheetId == JobSheetId).FirstOrDefault();
                    if (jobsheet.MeasurmentType == Enums.MeasurementType.Custom.ToString())
                    {
                        jobsheet.MeasurmentSetListForCustome = db.MeasurmentSets.Where(x => x.JobSheetId == JobSheetId && x.Type == Enums.MeasurementType.Custom).ToList();
                        foreach (var item1 in jobsheet.MeasurmentSetListForCustome)
                        {
                            item1.MeasurementList = new List<MeasurementAccessModel>();
                            item1.Measurment = db.Measurments.Where(x => x.MeasurmentId == item1.MeasurmentId).FirstOrDefault();
                            List<string> ValueList = item1.MeasurmentValues.Split(',').ToList();
                            foreach (var value in ValueList)
                            {
                                MeasurementAccessModel AccessModel = new MeasurementAccessModel
                                {
                                    MeasurementType = value.Substring(0, value.IndexOf('=')),
                                    MeasurementValue = value.Substring(value.LastIndexOf('=') + 1)
                                };
                                item1.MeasurementList.Add(AccessModel);
                            }
                        }
                    }
                    else if (jobsheet.MeasurmentType == Enums.MeasurementType.Readymade.ToString())
                    {
                        jobsheet.MeasurmentSetListForReadymade = db.MeasurmentSets.Where(x => x.JobSheetId == JobSheetId && x.Type == Enums.MeasurementType.Readymade).ToList();
                    }
                    modal.JobSheetList.Add(jobsheet);
                }
            }
            else
            {
                List<JobSheetService> JobSheetServiceList = db.JobSheetServices.Where(x => x.JobSheetId == JobSheetId && x.LedgerId == Option).ToList();
                if (JobSheetServiceList != null && JobSheetServiceList.Count > 0)
                {
                    Dictionary<int, Ledger> idVsLedger = db.GetLedgerDictionary(JobSheetServiceList.Select(x => x.LedgerId).ToList());
                    foreach (var item in JobSheetServiceList)
                    {
                        JobSheet Newjobsheet = new JobSheet();
                        Newjobsheet.DeliveryDate = jobsheet.DeliveryDate;
                        Newjobsheet.ProductNumber = jobsheet.ProductNumber;
                        Newjobsheet.JobDate = jobsheet.JobDate;
                        Newjobsheet.Quantity = jobsheet.Quantity;
                        Newjobsheet.Description = jobsheet.Description;
                        Newjobsheet.MeasurmentSetListForReadymade = jobsheet.MeasurmentSetListForReadymade;
                        Newjobsheet.MeasurmentSetListForCustome = jobsheet.MeasurmentSetListForCustome;
                        Newjobsheet.NewMeasurmentList = jobsheet.NewMeasurmentList;
                        Newjobsheet.BarcodeList = jobsheet.BarcodeList;
                        Newjobsheet.JobSheetItemList = jobsheet.JobSheetItemList;
                        Newjobsheet.OrderNumber = jobsheet.OrderNumber;
                        Contact contact = idVsLedger[item.LedgerId].Contact;
                        Newjobsheet.Name = idVsLedger[item.LedgerId].Name;
                        if (contact != null)
                        {
                            Newjobsheet.ConatctNumber = contact.Mobile;
                            Newjobsheet.Address = contact.AddressLine1 + " " + contact.AddressLine2;
                        }
                        Newjobsheet.SKUMaster = jobsheet.SKUMaster;
                        Newjobsheet.IsTailor = true;
                        Newjobsheet.IsCustomer = false;

                        modal.JobSheetList.Add(Newjobsheet);
                    }
                }
            }            
            return PartialView(modal);
        }



        #region Alteration
            public ActionResult SelectAlteration(JobSheet model)
        {
            return RedirectToAction("Alteration", new { SaleInvoiceId = model.SaleInvoiceId, SaleOrderId = model.SaleOrderId });
        }

        [HttpPost]
        public ActionResult SaveAlteration(JobSheet model)
        {
            model = handler.SaveAlteration(model);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult RemoveSaleItem(int ProductId, int? JobSheetId)
        {
            JobSheet jobSheet = new JobSheet();
            if (JobSheetId == null || JobSheetId == 0)
            {
                SessionManager.RemoveItemFromList(ProductId,Constants.SALE_TYPE_SALE_INVOICE);
                jobSheet.SaleInvoiceItemList = SessionManager.GetInvoiceItemList(Constants.SALE_TYPE_SALE_INVOICE);
            }
            else
            {
                handler.RemoveSalesInvoiceItem(JobSheetId, ProductId);
                jobSheet.SaleInvoiceItemList = db.SaleInvoiceItems.Where(x => x.JobSheetId == JobSheetId).ToList();
            }

            Dictionary<int, SKUMaster> idVsSKU = db.GetSKUMasterDictionary(jobSheet.SaleInvoiceItemList.Select(x => x.SKUMasterId ?? 0).ToList());
            Dictionary<int, GST> idVsGST = db.GetGSTMasterDictionary(jobSheet.SaleInvoiceItemList.Select(x => x.GSTId).ToList());
            foreach (var Item in jobSheet.SaleInvoiceItemList)
            {
                Item.SKUMaster = idVsSKU[Item.SKUMasterId ?? 0];
                Item.GST = idVsGST[Item.GSTId];
            }

            return PartialView("_SaleInvoiceItem", jobSheet);
        }

        public ActionResult Alteration(int? JobSheetId, int? SaleInvoiceId, int? SaleOrderId)
        {
            SessionManager.EmptySessionList(Constants.SALE_TYPE_SALE_RETURN);
            JobSheet production = handler.GetProductionForAlteration(JobSheetId, SaleInvoiceId, SaleOrderId);
            return View(production);
        }
        #endregion

        #region Create Product
        public ActionResult DeleteProduct(PurchaseInvoiceItem Model, string btn)
        {
            if (Model.ProductList != null)
            {
                List<int> ProductIds = Model.ProductList.Where(x => x.checkProduct == true).Select(x => x.ProductId).ToList();
                List<Product> ProductList = db.Products.Where(x => ProductIds.Contains(x.ProductId)).ToList();
                if (btn == "Delete")
                {
                    foreach (var item in ProductList.ToList())
                    {
                        db.RemoveSingleProduct(item);
                    }
                    if (db.Products.Where(x => x.PurchaseInvoiceItemId == Model.PurchaseInvoiceItemId).Count() == 0)
                    {
                        db.Configuration.ProxyCreationEnabled = false;
                        JobSheet JobSheet = db.JobSheets.Where(x => x.JobSheetId == Model.JobsheetId).FirstOrDefault();
                        if (JobSheet != null)
                        {
                            JobSheet.IsProductCreated = false;
                            PrdDb.SaveJobSheet(JobSheet);
                        }

                        PurchaseInvoiceItem PurchaseInvoiceItem = db.PurchaseInvoiceItems.Where(x => x.PurchaseInvoiceItemId == Model.PurchaseInvoiceItemId).FirstOrDefault();
                        if (PurchaseInvoiceItem != null)
                        {
                            PurchaseInvoiceItem.IsProductCreated = false;
                            db.SavePurchaseInvoiceItem(PurchaseInvoiceItem);
                        }
                    }
                }
                else if (btn == "Add To Barcode Queue")
                {
                    foreach (var item in ProductList)
                    {
                        List<Product> ProductForBarcode = SessionManager.GetBarcodeQueueList();
                        bool isAdded = ProductForBarcode.Any(x => x.ProductId == item.ProductId);
                        if (!isAdded)
                        {
                            SessionManager.AddItemInBarcodeQueue(item);
                        }
                    }
                }
                else if (btn == "Print Barcode")
                {
                    List<Product> ProductForBarcode = new List<Product>();
                    foreach (var item in ProductList)
                    {
                        ProductForBarcode.Add(item);
                    }
                    TempData["List"] = ProductForBarcode;
                    return RedirectToAction("PrintBarcode", "Product");
                }
            }
            return RedirectToAction("Index");
        }

        public ActionResult Details(int JobSheetId)
        {
            PurchaseInvoiceItem item = db.PurchaseInvoiceItems.Where(x => x.JobsheetId == JobSheetId).FirstOrDefault(); 
            item.ProductList = db.GetProductsListByInvoiceId(item.PurchaseInvoiceItemId);
            return View(item);
        }

        public ActionResult EditProduct(int? id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            Product Product = db.GetProductById(id.Value);
            Product.CategoryList = DropdownManager.GetCategoryList((id ?? 0));
            Product.SubCategoryList = DropdownManager.GetSubCategoryList((id ?? 0));
            Product.UnitList = DropdownManager.GetFreezedUnitList((Product.UnitId));
            Product.HSNList = DropdownManager.GetHSNList((id ?? 0));
            Product.LocationList = DropdownManager.GetLocationByBranch(SessionManager.GetSessionBranch().BranchId);
            Product.BrandList = DropdownManager.GetBrandList((id ?? 0));
            Product.GSTList = DropdownManager.GetGSTList((id ?? 0));
            Product.SKUList = DropdownManager.GetSKUList((id ?? 0));
            Product.DiscountCodeList = DropdownManager.GetDiscountsAccessibleList(Product.DiscountsAccessible,null);
            return View(Product);
        }

        public ActionResult CreateProduct(int JobsheetId)
        {
            JobSheet JobSheet = db.JobSheets.Where(x => x.JobSheetId == JobsheetId).FirstOrDefault();
            PurchaseInvoiceItem PurchaseInvoiceItem = handler.CreateProductFromJobsheet(JobSheet);
            return View(PurchaseInvoiceItem);
        }

        [HttpPost]
        public ActionResult CreateProduct(PurchaseInvoiceItem Modal)
        {
            db.Configuration.ProxyCreationEnabled = false;
            PurchaseInvoiceItem PurchaseInvoiceItem = handler.SavePurchaseInvoiceItem(Modal);
            handler.CreateProducts(PurchaseInvoiceItem.JobsheetId.Value,db.CompanyId ?? 0);
            JobSheet JobSheet = db.JobSheets.Where(x => x.JobSheetId == PurchaseInvoiceItem.JobsheetId.Value).FirstOrDefault();
            JobSheet.IsProductCreated = true;
            PrdDb.SaveJobSheet(JobSheet);
            JobSheet.FutherProcessJobSheetList = db.JobSheets.Where(x => x.ParentJobSheetId == JobSheet.JobSheetId && x.JobSheetComplete == Enums.JobSheetComplete.Complete).ToList();
            if (JobSheet.FutherProcessJobSheetList != null && JobSheet.FutherProcessJobSheetList.Count > 0)
            {
                foreach (var Jobsheets in JobSheet.FutherProcessJobSheetList)
                {
                    Jobsheets.JobSheetComplete = Enums.JobSheetComplete.Complete;
                    Jobsheets.IsProductCreated = true;
                    handler.SaveFutherProcessJobSheet(Jobsheets);
                }
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult EditProduct(Product product)
        {
            if (product.DiscountIds != null)
            {
                List<int> templist = product.DiscountIds.OfType<int>().ToList();
                product.DiscountsAccessible = string.Join(",", templist);
            }
            else
            {
                product.DiscountsAccessible = "";
            }
            db.SaveProduct(product);
            return RedirectToAction("Index");
        }
        #endregion

        #region MeasurmentSet
        [HttpPost]
        public ActionResult _MeasurementList(MeasurmentSet model)
        {
            JobSheet jobsheet = new JobSheet();
            model.Type = Enums.MeasurementType.Readymade;
            if (model.JobSheetId > 0)
            {
                db.SaveMeasurmentSet(model);
                jobsheet.MeasurmentSetListForReadymade = db.MeasurmentSets.Where(x => x.JobSheetId == model.JobSheetId && x.Type == Enums.MeasurementType.Readymade).ToList();
            }
            else
            {
                SessionManager.SetMeasurmentSet(model);
                jobsheet.MeasurmentSetListForReadymade = SessionManager.GetMeasurmentSetList();
            }
            return PartialView(jobsheet);
        }

        [HttpPost]
        public JsonResult EditMeasurmentSet(int MeasurmentSetId, int JobSheetId)
        {
            MeasurmentSet model = null;
            if (JobSheetId > 0)
            {
                model = db.MeasurmentSets.Where(x => x.MeasurmentSetId == MeasurmentSetId).FirstOrDefault();
            }
            else
            {
                List<MeasurmentSet> MeasurmentSetList = SessionManager.GetMeasurmentSetList();
                if (MeasurmentSetList != null)
                {
                    model = MeasurmentSetList.Where(x => x.MeasurmentSetId == MeasurmentSetId).FirstOrDefault();
                }
            }
            return Json(model);
        }

        public ActionResult RemoveMeasurement(int MeasurmentSetId, int JobSheetId)
        {
            db.Configuration.ProxyCreationEnabled = false;
            JobSheet jobsheet = new JobSheet();
            if (JobSheetId > 0)
            {
                MeasurmentSet model = db.MeasurmentSets.Where(x => x.MeasurmentSetId == MeasurmentSetId).FirstOrDefault();
                PrdDb.DeleteMeasurment(model);
                jobsheet.MeasurmentSetListForReadymade = db.MeasurmentSets.Where(x => x.JobSheetId == JobSheetId).ToList();
            }
            else
            {
                SessionManager.RemoveMeasurmentSet(MeasurmentSetId);
                jobsheet.MeasurmentSetListForReadymade = SessionManager.GetMeasurmentSetList();
            }
            return PartialView("_MeasurementList", jobsheet);
        }
        #endregion

        #region JobSheet Services
        [HttpPost]
        public ActionResult _JobSheetServices(JobSheetService model, int Quantity)
        {
            JobSheet jobsheet = new JobSheet();
            if (model.JobSheetId > 0)
            {
                PrdDb.SaveJobService(model);
                model.GST = db.GSTs.Where(x => x.GSTId == model.GSTId).FirstOrDefault();
                string JobSheetNo = db.JobSheets.Where(x => x.JobSheetId == model.JobSheetId).Select(x => x.ProductNumber).FirstOrDefault();
                decimal TotalGST = (model.GST.CGST ?? 0) + (model.GST.IGST ?? 0) + model.GST.SGST;
                model.TotalIncludeGST = (model.Price.Value + ((model.Price.Value * TotalGST) / 100)) * model.Quantity;

                Voucher Voucher = db.Vouchers.Where(x => x.JobSheetServiceId == model.JobSheetServiceId).FirstOrDefault();
                if (Voucher != null)
                {
                    Voucher.JobSheetServiceId = model.JobSheetServiceId;
                    Voucher.LedgerId = model.LedgerId;
                    Voucher.Total = model.TotalIncludeGST.Value;
                    Voucher.Type = Constants.CR;
                    Voucher.Particulars = "JobSheet Number :" + JobSheetNo;
                    db.SaveVauchers(Voucher);
                }
                else
                {
                    Voucher = new Voucher();
                    Voucher.JobSheetServiceId = model.JobSheetServiceId;
                    Voucher.LedgerId = model.LedgerId;
                    Voucher.Total = model.TotalIncludeGST.Value;
                    Voucher.Type = Constants.CR;
                    Voucher.EntryDate = DateTime.Now;
                    Voucher.Particulars = "JobSheet Number :" + JobSheetNo;
                    db.SaveVauchers(Voucher);
                }

                jobsheet.JobSheetServicesList = db.JobSheetServices.Where(x => x.JobSheetId == model.JobSheetId).ToList();
                handler.SaveJobSheetCost(model.JobSheetId);
            }
            else
            {
                SessionManager.SetJobService(model);
                jobsheet.JobSheetServicesList = SessionManager.GetServiceList();
            }

            if (jobsheet.JobSheetServicesList != null)
            {
                jobsheet.ServicesListQuantity = 0;
                jobsheet.TotalProductionCostNotMapped = 0;
                Dictionary<int, GST> idVsGST = db.GetGSTMasterDictionary(jobsheet.JobSheetServicesList.Select(x => x.GSTId.Value).ToList());
                Dictionary<int, ServiceType> idVsServiceType = db.GetServiceTypeMasterDictionary(jobsheet.JobSheetServicesList.Select(x => x.ServiceTypeId).ToList());
                Dictionary<int, Ledger> idVsLedger = db.GetLedgerDictionary(jobsheet.JobSheetServicesList.Select(x => x.LedgerId).ToList());
                foreach (var Newitem in jobsheet.JobSheetServicesList)
                {
                    Newitem.ServiceType = idVsServiceType[Newitem.ServiceTypeId];
                    Newitem.GST = idVsGST[Newitem.GSTId.Value];
                    Newitem.Ledger = idVsLedger[Newitem.LedgerId];
                    decimal TotalGST = (Newitem.GST.CGST ?? 0) + (Newitem.GST.IGST ?? 0) + Newitem.GST.SGST;
                    Newitem.TotalIncludeGST = (Newitem.Price.Value + ((Newitem.Price.Value * TotalGST) / 100)) * Newitem.Quantity;
                    jobsheet.TotalServiceCostNotMapped = jobsheet.TotalServiceCostNotMapped + (Newitem.TotalIncludeGST ?? 0);
                }
            }
            return PartialView(jobsheet);
        }

        [HttpPost]
        public JsonResult EditServices(int JobSheetServiceId, int JobSheetId)
        {
            JobSheetService model = null;
            if (JobSheetId > 0)
            {
                model = db.JobSheetServices.Where(x => x.JobSheetServiceId == JobSheetServiceId).FirstOrDefault();
            }
            else
            {
                List<JobSheetService> ServiceList = SessionManager.GetServiceList();
                if (ServiceList != null)
                {
                    model = ServiceList.Where(x => x.JobSheetServiceId == JobSheetServiceId).FirstOrDefault();
                }
            }
            return Json(model);
        }

        public ActionResult RemoveServices(int JobSheetServiceId, int JobSheetId)
        {
            JobSheet jobsheet = new JobSheet();
            if (JobSheetId > 0)
            {
                JobSheetService model = db.JobSheetServices.Where(x => x.JobSheetServiceId == JobSheetServiceId).FirstOrDefault();
                model.GST = db.GSTs.Where(x => x.GSTId == model.GSTId).FirstOrDefault();
                decimal TotalGST = (model.GST.CGST ?? 0) + (model.GST.IGST ?? 0) + model.GST.SGST;
                model.TotalIncludeGST = (model.Price.Value + ((model.Price.Value * TotalGST) / 100)) * model.Quantity;
                Voucher Voucher = db.Vouchers.Where(x => x.JobSheetServiceId == model.JobSheetServiceId).FirstOrDefault();
                if (Voucher != null)
                {
                    //Ledger Ledger = db.Ledgers.Where(x => x.LedgerId == model.LedgerId).FirstOrDefault();
                    //Ledger.CurrentBalance = Ledger.CurrentBalance - Voucher.Total;

                    //db.SaveLedger(Ledger);
                    db.RemoveVoucher(Voucher);
                    model.GST = null;
                    model.Ledger = null;
                }
                PrdDb.RemoveJobService(model);
                jobsheet.JobSheetServicesList = db.JobSheetServices.Where(x => x.JobSheetId == JobSheetId).ToList();
                handler.SaveJobSheetCost(JobSheetId);
            }
            else
            {
                SessionManager.RemoveJobService(JobSheetServiceId);
                jobsheet.JobSheetServicesList = SessionManager.GetServiceList();
            }
            if (jobsheet.JobSheetServicesList != null)
            {
                jobsheet.ServicesListQuantity = 0;
                jobsheet.TotalProductionCostNotMapped = 0;
                Dictionary<int, GST> idVsGST = db.GetGSTMasterDictionary(jobsheet.JobSheetServicesList.Select(x => x.GSTId.Value).ToList());
                Dictionary<int, ServiceType> idVsServiceType = db.GetServiceTypeMasterDictionary(jobsheet.JobSheetServicesList.Select(x => x.ServiceTypeId).ToList());
                Dictionary<int, Ledger> idVsLedger = db.GetLedgerDictionary(jobsheet.JobSheetServicesList.Select(x => x.LedgerId).ToList());
                foreach (var Newitem in jobsheet.JobSheetServicesList)
                {
                    Newitem.ServiceType = idVsServiceType[Newitem.ServiceTypeId];
                    Newitem.GST = idVsGST[Newitem.GSTId.Value];
                    Newitem.Ledger = idVsLedger[Newitem.LedgerId];
                    decimal TotalGST = (Newitem.GST.CGST ?? 0) + (Newitem.GST.IGST ?? 0) + Newitem.GST.SGST;
                    Newitem.TotalIncludeGST = (Newitem.Price.Value + ((Newitem.Price.Value * TotalGST) / 100)) * Newitem.Quantity;
                    jobsheet.TotalServiceCostNotMapped = jobsheet.TotalServiceCostNotMapped + (Newitem.TotalIncludeGST ?? 0);
                }
            }
            return PartialView("_JobSheetServices", jobsheet);
        }
        #endregion

        #region JobSheet Item
        [HttpPost]
        public ActionResult UpdateProduct(JobSheetItem model)
        {
            JobSheet jobsheet = new JobSheet();
            if (model.JobSheetItemId > 0)
            {
                int JobSheetId = handler.UpDateJobSheetItem(model);
                jobsheet.JobSheetItemList = handler.GetJobSheetItems(JobSheetId);
                handler.SaveJobSheetCost(JobSheetId);
            }
            else
            {
                SessionManager.UpdateJobSheetItem(model);
                jobsheet.JobSheetItemList = SessionManager.GetJobSheetItemList();
            }

            if (jobsheet.JobSheetItemList != null)
            {
                jobsheet = SaleCalculator.CalculateProduction(jobsheet);
            }
            return PartialView("_ItemList", jobsheet);
        }

        [HttpPost]
        public ActionResult _ItemList(string ProdId, int? ProductionId)
        {
            string message = string.Empty;
            JobSheet production = new JobSheet();
            //Query from appropriate channel depending upon SalesInvoiceid
            if (ProductionId.HasValue && ProductionId.Value == 0)
            {
                production.JobSheetItemList = SessionManager.GetJobSheetItemList();
            }
            else
            {
                production.JobSheetItemList = handler.GetJobSheetItems(ProductionId);
            }
            //Check if product is already available in the current invoice
            //Check if product is not sold
            Product product = db.GetProductByNumber(ProdId);
            if (product == null)
            {
                ViewBag.ErrorMsg = "The product is unavailable or already sold";
            }
            else
            {
                bool isAdded = production.JobSheetItemList.Any(x => x.ProductId == product.ProductId);
                if (isAdded)
                {
                    ViewBag.ErrorMsg = "This product is already added";
                }
                else
                {
                    //Add item to invoice
                    if (ProductionId.HasValue && ProductionId.Value == 0)
                    {
                        ProductManager.AddProductionItem(product, ProductionId);
                        production.JobSheetItemList = SessionManager.GetJobSheetItemList();
                    }
                    else
                    {
                        production.JobSheetItemList.Add(ProductManager.AddProductionItem(product, ProductionId));
                    }
                }
            }
            if ((ProductionId ?? 0) > 0)
            {
                handler.SaveJobSheetCost(ProductionId);
            }
            production = SaleCalculator.CalculateProduction(production);
            return PartialView(production);
        }

        [HttpPost]
        public ActionResult RemoveItem(int ProductId, int? JobSheetId)
        {
            JobSheet jobsheet = new JobSheet();
            if (JobSheetId == null || JobSheetId == 0)
            {
                SessionManager.RemoveJobSheetItem(ProductId);
                jobsheet.JobSheetItemList = SessionManager.GetJobSheetItemList();
            }
            else
            {
                handler.RemoveJobSheetItem(JobSheetId, ProductId);
                jobsheet.JobSheetItemList = handler.GetJobSheetItems(JobSheetId);
                handler.SaveJobSheetCost(JobSheetId);
            }

            if (jobsheet.JobSheetItemList != null)
            {
                jobsheet = SaleCalculator.CalculateProduction(jobsheet);
            }
            return PartialView("_ItemList", jobsheet);
        }
        #endregion
    }
}