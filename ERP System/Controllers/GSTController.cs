﻿using System;
using System.Web.Mvc;
using DataLayer;
using DataLayer.Models;
using DataLayer.ViewModels;
using DataLayer.Utils;
using System.Linq;
using ERP_System.Utils;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class GSTController : Controller
    {
        DbConnector db = new DbConnector();
        // GET: UnitMaster
        [RBAC(AccessType = 2)]
        public ActionResult Index(int? id, string ErrorMsg)
        {
            GSTViewModel model = new GSTViewModel();
            if (model.GSTvalueList == null)
            {
                model.GSTvalueList = db.GetGSTvalueList();
            }
            if (id.HasValue)
            {
                model.GST = db.GetGSTMasterById(id.Value);
            }
            else
            {
                model.GST = new GST();
            }
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            return View(model);
        }

        [RBAC(AccessType = 0)]
        [HttpPost]
        public ActionResult Create(GST gst)
        {
            db.SaveGSTMaster(gst);
            return RedirectToAction("Index");
        }

        [RBAC(AccessType = 3)]
        public ActionResult Delete(int id)
        {
            GST gst = db.GetGSTMasterById(id);
            try
            {
                if (gst == null)
                {
                    return HttpNotFound();
                }
                else
                {
                    if (db.RemoveGST(gst))
                    {
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (Exception ex)
            {
                db.RegisterException(ex);
            }
            return RedirectToAction("Index", new { ErrorMsg = MessageStore.DeleteError });
        }

        [HttpPost]
        public JsonResult ExistingGST(string Name)
        {
            var GST = db.GSTs.Where(x => x.Name == Name).FirstOrDefault();
            if (GST == null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}