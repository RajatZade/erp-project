﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DataLayer.Models;
using DataLayer.Utils;
using DataLayer;
using DataLayer.ViewModels;
using ERP_System.Utils;
using System;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class LedgerController : Controller
    {
        DbConnector db = new DbConnector();

        // GET: Ledger
        [RBAC(AccessType = 2)]
        public ActionResult Index(string ErrorMsg,string Type)
        {
            LedgerViewModel model = new LedgerViewModel();
            if (string.IsNullOrEmpty(Type))
            {                
                model.Type = Enums.RecordType.Supplier.ToString();
                model.LedgerList = db.GetLedgerListByType(model.Type);
            }
            else
            {
                if (Type == "Other")
                {
                    model.LedgerList = db.LedgerListOfOther();
                }
                else
                {
                    model.LedgerList = db.GetLedgerListByType(Type);
                }
            }
            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            return View(model);
        }

        public ActionResult Create(int? id)
        {
            Ledger ledger = null;
            if (id.HasValue && id.Value > 0)
            {
                ledger = PurchaseCalculator.GetLedgerOeningBalance(id.Value); 
                if(ledger.Contact == null)
                {
                    ledger.Contact = new Contact();
                }
                ledger.ContactCategoryList = DropdownManager.GetContactCategoryList(null);
                ledger.ContactSubCategoryList = DropdownManager.GetContactSubCategoryList(ledger.AccountCategoryId);
                Voucher voucher = db.Vouchers.Where(x => x.LedgerId == id.Value && x.VoucherFor == Constants.OPENING_BALANCE && x.CompanyId == db.CompanyId).FirstOrDefault();
                if(voucher != null)
                {
                    ledger.StartingBalance = voucher.Total;
                    ledger.EntryDate = voucher.EntryDate;
                }
                else
                {
                    ledger.StartingBalance = 0;
                    ledger.EntryDate = DateTime.Now;
                }
            }
            else
            {
                ledger = new Ledger();
                ledger.Contact = new Contact();
                ledger.ContactCategoryList = DropdownManager.GetContactCategoryList(null);
                ledger.ContactSubCategoryList = DropdownManager.GetContactSubCategoryList(null);
                ledger.EntryDate = DateTime.Now;
            }
            return View(ledger);
        }

        [RBAC(AccessType = 0)]
        [HttpPost]
        public ActionResult Create(Ledger ledger)
        {
            if (!string.IsNullOrEmpty(ledger.other))
            {
                ledger.AccountCategoryId = 0;
                ledger.AccountCategoryId = db.GetContactCategoryIdByName(ledger.other);
            }
            if(ledger.AccountSubCategoryId==-1)
            {
                ledger.AccountSubCategoryId = null;
            }
          
            if(ledger.AccountCategoryId == 1 || ledger.AccountCategoryId == 47)
            {
                ledger.LedgerType = Constants.LEDGER_TYPE_PAYMENTTYPE;
            }
            IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                if (db.SaveLedger(ledger))
                {
                    Voucher Voucher = db.Vouchers.Where(x => x.LedgerId == ledger.LedgerId && x.VoucherFor == Constants.OPENING_BALANCE && x.CompanyId == db.CompanyId).FirstOrDefault();
                    if (Voucher != null)
                    {
                        Voucher.Type = ledger.Type;
                        Voucher.Total = ledger.StartingBalance;
                        Voucher.EntryDate = ledger.EntryDate ?? DateTime.Now;
                        db.SaveVauchers(Voucher);
                    }
                    else
                    {
                        Voucher = new Voucher();
                        Voucher.LedgerId = ledger.LedgerId;
                        Voucher.Total = ledger.StartingBalance;
                        Voucher.Type = ledger.Type;
                        Voucher.VoucherFor = Constants.OPENING_BALANCE;
                        Voucher.Particulars = Constants.OPENING_BALANCE;
                        Voucher.EntryDate = ledger.EntryDate ?? DateTime.Now;
                        db.SaveVauchers(Voucher);
                    }
                    return RedirectToAction("Index", new { Type = ledger.Contact.Type });
                }
            }
            return View(ledger);
        }

        public JsonResult GetLedgerList(string term)
        {
            var result = new List<KeyValuePair<string, string>>();
            IList<SelectListItem> List = DropdownManager.GetLedgerSelectList();
            foreach (var item in List)
            {
                result.Add(new KeyValuePair<string, string>(item.Value.ToString(), item.Text));
            }
            var result3 = result.Where(s => s.Value.ToLower().Contains(term.ToLower())).Select(w => w).ToList();
            return Json(result3, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveNewContact(string othercontact)
        {
            AccountCategory contactcategory = new AccountCategory();
            contactcategory.Name = othercontact;
            if (db.SaveContactcategory(contactcategory))
            {
                return Json(true);
            }
            else
            {
                return Json(false);
            }
        }
        
        [HttpPost]
        public ActionResult SaveContactCategory(ContactCategoryViewModel model)
        {
            db.SaveContactcategory(model.ContactCategory);
            return RedirectToAction("CreateContactCategory");
        }

        public ActionResult CreateContactCategory(int? id)
        {
            ContactCategoryViewModel model = new ContactCategoryViewModel();
            if (model.ContactCategoryList == null)
            {
                model.ContactCategoryList = db.GetContactCategoryList(null);
            }
            if (id.HasValue)
            {
                model.ContactCategory = db.GetContactCategoryById(id.Value);
            }
            else
            {
                model.ContactCategory = new AccountCategory();
            }
            return View(model);
        }

        public ActionResult CreateContactSubCategory(int? id)
        {
            ContactCategoryViewModel model = new ContactCategoryViewModel();
            if (model.ContactSubCategoryList == null)
            {
                model.ContactCategoryList = db.GetContactCategoryList(0);
            }
            if (id.HasValue)
            {
                model.ContactCategory = db.GetContactCategoryById(id.Value);
            }
            else
            {
                model.ContactCategory = new AccountCategory();
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult CreateContactSubCategory(AccountSubCategory model)
        {
            db.SaveContactSubCategory(model);
            return View(model);
        }

        [HttpPost]
        public JsonResult ContactSubCategoryList(int contactcategoryid)
        {
            List<AccountSubCategory> sublist = db.GetContactSubCategoryList(contactcategoryid);
            List<SelectListItem> itemList = new List<SelectListItem>();
            if (sublist != null && sublist.Count > 0)
            {
                foreach (var item in sublist)
                {
                    itemList.Add(new SelectListItem() { Text = item.ContactSubCategoryName, Value = item.AccountSubCategoryId.ToString() });
                }
            }
            return Json(new SelectList(itemList, "Value", "Text"));
        }

        public ActionResult Delete(int id)
        {
            string Type = null;
            Ledger ledger = db.Ledgers.Where(x => x.LedgerId == id).FirstOrDefault();
            if (db.DeleteLedger(ledger))
            {
                return RedirectToAction("Index", new { Type = Type });
            }
            return RedirectToAction("Index", new { ErrorMsg = MessageStore.DeleteError , Type  = Type });
        }

        [HttpPost]
        public ActionResult _LedgerList(string Type)
        {
            LedgerViewModel model = new LedgerViewModel();
            if(Type == "Other")
            {
                model.LedgerList = db.LedgerListOfOther();
            }
            else
            {
                model.LedgerList = db.GetLedgerListByType(Type);
            }
            model.Type = Type;
            return PartialView(model);
        }

        [HttpPost]
        public JsonResult ExistingLedger(string Name)
        {
            var Ledger = db.Ledgers.Where(x => (x.Name == Name && x.CompanyId == null) || (x.Name == Name && x.CompanyId == db.CompanyId)).FirstOrDefault();
            if (Ledger == null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}
