﻿using DataLayer;
using DataLayer.Models;
using DataLayer.Utils;
using DataLayer.ViewModels;
using ERP_System.Utils;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class ProductionReportController : Controller
    {
        // GET: ProductionReport
        DbConnector db = new DbConnector();
        [RBAC(AccessType = 2)]
        public ActionResult Index()
        {
            ProductionViewModel model = new ProductionViewModel();
            model.JobSheetList = db.JobSheets.Where(x => x.CompanyId == db.CompanyId).ToList();
            model.Company = SessionManager.GetSessionCompany();
            model.Branch = SessionManager.GetSessionBranch();
            Dictionary<int, SalesOrder> idVsSalesOrder = db.GetSaleOrderDictionary(model.JobSheetList.Select(x => x.SaleOrderId ?? 0).ToList());
            foreach (var item in model.JobSheetList)
            {
                if (item.SaleOrderId.HasValue && item.SaleOrderId != -1)
                {
                    item.SalesOrder = idVsSalesOrder[item.SaleOrderId.Value];
                }
                JobSheetService service = db.JobSheetServices.Where(x => x.JobSheetId == item.JobSheetId).FirstOrDefault();
                if (service != null)
                {
                    item.AssignedWorker = db.Ledgers.Where(x => x.LedgerId == service.LedgerId).FirstOrDefault();
                }
                item.Customer = db.Ledgers.Where(x => x.LedgerId == item.CustomerId).FirstOrDefault();
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult _JobSheetList(ProductionViewModel model)
        {
            model.JobSheetList = db.JobSheets.Where(x => x.CompanyId == db.CompanyId).ToList();
            if (model.FromDate.HasValue)
            {
                model.JobSheetList = model.JobSheetList.Where(x => x.JobDate >= model.FromDate).ToList();
            }
            if (model.ToDate.HasValue)
            {
                model.JobSheetList = model.JobSheetList.Where(x => x.JobDate <= model.ToDate).ToList();
            }
            model.Company = SessionManager.GetSessionCompany();
            model.Branch = SessionManager.GetSessionBranch();
            Dictionary<int, SalesOrder> idVsSalesOrder = db.GetSaleOrderDictionary(model.JobSheetList.Select(x => x.SaleOrderId ?? 0).ToList());
            foreach (var item in model.JobSheetList)
            {
                if (item.SaleOrderId.HasValue && item.SaleOrderId != -1)
                {
                    item.SalesOrder = idVsSalesOrder[item.SaleOrderId.Value];
                }
                JobSheetService service = db.JobSheetServices.Where(x => x.JobSheetId == item.JobSheetId).FirstOrDefault();
                if (service != null)
                {
                    item.AssignedWorker = db.Ledgers.Where(x => x.LedgerId == service.LedgerId).FirstOrDefault();
                }
                item.Customer = db.Ledgers.Where(x => x.LedgerId == item.CustomerId).FirstOrDefault();
            }
            return PartialView(model);
        }
    }
}