﻿using System;
using System.Web.Mvc;
using DataLayer;
using DataLayer.Models;
using DataLayer.ViewModels;
using DataLayer.Utils;
using ERP_System.Utils;
using System.Linq;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class UnitMasterController : Controller
    {
        DbConnector db = new DbConnector();
        UnitMasterViewModel model = new UnitMasterViewModel();
        // GET: UnitMaster

        public ActionResult Index(int? UnitId,string ErrorMsg)
        {            

            if (model.UnitMasterList == null)
            {
                model.UnitMasterList = db.GetUnitMasterList();
            }

            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            
            
            if (UnitId.HasValue)
            {
                model.unitmaster = db.GetUnitMasterById(UnitId.Value);
            }
            else
            {
                model.unitmaster = new Unit();                
            }
            model.unitmaster.UnitTypeList = DropdownManager.GetUnitMasterList(UnitId);
            return View(model);
        }

        [RBAC(AccessType = 0)]
        [HttpPost]
        public ActionResult CreateUnit(Unit unitMaster)
        {
            Unit model = db.GetUnitMasterById(unitMaster.UnitTypeId.GetValueOrDefault());
            unitMaster.UnitParents = model.UnitValue;
            unitMaster.UnitTypeId = model.UnitId;
            unitMaster.SellInPartial = model.SellInPartial;
            db.SaveUnitMaster(unitMaster);
            return RedirectToAction("Index");
        }

        [RBAC(AccessType = 0)]
        [HttpPost]
        public ActionResult CreateUnitCategory(Unit unitMaster)
        {
            unitMaster.UnitParents = unitMaster.UnitValue;
            unitMaster.Conversion = 1;
            unitMaster.DisableDelete = true;
            db.SaveUnitMaster(unitMaster);
            unitMaster.UnitTypeId = unitMaster.UnitId;
            db.SaveUnitMaster(unitMaster);
            return RedirectToAction("Index");
        }

        [RBAC(AccessType = 3)]
        public ActionResult DeleteUnit(int id)
        {
            Unit unitMaster = db.UnitMasters.Find(id);
            if (unitMaster == null)
            {
                return HttpNotFound();
            }
            else
            {
                if (db.RemoveUnit(unitMaster))
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index", new { ErrorMsg = MessageStore.DeleteError });
        }

        [HttpPost]
        public JsonResult ExistingUnit(string UnitValue)
        {
            Unit model = db.UnitMasters.Where(x => x.UnitValue == UnitValue).FirstOrDefault();
            if (model == null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}