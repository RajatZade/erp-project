﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DataLayer;
using DataLayer.Models;
using DataLayer.ViewModels;
using DataLayer.Utils;
using ERP_System.Utils;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class LeadController : Controller
    {
        LeadViewModel model = new LeadViewModel();
        DbConnector db = new DbConnector();
        // GET: Lead
        public ActionResult Index(string ErrorMsg)
        {
            model.LeadsList = db.GetLeadList();
           

            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                ViewBag.ErrorMsg = ErrorMsg;
            }
            return View(model);
        }

        public ActionResult Create(int? id)
        {
            Lead lead = new Lead();
            if (id.HasValue && id.Value > 0)
            {
                lead = db.Leads.Include("LeadItemList").Where(x => x.LeadId == id).FirstOrDefault();
            }
            else
            {
                lead.Country = "India";
                lead = new Lead();
            }
            lead.ContactList = DropdownManager.GetCustomerForLead();
            return View(lead);
        }

        [HttpPost]
        public ActionResult Create(Lead lead)
        {

            if (ModelState.IsValid)
            {
                //if (lead.City != null)
                //    lead.City = lead.City;
                //else
                //    lead.City = lead.OtherCity.ToString();

               
               
                if (db.SaveLead(lead))
                {
                    List<LeadItem> LeadItemList = SessionManager.GetLeadItemList();
                    foreach(var Item in LeadItemList)
                    {
                        Item.LeadId = lead.LeadId;
                        Item.LeadItemId = 0;
                        db.SaveLeadItem(Item);
                    }
                    SessionManager.EmptySessionList(Constants.LEAD_ITEM_LIST);
                    return RedirectToAction("Index");
                }
            }
            return View(lead);
        }

        [HttpPost]
        public ActionResult Convert(Lead lead)
        {

            if (lead.LeadStatus == "Converted")
            {
                Contact contact = new Contact();
                contact.FirstName = lead.FirstName;
                contact.LastName = lead.LastName;
                contact.Country = lead.Country;
                contact.State = lead.State;
                contact.City = lead.City;
                contact.Email = lead.EmailId;
                contact.Type = "Customer";
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                db.SaveContact(contact);
            }
            else
            {

            }
            return View(lead);
        }

        public ActionResult Delete(int id)
        {
            Lead lead = db.Leads.Find(id);
            db.RemoveLead(lead);
            if (lead == null)
            {
                return HttpNotFound();
            }
            else
            {
                if (db.RemoveLead(lead))
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index", new { ErrorMsg = MessageStore.DeleteError });
        }

        public ActionResult CustomerInformation()
        {
            CustomerInformationViewModel model = new CustomerInformationViewModel();
            model.ContactList = DropdownManager.GetCustomerForCRM();
            return View(model);
        }

        [HttpPost]
        public ActionResult CustomerInformation(CustomerInformationViewModel model)
        {
            model.Ledger = db.Ledgers.Where(x => x.LedgerId == model.ContactId).FirstOrDefault();
            model.Contact = model.Ledger.Contact;
            model.SaleInvoiceList = db.SaleInvoices.Where(x => x.CustomerId == model.Ledger.LedgerId).ToList();
            if(model.SaleInvoiceList != null)
            {
                model.TotalSaleValue = model.SaleInvoiceList.Sum(x => x.TotalWithAdjustment);
            }
            model.PurchaseInvoiceList = db.PurchaseInvoices.Where(x => x.AccountId == model.Ledger.LedgerId).ToList();
            if (model.SaleInvoiceList != null)
            {
                model.TotalPurchaseValue = model.PurchaseInvoiceList.Sum(x => x.TotalInvoiceValue.HasValue ? x.TotalInvoiceValue.Value : 0);
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult _LeadItemList(LeadItem model)
        {
            Lead Lead = new Lead();
            if (model.LeadId > 0)
            {
                db.SaveLeadItem(model);
                Lead.LeadItemList = db.LeadItems.Where(x => x.LeadId == model.LeadId).ToList();
            }
            else
            {
                SessionManager.SetLeadItem(model);
                Lead.LeadItemList = SessionManager.GetLeadItemList();
            }
            return PartialView(Lead);
        }

        public ActionResult RemoveLead(int LeadItemId, int LeadId)
        {
            Lead Lead = new Lead();
            if (LeadId > 0)
            {
                LeadItem model = db.LeadItems.Where(x => x.LeadItemId == LeadItemId).FirstOrDefault();
                db.RemoveLeadItem(model);
                Lead.LeadItemList = db.LeadItems.Where(x => x.LeadId == LeadId).ToList();
            }
            else
            {
                SessionManager.RemoveLeadItem(LeadItemId);
                Lead.LeadItemList = SessionManager.GetLeadItemList();
            }
            return PartialView("_LeadItemList", Lead);
        }

        [HttpPost]
        public JsonResult EditLead(int LeadItemId, int LeadId)
        {
            LeadItem model = null;
            if (LeadId > 0)
            {
                model = db.LeadItems.Where(x => x.LeadItemId == LeadItemId).FirstOrDefault();
            }
            else
            {
                model = SessionManager.GetLeadItemList().Where(x => x.LeadItemId == LeadItemId).FirstOrDefault();
            }
            return Json(model);
        }

        [HttpPost]
        public JsonResult GetContact(int ContactId)
        {
            Contact model = db.Contacts.Where(x => x.ContactId == ContactId).FirstOrDefault();
            return Json(model);
        }
    }

}
