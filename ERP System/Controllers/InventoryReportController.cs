﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DataLayer.Models;
using DataLayer.ViewModels;
using ERP_System.Utils;
using DataLayer.Utils;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class InventoryReportController : Controller
    {
        DbConnector db = new DbConnector();
        // GET: InventoryReport
        [RBAC(AccessType = 2)]
        public ActionResult Index()
        {
            InventoryReportViewModel ViewModel = new InventoryReportViewModel();
            ViewModel.LocationList = DropdownManager.GetLocationByBranch(SessionManager.GetSessionBranch().BranchId);
            List<int> SKUIds = db.Products.Where(x => x.CompanyId == (db.CompanyId ?? 0)).Select(x => x.SKUMasterId).ToList();
            List<SKUMaster> SKUList = db.SKUMasters.Where(x => SKUIds.Contains(x.SKUMasterId)).ToList();
            List<Product> ProductList = db.Products.Where(x => SKUIds.Contains(x.SKUMasterId)).ToList();
            List<PurchaseInvoiceItem> PurchaseInvoiceItemList = db.PurchaseInvoiceItems.Where(x => SKUIds.Contains(x.SKUMasterId) && x.IsProductCreated == true && x.CompanyId == db.CompanyId).ToList();
            List<SaleInvoiceItem> EXCESSESItemList = db.SaleInvoiceItems.Where(x => SKUIds.Contains(x.SKUMasterId.Value) && x.Status == Constants.SALE_ITEM_EXCESSES && x.Product.CompanyId == db.CompanyId).ToList();
            List<SaleInvoiceItem> SaleInvoiceItemList = db.SaleInvoiceItems.Where(x => SKUIds.Contains(x.SKUMasterId.Value) && x.Status == Constants.SALE_ITEM_SOLD && x.Product.CompanyId == db.CompanyId).ToList();
            List<JobSheetItem> JobSheetItemList = db.JobSheetItems.Where(x => SKUIds.Contains(x.Product.SKUMasterId) && x.Product.CompanyId == db.CompanyId).ToList();
            List<int> SubCategoryIds = SKUList.Select(x => x.SubCategoryId).ToList();
            List<SubCategory> SubCategoryList = db.SubCategories.Where(x => SubCategoryIds.Contains(x.SubCategoryId)).ToList();
            List<int> CategoryIds = SKUList.Select(x => x.CategoryId).ToList();
            List<Category> CategoryList = db.Categories.Where(x => CategoryIds.Contains(x.CategoryId)).ToList();
            List<int> UnitIds = PurchaseInvoiceItemList.Select(x => x.UnitId).ToList();
            List<Unit> UnitList = db.UnitMasters.Where(x => UnitIds.Contains(x.UnitId)).ToList();
            foreach (var item in CategoryList)
            {
                CateogoryList Cateogory = new CateogoryList
                {
                    CategoryName = item.Name,
                    SubCategoryList = new List<SubCategoryList>()
                };
                foreach (var Subcategory in SubCategoryList.Where(x => x.CategoryId == item.CategoryId))
                {
                    SubCategoryList subCategoryList = new SubCategoryList
                    {
                        SubCategoryName = Subcategory.Name,
                        SKUMasterList = new List<SKUMasterList>()
                    };
                    foreach (var SKU in SKUList.Where(x => x.CategoryId == item.CategoryId && x.SubCategoryId == Subcategory.SubCategoryId))
                    {
                        SKUMasterList SKUMasterItem = new SKUMasterList();
                        SKUMasterItem.SKUName = SKU.SKU;
                        SKUMasterItem.SKUMasterId = SKU.SKUMasterId;
                        SKUMasterItem.Unit = UnitList.Where(x => x.UnitId == PurchaseInvoiceItemList.Where(y => y.SKUMasterId == SKU.SKUMasterId).Select(y => y.UnitId).FirstOrDefault()).Select(x => x.UnitValue).FirstOrDefault();

                        SKUMasterItem.InWards = new ProductQuantity();
                        SKUMasterItem.InWards.Count = PurchaseInvoiceItemList.Where(x => x.SKUMasterId == SKU.SKUMasterId).Sum(x => x.Quantity);
                        SKUMasterItem.InWards.Value = PurchaseInvoiceItemList.Where(x => x.SKUMasterId == SKU.SKUMasterId).Sum(x => x.Rate * x.Quantity);
                        if (SKUMasterItem.InWards.Count > 0)
                        {
                            SKUMasterItem.InWards.Rate = SKUMasterItem.InWards.Value / SKUMasterItem.InWards.Count;
                        }
                        else
                        {
                            SKUMasterItem.InWards.Value = 0;
                        }

                        ViewModel.TotalInWardsValue = ViewModel.TotalInWardsValue + SKUMasterItem.InWards.Value;
                        subCategoryList.InWardsCountTotal = subCategoryList.InWardsCountTotal + SKUMasterItem.InWards.Count;
                        subCategoryList.InWardsValueTotal = subCategoryList.InWardsValueTotal + SKUMasterItem.InWards.Value;
                        if (subCategoryList.InWardsCountTotal != 0)
                        {
                            subCategoryList.InWardsRateTotal = subCategoryList.InWardsValueTotal / subCategoryList.InWardsCountTotal;
                        }


                        SKUMasterItem.OnWards = new ProductQuantity();
                        SKUMasterItem.OnWards.Count = SaleInvoiceItemList.Where(x => x.SKUMasterId == SKU.SKUMasterId).Sum(x => x.ConversionValue ?? x.Quantity) + JobSheetItemList.Where(x => x.Product.SKUMasterId == SKU.SKUMasterId).Sum(x => x.ConversionValue ?? x.Quantity);
                        SKUMasterItem.OnWards.Value = SaleInvoiceItemList.Where(x => x.SKUMasterId == SKU.SKUMasterId).Sum(x => x.Product.Rate * (x.ConversionValue ?? x.Quantity)) + JobSheetItemList.Where(x => x.Product.SKUMasterId == SKU.SKUMasterId).Sum(x => x.Product.NetAmount.Value * (x.ConversionValue ?? x.Quantity));
                        if (SKUMasterItem.OnWards.Count > 0)
                        {
                            SKUMasterItem.OnWards.Rate = SKUMasterItem.OnWards.Value / SKUMasterItem.OnWards.Count;
                        }
                        else
                        {
                            SKUMasterItem.OnWards.Value = 0;
                        }

                        ViewModel.TotalOutWardsValue = ViewModel.TotalOutWardsValue + SKUMasterItem.OnWards.Value;
                        subCategoryList.OnWardsCountTotal = subCategoryList.OnWardsCountTotal + SKUMasterItem.OnWards.Count;
                        subCategoryList.OnWardsValueTotal = subCategoryList.OnWardsValueTotal + SKUMasterItem.OnWards.Value;
                        if (subCategoryList.OnWardsCountTotal != 0)
                        {
                            subCategoryList.OnWardsRateTotal = subCategoryList.OnWardsValueTotal / subCategoryList.OnWardsCountTotal;
                        }

                        SKUMasterItem.ClosingBalance = new ProductQuantity
                        {
                            Count = SKUMasterItem.InWards.Count - SKUMasterItem.OnWards.Count,
                            Rate = SKUMasterItem.InWards.Rate,
                            Value = (SKUMasterItem.InWards.Count - SKUMasterItem.OnWards.Count) * SKUMasterItem.InWards.Rate,
                        };

                        subCategoryList.ClosingBalanceCountTotal = subCategoryList.ClosingBalanceCountTotal + SKUMasterItem.ClosingBalance.Count;
                        subCategoryList.ClosingBalanceValueTotal = subCategoryList.ClosingBalanceValueTotal + SKUMasterItem.ClosingBalance.Value;
                        subCategoryList.SKUMasterList.Add(SKUMasterItem);
                    }
                    if (subCategoryList.ClosingBalanceCountTotal > 0)
                    {
                        ViewModel.Count++;
                        subCategoryList.Count = ViewModel.Count;
                        subCategoryList.ClosingBalanceRateTotal = subCategoryList.ClosingBalanceValueTotal / subCategoryList.ClosingBalanceCountTotal;
                        Cateogory.SubCategoryList.Add(subCategoryList);
                    }
                }
                ViewModel.CateogoryList.Add(Cateogory);
            }
            ViewModel.ClosingBalance = (ViewModel.TotalInWardsValue ?? 0) - (ViewModel.TotalOutWardsValue ?? 0);
            ViewModel.Branch = SessionManager.GetSessionBranch();
            ViewModel.Company = SessionManager.GetSessionCompany();
            return View(ViewModel);
        }

        [HttpPost]
        public ActionResult _ConsolidatedReport(int LocationId, DateTime? FromDate, DateTime? ToDate)
        {
            InventoryReportViewModel model = new InventoryReportViewModel();
            List<PurchaseInvoiceItem> PurchaseInvoiceItemList = new List<PurchaseInvoiceItem>();
            List<SaleInvoiceItem> SaleInvoiceItemList = new List<SaleInvoiceItem>();
            List<JobSheetItem> JobSheetItemList = new List<JobSheetItem>();
            if (LocationId > 0)
            {
                PurchaseInvoiceItemList = db.PurchaseInvoiceItems.Where(x => x.LocationId == LocationId && x.IsProductCreated == true && x.CompanyId == db.CompanyId).ToList();
                SaleInvoiceItemList = db.SaleInvoiceItems.Where(x => x.Product.LocationId == LocationId && x.Status == Constants.SALE_ITEM_SOLD && x.Product.CompanyId == db.CompanyId).ToList();
                JobSheetItemList = db.JobSheetItems.Where(x => x.Product.LocationId == LocationId && x.Product.CompanyId == db.CompanyId).ToList();
            }
            else
            {
                PurchaseInvoiceItemList = db.PurchaseInvoiceItems.Where(x => x.IsProductCreated == true && x.CompanyId == db.CompanyId).ToList();
                SaleInvoiceItemList = db.SaleInvoiceItems.Where(x => x.Status == Constants.SALE_ITEM_SOLD && x.Product.CompanyId == db.CompanyId).ToList();
                JobSheetItemList = db.JobSheetItems.Where(x => x.Product.CompanyId == db.CompanyId).ToList();
            }

            List<int> PurchaseInvoiceItemIds = PurchaseInvoiceItemList.Select(x => x.PurchaseInvoiceItemId).ToList();
            List<Product> ProductList = db.Products.Where(x => PurchaseInvoiceItemIds.Contains(x.PurchaseInvoiceItemId)).ToList();

            if (FromDate.HasValue)
            {
                List<PurchaseInvoiceItem> ProductionItemList = PurchaseInvoiceItemList.Where(x => x.JobsheetId != null).ToList();
                List<int?> ItemIds = ProductionItemList.Select(x => x.JobsheetId).ToList();
                List<JobSheet> TempSheetList = db.JobSheets.Where(x => ItemIds.Contains(x.JobSheetId)).ToList();
                foreach (var item in ProductionItemList)
                {
                    item.JobSheet = TempSheetList.Where(x => x.JobSheetId == item.JobsheetId).FirstOrDefault();
                }
                List<PurchaseInvoiceItem> InvoiceItemList = PurchaseInvoiceItemList.Where(x => x.PurchaseInvoice != null).ToList();
                PurchaseInvoiceItemList = new List<PurchaseInvoiceItem>();
                PurchaseInvoiceItemList.AddRange(ProductionItemList.Where(x => x.JobSheet.JobDate >= FromDate).ToList());
                PurchaseInvoiceItemList.AddRange(InvoiceItemList.Where(x => x.PurchaseInvoice.EntryDate >= FromDate).ToList());
                List<int> ProdIds = PurchaseInvoiceItemList.Select(x => x.PurchaseInvoiceItemId).ToList();
                ProductList = ProductList.Where(x => ProdIds.Contains(x.PurchaseInvoiceItemId)).ToList();
                ProdIds = ProductList.Select(x => x.ProductId).ToList();
                SaleInvoiceItemList = SaleInvoiceItemList.Where(x => ProdIds.Contains(x.ProductId)).ToList();
                JobSheetItemList = JobSheetItemList.Where(x => ProdIds.Contains(x.ProductId)).ToList();
            }

            if (ToDate.HasValue)
            {
                SaleInvoiceItemList = SaleInvoiceItemList.Where(x => x.SaleInvoice != null).ToList();
                JobSheetItemList = JobSheetItemList.Where(x => x.JobSheet != null).ToList();

                JobSheetItemList = JobSheetItemList.Where(x => x.JobSheet.CreatedDate <= ToDate).ToList();
                SaleInvoiceItemList = SaleInvoiceItemList.Where(x => x.SaleInvoice.InvoicingDate <= ToDate).ToList();

            }

            List<int> SKUIds = PurchaseInvoiceItemList.Select(x => x.SKUMasterId).ToList();
            List<SKUMaster> SKUList = db.SKUMasters.Where(x => SKUIds.Contains(x.SKUMasterId)).ToList();
            List<int> SubCategoryIds = SKUList.Select(x => x.SubCategoryId).ToList();
            List<SubCategory> SubCategoryList = db.SubCategories.Where(x => SubCategoryIds.Contains(x.SubCategoryId)).ToList();
            List<int> CategoryIds = SKUList.Select(x => x.CategoryId).ToList();
            List<Category> CategoryList = db.Categories.Where(x => CategoryIds.Contains(x.CategoryId)).ToList();
            List<int> UnitIds = PurchaseInvoiceItemList.Select(x => x.UnitId).ToList();
            List<Unit> UnitList = db.UnitMasters.Where(x => UnitIds.Contains(x.UnitId)).ToList();
            foreach (var item in CategoryList)
            {
                CateogoryList Cateogory = new CateogoryList
                {
                    CategoryName = item.Name,
                    SubCategoryList = new List<SubCategoryList>()
                };
                foreach (var Subcategory in SubCategoryList.Where(x => x.CategoryId == item.CategoryId))
                {
                    SubCategoryList subCategoryList = new SubCategoryList
                    {
                        SubCategoryName = Subcategory.Name,
                        SKUMasterList = new List<SKUMasterList>()
                    };
                    foreach (var SKU in SKUList.Where(x => x.CategoryId == item.CategoryId && x.SubCategoryId == Subcategory.SubCategoryId))
                    {
                        SKUMasterList SKUMasterItem = new SKUMasterList();
                        SKUMasterItem.SKUName = SKU.SKU;
                        SKUMasterItem.SKUMasterId = SKU.SKUMasterId;
                        SKUMasterItem.Unit = UnitList.Where(x => x.UnitId == PurchaseInvoiceItemList.Where(y => y.SKUMasterId == SKU.SKUMasterId).Select(y => y.UnitId).FirstOrDefault()).Select(x => x.UnitValue).FirstOrDefault();

                        SKUMasterItem.InWards = new ProductQuantity();
                        SKUMasterItem.InWards.Count = PurchaseInvoiceItemList.Where(x => x.SKUMasterId == SKU.SKUMasterId).Sum(x => x.Quantity);
                        SKUMasterItem.InWards.Value = PurchaseInvoiceItemList.Where(x => x.SKUMasterId == SKU.SKUMasterId).Sum(x => x.Rate * x.Quantity);
                        if (SKUMasterItem.InWards.Count > 0)
                        {
                            SKUMasterItem.InWards.Rate = SKUMasterItem.InWards.Value / SKUMasterItem.InWards.Count;
                        }
                        else
                        {
                            SKUMasterItem.InWards.Value = 0;
                        }

                        model.TotalInWardsValue = model.TotalInWardsValue + SKUMasterItem.InWards.Value;
                        subCategoryList.InWardsCountTotal = subCategoryList.InWardsCountTotal + SKUMasterItem.InWards.Count;
                        subCategoryList.InWardsValueTotal = subCategoryList.InWardsValueTotal + SKUMasterItem.InWards.Value;
                        if (subCategoryList.InWardsCountTotal != 0)
                        {
                            subCategoryList.InWardsRateTotal = subCategoryList.InWardsValueTotal / subCategoryList.InWardsCountTotal;
                        }


                        SKUMasterItem.OnWards = new ProductQuantity();
                        SKUMasterItem.OnWards.Count = SaleInvoiceItemList.Where(x => x.SKUMasterId == SKU.SKUMasterId).Sum(x => x.ConversionValue ?? x.Quantity) + JobSheetItemList.Where(x => x.Product.SKUMasterId == SKU.SKUMasterId).Sum(x => x.ConversionValue ?? x.Quantity);
                        SKUMasterItem.OnWards.Value = SaleInvoiceItemList.Where(x => x.SKUMasterId == SKU.SKUMasterId).Sum(x => x.Product.Rate * (x.ConversionValue ?? x.Quantity)) + JobSheetItemList.Where(x => x.Product.SKUMasterId == SKU.SKUMasterId).Sum(x => x.Product.NetAmount.Value * (x.ConversionValue ?? x.Quantity));
                        if (SKUMasterItem.OnWards.Count > 0)
                        {
                            SKUMasterItem.OnWards.Rate = SKUMasterItem.OnWards.Value / SKUMasterItem.OnWards.Count;
                        }
                        else
                        {
                            SKUMasterItem.OnWards.Value = 0;
                        }

                        model.TotalOutWardsValue = model.TotalOutWardsValue + SKUMasterItem.OnWards.Value;
                        subCategoryList.OnWardsCountTotal = subCategoryList.OnWardsCountTotal + SKUMasterItem.OnWards.Count;
                        subCategoryList.OnWardsValueTotal = subCategoryList.OnWardsValueTotal + SKUMasterItem.OnWards.Value;
                        if (subCategoryList.OnWardsCountTotal != 0)
                        {
                            subCategoryList.OnWardsRateTotal = subCategoryList.OnWardsValueTotal / subCategoryList.OnWardsCountTotal;
                        }

                        SKUMasterItem.ClosingBalance = new ProductQuantity
                        {
                            Count = SKUMasterItem.InWards.Count - SKUMasterItem.OnWards.Count,
                            Rate = SKUMasterItem.InWards.Rate,
                            Value = (SKUMasterItem.InWards.Count - SKUMasterItem.OnWards.Count) * SKUMasterItem.InWards.Rate,
                        };

                        subCategoryList.ClosingBalanceCountTotal = subCategoryList.ClosingBalanceCountTotal + SKUMasterItem.ClosingBalance.Count;
                        subCategoryList.ClosingBalanceValueTotal = subCategoryList.ClosingBalanceValueTotal + SKUMasterItem.ClosingBalance.Value;
                        subCategoryList.SKUMasterList.Add(SKUMasterItem);
                    }
                    if (subCategoryList.ClosingBalanceCountTotal > 0)
                    {
                        model.Count++;
                        subCategoryList.Count = model.Count;
                        subCategoryList.ClosingBalanceRateTotal = subCategoryList.ClosingBalanceValueTotal / subCategoryList.ClosingBalanceCountTotal;
                        Cateogory.SubCategoryList.Add(subCategoryList);
                    }
                }
                model.CateogoryList.Add(Cateogory);
            }
            model.ClosingBalance = (model.TotalInWardsValue ?? 0) - (model.TotalOutWardsValue ?? 0);
            model.Branch = SessionManager.GetSessionBranch();
            model.Company = SessionManager.GetSessionCompany();
            return PartialView(model);
        }

        public ActionResult CustomeReport()
        {
            CustomInventoryReportModal model = new CustomInventoryReportModal();
            model.CategoryList = DropdownManager.GetCategoryList(null);
            model.SubcategoryList = DropdownManager.GetSubCategoryList(null);
            model.SKUList = DropdownManager.GetSKUList(0);
            model.BrandList = DropdownManager.GetBrandList(null);
            model.LocationList = DropdownManager.GetLocationByBranch(SessionManager.GetSessionBranch().BranchId);

            model.CategoryList.Remove(model.CategoryList.Where(x => x.Value == "-1").FirstOrDefault());
            model.SubcategoryList.Remove(model.SubcategoryList.Where(x => x.Value == "-1").FirstOrDefault());
            model.SKUList.Remove(model.SKUList.Where(x => x.Value == "-1").FirstOrDefault());
            model.BrandList.Remove(model.BrandList.Where(x => x.Value == "-1").FirstOrDefault());
            model.LocationList.Remove(model.LocationList.Where(x => x.Value == "-1").FirstOrDefault());

            Dictionary<AttributeSet, List<SelectListItem>> AttributeDictionayList = null;
            model.AttributeSetListNew = db.GetAttributeSets();
            AttributeDictionayList = new Dictionary<AttributeSet, List<SelectListItem>>();
            for (int i = 0; i < model.AttributeSetListNew.Count; i++)
            {
                int Id = model.AttributeSetListNew[i].AttributeSetId;
                model.AttributeSetListNew[i].AttributeValueList = DropdownManager.GetAttributeValueList(Id);
                AttributeDictionayList.Add(model.AttributeSetListNew[i], model.AttributeSetListNew[i].AttributeValueList);
            }
            model.AttributeDictionayList = AttributeDictionayList;
            return View(model);
        }

        [HttpPost]
        public ActionResult CustomeReport(CustomInventoryReportModal model)
        {
            model.InventoryReportViewModel = new InventoryReportViewModel();
            List<Product> ProductList = new List<Product>();
            List<Brand> BrandList = db.Brands.ToList();
            List<PurchaseInvoiceItem> PurchaseInvoiceItemList = new List<PurchaseInvoiceItem>();
            List<SaleInvoiceItem> SaleInvoiceItemList = new List<SaleInvoiceItem>();
            List<JobSheetItem> JobSheetItemList = new List<JobSheetItem>();
            List<SKUMaster> SKUList = new List<SKUMaster>();
            List<int> SKUIds = new List<int>();
            if (model.SKUIds != null)
            {
                SKUList = db.SKUMasters.Where(p => model.SKUIds.Contains(p.SKUMasterId)).ToList();
                SKUIds = SKUList.Select(x => x.SKUMasterId).ToList();
            }
            else if (model.SubCategoryIds != null)
            {
                SKUList = db.SKUMasters.Where(p => model.SubCategoryIds.Contains(p.SubCategoryId)).ToList();
                SKUIds = SKUList.Select(x => x.SKUMasterId).ToList();
            }
            else if (model.CategoryIds != null)
            {
                SKUList = db.SKUMasters.Where(p => model.CategoryIds.Contains(p.CategoryId)).ToList();
                SKUIds = SKUList.Select(x => x.SKUMasterId).ToList();
            }
            else
            {
                SKUIds = db.Products.Select(x => x.SKUMasterId).ToList();
                SKUList = db.SKUMasters.Where(x => SKUIds.Contains(x.SKUMasterId)).ToList();
            }

            List<int> PurchaseInvoiceItemIds = new List<int>();
            List<string> AttributeSet = model.AttributeSetListNew.Select(x => x.ApplicableValues).ToList();
            AttributeSet = AttributeSet.Where(x => x != "-1").ToList();
            if (AttributeSet.Count > 0)
            {
                List<AttributeLinking> AttributeList = db.AttributeLinkings.ToList();
                List<int> AttributeIds = new List<int>();
                foreach (var item in AttributeList)
                {
                    bool IsContainAllElement = true;
                    item.ValuesList = item.Value.Split(',').ToList();
                    foreach (var Value in AttributeSet)
                    {
                        if (!item.ValuesList.Contains(Value))
                        {
                            IsContainAllElement = false;
                            break;
                        }
                    }
                    if (IsContainAllElement == true)
                    {
                        AttributeIds.Add(item.AttributeLinkingId);
                        PurchaseInvoiceItemIds.Add(item.PurchaseInvoiceItemId ?? 0);
                    }
                }

                if (model.BrandIds != null)
                {
                    PurchaseInvoiceItemList = db.PurchaseInvoiceItems.Where(x => SKUIds.Contains(x.SKUMasterId) && x.IsProductCreated == true && x.CompanyId == db.CompanyId && PurchaseInvoiceItemIds.Contains(x.PurchaseInvoiceItemId) && model.BrandIds.Contains(x.BrandId)).ToList();
                    ProductList = db.Products.Where(x => SKUIds.Contains(x.SKUMasterId) && model.BrandIds.Contains(x.BrandId) && PurchaseInvoiceItemIds.Contains(x.PurchaseInvoiceItemId) && AttributeIds.Contains(x.AttributeLinkingId ?? 0)).ToList();
                }
                else
                {
                    PurchaseInvoiceItemList = db.PurchaseInvoiceItems.Where(x => SKUIds.Contains(x.SKUMasterId) && x.IsProductCreated == true && x.CompanyId == db.CompanyId && PurchaseInvoiceItemIds.Contains(x.PurchaseInvoiceItemId)).ToList();
                    ProductList = db.Products.Where(x => SKUIds.Contains(x.SKUMasterId) && PurchaseInvoiceItemIds.Contains(x.PurchaseInvoiceItemId) && AttributeIds.Contains(x.AttributeLinkingId ?? 0)).ToList();
                }

                foreach (var item in PurchaseInvoiceItemList)
                {
                    item.Quantity = ProductList.Where(x => x.PurchaseInvoiceItemId == item.PurchaseInvoiceItemId).Sum(x => x.Quantity);
                }
                List<int> ProductIds = ProductList.Select(x => x.ProductId).ToList();
                SaleInvoiceItemList = db.SaleInvoiceItems.Where(x => ProductIds.Contains(x.ProductId) && x.Status == Constants.SALE_ITEM_SOLD && x.Product.CompanyId == db.CompanyId).ToList();
            }
            else
            {
                if (model.BrandIds != null)
                {
                    ProductList = db.Products.Where(x => SKUIds.Contains(x.SKUMasterId) && model.BrandIds.Contains(x.BrandId) && x.CompanyId == db.CompanyId).ToList();
                    PurchaseInvoiceItemList = db.PurchaseInvoiceItems.Where(x => SKUIds.Contains(x.SKUMasterId) && x.CompanyId == db.CompanyId && model.BrandIds.Contains(x.BrandId)).ToList();
                    SaleInvoiceItemList = db.SaleInvoiceItems.Where(x => SKUIds.Contains(x.SKUMasterId.Value) && x.Status == Constants.SALE_ITEM_SOLD && x.Product.CompanyId == db.CompanyId && model.BrandIds.Contains(x.Product.BrandId)).ToList();
                    JobSheetItemList = db.JobSheetItems.Where(x => SKUIds.Contains(x.Product.SKUMasterId) && x.Product.CompanyId == db.CompanyId && model.BrandIds.Contains(x.Product.BrandId)).ToList();
                }
                else
                {
                    ProductList = db.Products.Where(x => SKUIds.Contains(x.SKUMasterId) && x.CompanyId == db.CompanyId).ToList();
                    PurchaseInvoiceItemList = db.PurchaseInvoiceItems.Where(x => SKUIds.Contains(x.SKUMasterId) && x.IsProductCreated == true && x.CompanyId == db.CompanyId).ToList();
                    SaleInvoiceItemList = db.SaleInvoiceItems.Where(x => SKUIds.Contains(x.SKUMasterId.Value) && x.Status == Constants.SALE_ITEM_SOLD && x.Product.CompanyId == db.CompanyId).ToList();
                    JobSheetItemList = db.JobSheetItems.Where(x => SKUIds.Contains(x.Product.SKUMasterId) && x.Product.CompanyId == db.CompanyId).ToList();
                }


            }

            if (model.LocationIds != null)
            {
                ProductList = ProductList.Where(x => model.LocationIds.Contains(x.LocationId)).ToList();
                PurchaseInvoiceItemList = PurchaseInvoiceItemList.Where(x => model.LocationIds.Contains(x.LocationId)).ToList();
                SaleInvoiceItemList = SaleInvoiceItemList.Where(x => x.SaleInvoice != null).ToList();
                SaleInvoiceItemList = SaleInvoiceItemList.Where(x => model.LocationIds.Contains(x.SaleInvoice.LocationId)).ToList();
                JobSheetItemList = JobSheetItemList.Where(x => model.LocationIds.Contains(x.Product.LocationId)).ToList();
            }



            List<PurchaseInvoiceItem> ProductionItemList = PurchaseInvoiceItemList.Where(x => x.JobsheetId != null).ToList();
            List<int?> ItemIds = ProductionItemList.Select(x => x.JobsheetId).ToList();
            List<JobSheet> TempSheetList = db.JobSheets.Where(x => ItemIds.Contains(x.JobSheetId)).ToList();
            foreach (var item in ProductionItemList)
            {
                item.JobSheet = TempSheetList.Where(x => x.JobSheetId == item.JobsheetId).FirstOrDefault();
            }
            List<PurchaseInvoiceItem> InvoiceItemList = PurchaseInvoiceItemList.Where(x => x.PurchaseInvoice != null).ToList();
            PurchaseInvoiceItemList = new List<PurchaseInvoiceItem>();

            if (model.ToDate.HasValue)
            {
                ProductionItemList = ProductionItemList.Where(x => x.JobSheet.JobDate <= model.ToDate.Value).ToList();
                InvoiceItemList = InvoiceItemList.Where(x => x.PurchaseInvoice.EntryDate <= model.ToDate.Value).ToList();

                PurchaseInvoiceItemList.AddRange(ProductionItemList.ToList());
                PurchaseInvoiceItemList.AddRange(InvoiceItemList.ToList());
            }
            else
            {
                PurchaseInvoiceItemList.AddRange(ProductionItemList.ToList());
                PurchaseInvoiceItemList.AddRange(InvoiceItemList.ToList());
            }

            if (model.FromDate.HasValue)
            {
                List<int> ProdIds = PurchaseInvoiceItemList.Select(x => x.PurchaseInvoiceItemId).ToList();
                ProductList = ProductList.Where(x => ProdIds.Contains(x.PurchaseInvoiceItemId)).ToList();
                ProdIds = ProductList.Select(x => x.ProductId).ToList();
                SaleInvoiceItemList = SaleInvoiceItemList.Where(x => ProdIds.Contains(x.ProductId)).ToList();
                JobSheetItemList = JobSheetItemList.Where(x => ProdIds.Contains(x.ProductId)).ToList();

                SaleInvoiceItemList = SaleInvoiceItemList.Where(x => x.SaleInvoice != null).ToList();
                JobSheetItemList = JobSheetItemList.Where(x => x.JobSheet != null).ToList();

                JobSheetItemList = JobSheetItemList.Where(x => x.JobSheet.JobDate >= model.FromDate).ToList();
                SaleInvoiceItemList = SaleInvoiceItemList.Where(x => x.SaleInvoice.InvoicingDate >= model.FromDate).ToList();
            }

            if (model.ToDate.HasValue)
            {
                SaleInvoiceItemList = SaleInvoiceItemList.Where(x => x.SaleInvoice != null).ToList();
                JobSheetItemList = JobSheetItemList.Where(x => x.JobSheet != null).ToList();

                JobSheetItemList = JobSheetItemList.Where(x => x.JobSheet.JobDate <= model.ToDate).ToList();
                SaleInvoiceItemList = SaleInvoiceItemList.Where(x => x.SaleInvoice.InvoicingDate <= model.ToDate).ToList();
            }

            if (model.MRPFrom.HasValue)
            {
                ProductList = ProductList.Where(x => x.MRP >= model.MRPFrom).ToList();
                PurchaseInvoiceItemList = PurchaseInvoiceItemList.Where(x => x.MRP >= model.MRPFrom).ToList();
                SaleInvoiceItemList = SaleInvoiceItemList.Where(x => x.Product.MRP >= model.MRPFrom).ToList();
                JobSheetItemList = JobSheetItemList.Where(x => x.Product.MRP >= model.MRPFrom).ToList();
            }

            if (model.MRPTo.HasValue)
            {
                ProductList = ProductList.Where(x => x.MRP <= model.MRPTo).ToList();
                PurchaseInvoiceItemList = PurchaseInvoiceItemList.Where(x => x.MRP <= model.MRPTo).ToList();
                SaleInvoiceItemList = SaleInvoiceItemList.Where(x => x.Product.MRP <= model.MRPTo).ToList();
                JobSheetItemList = JobSheetItemList.Where(x => x.Product.MRP <= model.MRPTo).ToList();
            }

            model.SKUMasterList = new SKUMasterList();
            model.SKUMasterList.ProductList = ProductList;
            model.SKUMasterList.AttributeSetList = model.AttributeSetListNew;
            List<int> SubCategoryIds = SKUList.Select(x => x.SubCategoryId).ToList();
            List<SubCategory> SubCategoryList = db.SubCategories.Where(x => SubCategoryIds.Contains(x.SubCategoryId)).ToList();
            List<int> CategoryIds = SKUList.Select(x => x.CategoryId).ToList();
            List<Category> CategoryList = db.Categories.Where(x => CategoryIds.Contains(x.CategoryId)).ToList();

            List<int> PurchaseInvoiceIds = PurchaseInvoiceItemList.Select(x => x.PurchaseInvoiceId ?? 0).ToList();
            List<PurchaseInvoice> PurchaseInvoiceList = db.PurchaseInvoices.Where(x => PurchaseInvoiceIds.Contains(x.PurchaseInvoiceId) && x.CompanyId == db.CompanyId).ToList();

            List<int> SaleInvoiceIds = SaleInvoiceItemList.Select(x => x.SaleInvoiceId ?? 0).ToList();
            List<SaleInvoice> SaleInvoiceList = db.SaleInvoices.Where(x => SaleInvoiceIds.Contains(x.SaleInvoiceId) && x.CompanyId == db.CompanyId).ToList();

            List<int> JobSheetIds = JobSheetItemList.Select(x => x.JobSheetId ?? 0).ToList();
            JobSheetIds.AddRange(PurchaseInvoiceItemList.Select(x => x.JobsheetId ?? 0).ToList());
            List<JobSheet> JobSheetList = db.JobSheets.Where(x => JobSheetIds.Contains(x.JobSheetId) && x.CompanyId == db.CompanyId).ToList();

            List<int> AttributeLinkingIds = model.SKUMasterList.ProductList.Select(x => x.AttributeLinkingId ?? 0).ToList();
            List<AttributeLinking> AttributeLinkingList = db.AttributeLinkings.Where(x => AttributeLinkingIds.Contains(x.AttributeLinkingId)).ToList();

            List<int> UnitIds = PurchaseInvoiceItemList.Select(x => x.UnitId).ToList();
            List<Unit> UnitList = db.UnitMasters.Where(x => UnitIds.Contains(x.UnitId)).ToList();

            foreach (var item in ProductList)
            {
                item.AttributeLinking = AttributeLinkingList.Where(x => x.AttributeLinkingId == item.AttributeLinkingId).FirstOrDefault();
                if (item.AttributeLinking != null)
                {
                    item.AttributeList = item.AttributeLinking.Value.Split(',').ToList();
                }
                PurchaseInvoiceItem PurchaseInvoiceItem = PurchaseInvoiceItemList.Where(x => x.PurchaseInvoiceItemId == item.PurchaseInvoiceItemId).FirstOrDefault();
                if (PurchaseInvoiceItem != null)
                {
                    item.PurchaseInvoiceNumber = PurchaseInvoiceList.Where(x => x.PurchaseInvoiceId == PurchaseInvoiceItem.PurchaseInvoiceId).Select(x => x.PurchaseInvoiceNo).FirstOrDefault();
                    item.PurchaseInvoiceId = PurchaseInvoiceList.Where(x => x.PurchaseInvoiceId == PurchaseInvoiceItem.PurchaseInvoiceId).Select(x => x.PurchaseInvoiceId).FirstOrDefault();
                }
                SaleInvoiceIds = SaleInvoiceItemList.Where(x => x.ProductId == item.ProductId).Select(x => x.SaleInvoiceId ?? 0).ToList();
                item.SaleInvoiceList = SaleInvoiceList.Where(x => SaleInvoiceIds.Contains(x.SaleInvoiceId)).ToList();
                item.Brand = BrandList.Where(x => x.BrandId == item.BrandId).FirstOrDefault();
            }


            foreach (var item in CategoryList)
            {
                CateogoryList Cateogory = new CateogoryList
                {
                    CategoryName = item.Name,
                    SubCategoryList = new List<SubCategoryList>()
                };
                foreach (var Subcategory in SubCategoryList.Where(x => x.CategoryId == item.CategoryId))
                {
                    SubCategoryList subCategoryList = new SubCategoryList
                    {
                        SubCategoryName = Subcategory.Name,
                        SKUMasterList = new List<SKUMasterList>()
                    };
                    foreach (var SKU in SKUList.Where(x => x.CategoryId == item.CategoryId && x.SubCategoryId == Subcategory.SubCategoryId))
                    {
                        SKUMasterList SKUMasterItem = new SKUMasterList();
                        SKUMasterItem.SKUName = SKU.SKU;
                        SKUMasterItem.SKUMasterId = SKU.SKUMasterId;
                        SKUMasterItem.Unit = UnitList.Where(x => x.UnitId == PurchaseInvoiceItemList.Where(y => y.SKUMasterId == SKU.SKUMasterId).Select(y => y.UnitId).FirstOrDefault()).Select(x => x.UnitValue).FirstOrDefault();

                        SKUMasterItem.OpeningBalance = new ProductQuantity();
                        SKUMasterItem.InWards = new ProductQuantity();

                        if (model.FromDate.HasValue)
                        {

                            SKUMasterItem.OpeningBalance.Count = PurchaseInvoiceItemList.Where(x => x.SKUMasterId == SKU.SKUMasterId && (CompareJobSheetDateIsGreater(x.JobSheet, model.FromDate.Value) || ComparePurchaseDateIsGreater(x.PurchaseInvoice, model.FromDate.Value))).Sum(x => x.Quantity);
                            SKUMasterItem.OpeningBalance.Value = PurchaseInvoiceItemList.Where(x => x.SKUMasterId == SKU.SKUMasterId && (CompareJobSheetDateIsGreater(x.JobSheet, model.FromDate.Value) || ComparePurchaseDateIsGreater(x.PurchaseInvoice, model.FromDate.Value))).Sum(x => x.Rate * x.Quantity);

                            SKUMasterItem.InWards.Count = PurchaseInvoiceItemList.Where(x => x.SKUMasterId == SKU.SKUMasterId && (CompareJobSheetDateIsless(x.JobSheet, model.FromDate.Value) || ComparePurchaseDateIsLess(x.PurchaseInvoice, model.FromDate.Value))).Sum(x => x.Quantity);
                            SKUMasterItem.InWards.Value = PurchaseInvoiceItemList.Where(x => x.SKUMasterId == SKU.SKUMasterId && (CompareJobSheetDateIsless(x.JobSheet, model.FromDate.Value) || ComparePurchaseDateIsLess(x.PurchaseInvoice, model.FromDate.Value))).Sum(x => x.Rate * x.Quantity);
                        }
                        else
                        {
                            SKUMasterItem.OpeningBalance.Count = PurchaseInvoiceItemList.Where(x => x.SKUMasterId == SKU.SKUMasterId).Sum(x => x.Quantity);
                            SKUMasterItem.OpeningBalance.Value = PurchaseInvoiceItemList.Where(x => x.SKUMasterId == SKU.SKUMasterId).Sum(x => x.Rate * x.Quantity);
                        }

                        if (SKUMasterItem.OpeningBalance.Count > 0)
                        {
                            SKUMasterItem.OpeningBalance.Rate = SKUMasterItem.OpeningBalance.Value / SKUMasterItem.OpeningBalance.Count;
                            SKUMasterItem.OpeningBalance.MRP = PurchaseInvoiceItemList.Where(x => x.SKUMasterId == SKU.SKUMasterId).Average(x => x.MRP);
                        }
                        else
                        {
                            SKUMasterItem.OpeningBalance.Value = 0;
                            SKUMasterItem.OpeningBalance.MRP = 0;
                        }

                        if (SKUMasterItem.InWards.Count > 0)
                        {
                            SKUMasterItem.InWards.Rate = SKUMasterItem.InWards.Value / SKUMasterItem.InWards.Count;
                            SKUMasterItem.InWards.MRP = PurchaseInvoiceItemList.Where(x => x.SKUMasterId == SKU.SKUMasterId).Average(x => x.MRP);
                        }
                        else
                        {
                            SKUMasterItem.InWards.Value = 0;
                            SKUMasterItem.InWards.MRP = 0;
                        }

                        model.InventoryReportViewModel.TotalInWardsValue = model.InventoryReportViewModel.TotalInWardsValue + SKUMasterItem.InWards.Value;
                        subCategoryList.InWardsCountTotal = subCategoryList.InWardsCountTotal + SKUMasterItem.InWards.Count;
                        subCategoryList.InWardsValueTotal = subCategoryList.InWardsValueTotal + SKUMasterItem.InWards.Value;
                        if (subCategoryList.InWardsCountTotal != 0)
                        {
                            subCategoryList.InWardsRateTotal = subCategoryList.InWardsValueTotal / subCategoryList.InWardsCountTotal;
                        }

                        subCategoryList.OpeningCountTotal = subCategoryList.OpeningCountTotal + SKUMasterItem.OpeningBalance.Count;
                        subCategoryList.OpeningValueTotal = subCategoryList.OpeningValueTotal + SKUMasterItem.OpeningBalance.Value;
                        if (subCategoryList.OpeningCountTotal != 0)
                        {
                            subCategoryList.OpeningRateTotal = subCategoryList.OpeningValueTotal / subCategoryList.OpeningCountTotal;
                        }

                        SKUMasterItem.OnWards = new ProductQuantity();
                        SKUMasterItem.OnWards.Count = SaleInvoiceItemList.Where(x => x.SKUMasterId == SKU.SKUMasterId).Sum(x => x.ConversionValue ?? x.Quantity) + JobSheetItemList.Where(x => x.Product.SKUMasterId == SKU.SKUMasterId).Sum(x => x.ConversionValue ?? x.Quantity);
                        SKUMasterItem.OnWards.Value = SaleInvoiceItemList.Where(x => x.SKUMasterId == SKU.SKUMasterId).Sum(x => x.Product.Rate * (x.ConversionValue ?? x.Quantity)) + JobSheetItemList.Where(x => x.Product.SKUMasterId == SKU.SKUMasterId).Sum(x => x.Product.NetAmount.Value * (x.ConversionValue ?? x.Quantity));
                        if (SKUMasterItem.OnWards.Count > 0)
                        {
                            SKUMasterItem.OnWards.Rate = SKUMasterItem.OnWards.Value / SKUMasterItem.OnWards.Count;
                        }
                        else
                        {
                            SKUMasterItem.OnWards.Value = 0;
                        }


                        model.InventoryReportViewModel.TotalOutWardsValue = model.InventoryReportViewModel.TotalOutWardsValue + SKUMasterItem.OnWards.Value;
                        subCategoryList.OnWardsCountTotal = subCategoryList.OnWardsCountTotal + SKUMasterItem.OnWards.Count;
                        subCategoryList.OnWardsValueTotal = subCategoryList.OnWardsValueTotal + SKUMasterItem.OnWards.Value;
                        if (subCategoryList.OnWardsCountTotal != 0)
                        {
                            subCategoryList.OnWardsRateTotal = subCategoryList.OnWardsValueTotal / subCategoryList.OnWardsCountTotal;
                        }

                        SKUMasterItem.ClosingBalance = new ProductQuantity();

                        SKUMasterItem.ClosingBalance.Count = (SKUMasterItem.OpeningBalance.Count + SKUMasterItem.InWards.Count) - SKUMasterItem.OnWards.Count;
                        if (SKUMasterItem.InWards.Rate > 0)
                        {
                            SKUMasterItem.ClosingBalance.Rate = SKUMasterItem.InWards.Rate;
                        }
                        else
                        {
                            SKUMasterItem.ClosingBalance.Rate = SKUMasterItem.OpeningBalance.Rate;
                        }
                        if (SKUMasterItem.InWards.MRP > 0)
                        {
                            SKUMasterItem.OnWards.MRP = SKUMasterItem.InWards.MRP;
                            SKUMasterItem.ClosingBalance.MRP = SKUMasterItem.InWards.MRP;
                        }
                        else
                        {
                            SKUMasterItem.OnWards.MRP = SKUMasterItem.OpeningBalance.MRP;
                            SKUMasterItem.ClosingBalance.MRP = SKUMasterItem.OpeningBalance.MRP;
                        }


                        
                        SKUMasterItem.ClosingBalance.Value = SKUMasterItem.ClosingBalance.Count * SKUMasterItem.ClosingBalance.Rate;

                        model.InventoryReportViewModel.TotalOpeningValue = model.InventoryReportViewModel.TotalOpeningValue + SKUMasterItem.OpeningBalance.Value;
                        subCategoryList.ClosingBalanceCountTotal = subCategoryList.ClosingBalanceCountTotal + SKUMasterItem.ClosingBalance.Count;
                        subCategoryList.ClosingBalanceValueTotal = subCategoryList.ClosingBalanceValueTotal + SKUMasterItem.ClosingBalance.Value;
                        if (SKUMasterItem.InWards.Count > 0 || SKUMasterItem.OpeningBalance.Count > 0)
                        {
                            subCategoryList.SKUMasterList.Add(SKUMasterItem);
                        }
                    }
                    if (subCategoryList.ClosingBalanceCountTotal > 0)
                    {
                        model.InventoryReportViewModel.Count++;
                        subCategoryList.Count = model.InventoryReportViewModel.Count;
                        subCategoryList.ClosingBalanceRateTotal = subCategoryList.ClosingBalanceValueTotal / subCategoryList.ClosingBalanceCountTotal;
                        Cateogory.SubCategoryList.Add(subCategoryList);
                    }
                    else if (subCategoryList.OpeningCountTotal > 0 || subCategoryList.InWardsCountTotal > 0)
                    {
                        model.InventoryReportViewModel.Count++;
                        subCategoryList.Count = model.InventoryReportViewModel.Count;
                        subCategoryList.ClosingBalanceRateTotal = 0;
                        Cateogory.SubCategoryList.Add(subCategoryList);
                    }
                }
                if (Cateogory.SubCategoryList.Count > 0)
                {
                    model.InventoryReportViewModel.CateogoryList.Add(Cateogory);
                }
            }

            model.InventoryReportViewModel.ClosingBalance = ((model.InventoryReportViewModel.TotalOpeningValue ?? 0) + (model.InventoryReportViewModel.TotalInWardsValue ?? 0)) - (model.InventoryReportViewModel.TotalOutWardsValue ?? 0);

            model.CategoryList = DropdownManager.GetCategoryList(null);
            model.SubcategoryList = DropdownManager.GetSubCategoryList(null);
            model.SKUList = DropdownManager.GetSKUList(0);
            model.LocationList = DropdownManager.GetLocationByBranch(SessionManager.GetSessionBranch().BranchId);
            model.BrandList = DropdownManager.GetBrandList(null);

            model.CategoryList.Remove(model.CategoryList.Where(x => x.Value == "-1").FirstOrDefault());
            model.SubcategoryList.Remove(model.SubcategoryList.Where(x => x.Value == "-1").FirstOrDefault());
            model.SKUList.Remove(model.SKUList.Where(x => x.Value == "-1").FirstOrDefault());
            model.BrandList.Remove(model.BrandList.Where(x => x.Value == "-1").FirstOrDefault());
            model.LocationList.Remove(model.LocationList.Where(x => x.Value == "-1").FirstOrDefault());

            Dictionary<AttributeSet, List<SelectListItem>> AttributeDictionayList = null;
            model.AttributeSetListNew = db.GetAttributeSets();

            AttributeDictionayList = new Dictionary<AttributeSet, List<SelectListItem>>();
            for (int i = 0; i < model.AttributeSetListNew.Count; i++)
            {
                int Id = model.AttributeSetListNew[i].AttributeSetId;
                model.AttributeSetListNew[i].AttributeValueList = DropdownManager.GetAttributeValueList(Id);
                AttributeDictionayList.Add(model.AttributeSetListNew[i], model.AttributeSetListNew[i].AttributeValueList);
            }
            model.AttributeDictionayList = AttributeDictionayList;
            return View(model);
        }

        public bool CompareJobSheetDateIsGreater(JobSheet jobSheet,DateTime dateTime)
        {
            if(jobSheet == null)
            {
                return false;
            }
            else if(jobSheet.JobDate <= dateTime)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CompareJobSheetDateIsless(JobSheet jobSheet, DateTime dateTime)
        {
            if (jobSheet == null)
            {
                return false;
            }
            else if (jobSheet.JobDate > dateTime)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool ComparePurchaseDateIsGreater(PurchaseInvoice purchaseInvoice, DateTime dateTime)
        {
            if (purchaseInvoice == null)
            {
                return false;
            }
            else if (purchaseInvoice.EntryDate <= dateTime)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ComparePurchaseDateIsLess(PurchaseInvoice purchaseInvoice, DateTime dateTime)
        {
            if (purchaseInvoice == null)
            {
                return false;
            }
            else if (purchaseInvoice.EntryDate > dateTime)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public ActionResult AgingReport()
        {
            CustomInventoryReportModal model = new CustomInventoryReportModal();
            model.CategoryList = DropdownManager.GetCategoryList(null);
            model.SubcategoryList = DropdownManager.GetSubCategoryList(null);
            model.SKUList = DropdownManager.GetSKUList(0);
            model.BrandList = DropdownManager.GetBrandList(null);
            model.LocationList = DropdownManager.GetLocationByBranch(SessionManager.GetSessionBranch().BranchId);

            model.CategoryList.Remove(model.CategoryList.Where(x => x.Value == "-1").FirstOrDefault());
            model.SubcategoryList.Remove(model.SubcategoryList.Where(x => x.Value == "-1").FirstOrDefault());
            model.SKUList.Remove(model.SKUList.Where(x => x.Value == "-1").FirstOrDefault());
            model.BrandList.Remove(model.BrandList.Where(x => x.Value == "-1").FirstOrDefault());
            model.LocationList.Remove(model.LocationList.Where(x => x.Value == "-1").FirstOrDefault());

            Dictionary<AttributeSet, List<SelectListItem>> AttributeDictionayList = null;
            model.AttributeSetListNew = db.GetAttributeSets();
            AttributeDictionayList = new Dictionary<AttributeSet, List<SelectListItem>>();
            for (int i = 0; i < model.AttributeSetListNew.Count; i++)
            {
                int Id = model.AttributeSetListNew[i].AttributeSetId;
                model.AttributeSetListNew[i].AttributeValueList = DropdownManager.GetAttributeValueList(Id);
                AttributeDictionayList.Add(model.AttributeSetListNew[i], model.AttributeSetListNew[i].AttributeValueList);
            }
            model.AttributeDictionayList = AttributeDictionayList;
            return View(model);
        }

        [HttpPost]
        public ActionResult AgingReport(CustomInventoryReportModal model)
        {
            model.InventoryReportViewModel = new InventoryReportViewModel();
            try
            {
                List<Product> ProductList = new List<Product>();
                List<Brand> BrandList = db.Brands.ToList();
                List<PurchaseInvoiceItem> PurchaseInvoiceItemList = new List<PurchaseInvoiceItem>();
                List<SKUMaster> SKUList = new List<SKUMaster>();
                List<int> SKUIds = new List<int>();
                if (model.SKUIds != null)
                {
                    SKUList = db.SKUMasters.Where(p => model.SKUIds.Contains(p.SKUMasterId)).ToList();
                    SKUIds = SKUList.Select(x => x.SKUMasterId).ToList();
                }
                else if (model.SubCategoryIds != null)
                {
                    SKUList = db.SKUMasters.Where(p => model.SubCategoryIds.Contains(p.SubCategoryId)).ToList();
                    SKUIds = SKUList.Select(x => x.SKUMasterId).ToList();
                }
                else if (model.CategoryIds != null)
                {
                    SKUList = db.SKUMasters.Where(p => model.CategoryIds.Contains(p.CategoryId)).ToList();
                    SKUIds = SKUList.Select(x => x.SKUMasterId).ToList();
                }
                else
                {
                    SKUIds = db.Products.Select(x => x.SKUMasterId).ToList();
                    SKUList = db.SKUMasters.Where(x => SKUIds.Contains(x.SKUMasterId)).ToList();
                }

                List<int> PurchaseInvoiceItemIds = new List<int>();
                List<string> AttributeSet = model.AttributeSetListNew.Select(x => x.ApplicableValues).ToList();
                AttributeSet = AttributeSet.Where(x => x != "-1").ToList();
                if (AttributeSet.Count > 0)
                {
                    List<AttributeLinking> AttributeList = db.AttributeLinkings.ToList();
                    List<int> AttributeIds = new List<int>();
                    foreach (var item in AttributeList)
                    {
                        bool IsContainAllElement = true;
                        item.ValuesList = item.Value.Split(',').ToList();
                        foreach (var Value in AttributeSet)
                        {
                            if (!item.ValuesList.Contains(Value))
                            {
                                IsContainAllElement = false;
                                break;
                            }
                        }
                        if (IsContainAllElement == true)
                        {
                            AttributeIds.Add(item.AttributeLinkingId);
                            PurchaseInvoiceItemIds.Add(item.PurchaseInvoiceItemId ?? 0);
                        }
                    }

                    if (model.BrandIds != null)
                    {
                        PurchaseInvoiceItemList = db.PurchaseInvoiceItems.Where(x => SKUIds.Contains(x.SKUMasterId) && x.IsProductCreated == true && x.CompanyId == db.CompanyId && PurchaseInvoiceItemIds.Contains(x.PurchaseInvoiceItemId) && model.BrandIds.Contains(x.BrandId)).ToList();
                        ProductList = db.Products.Where(x => SKUIds.Contains(x.SKUMasterId) && model.BrandIds.Contains(x.BrandId) && PurchaseInvoiceItemIds.Contains(x.PurchaseInvoiceItemId) && AttributeIds.Contains(x.AttributeLinkingId ?? 0) && !x.IsSold).ToList();
                    }
                    else
                    {
                        PurchaseInvoiceItemList = db.PurchaseInvoiceItems.Where(x => SKUIds.Contains(x.SKUMasterId) && x.IsProductCreated == true && x.CompanyId == db.CompanyId && PurchaseInvoiceItemIds.Contains(x.PurchaseInvoiceItemId)).ToList();
                        ProductList = db.Products.Where(x => SKUIds.Contains(x.SKUMasterId) && PurchaseInvoiceItemIds.Contains(x.PurchaseInvoiceItemId) && AttributeIds.Contains(x.AttributeLinkingId ?? 0) && !x.IsSold).ToList();
                    }
                    List<int> ProductIds = ProductList.Select(x => x.ProductId).ToList();

                }
                else
                {
                    if (model.BrandIds != null)
                    {
                        ProductList = db.Products.Where(x => SKUIds.Contains(x.SKUMasterId) && model.BrandIds.Contains(x.BrandId) && x.CompanyId == db.CompanyId && !x.IsSold).ToList();
                        PurchaseInvoiceItemList = db.PurchaseInvoiceItems.Where(x => SKUIds.Contains(x.SKUMasterId) && x.CompanyId == db.CompanyId && model.BrandIds.Contains(x.BrandId)).ToList();
                    }
                    else
                    {
                        ProductList = db.Products.Where(x => SKUIds.Contains(x.SKUMasterId) && x.CompanyId == db.CompanyId && !x.IsSold).ToList();
                        PurchaseInvoiceItemList = db.PurchaseInvoiceItems.Where(x => SKUIds.Contains(x.SKUMasterId) && x.IsProductCreated == true && x.CompanyId == db.CompanyId).ToList();
                    }
                }

                List<PurchaseInvoiceItem> ProductionItemList = PurchaseInvoiceItemList.Where(x => x.JobsheetId != null).ToList();
                List<int?> ItemIds = ProductionItemList.Select(x => x.JobsheetId).ToList();
                List<JobSheet> TempSheetList = db.JobSheets.Where(x => ItemIds.Contains(x.JobSheetId)).ToList();
                foreach (var item in ProductionItemList)
                {
                    item.JobSheet = TempSheetList.Where(x => x.JobSheetId == item.JobsheetId).FirstOrDefault();
                }
                List<PurchaseInvoiceItem> InvoiceItemList = PurchaseInvoiceItemList.Where(x => x.PurchaseInvoice != null).ToList();
                PurchaseInvoiceItemList = new List<PurchaseInvoiceItem>();
                PurchaseInvoiceItemList.AddRange(ProductionItemList.ToList());
                PurchaseInvoiceItemList.AddRange(InvoiceItemList.ToList());


                TimeZoneInfo timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                DateTime dateTime = TimeZoneInfo.ConvertTime(DateTime.UtcNow, timeZoneInfo);

                List<int> PurchaseInvoiceIds = PurchaseInvoiceItemList.Select(x => x.PurchaseInvoiceId ?? 0).ToList();
                List<PurchaseInvoice> PurchaseInvoiceList = db.PurchaseInvoices.Where(x => PurchaseInvoiceIds.Contains(x.PurchaseInvoiceId) && x.CompanyId == db.CompanyId).ToList();

                List<int> JobSheetIds = PurchaseInvoiceItemList.Select(x => x.JobsheetId ?? 0).ToList();
                List<JobSheet> JobSheetList = db.JobSheets.Where(x => JobSheetIds.Contains(x.JobSheetId) && x.CompanyId == db.CompanyId).ToList();

                List<int> AttributeLinkingIds = ProductList.Select(x => x.AttributeLinkingId ?? 0).ToList();
                List<AttributeLinking> AttributeLinkingList = db.AttributeLinkings.Where(x => AttributeLinkingIds.Contains(x.AttributeLinkingId)).ToList();

                foreach (var item in ProductList)
                {
                    item.AttributeLinking = AttributeLinkingList.Where(x => x.AttributeLinkingId == item.AttributeLinkingId).FirstOrDefault();
                    if (item.AttributeLinking != null)
                    {
                        item.AttributeList = item.AttributeLinking.Value.Split(',').ToList();
                    }
                    PurchaseInvoiceItem PurchaseInvoiceItem = PurchaseInvoiceItemList.Where(x => x.PurchaseInvoiceItemId == item.PurchaseInvoiceItemId).FirstOrDefault();
                    if (PurchaseInvoiceItem != null)
                    {
                        if (PurchaseInvoiceItem.PurchaseInvoiceId.HasValue)
                        {
                            PurchaseInvoiceItem.PurchaseInvoice = PurchaseInvoiceList.Where(x => x.PurchaseInvoiceId == PurchaseInvoiceItem.PurchaseInvoiceId).FirstOrDefault();
                            if (PurchaseInvoiceItem.PurchaseInvoice != null)
                            {
                                item.Agedays = (dateTime - (PurchaseInvoiceItem.PurchaseInvoice.EntryDate ?? dateTime)).Days;
                            }
                        }
                        else if (PurchaseInvoiceItem.JobsheetId.HasValue)
                        {
                            PurchaseInvoiceItem.JobSheet = JobSheetList.Where(x => x.JobSheetId == PurchaseInvoiceItem.JobsheetId).FirstOrDefault();
                            if (PurchaseInvoiceItem.JobSheet != null)
                            {
                                item.Agedays = (dateTime - (PurchaseInvoiceItem.JobSheet.CreatedDate ?? dateTime)).Days;
                            }
                        }
                    }
                    item.Brand = BrandList.Where(x => x.BrandId == item.BrandId).FirstOrDefault();
                }

                if (model.FromAgeNo.HasValue)
                {
                    ProductList = ProductList.Where(x => x.Agedays >= model.FromAgeNo).ToList();
                }

                if (model.ToAgeNo.HasValue)
                {
                    ProductList = ProductList.Where(x => x.Agedays <= model.ToAgeNo).ToList();
                }
                model.SKUMasterList = new SKUMasterList();
                model.SKUMasterList.AttributeSetList = model.AttributeSetListNew;
                model.SKUMasterList.ProductList = ProductList;

                model.CategoryList = DropdownManager.GetCategoryList(null);
                model.SubcategoryList = DropdownManager.GetSubCategoryList(null);
                model.SKUList = DropdownManager.GetSKUList(0);
                model.LocationList = DropdownManager.GetLocationByBranch(SessionManager.GetSessionBranch().BranchId);
                model.BrandList = DropdownManager.GetBrandList(null);

                model.CategoryList.Remove(model.CategoryList.Where(x => x.Value == "-1").FirstOrDefault());
                model.SubcategoryList.Remove(model.SubcategoryList.Where(x => x.Value == "-1").FirstOrDefault());
                model.SKUList.Remove(model.SKUList.Where(x => x.Value == "-1").FirstOrDefault());
                model.BrandList.Remove(model.BrandList.Where(x => x.Value == "-1").FirstOrDefault());
                model.LocationList.Remove(model.LocationList.Where(x => x.Value == "-1").FirstOrDefault());

                Dictionary<AttributeSet, List<SelectListItem>> AttributeDictionayList = null;
                model.AttributeSetListNew = db.GetAttributeSets();

                AttributeDictionayList = new Dictionary<AttributeSet, List<SelectListItem>>();
                for (int i = 0; i < model.AttributeSetListNew.Count; i++)
                {
                    int Id = model.AttributeSetListNew[i].AttributeSetId;
                    model.AttributeSetListNew[i].AttributeValueList = DropdownManager.GetAttributeValueList(Id);
                    AttributeDictionayList.Add(model.AttributeSetListNew[i], model.AttributeSetListNew[i].AttributeValueList);
                }
                model.AttributeDictionayList = AttributeDictionayList;
            }
            catch (Exception ex)
            {

            }
            return View(model);
        }

        public ActionResult DetailList(int id)
        {
            PurchaseInvoiceItem item = new PurchaseInvoiceItem();
            item.SKUMaster = db.SKUMasters.Where(x => x.SKUMasterId == id).FirstOrDefault();
            item.ProductList = db.Products.Where(x => x.SKUMasterId == id && x.CompanyId == db.CompanyId).ToList();
            return View(item);
        }

        public ActionResult SKUStock(int id)
        {
            SKUMasterList SKUMasterList = new SKUMasterList();
            SKUMasterList.AttributeSetList = db.GetAttributeSets();
            SKUMasterList.SKUStockList = new List<ProductQuantity>();
            SKUMasterList.SKUMaster = db.SKUMasters.Where(x => x.SKUMasterId == id).FirstOrDefault();
            SKUMasterList.ProductList = db.Products.Where(x => x.SKUMasterId == id).ToList();

            List<PurchaseInvoiceItem> PurchaseInvoiceItemList = db.PurchaseInvoiceItems.Where(x => x.SKUMasterId == id && x.IsProductCreated == true && x.CompanyId == db.CompanyId).ToList();
            List<SaleInvoiceItem> SaleInvoiceItemList = db.SaleInvoiceItems.Where(x => x.SKUMasterId == id && (x.Status == Constants.SALE_ITEM_SOLD || x.Status == Constants.SALE_ITEM_RETURN) && x.Product.CompanyId == db.CompanyId).ToList();
            List<JobSheetItem> JobSheetItemList = db.JobSheetItems.Where(x => x.Product.SKUMasterId == id && x.Product.CompanyId == db.CompanyId).ToList();
            List<int> PurchaseInvoiceIds = PurchaseInvoiceItemList.Select(x => x.PurchaseInvoiceId ?? 0).ToList();
            List<PurchaseInvoice> PurchaseInvoiceList = db.PurchaseInvoices.Where(x => PurchaseInvoiceIds.Contains(x.PurchaseInvoiceId) && x.CompanyId == db.CompanyId).ToList();

            List<int> SaleInvoiceIds = SaleInvoiceItemList.Select(x => x.SaleInvoiceId ?? 0).ToList();
            SaleInvoiceIds.AddRange(SaleInvoiceItemList.Select(x => x.ReturnSaleInvoiceId ?? 0).ToList());
            List<SaleInvoice> SaleInvoiceList = db.SaleInvoices.Where(x => SaleInvoiceIds.Contains(x.SaleInvoiceId) && x.CompanyId == db.CompanyId && x.Type != Constants.SALE_TYPE_DELIVERY_MEMO).ToList();

            List<int> JobSheetIds = JobSheetItemList.Select(x => x.JobSheetId ?? 0).ToList();
            JobSheetIds.AddRange(PurchaseInvoiceItemList.Select(x => x.JobsheetId ?? 0).ToList());
            List<JobSheet> JobSheetList = db.JobSheets.Where(x => JobSheetIds.Contains(x.JobSheetId) && x.CompanyId == db.CompanyId).ToList();

            List<int> AttributeLinkingIds = SKUMasterList.ProductList.Select(x => x.AttributeLinkingId ?? 0).ToList();
            List<AttributeLinking> AttributeLinkingList = db.AttributeLinkings.Where(x => AttributeLinkingIds.Contains(x.AttributeLinkingId)).ToList();

            foreach (var item in PurchaseInvoiceItemList)
            {
                ProductQuantity ListItem = new ProductQuantity();
                if (item.PurchaseInvoiceId.HasValue)
                {
                    ListItem.PurchaseInvoice = PurchaseInvoiceList.Where(x => x.PurchaseInvoiceId == item.PurchaseInvoiceId).FirstOrDefault();
                }
                else if (item.JobsheetId.HasValue)
                {
                    ListItem.JobSheet = JobSheetList.Where(x => x.JobSheetId == item.JobsheetId).FirstOrDefault();
                }
                ListItem.IsInword = true;
                ListItem.Count = item.Quantity;
                ListItem.Rate = item.Rate;
                ListItem.MRP = item.MRP;

                ListItem.Value = ListItem.Count * ListItem.Rate;
                SKUMasterList.SKUStockList.Add(ListItem);
            }

            foreach (var item in SKUMasterList.ProductList)
            {
                item.AttributeLinking = AttributeLinkingList.Where(x => x.AttributeLinkingId == item.AttributeLinkingId).FirstOrDefault();
                if (item.AttributeLinking != null)
                {
                    item.AttributeList = item.AttributeLinking.Value.Split(',').ToList();
                }
                PurchaseInvoiceItem PurchaseInvoiceItem = PurchaseInvoiceItemList.Where(x => x.PurchaseInvoiceItemId == item.PurchaseInvoiceItemId).FirstOrDefault();
                if (PurchaseInvoiceItem != null)
                {
                    item.PurchaseInvoiceNumber = PurchaseInvoiceList.Where(x => x.PurchaseInvoiceId == PurchaseInvoiceItem.PurchaseInvoiceId).Select(x => x.PurchaseInvoiceNo).FirstOrDefault();
                    item.PurchaseInvoiceId = PurchaseInvoiceList.Where(x => x.PurchaseInvoiceId == PurchaseInvoiceItem.PurchaseInvoiceId).Select(x => x.PurchaseInvoiceId).FirstOrDefault();
                }
            }

            foreach (var item in SaleInvoiceItemList)
            {
                ProductQuantity ListItem = new ProductQuantity();
                if (item.SaleInvoiceId.HasValue)
                {
                    ListItem.SaleInvoice = SaleInvoiceList.Where(x => x.SaleInvoiceId == item.SaleInvoiceId).FirstOrDefault();
                }
                ListItem.IsInword = false;
                if (item.Product.SellInPartial == true)
                {
                    ListItem.Count = item.ConversionValue ?? 0;
                    ListItem.Rate = item.MRP;
                    ListItem.Value = ListItem.Rate * ListItem.Count;
                }
                else
                {
                    ListItem.Count = 1;
                    ListItem.Rate = item.MRP;
                    ListItem.Value = ListItem.Rate * ListItem.Count;
                }
                if (item.Status == Constants.SALE_ITEM_RETURN)
                {
                    ProductQuantity ListItem2 = new ProductQuantity();
                    if (item.ReturnSaleInvoiceId.HasValue)
                    {
                        ListItem2.SaleInvoice = SaleInvoiceList.Where(x => x.SaleInvoiceId == item.ReturnSaleInvoiceId).FirstOrDefault();
                    }
                    ListItem2.IsInword = true;
                    if (item.Product.SellInPartial == true)
                    {
                        ListItem2.Count = item.ConversionValue ?? 0;
                        ListItem2.Rate = item.MRP;
                        ListItem2.Value = ListItem2.Rate * ListItem2.Count;
                    }
                    else
                    {
                        ListItem2.Count = 1;
                        ListItem2.Rate = item.MRP;
                        ListItem2.Value = ListItem2.Rate * ListItem2.Count;
                    }
                    SKUMasterList.SKUStockList.Add(ListItem2);
                }
                SKUMasterList.SKUStockList.Add(ListItem);
            }

            foreach (var item in JobSheetItemList)
            {
                ProductQuantity ListItem = new ProductQuantity();
                ListItem.IsInword = false;
                if (item.JobSheetId.HasValue)
                {
                    ListItem.JobSheet = JobSheetList.Where(x => x.JobSheetId == item.JobSheetId).FirstOrDefault();
                }
                if (item.Product.SellInPartial == true)
                {
                    ListItem.Count = item.ConversionValue ?? 0;
                    ListItem.Rate = item.Product.NetAmount ?? 0;
                    ListItem.Value = ListItem.Rate * ListItem.Count;
                }
                else
                {
                    ListItem.Count = 1;
                    ListItem.Rate = item.Product.NetAmount ?? 0;
                    ListItem.Value = ListItem.Rate * ListItem.Count;
                }
                SKUMasterList.SKUStockList.Add(ListItem);
            }

            return View(SKUMasterList);
        }

        public ActionResult ProductStock(int ProductId)
        {
            SKUMasterList SKUMasterList = new SKUMasterList();
            SKUMasterList.SKUStockList = new List<ProductQuantity>();
            Product Product = db.Products.Where(x => x.ProductId == ProductId).FirstOrDefault();
            List<PurchaseInvoiceItem> PurchaseInvoiceItemList = db.PurchaseInvoiceItems.Where(x => x.PurchaseInvoiceItemId == Product.PurchaseInvoiceItemId).ToList();
            List<SaleInvoiceItem> SaleInvoiceItemList = db.SaleInvoiceItems.Where(x => x.ProductId == ProductId && x.Status == Constants.SALE_ITEM_SOLD && x.Product.CompanyId == db.CompanyId).ToList();
            List<JobSheetItem> JobSheetItemList = db.JobSheetItems.Where(x => x.ProductId == ProductId && x.Product.CompanyId == db.CompanyId).ToList();

            List<int> PurchaseInvoiceIds = PurchaseInvoiceItemList.Select(x => x.PurchaseInvoiceId ?? 0).ToList();
            List<PurchaseInvoice> PurchaseInvoiceList = db.PurchaseInvoices.Where(x => PurchaseInvoiceIds.Contains(x.PurchaseInvoiceId) && x.CompanyId == db.CompanyId).ToList();

            List<int> SaleInvoiceIds = SaleInvoiceItemList.Select(x => x.SaleInvoiceId ?? 0).ToList();
            SaleInvoiceIds.AddRange(SaleInvoiceItemList.Select(x => x.ReturnSaleInvoiceId ?? 0).ToList());
            List<SaleInvoice> SaleInvoiceList = db.SaleInvoices.Where(x => SaleInvoiceIds.Contains(x.SaleInvoiceId) && x.CompanyId == db.CompanyId && x.Type != Constants.SALE_TYPE_DELIVERY_MEMO).ToList();

            List<int> JobSheetIds = JobSheetItemList.Select(x => x.JobSheetId ?? 0).ToList();
            JobSheetIds.AddRange(PurchaseInvoiceItemList.Select(x => x.JobsheetId ?? 0).ToList());
            List<JobSheet> JobSheetList = db.JobSheets.Where(x => JobSheetIds.Contains(x.JobSheetId) && x.CompanyId == db.CompanyId).ToList();

            foreach (var item in PurchaseInvoiceItemList)
            {
                ProductQuantity ListItem = new ProductQuantity();
                if (item.PurchaseInvoiceId.HasValue)
                {
                    ListItem.PurchaseInvoice = PurchaseInvoiceList.Where(x => x.PurchaseInvoiceId == item.PurchaseInvoiceId).FirstOrDefault();
                }
                else if (item.JobsheetId.HasValue)
                {
                    ListItem.JobSheet = JobSheetList.Where(x => x.JobSheetId == item.JobsheetId).FirstOrDefault();
                }
                ListItem.IsInword = true;
                ListItem.Count = item.Quantity;
                ListItem.Rate = item.Rate;

                if (item.SellInPartial == true)
                {
                    ListItem.Value = ListItem.Rate;
                }
                else
                {
                    ListItem.Value = ListItem.Count * ListItem.Rate;
                }
                SKUMasterList.SKUStockList.Add(ListItem);
            }

            foreach (var item in SaleInvoiceItemList)
            {
                ProductQuantity ListItem = new ProductQuantity();
                if (item.SaleInvoiceId.HasValue)
                {
                    ListItem.SaleInvoice = SaleInvoiceList.Where(x => x.SaleInvoiceId == item.SaleInvoiceId).FirstOrDefault();
                }
                ListItem.IsInword = false;
                if (item.Product.SellInPartial == true)
                {
                    ListItem.Count = item.ConversionValue ?? 0;
                    ListItem.Rate = item.MRP;
                    ListItem.Value = ListItem.Rate * ListItem.Count;
                }
                else
                {
                    ListItem.Count = 1;
                    ListItem.Rate = item.MRP;
                    ListItem.Value = ListItem.Rate * ListItem.Count;
                }
                if (item.Status == Constants.SALE_ITEM_RETURN)
                {
                    ProductQuantity ListItem2 = new ProductQuantity();
                    if (item.ReturnSaleInvoiceId.HasValue)
                    {
                        ListItem2.SaleInvoice = SaleInvoiceList.Where(x => x.SaleInvoiceId == item.ReturnSaleInvoiceId).FirstOrDefault();
                    }
                    ListItem2.IsInword = true;
                    if (item.Product.SellInPartial == true)
                    {
                        ListItem2.Count = item.ConversionValue ?? 0;
                        ListItem2.Rate = item.MRP;
                        ListItem2.Value = ListItem2.Rate * ListItem2.Count;
                    }
                    else
                    {
                        ListItem2.Count = 1;
                        ListItem2.Rate = item.MRP;
                        ListItem2.Value = ListItem2.Rate * ListItem2.Count;
                    }
                    SKUMasterList.SKUStockList.Add(ListItem2);
                }
                SKUMasterList.SKUStockList.Add(ListItem);
            }

            foreach (var item in JobSheetItemList)
            {
                ProductQuantity ListItem = new ProductQuantity();
                ListItem.IsInword = false;
                if (item.JobSheetId.HasValue)
                {
                    ListItem.JobSheet = JobSheetList.Where(x => x.JobSheetId == item.JobSheetId).FirstOrDefault();
                }
                if (item.Product.SellInPartial == true)
                {
                    ListItem.Count = item.ConversionValue ?? 0;
                    ListItem.Rate = item.MRP;
                    ListItem.Value = ListItem.Rate * ListItem.Count;
                }
                else
                {
                    ListItem.Count = 1;
                    ListItem.Rate = item.MRP;
                    ListItem.Value = ListItem.Rate * ListItem.Count;
                }
                SKUMasterList.SKUStockList.Add(ListItem);
            }

            return View(SKUMasterList);
        }

        public ActionResult ProductSearch(string SearchProduct,int? SKUMasterId)
        {
            ProductSearchViewModel model = new ProductSearchViewModel();
            if(!string.IsNullOrEmpty(SearchProduct) || SKUMasterId.HasValue)
            {
                model.SearchProduct = SearchProduct;
                model.SKUMasterId = SKUMasterId ?? -1;
                model.SearchType = "Product";
            }
            //model.CategoryList = DropdownManager.GetCategoryList(null);
            //model.SubcategoryList = DropdownManager.GetSubCategoryList(null);
            //model.SKUList = DropdownManager.GetSKUList(0);
            return View(model);
        }

        [HttpPost]
        public ActionResult _SKUDetails(int SKUMasterId)
        {
            ProductSearchViewModel model = new ProductSearchViewModel();
            model.SKUMasterList = new SKUMasterList();
            model.SKUMasterList.SKUStockList = new List<ProductQuantity>();
            model.SKUMasterList.AttributeSetList = db.GetAttributeSets();
            model.SKUMasterList.SKUMaster = db.SKUMasters.Where(x => x.SKUMasterId == SKUMasterId).FirstOrDefault();
            model.SKUMasterList.ProductList = db.Products.Where(x => x.SKUMasterId == SKUMasterId).ToList();

            List<PurchaseInvoiceItem> PurchaseInvoiceItemList = db.PurchaseInvoiceItems.Where(x => x.SKUMasterId == SKUMasterId && x.IsProductCreated == true && x.CompanyId == db.CompanyId).ToList();
            List<SaleInvoiceItem> SaleInvoiceItemList = db.SaleInvoiceItems.Where(x => x.SKUMasterId == SKUMasterId && (x.Status == Constants.SALE_ITEM_SOLD || x.Status == Constants.SALE_ITEM_RETURN) && x.Product.CompanyId == db.CompanyId).ToList();
            List<JobSheetItem> JobSheetItemList = db.JobSheetItems.Where(x => x.Product.SKUMasterId == SKUMasterId && x.Product.CompanyId == db.CompanyId).ToList();
            List<int> PurchaseInvoiceIds = PurchaseInvoiceItemList.Select(x => x.PurchaseInvoiceId ?? 0).ToList();
            List<PurchaseInvoice> PurchaseInvoiceList = db.PurchaseInvoices.Where(x => PurchaseInvoiceIds.Contains(x.PurchaseInvoiceId) && x.CompanyId == db.CompanyId).ToList();

            List<int> SaleInvoiceIds = SaleInvoiceItemList.Select(x => x.SaleInvoiceId ?? 0).ToList();
            SaleInvoiceIds.AddRange(SaleInvoiceItemList.Select(x => x.ReturnSaleInvoiceId ?? 0).ToList());
            List<SaleInvoice> SaleInvoiceList = db.SaleInvoices.Where(x => SaleInvoiceIds.Contains(x.SaleInvoiceId) && x.CompanyId == db.CompanyId && x.Type != Constants.SALE_TYPE_DELIVERY_MEMO).ToList();

            List<int> JobSheetIds = JobSheetItemList.Select(x => x.JobSheetId ?? 0).ToList();
            JobSheetIds.AddRange(PurchaseInvoiceItemList.Select(x => x.JobsheetId ?? 0).ToList());
            List<JobSheet> JobSheetList = db.JobSheets.Where(x => JobSheetIds.Contains(x.JobSheetId) && x.CompanyId == db.CompanyId).ToList();

            List<int> AttributeLinkingIds = model.SKUMasterList.ProductList.Select(x => x.AttributeLinkingId ?? 0).ToList();
            List<AttributeLinking> AttributeLinkingList = db.AttributeLinkings.Where(x => AttributeLinkingIds.Contains(x.AttributeLinkingId)).ToList();

            foreach (var item in PurchaseInvoiceItemList)
            {
                ProductQuantity ListItem = new ProductQuantity();
                if (item.PurchaseInvoiceId.HasValue)
                {
                    ListItem.PurchaseInvoice = PurchaseInvoiceList.Where(x => x.PurchaseInvoiceId == item.PurchaseInvoiceId).FirstOrDefault();
                }
                else if (item.JobsheetId.HasValue)
                {
                    ListItem.JobSheet = JobSheetList.Where(x => x.JobSheetId == item.JobsheetId).FirstOrDefault();
                }
                ListItem.IsInword = true;
                ListItem.Count = item.Quantity;
                ListItem.Rate = item.Rate;
                ListItem.MRP = item.MRP;

                ListItem.Value = ListItem.Count * ListItem.Rate;
                model.SKUMasterList.SKUStockList.Add(ListItem);
            }

            foreach (var item in model.SKUMasterList.ProductList)
            {
                item.AttributeLinking = AttributeLinkingList.Where(x => x.AttributeLinkingId == item.AttributeLinkingId).FirstOrDefault();
                if (item.AttributeLinking != null)
                {
                    item.AttributeList = item.AttributeLinking.Value.Split(',').ToList();
                }
                else
                {
                    int Count = db.AttributeSets.Count();
                    for (int i = 0; i < Count; i++)
                    {
                        item.AttributeList.Add("None");
                    }
                }

                PurchaseInvoiceItem PurchaseInvoiceItem = PurchaseInvoiceItemList.Where(x => x.PurchaseInvoiceItemId == item.PurchaseInvoiceItemId).FirstOrDefault();
                if (PurchaseInvoiceItem != null)
                {
                    item.PurchaseInvoiceNumber = PurchaseInvoiceList.Where(x => x.PurchaseInvoiceId == PurchaseInvoiceItem.PurchaseInvoiceId).Select(x => x.PurchaseInvoiceNo).FirstOrDefault();
                    item.PurchaseInvoiceId = PurchaseInvoiceList.Where(x => x.PurchaseInvoiceId == PurchaseInvoiceItem.PurchaseInvoiceId).Select(x => x.PurchaseInvoiceId).FirstOrDefault();
                }
            }

            foreach (var item in SaleInvoiceItemList)
            {
                ProductQuantity ListItem = new ProductQuantity();
                if (item.SaleInvoiceId.HasValue)
                {
                    ListItem.SaleInvoice = SaleInvoiceList.Where(x => x.SaleInvoiceId == item.SaleInvoiceId).FirstOrDefault();
                }
                ListItem.IsInword = false;
                if (item.Product.SellInPartial == true)
                {
                    ListItem.Count = item.ConversionValue ?? 0;
                    ListItem.Rate = item.MRP;
                    ListItem.Value = ListItem.Rate * ListItem.Count;
                }
                else
                {
                    ListItem.Count = 1;
                    ListItem.Rate = item.MRP;
                    ListItem.Value = ListItem.Rate * ListItem.Count;
                }
                if (item.Status == Constants.SALE_ITEM_RETURN)
                {
                    ProductQuantity ListItem2 = new ProductQuantity();
                    if (item.ReturnSaleInvoiceId.HasValue)
                    {
                        ListItem2.SaleInvoice = SaleInvoiceList.Where(x => x.SaleInvoiceId == item.ReturnSaleInvoiceId).FirstOrDefault();
                    }
                    ListItem2.IsInword = true;
                    if (item.Product.SellInPartial == true)
                    {
                        ListItem2.Count = item.ConversionValue ?? 0;
                        ListItem2.Rate = item.MRP;
                        ListItem2.Value = ListItem2.Rate * ListItem2.Count;
                    }
                    else
                    {
                        ListItem2.Count = 1;
                        ListItem2.Rate = item.MRP;
                        ListItem2.Value = ListItem2.Rate * ListItem2.Count;
                    }
                    model.SKUMasterList.SKUStockList.Add(ListItem2);
                }
                model.SKUMasterList.SKUStockList.Add(ListItem);
            }

            foreach (var item in JobSheetItemList)
            {
                ProductQuantity ListItem = new ProductQuantity();
                ListItem.IsInword = false;
                if (item.JobSheetId.HasValue)
                {
                    ListItem.JobSheet = JobSheetList.Where(x => x.JobSheetId == item.JobSheetId).FirstOrDefault();
                }
                if (item.Product.SellInPartial == true)
                {
                    ListItem.Count = item.ConversionValue ?? 0;
                    ListItem.Rate = item.Product.NetAmount ?? 0;
                    ListItem.Value = ListItem.Rate * ListItem.Count;
                }
                else
                {
                    ListItem.Count = 1;
                    ListItem.Rate = item.Product.NetAmount ?? 0;
                    ListItem.Value = ListItem.Rate * ListItem.Count;
                }
                model.SKUMasterList.SKUStockList.Add(ListItem);
            }
            return PartialView(model);
        }


        [HttpPost]
        public ActionResult _ProductDetails(string SearchProduct)
        {
            ProductSearchViewModel model = new ProductSearchViewModel();
            Product Product = db.Products.Where(x => x.SerialNumber == SearchProduct && x.CompanyId == db.CompanyId).FirstOrDefault();
            if (Product != null)
            {
                model.SKUMasterList = new SKUMasterList();
                model.SKUMasterList.SKUStockList = new List<ProductQuantity>();
                model.SKUMasterList.AttributeSetList = db.GetAttributeSets();
                model.SKUMasterList.SKUMaster = db.SKUMasters.Where(x => x.SKUMasterId == Product.SKUMasterId).FirstOrDefault();
                model.SKUMasterList.ProductList = db.Products.Where(x => x.ProductId == Product.ProductId).ToList();

                PurchaseInvoiceItem PurchaseInvoiceItem = db.PurchaseInvoiceItems.Where(x => x.PurchaseInvoiceItemId == Product.PurchaseInvoiceItemId && x.IsProductCreated == true && x.CompanyId == db.CompanyId).FirstOrDefault();

                List<SaleInvoiceItem> SaleInvoiceItemList = db.SaleInvoiceItems.Where(x => x.ProductId == Product.ProductId && (x.Status == Constants.SALE_ITEM_SOLD || x.Status == Constants.SALE_ITEM_RETURN) && x.Product.CompanyId == db.CompanyId).ToList();
                List<JobSheetItem> JobSheetItemList = db.JobSheetItems.Where(x => x.ProductId == Product.ProductId && x.Product.CompanyId == db.CompanyId).ToList();

                List<PurchaseInvoice> PurchaseInvoiceList = db.PurchaseInvoices.Where(x => x.PurchaseInvoiceId == PurchaseInvoiceItem.PurchaseInvoiceId && x.CompanyId == db.CompanyId).ToList();

                List<int> SaleInvoiceIds = SaleInvoiceItemList.Select(x => x.SaleInvoiceId ?? 0).ToList();
                SaleInvoiceIds.AddRange(SaleInvoiceItemList.Select(x => x.ReturnSaleInvoiceId ?? 0).ToList());
                List<SaleInvoice> SaleInvoiceList = db.SaleInvoices.Where(x => SaleInvoiceIds.Contains(x.SaleInvoiceId) && x.CompanyId == db.CompanyId && x.Type != Constants.SALE_TYPE_DELIVERY_MEMO).ToList();

                List<int> JobSheetIds = JobSheetItemList.Select(x => x.JobSheetId ?? 0).ToList();
                JobSheetIds.Add(PurchaseInvoiceItem.JobsheetId ?? 0);
                List<JobSheet> JobSheetList = db.JobSheets.Where(x => JobSheetIds.Contains(x.JobSheetId) && x.CompanyId == db.CompanyId).ToList();

                AttributeLinking AttributeLinking = db.AttributeLinkings.Where(x => x.AttributeLinkingId == Product.AttributeLinkingId).FirstOrDefault();

                ProductQuantity ListItem1 = new ProductQuantity();

                if (PurchaseInvoiceItem.PurchaseInvoiceId.HasValue)
                {
                    ListItem1.PurchaseInvoice = PurchaseInvoiceList.Where(x => x.PurchaseInvoiceId == PurchaseInvoiceItem.PurchaseInvoiceId).FirstOrDefault();
                }
                else if (PurchaseInvoiceItem.JobsheetId.HasValue)
                {
                    ListItem1.JobSheet = JobSheetList.Where(x => x.JobSheetId == PurchaseInvoiceItem.JobsheetId).FirstOrDefault();
                }

                ListItem1.IsInword = true;
                if (Product.SellInPartial == true)
                {
                    if (AttributeLinking != null)
                    {
                        ListItem1.Count = AttributeLinking.Quantity;
                    }
                    else
                    {
                        ListItem1.Count = PurchaseInvoiceItem.Quantity;
                    }
                }
                else
                {
                    ListItem1.Count = 1;
                }
                //ListItem1.Count = PurchaseInvoiceItem.Quantity;
                ListItem1.Rate = PurchaseInvoiceItem.Rate;
                ListItem1.MRP = PurchaseInvoiceItem.MRP;

                ListItem1.Value = ListItem1.Count * ListItem1.Rate;
                model.SKUMasterList.SKUStockList.Add(ListItem1);


                foreach (var item in SaleInvoiceItemList)
                {
                    ProductQuantity ListItem = new ProductQuantity();
                    if (item.SaleInvoiceId.HasValue)
                    {
                        ListItem.SaleInvoice = SaleInvoiceList.Where(x => x.SaleInvoiceId == item.SaleInvoiceId).FirstOrDefault();
                    }
                    ListItem.IsInword = false;
                    if (item.Product.SellInPartial == true)
                    {
                        ListItem.Count = item.ConversionValue ?? 0;
                        ListItem.Rate = item.MRP;
                        ListItem.Value = ListItem.Rate * ListItem.Count;
                    }
                    else
                    {
                        ListItem.Count = 1;
                        ListItem.Rate = item.MRP;
                        ListItem.Value = ListItem.Rate * ListItem.Count;
                    }
                    if (item.Status == Constants.SALE_ITEM_RETURN)
                    {
                        ProductQuantity ListItem2 = new ProductQuantity();
                        if (item.ReturnSaleInvoiceId.HasValue)
                        {
                            ListItem2.SaleInvoice = SaleInvoiceList.Where(x => x.SaleInvoiceId == item.ReturnSaleInvoiceId).FirstOrDefault();
                        }
                        ListItem2.IsInword = true;
                        if (item.Product.SellInPartial == true)
                        {
                            ListItem2.Count = item.ConversionValue ?? 0;
                            ListItem2.Rate = item.MRP;
                            ListItem2.Value = ListItem2.Rate * ListItem2.Count;
                        }
                        else
                        {
                            ListItem2.Count = 1;
                            ListItem2.Rate = item.MRP;
                            ListItem2.Value = ListItem2.Rate * ListItem2.Count;
                        }
                        model.SKUMasterList.SKUStockList.Add(ListItem2);
                    }
                    model.SKUMasterList.SKUStockList.Add(ListItem);
                }

                foreach (var item in JobSheetItemList)
                {
                    ProductQuantity ListItem = new ProductQuantity();
                    ListItem.IsInword = false;
                    if (item.JobSheetId.HasValue)
                    {
                        ListItem.JobSheet = JobSheetList.Where(x => x.JobSheetId == item.JobSheetId).FirstOrDefault();
                    }
                    if (item.Product.SellInPartial == true)
                    {
                        ListItem.Count = item.ConversionValue ?? 0;
                        ListItem.Rate = item.Product.NetAmount ?? 0;
                        ListItem.Value = ListItem.Rate * ListItem.Count;
                    }
                    else
                    {
                        ListItem.Count = 1;
                        ListItem.Rate = item.Product.NetAmount ?? 0;
                        ListItem.Value = ListItem.Rate * ListItem.Count;
                    }
                    model.SKUMasterList.SKUStockList.Add(ListItem);
                }

                model.SKUMasterList.ProductList = model.SKUMasterList.ProductList.Where(x => x.ProductId == Product.ProductId).ToList();

                foreach (var item in model.SKUMasterList.ProductList)
                {
                    item.AttributeLinking = AttributeLinking;
                    if (item.AttributeLinking != null)
                    {
                        item.AttributeList = item.AttributeLinking.Value.Split(',').ToList();
                    }
                    else
                    {
                        int Count = db.AttributeSets.Count();
                        item.AttributeList = new List<string>();
                        for (int i = 0; i < Count; i++)
                        {
                            item.AttributeList.Add("None");
                        }
                    }
                    if (PurchaseInvoiceItem != null)
                    {
                        item.PurchaseInvoiceNumber = PurchaseInvoiceList.Where(x => x.PurchaseInvoiceId == PurchaseInvoiceItem.PurchaseInvoiceId).Select(x => x.PurchaseInvoiceNo).FirstOrDefault();
                        item.PurchaseInvoiceId = PurchaseInvoiceList.Where(x => x.PurchaseInvoiceId == PurchaseInvoiceItem.PurchaseInvoiceId).Select(x => x.PurchaseInvoiceId).FirstOrDefault();
                    }
                }
            }
            return PartialView("_SKUDetails", model);
        }

        public ActionResult _SubCategoryList(string CategoryIds)
        {
            CustomInventoryReportModal model = new CustomInventoryReportModal();
            List<string> CategoryIdList = CategoryIds.Split(',').ToList();
            List<SubCategory> SubCategoryList = db.SubCategories.Where(x => CategoryIdList.Contains(x.CategoryId.ToString())).ToList();
            model.SubcategoryList = new List<SelectListItem>();
            if (SubCategoryList != null && SubCategoryList.Count > 0)
            {
                foreach (var item in SubCategoryList)
                {
                    model.SubcategoryList.Add(new SelectListItem() { Text = item.Name, Value = item.SubCategoryId.ToString() });
                }
            }
            return PartialView(model);
        }

        public ActionResult _SKUList(string SubCategoryIds)
        {
            CustomInventoryReportModal model = new CustomInventoryReportModal();
            List<string> SubCategoryIdList = SubCategoryIds.Split(',').ToList();
            List<SKUMaster> SKUList = db.SKUMasters.Where(x => SubCategoryIdList.Contains(x.SubCategoryId.ToString())).ToList();
            model.SKUList = new List<SelectListItem>();
            if (SKUList != null && SKUList.Count > 0)
            {
                foreach (var item in SKUList)
                {
                    model.SKUList.Add(new SelectListItem() { Text = item.SKU, Value = item.SKUMasterId.ToString() });
                }
            }
            return PartialView(model);
        }

        public ActionResult GetSelectedCategory(string SubCategoryIds)
        {
            CustomInventoryReportModal model = new CustomInventoryReportModal();
            List<string> SubCategoryIdList = SubCategoryIds.Split(',').ToList();
            List<string> CategoryIdList = db.SubCategories.Where(x => SubCategoryIdList.Contains(x.SubCategoryId.ToString())).Select(x => x.CategoryId.ToString()).ToList();
            model.CategoryList = DropdownManager.GetCategoryList(null);
            if (model.CategoryList != null && model.CategoryList.Count > 0)
            {
                foreach (var item in model.CategoryList)
                {
                    if (CategoryIdList.Contains(item.Value))
                    {
                        item.Selected = true;
                    }
                }
            }
            return PartialView("_CategoryList", model);
        }

        public ActionResult GetSelectedSubCategory(string SKUIds)
        {
            CustomInventoryReportModal model = new CustomInventoryReportModal();
            List<string> SKUIdList = SKUIds.Split(',').ToList();
            List<string> SubcategoryList = db.SKUMasters.Where(x => SKUIdList.Contains(x.SKUMasterId.ToString())).Select(x => x.SubCategoryId.ToString()).ToList();
            model.SubcategoryList = DropdownManager.GetSubCategoryList(null);
            if (model.SubcategoryList != null && model.SubcategoryList.Count > 0)
            {
                foreach (var item in model.SubcategoryList)
                {
                    if (SubcategoryList.Contains(item.Value))
                    {
                        item.Selected = true;
                    }
                }
            }
            return PartialView("_SubCategoryList", model);
        }

        [HttpPost]
        public ActionResult _LedgerList(ReportViewModel ViewModel)
        {
            ProductSearchViewModel Model = new ProductSearchViewModel();
            Model.LedgerReportViewModel.StartingDate = ViewModel.FromDate;
            Model.LedgerReportViewModel.LastDate = ViewModel.ToDate;
            Model.LedgerReportViewModel.Company = SessionManager.GetSessionCompany();
            Model.LedgerReportViewModel.Branch = SessionManager.GetSessionBranch();
            Model.LedgerReportViewModel.Ledger = db.Ledgers.Where(x => x.LedgerId == ViewModel.LedgerId).FirstOrDefault();
            Model.LedgerReportViewModel.VoucherList = db.Vouchers.Where(x => x.LedgerId == ViewModel.LedgerId && x.CompanyId == db.CompanyId).OrderBy(x => x.EntryDate).ToList();

            foreach (var item in Model.LedgerReportViewModel.VoucherList)
            {
                if (item.Type == Constants.DR)
                {
                    item.OpeningBalance = Model.LedgerReportViewModel.LedgerOpeningBalanceForVoucher;
                    item.ClosingBalance = item.OpeningBalance - item.Total;
                    Model.LedgerReportViewModel.LedgerOpeningBalanceForVoucher = item.ClosingBalance;
                    Model.LedgerReportViewModel.TotalDebitAmmount = Model.LedgerReportViewModel.TotalDebitAmmount + item.Total;
                }
                else
                {
                    item.OpeningBalance = Model.LedgerReportViewModel.LedgerOpeningBalanceForVoucher;
                    item.ClosingBalance = item.OpeningBalance + item.Total;
                    Model.LedgerReportViewModel.LedgerOpeningBalanceForVoucher = item.ClosingBalance;
                    Model.LedgerReportViewModel.TotalCreditAmmount = Model.LedgerReportViewModel.TotalCreditAmmount + item.Total;
                }
            }
            if (ViewModel.FromDate.HasValue)
            {
                Model.LedgerReportViewModel.VoucherList = db.Vouchers.Where(x => x.EntryDate >= ViewModel.FromDate && x.LedgerId == ViewModel.LedgerId && x.CompanyId == db.CompanyId).OrderBy(x => x.EntryDate).ToList();
            }

            if (ViewModel.ToDate.HasValue)
            {
                Model.LedgerReportViewModel.VoucherList = Model.LedgerReportViewModel.VoucherList.Where(x => x.EntryDate <= ViewModel.ToDate).OrderBy(x => x.EntryDate).ToList();
            }
            if (Model.LedgerReportViewModel.VoucherList.Any(x => x.VoucherFor == Constants.OPENING_BALANCE))
            {
                Model.LedgerReportViewModel.Ledger.StartingBalance = Model.LedgerReportViewModel.VoucherList.Where(x => x.VoucherFor == Constants.OPENING_BALANCE).Select(x => x.ClosingBalance).FirstOrDefault();
                Model.LedgerReportViewModel.Ledger.OpeningBalanceType = Model.LedgerReportViewModel.VoucherList.Select(x => x.Type).FirstOrDefault();
            }
            else
            {
                Model.LedgerReportViewModel.Ledger.StartingBalance = Model.LedgerReportViewModel.VoucherList.Select(x => x.OpeningBalance).FirstOrDefault();
                Model.LedgerReportViewModel.Ledger.OpeningBalanceType = Model.LedgerReportViewModel.VoucherList.Select(x => x.Type).FirstOrDefault();
            }
            Model.LedgerReportViewModel.Ledger.CurrentBalance = Model.LedgerReportViewModel.VoucherList.Select(x => x.ClosingBalance).LastOrDefault();
            return PartialView(Model);
        }
    }
}