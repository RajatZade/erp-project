﻿using DataLayer;
using DataLayer.Models;
using DataLayer.Utils;
using DataLayer.ViewModels;
using ERP_System.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ERP_System.Controllers
{
    [CheckSession]
    public class TradingReportController : Controller
    {
        DbConnector db = new DbConnector();

        // GET: TradingReport
        [RBAC(AccessType = 2)]
        public ActionResult Index()
        {
            List<SaleInvoice> SaleInvoiceList = db.SaleInvoices.Where(x => (x.Type == Constants.SALE_TYPE_SALE_INVOICE || x.Type == Constants.SALE_TYPE_SALE_VOUCHER || x.Type == Constants.SALE_TYPE_SALE_EXCHANGE) && x.CompanyId == db.CompanyId).ToList();
            TradingReportViewModel Model = GeneRateReport(SaleInvoiceList);
            return View(Model);
        }
       

        [HttpPost]
        public ActionResult _ItemList(TradingReportViewModel model)
        {
            List<SaleInvoice> SaleInvoiceList = db.SaleInvoices.Where(x => (x.Type == Constants.SALE_TYPE_SALE_INVOICE || x.Type == Constants.SALE_TYPE_SALE_VOUCHER || x.Type == Constants.SALE_TYPE_SALE_EXCHANGE) && x.CompanyId == db.CompanyId).ToList();
            if (model.StartingDate != null)
            {
                SaleInvoiceList = SaleInvoiceList.Where(x => x.InvoicingDate >= model.StartingDate).ToList();
            }

            if (model.LastDate != null)
            {
                SaleInvoiceList = SaleInvoiceList.Where(x => x.InvoicingDate <= model.LastDate).ToList();
            }

            model = GeneRateReport(SaleInvoiceList);
            return PartialView(model);
        }

        public TradingReportViewModel GeneRateReport(List<SaleInvoice> SaleInvoiceList)
        {
            TradingReportViewModel Model = new TradingReportViewModel();
            Model.Branch = SessionManager.GetSessionBranch();
            Model.Company = SessionManager.GetSessionCompany();
            List<int> InvoiceIds = SaleInvoiceList.Select(x => x.SaleInvoiceId).ToList();
            List<SaleInvoiceItem> SaleInvoiceItems = db.SaleInvoiceItems.Where(x => InvoiceIds.Contains(x.SaleInvoiceId ?? 0)).ToList();
            List<ProductVoucher> ProductVouchers = db.ProductVouchers.Where(x => InvoiceIds.Contains(x.SaleInvoiceId ?? 0)).ToList();
            foreach (var item in SaleInvoiceList)
            {
                TradingReportItem Item = new TradingReportItem();
                if (item.Type == Constants.SALE_TYPE_SALE_INVOICE || item.Type == Constants.SALE_TYPE_SALE_EXCHANGE)
                {
                    List<SaleInvoiceItem> InvoiceItems = SaleInvoiceItems.Where(x => x.Status == Constants.SALE_ITEM_SOLD && x.SaleInvoiceId == item.SaleInvoiceId).ToList();
                    foreach (var InvoiceItem in InvoiceItems)
                    {
                        Item.PurchaseAmmount = Item.PurchaseAmmount + InvoiceItem.Product.NetAmount ?? 0;
                        Item.SalesAmmount = Item.SalesAmmount + InvoiceItem.SubTotal;
                    }
                }
                else if (item.Type == Constants.SALE_TYPE_SALE_VOUCHER)
                {
                    List<ProductVoucher> ProductItems = ProductVouchers.Where(x => x.SaleInvoiceId == item.SaleInvoiceId).ToList();
                    foreach (var InvoiceItem in ProductItems)
                    {
                        Item.PurchaseAmmount = 0;
                        Item.SalesAmmount = Item.SalesAmmount + InvoiceItem.Total;
                    }
                }
                Item.SaleInvoice = item;
                Item.ProfiteLoss = Item.SalesAmmount - Item.PurchaseAmmount;
                if(Item.SalesAmmount > 0)
                {
                    Model.TradingReportItemList.Add(Item);
                }
                Model.TotalPurchaseAmmount = Model.TotalPurchaseAmmount + Item.PurchaseAmmount;
                Model.TotalSalesAmmount = Model.TotalSalesAmmount + Item.SalesAmmount;
                Model.TotalProfiteLoss = Model.TotalProfiteLoss + Item.ProfiteLoss;
            }
            return Model;
        }
    }
}