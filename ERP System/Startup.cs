﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ERP_System.Startup))]
namespace ERP_System
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
