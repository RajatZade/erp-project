﻿var a;
function AddToCart(productId, type) {
    a = productId;
    $.ajax({
        type: "GET",
        url: '@Url.Action("AddToCart", "Product")?productId=' + productId + '&isBuyNow=true',
        data: param = "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: successFunc,
        error: errorFunc
    });
}

function successFunc(data, status) {
    UpdateCartCount(data);
}

function errorFunc(xhr, status, error) {
    var err = xhr.responseText;
    alert(err.Message);
}

function UpdateCartCount(data) {
    $('#cartBadge').attr('data-badge', data);
    $('#' + a).html('In Cart');
    $('#' + a).prop("disabled", true);
}