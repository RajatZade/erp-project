﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using DataLayer.Utils;

namespace ERP_System.Utils
{
    public class CheckSession : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (!SessionUserCheck.IsSessionUserNull())
            {
                filterContext.Result = new RedirectToRouteResult(
                                                               new RouteValueDictionary {
                                                { "action", "Login" },
                                                { "controller", "Login" } });
            }
        }
    }
}