﻿using DataLayer.Models;
using DataLayer.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Data.Entity;

namespace ERP_System.Utils
{
    public class RBACAttribute : AuthorizeAttribute
    {
        public int AccessType { get; set; }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            /*Create permission string based on the requested controller 
              name and action name in the format 'controllername-action'*/
            string requiredPermission = String.Format("{0}-{1}",
                   filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                   filterContext.ActionDescriptor.ActionName);

            /*Create an instance of our custom user authorisation object passing requesting 
              user's 'Windows Username' into constructor*/

            if (!RBACUtil.HasAccess(requiredPermission, AccessType))
            {
                filterContext.Result = new RedirectToRouteResult(
                                                               new RouteValueDictionary {
                                                { "action", "UnauthorizedAccess" },
                                                { "controller", "Error" } });
            }
        }
    }
}